<?php defined('BASEPATH') OR exit('No direct script access allowed');


if(! function_exists('ajax_homeVisitMemberChange')) {
    function ajax_homeVisitMemberChange() {
    	$ci=& get_instance();
    	$data = array();
    	$id = $ci->input->get('id');
    	if ( $id != '' ){
    		$member_id = $ci->input->get('member_id');

			if($member_id > 0){
				$data['result'] = $ci->db->select("fa_family_members.firstname_kh,fa_family_members.lastname_kh,fa_family_members.gender,fa_family_members.dob,fa_refugee_home_visit.*")
								->from("fa_family_members")
								->join("fa_refugee_home_visit","fa_refugee_home_visit.member_id=fa_family_members.id","left")
								->where("fa_family_members.id",$member_id)
								->order_by("fa_refugee_home_visit.day_visit DESC")
								->limit(1) 
								->get()->row();
			}else{
				$result = $ci->db->query("SELECT 
											a.firstname_kh,a.lastname_kh,a.gender,a.dob,v.* 
										FROM erp_fa_rsd_applications a
										LEFT JOIN erp_fa_refugee_home_visit v ON v.application_id = a.id
										AND v.member_id = 0
										WHERE a.id = ".$id."
										ORDER BY
											v.day_visit DESC
										LIMIT 1")->row(); 
				$data['result'] = $result;
				//$ci->erp->print_arrays($data['result']);
			}
			
    	} else {
    		$data['result'] = 'false';
    	}
		header("Content-Type: application/json");
		echo json_encode($data);
    }
}