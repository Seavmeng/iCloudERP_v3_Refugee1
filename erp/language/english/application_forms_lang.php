<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * Module: Application Forms
 * Language: English
 *
 * Last edited:
 * 30th April 2015
 *
 * Package:
 * iCloudERP ACC 3.0
 *
 * You can translate this file to your language.
 * For instruction on new language setup, please visit the documentations.
 * You also can share your language files by emailing to saleem@tecdiary.com
 * Thank you
 */ 
 
 $lang['add_refugee_card'] = "Add Refugee Card Request";
 $lang['delete_refugee_card'] = "Delete Refugee Card";
 $lang['refugee_card_request_saved'] = "Refugee Card Request Saved";
 $lang['refugee_card_request_edited'] = "Refugee Card Request Edited";
 $lang['refugee_cards_deleted'] = "Refugee Card Request Deleted";
  
 $lang['permanent_request_card_refugee_deleted'] = "Permanent Request Card Refugee Deleted";
 $lang['permanent_resident_card_refugee_saved'] = "Permanent Request Card Refugee Saved"; 
 $lang['add_permanent_resident_card_refugee'] = "Add Resident Permanent Card";
 $lang['delete_permanent_resident_card_refugee'] = "Delete Resident Permanent Card";
 
 $lang['delete_travel_document_refugee'] = "Delete Travel Document Refugee";
 $lang['travel_document_refugee_deleted'] = "Travel Document Rrefugee Deleted";
 $lang['edit_travel_document_refugee'] = "Edit Travel Document Refugee";
 $lang['travel_document_refugee_saved'] = "Travel Document Refugee Saved";
 $lang['travel_document_refugee_cannot_saved'] = "Travel Document Refugee Cannot Saved";
 $lang['travel_document_refugee_edited'] = "Travel Document Refugee Edited";
 $lang['travel_document_refugee_cannot_edited'] = "Travel Document Refugee Cannot Edited";
 $lang['add_travel_document_refugee'] = "Add Travel Document Refugee";
 
// ACTION LINK TRANSLATION
 
$lang['approve_home_visit'] = "Approve Home Visit";
$lang['reject_home_visit'] = "Reject Home Visit";
$lang['home_visit_approved'] = "Home Visit Approved";
$lang['home_visit_rejected'] = "Home Visit Rejected";
$lang['home_visit_deleted'] = "Home Visit Deleted";


$lang['approve_preliminary'] = "Approve Preliminary Stay";
$lang['reject_preliminary'] = "Reject Preliminary Stay";
$lang['preliminary_approved'] = "Preliminary Stay Approved";
$lang['preliminary_rejected'] = "Preliminary Stay Rejected";
$lang['preliminary_stay_deleted'] = "Preliminary Stay Deleted";
$lang['preliminary_stay_cannot_deleted'] = "Preliminary Stay Cannot Deleted";


$lang['approve_written_report'] = "Approve Written Report";
$lang['reject_written_report'] = "Reject Written Report";
$lang['written_report_approved'] = "Written Report Approved";
$lang['written_report_rejected'] = "Written Report Rejected";
$lang['written_report_deleted'] = "Written Report Deleted";


$lang['approve_withdrawal'] = "Approve Withdrawal Asylum";
$lang['reject_withdrawal'] = "Reject Withdrawal Asylum";
$lang['withdrawal_approved'] = "Withdrawal Asylum Approved";
$lang['withdrawal_rejected'] = "Withdrawal Asylum Rejected";
$lang['withdrawal_deleted'] = "Withdrawal Asylum deleted";
$lang['withdrawal_of_asylum_application_form'] = "Withdrawal Of Asylum Application Form";

$lang['approve_refugee'] = "Approve Refugee";
$lang['reject_refugee'] = "Reject Refugee"; 
$lang['refugee_card_approved'] = "Refugee Card Approved";
$lang['refugee_card_rejected'] = "Refugee Card Rejected";
$lang['refugee_card_deleted'] = "Refugee Card deleted";
$lang['depression'] = "Depression";
$lang['member'] = "Member";
$lang['occupation'] = "Occupation";
$lang['province'] = "Province";
 
$lang['approve_permanent_resident'] = "Approve Resident"; 
$lang['reject_permanent_resident'] = "Reject Resident"; 
$lang['permanent_resident_card_deleted'] = "Resident Card Deleted";

$lang['approve_travel_document'] = "Approve Travel Document";
$lang['reject_travel_document'] = "Reject Travel Document ";
$lang['travel_document_refugee_approved'] = "Travel Document Refugee Approved ";
$lang['travel_document_refugee_rejected'] = "Travel Document Refugee Rejected";
$lang['travel_document_refugee_delete'] = "Travel Document Refugee Deleted";
$lang['guarantee'] = "Guarantee";
$lang['error_fill_application_id'] = "Please Fill Case No";
$lang['date_request'] = "Date Request";


 
 
 
 
 
 
 
 
 
 
 
 
 
 