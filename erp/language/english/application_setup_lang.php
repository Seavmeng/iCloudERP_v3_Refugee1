<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * Module: Products
 * Language: English
 *
 * Last edited:
 * 30th April 2015
 *
 * Package:
 * iCloudERP ACC 3.0
 *
 * You can translate this file to your language.
 * For instruction on new language setup, please visit the documentations.
 * You also can share your language files by emailing to saleem@tecdiary.com
 * Thank you
 */
$lang['department_refugee_deleted'] = "Refugee Department Deleted";
$lang['department_refugee_added'] = "Refugee Department Added";
$lang['department_refugee_edited'] = "Refugee Department Edited";
$lang['add_department_refugee'] = "Add Refugee Department";
$lang['edit_department_refugee'] = "Edit Refugee Department";
$lang['delete_department_refugee'] = "Delete Refugee Department";
$lang['department_kh'] = "Refugee Department director(Kh)";
$lang['department_en'] = "Refugee Department director(En)";
$lang['please_select_all'] = "You miss select options. Please Select All Options to submit your request";
$lang['department_refugee'] = "Department Refugee";
$lang['add_recognition_template'] = "បន្ថែម ទម្រង់ប្រកាស";
$lang['edit_recognition_template'] = "កែប្រែ ទម្រង់ប្រកាស";
$lang['delete_recognition_template'] = "លុប ទម្រង់ប្រកាស";
$lang['recognition_template_deleted'] = "ទម្រង់ប្រកាស​បានលុប";
$lang['recognition_template_added'] = "ទម្រង់ប្រកាស​បានកត់ត្រា";
$lang['recognition_template_edited'] = "ទម្រង់ប្រកាស​បានកែប្រែ";
$lang['delete_recognition_template'] = "លុប ទម្រង់ប្រកាស";
$lang['add_family_relationship'] = "Add Family Relationship"; 
$lang['delete_family_relationship'] = "Delete Family Relationship"; 
$lang['edit_family_relationship'] = "Edit Family Relationship"; 
$lang['family_relationship_added'] = "Family Relationship Added"; 
$lang['family_relationship_deleted'] = "Family Relationship Deleted"; 
$lang['family_relationship_edited'] = "Family Relationship Edited"; 
$lang['relationship_kh'] = "Relationship(Khmer)"; 
$lang['relationship'] = "Relationship(English)"; 
$lang['manage_address'] = "Manage Address"; 
//******countries*******//
$lang['countries']="Countries";
$lang['country_code']="Country Code";
$lang['iso_code']="ISO Code";
$lang['country']="Country";
$lang['native_name']="Native Name";
$lang['region']="Region ";
$lang['add_country']="Add Country ";
$lang['delete_country']="Delete Country ";
$lang['edit_country']="Edit Country ";
$lang['countries_added']="Country Added ";
$lang['countries_edited']="Country Updated ";
$lang['country_deleted']="Country Deleted ";
$lang['update']="Update  ";
$lang['states']="States ";
$lang['add_state']="Add States  ";
$lang['edit_state']="Edit States  ";
$lang['delete_state']="Delete States  ";
$lang['state_added']=" States Is Added ";
$lang['state_edited']=" States Is Edited ";
$lang['state_deleted']=" States Is Deleted ";
$lang['add_province']="Add Province ";
$lang['edit_province']="Edit Province ";
$lang['delete_province']="Delete Province ";
$lang['provinces']=" Province ";
$lang['province_added']=" Province Is Added ";
$lang['province_edited']=" Province Is Edited ";
$lang['province_deleted']=" Province Is Deleted ";
$lang['communces']="Communces ";
$lang['communce_added']="Communces is added";
$lang['communce_edited']="Communces is edited";
$lang['communce_deleted']="Communces is deleted	";
$lang['add_communce']="Add Communce ";
$lang['edit_communce']=" Edit Communce";
$lang['delete_communce']=" Delete Communce";
$lang['districts']="Districts ";
$lang['add_district']="Add District ";
$lang['edit_district']="Edit District ";
$lang['delete_district']="Delete District ";
$lang['district_added']=" District Is Added";
$lang['district_edited']=" District Is Edited";
$lang['district_deleted']=" District Is Deleted";
$lang['tags']=" tags";
$lang['add_tag']=" Add Tag";
$lang['edit_tag']=" Edit Tag";
$lang['delete_tag']=" Delete Tag";
$lang['tag_added']=" Tag Is Add";
$lang['tag_edited']=" Tag Is Edited";
$lang['tag_deleted']=" Tag Is Deleted";
$lang['delete_category']="Delete Job ";
$lang['add_category']="Add Job ";
$lang['edit_category']=" Edit Job";
$lang['categories']="Type Job";


$lang['recognition_form'] = "Recognition Form";
$lang['cessation_refugee_declaration_form'] = "Cessation Form";
$lang['cancellation_refugee_declaration_form']= "Cancellation  Form";
$lang['expellation_form'] = "Expellation Form";

$lang['case_prefix'] = "Case No Prefix"; 
$lang['no'] = "No"; 
$lang['add_case_prefix'] = "Add Case Prefix"; 
$lang['delete_case_prefix'] = "Delete Case Prefix"; 
$lang['edit_case_prefix'] = "Edit Case Prefix"; 
$lang['case_prefix_added'] = "Case Prefix Is Added";
$lang['case_prefix_deleted'] = "Case Prefix Is Deleted";  
$lang['case_prefix_edited'] = "Case Prefix Is Edited"; 
$lang['manage_application_type'] = "Manage Application Type";
$lang['question'] = "Question";
$lang['question_type'] = "Question Type";
$lang['question_kh'] = "Question Khmer";
$lang['level'] = "Level";
$lang['add_question'] = "Add Question";
$lang['edit_question'] = "Edit Question";
$lang['delete_question'] = "Delete Question";
$lang['order_by'] = "Order By";
$lang['action'] = "Action";
$lang['office_en'] = "Office En";
$lang['office_kh'] = "Office Kh";
$lang['office'] = "Offices";
$lang['address'] = "Address General Department of Immigration";
$lang['add_address'] = "Add Address General Department of Immigration";
$lang['edit_address'] = "Edit Address General Department of Immigration";
$lang['delete_address'] = "Delete Address General Department of Immigration";
$lang['address_added'] = " Address General Department of Immigration Is Added";
$lang['address_edited'] = "Address General Department of Immigration Is Edited";
$lang['address_deleted'] = " Address General Department of Immigration Deleted";
$lang['office_name'] = "Office Name";
$lang['location'] = "Location (En)";
$lang['location_kh'] = "Location (Kh)";
$lang['status'] = "Status"; 
$lang['edit_office'] = "Edit Office"; 
$lang['add_office'] = "Add Office"; 
$lang['delete_office'] = "Delete Office"; 
$lang['recognition_templete'] = "Recognition Template";
$lang['application_setup'] = "Application Setup";
$lang['module'] = "Module";
$lang['logs'] = "Logs";
$lang['delete_audit_logs'] = "Delete Audit Log";
$lang['method'] = "Method";
$lang['path_info'] = "Path Info";
$lang['question_saved'] = "The Question Saved";
$lang['question_updated'] = "The Question Updated";
$lang['office_added'] = "The Office Added"; 
$lang['office_edited'] = "The Office Edited"; 
$lang['office_deleted'] = "The Office Deleted";
$lang['recognition_templete_updated'] = "The Recognition Templete Updated";


















