<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * Module: Products
 * Language: English
 *
 * Last edited:
 * 30th April 2015
 *
 * Package:
 * iCloudERP ACC 3.0
 *
 * You can translate this file to your language.
 * For instruction on new language setup, please visit the documentations.
 * You also can share your language files by emailing to saleem@tecdiary.com
 * Thank you
 */
 
$lang['department_refugee_deleted'] = "នាយកដ្ឋានគ្រប់គ្រង បានលុប";
$lang['department_refugee_added'] = "នាយកដ្ឋានគ្រប់គ្រង បានបន្ថែម";
$lang['department_refugee_edited'] = "នាយកដ្ឋានគ្រប់គ្រង បានកែប្រែ";
$lang['add_department_refugee'] = "បន្ថែមនាយកដ្ឋានគ្រប់គ្រង";
$lang['edit_department_refugee'] = "កែប្រែនាយកដ្ឋានគ្រប់គ្រង";
$lang['delete_department_refugee'] = "លុបនាយកដ្ឋានគ្រប់គ្រង";
$lang['department_kh'] = "នាយកដ្ឋានគ្រប់គ្រង(ខ្មែរ)";
$lang['department_en'] = "នាយកដ្ឋានគ្រប់គ្រង(អង់គ្លេស)";
$lang['department_refugee'] = "នាយកដ្ឋានគ្រប់គ្រង  "; 
$lang['please_select_all'] = "អ្នកបានភ្លេចជ្រើសរើសទិន្នន័យ ។សូមជ្រើសជំរើសមួយគ្រប់ទម្រង់ ដែលបានបង្ហាញ";
$lang['add_recognition_template'] = "បន្ថែម ទម្រង់ប្រកាស";
$lang['edit_recognition_template'] = "កែប្រែ ទម្រង់ប្រកាស";
$lang['delete_recognition_template'] = "លុប ទម្រង់ប្រកាស";
$lang['recognition_template_deleted'] = "ទម្រង់ប្រកាស​បានលុប";
$lang['recognition_template_added'] = "ទម្រង់ប្រកាស​បានកត់ត្រា";
$lang['recognition_template_edited'] = "ទម្រង់ប្រកាស​បានកែប្រែ";
$lang['delete_recognition_template'] = "លុប ទម្រង់ប្រកាស";
$lang['add_family_relationship'] = "បន្ថែមទំនាក់ទំនងគ្រួសារ"; 
$lang['delete_family_relationship'] = "លុបទំនាក់ទំនងគ្រួសារ"; 
$lang['edit_family_relationship'] = "កែប្រែទំនាក់ទំនងគ្រួសារ"; 
$lang['family_relationship_added'] = "ទំនាក់ទំនងគ្រួសារបានរក្សាទុក"; 
$lang['family_relationship_deleted'] = "ទំនាក់ទំនងគ្រួសារបានលុប"; 
$lang['family_relationship_edited'] = "ទំនាក់ទំនងគ្រួសារ បានកែប្រែ"; 
$lang['relationship_kh'] = "ទំនាក់ទំនងគ្រួសារ(ខ្មែរ)"; 
$lang['relationship'] = "ទំនាក់ទំនងគ្រួសារ(អង់គ្លេស)"; 
$lang['countries'] = "ប្រទេស";
$lang['country_code']="លេខ​កូដ​ប្រទេស";
$lang['iso_code']="កូដអាយអេសអូ";
$lang['country']="ប្រទេស";
$lang['english_name']="ឈ្មោះ(អង់គ្លេស)";
$lang['native_name']="ឈ្មោះ(ខ្មែរ)";
$lang['region']="តំបន់ ";
$lang['add_country']="បន្ថែមប្រទេស ";
$lang['delete_country']="លុបប្រទេស ";
$lang['edit_country']="កែសម្រួលប្រទេស ";
$lang['countries_added']="ប្រទេសបានបន្ថែម ";
$lang['countries_edited']="ប្រទេសបានបន្ថែម ";
$lang['country_deleted']="ប្រទេសបានលុប ";
$lang['update']="Update  ";
$lang['case_prefix'] = "លេខករណី"; 
$lang['no'] = "ទេ"; 
$lang['add_case_prefix'] = "បន្ថែមលេខផ្តើមករណី ";  
$lang['delete_case_prefix'] = "លុបលេខផ្តើមករណី"; 
$lang['edit_case_prefix'] = "កែសម្រួល លេខផ្តើមករណី";
$lang['case_prefix_added'] = "លេខផ្តើមករណីបានបន្ថែម ";
$lang['case_prefix_deleted'] = "លេខផ្តើមករណីបានលុប";  
$lang['case_prefix_edited'] = "លេខផ្តើមករណីបានកែសម្រួល "; 
$lang['manage_application_type']= "គ្រប់គ្រងប្រភេទកម្មវិធី"; 
$lang['manage_address']		       = "គ្រប់គ្រងអាសយដ្ឋាន";
$lang['question'] = "Question";
$lang['question_type'] = "Question Type";
$lang['question_kh'] = "Question Khmer";
$lang['level'] = "Level";
$lang['add_question'] = "Add Question";
$lang['edit_question'] = "Edit Question";
$lang['delete_question'] = "Delete Question";
$lang['order_by'] = "Order By"; 
$lang['question_saved'] = "The Question Saved";
$lang['question_updated'] = "The Question Updated";
$lang['updated'] = "បានធ្វើការកែប្រែ";
$lang['office_en'] = "ការិយាល័យ (En)";
$lang['office_kh'] = "ការិយាល័យ (Kh)";
$lang['office'] = "ការិយាល័យ";
$lang['address'] = "អាសយដ្ឋានអគ្គនាយកដ្ឋានអន្តោប្រវេសន៍ ";
$lang['add_address'] = "បន្ថែមអាសយដ្ឋានអគ្គនាយកដ្ឋានអន្តោប្រវេសន៍";
$lang['edit_address'] = "កែប្រែអាសយដ្ឋានអគ្គនាយកដ្ឋានអន្តោប្រវេសន៍";
$lang['delete_address'] = "លុបអាសយដ្ឋានអគ្គនាយកដ្ឋានអន្តោប្រវេសន៍";
$lang['address_added'] = " អាសយដ្ឋានអគ្គនាយកដ្ឋានអន្តោប្រវេសន៍បានបន្ថែម";
$lang['address_edited'] = "អាសយដ្ឋានអគ្គនាយកដ្ឋានអន្តោប្រវេសន៍បានកែប្រែ";
$lang['address_deleted'] = " អាសយដ្ឋានអគ្គនាយកដ្ឋានអន្តោប្រវេសន៍បានលុប";
$lang['office_name'] = "ឈ្មោះការិយាល័យ";
$lang['id'] = "ល.រ";
$lang['location'] = "ទីតាំង (En)";
$lang['location_kh'] = "ទីតាំង (Kh)";
$lang['status'] = "ស្ថានភាព"; 
$lang['action'] = "សកម្មភាព"; 
$lang['edit_office'] = "កែសម្រួលការិយាល័យ"; 
$lang['add_office'] = "បន្ថែមការិយាល័យ"; 
$lang['delete_office'] = "លុបការិយាល័យ"; 
$lang['office_added'] = "ការិយាល័យត្រូវបានបន្ថែម"; 
$lang['office_edited'] = "ការិយាល័យត្រូវបានកែប្រែ"; 
$lang['office_deleted'] = "ការិយាល័យត្រូវបានលុប"; 
$lang['method'] = "កម្មវិធី";
$lang['path_info'] = "ផ្នែក";
$lang['logs'] = "សកម្មភាព";
$lang['delete_audit_logs'] = "ការលុបចោលសកម្មភាព";
$lang['update']="Update  ";
$lang['states']="រដ្ឋ ";
$lang['add_state']="បន្ថែម​រដ្ឋ  ";
$lang['state_added']=" រដ្ឋបានបន្ថែម";
$lang['state_edited']=" រដ្ឋបានកែប្រែ";
$lang['state_deleted']=" រដ្ឋបានលុប";
$lang['delete_state']="លុបរដ្ឋ ";
$lang['edit_state']="កែប្រែរដ្ឋ ";
$lang['add_province']="បន្ថែមខេត្ត";
$lang['edit_province']="កែប្រែខេត្ត";
$lang['delete_province']="លុបខេត្ត ";
$lang['provinces']=" ខេត្ត ";
$lang['province_added']=" ខេត្តបានបន្ថែម ";
$lang['province_edited']=" ខេត្តបានកែប្រែ";
$lang['province_deleted']=" ខេត្តបានលុប ";
$lang['communce_added']=" ឃុំបានបន្ថែម ";
$lang['communce_edited']=" ឃុំបានកែប្រែ ";
$lang['communce_deleted']=" ឃុំបានលុប ";
$lang['communces']=" ឃុំ ";
$lang['add_communce']=" បន្ថែមឃុំ ";
$lang['edit_communce']=" កែប្រែឃុំ ";
$lang['delete_communce']=" លុបឃុំ ";
$lang['districts']="ស្រុក ";
$lang['add_district']="បន្ថែមស្រុក";
$lang['edit_district']="កែប្រែស្រុក";
$lang['delete_district']="លុបស្រុក";
$lang['district_added']=" ស្រុកបានបន្ថែម";
$lang['district_edited']=" ស្រុកបានកែប្រែ";
$lang['district_deleted']=" ស្រុកបានលុប";
$lang['tags']=" ភូមិ";
$lang['add_tag']=" បន្ថែមភូមិ";
$lang['edit_tag']=" កែប្រែភូមិ";
$lang['delete_tag']=" លុបភូមិ";
$lang['tag_added']=" ភូមិបានបន្ថែម";
$lang['tag_edited']=" ភូមិបានកែប្រែ";
$lang['tag_deleted']="ភូមិបានលុប";
$lang['reason']="ហេតុផល";
$lang['add_reason']="បន្ថែមហេតុផល";
$lang['edit_reason']="កែប្រែហេតុផល";
$lang['delete_reason']="លុបហេតុផល";
$lang['reason_kh']="ហេតុផល";
$lang['categories']="ប្រភេទការងារ";
$lang['delete_category']="លុបប្រភេទការងារ ";
$lang['add_category']="បន្ថែមប្រភេទការងារ ";
$lang['edit_category']=" កែប្រែប្រភេទការងារ";
$lang['categorie_edited'] = "ប្រភេទការងារ​បានកែប្រែ";
$lang['categorie_added'] = "ប្រភេទការងារ​បានរក្សាទុក";
$lang['categorie_deleted'] = "ប្រភេទការងារ​បានលុប";













