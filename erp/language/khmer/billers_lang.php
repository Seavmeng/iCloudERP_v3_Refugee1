<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * Language: English
 * Module: Billers
 * 
 * Last edited:
 * 20th October 2015
 *
 * Package:
 * iCloudERP - POS v3.0
 * 
 * You can translate this file to your language. 
 * For instruction on new language setup, please visit the documentations. 
 * You also can share your language files by emailing to icloud.erp@gmail.com 
 * Thank you 
 */


$lang['add_biller']                     			= "បន្ថែមសាខា";
$lang['edit_biller']                    			= "កែសម្រួលសាខា";
$lang['delete_biller']                  			= "លុបសាខា";
$lang['delete_billers']                			= "លុបសាខា";
$lang['biller_added']                   			= "បានបន្ថែមសាខា ទទួលបានជោគជ័យ";
$lang['biller_updated']                 		= "បានកែសម្រួលសាខា ទទួលបានជោគជ័យ";
$lang['biller_deleted']                 			= "បានលុបសាខា ទទួលបានជោគជ័យ";
$lang['billers_deleted']                			= "បានលុបសាខា ទទួលបានជោគជ័យ";
$lang['no_biller_selected']            		= "ធ្វើឱ្យទាន់សម័យ Biller ទទួលបានជោគជ័យ";
$lang['invoice_footer']                 			= "ខាងក្រោមជើងវិក័យប័ត្រ";
$lang['biller_x_deleted_have_sales']    	= "សកម្មភាពដែលសាខា បរាជ័យក្នុងការលក់";
$lang['billers_x_deleted_have_sales']   	= "សាខាមួយចំនួនមិនអាចត្រូវបានលុប ដូចជាបានការលក់";
$lang['no']											= "លេខរៀង";
$lang["no"]											= "លេខរៀង";
$lang["project_name"] = "ឈ្មោះសាខា";
$lang["contact_phone"] = "លេខទូរសព្ទ័";
$lang["period"] = "រយះពេល";