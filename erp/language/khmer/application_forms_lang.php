<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * Module: Products
 * Language: English
 *
 * Last edited:
 * 30th April 2015
 *
 * Package:
 * iCloudERP ACC 3.0
 *
 * You can translate this file to your language.
 * For instruction on new language setup, please visit the documentations.
 * You also can share your language files by emailing to saleem@tecdiary.com
 * Thank you
 */  
$lang['add_refugee_card'] = "បន្ថែមពាក្យសុំប័ណ្ណសំគាល់ជនភៀសខ្លួន ";
$lang['edit_refugee_card'] = "កែប្រែពាក្យសុំប័ណ្ណសំគាល់ជនភៀសខ្លួន ";
$lang['delete_refugee_card'] = "លុបប័ណ្ណសម្គាល់ជនភៀសខ្លួន";
$lang['refugee_card_request_saved'] = " ប័ណ្ណស្នាក់នៅជនភៀសខ្លួនបានរក្សាទុក";
$lang['refugee_card_request_edited'] = " ប័ណ្ណស្នាក់នៅជនភៀសខ្លួនបានកែប្រែ";
$lang['refugee_cards_deleted'] = " ប័ណ្ណស្នាក់នៅជនភៀសខ្លួនបានលុប"; 
$lang['permanent_request_card_refugee_deleted'] = "ពាក្យសុំប័ណ្ណស្នាក់នៅបានលុប";
$lang['permanent_resident_card_refugee_saved'] = "ពាក្យសុំប័ណ្ណស្នាក់នៅបានរក្សាទុក"; 
$lang['add_permanent_resident_card_refugee'] = "បន្ថែមពាក្យសុំប័ណ្ណស្នាក់នៅ";
$lang['delete_permanent_resident_card_refugee'] = "លុបពាក្យសុំប័ណ្ណស្នាក់នៅ";
$lang['create_date'] = "កាលបរិច្ឆេទ បង្កើត​ ៖"; 
$lang['date_stop'] = "កាលបរិច្ឆេទ "; 
$lang['delete_travel_document_refugee'] = "លុបឯកសារធ្វើដំណើរជនភៀសខ្លួន";
$lang['travel_document_refugee_deleted'] = " ឯកសារធ្វើដំណើរជនភៀសខ្លួនបានលុប";
$lang['edit_travel_document_refugee'] = "កែប្រែឯកសារធ្វើដំណើរជនភៀសខ្លួន";
$lang['travel_document_refugee_saved'] = " ឯកសារធ្វើដំណើរជនភៀសខ្លួនបានរក្សាទុក";
$lang['travel_document_refugee_cannot_saved'] = "ឯកសារធ្វើដំណើរជនភៀសខ្លួនមិនបានរក្សាទុកទេ";
$lang['travel_document_refugee_edited'] = " ឯកសារធ្វើដំណើរជនភៀសខ្លួនបានកែប្រែ";
$lang['travel_document_refugee_cannot_edited'] = " ឯកសារធ្វើដំណើរជនភៀសខ្លួនមិនបានកែប្រែទេ";
$lang['add_travel_document_refugee'] = "បន្ថែមឯកសារធ្វើដំណើរជនភៀសខ្លួន";
$lang['home_visit_added'] = "កំណត់ត្រាការចុះសួរសុខទុក្ខ បានធ្វើការរក្សាទុក";
$lang['street'] = "ផ្លូវ ";
$lang['house'] = "ផ្ទះ";
$lang['just_information'] = "សំរាប់បង្ហាញពត៌មាន";
$lang['guarantee'] = "ធ្វើការធានា";
$lang['repeat'] = "ទុតិយតា";
$lang['approve_recognition_refugee'] = "ការប្រកាសទទួលស្គាល់ជនភៀសខ្លួន";
$lang['home_visit_form_sample'] = "ទម្រង់ព្រីនចេញ";
$lang['error_fill_application_id'] = "សូមបញ្ចូលលេខករណី";
$lang['date_request'] = "កាលបរិច្ឆេទសុំ";
$lang['MALE'] = "ប្រុស";
$lang['FEMALE'] = "ស្រី";
$lang['active'] = "សកម្ម";
$lang['inactive'] = "អសកម្ម";


