<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Global_model extends CI_Model
{   
	public function __construct(){
		
	}
	public function view_attach_forms(){

	}
	public function array_to_object($array = array()){
		$object = (object) $array;
		$object = new stdClass();
		foreach ($array as $key => $value){
		    $object->$key = $value;
		}
		$object = json_decode(json_encode($array), FALSE);
		return $object;
	}
	public function attach_forms($table = null, $id = null , $upload_path = ''){
		$this->load->model('applications_model','applications');
		$result = $this->db->where("id", $id)->get($table)->row();
		if($_FILES) {
			// $this->erp->checkPermissions('attachment', true, 'recognition_refugees');			
			$config_document =array(
				'upload_path' => './assets/uploads/document/'. $upload_path .'/',
				'allowed_types' => '*'
			);
			$attachment = array();
			if($result->attachment != null ) {
				$data["attachment"] = $result->attachment;
				$attachment = json_decode($result->attachment);
			} else {
				$data["attachment"] = json_encode(array());
			}
			$this->load->library('upload', $config_document);
			  
			$this->upload->initialize($config_document); 
			if($this->upload->do_upload('attachment')){  
				array_push($attachment, array(
									"file_name"=>$this->upload->data("file_name"),
									"file_rename"=>$this->input->post("file_rename"),
									"date_created"=>$this->erp->fld(date("d/m/Y H:i")))
							);
				$data["attachment"] = json_encode($attachment);
			}
			$img = array();
			if($result->img != null OR $result->img != '') {
				$data["img"] = $result->img;
				$img = json_decode($result->img);
			} else {
				$data["img"] = json_encode(array());
			}
			$pho = array();
			if ($_FILES['userfile']['name'][0] != "") {
                $config['upload_path'] = './assets/uploads/document/'. $upload_path .'/';
                $config['allowed_types'] = '*';
                $config['overwrite'] = FALSE;
                $config['max_filename'] = 25;
                $files = $_FILES;
                $cpt = count($_FILES['userfile']['name']);
                for ($i = 0; $i < $cpt; $i++) {

                    $_FILES['userfile']['name'] = $files['userfile']['name'][$i];
                    $_FILES['userfile']['type'] = $files['userfile']['type'][$i];
                    $_FILES['userfile']['tmp_name'] = $files['userfile']['tmp_name'][$i];
                    $_FILES['userfile']['error'] = $files['userfile']['error'][$i];
                    $_FILES['userfile']['size'] = $files['userfile']['size'][$i];
                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);

                    if (!$this->upload->do_upload()) {
                        $error = $this->upload->display_errors();
                        $this->session->set_flashdata('error', $error);
                    } else {
                        $pho[] = $this->upload->file_name;
                        //array_push($img, $this->upload->file_name);
                        array_push($img, array(
									"file_name"=>$this->upload->file_name,
									"file_rename"=>$files['userfile']['name'][$i],
									"date_created"=>$this->erp->fld(date("d/m/Y H:i:s")))
							);
                    }
                }
                $data["img"] = json_encode($img);
                $config = NULL;
                
            } else {
                $photos = NULL;
            }
            
			if($this->db->where("id",$id)->update($table, $data)) {
				$this->session->set_flashdata('message', lang("attachment_added"));
				redirect($_SERVER['HTTP_REFERER']);
			} 
		}
	}
	function delete_attach_forms($table = null, $id = false , $name = null, $img = null){
		$this->load->model('applications_model','applications');
		$result = $this->db->where("id",$id)->get($table)->row();
		// $result = $this->applications->getRefugeeRecognitionById($id);
		// $this->erp->checkPermissions('delete', true, 'recognition_refugees');
		$data = array();
		if ($img == null){
			$a = array();
			if($result->attachment != null ) {
				$a = json_decode($result->attachment);
				foreach ($a as $key => $rows) {
					if ($rows->file_name != $name){
						$data['attachment'][] =  $rows;
					}
				}
				
				if($data['attachment']){
					$data['attachment'] = json_encode($data['attachment']);
				}else{
					$data['attachment'] = '';
				}
			}

		} else {
			if ($img == 'img'){
				$a = array();
				if($result->img != null ) {
					$a = json_decode($result->img);
					foreach ($a as $key => $rows) {
						if ((string)$rows->file_name != (string)$name){
							$data['img'][] =  $rows;
						}
					}
					if($data['img']){
						$data['img'] = json_encode($data['img']);
					}else{
						$data['img'] = '';
					}
					
				}
			}
		}
		//echo json_encode($data);
		//echo json_encode($result);
		//$this->erp->print_arrays($data);
		if (count($data) > 0){
			if($this->db->where("id",$id)->update($table, $data)){
				$this->session->set_flashdata('message', lang("attachment_deleted"));
				redirect($_SERVER['HTTP_REFERER']);
			} else {
				$this->session->set_flashdata('error', lang("attachment_deleted"));
				redirect($_SERVER['HTTP_REFERER']);
			}
		} else {
			$this->session->set_flashdata('error', lang("attachment_deleted"));
				redirect($_SERVER['HTTP_REFERER']);
		}
		
	}
}