<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Application_setup_model extends CI_Model
{    	
	public function getQuestions()
	{
		$result = $this->db->get("fa_questions")->result();
		return $result;	 
	}
	
	public function getQuestionByid($id=null)
	{
		$row = $this->db->where("fa_questions.id",$id)
						 ->get("fa_questions")
						 ->row();
		return $row;	 
	} 
	
	public function save_question($data,$id=null)
	{
		if($id != null){
			$update = $this->db->where('id',$id)->update('fa_questions',$data);
			return $update;
		}else{
			$save=$this->db->insert('fa_questions',$data);
			return $save;
		}	
	}
	
	public function delete_question($id=null)
	{
		$result = $this->db->delete('fa_questions',array('id'=>$id));
		return $result;
	}
	 
	public function save_office($data,$id=null)
	{
		if($id != null){
			$update = $this->db->where('id',$id)->update('fa_offices',$data);
			return $update;
		}else{
			$save=$this->db->insert('fa_offices',$data);
			return $save;
		}	
	}	
	
	public function getOfficeByid($id=null)
	{
		$row = $this->db->where("fa_offices.id",$id)
						 ->get("fa_offices")
						 ->row();
		return $row;	 
	}
	
	public function delete_office($id=null)
	{
		$result = $this->db->delete('fa_offices',array('id'=>$id));
		return $result;
	}
	//**address**//
	public function save_address($data,$id=null)
	{
		if($id != null){
			$update = $this->db->where('id',$id)->update('fa_address_refugee',$data);
			return $update;
		}else{
			$save=$this->db->insert('fa_address_refugee',$data);
			return $save;
		}	
	}	
	
	public function getAddressByid($id=null)
	{
		$row = $this->db->where("fa_address_refugee.id",$id)
						 ->get("fa_address_refugee")
						 ->row();
		return $row;	 
	}
	
	public function delete_address($id=null)
	{
		$result = $this->db->delete('fa_address_refugee',array('id'=>$id));
		return $result;
	}
	public function delete_audit_logs($id=null)
	{
		$result = $this->db->delete('fa_audit_logs',array('ID'=>$id));
		return $result;
	}
	
	public function save_case_prefix($data,$id=null)
	{
		if($id != null){
			$update = $this->db->where('id',$id)->update('fa_case_prefix',$data);
			return $update;
		}else{
			$save=$this->db->insert('fa_case_prefix',$data);
			return $save;
		}	
	} 
	
	public function getCasePrefixById($id=null)
	{
		$result = $this->db->where("fa_case_prefix.id",$id)
						 ->get("fa_case_prefix")
						 ->row();
		return $result;	 
	}
	
	public function  delete_case_prefix($id=null) 
	{
		$result = $this->db->delete('fa_case_prefix',array('id'=>$id));
		return $result;
	}

	public function save_family_relationship($data,$id=null)
	{
		if($id != null){
			$update = $this->db->where('id',$id)->update('fa_family_relationship',$data);
			return $update;
		}else{
			$save=$this->db->insert('fa_family_relationship',$data);
			return $save;
		}	
	}  
	
	public function  deleteFamilyRelationship($id=null) 
	{
		$result = $this->db->delete('fa_family_relationship',array('id'=>$id));
		return $result;
	}
	
	public function getFamilyRelationshipById($id = null)
	{
		$result = $this->db->where("fa_family_relationship.id",$id)
						 ->get("fa_family_relationship")
						 ->row();
		return $result;	 
	} 

	public function getDepartmentRefugeeById($id = null)
	{
		$result = $this->db->where("fa_signature.id",$id)
						 ->get("fa_signature")
						 ->row();
		return $result;	 
	}  
	public function getTypeCaseNumber() 
	{
		$result = $this->db->get("fa_case_prefix")->result();
		return $result;	
	}

	public function delete_recognition_template($id=false) 
	{
		$result = $this->db->delete('fa_recognition',array('id'=>$id));
		return $result;
 	} 
	
	public function getRegconitionTemplate($id=false) 
	{
		$result = $this->db->where("id",$id)->get("fa_recognition")->row();
		return $result;	
	}
	
	public function addRecognition($data=null)
	{
		$result = $this->db->insert("fa_recognition",$data);
		return $result;
	}
	
	public function editRecognition($id=null,$data=null)
	{
		$result = $this->db->where('id',$id)->update("fa_recognition",$data);
		return $result;
	}
	public function getRegconitionTemplateByType($type = false) 
	{
		$result = $this->db->where("type",$type)->get("fa_recognition")->row();
		return $result;	
	}
	public function getAllRegconitionTemplate() 
	{
		$result = $this->db->get("fa_recognition")->result();
		return $result;	
	}
	
	public function save_department_refugee($data,$id=null)
	{
		if($id != null){
			$update = $this->db->where('id',$id)->update('fa_signature',$data);
			return $update;
		}else{
			$save=$this->db->insert('fa_signature',$data);
			return $save;
		}	
	}
 
	public function deleteDepartmentRefugee($id=false) 
	{
		$result = $this->db->delete('fa_signature',array('id'=>$id));
		return $result;
 	}
    //*******countries*********//
    public function save_country($data,$id=null)
	{
		if($id != null){
			$update = $this->db->where('id',$id)->update('fa_countries',$data);
			return $update;
		}else{
			$save=$this->db->insert('fa_countries',$data);
			return $save;
		}	
	}  
    public function getCountriesById($id = null)
	{
		$result = $this->db->where("fa_countries.id",$id)
						 ->get("fa_countries")
						 ->row();
		return $result;	 
	} 
    public function deleteCounrty($id=null)
    {
        $result = $this->db->delete('fa_countries',array('id'=>$id));
		return $result;
    }
	public function save_state($data,$id=null)
	{
		if($id != null){
			$update = $this->db->where('id',$id)->update('erp_fa_states',$data);
			return $update;
		}else{
			$save=$this->db->insert('erp_fa_states',$data);
			return $save;
		}	
	}
	public function getStatesById($id=null)
	{
		$result = $this->db->where("erp_fa_states.id",$id)
						 ->get("erp_fa_states")
						 ->row();
		return $result;	 
	}
	public function deleteState($id=null)
	{ 	
        $result = $this->db->delete('erp_fa_states',array('id'=>$id));
		return $result;
	}

	public function getAllCountries() {
		$result = $this->db->get("fa_countries")->result();
		return $result;
	}
	public function save_province($data,$id=null)
	{
		
		if($id != null){
			$update = $this->db->where('id',$id)->update('fa_provinces',$data);
			return $update;
		}else{
			$save=$this->db->insert('fa_provinces',$data);
			return $save;
		}	
	}
	public function getProvincesById($id=null)
	{
		$result = $this->db->where("fa_provinces.id",$id)
						 ->get("fa_provinces")
						 ->row();
		return $result;	 
	}
	public function delete_province($id=null)
	{
	   $result = $this->db->delete('fa_provinces',array('id'=>$id));
		return $result;
	}
	public function getAllStates()
	{
		$result = $this->db->get("erp_fa_states")->result();
		return $result;
	}
	public function save_communce($data,$id=null)
	{
		if($id != null){
			$update = $this->db->where('id',$id)->update('erp_fa_communces',$data);
			return $update;
		}else{
			$save=$this->db->insert('erp_fa_communces',$data);
			return $save;
		}	
	}
	public function getCommuncesById($id=null)
	{
			$result = $this->db->where("erp_fa_communces.id",$id)
						 ->get("erp_fa_communces")
						 ->row();
		return $result;	 
	}
	public function delete_communce($id=null)
	{
		$result = $this->db->delete('erp_fa_communces',array('id'=>$id));
		return $result;
	}
	public function getAllDistrict()
	{
		$result = $this->db->get("fa_districts")->result();
		return $result;
	}
	public function save_districts($data,$id=null)
	{
			if($id != null){
			$update = $this->db->where('id',$id)->update('fa_districts',$data);
			return $update;
		}else{
			$save=$this->db->insert('fa_districts',$data);
			return $save;
		}	
	}
	public function getDistrictById($id=null)
	{
		$result = $this->db->where("fa_districts.id",$id)
						 ->get("fa_districts")
						 ->row();
		return $result;	 
	}
	public function delete_district($id=null)
	{
		$result = $this->db->delete('fa_districts',array('id'=>$id));
		return $result;
	}
	public function getAllProvince()
	{
		$result = $this->db->get("fa_provinces")->result();
		return $result;
	}
	public function save_tags($data,$id=null)
	{
		if($id != null){
			$update = $this->db->where('id',$id)->update('fa_tags',$data);
			return $update;
		}else{
			$save=$this->db->insert('fa_tags',$data);
			return $save;
		}	
	}
	public function getTagById($id=null)
	{
		$result = $this->db->where("fa_tags.id",$id)
						 ->get("fa_tags")
						 ->row();
		return $result;	 
	}
	public function delete_tag($id=null)
	{
		$result = $this->db->delete('fa_tags',array('id'=>$id));
		return $result;
	}
	
	
	public function save_reason($data,$id=null)
	{
		if($id != null){
			$update = $this->db->where('id',$id)->update('fa_reasons',$data);
			return $update;
		}else{
			$save=$this->db->insert('fa_reasons',$data);
			return $save;
		}	
	}	

	public function getReasonByid($id=null)
	{
		$row = $this->db->where("fa_reasons.id",$id)
						 ->get("fa_reasons")
						 ->row();
		return $row;	 
	}

	public function delete_reason($id=null)
	{
		$result = $this->db->delete('fa_reasons',array('id'=>$id));
		return $result;
	}
	public function save_category($data,$id=null)
	{
		if($id != null){
				$update = $this->db->where('id',$id)->update('erp_categories',$data);
				return $update;
		}else{
			$save=$this->db->insert('erp_categories',$data);
			return $save;
		}	
	}
	
	public function getCategoryById($id=null)
	{
		$row = $this->db->where("erp_categories.id",$id)
							 ->get("erp_categories")
							 ->row();
			return $row;	 
	}
	public function delete_category($id=null)
	{
		$result = $this->db->delete('erp_categories',array('id'=>$id));
			return $result;
	}
		
}