<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Applications_model extends CI_Model
{    
	
	public function getReference($field) 
	{
        $q = $this->db->get_where('order_ref', array('DATE_FORMAT(date,"%Y-%m")' => date('Y-m')), 1);
        if ($q->num_rows() > 0) {
            $ref = $q->row();
			
            switch ($field) {
                case 'so':
                    $prefix = $this->Settings->sales_prefix;
                    break;
                case 'qu':
                    $prefix = $this->Settings->quote_prefix;
                    break;
                case 'po':
                    $prefix = $this->Settings->purchase_prefix;
                    break;
                case 'to':
                    $prefix = $this->Settings->transfer_prefix;
                    break;
                case 'do':
                    $prefix = $this->Settings->delivery_prefix;
                    break;
                case 'pay':
                    $prefix = $this->Settings->payment_prefix;
                    break;
                case 'pos':
                    $prefix = isset($this->Settings->sales_prefix) ? $this->Settings->sales_prefix . '/POS' : '';
                    break;
                case 're':
                    $prefix = $this->Settings->return_prefix;
                    break;
                case 'ex':
                    $prefix = $this->Settings->expense_prefix;
                    break;
				case 'sp':
                    $prefix = $this->Settings->sale_payment_prefix;
                    break;
				case 'pp':
                    $prefix = $this->Settings->purchase_payment_prefix;
                    break;
				case 'sl':
                    $prefix = $this->Settings->sale_loan_prefix;
                    break;
				case 'tr':
                    $prefix = $this->Settings->transaction_prefix;
					break;
				case 'con':
                    $prefix = $this->Settings->convert_prefix;
					break;
                case 'rep':
                    $prefix = $this->Settings->returnp_prefix;
					break;
				case 'es':
                    $prefix = $this->Settings->enter_using_stock_prefix;
					break;
				case 'esr':
                    $prefix = $this->Settings->enter_using_stock_return_prefix;
					break;
				case 'sd':
                    $prefix = $this->Settings->supplier_deposit_prefix;
					break;	
				case 'sao':
                    $prefix = $this->Settings->sale_order_prefix;
					break;
				case 'poa':
                    $prefix = $this->Settings->purchase_order_prefix;
					break;
				case 'pq':
                    $prefix = $this->Settings->purchase_request_prefix;
					break;
                default:
                    $prefix = '';
            }
			
            $ref_no = 0;
			
			if ($this->Settings->reference_format == 1) {
                $ref_no .= date('ym') . "/" . sprintf("%05s", $ref->{$field});
            }elseif ($this->Settings->reference_format == 2) {
                $ref_no .= date('Y') . "/" . sprintf("%05s", $ref->{$field});
            } elseif ($this->Settings->reference_format == 3) {
                $ref_no .= date('Y/m') . "/" . sprintf("%05s", $ref->{$field});
            } elseif ($this->Settings->reference_format == 4) {
                $ref_no .= sprintf("%03s", $ref->{$field});
            } else {
                $ref_no .= $this->getRandomReference();
            }
			
            return $ref_no;
        }
        return FALSE;
    }
	
	public function updateReference($field) 
	{
        $q = $this->db->get_where('order_ref', array('DATE_FORMAT(date,"%Y-%m")' => date('Y-m')), 1);
        if ($q->num_rows() > 0) {
            $ref = $q->row();
            $this->db->update('order_ref', array($field => $ref->{$field} + 1), array('DATE_FORMAT(date,"%Y-%m")' => date('Y-m')));
            return TRUE;
        }
        return FALSE;
    }
	
	public function getCounselingById($id = false)
	{
		$result = $this->db->where("id",$id)->get("fa_counseling")->row();
		return $result;
	}
	
	public function getCounselingMemberId($id = false)
	{	
		$result = $this->db->select("fa_family_members.*, fa_family_relationship.relationship_kh as relationship")
						->where("counseling_id",$id)
						->join("fa_family_relationship","fa_family_members.relationship=fa_family_relationship.id","left")
						->get("fa_family_members")
						->result();
		return $result;
	}
	
	public function getMemberId($id = false)
	{	
		$result = $this->db->where("id",$id)->get("fa_family_members")->row();
		return $result;
	}
	
	public function getAllMembersByApplicationId($application_id = false)
	{	
		$result = $this->db->where("application_id",$application_id)->get("fa_family_members")->result();
		return $result;
	}
	
	public function getGroupByGenderMembersByApplicationId($application_id = false, $gender = false)
	{			
		if($gender){
			$this->db->where("gender",$gender);
		}
		$result = $this->db->select("count(id) as count")->group_by("gender")->where("application_id",$application_id)->get("fa_family_members")->row();
		return $result;
	}
	
	public function getAllMembers()
	{
		$result = $this->db->get("fa_family_members")->result();
		return $result;
	}
	
	public function getAllCounseling()
	{
		$result = $this->db->get("fa_counseling")->result();
		return $result;
	}
	
	public function getCounselingByClaim($id = false)
	{
		$result = $this->db->where("id",$id)->get("fa_counseling_claim")->row();
		return $result;
	}
	
	public function getRegisterSchedule($id = false)
	{
		$result = $this->db->where("id",$id)->get("fa_rsd_applications")->row();
		return $result;
	}

	///*********
	///*** REFUGEE Suggestion 
	///*********
	public function getSuggestionsCounseling($id = false)
	{
		$result = $this->db->select("fa_counseling.*")
						   ->from("fa_counseling")
						   ->where("counseling_no",$id)
						   ->where("fa_counseling.status","approved")
						   ->where("fa_counseling.id 
								NOT IN (SELECT counseling_id FROM erp_fa_rsd_applications)
								")
						   ->get()
						   ->result();	
		return $result;
	} 
	
	public function getSuggestionsCaseNo($case_no = false)
	{
		$result = $this->db->select("fa_rsd_applications.*")
						   ->like("concat(case_prefix,case_no)",$case_no)
						   ->where("fa_rsd_applications.status","active") 	
						   ->where("erp_fa_rsd_applications.id
								NOT IN (SELECT application_id FROM erp_fa_interview_appointments WHERE erp_fa_interview_appointments.status != 'miss_interview')
								")						   
						   ->get("fa_rsd_applications") 
						   ->result();	
		return $result;
	}
	
	public function getSuggestionsCaseNoWithdrawal($case_no = false)
	{
		$result = $this->db->select("fa_rsd_applications.*")
						   ->like("concat(case_prefix,case_no)",$case_no)
						   ->where("fa_rsd_applications.status","active") 	
						   ->where("fa_rsd_applications.procedure_sequence <>","approved_1") 	
						   ->where("fa_rsd_applications.procedure_sequence <>","approved_appeal") 	
						   ->where("fa_rsd_applications.id
								NOT IN (SELECT application_id FROM erp_fa_asylum_withdrawal)
								")						   
						   ->get("fa_rsd_applications") 			   
						   ->result();	
		return $result;
	}
	
	public function getSuggestionsCaseNoSocialAffair($case_no = false,$type=false)
	{ 
		$this->db->select("fa_rsd_applications.*")
						   ->like("concat(case_prefix,case_no)",$case_no)
						   ->where("fa_rsd_applications.status","active");
		if($type == "family_guarantee"){
			  $this->db->where("fa_rsd_applications.id
								NOT IN (SELECT application_id FROM erp_fa_refugee_family_guarantee_request)
								");
		} 
		if($type == "home_visit"){
			/*   $this->db->where("fa_rsd_applications.id
								NOT IN (SELECT application_id FROM erp_fa_refugee_home_visit)
								"); */
		} 
		$result = $this->db->get("fa_rsd_applications")->result();	
		return $result;
	}
	public function getSuggestionsCaseNoSocialAffairForAddRe($case_no = false,$type=false)
	{ 
		$this->db->select("fa_rsd_applications.*")
						   ->like("concat(case_prefix,case_no)",$case_no)
						   ->where("fa_rsd_applications.status","active");
		// if($type == "family_guarantee"){
		// 	  $this->db->where("fa_rsd_applications.id
		// 						 not IN (SELECT application_id FROM erp_fa_refugee_family_guarantee_request)
		// 						");
		// } 
		if($type == "home_visit"){
			/*   $this->db->where("fa_rsd_applications.id
								NOT IN (SELECT application_id FROM erp_fa_refugee_home_visit)
								"); */
		} 
		$result = $this->db->get("fa_rsd_applications")->result();	
		return $result;
	}
	public function getSuggestionsCaseNoAppeal($case_no = false)
	{
		$result = $this->db->select("fa_rsd_applications.*")
						   ->like("concat(case_prefix,case_no)",$case_no)
						   ->where("fa_rsd_applications.status","active") 	
						   ->where("fa_rsd_applications.id
								 IN (SELECT application_id FROM erp_fa_interview_negatives_appeal WHERE status = 'approved')
								")	 
						   ->get("fa_rsd_applications") 
						   ->result();	
		return $result;
	}
	
	public function getSuggestionsCaseNoPreliminaryStay($case_no = false)
	{
		$result = $this->db->select("fa_rsd_applications.*")
						   ->like("concat(case_prefix,case_no)",$case_no)
						   // ->where("fa_rsd_applications.status","closed")	
						   ->get("fa_rsd_applications") 
						   ->result();	
		return $result;
	}
	
	public function getSuggestionsCaseNoRecognition($case_no = false)
	{
		$result = $this->db->select("fa_rsd_applications.*")
						   ->from("fa_interview_evaluations")
						   ->join("fa_rsd_applications","application_id = fa_rsd_applications.id")	
						   ->like("concat(case_prefix,case_no)",$case_no)
						   ->where("fa_interview_evaluations.status","approved") 
						   ->where("fa_rsd_applications.status","active") 
						   ->get()
						   ->result();	 
  		return $result;
	}
	
	public function getSuggestionsCaseNoRejected($case_no = false)
	{
		$result = $this->db->select("fa_rsd_applications.*")
						   ->from("fa_interview_evaluations")
						   ->join("fa_rsd_applications","application_id = fa_rsd_applications.id")	
						   ->like("concat(case_prefix,case_no)",$case_no)
						   ->where("fa_interview_evaluations.status","rejected") 
						   ->where("fa_rsd_applications.status","active") 
						   ->get()
						   ->result();	 
  		return $result;
	}	
	 
	public function getSuggestionsAllAppointments($case_no = false)
	{
		$result = $this->db->select("fa_rsd_applications.*")
						   ->join("fa_interview_appointments","application_id = fa_rsd_applications.id","right")						   
						   ->like("concat(case_prefix,case_no)",$case_no) 
						   ->where("fa_rsd_applications.status","active") 						   
						   ->get("fa_rsd_applications")
						   ->result();
		return $result;
	}
	
	public function getSuggestionsInterviewCaseNo($case_no = false)
	{
		$result = $this->db->select("fa_rsd_applications.*")
						   ->join("fa_interviews","fa_interviews.application_id = fa_rsd_applications.id","right")						   
						   ->like("concat(case_prefix,case_no)",$case_no)
						   ->where("fa_rsd_applications.status","active") 
						   ->get('fa_rsd_applications')
						   ->result();	
		return $result;
	}
	
	public function getSuggestionsCaseNoNegative($case_no = false)
	{ 
		$result = $this->db->select("fa_rsd_applications.*")
						   ->from("fa_interview_evaluations")
						   ->join("fa_rsd_applications","application_id = fa_rsd_applications.id")	
						   ->join("fa_interviews","fa_interview_evaluations.interview_id = fa_interviews.id","left")	
						   ->like("concat(case_prefix,case_no)",$case_no)
						   ->where("fa_interview_evaluations.status","rejected") 
						   ->where("fa_interviews.is_appeal",0) 
						   ->where("fa_rsd_applications.status","active")
						   ->where("fa_interview_evaluations.application_id 
								NOT IN (SELECT application_id FROM erp_fa_interview_negatives)
								")
						   ->get()
						   ->result();	 
  		return $result;
	}
	
	public function getSuggestionsCaseNoNegativeAppeal($case_no = false)
	{
		$result = $this->db->select("fa_rsd_applications.*")
						   ->from("fa_interview_evaluations")
						   ->join("fa_rsd_applications","application_id = fa_rsd_applications.id")	
						   ->join("fa_interviews","fa_interview_evaluations.interview_id = fa_interviews.id","left")	
						   ->join("fa_interview_negatives","fa_interview_negatives.application_id = fa_rsd_applications.id","left")	
						   ->like("concat(case_prefix,case_no)",$case_no)
						   ->where("fa_interview_evaluations.status","rejected")  
						   ->where("fa_interview_negatives.status","approved")  
						   ->where("fa_rsd_applications.status","active") 
						   ->where("fa_interview_evaluations.application_id 
								NOT IN (SELECT application_id FROM erp_fa_interview_negatives_appeal)
								")  
						   ->get()
						   ->result();		 
  		return $result;
	}
	
	public function getSuggestionsCaseNoDeclareAppeal($case_no = false)
	{
		$result = $this->db->select("fa_rsd_applications.*")
						   ->from("fa_interview_evaluations")
						   ->join("fa_rsd_applications","application_id = fa_rsd_applications.id")	
						   ->join("fa_interviews","fa_interview_evaluations.interview_id = fa_interviews.id","left")	
						   ->like("concat(case_prefix,case_no)",$case_no)
						   ->where("fa_interview_evaluations.status","rejected")   
						   ->where("fa_interviews.is_appeal",1)
                           ->where("fa_rsd_applications.status","active")
                           ->where("fa_interview_evaluations.application_id 
								NOT IN (SELECT application_id FROM erp_fa_interview_negatives WHERE erp_fa_interview_negatives.status = 'pending')
								")
						   ->get()
						   ->result();		 
  		return $result;
	}
	
	public function getSuggestionsCaseNoRefugeeCard($case_no = false)
	{
		$result = $this->db->select("fa_rsd_applications.*")
						   ->from("fa_interview_evaluations")
						   ->join("fa_rsd_applications","application_id = fa_rsd_applications.id")	
						   ->like("concat(case_prefix,case_no)",$case_no)
						   ->where("fa_interview_evaluations.status","approved") 
						   ->where("fa_rsd_applications.status","active")  
						   ->get()
						   ->result();	 
  		return $result;
	}
	
	public function getRSDApplicationById($id = false)
	{
		$result = $this->db->where("id",$id)->get("fa_rsd_applications")->row();		
		return $result;
	}

	public function getRSDApplicationPrintCardBlankById($id = false)
	{
		$result = $this->db->select("
								erp_fa_rsd_applications.case_prefix,
								erp_fa_rsd_applications.case_no,
								if(erp_fa_refugee_card_request.member_id > 0, erp_fa_family_members.firstname, erp_fa_rsd_applications.firstname) as firstname,
								if(erp_fa_refugee_card_request.member_id > 0, erp_fa_family_members.lastname, erp_fa_rsd_applications.lastname) as lastname,
								if(erp_fa_refugee_card_request.member_id > 0, erp_fa_family_members.firstname_kh, erp_fa_rsd_applications.firstname_kh) as firstname_kh,
								if(erp_fa_refugee_card_request.member_id > 0, erp_fa_family_members.lastname_kh, erp_fa_rsd_applications.lastname_kh) as lastname_kh,
								if(erp_fa_refugee_card_request.member_id > 0, erp_fa_family_members.gender, erp_fa_rsd_applications.gender) as gender,
								if(erp_fa_refugee_card_request.member_id > 0, erp_fa_family_members.dob, erp_fa_rsd_applications.dob) as dob,
								if(erp_fa_refugee_card_request.member_id > 0, erp_fa_family_members.nationality, erp_fa_rsd_applications.nationality) as nationality,
								if(erp_fa_refugee_card_request.member_id > 0, erp_fa_family_members.nationality_kh, erp_fa_rsd_applications.nationality_kh) as nationality_kh,
								erp_fa_refugee_card_request.recognized_no,
								erp_fa_refugee_card_request.recognized_date,
							")
							->from("fa_rsd_applications")
							->join("fa_refugee_card_request","fa_rsd_applications.id = fa_refugee_card_request.application_id")
							->join("fa_family_members","fa_family_members.id = fa_refugee_card_request.member_id","left")
							->where("fa_refugee_card_request.id",$id)
							->get()
							->row();

		return $result;
	}
	
	public function updateRefugeeRecognition($id = false, $data = false)
	{
		if($this->db->where("id",$id)->update("fa_interview_recognitions", $data)){
			return true;
		}		
		return false;
	}
	 
	public function addRefugeeRecognition($data = false, $members = false)
	{
		if($this->db->insert("fa_interview_recognitions",$data)){
			$recognition_id = $this->db->insert_id();
			foreach($members as $member){	
				$member["application_id"] = $data['application_id'];
				$member["recognition_id"] = $recognition_id;
				$member["recognition_id"] = $recognition_id;
				$this->db->insert("fa_interview_recognitions_members",$member);
			}
			return true;
		}
		return false;
	}
	
	public function getRecognitionMembersById($id = false)
	{
		$result = $this->db->where("id",$id)->get("fa_interview_recognitions")->row();
		return $result;
	}
	
	public function getRecognitionMembersByRecognitionId($id = false)
	{
		$result = $this->db->where("recognition_id",$id)->get("fa_interview_recognitions_members")->result();
		return $result;
	}
	
	public function getWithdrawalMembersByWithdrawalId($id = false)
	{
		$result = $this->db->where("withdrawal_id",$id)->get("fa_refugee_withdrawal_members")->result();
		return $result;
	}
	
	public function getAppointmentMemberByAppointmentId($id = false)
	{
		$result = $this->db->where("appointment_id",$id)->get("fa_interview_appointment_members")->result();
		return $result;
	}
	
	public function getCessationEliminationMembersByCessationId($id = false)
	{
		$result = $this->db->where("cessation_id",$id)->get("fa_refugee_cessation_elimination_members")->result();
		return $result;
	}
	
	public function deleteRefugeeRecognition($id = false)
	{
		if($this->db->where("id",$id)->update("fa_interview_recognitions",array("status" => "deleted"))){
			return true;
		}
		return false;
	}
	
	public function getRecognizationByApplicationId($id = false)
	{  
				  $this->db->where("recognized_no <>","");
		$result = $this->db->where("application_id",$id)->get("fa_interview_recognitions")->row();
		return $result; 
	}

	public function getInterviewByApplicationId($id = false)
	{  
		$result = $this->db->where("application_id",$id)->get("fa_interviews")->row();
		return $result; 
	}
	
	public function getInterviewById($id = false)
	{  
		$result = $this->db->where("id",$id)->get("fa_interviews")->row();
		return $result; 
	}
	
	public function addInterviewRefugee($data = false, $data2 = false)
	{ 
		if($this->db->insert("fa_interviews",$data)){ 
			$id = $this->db->insert_id();
			$rsd = $this->getRSDApplicationById($data['application_id']);
			if($data2){
				$this->db->where("id",$data['appointment_id'])->update("fa_interview_appointments", array("status"=>"inactive"));
				$this->db->where("id",$data['application_id'])->update("fa_rsd_applications",$data2);
				$this->db->where("id",$rsd->counseling_id)->update("fa_counseling",$data2);
			}  
			return $id;
		}
		return false;
	} 
	
	public function deleteInterviewRefugee($id = false)
	{
		if($this->db->where("id",$id)->update("fa_interviews", array("status" => "deleted"))){
			return true;
		}
		return false;
	}
	
	public function updateInterviewRefugee($id = false, $data = false, $data2 = false)
	{
		if($this->db->where("id",$id)->update("fa_interviews",$data)){	
			if($data2){
				$interview = $this->getInterviewById($id);
				$rsd = $this->getRSDApplicationById($interview->application_id);
				$this->db->where("id",$interview->application_id)->update("fa_rsd_applications",$data2);
				$this->db->where("id",$rsd->counseling_id)->update("fa_counseling",$data2);
			}
			return true;
		}
		return false;
	}
	
	public function getAppointmentById($id = false)
	{ 
		$result = $this->db->where("id",$id)->get("fa_interview_appointments")->row();
		return $result; 
	}
	
	public function addAppointmentRefugee($data = false,$members1)
	{		
		if($this->db->insert("fa_interview_appointments",$data)){
			$appointment_id = $this->db->insert_id();
			foreach($members1 as $member){	
				$member["application_id"] = $data['application_id'];
				$member["appointment_id"] = $appointment_id; 
				$this->db->insert("fa_interview_appointment_members",$member);
			}
			return true;
		}
		return false;
	 
	}
	
	public function updateAppointmentRefugee($id =false , $data = false)
	{		
		if($this->db->where("id",$id)->update("fa_interview_appointments",$data)){
			return true;
		}
		return false;		
	}
	
	public function deleteAppointmentRefugee($id =false)
	{		
		if($this->db->where("id",$id)->update("fa_interview_appointments",array("status"=>"deleted") )){
			return true;
		}
		return false;		
	}
	
	public function getAppointmentByApplicationId($id = false)
	{ 
		$result = $this->db->where("application_id",$id)->get("fa_interview_appointments")->row();
		return $result;
		
	}	 
	
	public function getQuestionaireEvaluation($level = false)
	{
		$result = $this->db->select("fa_questions.*")
						   ->from("fa_questions")
						   ->join("fa_question_types","type_id=fa_question_types.id","left")
						   ->where("question_type","Evaluation")
						   ->where("level",$level)
						   ->order_by("order_by","asc")
						   ->get()
						   ->result();
		return $result;
	}
	
	public function getQuestionKh($id = false)
	{
		$result = $this->db->where("level",$id)->get("fa_questions")->row();
		return $result->question_kh;
	}
	
	public function getEvaluationById($id = false)
	{
		$result = $this->db->where("id",$id)->get("fa_interview_evaluations")->row();
		return $result;
	}
	
	public function addEvaluation($data = false,$is_appeal = false)
	{
		if($this->db->insert("fa_interview_evaluations",$data)){
			$id = $this->db->insert_id();
			$this->db->where("id",$data['interview_id'])->update("fa_interviews", array("status"=>"approved"));	
			if($is_appeal=="is_appeal") {  
				$this->db->where("id",$data['application_id'])->update("fa_rsd_applications", array( "procedure_sequence" => "evaluating_appeal"));			
			}else {  
				$this->db->where("id",$data['application_id'])->update("fa_rsd_applications", array( "procedure_sequence" => "evaluating"));			
			} 
			return $id;
		}
		return false;
	}
	
	public function deleteEvaluation($id = false)
	{
		if($this->db->where("id",$id)->update("fa_interview_evaluations", array("status" => "deleted"))){
			return true;
		}
		return false;
	}
	
	public function updateEvaluation($id = false, $data = false)
	{
		$result = $this->getEvaluationById($id);	
		if($this->db->where("id",$id)->update("fa_interview_evaluations",$data)){
		   //$this->db->where("evaluation_id",$id)->delete("fa_interview_evaluation_detail");
			if($data['status']=="approved" && $_GET['v'] == "appeal") {
				$this->db->where("id",$result->application_id)->update("fa_rsd_applications",array('level'=>"approved2"));
			}
			if($data['status']=="approved" && $_GET['v'] == "") {
				$this->db->where("id",$result->application_id)->update("fa_rsd_applications",array('level'=>"approved1"));
			}
			if($data['status']=="rejected" && $_GET['v'] == "appeal") {
				$this->db->where("id",$result->application_id)->update("fa_rsd_applications",array('level'=>"rejected2"));
			}
			if($data['status']=="rejected" && $_GET['v'] == "") {
				$this->db->where("id",$result->application_id)->update("fa_rsd_applications",array('level'=>"rejected1"));
			}
		   return true;
		}
		return false;
	}
	
	public function addEvaluationDetail($data = false)
	{
		if($this->db->insert_batch("fa_interview_evaluation_detail",$data)){
			return $this->db->insert_id();
		}
		return false;
	}
	
	public function getEvaluationDetail($id = false, $group = false)
	{ 
		if($group){
			$this->db->where("group",$group);
		}
		$result = $this->db->select("*")
						   ->from("fa_interview_evaluation_detail")
						   ->where("fa_interview_evaluation_detail.evaluation_id",$id)
						   ->get()
						   ->result();		
		return $result;
	}  
	
	public function getWithdrawalAsylumSeekrById($id = false)
	{
		$result = $this->db->where("id",$id)->get("fa_asylum_withdrawal")->row();
		return $result;
	}
	
	public function addWithdrawalAsylumSeeker($data = false)
	{
		if($this->db->insert('fa_asylum_withdrawal', $data)){
			$this->db->where("id",$data['application_id'])->update("fa_rsd_applications", array("status"=>"inactive"));
			$this->db->where("application_id",$data['application_id'])->update("fa_interviews", array("status"=>"inactive"));
			$this->db->where("application_id",$data['application_id'])->update("fa_interview_evaluations", array("status"=>"inactive"));
			$this->db->where("application_id",$data['application_id'])->update("fa_interview_negatives", array("status"=>"inactive"));
			$this->db->where("application_id",$data['application_id'])->update("fa_interview_negatives_appeal", array("status"=>"inactive"));
			return true;
		}
		return false;
	}
	
	public function updateWithdrawalAsylumSeeker($id = false, $data = false)
	{ 
		if($this->db->where("id",$id)->update('fa_asylum_withdrawal', $data)){
			return true;
		}
		return false;
	}
	
	public function deleteWithdrawalAsylumSeeker($id = false)
	{
		$widthdrawal = $this->getWithdrawalAsylumSeekrById($id);
		if($this->db->where('id',$id)->update('fa_asylum_withdrawal',array("status" => "deleted"))){
			$this->db->where("id", $widthdrawal->application_id)->update("fa_rsd_applications", array("status"=>"active")); 
			$this->db->where("application_id",$widthdrawal->application_id)->update("fa_interviews", array("status"=>"pending"));
			$this->db->where("application_id",$widthdrawal->application_id)->update("fa_interview_evaluations", array("status"=>"pending"));
			$this->db->where("application_id",$widthdrawal->application_id)->update("fa_interview_negatives", array("status"=>"pending"));
			$this->db->where("application_id",$widthdrawal->application_id)->update("fa_interview_negatives_appeal", array("status"=>"pending"));
			
			return true;
		}
		return false;
	}

	public function getPreliminaryStayById($id = false)
	{	
		$result = $this->db->where("id",$id)->get("fa_asylum_preliminary_stay")->row();
		return $result;
	}
	
	public function addPreliminaryStay($data = false)
	{
		if($this->db->insert('fa_asylum_preliminary_stay', $data)){
			return true;
		}
		return false;
	}
	
	public function updatePreliminaryStay($id = false, $data = false)
	{
		if($this->db->where('id',$id)->update('fa_asylum_preliminary_stay', $data)){
			return true;
		}
		return false;
	}
	
	public function deletePreliminaryStay($id = false)
	{
		if($this->db->where('id',$id)->update("fa_asylum_preliminary_stay", array("status" => "deleted"))){
			return true;
		}
		return false;
	}
	
	public function getHomeVisitByAppId($id = false, $home_visit_id =  false)
	{	
		if($home_visit_id){
			$this->db->where("id",$home_visit_id);
		}
		$result = $this->db->where("application_id",$id)->get("fa_refugee_home_visit")->row();
		return $result;
	}
	
	public function getlocationByOffice()
	{
		$result = $this->db->get("fa_offices")->result();
		return $result;
	}
	
	public function getOfficeById($id = false)
	{
		$result = $this->db->where("id",$id)->get("fa_offices")->row();
		return $result;
	}
	
	public function getUsers()
	{
		$result = $this->db->select("users.*,CONCAT(last_name,' ',first_name) AS full_name")->from("users")->get()->result();
		return $result;
	}
	
	public function getSignatureById($id)
	{
		$result = $this->db->where("id",$id)->get("users")->row();
		if($result){
			if($result->signature != NULL){
				return "<img width='150' src='".base_url('assets/uploads/signature/'.$result->signature)."' /><br/><b>".strtoupper($result->username)."</b>";
			}else{
				return "<b>".strtoupper($result->username)."</b>";
			}
		}
		return false;
	}
	
	public function getCheckById($id = false)
	{
		
		$result = $this->db->where("id",$id)->get("fa_interview_evaluation_detail")->row();		
		if($result->is_yes == 1){
			$html = '<div style="float:right"><span style="font-size:17px;">&#x2611;</span>
						'.lang("បាទ").'
					 <span style="font-size:17px;">&#x2610;</span>
						'.lang("ទេ").'</div>';
			return $html;
		}else{
			$html = '<div style="float:right"><span style="font-size:17px;">&#x2610;</span>
						'.lang("បាទ").'
					<span style="font-size:17px;">&#x2611;</span>
						'.lang("ទេ").'</div>';
			return $html;
		}
	}
	
	public function getRefugeeRecognitionByApplicationId($id = false)
	{		
		$result = $this->db->where("application_id",$id)->get("fa_interview_recognitions")->row();
		return $result;
	}
	
	public function getRefugeeRecognitionById($id = false)
	{		
		$result = $this->db->where("id",$id)->get("fa_interview_recognitions")->row();
		return $result;
	}
	
	public function getMemberByApplicationId($id = null)
	{
		$result = $this->db->where("application_id")->get("fa_family_members")->result();
		return $result;
	}
	
	public function getEducationApplication($id)
	{ 
		$result = $this->db->where("counseling_id",$id)->get("fa_rsd_application_educations")->row();
		return $result;
	}
	
	public function getOccupationApplication($id)
	{
		$result = $this->db->select("erp_fa_rsd_application_occupations.*")
						   ->from("erp_fa_rsd_applications")
						   ->join("erp_fa_rsd_application_occupations","erp_fa_rsd_applications.id = erp_fa_rsd_application_occupations.application_id")
						   ->where("erp_fa_rsd_applications.id",$id)
						   ->get()
						   ->result();		
		return $result;
	}
	
	public function getIdentificationDocuments($id)
	{
		$result = $this->db->select("fa_rsd_application_documents.*")
						   ->from("erp_fa_rsd_applications")
						   ->join("fa_rsd_application_documents","erp_fa_rsd_applications.id = fa_rsd_application_documents.application_id")
						   ->where("erp_fa_rsd_applications.id",$id)
						   ->get()
						   ->result();		
		return $result;
	} 
	
	public function getFamilyMember_F($id)
	{
		$where = array(
				"fa_rsd_applications.id"=>$id, 
				'fa_family_members.non_accompanying' => 0,
				'fa_family_members.outside_country' => 0
		);
		$result = $this->db->select("fa_family_members.*")
						   ->from("fa_rsd_applications")
						   ->join("fa_counseling","fa_rsd_applications.counseling_id = fa_counseling.id")
						   ->join("fa_family_members"," fa_counseling.id = fa_family_members.counseling_id")
						   ->where($where)  
						   ->get()
						   ->result();		
		return $result;
	}
	
	public function getFamilyMember_G($id)
	{
		$where = array(
				"fa_rsd_applications.id"=>$id, 
				'fa_family_members.non_accompanying' => 1,
				'fa_family_members.outside_country' => 0
		);
		$result = $this->db->select("fa_family_members.*")
						   ->from("fa_rsd_applications")
						   ->join("fa_counseling","fa_rsd_applications.counseling_id = fa_counseling.id")
						   ->join("fa_family_members"," fa_counseling.id = fa_family_members.counseling_id")
						   ->where($where)  
						   ->get()
						   ->result();		
		return $result;
	} 
	
	public function getFamilyMember_H($id)
	{
		$where = array(
				"fa_rsd_applications.id"=>$id, 
				'fa_family_members.non_accompanying' => 1,
				'fa_family_members.outside_country' => 1
		); 
		$result = $this->db->select("fa_family_members.*")
						   ->from("fa_rsd_applications")
						   ->join("fa_counseling","fa_rsd_applications.counseling_id = fa_counseling.id")
						   ->join("fa_family_members"," fa_counseling.id = fa_family_members.counseling_id")						   
						   ->where($where)  
						   ->get()
						   ->result();		
		return $result;
	} 
	  
	public function getHomeVisitById($id=false)
	{ 
		$result = $this->db->where("id",$id)->get("fa_refugee_home_visit")->row();
		return $result;
	} 
	
	public function getTravelDocumentId($id=false)
	{ 
		$result = $this->db->where("id",$id)->get("fa_refugee_travel_document_request")->row();
		return $result;
	} 
	
	public function getRequestById($id=false)
	{ 
		$result = $this->db->where("id",$id)->get("fa_refugee_card_request")->row();
		return $result;
	} 
	
	public function addRefugeeCardRequest($data = false)
	{
		if($this->db->insert("fa_refugee_card_request", $data)){
			return true;
		}
		return false;
	}
	
	public function getAllRequestCards()
	{
		$result = $this->db->get("fa_refugee_card_request")->result();
		return $result;	
	}
	
	public function updateRefugeeCardRequest($id = false, $data = false)
	{
		if($this->db->where('id',$id)->update("fa_refugee_card_request", $data)){
			return true;
		}
		return false;
	}
	
	public function deleteRefugeeCardRequest($id = false)
	{
		if($this->db->where('id',$id)->update("fa_refugee_card_request",array("status" => "deleted"))){
			return true;
		}
		return false;
	}
	
	public function getPermanentById($id=false)
	{ 
		$result = $this->db->where("id", $id)->get("fa_refugee_permanent_card_request")->row();
		return $result;
	} 
	
	public function addRefugeePermanent($data = false)
	{ 
		if($this->db->insert("fa_refugee_permanent_card_request", $data)){
			return true;
		}
		return false;
	} 
	
	public function updateRefugeePermanent($id=false, $data = false)
	{ 
		if($this->db->where("id",$id)->update("fa_refugee_permanent_card_request", $data)){
			return true;
		}
		return false;
	} 
	
	public function deleteRefugeePermanent($id=false)
	{ 
		if($this->db->where("id",$id)->update("fa_refugee_permanent_card_request", array("status" => "deleted"))){
			return true;
		}
		return false;
	} 

	public function getCessationEliminatinRefugeeById($id=false)
	{ 
		$result = $this->db->where("id",$id)->get("fa_refugee_cessation_elimination_request")->row();
		return $result;
	} 
	
	public function addCessationEliminatinRefugee($data = false, $members = false)
	{
		if($this->db->insert("fa_refugee_cessation_elimination_request",$data)){
			$withdrawal_id = $this->db->insert_id();
			foreach($members as $member){	
				$member["application_id"] = $data['application_id'];
				$member["cessation_id"] = $withdrawal_id;				
				$this->db->insert("fa_refugee_cessation_elimination_members",$member);
			}
			return true;
		}
		return false;
	}
	
	public function deleteCessationEliminatinRefugee($id = false)
	{
		if($this->db->where("id",$id)->update("fa_refugee_cessation_elimination_request", array("status" => "deleted"))){
			//$this->db->where("cessation_id",$id)->delete("fa_refugee_cessation_elimination_members");
			return true;
		}
		return false;
	}
	
	public function updateCessationEliminatinRefugee($id = false, $data = false)
	{
		if($this->db->where("id",$id)->update("fa_refugee_cessation_elimination_request",$data)){
			return true;
		}
		return false;
	}

 	public function getRefugeeAddressChangeById($id=false)
	{ 
		$result = $this->db->where("id",$id)->get("fa_refugee_change_address_request")->row();
		return $result;
	} 
	
	public function updateRefugeeAddressChange($id = false, $data = false)
	{
		if($this->db->where('id',$id)->update("fa_refugee_change_address_request",$data)){
			return true;
		}	
		return false;
	}
	
	public function addRefugeeAddressChange($data = false)
	{
		if($this->db->insert("fa_refugee_change_address_request",$data)){
			return true;
		}	
		return false;
	}
	
	public function deleteRefugeeAddressChange($id = false)
	{
		if($this->db->where("id",$id)->delete("fa_refugee_change_address_request")){
			return true;
		}
		return false;
	}
	
 	public function getRefugeeIntergrationById($id=false)
	{ 
		$result = $this->db->where("id",$id)->get("fa_refugee_intergration_request")->row();
		return $result;
	}  
	
	public function addRefugeeIntergration($data = false)
	{
		if($this->db->insert("fa_refugee_intergration_request",$data)){
			return true;
		}
		return false;
	}
	
	public function updateRefugeeIntergration($id = false, $data = false)
	{
		if($this->db->where("id",$id)->update("fa_refugee_intergration_request",$data)){
			return true;
		}
		return false;
	}
	
	public function deleteRefugeeIntergration($id = false)
	{
		if($this->db->where("id",$id)->delete("fa_refugee_intergration_request")){
			return true;
		}
		return false;
	}
	
 	public function getFamilyMembersDependantAccompanyingById($id=false)
	{ 
		$result = $this->db->where("id",$id)->get("fa_family_members")->row();
		return $result;
	}  
	
	public function getApplicationById($id) 
	{
		return $this->db->where("id", $id)->get("fa_rsd_applications")->row();
	}	 		
 	
	public function getWithdrawalById($id=false)
	{ 
		$result = $this->db->where("id",$id)->get("fa_refugee_withdrawal_request")->row();
		return $result;
	} 
	
	public function addWithdrawalRefugee($data = false, $members = false)
	{
		if($this->db->insert("fa_refugee_withdrawal_request",$data)){
			$withdrawal_id = $this->db->insert_id();
			foreach($members as $member){	
				$member["application_id"] = $data['application_id'];
				$member["withdrawal_id"] = $withdrawal_id;				
				$this->db->insert("fa_refugee_withdrawal_members",$member);
			}
			return true;
		}
		return false;
	}
	
	public function updateWithdrawalRefugee($id = false, $data = false)
	{
		if($this->db->where("id",$id)->update("fa_refugee_withdrawal_request",$data)){			
			return true;
		}
		return false;
	}
	
	public function deleteWithdrawalRefugee($id = false)
	{
		if($this->db->where("id",$id)->update("fa_refugee_withdrawal_request", array("status" => "deleted"))){
			//$this->db->where("withdrawal_id",$id)->delete("fa_refugee_withdrawal_members");
			return true;
		}
		return false;
	}
	
	public function getEducationByCounseling($id=false)
	{ 
		$result = $this->db->where("counseling_id",$id)->get("fa_rsd_application_educations")->result();
		return $result;
	} 
	
	public function getOccupationByCounseling($id=false)
	{ 
		$result = $this->db->where("counseling_id",$id)->get("fa_rsd_application_occupations")->result();
		return $result;
	} 
	
	public function getEducationById($id=false)
	{ 
		$result = $this->db->where("id",$id)->get("fa_rsd_application_educations")->row();
		return $result;
	}  
	
	public function getOccupationById($id=false)
	{ 
		$result = $this->db->where("id",$id)->get("fa_rsd_application_occupations")->row();
		return $result;
	}  
	
	public function getIdentificationDocumentByCounseling($id=false)
	{ 
		$result = $this->db->where("counseling_id",$id)->get("fa_rsd_application_documents")->result();
		return $result;
	}  
	
	public function getFamilyMemberAccompanyingByCounselingIn($id=false, $member_id = FALSE)
	{  				
		$this->db->where("counseling_id",$id);
		$this->db->where("non_accompanying",0);
		$this->db->where_in("id",json_decode($member_id));
		$result = $this->db->get("fa_family_members")->result();
		return $result;
	}
	
	public function getFamilyMemberAccompanyingByCounseling($id=false)
	{  
		$result = $this->db->select("fa_family_members.*,fa_family_relationship.relationship_kh as relationship")
						->where(array("counseling_id"=>$id,"non_accompanying"=>0))
						->join("fa_family_relationship","fa_family_members.relationship=fa_family_relationship.id","left")
						->get("fa_family_members")
						->result();
		return $result;
	}

	public function getFamilyMemberAll($id=false)
	{  
		$result = $this->db->select("fa_family_members.*,fa_family_relationship.relationship_kh as relationship")
						->where(array("counseling_id"=>$id))
						->join("fa_family_relationship","fa_family_members.relationship=fa_family_relationship.id","left")
						->get("fa_family_members")
						->result();
		return $result;
	}
	
	public function getOwnerLastData($id=false){
		$result = $this->db->select('
							fa_refugee_permanent_card_request.occupation,

							fa_refugee_permanent_card_request.occupation_kh,

							IF
								( erp_fa_refugee_permanent_card_request.id > 0, erp_fa_refugee_permanent_card_request.country, erp_fa_rsd_applications.country ) AS country,
							IF
								( erp_fa_refugee_permanent_card_request.id > 0, erp_fa_refugee_permanent_card_request.province, erp_fa_rsd_applications.province ) AS province,
							IF
								( erp_fa_refugee_permanent_card_request.id > 0, erp_fa_refugee_permanent_card_request.district, erp_fa_rsd_applications.district ) AS district,
							IF
								( erp_fa_refugee_permanent_card_request.id > 0, erp_fa_refugee_permanent_card_request.commune, erp_fa_rsd_applications.commune ) AS commune,	
							IF
								( erp_fa_refugee_permanent_card_request.id > 0, erp_fa_refugee_permanent_card_request.village, erp_fa_rsd_applications.village ) AS village,
							IF
								( erp_fa_refugee_permanent_card_request.id > 0, erp_fa_refugee_permanent_card_request.address_kh, erp_fa_rsd_applications.address_kh ) AS address_kh,
							IF
								( erp_fa_refugee_permanent_card_request.id > 0, erp_fa_refugee_permanent_card_request.address, erp_fa_rsd_applications.address ) AS address,
							IF
								( erp_fa_refugee_permanent_card_request.id > 0, erp_fa_refugee_permanent_card_request.contact_number, erp_fa_rsd_applications.phone ) AS phone,
							IF
								( erp_fa_refugee_permanent_card_request.id > 0, erp_fa_refugee_permanent_card_request.photo, erp_fa_rsd_applications.photo ) AS photo,
							IF
								( erp_fa_refugee_permanent_card_request.id > 0, erp_fa_refugee_permanent_card_request.created_date, erp_fa_rsd_applications.created_date ) AS created_date
						')
						->from('fa_rsd_applications')
						->join('fa_refugee_permanent_card_request','fa_refugee_permanent_card_request.application_id=fa_rsd_applications.id AND fa_refugee_permanent_card_request.member_id = 0','left')
						->where('fa_rsd_applications.id',$id)
						->order_by('created_date','desc')
						->limit(1)
						->get()
						->row();
		return $result;
	}

	public function getMemberLastData($id=false){
		$result = $this->db->select('
							IF
								( erp_fa_refugee_permanent_card_request.id > 0, erp_fa_refugee_permanent_card_request.occupation, erp_fa_family_members.occupation ) AS occupation,
							IF
								( erp_fa_refugee_permanent_card_request.id > 0, erp_fa_refugee_permanent_card_request.occupation_kh, erp_fa_family_members.occupation_kh ) AS occupation_kh,
							
							fa_refugee_permanent_card_request.created_date,
							IF
								( erp_fa_refugee_permanent_card_request.id > 0, erp_fa_refugee_permanent_card_request.country, erp_fa_family_members.country ) AS country,
							IF
								( erp_fa_refugee_permanent_card_request.id > 0, erp_fa_refugee_permanent_card_request.province, erp_fa_family_members.province ) AS province,
							IF
								( erp_fa_refugee_permanent_card_request.id > 0, erp_fa_refugee_permanent_card_request.district, erp_fa_family_members.district ) AS district,
							IF
								( erp_fa_refugee_permanent_card_request.id > 0, erp_fa_refugee_permanent_card_request.commune, erp_fa_family_members.commune ) AS commune,	
							IF
								( erp_fa_refugee_permanent_card_request.id > 0, erp_fa_refugee_permanent_card_request.village, erp_fa_family_members.village ) AS village,
							IF
								( erp_fa_refugee_permanent_card_request.id > 0, erp_fa_refugee_permanent_card_request.address_kh, erp_fa_family_members.address_kh ) AS address_kh,
							IF
								( erp_fa_refugee_permanent_card_request.id > 0, erp_fa_refugee_permanent_card_request.address, erp_fa_family_members.address ) AS address,
							fa_refugee_permanent_card_request.contact_number as phone,
							IF
								( erp_fa_refugee_permanent_card_request.id > 0, erp_fa_refugee_permanent_card_request.photo, erp_fa_family_members.photo ) AS photo,
						')
						->from('fa_family_members')
						->join('fa_refugee_permanent_card_request','fa_refugee_permanent_card_request.member_id=fa_family_members.id','left')
						->where('fa_family_members.id',$id)
						->order_by('created_date','desc')
						->limit(1)
						->get()
						->row();
		return $result;
	}

	public function getOwnerLastDataCardRequest($id=false){
		$result = $this->db->select('
							fa_refugee_card_request.occupation,

							fa_refugee_card_request.occupation_kh,

							IF
								( erp_fa_refugee_card_request.id > 0, erp_fa_refugee_card_request.country, erp_fa_rsd_applications.country ) AS country,
							IF
								( erp_fa_refugee_card_request.id > 0, erp_fa_refugee_card_request.province, erp_fa_rsd_applications.province ) AS province,
							IF
								( erp_fa_refugee_card_request.id > 0, erp_fa_refugee_card_request.district, erp_fa_rsd_applications.district ) AS district,
							IF
								( erp_fa_refugee_card_request.id > 0, erp_fa_refugee_card_request.commune, erp_fa_rsd_applications.commune ) AS commune,	
							IF
								( erp_fa_refugee_card_request.id > 0, erp_fa_refugee_card_request.village, erp_fa_rsd_applications.village ) AS village,
							IF
								( erp_fa_refugee_card_request.id > 0, erp_fa_refugee_card_request.address_kh, erp_fa_rsd_applications.address_kh ) AS address_kh,
							IF
								( erp_fa_refugee_card_request.id > 0, erp_fa_refugee_card_request.address, erp_fa_rsd_applications.address ) AS address,
							IF
								( erp_fa_refugee_card_request.id > 0, erp_fa_refugee_card_request.contact_number, erp_fa_rsd_applications.phone ) AS phone,
							IF
								( erp_fa_refugee_card_request.id > 0, erp_fa_refugee_card_request.photo, erp_fa_rsd_applications.photo ) AS photo,
							IF
								( erp_fa_refugee_card_request.id > 0, erp_fa_refugee_card_request.created_date, erp_fa_rsd_applications.created_date ) AS created_date
						')
						->from('fa_rsd_applications')
						->join('fa_refugee_card_request','fa_refugee_card_request.application_id=fa_rsd_applications.id AND fa_refugee_card_request.member_id = 0','left')
						->where('fa_rsd_applications.id',$id)
						->order_by('created_date','desc')
						->limit(1)
						->get()
						->row();
		return $result;
	}
	
	public function getMemberLastDataCardRequest($id=false){
		$result = $this->db->select('
							IF
								( erp_fa_refugee_card_request.id > 0, erp_fa_refugee_card_request.occupation, erp_fa_family_members.occupation ) AS occupation,
							IF
								( erp_fa_refugee_card_request.id > 0, erp_fa_refugee_card_request.occupation_kh, erp_fa_family_members.occupation_kh ) AS occupation_kh,
							
							fa_refugee_card_request.created_date,
							IF
								( erp_fa_refugee_card_request.id > 0, erp_fa_refugee_card_request.country, erp_fa_family_members.country ) AS country,
							IF
								( erp_fa_refugee_card_request.id > 0, erp_fa_refugee_card_request.province, erp_fa_family_members.province ) AS province,
							IF
								( erp_fa_refugee_card_request.id > 0, erp_fa_refugee_card_request.district, erp_fa_family_members.district ) AS district,
							IF
								( erp_fa_refugee_card_request.id > 0, erp_fa_refugee_card_request.commune, erp_fa_family_members.commune ) AS commune,	
							IF
								( erp_fa_refugee_card_request.id > 0, erp_fa_refugee_card_request.village, erp_fa_family_members.village ) AS village,
							IF
								( erp_fa_refugee_card_request.id > 0, erp_fa_refugee_card_request.address_kh, erp_fa_family_members.address_kh ) AS address_kh,
							IF
								( erp_fa_refugee_card_request.id > 0, erp_fa_refugee_card_request.address, erp_fa_family_members.address ) AS address,
							fa_refugee_card_request.contact_number as phone,
							IF
								( erp_fa_refugee_card_request.id > 0, erp_fa_refugee_card_request.photo, erp_fa_family_members.photo ) AS photo,
						')
						->from('fa_family_members')
						->join('erp_fa_refugee_card_request','erp_fa_refugee_card_request.member_id=fa_family_members.id','left')
						->where('fa_family_members.id',$id)
						->order_by('created_date','desc')
						->limit(1)
						->get()
						->row();
		return $result;
	}

	public function getOwnerLastDataTravelDocumentRequest($id=false){

		$left_join='LEFT JOIN `erp_fa_refugee_travel_document_request` ON `erp_fa_refugee_travel_document_request`.`application_id` = `erp_fa_rsd_applications`.`id` AND `erp_fa_refugee_travel_document_request`.`member_id` = 0';

		$result = $this->db->select('
							fa_refugee_travel_document_request.occupation,

							fa_refugee_travel_document_request.occupation_kh,

							IF
								( erp_fa_refugee_travel_document_request.id > 0, erp_fa_refugee_travel_document_request.country, erp_fa_rsd_applications.country ) AS country,
							IF
								( erp_fa_refugee_travel_document_request.id > 0, erp_fa_refugee_travel_document_request.province, erp_fa_rsd_applications.province ) AS province,
							IF
								( erp_fa_refugee_travel_document_request.id > 0, erp_fa_refugee_travel_document_request.district, erp_fa_rsd_applications.district ) AS district,
							IF
								( erp_fa_refugee_travel_document_request.id > 0, erp_fa_refugee_travel_document_request.commune, erp_fa_rsd_applications.commune ) AS commune,	
							IF
								( erp_fa_refugee_travel_document_request.id > 0, erp_fa_refugee_travel_document_request.village, erp_fa_rsd_applications.village ) AS village,
							IF
								( erp_fa_refugee_travel_document_request.id > 0, erp_fa_refugee_travel_document_request.address_kh, erp_fa_rsd_applications.address_kh ) AS address_kh,
							IF
								( erp_fa_refugee_travel_document_request.id > 0, erp_fa_refugee_travel_document_request.address, erp_fa_rsd_applications.address ) AS address,
							IF
								( erp_fa_refugee_travel_document_request.id > 0, erp_fa_refugee_travel_document_request.phone, erp_fa_rsd_applications.phone ) AS phone,

							fa_refugee_travel_document_request.emergencies_contact,
							fa_refugee_travel_document_request.emergencies_address,
							fa_refugee_travel_document_request.height,
							fa_refugee_travel_document_request.complexion,
							fa_refugee_travel_document_request.shape_of_face,
							fa_refugee_travel_document_request.color_of_eyes,
							fa_refugee_travel_document_request.distinguishing_marks,
							fa_refugee_travel_document_request.hair,
							fa_refugee_travel_document_request.to_country_kh,
							fa_refugee_travel_document_request.to_country,
							fa_refugee_travel_document_request.travel_for_kh,
							fa_refugee_travel_document_request.travel_for,

							IF
								( erp_fa_refugee_travel_document_request.id > 0, erp_fa_refugee_travel_document_request.photo, erp_fa_rsd_applications.photo ) AS photo,
							IF
								( erp_fa_refugee_travel_document_request.id > 0, erp_fa_refugee_travel_document_request.created_date, erp_fa_rsd_applications.created_date ) AS created_date
						')
						->from('fa_rsd_applications')
						->join('fa_refugee_travel_document_request','fa_refugee_travel_document_request.application_id=fa_rsd_applications.id AND fa_refugee_travel_document_request.member_id = 0','left')
						->where('fa_rsd_applications.id',$id)
						->order_by('created_date','desc')
						->limit(1)
						->get()
						->row();
		return $result;
	}

	public function getMemberLastDataTravelDocumentRequest($id=false){
		$result = $this->db->select('
							IF
								( erp_fa_refugee_travel_document_request.id > 0, erp_fa_refugee_travel_document_request.occupation, erp_fa_family_members.occupation ) AS occupation,
							IF
								( erp_fa_refugee_travel_document_request.id > 0, erp_fa_refugee_travel_document_request.occupation_kh, erp_fa_family_members.occupation_kh ) AS occupation_kh,
							
							fa_refugee_travel_document_request.created_date,
							IF
								( erp_fa_refugee_travel_document_request.id > 0, erp_fa_refugee_travel_document_request.country, erp_fa_family_members.country ) AS country,
							IF
								( erp_fa_refugee_travel_document_request.id > 0, erp_fa_refugee_travel_document_request.province, erp_fa_family_members.province ) AS province,
							IF
								( erp_fa_refugee_travel_document_request.id > 0, erp_fa_refugee_travel_document_request.district, erp_fa_family_members.district ) AS district,
							IF
								( erp_fa_refugee_travel_document_request.id > 0, erp_fa_refugee_travel_document_request.commune, erp_fa_family_members.commune ) AS commune,	
							IF
								( erp_fa_refugee_travel_document_request.id > 0, erp_fa_refugee_travel_document_request.village, erp_fa_family_members.village ) AS village,
							IF
								( erp_fa_refugee_travel_document_request.id > 0, erp_fa_refugee_travel_document_request.address_kh, erp_fa_family_members.address_kh ) AS address_kh,
							IF
								( erp_fa_refugee_travel_document_request.id > 0, erp_fa_refugee_travel_document_request.address, erp_fa_family_members.address ) AS address,
							fa_refugee_travel_document_request.phone as phone,

							fa_refugee_travel_document_request.emergencies_contact,
							fa_refugee_travel_document_request.emergencies_address,
							fa_refugee_travel_document_request.height,
							fa_refugee_travel_document_request.complexion,
							fa_refugee_travel_document_request.shape_of_face,
							fa_refugee_travel_document_request.color_of_eyes,
							fa_refugee_travel_document_request.distinguishing_marks,
							fa_refugee_travel_document_request.hair,
							fa_refugee_travel_document_request.to_country_kh,
							fa_refugee_travel_document_request.to_country,
							fa_refugee_travel_document_request.travel_for_kh,
							fa_refugee_travel_document_request.travel_for,

							IF
								( erp_fa_refugee_travel_document_request.id > 0, erp_fa_refugee_travel_document_request.photo, erp_fa_family_members.photo ) AS photo,
						')
						->from('fa_family_members')
						->join('erp_fa_refugee_travel_document_request','erp_fa_refugee_travel_document_request.member_id=fa_family_members.id','left')
						->where('fa_family_members.id',$id)
						->order_by('created_date','desc')
						->limit(1)
						->get()
						->row();
		return $result;
	}

	public function getOwnerLastDataFamilyGuranteeRequest($id=false){
		$result = $this->db->select('

							fa_rsd_applications.firstname_kh,

							fa_rsd_applications.lastname_kh,

							fa_rsd_applications.dob,

							fa_rsd_applications.gender,

							fa_refugee_family_guarantee_request.occupation,

							fa_refugee_family_guarantee_request.occupation_kh,

							IF
								( erp_fa_refugee_family_guarantee_request.id > 0, erp_fa_refugee_family_guarantee_request.country, erp_fa_rsd_applications.country ) AS country,
							IF
								( erp_fa_refugee_family_guarantee_request.id > 0, erp_fa_refugee_family_guarantee_request.province, erp_fa_rsd_applications.province ) AS province,
							IF
								( erp_fa_refugee_family_guarantee_request.id > 0, erp_fa_refugee_family_guarantee_request.district, erp_fa_rsd_applications.district ) AS district,
							IF
								( erp_fa_refugee_family_guarantee_request.id > 0, erp_fa_refugee_family_guarantee_request.commune, erp_fa_rsd_applications.commune ) AS commune,	
							IF
								( erp_fa_refugee_family_guarantee_request.id > 0, erp_fa_refugee_family_guarantee_request.village, erp_fa_rsd_applications.village ) AS village,
							IF
								( erp_fa_refugee_family_guarantee_request.id > 0, erp_fa_refugee_family_guarantee_request.address_kh, erp_fa_rsd_applications.address_kh ) AS address_kh,
							IF
								( erp_fa_refugee_family_guarantee_request.id > 0, erp_fa_refugee_family_guarantee_request.address, erp_fa_rsd_applications.address ) AS address,
							IF
								( erp_fa_refugee_family_guarantee_request.id > 0, erp_fa_refugee_family_guarantee_request.contact_number, erp_fa_rsd_applications.phone ) AS phone,
							IF
								( erp_fa_refugee_family_guarantee_request.id > 0, erp_fa_refugee_family_guarantee_request.img, erp_fa_rsd_applications.photo ) AS photo,
							IF
								( erp_fa_refugee_family_guarantee_request.id > 0, erp_fa_refugee_family_guarantee_request.created_date, erp_fa_rsd_applications.created_date ) AS created_date
						')
						->from('fa_rsd_applications')
						->join('fa_refugee_family_guarantee_request','fa_refugee_family_guarantee_request.application_id=fa_rsd_applications.id AND fa_refugee_family_guarantee_request.member_id = 0','left')
						->where('fa_rsd_applications.id',$id)
						->order_by('created_date','desc')
						->limit(1)
						->get()
						->row();
		return $result;
	}

    public function getMemberLastDataFamilyGuranteeRequest($id=false){
        $result = $this->db->select('

							erp_fa_family_members.firstname_kh,

							erp_fa_family_members.lastname_kh,

							erp_fa_family_members.dob,

							erp_fa_family_members.gender,

							fa_refugee_family_guarantee_request.occupation,

							fa_refugee_family_guarantee_request.occupation_kh,

							IF
								( erp_fa_refugee_family_guarantee_request.id > 0, erp_fa_refugee_family_guarantee_request.country, erp_fa_family_members.country ) AS country,
							IF
								( erp_fa_refugee_family_guarantee_request.id > 0, erp_fa_refugee_family_guarantee_request.province, erp_fa_family_members.province ) AS province,
							IF
								( erp_fa_refugee_family_guarantee_request.id > 0, erp_fa_refugee_family_guarantee_request.district, erp_fa_family_members.district ) AS district,
							IF
								( erp_fa_refugee_family_guarantee_request.id > 0, erp_fa_refugee_family_guarantee_request.commune, erp_fa_family_members.commune ) AS commune,	
							IF
								( erp_fa_refugee_family_guarantee_request.id > 0, erp_fa_refugee_family_guarantee_request.village, erp_fa_family_members.village ) AS village,
							IF
								( erp_fa_refugee_family_guarantee_request.id > 0, erp_fa_refugee_family_guarantee_request.address_kh, erp_fa_family_members.address_kh ) AS address_kh,
							IF
								( erp_fa_refugee_family_guarantee_request.id > 0, erp_fa_refugee_family_guarantee_request.address, erp_fa_family_members.address ) AS address,
					
						')
            ->from('erp_fa_family_members')
            ->join('fa_refugee_family_guarantee_request','fa_refugee_family_guarantee_request.member_id=erp_fa_family_members.id AND fa_refugee_family_guarantee_request.application_id = 0','left')
            ->where('erp_fa_family_members.id',$id)
            ->order_by('dob','desc')
            ->limit(1)
            ->get()
            ->row();
        return $result;
    }

	public function getMemberLastDataCessationEliminationRefugee($id=false){
		$result = $this->db->select('

							fa_family_members.firstname_kh,

							fa_family_members.lastname_kh,

							erp_fa_family_members.dob,
							
							erp_fa_family_members.gender,

							IF
								( erp_fa_refugee_cessation_elimination_request.id > 0, erp_fa_refugee_cessation_elimination_request.occupation, erp_fa_family_members.occupation ) AS occupation,
						
							fa_refugee_cessation_elimination_request.created_date,
							IF
								( erp_fa_refugee_cessation_elimination_request.id > 0, erp_fa_refugee_cessation_elimination_request.country, erp_fa_family_members.country ) AS country,
							IF
								( erp_fa_refugee_cessation_elimination_request.id > 0, erp_fa_refugee_cessation_elimination_request.province, erp_fa_family_members.province ) AS province,
							IF
								( erp_fa_refugee_cessation_elimination_request.id > 0, erp_fa_refugee_cessation_elimination_request.district, erp_fa_family_members.district ) AS district,
							IF
								( erp_fa_refugee_cessation_elimination_request.id > 0, erp_fa_refugee_cessation_elimination_request.commune, erp_fa_family_members.commune ) AS commune,	
							IF
								( erp_fa_refugee_cessation_elimination_request.id > 0, erp_fa_refugee_cessation_elimination_request.village, erp_fa_family_members.village ) AS village,
							IF
								( erp_fa_refugee_cessation_elimination_request.id > 0, erp_fa_refugee_cessation_elimination_request.address_kh, erp_fa_family_members.address_kh ) AS address_kh,
							IF
								( erp_fa_refugee_cessation_elimination_request.id > 0, erp_fa_refugee_cessation_elimination_request.address, erp_fa_family_members.address ) AS address,
							erp_fa_refugee_cessation_elimination_request.contact_number as phone,
							IF
								( erp_fa_refugee_cessation_elimination_request.id > 0, erp_fa_refugee_cessation_elimination_request.img, erp_fa_family_members.photo ) AS photo,
						')
						->from('fa_family_members')
						->join('fa_refugee_cessation_elimination_request','fa_refugee_cessation_elimination_request.member_id=fa_family_members.id','left')
						->where('fa_family_members.id',$id)
						->order_by('created_date','desc')
						->limit(1)
						->get()
						->row();
		return $result;
	}	









	public function getFamilyMemberNoneAccompanyingByCounseling($id=false)
	{  
		$result = $this->db->select("fa_family_members.*,fa_family_relationship.relationship_kh as relationship_khm")
						->where(array("counseling_id"=>$id,"non_accompanying"=>1,"outside_country"=>0))
						->join("fa_family_relationship","fa_family_members.relationship=fa_family_relationship.id","left")
						->get("fa_family_members")
						->result();
		return $result;
	
	}  

	public function getFamilyMembersDependantNoneAccompanyingById($id=false)
	{   
		$result = $this->db->where(array("id"=>$id,"non_accompanying"=>1))->get("erp_fa_family_members")->row();
		return $result;
	} 

	public function getFamilyMemberNoneAccompanyingOutsideCountryByCounseling($id=false)
	{ 
		$where = array("counseling_id"=>$id,
						"non_accompanying"=>1,
						"outside_country"=>1
						);
						 
		$result = $this->db->select("fa_family_members.*,fa_family_relationship.relationship_kh as relationship")
						->where($where )
						->join("fa_family_relationship","fa_family_members.relationship=fa_family_relationship.id","left")
						->get("fa_family_members")
						->result();
		return $result;
	} 
	
	public function getIdentificationDocumentById($id=false)
	{ 
		$result = $this->db->where("id",$id)->get("fa_rsd_application_documents")->row();
		return $result;
	}   
	
	public function getAllRSDApplications()
	{
		$result = $this->db->where("status <>","deleted")->get("fa_rsd_applications")->result();
		return $result;
	}
	
	public function addRSDApplication($data = false)
	{
		if($this->db->insert("fa_rsd_applications", $data)){ 
			return $this->db->insert_id();
		}
		return false;
	}
	
	public function deleteRSDApplication($id = false)
	{
		if($this->db->where("id",$id)->delete("fa_rsd_applications")){
			return true;
		}
		return false;
	}
    public function deleteRSDApplications($id=NULL)
    {
        $result = $this->db->where("id",$id)->update("fa_rsd_applications", array("status"=>"deleted"));
        return $result;
    }

    public function updateRSDApplication($id = false, $data = false)
	{
		if($this->db->where("id",$id)->update("fa_rsd_applications", $data)){
			return true;
		}
		return false;
	}
	
	public function getFamilyMemberNoneAccompanyingOutsideCountryById($id=false)
	{  
		$result = $this->db->where("id",$id)->get("fa_family_members")->row();
		return $result;
		
	} 
	
	public function getCountryTransitByCounseling($id=false)
	{  
		$result = $this->db->where("counseling_id",$id)->get("erp_fa_rsd_application_transition")->result();
		return $result;
	}  
			   
	public function getCountryTransitById($id=false)
	{  
		$result = $this->db->where("id",$id)->get("erp_fa_rsd_application_transition")->row();
		return $result;
	}  
	
	public function getallOffices()
	{
		$result = $this->db->get("fa_offices")->result();
		return $result;
	}
	
	public function getallOfficeById($id = NULL)
	{
		$result = $this->db->where("id",$id)->get("fa_offices")->row();
		return $result;
	}
	
	public function getDataRefugeeUpdateById($id=false)
	{
		$result = $this->db->where("id",$id)->get("fa_data_refugee_update")->row();
		return $result;
	}
	
	public function getCasePrefix() 
	{
		$result = $this->db->select('DISTINCT(case_prefix)')
						->from('fa_case_prefix')
						->where('status','active')
						->get()
						->result();  
		return $result;
	} 
	
	public function updateCaseNo($c=null) 
	{
		$result = $this->db->where("case_prefix",$c)->get("fa_case_prefix")->result();
		return $result;
	}

	public function getRelationship() 
	{
		$result = $this->db->where('status','active')->get('fa_family_relationship')->result();  
		return $result;
	}
	
	public function getRelationshipById($id = false) 
	{
		$result = $this->db->where('id',$id)->get('fa_family_relationship')->row();  
		return $result;
	}
 
	public function getInterviewRecords($id = false, $member_id = false)
	{
		if($member_id > -1){
			$this->db->where("interview_member_id",$member_id);
		}
		$result = $this->db->where("interview_id",$id)->get("fa_interview_records")->result();
		return $result;
	} 
	
	public function addInterviewRecord($id =false , $data = false)
	{
		if($this->db->where("interview_id",$id)->delete("fa_interview_records")){
			$this->db->insert_batch("fa_interview_records",$data);
			return true;
		}
		return false;
	}
	
	public function deleteInterviewRecord($id = false)
	{
		if($this->db->where("id",$id)->delete("fa_interview_records")){
			return true;
		}
		return false;
	}	
	
	public function getDepartmentDirector() 
	{
		$result = $this->db->select('*')
						->from('fa_signature')
						->where('status','active')
						->get()
						->result();  
		return $result;
	}
		
	public function getAllProvinces() 
	{
		$result = $this->db->select("id,native_name AS name")->from("fa_provinces")->get()->result();
		//$result = $this->db->get("fa_provinces")->result();
		return $result;
	}
	
	public function getAllCountries()
	{
		$result =  $this->db->select("id,native_name AS country")->from("fa_countries")->get()->result();
		//$result = $this->db->get("fa_countries")->result();
		return $result;
	}
	
	public function getAllCommunces()
	{
		$result = $this->db->select("id,native_name AS name")->from("fa_communces")->get()->result();
	//	$result = $this->db->get("fa_communces")->result();
		return $result;
	}
	
	public function getAllDistricts()
	{
		$result = $this->db->select("id,native_name AS name")->from("fa_districts")->get()->result();
		//$result = $this->db->get("fa_districts")->result();
		return $result;
	}
	
	public function getAllStates()
	{
		$result = $this->db->select("id,native_name AS name")->from("fa_states")->get()->result();
///	$result = $this->db->get("fa_states")->result();
		return $result;
	}
	
	public function getProvinceById($id = NULL)
	{
		$result = $this->db->where("id",$id)->get("fa_provinces")->row();
		return $result;
	}
	
	public function getCommunceById($id = NULL)
	{
		$result = $this->db->where("id",$id)->get("fa_communces")->row();
		return $result;
	}
	
	public function getCountryById($id = NULL)
	{
		$result = $this->db->where("id",$id)->get("fa_countries")->row();
		return $result;
	}
	
	public function getDistrictById($id = NULL)
	{
		$result = $this->db->where("id",$id)->get("fa_districts")->row();
		return $result;
	}
	
	public function getStateById($id = NULL)
	{
		$result = $this->db->where("id",$id)->get("fa_states")->row();
		return $result;
	}

	public function getAllLanguages()
	{
		$result = $this->db->get("fa_languages")->result();
		return $result;
	}
	
	public function getAppointmentByIdLists($Ids = NULL)
	{
		$result = $this->db->where_in("id",$Ids)->get("fa_family_members")->result();
		return $result;
	}
		
	public function addTags($name = NULL, $type = NULL){
		
		if($this->getTagsByRows($name) <= 0){
			if($this->db->insert("fa_tags", array('name'=>trim($name), 'type'=> $type))){
				$insert_id = $this->db->insert_id();
				return $insert_id;
			}
		}else if($this->getTagsByRows($name) > 0){
			$result = $this->getTagsByName($name);
			return $result->id;
		}
		return false;
	}

	public function addTagsGurantee($name = NULL, $name_kh = NULL, $type = NULL){
		
		if($this->getTagsByRows($name) <= 0){
			if($this->db->insert("fa_tags", array('name'=>trim($name), 'native_name'=>trim($name_kh), 'type'=> $type))){
				$insert_id = $this->db->insert_id();
				return $insert_id;
			}
		}else if($this->getTagsByRows($name) > 0){
			$result = $this->getTagsByName($name);
			return $result->id;
		}
		return false;
	}
	
	public function getTagsByRows($name = NULL){ 
	
		$result = $this->db->get_where("fa_tags",array("name"=>$name))->num_rows();
		return $result;
	}
	
	public function getTagsByName($name = NULL){
		$result = $this->db->get_where("fa_tags",array("name"=>$name))->row();
		return $result;
	}
	
	public function getTagsById($id = NULL){
		$result = $this->db->get_where("fa_tags",array("id"=>$id))->row();
		return $result->name;
	}
	
	public function getTagsKhmerById($id = NULL){
		$result = $this->db->get_where("fa_tags",array("id"=>$id))->row();
		return $result->native_name;
	}

	public function getAllTagsByType($name = NULL, $type = NULL){
		$this->db->like("name",$name);
		$this->db->where("type",$type);
		$result = $this->db->get("fa_tags")->result();
		return $result;
	}
	
	public function getAllAddressKH($id = false,$type = false){
		if($type=="country"){
			$country = $this->getCountryById($id);
			return  "".$country->native_name.($country?"  ":"");
			
		}else if($type=="province") {
            $province = $this->getProvinceById($id);
            return "" . $province->native_name . ($province ? " , " : "");

        }else if($type=="province_r"){
            $province = $this->getProvinceById($id);
            return "".$province->native_name.($province? "  " : "");

        }else if($type == "district"){
			$district = $this->getDistrictById($id);
			return "".$district->native_name.($district?" , ":"");

        }else if($type == "district_r"){
            $district = $this->getDistrictById($id);
            return "".$district->native_name.($district?"  ":"");

		}else if($type == "commune"){
			$commune = $this->getCommunceById($id);
			return "".$commune->native_name.($commune?" , ":"");

        }else if($type == "commune_r"){
            $commune = $this->getCommunceById($id);
            return "".$commune->native_name.($commune?"  ":"");
			
		}else if($type == "state"){
			$state = $this->getStateById($id);
			return "".$state->native_name.($state?"  ":"");
			
		}else{
			$tag = $this->getAllTagsByType($id);
			return  $tag->native_name.($tag?" , ":"");			
		}
		return false;
	} 
	
	public function getAllAddressKHCard($id = false,$type = false){
		if($type=="country"){
			$country = $this->getCountryById($id);
			return $country->native_name.($country?:"");
			
		}else if($type=="province"){
			$province = $this->getProvinceById($id);
			return $province->native_name.($province?"":"");
			
		}else if($type == "district"){
			$district = $this->getDistrictById($id);
			return $district->native_name.($district?"":"");
			
		}else if($type == "commune"){
			$commune = $this->getCommunceById($id);
			return $commune->native_name.($commune?"":"");
			
		}else{
			$tag = $this->getAllTagsByType($id);
			return $tag->native_name.($tag?"":"");			
		}
		return false;
	}
	
	public function getAllAddress($id = false,$type = false){
		
		if($type=="country"){
			$country = $this->getCountryById($id);
			return $country->country.($country?"":"");
			
		}else if($type=="state"){
			$state = $this->getStateById($id);
			return $state->name.($state?"":"");

        }else if($type=="state_r"){
            $state = $this->getStateById($id);
            return $state->name.($state?"  ":"");

		}else if($type=="province"){
			$province = $this->getProvinceById($id);
			return $province->name.($province?" , ":"");

        }else if($type=="province_r"){
            $province = $this->getProvinceById($id);
            return $province->name.($province?"  ":"");

		}else if($type == "district"){
			$district = $this->getDistrictById($id);
			return $district->name.($district?" , ":"");

        }else if($type == "district_r"){
            $district = $this->getDistrictById($id);
            return $district->name.($district?"  ":"");

        }else if($type == "commune"){
			$commune = $this->getCommunceById($id);
			return $commune->name.($commune?" , ":"");

        }else if($type == "commune_r"){
            $commune = $this->getCommunceById($id);
            return $commune->name.($commune?"  ":"");

		}else{
			$tag = $this->getAllTagsByType($id);
			return $tag->name.($tag?" , ":"");					
		}
		return false;
	}
	
	
	/**
	* 
	* @param undefined $data
	* REFUGEE HOME VISIT
	* @return
	*/
		
	public function addRefugeeHomeVisit($data = NULL){
		if($this->db->insert("fa_refugee_home_visit",$data)){
			return true;
		}
		return false;
	}
	
	public function updateRefugeeHomeVisit($id = false, $data = NULL){
		if($this->db->where('id',$id)->update('fa_refugee_home_visit',$data)){
			return true;
		}
		return false;
	}
	
 	public function deleteRefugeeHomeVisit($id = false)
	{
		
		if($this->db->where('id',$id)->update('fa_refugee_home_visit', array("status" => "deleted"))){
			return true;
		}
		return false;
	}
	
	public function getLastRefugeeHomeVisit($id = false){
		if ($id != false){
			$count = $this->db->query("SELECT COUNT(*) AS TheValue FROM ". $this->db->dbprefix ."fa_refugee_home_visit WHERE application_id = '". $id ."' ")->row()->TheValue;
			if ($count > 0){
				return $this->db->where(array("application_id"=>$id,"member_id"=>0))->limit(1)->order_by('day_visit', 'DESC')->get("fa_refugee_home_visit")->row();
			} else {
				return false;
			}
		} else {
			return false;
		}
		
	}
	
	
	/**
	* 
	* @param undefined $data
	* REFUGEE GUARANTEE
	* @return
	*/
	
	public function addRefugeeGuarantee($data = false)
	{
		if($this->db->insert('fa_refugee_family_guarantee_request',$data)){
			return $this->db->insert_id();
		}
		return false;
	}
	
	public function updateRefugeeGuarantee($id = false, $data = NULL)
	{
		if($this->db->where('id',$id)->update('fa_refugee_family_guarantee_request',$data)){
			return true;
		}
		return false;
	}
	
	public function deleteRefugeeGuarantee($id = false)
	{
		
		if($this->db->where('id',$id)->update('fa_refugee_family_guarantee_request', array("status" => "deleted"))){
			return true;
		}
		return false;
	}
	
	public function getAllRefugeeGuaranteeById($id = false)
	{
		$result = $this->db->where('id',$id)->get('fa_refugee_family_guarantee_request')->row();
		return $result;
	}
	
	/**
	* 
	* @param undefined $data
	* MEMBER ASYLUM SEEKER OR REFUGEE
	* @return
	*/
	
	public function addFamilyGuaranteeRefugee($data = false)
	{
		if($this->db->insert('fa_refugee_family_guarantee_members',$data)){
			return true;
		}
		return false;
	}
	
	public function updateFamilyGuaranteeRefugee($id = false, $data = NULL)
	{
		if($this->db->where('id',$id)->update('fa_refugee_family_guarantee_members',$data)){
			return true;
		}
		return false;
	}
	
	public function deleteFamilyGuaranteeRefugee($id)
	{
		if($this->db->where('id',$id)->delete('fa_refugee_family_guarantee_members')){			
			return true;
		}
		return false;
	}
	
	public function getAllFamilyRefugeeByGuaranteeId($id = false)
	{
		$result = $this->db->where("guarantee_id",$id)->get("fa_refugee_family_guarantee_members")->result();
		return $result;
	}
	
	public function getFamilyGuaranteeRefugeeById($id = false)
	{
		$result = $this->db->where("id",$id)->get("fa_refugee_family_guarantee_members")->row();
		return $result;
	}

	
	public function addFamilyHomeVisit($data = false)
	{
		if($this->db->insert('fa_refugee_home_visit_members',$data)){
			return true;
		}
		return false;
	}
	
	public function updateFamilyHomeVisit($id = false, $data = NULL)
	{
		if($this->db->where('id',$id)->update('fa_refugee_home_visit_members',$data)){
			return true;
		}
		return false;
	}
	
	
	public function getFamilyHomeVisitRefugeeById($id = false)
	{
		$result = $this->db->where("id",$id)->get("fa_refugee_home_visit_members")->row();
		return $result;
	}
	
	public function getAllFamilyRefugeeByHomeVisitId($id = false)
	{
		$result = $this->db->where("visit_id",$id)->get("fa_refugee_home_visit_members")->result();
		return $result;
	}
	
	public function deleteFamilyHomeVisitRefugee($id)
	{
		if($this->db->where('id',$id)->delete('fa_refugee_home_visit_members')){			
			return true;
		}
		return false;
	}
	
	public function getAllMemberByCounseling($id = false)
	{
		$result = $this->db->where("counseling_id",$id)->get("fa_family_members")->result();
		return $result;
	}  
	
	public function addExpellationDeclaration($data = false)
	{
		if($this->db->insert("fa_expellation_declaration",$data)){
			return true;
		}
		return false;
	}
	
	public function deleteExpellationDeclaration($id = false, $data = false)
	{
		if($this->db->where("id",$id)->delete("fa_expellation_declaration")){
			return true;
		}
		return false;
	}	
	
	public function getExpellationDeclarationById($id = false)
	{
		$result = $this->db->where("id",$id)->get("fa_expellation_declaration")->row();
		return $result;
	}
	
	/**
	* 
	* @param undefined $data
	* Refugee Travel Document
	* @return
	*/
	
	public function addRefugeeTravelDocument($data = false)
	{		
		if($this->db->insert("fa_refugee_travel_document_request",$data)){
			return true;
		}
		return false;
	}
	
	public function updateRefugeeTravelDocument($id= false, $data = false)
	{
		if($this->db->where("id",$id)->update("fa_refugee_travel_document_request",$data)){
			return true;
		}
		return false;
	}
	
	public function deleteRefugeeTravelDocument($id= false, $data = false)
	{
		if($this->db->where("id",$id)->update("fa_refugee_travel_document_request", array("status" => "deleted"))){
			return true;
		}
		return false;
	}
	
	public function deleteNewMember($id = false) 
	{
		if($this->db->where("id",$id)->delete("fa_family_members")){
			return true;			
		}
		return false;
	}
	
	public function getAllReasons()
	{
		$result = $this->db->get("fa_reasons")->result();
		return $result;
	}
	
	public function getReasonById($id = false)
	{
		$result = $this->db->where("id",$id)->get("fa_reasons")->row();
		return $result;
	}
	
	public function getFamilyMemberById($id = null) 
	{
		$result = $this->db->where("id",$id)->get("fa_family_members")->result();
		return $result;
		
	}
	
	public function addNegative($data = false)
	{
		if($data) {
			$result = $this->db->insert("fa_interview_negatives",$data);
			return $result;
		}
		return false; 
	}
	
	public function addNegativeAppeal($data = false)
	{
		if($data) {
			$result = $this->db->insert("fa_interview_negatives_appeal",$data);
			return $result;
		}
		return false; 
	}
	
	public function updateNegativeAppeal($id = false, $data = false)
	{
		if($data) {
			$result = $this->db->where("id",$id)->update("fa_interview_negatives_appeal",$data);
			return $result;
		}
		return false; 
	}
	
	public function deleteNegative($id = false)
	{
		if($this->db->where("id",$id)->update("fa_interview_negatives",array("status" => "deleted"))){
			return true;
		}
		return false;
	}
	
	public function deleteNegativeAppeal($id = false)
	{
		if($this->db->where("id",$id)->update("fa_interview_negatives_appeal", array("status" => "deleted"))){
			return true;
		}
		return false;
	}
	
	public function updateNegative($id = false, $data = false)
	{
		if($this->db->where("id",$id)->update("fa_interview_negatives", $data)){
			return true;
		}
		return false;
	} 
	
	public function getNegative($id = false) 
	{
		if($id) {
			$result = $this->db->where("id",$id)->get("fa_interview_negatives")->row();
			return $result;
		}
		return false; 
	}
	public function getNegativeAppealById($id = false) 
	{
		if($id) {
			$result = $this->db->where("id",$id)->get("fa_interview_negatives_appeal")->row();
			return $result;
		}
		return false; 
	}
	
	public function getNegativeAppeal($id = false)
	{
		if($id) {
			$result = $this->db->where("id",$id)->get("fa_interview_negatives_appeal")->row();
			return $result;
		}
		return false; 
	}
	
	public function updateHomeVisitStatus($id = false,$status=false) 
	{ 
		if(!empty($status)) {
			$result = $this->db->where("id",$id)->update("fa_refugee_family_guarantee_members",array("is_come"=>0));
		}else {
			$result = $this->db->where("id",$id)->update("fa_refugee_family_guarantee_members",array("is_come"=>1));
		} 
		return $result; 
	}  
	 
	public function changeStatusItem($id = false,$status=false) 
	{ 
		if(!empty($status)) { 
			$result = $this->db->where("id",$id)->update("fa_rsd_application_documents",array("original_provided"=>"no"));
		}else {
			$result = $this->db->where("id",$id)->update("fa_rsd_application_documents",array("original_provided"=>"yes	"));
		} 
		return $result; 
	}  
	//**address**//
	public function getAddressByid($id=null)
	{
		$row = $this->db->get("fa_address_refugee")->row();
		return $row;	 
	}
 
	public function getAppointmentRefugeeById($id = false) 
	{
		$result = $this->db->where("id",$id)->get("fa_interview_appointments")->row();
		return $result;
	}  

	public function getSetting() 
	{
		$result = $this->db->get("settings")->row();
		return $result; 			
	}
	
	/*=============REPORT K1===================*/
	
	public function getRefugeeRecognitionReports($post = false) 
	{	    
		$post = $this->input->post();
		$sql = ""; 
		if($post['from']){
			$sql .= " AND created_date >= '".$this->erp->fld($post['from'])."'";
			$sql .= " AND created_date <= '".$this->erp->fld($post['to'])."'";			
		}		
		if($post['case_no']){
			$sql .= " AND concat(case_prefix,case_no) = '".$post['case_no']."'";
		}
		if($post['q']){
			$sql .= "AND firstname LIKE '%{$post['q']}%' OR lastname LIKE '%{$post['q']}%' OR nationality LIKE '%{$post['q']}%' OR 
					     firstname_kh LIKE '%{$post['q']}%' OR lastname_kh LIKE '%{$post['q']}%' OR nationality_kh LIKE '%{$post['q']}%' 
						  ";
		} 
		$result = $this->db->query("
								SELECT
									 *
								FROM
									(
										SELECT
											i.id,  
											a.case_prefix,
											a.case_no,
											if(i.member_id > 0, f.firstname,a.firstname) as firstname,
											if(i.member_id > 0, f.firstname_kh,a.firstname_kh) as firstname_kh,
											if(i.member_id > 0, f.lastname,a.lastname) as lastname,
											if(i.member_id > 0, f.lastname_kh,a.lastname_kh) as lastname_kh,
											if(i.member_id > 0, f.gender,a.gender) as gender,
											if(i.member_id > 0, f.dob,a.dob) as dob,
											if(i.member_id > 0, f.pob,a.pob) as pob,
											if(i.member_id > 0, f.pob_kh,a.pob_kh) as pob_kh,
											if(i.member_id > 0, f.nationality,a.nationality) as nationality,
											if(i.member_id > 0, f.nationality_kh,a.nationality_kh) as nationality_kh,
											if(i.member_id > 0, f.religion,a.religion) as religion,
											if(i.member_id > 0, f.religion_kh,a.religion_kh) as religion_kh,
											if(i.member_id > 0, f.ethnicity,a.ethnicity) as ethnicity,
											if(i.member_id > 0, f.ethnicity_kh,a.ethnicity_kh) as ethnicity_kh,
											if(i.member_id > 0, f.country,a.country) as country,
											if(i.member_id > 0, f.province,a.province) as province,
											if(i.member_id > 0, f.district,a.district) as district,
											if(i.member_id > 0, f.commune,a.commune) as commune,
											if(i.member_id > 0, f.village,a.village) as village,
											if(i.member_id > 0, f.address,a.address) as address,
											if(i.member_id > 0, f.address_kh,a.address_kh) as address_kh,
											if(i.member_id > 0, f.photo,a.photo) as photo,
											if(i.member_id > 0, f.relationship, 0) as relationship,  
											i.created_date,
											i.recognized_date,
											i.provided_date,
											i.status 
										FROM
											erp_fa_interview_recognitions i
										LEFT JOIN erp_fa_rsd_applications a ON i.application_id = a.id
										LEFT JOIN erp_fa_family_members f ON i.member_id = f.id
										UNION
											SELECT
												ii.id,  
												aa.case_prefix,
												aa.case_no,
												mm.firstname,
												mm.firstname_kh,
												mm.lastname,
												mm.lastname_kh,
												mm.gender,
												mm.dob,
												mm.pob,
												mm.pob_kh,
												mm.nationality,
												mm.nationality_kh,
												mm.religion,
												mm.religion_kh,
												mm.ethnicity_kh,
												mm.ethnicity,
												mm.country,
												mm.province,
												mm.district,
												mm.commune,
												mm.village,
												mm.address,
												mm.address_kh,
												mm.photo,
												mm.relationship , 
												ii.created_date,
												ii.recognized_date,
												ii.provided_date,
												ii.status 
											FROM
												erp_fa_interview_recognitions ii
											LEFT JOIN erp_fa_rsd_applications aa ON ii.application_id = aa.id
											INNER JOIN erp_fa_interview_recognitions_members AS m ON m.recognition_id = ii.id
											LEFT JOIN erp_fa_family_members AS mm ON m.member_id = mm.id
									) AS v
								WHERE 1=1 {$sql}
								AND v.status <> 'deleted'
								ORDER BY
									v.id ASC
							")
						   ->result();			  
		return $result;
	} 	
	
	public function getRefugeePermanentReports($post = false) 
	{ 
		$post = $this->input->post();
		if($post['from']){
			$this->db->where("date(created_date) >=",$this->erp->fld($post['from']));
			$this->db->where("date(created_date) <=",$this->erp->fld($post['to']));
		}
		if($post['case_no']){
			$this->db->where("concat(case_prefix,case_no)",$post['case_no']);			
		}  
		if($post['q']){  
			$this->db->where("firstname LIKE",'%'.$post['q'].'%');
		  	$this->db->or_where("firstname_kh LIKE",'%'.$post['q'].'%');
			$this->db->or_where("lastname_kh LIKE",'%'.$post['q'].'%');
			$this->db->or_where("lastname_kh LIKE",'%'.$post['q'].'%');
			$this->db->or_where("nationality LIKE",'%'.$post['q'].'%');
			$this->db->or_where("nationality_kh LIKE",'%'.$post['q'].'%');  
		} 
		$result = $this->db->select("*")
						   ->from("v_refugee_permanent_card_request_report")
                           ->where("v_refugee_permanent_card_request_report.status <> 'deleted' ")
						   ->order_by('created_date','DESC')
						   ->get()->result(); 
		return $result;
	}
	
	public function getRefugeeTravelDocumentReports($post = false) 
	{ 
		$post = $this->input->post();
		if($post['from']){
			$this->db->where("date(erp_fa_refugee_travel_document_request.created_date) >=",$this->erp->fld($post['from']));
			$this->db->where("date(erp_fa_refugee_travel_document_request.created_date) <=",$this->erp->fld($post['to']));
		}
		if($post['case_no']){
			$this->db->where("concat(case_prefix,case_no)",$post['case_no']);			
		}  
		if($post['q']){  
			$this->db->where("firstname LIKE",'%'.$post['q'].'%');
		  	$this->db->or_where("firstname_kh LIKE",'%'.$post['q'].'%');
			$this->db->or_where("lastname_kh LIKE",'%'.$post['q'].'%');
			$this->db->or_where("lastname_kh LIKE",'%'.$post['q'].'%');
			$this->db->or_where("nationality LIKE",'%'.$post['q'].'%');
			$this->db->or_where("nationality_kh LIKE",'%'.$post['q'].'%');  
		}
		$result = $this->db->select("*")
						   ->from("erp_v_fa_refugee_travel_document_request_report")
                           ->where("erp_v_fa_refugee_travel_document_request_report.status <> 'deleted' ")
						  	->order_by("created_date1","desc")
						   ->get()->result(); 
		return $result;
	}
	
	public function getRefugeeCessationReports($post = false) 
	{ 
		$post = $this->input->post();
		$sql = "";
		if($post['from']){
			$sql .= " AND created_date >= '".$this->erp->fld($post['from'])."'";
			$sql .= " AND created_date <= '".$this->erp->fld($post['to'])."'";			
		}		
		if($post['counseling_no']){
			$sql .= " AND v.counseling_no = '".$post['counseling_no']."'";
		}
		if($post['case_no']){
			$sql .= " AND concat(case_prefix,case_no) = '".$post['case_no']."'";
		}
		if($post['q']){
			$sql .= " AND firstname LIKE '%{$post['q']}%' OR lastname LIKE '%{$post['q']}%' OR nationality LIKE '%{$post['q']}%' OR 
					      firstname_kh LIKE '%{$post['q']}%' OR lastname_kh LIKE '%{$post['q']}%' OR nationality_kh LIKE '%{$post['q']}%' 
						 
						  ";
		}
		$result = $this->db->query("
								SELECT
									 *
								FROM
									(
										SELECT
											i.id,  
											a.case_prefix,
											a.case_no,
											if(i.member_id > 0, f.firstname,a.firstname) as firstname,
											if(i.member_id > 0, f.firstname_kh,a.firstname_kh) as firstname_kh,
											if(i.member_id > 0, f.lastname,a.lastname) as lastname,
											if(i.member_id > 0, f.lastname_kh,a.lastname_kh) as lastname_kh,
											if(i.member_id > 0, f.gender,a.gender) as gender,
											if(i.member_id > 0, f.dob,a.dob) as dob,
											if(i.member_id > 0, f.pob,a.pob) as pob,
											if(i.member_id > 0, f.pob_kh,a.pob_kh) as pob_kh,
											if(i.member_id > 0, f.nationality,a.nationality) as nationality,
											if(i.member_id > 0, f.nationality_kh,a.nationality_kh) as nationality_kh,
											if(i.member_id > 0, f.religion,a.religion) as religion,
											if(i.member_id > 0, f.religion_kh,a.religion_kh) as religion_kh,
											if(i.member_id > 0, f.ethnicity,a.ethnicity) as ethnicity,
											if(i.member_id > 0, f.ethnicity_kh,a.ethnicity_kh) as ethnicity_kh,
											if(i.member_id > 0, f.country,a.country) as country,
											if(i.member_id > 0, f.province,a.province) as province,
											if(i.member_id > 0, f.district,a.district) as district,
											if(i.member_id > 0, f.commune,a.commune) as commune,
											if(i.member_id > 0, f.village,a.village) as village,
											if(i.member_id > 0, f.address,a.address) as address,
											if(i.member_id > 0, f.address_kh,a.address_kh) as address_kh,
											if(i.member_id > 0, f.photo,a.photo) as photo,
											if(i.member_id > 0, f.relationship, 0) as relationship,
											i.contact_number as phone, 
											i.created_date,
											i.recognized_date,
											i.provided_date,
											i.status 
										FROM
											erp_fa_refugee_cessation_elimination_request i
										LEFT JOIN erp_fa_rsd_applications a ON i.application_id = a.id
										LEFT JOIN erp_fa_family_members f ON i.member_id = f.id
										UNION
											SELECT
												ii.id,  
												aa.case_prefix,
												aa.case_no,
												mm.firstname,
												mm.firstname_kh,
												mm.lastname,
												mm.lastname_kh,
												mm.gender,
												mm.dob,
												mm.pob,
												mm.pob_kh,
												mm.nationality,
												mm.nationality_kh,
												mm.religion,
												mm.religion_kh,
												mm.ethnicity_kh,
												mm.ethnicity,
												mm.country,
												mm.province,
												mm.district,
												mm.commune,
												mm.village,
												mm.address,
												mm.address_kh,
												mm.photo,
												mm.relationship,
												ii.contact_number as phone,
												ii.created_date,
												ii.recognized_date,
												ii.provided_date,
												ii.status 
											FROM
												erp_fa_refugee_cessation_elimination_request ii
											LEFT JOIN erp_fa_rsd_applications aa ON ii.application_id = aa.id
											INNER JOIN erp_fa_refugee_cessation_elimination_members AS m ON m.cessation_id = ii.id
											LEFT JOIN erp_fa_family_members AS mm ON m.member_id = mm.id
									) AS v
								WHERE 1=1 {$sql}
								AND v.status <> 'deleted'
								ORDER BY
									v.id ASC

									")
						   ->result();
		return $result;
	}
	
	public function getRefugeeWithdrawalReports($post = false) 
	{ 
		$post = $this->input->post();
		$sql = "";
		if($post['from']){
			$sql .= " AND created_date >= '".$this->erp->fld($post['from'])."'";
			$sql .= " AND created_date <= '".$this->erp->fld($post['to'])."'";			
		}		
		if($post['counseling_no']){
			$sql .= " AND v.counseling_no = '".$post['counseling_no']."'";
		}
		if($post['case_no']){
			$sql .= " AND concat(case_prefix,case_no) = '".$post['case_no']."'";
		}
		if($post['q']){
			$sql .= " AND firstname LIKE '%{$post['q']}%' OR lastname LIKE '%{$post['q']}%' OR nationality LIKE '%{$post['q']}%' OR 
					      firstname_kh LIKE '%{$post['q']}%' OR lastname_kh LIKE '%{$post['q']}%' OR nationality_kh LIKE '%{$post['q']}%' 
						 
						  ";
		}
		$result = $this->db->query("
									SELECT
									 *
								FROM
									(
										SELECT
											i.id,  
											a.case_prefix,
											a.case_no,
											if(i.member_id > 0, f.firstname,a.firstname) as firstname,
											if(i.member_id > 0, f.firstname_kh,a.firstname_kh) as firstname_kh,
											if(i.member_id > 0, f.lastname,a.lastname) as lastname,
											if(i.member_id > 0, f.lastname_kh,a.lastname_kh) as lastname_kh,
											if(i.member_id > 0, f.gender,a.gender) as gender,
											if(i.member_id > 0, f.dob,a.dob) as dob,
											if(i.member_id > 0, f.pob,a.pob) as pob,
											if(i.member_id > 0, f.pob_kh,a.pob_kh) as pob_kh,
											if(i.member_id > 0, f.nationality,a.nationality) as nationality,
											if(i.member_id > 0, f.nationality_kh,a.nationality_kh) as nationality_kh,
											if(i.member_id > 0, f.religion,a.religion) as religion,
											if(i.member_id > 0, f.religion_kh,a.religion_kh) as religion_kh,
											if(i.member_id > 0, f.ethnicity,a.ethnicity) as ethnicity,
											if(i.member_id > 0, f.ethnicity_kh,a.ethnicity_kh) as ethnicity_kh,
											if(i.member_id > 0, f.country,a.country) as country,
											if(i.member_id > 0, f.province,a.province) as province,
											if(i.member_id > 0, f.district,a.district) as district,
											if(i.member_id > 0, f.commune,a.commune) as commune,
											if(i.member_id > 0, f.village,a.village) as village,
											if(i.member_id > 0, f.address,a.address) as address,
											if(i.member_id > 0, f.address_kh,a.address_kh) as address_kh,
											if(i.member_id > 0, f.photo,a.photo) as photo,
											if(i.member_id > 0, f.relationship, 0) as relationship,
											i.contact_number as phone, 
											i.created_date,
											i.recognized_date,
											i.provided_date,
											i.status 
										FROM
											erp_fa_refugee_withdrawal_request i
										LEFT JOIN erp_fa_rsd_applications a ON i.application_id = a.id
										LEFT JOIN erp_fa_family_members f ON i.member_id = f.id
										UNION
											SELECT
												ii.id,  
												aa.case_prefix,
												aa.case_no,
												mm.firstname,
												mm.firstname_kh,
												mm.lastname,
												mm.lastname_kh,
												mm.gender,
												mm.dob,
												mm.pob,
												mm.pob_kh,
												mm.nationality,
												mm.nationality_kh,
												mm.religion,
												mm.religion_kh,
												mm.ethnicity_kh,
												mm.ethnicity,
												mm.country,
												mm.province,
												mm.district,
												mm.commune,
												mm.village,
												mm.address,
												mm.address_kh,
												mm.photo,
												mm.relationship,
												ii.contact_number as phone,
												ii.created_date,
												ii.recognized_date,
												ii.provided_date,
												ii.status
											FROM
												erp_fa_refugee_withdrawal_request ii
											LEFT JOIN erp_fa_rsd_applications aa ON ii.application_id = aa.id
											INNER JOIN erp_fa_refugee_withdrawal_members AS m ON m.withdrawal_id = ii.id
											LEFT JOIN erp_fa_family_members AS mm ON m.member_id = mm.id
									) AS v
								WHERE 1=1 {$sql}
								AND v.status <> 'deleted'
								ORDER BY
									v.id ASC
								")
						   ->result();
		return $result;
	}
	
	public function getRefugeeCardReports($post = false) 
	{ 
		$post = $this->input->post();
		if($post['from']){
			$this->db->where("date(created_date1) >=",$this->erp->fld($post['from']));
			$this->db->where("date(created_date1) <=",$this->erp->fld($post['to']));
		}
		if($post['case_no']){
			$this->db->where("concat(case_prefix,case_no)",$post['case_no']);			
		}  
		if($post['q']){  
			$this->db->where("firstname LIKE",'%'.$post['q'].'%');
		  	$this->db->or_where("firstname_kh LIKE",'%'.$post['q'].'%');
			$this->db->or_where("lastname_kh LIKE",'%'.$post['q'].'%');
			$this->db->or_where("lastname_kh LIKE",'%'.$post['q'].'%');
			$this->db->or_where("nationality LIKE",'%'.$post['q'].'%');
			$this->db->or_where("nationality_kh LIKE",'%'.$post['q'].'%');  
		}
		$result = $this->db->select("*")
						   ->from("erp_v_fa_refugee_card_request_report")
                            ->where("erp_v_fa_refugee_card_request_report.status <> 'deleted'")
							->order_by("created_date1","desc")
						   ->get()->result(); 
		return $result;
		
	}
	 
	/*=============REPORT K2===================*/
	
	public function getAsylumSeekerReports() 
	{		
		$post = $this->input->post();		
		$sql = "";
		if($post['from']){
			$sql .= " AND created_date >= '".$this->erp->fld($post['from'])."'";
			$sql .= " AND created_date <= '".$this->erp->fld($post['to'])."'";			
		}		
		if($post['counseling_no']){
			$sql .= " AND v.counseling_no = '".$post['counseling_no']."'";
		}
		if($post['q']){
			$sql .= " AND firstname LIKE '%{$post['q']}%' OR lastname LIKE '%{$post['q']}%' OR nationality LIKE '%{$post['q']}%' OR 
					      firstname_kh LIKE '%{$post['q']}%' OR lastname_kh LIKE '%{$post['q']}%' OR nationality_kh LIKE '%{$post['q']}%' OR
						  counseling_no LIKE '%{$post['q']}%'
						  ";
		}
		$result = $this->db->query("
								SELECT
									*
								FROM
									(
										SELECT
											id,
											counseling_no,
											firstname,
											firstname_kh,
											lastname,
											lastname_kh,
											gender,
											dob,
											pob,
											pob_kh,
											nationality,
											nationality_kh,
											religion,
											religion_kh,
											ethnicity_kh,
											ethnicity,
											country,
											province,
											district,
											commune,
											village,
											address,
											address_kh,
											photo,
											0 as relationship,
											status,
											phone,
											created_date,
											0 as non_accompanying
										FROM
											erp_fa_counseling
										UNION
											SELECT
												c.id,
												c.counseling_no,
												m.firstname,
												m.firstname_kh,
												m.lastname,
												m.lastname_kh,
												m.gender,
												m.dob,
												m.pob,
												m.pob_kh,
												m.nationality,
												m.nationality_kh,
												m.religion,
												m.religion_kh,
												m.ethnicity_kh,
												m.ethnicity,
												m.country,
												m.province,
												m.district,
												m.commune,
												m.village,
												m.address,
												m.address_kh,
												m.photo,
												m.relationship,
												c.status,
												c.phone,
												c.created_date,
												m.non_accompanying
											FROM
												erp_fa_counseling AS c
											INNER JOIN erp_fa_family_members AS m ON m.counseling_id = c.id
									) AS v
								WHERE 1=1 {$sql}
								AND non_accompanying=0
								ORDER BY
									v.counseling_no DESC
							")
						   ->result();
		return $result;
	}
	// Report K3 
	public function getAllRegisterReportsPrint() 
	{		
		$post = $this->input->post();		
		$sql = "";
		if($post['from']){
			$sql .= " AND created_date >= '".$this->erp->fld($post['from'])."'";
			$sql .= " AND created_date <= '".$this->erp->fld($post['to'])."'";			
		}		
		if($post['counseling_no']){
			$sql .= " AND v.counseling_no = '".$post['counseling_no']."'";
		}
		if($post['q']){
			$sql .= " AND firstname LIKE '%{$post['q']}%' OR lastname LIKE '%{$post['q']}%' OR nationality LIKE '%{$post['q']}%' OR 
					      firstname_kh LIKE '%{$post['q']}%' OR lastname_kh LIKE '%{$post['q']}%' OR nationality_kh LIKE '%{$post['q']}%' OR
						  counseling_no LIKE '%{$post['q']}%'
						  ";
		}
		$result = $this->db->query("
								SELECT
									*
								FROM
									(
										SELECT
											id,
											counseling_no,
											firstname,
											firstname_kh,
											lastname,
											lastname_kh,
											gender,
											dob,
											pob,
											pob_kh,
											nationality,
											nationality_kh,
											religion,
											religion_kh,
											ethnicity_kh,
											ethnicity,
											country,
											province,
											district,
											commune,
											village,
											address,
											address_kh,
											photo,
											0 as relationship,
											phone,
											created_date
										FROM
											erp_fa_counseling
										UNION
											SELECT
												c.id,
												c.counseling_no,
												m.firstname,
												m.firstname_kh,
												m.lastname,
												m.lastname_kh,
												m.gender,
												m.dob,
												m.pob,
												m.pob_kh,
												m.nationality,
												m.nationality_kh,
												m.religion,
												m.religion_kh,
												m.ethnicity_kh,
												m.ethnicity,
												m.country,
												m.province,
												m.district,
												m.commune,
												m.village,
												m.address,
												m.address_kh,
												m.photo,
												m.relationship,
												c.phone,
												c.created_date
											FROM
												erp_fa_counseling AS c
											INNER JOIN erp_fa_family_members AS m ON m.counseling_id = c.id
									) AS v
								WHERE 1=1 {$sql}
								ORDER BY
									v.counseling_no DESC
							")
						   ->result();
		return $result;
	}
	public function getGeneralReport() 
	{
		$post = $this->input->post();
		$sql = "";
		if($post['from']){
			$sql .= " AND created_date >= '".$this->erp->fld($post['from'])."'";
			$sql .= " AND created_date <= '".$this->erp->fld($post['to'])."'";			
		}		
		if($post['counseling_no']){
			$sql .= " AND v.counseling_no = '".$post['counseling_no']."'";
		}
		if($post['case_no']){
			$sql .= " AND concat(case_prefix,case_no) = '".$post['case_no']."'";
		}
		if($post['q']){
			$sql .= " AND firstname LIKE '%{$post['q']}%' OR lastname LIKE '%{$post['q']}%' OR nationality LIKE '%{$post['q']}%' OR 
					      firstname_kh LIKE '%{$post['q']}%' OR lastname_kh LIKE '%{$post['q']}%' OR nationality_kh LIKE '%{$post['q']}%' OR
						  counseling_no LIKE '%{$post['q']}%' OR case_no LIKE '%{$post['q']}%' OR concat(case_prefix,case_no) LIKE '%{$post['q']}%'
						  ";
		}
		$result = $this->db->query("
								SELECT
									*
								FROM
									(
										SELECT
											aa.id,
											counseling_no,
											aa.case_prefix,
											aa.case_no,			
											aa.firstname,
											aa.firstname_kh,
											aa.lastname,
											aa.lastname_kh,
											aa.gender,
											aa.dob,
											aa.pob,
											aa.pob_kh,
											aa.nationality,
											aa.nationality_kh,
											aa.religion,
											aa.religion_kh,
											aa.ethnicity_kh,
											aa.ethnicity,
											aa.country,
											aa.province,
											aa.district,
											aa.commune,
											aa.village,
											aa.address,
											aa.address_kh,
											aa.photo,
											0 AS relationship,
											aa.phone,
											aa.created_date
										FROM
											erp_fa_counseling as cc
										INNER JOIN erp_fa_rsd_applications AS aa ON aa.counseling_id = cc.id
										UNION
											SELECT
												c.id,				
												c.counseling_no,
												a.case_prefix,
												a.case_no,				
												m.firstname,
												m.firstname_kh,
												m.lastname,
												m.lastname_kh,
												m.gender,
												m.dob,
												m.pob,
												m.pob_kh,
												m.nationality,
												m.nationality_kh,
												m.religion,
												m.religion_kh,
												m.ethnicity_kh,
												m.ethnicity,
												m.country,
												m.province,
												m.district,
												m.commune,
												m.village,
												m.address,
												m.address_kh,
												m.photo,
												m.relationship,
												c.phone,
												c.created_date
											FROM
												erp_fa_counseling AS c
											INNER JOIN erp_fa_rsd_applications AS a ON a.counseling_id = c.id
											INNER JOIN erp_fa_family_members AS m ON m.counseling_id = a.counseling_id
									) AS v
								WHERE
									1 = 1 {$sql}
								ORDER BY
									v.counseling_no DESC")
						   ->result();
		return $result;
	}
	
	public function getAsylumSeekerRegisterReports() 
	{
		$post = $this->input->post();
		$sql = "";
		if($post['from']){
			$sql .= " AND created_date >= '".$this->erp->fld($post['from'])."'";
			$sql .= " AND created_date <= '".$this->erp->fld($post['to'])."'";			
		}		
		if($post['counseling_no']){
			$sql .= " AND v.counseling_no = '".$post['counseling_no']."'";
		}
		if($post['case_no']){
			$sql .= " AND concat(case_prefix,case_no) = '".$post['case_no']."'";
		}
		if($post['q']){
			$sql .= " AND firstname LIKE '%{$post['q']}%' OR lastname LIKE '%{$post['q']}%' OR nationality LIKE '%{$post['q']}%' OR 
					      firstname_kh LIKE '%{$post['q']}%' OR lastname_kh LIKE '%{$post['q']}%' OR nationality_kh LIKE '%{$post['q']}%' OR
						  counseling_no LIKE '%{$post['q']}%' OR case_no LIKE '%{$post['q']}%' OR concat(case_prefix,case_no) LIKE '%{$post['q']}%'
						  ";
		}
		$result = $this->db->query("
								SELECT
									*
								FROM
									(
										SELECT
											aa.id,
											counseling_no,
											aa.case_prefix,
											aa.case_no,			
											aa.firstname,
											aa.firstname_kh,
											aa.lastname,
											aa.lastname_kh,
											aa.gender,
											aa.dob,
											aa.pob,
											aa.pob_kh,
											aa.nationality,
											aa.nationality_kh,
											aa.religion,
											aa.religion_kh,
											aa.ethnicity_kh,
											aa.ethnicity,
											aa.country,
											aa.province,
											aa.district,
											aa.commune,
											aa.village,
											aa.address,
											aa.address_kh,
											aa.photo,
											0 AS relationship,
											aa.phone,
											aa.created_date,
											aa.level,
											0 AS non_accompanying,
											aa.status
										FROM
											erp_fa_counseling as cc
										INNER JOIN erp_fa_rsd_applications AS aa ON aa.counseling_id = cc.id
										UNION
											SELECT
												c.id,				
												c.counseling_no,
												a.case_prefix,
												a.case_no,				
												m.firstname,
												m.firstname_kh,
												m.lastname,
												m.lastname_kh,
												m.gender,
												m.dob,
												m.pob,
												m.pob_kh,
												m.nationality,
												m.nationality_kh,
												m.religion,
												m.religion_kh,
												m.ethnicity_kh,
												m.ethnicity,
												m.country,
												m.province,
												m.district,
												m.commune,
												m.village,
												m.address,
												m.address_kh,
												m.photo,
												m.relationship,
												c.phone,
												c.created_date,
												a.level,
												m.non_accompanying,
												a.status
											FROM
												erp_fa_counseling AS c
											INNER JOIN erp_fa_family_members AS m ON m.counseling_id = c.id
											INNER JOIN erp_fa_rsd_applications AS a ON a.counseling_id = c.id
									) AS v
								WHERE
									1 = 1 {$sql}
								AND non_accompanying=0
                                AND v.status <> 'deleted'
								ORDER BY
									v.counseling_no DESC")
						   ->result();
		return $result;
	}
	
	public function getAsylumSeekerPreliminaryReports() 
	{
		$post = $this->input->post();
		$sql = "";
		if($post['from']){
			$this->db->where("date(erp_fa_asylum_preliminary_stay.created_date) >=",$this->erp->fld($post['from']));
			$this->db->where("date(erp_fa_asylum_preliminary_stay.created_date) <=",$this->erp->fld($post['to']));
		}
		if($post['case_no']){
			$this->db->where("concat(case_prefix,case_no)",$post['case_no']);			
		}
		$result = $this->db->select("fa_rsd_applications.*, valid_until")
						   ->from("fa_asylum_preliminary_stay")
						   ->join("fa_rsd_applications","fa_rsd_applications.id=application_id","left")
                           ->where("fa_asylum_preliminary_stay.status <> 'deleted' ")
						   ->get()->result();
		return $result;
	}
	
	public function getAsylumSeekerWithdrawalReports() 
	{
		$post = $this->input->post();
		if($post['from']){
			$this->db->where("date(created_date) >=",$this->erp->fld($post['from']));
			$this->db->where("date(created_date) <=",$this->erp->fld($post['to']));
		}
		if($post['case_no']){
			$this->db->where("concat(case_prefix,case_no)",$post['case_no']);			
		}
		$result = $this->db->select("fa_rsd_applications.*")
						   ->from("fa_asylum_withdrawal")
						   ->join("fa_rsd_applications","fa_rsd_applications.id=application_id","left")
                           ->where("fa_asylum_withdrawal.status <> 'deleted' ")
						   ->get()
						   ->result();
		return $result;
	}
		
	/*==============REPORT K4==============*/
	
	public function getRefugeeGuaranteeReports()
	{	
		$post = $this->input->post();
		$sql = "";
		if($post['from']){
			$sql .= " AND created_date >= '".$this->erp->fld($post['from'])."'";
			$sql .= " AND created_date <= '".$this->erp->fld($post['to'])."'";			
		}
		if($post['case_no']){
			$sql .= " AND concat(case_prefix,case_no) = '".$post['case_no']."'";
		}
		$result = $this->db->query("SELECT
										*
									FROM
										(
											SELECT
												vv.id,
												aa.case_prefix,
												aa.case_no,
												aa.firstname,
												aa.firstname_kh,
												aa.lastname,
												aa.lastname_kh,
												aa.gender,
												aa.dob,
												aa.pob,
												aa.pob_kh,
												aa.nationality,
												aa.nationality_kh,
												aa.religion,
												aa.religion_kh,
												aa.ethnicity_kh,
												aa.ethnicity,
												vv.country,
												null as state,
												vv.province,
												vv.district,
												vv.commune,
												vv.village,
												vv.address,
												vv.address_kh,
												vv.created_date, 
												vv.occupation_kh,
												vv.occupation,
												0 AS relationship,
												null AS come_option,
												null AS status_option,
												null as member_id,
												vv.status
											FROM
												erp_fa_rsd_applications AS aa
											INNER JOIN erp_fa_refugee_family_guarantee_request AS vv ON vv.application_id = aa.id
											
											UNION
												SELECT
													v.id,
													a.case_prefix,
													a.case_no,
													m.firstname,
													m.firstname_kh,
													m.lastname,
													m.lastname_kh,
													m.gender,
													m.dob,
													m.pob,
													m.pob_kh,
													m.nationality,
													m.nationality_kh,
													m.religion,
													m.religion_kh,
													m.ethnicity_kh,
													m.ethnicity,
													m.country,
													m.state,
													m.province,
													m.district,
													m.commune,
													m.village,
													m.address,
													m.address_kh,
													v.created_date,
													m.occupation_kh,
													m.occupation,
													m.relationship,
													m.come_option,
													m.status_option,
													v.member_id,
													v.status
												FROM
													erp_fa_rsd_applications AS a
												INNER JOIN erp_fa_refugee_family_guarantee_request AS v ON v.application_id = a.id
												INNER JOIN erp_fa_refugee_family_guarantee_members as m on m.guarantee_id = v.id
												
										) AS v
									WHERE 1 = 1 {$sql}  
								    AND v.member_id >= 0
								    AND v.status <> 'deleted'
									ORDER BY
									v.id DESC
								    ")->result();	
		return $result;
	}

	public function getRefugeeHomeVisitReports()
	{
		$post = $this->input->post();
		$sql = "";
		if($post['from']){
			$sql .= " AND day_visit >= '".$this->erp->fld($post['from'])."'";
			$sql .= " AND day_visit <= '".$this->erp->fld($post['to'])."'";			
		}		
		if($post['case_no']){
			$sql .= " AND concat(case_prefix,case_no) = '".$post['case_no']."'";
		}
		if($post['q']){
			$sql .= " AND firstname LIKE '%{$post['q']}%' OR lastname LIKE '%{$post['q']}%' OR nationality LIKE '%{$post['q']}%' OR 
					      firstname_kh LIKE '%{$post['q']}%' OR lastname_kh LIKE '%{$post['q']}%' OR nationality_kh LIKE '%{$post['q']}%' OR
						  case_no LIKE '%{$post['q']}%' OR concat(case_prefix,case_no) LIKE '%{$post['q']}%'
						  ";
		}
		
		$result = $this->db->query("SELECT
										*
									FROM
										(
										SELECT
											vv.id,
											aa.case_prefix,
											aa.case_no,
											IF(vv.member_id >0 ,m.firstname,aa.firstname) AS firstname,
											IF(vv.member_id >0 ,m.firstname_kh,aa.firstname_kh) AS firstname_kh,
											IF(vv.member_id >0 ,m.lastname,aa.lastname) AS lastname,
											IF(vv.member_id >0 ,m.lastname_kh,aa.lastname_kh) AS lastname_kh,
											IF(vv.member_id >0 ,m.gender,aa.gender) AS gender,
											IF(vv.member_id >0 ,m.dob,aa.dob) AS dob,
											IF(vv.member_id >0 ,m.pob,aa.pob) AS pob,
											IF(vv.member_id >0 ,m.pob_kh,aa.pob_kh) AS pob_kh,
											IF(vv.member_id >0 ,m.nationality,aa.nationality) AS nationality,
											IF(vv.member_id >0 ,m.nationality_kh,aa.nationality_kh) AS nationality_kh,
											IF(vv.member_id >0 ,m.religion,aa.religion) AS religion,
											IF(vv.member_id >0 ,m.religion_kh,aa.religion_kh) AS religion_kh,
											IF(vv.member_id >0 ,m.ethnicity_kh,aa.ethnicity_kh) AS ethnicity_kh,
											IF(vv.member_id >0 ,m.ethnicity,aa.ethnicity) AS ethnicity,
											vv.country,
											vv.province,
											vv.district,
											vv.commune,
											vv.village,
											vv.address,
											vv.address_kh,
											vv.occupation,
											vv.occupation_kh,
											m.relationship,
											vv.day_visit,
											vv.created_date,
											vv.status
										FROM
										erp_fa_rsd_applications AS aa
											INNER JOIN erp_fa_refugee_home_visit AS vv ON vv.application_id = aa.id
											LEFT JOIN erp_fa_family_members as m ON m.id = vv.member_id
											UNION
												SELECT
													v.id,
													a.case_prefix,
													a.case_no,
													m.firstname,
													m.firstname_kh,
													m.lastname,
													m.lastname_kh,
													m.gender,
													m.dob,
													m.pob,
													m.pob_kh,
													m.nationality,
													m.nationality_kh,
													m.religion,
													m.religion_kh,
													m.ethnicity_kh,
													m.ethnicity,
													m.country,
													m.province,
													m.district,
													m.commune,
													m.village,
													m.address,
													m.address_kh,
													m.occupation,
													m.occupation_kh,
													m.relationship,
													v.day_visit,
													v.created_date,
													v.status
												FROM
											erp_fa_rsd_applications AS a
											INNER JOIN erp_fa_refugee_home_visit AS v ON v.application_id = a.id
											INNER JOIN erp_fa_refugee_home_visit_members AS m ON m.visit_id = v.id
										) AS v
									WHERE v.day_visit !='' {$sql}
									AND v.status <> 'deleted'
									ORDER BY v.created_date DESC ")->result();
		return $result;
	}
	public function getReferenceCounselingNo() 
	{
		$row = $this->db->get("fa_counseling")->num_rows();
		$no = sprintf("%04s", $row+1);
		return $no;
	}     		
	
	public function updateSetting($data = NULL) 
	{
		if($this->db->update("settings", $data)){
			return true;
		}
		return false;
	} 
	
	/*==============REPORT K3==============*/
	public function getAsylumRegisterReports() 
	{
		
		$post = $this->input->post();
		$sql = "";
		if($post['from']){
			$sql .= " AND registered_date >= '".$this->erp->fld($post['from'])."'";
			$sql .= " AND registered_date <= '".$this->erp->fld($post['to'])."'";
		}	
		if($post['case_no']){
			$sql .= " AND concat(case_prefix,case_no) = '".$post['case_no']."'";
		}
		if($post['q']){
			$sql .= " AND firstname LIKE '%{$post['q']}%' OR lastname LIKE '%{$post['q']}%' OR nationality LIKE '%{$post['q']}%' OR 
					      firstname_kh LIKE '%{$post['q']}%' OR lastname_kh LIKE '%{$post['q']}%' OR nationality_kh LIKE '%{$post['q']}%' OR
						  case_no LIKE '%{$post['q']}%' OR concat(case_prefix,case_no) LIKE '%{$post['q']}%'
						  ";
		} 
		// $sql .= " AND status = 'active'";
		// $sql .= " AND status = 'inactive'";
		$sql .= " AND id  NOT IN (SELECT application_id FROM erp_fa_interview_appointments) ";
		
		$result = $this->db->query("
					SELECT
                            *
                        FROM
                            (
                                SELECT
                                    aa.id,
                                    aa.case_prefix,
                                    aa.full_case,
                                    aa.case_no,
                                    aa.firstname,
			                        aa.lastname,
                                    aa.firstname_kh,
                                    aa.lastname_kh,
                                    aa.gender,
                                    aa.dob,
                                    aa.nationality_kh,
                                    aa.nationality,
                                    'ម្ចាស់ករណី' AS relationship_kh,
                                    0 AS relationship,
                                    aa.created_date,
                                    aa.registered_date,
                                    aa.`status`,
                                    aa.photo
                                FROM
                                    erp_fa_rsd_applications AS aa
                                UNION
                                    SELECT
                                        bb.id,
                                        bb.case_prefix,
                                        bb.full_case,
                                        bb.case_no,
                                        erp_fa_family_members.firstname,
			                            erp_fa_family_members.lastname,
                                        erp_fa_family_members.firstname_kh,
                                        erp_fa_family_members.lastname_kh,
                                        erp_fa_family_members.gender,
                                        erp_fa_family_members.dob,
                                        erp_fa_family_members.nationality_kh,
                                        erp_fa_family_members.nationality,
                                        erp_fa_family_relationship.relationship_kh,
                                        erp_fa_family_members.relationship,
                                        bb.created_date,
                                        bb.registered_date,
                                        bb.`status`,
                                        erp_fa_family_members.photo
                                    FROM
                                        erp_fa_rsd_applications AS bb
                                    INNER JOIN erp_fa_family_members ON erp_fa_family_members.counseling_id = bb.counseling_id
                                    INNER JOIN erp_fa_family_relationship ON erp_fa_family_relationship.id = erp_fa_family_members.relationship
                                    WHERE erp_fa_family_members.non_accompanying = 0
                            ) AS v
					WHERE  1=1 {$sql} 
					ORDER BY v.created_date DESC
		")->result();
		return $result;  
	}  
	public function getInteviewAppointmentReports() 
	{
		
		$post = $this->input->post();
		$f = '00:00:00';
		$t = '23:59:59';
		$sql = "";
		if($post['from']){
			$sql .= " AND v.appointment_date >= '".$this->erp->fld($post['from'])."$f'";
			$sql .= " AND v.appointment_date <= '".$this->erp->fld($post['to'])."$t'";
		}	
		if($post['case_no']){
			$sql .= " AND concat(case_prefix,case_no) = '".$post['case_no']."'";
		}
		if($post['q']){
			$sql .= " AND firstname LIKE '%{$post['q']}%' OR lastname LIKE '%{$post['q']}%' OR nationality LIKE '%{$post['q']}%' OR 
					      firstname_kh LIKE '%{$post['q']}%' OR lastname_kh LIKE '%{$post['q']}%' OR nationality_kh LIKE '%{$post['q']}%' OR
						  case_no LIKE '%{$post['q']}%' OR concat(case_prefix,case_no) LIKE '%{$post['q']}%'
						  ";
		} 
		$sql .= " AND status = 'active'";
		$sql .= " AND procedure_sequence = 'appointmented'";
		$sql .= " AND status <> 'deleted'";
		$sql .= " AND is_appeal = 0";
	    $sql .= " AND relationship_kh = 'ម្ចាស់ករណី'";
		 $result = $this->db->query("SELECT
                                *
                            FROM
                                (
                                    SELECT
                                        aa.id AS ids,
                                        firstname_kh,
                                        case_no,
                                        case_prefix,
                                        gender,
                                        aa.dob,
                                        lastname_kh,
                                        firstname,
                                        lastname,
                                        nationality_kh,
                                        nationality,
                                        aa.religion,
                                        aa.religion_kh,
                                        aa. STATUS,
                                        erp_fa_interview_appointments.created_date,
                                        erp_fa_interview_appointments.appointment_date,
                                        aa.phone,
                                        aa.country,
                                        aa.commune,
                                        aa.district,
                                        aa.province,
                                        aa.pob_kh,
                                        aa.pob,
                                        aa.procedure_sequence,
                                        erp_fa_interview_appointments.is_appeal,
                                        'ម្ចាស់ករណី'	as relationship_kh,
                                        aa.photo
                                    FROM
                                        erp_fa_rsd_applications AS aa
                                    INNER JOIN erp_fa_interview_appointments ON application_id = aa.id
                                    UNION
                                        SELECT
                                            bb.id AS ids,
                                            erp_fa_family_members.firstname_kh,
                                            case_no,
                                            case_prefix,
                                            erp_fa_family_members.gender,
                                            erp_fa_family_members.dob,
                                            erp_fa_family_members.lastname_kh,
                                            erp_fa_family_members.firstname,
                                            erp_fa_family_members.lastname,
                                            erp_fa_family_members.nationality_kh,
                                            erp_fa_family_members.nationality,
                                            erp_fa_family_members.religion,
                                            erp_fa_family_members.religion_kh,
                                            bb. STATUS,
                                            erp_fa_interview_appointments.created_date,
                                            erp_fa_interview_appointments.appointment_date,
                                            bb.phone,
                                            erp_fa_family_members.country,
                                            erp_fa_family_members.commune,
                                            erp_fa_family_members.district,
                                            erp_fa_family_members.province,
                                            erp_fa_family_members.pob_kh,
                                            erp_fa_family_members.pob,
                                            bb.procedure_sequence,
                                            erp_fa_interview_appointments.is_appeal,
                                            erp_fa_family_relationship.relationship_kh,
                                            bb.photo
                                        FROM
                                            erp_fa_rsd_applications AS bb
                                        INNER JOIN erp_fa_interview_appointments ON application_id = bb.id
                                        INNER JOIN erp_fa_family_members ON erp_fa_family_members.counseling_id = bb.counseling_id
                                        INNER JOIN erp_fa_family_relationship ON erp_fa_family_relationship.id = erp_fa_family_members.relationship
                                        WHERE erp_fa_family_members.non_accompanying = 0
                                ) AS v
		 			WHERE  1=1 {$sql} 
		 			ORDER BY v.created_date DESC
		 ")->result();

		return $result; 
		
	}
	
	public function getInteviewedReports() 
	{ 
		$post = $this->input->post();
		$sql = "";
		if($post['from']){
			$sql .= " AND interview_date >= '".$this->erp->fld($post['from'])."'";
			$sql .= " AND interview_date <= '".$this->erp->fld($post['to'])."'";
		}	
		if($post['case_no']){
			$sql .= " AND concat(case_prefix,case_no) = '".$post['case_no']."'";
		}
		if($post['q']){
			$sql .= " AND firstname LIKE '%{$post['q']}%' OR lastname LIKE '%{$post['q']}%' OR nationality LIKE '%{$post['q']}%' OR 
					      firstname_kh LIKE '%{$post['q']}%' OR lastname_kh LIKE '%{$post['q']}%' OR nationality_kh LIKE '%{$post['q']}%' OR
						  case_no LIKE '%{$post['q']}%' OR concat(case_prefix,case_no) LIKE '%{$post['q']}%'
						  ";
		}  
		$sql .= " AND procedure_sequence = 'interviewed'";
		$sql .= " AND is_appeal <> 1";
		$sql .= " AND relationship_kh = 'ម្ចាស់ករណី'";
		$result = $this->db->query("SELECT
                                    *
                                FROM
                                    (
                                        SELECT
                                            aa.id AS ids,
                                            firstname_kh,
                                            case_no,
                                            case_prefix,
                                            gender,
                                            aa.dob,
                                            lastname_kh,
                                            firstname,
                                            lastname,
                                            nationality_kh,
                                            nationality,
                                            religion,
                                            religion_kh,
                                            erp_fa_interviews. status,
                                            erp_fa_interviews.interview_date,
                                            erp_fa_interviews.created_date,
                                            aa.phone,
                                            aa.country,
                                            aa.commune,
                                            aa.district,
                                            aa.province,
                                            aa.pob_kh,
                                            aa.pob,
                                            negative_appeal_id,
                                            'ម្ចាស់ករណី' AS relationship_kh,
                                            aa.procedure_sequence,
                                            erp_fa_interviews.is_appeal,
                                            aa.photo
                                        FROM
                                            erp_fa_rsd_applications AS aa
                                        INNER JOIN erp_fa_interviews ON application_id = aa.id
                                        UNION
                                            SELECT
                                                bb.id AS ids,
                                                erp_fa_family_members.firstname_kh,
                                                case_no,
                                                case_prefix,
                                                erp_fa_family_members.gender,
                                                erp_fa_family_members.dob,
                                                erp_fa_family_members.lastname_kh,
                                                erp_fa_family_members.firstname,
                                                erp_fa_family_members.lastname,
                                                erp_fa_family_members.nationality_kh,
                                                erp_fa_family_members.nationality,
                                                erp_fa_family_members.religion,
                                                erp_fa_family_members.religion_kh,
                                                erp_fa_interviews. status,
                                                erp_fa_interviews.interview_date,
                                                erp_fa_interviews.created_date,
                                                bb.phone,
                                                bb.country,
                                                bb.commune,
                                                bb.district,
                                                bb.province,
                                                bb.pob_kh,
                                                bb.pob,
                                                negative_appeal_id,
                                                erp_fa_family_relationship.relationship_kh,
                                                bb.procedure_sequence,
                                                erp_fa_interviews.is_appeal,
                                                bb.photo
                                            FROM
                                                erp_fa_rsd_applications AS bb
                                            INNER JOIN erp_fa_interviews ON application_id = bb.id
                                            INNER JOIN erp_fa_family_members ON erp_fa_family_members.counseling_id = bb.counseling_id
                                            INNER JOIN erp_fa_family_relationship ON erp_fa_family_relationship.id = erp_fa_family_members.relationship
                                            WHERE erp_fa_family_members.non_accompanying = 0
                                    ) AS v
					        WHERE  1=1 {$sql} 
					        AND v.STATUS <> 'deleted'
					        ORDER BY v.created_date DESC
		")->result();
		return $result; 
		
	}
	
	public function getInteviewOnAppealReports() 
	{
		$post = $this->input->post();
		$sql = "";
		if($post['from']){
			$sql .= " AND date(interview_date) >= '".$this->erp->fld($post['from'])."'";
			$sql .= " AND date(interview_date) <= '".$this->erp->fld($post['to'])."'";
		}	
		if($post['case_no']){
			$sql .= " AND concat(case_prefix,case_no) = '".$post['case_no']."'";
		}
		if($post['q']){
			$sql .= " AND firstname LIKE '%{$post['q']}%' OR lastname LIKE '%{$post['q']}%' OR nationality LIKE '%{$post['q']}%' OR 
					      firstname_kh LIKE '%{$post['q']}%' OR lastname_kh LIKE '%{$post['q']}%' OR nationality_kh LIKE '%{$post['q']}%' OR
						  case_no LIKE '%{$post['q']}%' OR concat(case_prefix,case_no) LIKE '%{$post['q']}%'
						  ";
		} 
		$sql .= " AND procedure_sequence = 'interview_appeal'";
		$sql .= " AND is_appeal = 1";

		$result = $this->db->query("SELECT
                                *
                            FROM
                                (
                                    SELECT
                                        aa.id AS ids,
                                        firstname_kh,
                                        case_no,
                                        case_prefix,
                                        gender,
                                        aa.dob,
                                        lastname_kh,
                                        firstname,
                                        lastname,
                                        nationality_kh,
                                        nationality,
                                        religion,
                                        religion_kh,
                                        erp_fa_interviews.interview_date,
                                        erp_fa_interviews.created_date,
                                        aa.phone,
                                        aa.country,
                                        aa.commune,
                                        aa.district,
                                        aa.province,
                                        aa.pob_kh,
                                        aa.pob,
                                        negative_appeal_id,
                                        aa.procedure_sequence,
                                        erp_fa_interviews.is_appeal,
                                        erp_fa_interviews.`status`,
                                        'ម្ចាស់ករណី' as relationship_kh,
                                        aa.photo
                                    FROM
                                        erp_fa_rsd_applications aa
                                    INNER JOIN erp_fa_interviews ON application_id = aa.id
                                    UNION
                                        SELECT
                                            bb.id AS ids,
                                            erp_fa_family_members.firstname_kh,
                                            bb.case_no,
                                            bb.case_prefix,
                                            erp_fa_family_members.gender,
                                            erp_fa_family_members.dob,
                                            erp_fa_family_members.lastname_kh,
                                            erp_fa_family_members.firstname,
                                            erp_fa_family_members.lastname,
                                            erp_fa_family_members.nationality_kh,
                                            erp_fa_family_members.nationality,
                                            erp_fa_family_members.religion,
                                            erp_fa_family_members.religion_kh,
                                            erp_fa_interviews.interview_date,
                                            erp_fa_interviews.created_date,
                                            bb.phone,
                                            erp_fa_family_members.country,
                                            erp_fa_family_members.commune,
                                            erp_fa_family_members.district,
                                            erp_fa_family_members.province,
                                            erp_fa_family_members.pob_kh,
                                            erp_fa_family_members.pob,
                                            negative_appeal_id,
                                            bb.procedure_sequence,
                                            erp_fa_interviews.is_appeal,
                                            erp_fa_interviews.`status`,
                                            erp_fa_family_relationship.relationship_kh,
                                            erp_fa_family_members.photo
                                        FROM
                                            erp_fa_rsd_applications bb
                                        INNER JOIN erp_fa_interviews ON application_id = bb.id
                                        INNER JOIN erp_fa_family_members ON erp_fa_family_members.counseling_id = bb.counseling_id
                            INNER JOIN erp_fa_family_relationship ON erp_fa_family_relationship.id = erp_fa_family_members.relationship
                            WHERE erp_fa_family_members.non_accompanying = 0
                                ) AS v
					      WHERE  1=1 {$sql} 
					      AND v.status <> 'deleted'
					      ORDER BY created_date DESC
		")->result();
		return $result; 
		
	}
	
	public function getInteviewedEvaluateReports() 
	{
		$post = $this->input->post();
		$sql = "";
		if($post['from']){
			$sql .= " AND date(created_date) >= '".$this->erp->fld($post['from'])."'";
			$sql .= " AND date(created_date) <= '".$this->erp->fld($post['to'])."'";
		}	
		if($post['case_no']){
			$sql .= " AND concat(case_prefix,case_no) = '".$post['case_no']."'";
		}	
		if($post['status']){
			$sql .= " AND status = '".$post['status']."'";
		}
		if($post['q']){
			$sql .= " AND firstname LIKE '%{$post['q']}%' OR lastname LIKE '%{$post['q']}%' OR nationality LIKE '%{$post['q']}%' OR 
					      firstname_kh LIKE '%{$post['q']}%' OR lastname_kh LIKE '%{$post['q']}%' OR nationality_kh LIKE '%{$post['q']}%' OR
						  case_no LIKE '%{$post['q']}%' OR concat(case_prefix,case_no) LIKE '%{$post['q']}%'
						  ";
		} 
		$sql .= " AND procedure_sequence = 'evaluating'";
		$sql .= " AND is_appeal = 0";
		
		$result = $this->db->query("
                                SELECT
                                    *
                                FROM
                                    (
                                        SELECT
                                            aa.id AS ids,
                                            firstname_kh,
                                            case_no,
                                            case_prefix,
                                            gender,
                                            aa.dob,
                                            lastname_kh,
                                            firstname,
                                            lastname,
                                            nationality_kh,
                                            nationality,
                                            religion,
                                            religion_kh,
                                            ee.approved_date,
                                            ee.rejected_date,
                                            ee.created_date,
                                            ee.evaluate_date,
                                            CONCAT(
                                                IFNULL(ee.approved_date, ''),
                                                '',
                                                IFNULL(ee.rejected_date, '')
                                            ) AS status_date,
                                            aa.phone,
                                            aa.country,
                                            aa.commune,
                                            aa.district,
                                            aa.province,
                                            ee. STATUS,
                                            aa.pob_kh,
                                            aa.pob,
                                            negative_appeal_id,
                                            aa.procedure_sequence,
                                            erp_fa_interviews.is_appeal,
                                            'ម្ចាស់ករណី' as relationship_kh,
                                            aa.`status` as app_status,
                                            aa.photo
                                        FROM
                                            erp_fa_rsd_applications AS aa
                                        INNER JOIN erp_fa_interview_evaluations ee ON application_id = aa.id
                                        INNER JOIN erp_fa_interviews ON erp_fa_interviews.id = ee.interview_id
                                        UNION
                                            SELECT
                                                bb.id AS ids,
                                                erp_fa_family_members.firstname_kh,
                                                case_no,
                                                case_prefix,
                                                erp_fa_family_members.gender,
                                                erp_fa_family_members.dob,
                                                erp_fa_family_members.lastname_kh,
                                                erp_fa_family_members.firstname,
                                                erp_fa_family_members.lastname,
                                                erp_fa_family_members.nationality_kh,
                                                erp_fa_family_members.nationality,
                                                erp_fa_family_members.religion,
                                                erp_fa_family_members.religion_kh,
                                                ee.approved_date,
                                                ee.rejected_date,
                                                ee.created_date,
                                                ee.evaluate_date,
                                                CONCAT(
                                                    IFNULL(ee.approved_date, ''),
                                                    '',
                                                    IFNULL(ee.rejected_date, '')
                                                ) AS status_date,
                                                bb.phone,
                                                erp_fa_family_members.country,
                                                erp_fa_family_members.commune,
                                                erp_fa_family_members.district,
                                                erp_fa_family_members.province,
                                                ee. STATUS,
                                                erp_fa_family_members.pob_kh,
                                                erp_fa_family_members.pob,
                                                negative_appeal_id,
                                                bb.procedure_sequence,
                                                erp_fa_interviews.is_appeal,
                                                erp_fa_family_relationship.relationship_kh,
                                                bb.`status` AS app_status,
                                                erp_fa_family_members.photo
                                            FROM
                                                erp_fa_rsd_applications AS bb
                                            INNER JOIN erp_fa_interview_evaluations ee ON application_id = bb.id
                                            INNER JOIN erp_fa_interviews ON erp_fa_interviews.id = ee.interview_id
                                            INNER JOIN erp_fa_family_members ON erp_fa_family_members.counseling_id = bb.counseling_id
                                            INNER JOIN erp_fa_family_relationship ON erp_fa_family_relationship.id = erp_fa_family_members.relationship
                                            WHERE erp_fa_family_members.non_accompanying = 0
                                    ) AS v
						WHERE  1=1 {$sql} 
						AND v.STATUS <> 'deleted'
						ORDER BY created_date DESC
		")->result();
		return $result; 
		
	}
	
	public function getInteviewedEvaluateOnAppealReports() 
	{
		$post = $this->input->post();
		$sql = "";
		if($post['from']){
			$sql .= " AND date(evaluate_date) >= '".$this->erp->fld($post['from'])."'";
			$sql .= " AND date(evaluate_date) <= '".$this->erp->fld($post['to'])."'";
		}	
		if($post['case_no']){
			$sql .= " AND concat(case_prefix,case_no) = '".$post['case_no']."'";
		}	
		if($post['status']){
			$sql .= " AND status = '".$post['status']."'";
		}
		if($post['q']){
			$sql .= " AND firstname LIKE '%{$post['q']}%' OR lastname LIKE '%{$post['q']}%' OR nationality LIKE '%{$post['q']}%' OR 
					      firstname_kh LIKE '%{$post['q']}%' OR lastname_kh LIKE '%{$post['q']}%' OR nationality_kh LIKE '%{$post['q']}%' OR
						  case_no LIKE '%{$post['q']}%' OR concat(case_prefix,case_no) LIKE '%{$post['q']}%'
						  ";
		} 
		 
		$sql .= " AND is_appeal = 1";
		$sql .= " AND procedure_sequence = 'evaluating_appeal' ";
		
		$result = $this->db->query("SELECT
                                *
                            FROM
                                (
                                    SELECT
                                        aa.id AS ids,
                                        firstname_kh,
                                        case_no,
                                        case_prefix,
                                        gender,
                                        aa.dob,
                                        lastname_kh,
                                        firstname,
                                        lastname,
                                        nationality_kh,
                                        nationality,
                                        religion,
                                        religion_kh,
                                        ee.approved_date,
                                        ee.rejected_date,
                                        ee.created_date,
                                        ee.evaluate_date,
                                        CONCAT(
                                            IFNULL(ee.approved_date, ''),
                                            '',
                                            IFNULL(ee.rejected_date, '')
                                        ) AS status_date,
                                        aa.phone,
                                        aa.country,
                                        aa.commune,
                                        aa.district,
                                        aa.province,
                                        ee. STATUS,
                                        aa.pob_kh,
                                        aa.pob,
                                        negative_appeal_id,
                                        erp_fa_interviews.is_appeal,
                                        aa.procedure_sequence,
                                        aa.`status` AS app_status,
                                        'ម្ចាស់ករណី' AS relationship_kh,
                                        aa.photo
                                    FROM
                                        erp_fa_rsd_applications aa
                                    INNER JOIN erp_fa_interview_evaluations ee ON application_id = aa.id
                                    INNER JOIN erp_fa_interviews ON erp_fa_interviews.id = ee.interview_id
                                    UNION
                                        SELECT
                                            bb.id AS ids,
                                            erp_fa_family_members.firstname_kh,
                                            case_no,
                                            case_prefix,
                                            erp_fa_family_members.gender,
                                            erp_fa_family_members.dob,
                                            erp_fa_family_members.lastname_kh,
                                            erp_fa_family_members.firstname,
                                            erp_fa_family_members.lastname,
                                            erp_fa_family_members.nationality_kh,
                                            erp_fa_family_members.nationality,
                                            erp_fa_family_members.religion,
                                            erp_fa_family_members.religion_kh,
                                            ee.approved_date,
                                            ee.rejected_date,
                                            ee.created_date,
                                            ee.evaluate_date,
                                            CONCAT(
                                                IFNULL(ee.approved_date, ''),
                                                '',
                                                IFNULL(ee.rejected_date, '')
                                            ) AS status_date,
                                            bb.phone,
                                            erp_fa_family_members.country,
                                            erp_fa_family_members.commune,
                                            erp_fa_family_members.district,
                                            erp_fa_family_members.province,
                                            ee. STATUS,
                                            erp_fa_family_members.pob_kh,
                                            erp_fa_family_members.pob,
                                            negative_appeal_id,
                                            erp_fa_interviews.is_appeal,
                                            bb.procedure_sequence,
                                            bb.`status` AS app_status,
                                            erp_fa_family_relationship.relationship_kh,
                                            erp_fa_family_members.photo
                                        FROM
                                            erp_fa_rsd_applications bb
                                        INNER JOIN erp_fa_interview_evaluations ee ON application_id = bb.id
                                        INNER JOIN erp_fa_interviews ON erp_fa_interviews.id = ee.interview_id
                                        INNER JOIN erp_fa_family_members ON erp_fa_family_members.counseling_id = bb.counseling_id
                                        INNER JOIN erp_fa_family_relationship ON erp_fa_family_relationship.id = erp_fa_family_members.relationship
                                        WHERE erp_fa_family_members.non_accompanying = 0
                                ) AS v
						WHERE  1=1 {$sql} 
						AND v.STATUS <> 'deleted'
						ORDER BY created_date DESC
		")->result();
		return $result; 
		
	}
	
	public function getInterviewNegativeAppealReports() 
	{
		$post = $this->input->post();
		$sql = "";
		if($post['from']){
			$sql .= " AND b.created_date >= '".$this->erp->fld($post['from'])."'";
			$sql .= " AND b.created_date <= '".$this->erp->fld($post['to'])."'";			
		}	
		if($post['case_no']){
			$sql .= " AND concat(case_prefix,case_no) = '".$post['case_no']."'";
		}
		if($post['q']){
			$sql .= " AND firstname LIKE '%{$post['q']}%' OR lastname LIKE '%{$post['q']}%' OR nationality LIKE '%{$post['q']}%' OR 
					      firstname_kh LIKE '%{$post['q']}%' OR lastname_kh LIKE '%{$post['q']}%' OR nationality_kh LIKE '%{$post['q']}%' OR
						  case_no LIKE '%{$post['q']}%' OR concat(case_prefix,case_no) LIKE '%{$post['q']}%'
						  ";
		} 
		$sql .= " AND a.status = 'active'";
		
		$result = $this->db->query("
					SELECT 
						a.id AS ids,
						firstname_kh,
						case_no,
						case_prefix,
						gender,
						a.dob,
						lastname_kh,
						firstname,
						lastname,
						nationality_kh,
						nationality,
						religion,
						religion_kh,  
						b.approved_date,
						b.rejected_date,
						b.created_date,
						a.phone,
						a.country,
						a.commune,
						a.district,
						a.province,
						b.status, 
						a.pob_kh,
						a.pob
						FROM
							erp_fa_rsd_applications AS a
						INNER JOIN erp_fa_interview_negatives_appeal AS b ON b.application_id = a.id
						WHERE  1=1 {$sql} 
		")->result();
		return $result; 
		
	}
	
	public function getInterviewRequestOnAppealReports() 
	{
		$post = $this->input->post();
		$sql = "";
		if($post['from']){
			$sql .= " AND date(appeal_date) >= '".$this->erp->fld($post['from'])."'";
			$sql .= " AND date(appeal_date) <= '".$this->erp->fld($post['to'])."'";
		}	
		if($post['case_no']){
			$sql .= " AND concat(case_prefix,case_no) = '".$post['case_no']."'";
		}
		if($post['status']){
			$sql .= " AND b.status = '".$post['status']."'";
		}
		if($post['q']){
			$sql .= " AND firstname LIKE '%{$post['q']}%' OR lastname LIKE '%{$post['q']}%' OR nationality LIKE '%{$post['q']}%' OR 
					      firstname_kh LIKE '%{$post['q']}%' OR lastname_kh LIKE '%{$post['q']}%' OR nationality_kh LIKE '%{$post['q']}%' OR
						  case_no LIKE '%{$post['q']}%' OR concat(case_prefix,case_no) LIKE '%{$post['q']}%'
						  ";
		} 
		// $sql .= " AND a.level = 'rejected1' ";
		$sql .= " AND procedure_sequence = 'request_appeal' ";

		$result = $this->db->query("SELECT
                                *
                            FROM
                                (
                                    SELECT
                                        a.id AS ids,
                                        firstname_kh,
                                        case_no,
                                        case_prefix,
                                        gender,
                                        a.dob,
                                        lastname_kh,
                                        firstname,
                                        lastname,
                                        nationality_kh,
                                        nationality,
                                        religion,
                                        religion_kh,
                                        b.approved_date,
                                        b.rejected_date,
                                        b.created_date,
                                        b.appeal_date,
                                        CONCAT(
                                            IFNULL(b.approved_date, ''),
                                            '',
                                            IFNULL(b.rejected_date, '')
                                        ) AS status_date,
                                        a.phone,
                                        a.country,
                                        a.commune,
                                        a.district,
                                        a.province,
                                        b. STATUS,
                                        a.pob_kh,
                                        a.pob,
                                        a.procedure_sequence,
                                        a.`status` app_status,
                                        'ម្ចាស់ករណី' as relationship_kh,
                                        a.photo
                                    FROM
                                        erp_fa_rsd_applications AS a
                                    LEFT JOIN erp_fa_interview_negatives_appeal AS b ON b.application_id = a.id
                                    UNION
                                        SELECT
                                            aa.id AS ids,
                                            erp_fa_family_members.firstname_kh,
                                            case_no,
                                            case_prefix,
                                            erp_fa_family_members.gender,
                                            erp_fa_family_members.dob,
                                            erp_fa_family_members.lastname_kh,
                                            erp_fa_family_members.firstname,
                                            erp_fa_family_members.lastname,
                                            erp_fa_family_members.nationality_kh,
                                            erp_fa_family_members.nationality,
                                            erp_fa_family_members.religion,
                                            erp_fa_family_members.religion_kh,
                                            b.approved_date,
                                            b.rejected_date,
                                            b.created_date,
                                            b.appeal_date,
                                            CONCAT(
                                                IFNULL(b.approved_date, ''),
                                                '',
                                                IFNULL(b.rejected_date, '')
                                            ) AS status_date,
                                            aa.phone,
                                            erp_fa_family_members.country,
                                            erp_fa_family_members.commune,
                                            erp_fa_family_members.district,
                                            erp_fa_family_members.province,
                                            b. STATUS,
                                            erp_fa_family_members.pob_kh,
                                            erp_fa_family_members.pob,
                                            aa.procedure_sequence,
                                            aa.`status` app_status,
                                            erp_fa_family_relationship.relationship_kh,
                                            erp_fa_family_members.photo
                                        FROM
                                            erp_fa_rsd_applications AS aa
                                        LEFT JOIN erp_fa_interview_negatives_appeal AS b ON b.application_id = aa.id
                                        INNER JOIN erp_fa_family_members ON erp_fa_family_members.counseling_id = aa.counseling_id
                                        INNER JOIN erp_fa_family_relationship ON erp_fa_family_relationship.id = erp_fa_family_members.relationship

                                    WHERE erp_fa_family_members.non_accompanying = 0
                                ) AS v
						WHERE  1=1 {$sql} 
						AND v.STATUS <> 'deleted'
		")->result();
		return $result; 
		



	}
	// k3
	public function getAllRegisterReports() 
	{
		$post = $this->input->post();
		$sql = "";
		if($post['from']){
			$sql .= " AND date(created_date) >= '".$this->erp->fld($post['from'])."'";
			$sql .= " AND date(created_date) <= '".$this->erp->fld($post['to'])."'";
		}		
		if($post['counseling_no']){
			$sql .= " AND counseling_no = '".$post['counseling_no']."'";
		}
		if($post['case_no']){
			$sql .= " AND concat(case_prefix,case_no) = '".$post['case_no']."'";
		}
		if($post['q']){
			$sql .= " AND firstname LIKE '%{$post['q']}%' OR lastname LIKE '%{$post['q']}%' OR nationality LIKE '%{$post['q']}%' OR 
					      firstname_kh LIKE '%{$post['q']}%' OR lastname_kh LIKE '%{$post['q']}%' OR nationality_kh LIKE '%{$post['q']}%' OR
						  counseling_no LIKE '%{$post['q']}%' OR case_no LIKE '%{$post['q']}%' OR concat(case_prefix,case_no) LIKE '%{$post['q']}%'
						  ";
		} 	
		
		$result = $this->db->query("SELECT
                                    *
                                FROM
                                    (
                                        SELECT
                                            aa.id,
                                            counseling_no,
                                            aa.case_prefix,
                                            aa.case_no,
                                            aa.firstname,
                                            aa.firstname_kh,
                                            aa.lastname,
                                            aa.lastname_kh,
                                            aa.gender,
                                            aa.dob,
                                            aa.pob,
                                            aa.pob_kh,
                                            aa.nationality,
                                            aa.nationality_kh,
                                            aa.religion,
                                            aa.religion_kh,
                                            aa.ethnicity_kh,
                                            aa.ethnicity,
                                            aa.country,
                                            aa.province,
                                            aa.district,
                                            aa.commune,
                                            aa.village,
                                            aa.address,
                                            aa.address_kh,
                                            aa.photo,
                                            0 AS relationship,
                                            aa.phone,
                                            aa.created_date,
                                            aa. LEVEL,
                                            aa.procedure_sequence,
                                            aa. STATUS,
                                            'ម្ចាស់ករណី' AS relationship_kh
                                        FROM
                                            erp_fa_counseling AS cc
                                        INNER JOIN erp_fa_rsd_applications AS aa ON aa.counseling_id = cc.id
                                        UNION
                                            SELECT
                                                a.id,
                                                counseling_no,
                                                a.case_prefix,
                                                a.case_no,
                                                erp_fa_family_members.firstname,
                                                erp_fa_family_members.firstname_kh,
                                                erp_fa_family_members.lastname,
                                                erp_fa_family_members.lastname_kh,
                                                erp_fa_family_members.gender,
                                                erp_fa_family_members.dob,
                                                erp_fa_family_members.pob,
                                                erp_fa_family_members.pob_kh,
                                                erp_fa_family_members.nationality,
                                                erp_fa_family_members.nationality_kh,
                                                erp_fa_family_members.religion,
                                                erp_fa_family_members.religion_kh,
                                                erp_fa_family_members.ethnicity_kh,
                                                erp_fa_family_members.ethnicity,
                                                erp_fa_family_members.country,
                                                erp_fa_family_members.province,
                                                erp_fa_family_members.district,
                                                erp_fa_family_members.commune,
                                                erp_fa_family_members.village,
                                                erp_fa_family_members.address,
                                                erp_fa_family_members.address_kh,
                                                erp_fa_family_members.photo,
                                                erp_fa_family_members.relationship,
                                                a.phone,
                                                a.created_date,
                                                a. LEVEL,
                                                a.procedure_sequence,
                                                a.STATUS,
                                                erp_fa_family_relationship.relationship_kh
                                            FROM
                                                erp_fa_counseling AS c
                                            INNER JOIN erp_fa_rsd_applications AS a ON a.counseling_id = c.id
                                            INNER JOIN erp_fa_family_members ON erp_fa_family_members.counseling_id = c.id
                                            INNER JOIN erp_fa_family_relationship ON erp_fa_family_relationship.id = erp_fa_family_members.relationship
                                            WHERE erp_fa_family_members.non_accompanying = 0
                                    ) AS v
                                WHERE
                                1 = 1 {$sql}
								ORDER BY created_date DESC")
						   ->result();
		return $result;
		
	}
	 
	public function addCounseling($data = NULL) 
	{ 
		if($this->db->insert('fa_counseling',$data))
		{	  
			return $this->db->insert_id();	
		} 
		return false;
	}
	
	public function updateCounseling($id = NULL,$data=NULL) 
	{ 
		$result = $this->db->where("id",$id)->update("fa_counseling",$data);
		return $result;
	}
	
	public function deleteCounseling($id=NULL) 
	{
		$result = $this->db->where("id",$id)->update("fa_counseling", array("status"=>"deleted"));
		return $result;
	} 
	
	public function getLastRefugeeCard()
	{ 
		$result = $this->db->select("*")->limit(1)->order_by("id","desc")->get("fa_refugee_card_request")->row();
		return $result;
	}
	// add report K3 
	// accept on appeal
	public function getAcceptOnAppeal() 
	{
		
		$post = $this->input->post();
		$sql = "";
		if($post['from']){
			$sql .= " AND date(created_date) >= '".$this->erp->fld($post['from'])."'";
			$sql .= " AND date(created_date) <= '".$this->erp->fld($post['to'])."'";
		}	
		if($post['case_no']){
			$sql .= " AND concat(case_prefix,case_no) = '".$post['case_no']."'";
		}
		if($post['q']){
			$sql .= " AND firstname LIKE '%{$post['q']}%' OR lastname LIKE '%{$post['q']}%' OR nationality LIKE '%{$post['q']}%' OR 
					      firstname_kh LIKE '%{$post['q']}%' OR lastname_kh LIKE '%{$post['q']}%' OR nationality_kh LIKE '%{$post['q']}%' OR
						  case_no LIKE '%{$post['q']}%' OR concat(case_prefix,case_no) LIKE '%{$post['q']}%'
						  ";
		} 
		$sql .= " AND is_appeal = 1";
		$sql .= " AND status = 'approved' ";
		$sql .= " AND level = 'approved2' ";
	
		$result = $this->db->query("SELECT
                                    *
                                FROM
                                    (
                                        SELECT
                                            erp_fa_interview_evaluations.id AS id,
                                            CONCAT(aa.case_prefix, aa.case_no) AS case_no,
                                            UPPER(firstname) AS firstname,
                                            UPPER(lastname) AS lastname,
                                            UPPER(firstname_kh) AS firstname_kh,
                                            UPPER(lastname_kh) AS lastname_kh,
                                            UPPER(gender) AS gender,
                                            dob,
                                            UPPER(nationality_kh) AS nationality_kh,
                                            erp_fa_interview_evaluations.created_date,
                                            erp_fa_interview_evaluations.approved_date,
                                            erp_fa_interview_evaluations. STATUS,
                                            aa.photo,
                                            erp_fa_interviews.is_appeal,
                                            aa. LEVEL,
                                            'ម្ចាស់ករណី' AS relationship_kh,
                                            aa.`status` AS app_status
                                        FROM
                                            erp_fa_rsd_applications aa
                                        RIGHT JOIN erp_fa_interview_evaluations ON application_id = aa.id
                                        JOIN erp_fa_interviews ON erp_fa_interview_evaluations.interview_id = erp_fa_interviews.id
                                        UNION
                                            SELECT
                                                erp_fa_interview_evaluations.id AS id,
                                                CONCAT(bb.case_prefix, bb.case_no) AS case_no,
                                                UPPER(
                                                    erp_fa_family_members.firstname
                                                ) AS firstname,
                                                UPPER(
                                                    erp_fa_family_members.lastname
                                                ) AS lastname,
                                                UPPER(
                                                    erp_fa_family_members.firstname_kh
                                                ) AS firstname_kh,
                                                UPPER(
                                                    erp_fa_family_members.lastname_kh
                                                ) AS lastname_kh,
                                                UPPER(
                                                    erp_fa_family_members.gender
                                                ) AS gender,
                                                erp_fa_family_members.dob,
                                                UPPER(
                                                    erp_fa_family_members.nationality_kh
                                                ) AS nationality_kh,
                                                erp_fa_interview_evaluations.created_date,
                                                erp_fa_interview_evaluations.approved_date,
                                                erp_fa_interview_evaluations. STATUS,
                                                erp_fa_family_members.photo,
                                                erp_fa_interviews.is_appeal,
                                                bb. LEVEL,
                                                erp_fa_family_relationship.relationship_kh,
                                                bb.`status` AS app_status
                                            FROM
                                                erp_fa_rsd_applications bb
                                            RIGHT JOIN erp_fa_interview_evaluations ON application_id = bb.id
                                            JOIN erp_fa_interviews ON erp_fa_interview_evaluations.interview_id = erp_fa_interviews.id
                                            INNER JOIN erp_fa_family_members ON erp_fa_family_members.counseling_id = bb.counseling_id
                                            INNER JOIN erp_fa_family_relationship ON erp_fa_family_relationship.id = erp_fa_family_members.relationship
                                            WHERE erp_fa_family_members.non_accompanying = 0
                                    ) AS v
					            WHERE  1=1 {$sql} 
					            ORDER BY created_date DESC
		")->result();
		return $result; 		
	}
    // Reject on appeal
	public function getRejectOnAppeal() 
	{
		
		$post = $this->input->post();
		$sql = "";
		if($post['from']){
			$sql .= " AND date(negative_date) >= '".$this->erp->fld($post['from'])."'";
			$sql .= " AND date(negative_date) <= '".$this->erp->fld($post['to'])."'";
		}	
		if($post['case_no']){
			$sql .= " AND concat(case_prefix,case_no) = '".$post['case_no']."'";
		}
		if($post['q']){
			$sql .= " AND firstname LIKE '%{$post['q']}%' OR lastname LIKE '%{$post['q']}%' OR nationality LIKE '%{$post['q']}%' OR 
					      firstname_kh LIKE '%{$post['q']}%' OR lastname_kh LIKE '%{$post['q']}%' OR nationality_kh LIKE '%{$post['q']}%' OR
						  case_no LIKE '%{$post['q']}%' OR concat(case_prefix,case_no) LIKE '%{$post['q']}%'
						  ";
		} 
		//$sql .= " AND resubmit = 1";
		//$sql .= " AND status = 'closed' ";
        $sql .= " AND v.status = 'approved'";
		$sql .= " AND procedure_sequence = 'rejected_appeal' ";
	
		$result = $this->db->query("SELECT
                            *
                        FROM
                            (
                                SELECT
                                    erp_fa_interview_negatives.id AS id,
                                    CONCAT(aa.case_prefix, aa.case_no) AS case_no,
                                    UPPER(firstname) AS firstname,
                                    UPPER(lastname) AS lastname,
                                    UPPER(firstname_kh) AS firstname_kh,
                                    UPPER(lastname_kh) AS lastname_kh,
                                    UPPER(gender) AS gender,
                                    dob,
                                    UPPER(nationality_kh) AS nationality_kh,
                                    erp_fa_interview_negatives.created_date,
                                    erp_fa_interview_negatives. STATUS,
                                    UPPER(nationality) AS nationality,
                                    religion_kh,
                                    religion,
                                    ethnicity_kh,
                                    ethnicity,
                                    phone,
                                    pob,
                                    pob_kh,
                                    negative_date,
                                    provided_date,
                                    marital_status,
                                    fathername_kh,
                                    mothername_kh,
                                    erp_fa_interview_negatives.img,
                                    erp_fa_interview_negatives.resubmit,
                                    aa.procedure_sequence,
									'ម្ចាស់ករណី' AS relationship_kh,
                                    aa.`status` AS app_status,
                                    aa.photo
                                FROM
                                    erp_fa_rsd_applications aa
                                INNER JOIN erp_fa_interview_negatives ON application_id = aa.id
                                UNION
                                    SELECT
                                        erp_fa_interview_negatives.id AS id,
                                        CONCAT(bb.case_prefix, bb.case_no) AS case_no,
                                        UPPER(erp_fa_family_members.firstname) AS firstname,
                                        UPPER(erp_fa_family_members.lastname) AS lastname,
                                        UPPER(erp_fa_family_members.firstname_kh) AS firstname_kh,
                                        UPPER(erp_fa_family_members.lastname_kh) AS lastname_kh,
                                        UPPER(erp_fa_family_members.gender) AS gender,
                                        erp_fa_family_members.dob,
                                        UPPER(erp_fa_family_members.nationality_kh) AS nationality_kh,
                                        erp_fa_interview_negatives.created_date,
                                        erp_fa_interview_negatives. STATUS,
                                        UPPER(erp_fa_family_members.nationality) AS nationality,
                                        erp_fa_family_members.religion_kh,
                                        erp_fa_family_members.religion,
                                        erp_fa_family_members.ethnicity_kh,
                                        erp_fa_family_members.ethnicity,
                                        bb.phone,
                                        erp_fa_family_members.pob,
                                        erp_fa_family_members.pob_kh,
                                        negative_date,
                                        provided_date,
                                        marital_status,
                                        bb.fathername_kh,
                                        bb.mothername_kh,
                                        erp_fa_interview_negatives.img,
                                        erp_fa_interview_negatives.resubmit,
                                        bb.procedure_sequence,
                                        erp_fa_family_relationship.relationship_kh,
                                        bb.`status` AS app_status,
                                        erp_fa_family_members.photo
                                    FROM
                                        erp_fa_rsd_applications bb
                                    INNER JOIN erp_fa_interview_negatives ON application_id = bb.id
                                    INNER JOIN erp_fa_family_members ON erp_fa_family_members.counseling_id = bb.counseling_id
                                    INNER JOIN erp_fa_family_relationship ON erp_fa_family_relationship.id = erp_fa_family_members.relationship
                                    WHERE erp_fa_family_members.non_accompanying = 0
                            ) AS v
					    WHERE  1=1 {$sql} 
					    AND v.resubmit = 1
					    ORDER BY created_date DESC
		")->result();
		return $result; 		
	}
		// Report k3 Chec out of request on appeal 
		



	public function getCheckOutRequestOnAppeal() 
	{		
		$post = $this->input->post();
		$sql = "";
		if($post['from']){
			$sql .= " AND date(created_date) >= '".$this->erp->fld($post['from'])."'";
			$sql .= " AND date(created_date) <= '".$this->erp->fld($post['to'])."'";
		}	
		if($post['case_no']){
			$sql .= " AND concat(case_prefix,case_no) = '".$post['case_no']."'";
		}
		if($post['q']){
			$sql .= " AND firstname LIKE '%{$post['q']}%' OR lastname LIKE '%{$post['q']}%' OR nationality LIKE '%{$post['q']}%' OR 
					      firstname_kh LIKE '%{$post['q']}%' OR lastname_kh LIKE '%{$post['q']}%' OR nationality_kh LIKE '%{$post['q']}%' OR
						  case_no LIKE '%{$post['q']}%' OR concat(case_prefix,case_no) LIKE '%{$post['q']}%'
						  ";
		} 
		$sql .= " AND (procedure_sequence = 'rejected_request_appeal' OR procedure_sequence = 'approved_request_appeal' )";
		//$sql .= " OR procedure_sequence = 'approved_request_appeal' ";
	
		$result = $this->db->query("
                              SELECT
                                    *
                                FROM
                                    (
                                        SELECT
                                            erp_fa_interview_negatives_appeal.id AS id,
                                            CONCAT(aa.case_prefix, aa.case_no) AS case_no,
                                            UPPER(firstname) AS firstname,
                                            UPPER(lastname) AS lastname,
                                            UPPER(firstname_kh) AS firstname_kh,
                                            UPPER(lastname_kh) AS lastname_kh,
                                            UPPER(gender) AS gender,
                                            dob,
                                            UPPER(nationality_kh) AS nationality_kh,
                                            erp_fa_interview_negatives_appeal.created_date,
                                            erp_fa_interview_negatives_appeal. STATUS,
                                            appeal_date,
                                            rejected_date,
                                            approved_date,
                                            phone,
                                            nationality,
                                            fathername,
                                            fathername_kh,
                                            mothername,
                                            mothername_kh,
                                            pob,
                                            pob_kh,
                                            religion,
                                            religion_kh,
                                            ethnicity,
                                            ethnicity_kh,
                                            address_kh,
                                            address,
                                            procedure_sequence,
                                            'ម្ចាស់ករណី' as relationship_kh,
			                                aa.`status` as app_status,
			                                aa.photo
                                        FROM
                                            erp_fa_rsd_applications AS aa
                                        INNER JOIN erp_fa_interview_negatives_appeal ON application_id = aa.id
                                        UNION
                                            SELECT
                                                erp_fa_interview_negatives_appeal.id AS id,
                                                CONCAT(bb.case_prefix, bb.case_no) AS case_no,
                                                UPPER(erp_fa_family_members.firstname) AS firstname,
                                                UPPER(erp_fa_family_members.lastname) AS lastname,
                                                UPPER(erp_fa_family_members.firstname_kh) AS firstname_kh,
                                                UPPER(erp_fa_family_members.lastname_kh) AS lastname_kh,
                                                UPPER(erp_fa_family_members.gender) AS gender,
                                                erp_fa_family_members.dob,
                                                UPPER(erp_fa_family_members.nationality_kh) AS nationality_kh,
                                                erp_fa_interview_negatives_appeal.created_date,
                                                erp_fa_interview_negatives_appeal. STATUS,
                                                appeal_date,
                                                rejected_date,
                                                approved_date,
                                                bb.phone,
                                                erp_fa_family_members.nationality,
                                                bb.fathername,
                                                bb.fathername_kh,
                                                bb.mothername,
                                                bb.mothername_kh,
                                                erp_fa_family_members.pob,
                                                erp_fa_family_members.pob_kh,
                                                erp_fa_family_members.religion,
                                                erp_fa_family_members.religion_kh,
                                                erp_fa_family_members.ethnicity,
                                                erp_fa_family_members.ethnicity_kh,
                                                erp_fa_family_members.address_kh,
                                                erp_fa_family_members.address,
                                                bb.procedure_sequence,
                                                erp_fa_family_relationship.relationship_kh,
				                                bb.`status` as app_status,
				                                erp_fa_family_members.photo
                                            FROM
                                                erp_fa_rsd_applications AS bb
                                            INNER JOIN erp_fa_interview_negatives_appeal ON application_id = bb.id
                                            INNER JOIN erp_fa_family_members ON erp_fa_family_members.counseling_id = bb.counseling_id
                                            INNER JOIN erp_fa_family_relationship ON erp_fa_family_relationship.id = erp_fa_family_members.relationship
                                            WHERE erp_fa_family_members.non_accompanying = 0
                                    ) AS v
					WHERE  1=1 {$sql} 
					ORDER BY created_date DESC
		")->result();
		return $result; 	
	}
	// Report k3 First Reject
	public function getFirstReject() 
	{	
		$post = $this->input->post();
		$sql = "";
		if($post['from']){
			$sql .= " AND date(rejected_date) >= '".$this->erp->fld($post['from'])."'";
			$sql .= " AND date(rejected_date) <= '".$this->erp->fld($post['to'])."'";
		}	
		if($post['case_no']){
			$sql .= " AND concat(case_prefix,case_no) = '".$post['case_no']."'";
		}
		if($post['q']){
			$sql .= " AND firstname LIKE '%{$post['q']}%' OR lastname LIKE '%{$post['q']}%' OR nationality LIKE '%{$post['q']}%' OR 
					      firstname_kh LIKE '%{$post['q']}%' OR lastname_kh LIKE '%{$post['q']}%' OR nationality_kh LIKE '%{$post['q']}%' OR
						  case_no LIKE '%{$post['q']}%' OR concat(case_prefix,case_no) LIKE '%{$post['q']}%' ";
		} 

		// $sql .= " AND erp_fa_interview_evaluations.resubmit <> 1 ";
		// $sql .= " AND erp_fa_interview_evaluations.status = 'closed' ";
		// $sql .= " AND erp_fa_interviews.is_appeal = 0";
		$sql .= " AND procedure_sequence = 'rejected_1' ";
		$sql .= " AND is_appeal = 0 ";

	
		$result = $this->db->query("SELECT
                                    *
                                FROM
                                    (
                                        SELECT
                                            erp_fa_interview_evaluations.id AS id,
                                            CONCAT(aa.case_prefix, aa.case_no) AS case_no,
                                            UPPER(firstname) AS firstname,
                                            UPPER(lastname) AS lastname,
                                            UPPER(firstname_kh) AS firstname_kh,
                                            UPPER(lastname_kh) AS lastname_kh,
                                            UPPER(gender) AS gender,
                                            dob,
                                            nationality,
                                            UPPER(nationality_kh) AS nationality_kh,
                                            erp_fa_interview_evaluations.created_date,
                                            erp_fa_interview_evaluations.rejected_date,
                                            aa. STATUS,
                                            religion,
                                            religion_kh,
                                            ethnicity_kh,
                                            ethnicity,
                                            aa.phone,
                                            pob,
                                            pob_kh,
                                            marital_status,
                                            fathername_kh,
                                            mothername_kh,
                                            aa.procedure_sequence,
                                            erp_fa_interviews.is_appeal,
                                            'ម្ចាស់ករណី' as relationship_kh,
                                            aa.photo
                                        FROM
                                            erp_fa_rsd_applications AS aa
                                        RIGHT JOIN erp_fa_interview_evaluations ON erp_fa_interview_evaluations.application_id = aa.id
                                        JOIN erp_fa_interviews ON erp_fa_interview_evaluations.interview_id = erp_fa_interviews.id
                                        UNION
                                            SELECT
                                                erp_fa_interview_evaluations.id AS id,
                                                CONCAT(bb.case_prefix, bb.case_no) AS case_no,
                                                UPPER(erp_fa_family_members.firstname) AS firstname,
                                                UPPER(erp_fa_family_members.lastname) AS lastname,
                                                UPPER(erp_fa_family_members.firstname_kh) AS firstname_kh,
                                                UPPER(erp_fa_family_members.lastname_kh) AS lastname_kh,
                                                UPPER(erp_fa_family_members.gender) AS gender,
                                                erp_fa_family_members.dob,
                                                erp_fa_family_members.nationality,
                                                UPPER(erp_fa_family_members.nationality_kh) AS nationality_kh,
                                                erp_fa_interview_evaluations.created_date,
                                                erp_fa_interview_evaluations.rejected_date,
                                                bb. STATUS,
                                                erp_fa_family_members.religion,
                                                erp_fa_family_members.religion_kh,
                                                bb.ethnicity_kh,
                                                bb.ethnicity,
                                                bb.phone,
                                                erp_fa_family_members.pob,
                                                erp_fa_family_members.pob_kh,
                                                marital_status,
                                                bb.fathername_kh,
                                                bb.mothername_kh,
                                                bb.procedure_sequence,
                                                erp_fa_interviews.is_appeal,
                                                erp_fa_family_relationship.relationship_kh,
                                                erp_fa_family_members.photo
                                            FROM
                                                erp_fa_rsd_applications AS bb
                                            RIGHT JOIN erp_fa_interview_evaluations ON erp_fa_interview_evaluations.application_id = bb.id
                                            JOIN erp_fa_interviews ON erp_fa_interview_evaluations.interview_id = erp_fa_interviews.id
                                            INNER JOIN erp_fa_family_members ON erp_fa_family_members.counseling_id = bb.counseling_id
                                            INNER JOIN erp_fa_family_relationship ON erp_fa_family_relationship.id = erp_fa_family_members.relationship
                                            WHERE erp_fa_family_members.non_accompanying = 0
                                    ) AS v
					WHERE  1=1 {$sql} 
					ORDER BY created_date DESC

		")->result();
		return $result;
	}
	// Report k3 First Recognise
	public function getFirstRecognise() 
	{	
		$post = $this->input->post();
		$sql = "";
		if($post['from']){
			$sql .= " AND date(approved_date) >= '".$this->erp->fld($post['from'])."'";
			$sql .= " AND date(approved_date) <= '".$this->erp->fld($post['to'])."'";
		}
		if($post['case_no']){
			$sql .= " AND concat(case_prefix,case_no) = '".$post['case_no']."'";
		}
		if($post['q']){
			$sql .= " AND firstname LIKE '%{$post['q']}%' OR lastname LIKE '%{$post['q']}%' OR nationality LIKE '%{$post['q']}%' OR 
					      firstname_kh LIKE '%{$post['q']}%' OR lastname_kh LIKE '%{$post['q']}%' OR nationality_kh LIKE '%{$post['q']}%' OR
						  case_no LIKE '%{$post['q']}%' OR concat(case_prefix,case_no) LIKE '%{$post['q']}%'
						  ";
		} 

		// $sql .= " AND erp_fa_interview_evaluations.resubmit <> 1 ";
		// $sql .= " AND erp_fa_interview_evaluations.status = 'approved' ";
		$sql .= " AND procedure_sequence = 'approved_1' ";
		$sql .= " AND is_appeal = 0 ";
		$result = $this->db->query("SELECT
                                    *
                                FROM
                                    (
                                        SELECT
                                            erp_fa_interview_evaluations.id AS id,
                                            CONCAT(aa.case_prefix, aa.case_no) AS case_no,
                                            UPPER(firstname) AS firstname,
                                            UPPER(lastname) AS lastname,
                                            UPPER(firstname_kh) AS firstname_kh,
                                            UPPER(lastname_kh) AS lastname_kh,
                                            UPPER(gender) AS gender,
                                            dob,
                                            nationality,
                                            UPPER(nationality_kh) AS nationality_kh,
                                            erp_fa_interview_evaluations.created_date,
                                            erp_fa_interview_evaluations.approved_date,
                                            aa. STATUS,
                                            religion,
                                            religion_kh,
                                            ethnicity_kh,
                                            ethnicity,
                                            phone,
                                            pob,
                                            pob_kh,
                                            marital_status,
                                            fathername_kh,
                                            mothername_kh,
                                            aa.procedure_sequence,
                                            erp_fa_interviews.is_appeal,
                                            'ម្ចាស់ករណី' as relationship_kh,
                                            photo
                                        FROM
                                            erp_fa_rsd_applications AS aa
                                        INNER JOIN erp_fa_interview_evaluations ON application_id = aa.id
                                        JOIN erp_fa_interviews ON erp_fa_interview_evaluations.interview_id = erp_fa_interviews.id
                                        UNION
                                            SELECT
                                                erp_fa_interview_evaluations.id AS id,
                                                CONCAT(bb.case_prefix, bb.case_no) AS case_no,
                                                UPPER(erp_fa_family_members.firstname) AS firstname,
                                                UPPER(erp_fa_family_members.lastname) AS lastname,
                                                UPPER(erp_fa_family_members.firstname_kh) AS firstname_kh,
                                                UPPER(erp_fa_family_members.lastname_kh) AS lastname_kh,
                                                UPPER(erp_fa_family_members.gender) AS gender,
                                                erp_fa_family_members.dob,
                                                erp_fa_family_members.nationality,
                                                UPPER(erp_fa_family_members.nationality_kh) AS nationality_kh,
                                                erp_fa_interview_evaluations.created_date,
                                                erp_fa_interview_evaluations.approved_date,
                                                bb. STATUS,
                                                erp_fa_family_members.religion,
                                                erp_fa_family_members.religion_kh,
                                                erp_fa_family_members.ethnicity_kh,
                                                erp_fa_family_members.ethnicity,
                                                bb.phone,
                                                erp_fa_family_members.pob,
                                                erp_fa_family_members.pob_kh,
                                                marital_status,
                                                bb.fathername_kh,
                                                bb.mothername_kh,
                                                bb.procedure_sequence,
                                                erp_fa_interviews.is_appeal,
                                                erp_fa_family_relationship.relationship_kh,
                                                erp_fa_family_members.photo
                                            FROM
                                                erp_fa_rsd_applications AS bb
                                            INNER JOIN erp_fa_interview_evaluations ON application_id = bb.id
                                            JOIN erp_fa_interviews ON erp_fa_interview_evaluations.interview_id = erp_fa_interviews.id
                                            INNER JOIN erp_fa_family_members ON erp_fa_family_members.counseling_id = bb.counseling_id
                                            INNER JOIN erp_fa_family_relationship ON erp_fa_family_relationship.id = erp_fa_family_members.relationship
                                            WHERE erp_fa_family_members.non_accompanying = 0
                                    ) AS v
					          WHERE  1=1 {$sql} 
					          ORDER BY created_date DESC

		")->result();
		return $result; 
	}
// interview_appointment_on_appeal
    public function getInterviewAppointmentOnAppeal() 
	{	
		$post = $this->input->post();
		$sql = "";
		if($post['from']){
			$sql .= " AND date(appointment_date) >= '".$this->erp->fld($post['from'])."'";
			$sql .= " AND date(appointment_date) <= '".$this->erp->fld($post['to'])."'";
		}	
		if($post['case_no']){
			$sql .= " AND concat(case_prefix,case_no) = '".$post['case_no']."'";
		}
		if($post['q']){
			$sql .= " AND firstname LIKE '%{$post['q']}%' OR lastname LIKE '%{$post['q']}%' OR nationality LIKE '%{$post['q']}%' OR 
					      firstname_kh LIKE '%{$post['q']}%' OR lastname_kh LIKE '%{$post['q']}%' OR nationality_kh LIKE '%{$post['q']}%' OR
						  case_no LIKE '%{$post['q']}%' OR concat(case_prefix,case_no) LIKE '%{$post['q']}%'
						  ";
		} 
		$sql .= " AND  status = 'active' ";
		$sql .= " AND is_appeal = 1";
		$sql .= " AND procedure_sequence = 'appointmented_appeal' ";
		$result = $this->db->query("SELECT
                                *
                            FROM
                                (
                                    SELECT
                                        erp_fa_interview_appointments.id AS id,
                                        CONCAT(aa.case_prefix, aa.case_no) AS case_no,
                                        UPPER(firstname) AS firstname,
                                        UPPER(lastname) AS lastname,
                                        UPPER(firstname_kh) AS firstname_kh,
                                        UPPER(lastname_kh) AS lastname_kh,
                                        UPPER(gender) AS gender,
                                        dob,
                                        nationality,
                                        UPPER(nationality_kh) AS nationality_kh,
                                        erp_fa_interview_appointments.created_date,
                                        erp_fa_interview_appointments. STATUS,
                                        religion,
                                        religion_kh,
                                        ethnicity_kh,
                                        ethnicity,
                                        phone,
                                        pob,
                                        pob_kh,
                                        marital_status,
                                        fathername_kh,
                                        mothername_kh,
                                        aa.photo,
                                        erp_fa_interview_appointments.is_appeal,
                                        erp_fa_interview_appointments.appointment_date,
                                        aa.procedure_sequence,
                                        'ម្ចាស់ករណី' AS relationship_kh,
                                        aa.`status` AS app_status
                                    FROM
                                        erp_fa_rsd_applications AS aa
                                    RIGHT JOIN erp_fa_interview_appointments ON application_id = aa.id
                                    UNION
                                        SELECT
                                            erp_fa_interview_appointments.id AS id,
                                            CONCAT(bb.case_prefix, bb.case_no) AS case_no,
                                            UPPER(
                                                erp_fa_family_members.firstname
                                            ) AS firstname,
                                            UPPER(
                                                erp_fa_family_members.lastname
                                            ) AS lastname,
                                            UPPER(
                                                erp_fa_family_members.firstname_kh
                                            ) AS firstname_kh,
                                            UPPER(
                                                erp_fa_family_members.lastname_kh
                                            ) AS lastname_kh,
                                            UPPER(
                                                erp_fa_family_members.gender
                                            ) AS gender,
                                            erp_fa_family_members.dob,
                                            erp_fa_family_members.nationality,
                                            UPPER(
                                                erp_fa_family_members.nationality_kh
                                            ) AS nationality_kh,
                                            erp_fa_interview_appointments.created_date,
                                            erp_fa_interview_appointments. STATUS,
                                            erp_fa_family_members.religion,
                                            erp_fa_family_members.religion_kh,
                                            erp_fa_family_members.ethnicity_kh,
                                            erp_fa_family_members.ethnicity,
                                            bb.phone,
                                            erp_fa_family_members.pob,
                                            erp_fa_family_members.pob_kh,
                                            marital_status,
                                            fathername_kh,
                                            mothername_kh,
                                            erp_fa_family_members.photo,
                                            erp_fa_interview_appointments.is_appeal,
                                            erp_fa_interview_appointments.appointment_date,
                                            bb.procedure_sequence,
                                            erp_fa_family_relationship.relationship_kh,
                                            bb.`status` AS app_status
                                        FROM
                                            erp_fa_rsd_applications AS bb
                                        RIGHT JOIN erp_fa_interview_appointments ON application_id = bb.id
                                        INNER JOIN erp_fa_family_members ON erp_fa_family_members.counseling_id = bb.counseling_id
                                        INNER JOIN erp_fa_family_relationship ON erp_fa_family_relationship.id = erp_fa_family_members.relationship
                                        WHERE erp_fa_family_members.non_accompanying = 0
                                ) AS v
					  WHERE  1=1 {$sql} 
                      ORDER BY created_date DESC
		")->result();
		return $result; 
	}
    /*
    Change status to miss_interview if refugee miss an interview
     */
	public function missInterviewAppointmentRefugee($id =false)
	{		
		if($this->db->where("id",$id)->update("fa_interview_appointments",array("status"=>"miss_interview") )){
			return true;
		}
		return false;		
	}



}
