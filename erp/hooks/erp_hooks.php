<?php

class erp_hooks {
	
    protected $CI;
	
    public function __construct() 
	{
        $this->CI =& get_instance();
    }
    public function check() 
	{
        if(! ($this->CI->db->conn_id)) {
            header("Location: install/index.php");
            die();
        }
    }
    
	public function minify() 
	{
        ini_set("pcre.recursion_limit", "16777");
        $buffer = $this->CI->output->get_output();
        $re =   '%# Collapse whitespace everywhere but in blacklisted elements.
                (?>             # Match all whitespans other than single space.
                  [^\S ]\s*     # Either one [\t\r\n\f\v] and zero or more ws,
                  | \s{2,}        # or two or more consecutive-any-whitespace.
                  ) # Note: The remaining regex consumes no text at all...
                (?=             # Ensure we are not in a blacklist tag.
                  [^<]*+        # Either zero or more non-"<" {normal*}
                  (?:           # Begin {(special normal*)*} construct
                    <           # or a < starting a non-blacklist tag.
                    (?!/?(?:textarea|pre|script)\b)
                    [^<]*+      # more non-"<" {normal*}
                    )*+           # Finish "unrolling-the-loop"
                (?:           # Begin alternation group.
                    <           # Either a blacklist start tag.
                    (?>textarea|pre|script)\b
                    | \z          # or end of file.
                    )             # End alternation group.
                )  # If we made it here, we are not in a blacklist tag.
                %Six';
        $new_buffer = preg_replace($re, " ", $buffer);
        if($new_buffer === null) {
            $new_buffer = $buffer;
        }
        $this->CI->output->set_output($new_buffer);
        $this->CI->output->_display();
    }

}



/* config/hooks.php */
$hook['display_override'][] = array(
    'class' => 'LogQueryHook',
    'function' => 'log_queries',
    'filename' => 'erp_hooks.php',
    'filepath' => 'hooks'
);

/* application/hooks/LogQueryHook.php */
class LogQueryHook {

    function log_queries() {    
        $CI =& get_instance();
        $times = $CI->db->query_times;
        $dbs    = array();
        $output = NULL;     
        $queries = $CI->db->queries;

        if (count($queries) == 0){
            $output .= "no queries\n";
        }else{
            foreach ($queries as $key=>$query){
                $output .= $query . "\n";
            }
            $took = round(doubleval($times[$key]), 3);
            $output .= "===[took:{$took}]\n\n";
        }

        $CI->load->helper('file');
        if ( ! write_file(APPPATH  . "/logs/queries.log.txt", $output, 'a+')){
             log_message('debug','Unable to write query the file');
        }
	
    }
}