<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Asylum_seekers extends MY_Controller
{
	
	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->library('pagination');
		$this->load->model('applications_model','asylum_seekers');
		$this->load->model('global_model');
		$this->lang->load('applications', $this->Settings->language);
		$this->load->model('Site','site');		
		$this->site->auditLogs();
	} 
	
	/**
	* 
	* COUNSELINGS
	* @return
	*/
	
	public function index()
	{ 
		redirect("asylum_seekers/counseling");
	}
	
	public function counseling()
	{
		$this->data["result"] = "";
		$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('counseling')));
		$meta = array('page_title' => lang('counseling'), 'bc' => $bc);	
		$this->page_construct('asylum_seekers/counseling',$meta, $this->data);	
	}

	public function getCounseling($id = false)
	{		
        $this->load->library('datatables');	
		
		$deleted = "<a href='#' class='delete-counseling po' title='" . lang("delete_counseling_application") . "' data-content=\"<p>"
            . lang('r_u_sure') . "</p><a class='btn btn-danger' href='" . site_url('asylum_seekers/delete_counseling_application/$1') . "'>"
            . lang('i_m_sure') . "</a><button class='btn'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i>"
            . lang('delete_counseling_application') . "</a>";
		
		$approved = "<a href='#' class='approve-counseling po' title='" . lang("approve_counseling") . "' data-content=\"<p>"
            . lang('r_u_sure') . "</p><a class='btn btn-danger' href='" . site_url('asylum_seekers/approve_counseling/$1') . "'>"
            . lang('i_m_sure') . "</a><button class='btn'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-check-square-o\"></i>"
            . lang('approve_counseling') . "</a>";
		
		$rejected = "<a href='#' class='reject-counseling po' title='" . lang("reject_counseling") . "' data-content=\"<p>"
            . lang('r_u_sure') . "</p><a class='btn btn-danger' href='" . site_url('asylum_seekers/reject_counseling/$1') . "'>"
            . lang('i_m_sure') . "</a><button class='btn'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-times\"></i>"
            . lang('reject_counseling') . "</a>";

        $action_link = '<div class="btn-group text-left"><button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">'.lang("actions").'<span class="caret"></span></button>
					        <ul class="dropdown-menu pull-right" role="menu">					        									
								<li><a class="form-counseling" href="'.site_url('asylum_seekers/counseling_application_form/$1').'" ><i class="fa fa-newspaper-o"></i>'.lang('counseling_application_form').'</a></li>								
					            <li>'.$approved.'</li>
								<li>'.$rejected.'</li>
								<li><a class="edit-counseling" href="'.site_url('asylum_seekers/edit_counseling_application/$1').'" ><i class="fa fa-pencil-square-o"></i>'.lang('edit_counseling_application').'</a></li>
								<li><a  data-backdrop="static" data-keyboard="false"  data-toggle="modal" data-target="#myModal" href="'.site_url('asylum_seekers/attach_counseling/$1').'" ><i class="fa fa-plus-circle"></i>'.lang('attachment').'</a></li>	
								<li>'.$deleted.'</li>								
					        </ul>
					    </div>';
		
        $this->datatables->select("
					id as id,
					counseling_no,
					UPPER(firstname) AS firstname,
					UPPER(lastname) AS lastname,
					dob,
					UPPER(gender) AS gender,
					UPPER(nationality) AS nationality,
					phone,
					created_date, 
					status
					", false)
                ->from("fa_counseling")
                ->where("fa_counseling.status <>","deleted");

   //          ->from("fa_rsd_applications")
   //          ->join("fa_counseling", "fa_rsd_applications.counseling_id =  fa_counseling.id", "right")
   //          ->join("fa_family_members", "fa_family_members.id = fa_rsd_applications.counseling_id", "left")
   //          ->join("fa_family_relationship", "fa_family_relationship.id = fa_family_members.relationship", "left")
			// ->where("fa_counseling.status <>","deleted");


		if($this->input->get("counseling_no")) {
			$this->db->where("counseling_no", $this->input->get("counseling_no"));
		}

		if($this->input->get('firstname')) {
			$this->db->where('firstname',$this->input->get('firstname'));
		}

		if($this->input->get("lastname")) {
			$this->db->where("lastname", $this->input->get("lastname"));
		}

		if($this->input->get("nationality")) {
			$this->db->where("nationality", $this->input->get("nationality"));
		}

		if($this->input->get("date_from")) {
			$date_from = $this->input->get("date_from");
			$this->db->where("date(erp_fa_counseling.created_date) >=", $this->erp->fld($date_from));
		}

		if($this->input->get("date_to")) {
			$date_to = $this->input->get("date_to");
			$this->db->where("date(erp_fa_counseling.created_date) <=", $this->erp->fld($date_to));
		}
 
        $this->datatables->add_column("Actions", $action_link, "id");
        echo $this->datatables->generate();
	}
	
	public function add_counseling_application()
	{		
	
		$post = $this->input->post();
		$config = array(
			    array(
                     'field'   => 'counseling_no',
                     'label'   => lang("counseling_no"),
                     'rules'   => 'is_unique[erp_fa_counseling.counseling_no]|required'
                  ),
				array(
                     'field'   => 'firstname_kh',
                     'label'   => lang("firstname_kh"),
                     'rules'   => 'required'
                  ), 
				array(
                     'field'   => 'lastname',
                     'label'   => lang("lastname"),
                     'rules'   => 'required'
                  ),
				array(
                     'field'   => 'lastname_kh',
                     'label'   => lang("lastname"),
                     'rules'   => 'required'
                  ),
				array(
                     'field'   => 'nationality',
                     'label'   => lang("nationality"),
                     'rules'   => 'required'
                  ) ,
				array(
                     'field'   => 'nationality_kh',
                     'label'   => lang("nationality_kh"),
                     'rules'   => 'required'
                  ) ,
            ); 
		$this->form_validation->set_rules($config);
			
		if($post['counseling_no'] != '')
		{
			$this->form_validation->set_rules('counseling_no',lang("counseling_no"),
			'is_unique[erp_fa_counseling.counseling_no]');
		}
		
		$this->erp->checkPermissions('add', null, 'counseling');
		if ($this->form_validation->run() == true) {
			
			/*==============================*/
			
			$config_file = array(
					'upload_path' => 'assets/uploads',					
					'allowed_types' => 'gif|jpg|png|doc|pdf',
				);
			
			$this->load->library('upload', $config_file);
			if($this->upload->do_upload('photo')){
				$file_name = $this->upload->data('file_name');			
			}
			
			/*==============================*/
			
			$config_doc = array(
					'upload_path' => 'assets/uploads',
					'allowed_types' => 'gif|jpg|png|doc|pdf',
				);
			$this->load->library('upload', $config_doc);
			$file_document = $this->upload->data('file_name');
			
			/*==============================*/
			
			// $commune_o = $this->asylum_seekers->addTags($post['commune_o'],'commune');
			// $district_o = $this->asylum_seekers->addTags($post['district_o'],'district');
			$village_o = $this->asylum_seekers->addTags($post['village_o'],'village');
			$village = $this->asylum_seekers->addTags($post['village'],'village');
			
			$data = array(
				"counseling_no" => $post["counseling_no"],
				"firstname" => $post["firstname"],
				"lastname"  =>  $post["lastname"],
				"nationality" => $post["nationality"],
				"nationality_kh" => $post["nationality_kh"],
				"firstname_kh" => $post["firstname_kh"],
				"lastname_kh"  =>  $post["lastname_kh"],
				"nickname_kh" => $post["nickname_kh"],
				"nickname" => $post["nickname"],
				"religion_kh" => $post["religion_kh"],
				"religion" => $post["religion"],
				"ethnicity_kh" => $post["ethnicity_kh"],
				"ethnicity" => $post["ethnicity"],
				"first_arrived_date" => $this->erp->fld($post["first_arrived_date"]),
				"last_entry_date" => $this->erp->fld($post["last_entry_date"]),
				"pob_kh" => $post["pob_kh"],
				"pob" => $post["pob"],
				"travel_documents" => $post["travel_documents"],
				"travel_documents_kh" => $post["travel_documents_kh"],
				"place_of_issued" => $post["place_of_issued"],
				"place_of_issued_kh" => $post["place_of_issued_kh"],
				"issued_date" => $this->erp->fld($post["issued_date"]),
				"expired_date" => $this->erp->fld($post["expired_date"]),
				"dob" => $this->erp->fld($post["dob"]),
				"gender" => $post["gender"],
				"phone" => $post["phone"],
				"original_address" => $post["address_original"],
				"original_address_kh" => $post["address_original_kh"],
				"address" => $post["address"],
				"address_kh" => $post["address_kh"],
				"means_of_transportations" => $post["means_of_transportations"],
				"port_of_entry" => $post["port_of_entry"],
				"port_of_entry_kh" => $post["port_of_entry_kh"],
				"purpose_of_entry" => $post["purpose_of_entry"],
				"purpose_of_entry_kh" => $post["purpose_of_entry_kh"],
				"date_of_entry" => $this->erp->fld($post["date_of_entry"]),
				"photo" => $file_name,
				"attachment_file" => $file_document,
				"status" => "pending",
				"created_by" => $this->session->userdata("user_id"),
				"created_date" => $this->erp->fld(date("d/m/Y H:i")),
				"country" => $post["country"],
				"province" => $post["province"],
				"district" => $post["district"],
				"commune" => $post["commune"],				
				"country_o" => $post["country_o"],
				"state_o" => $post["state_o"],
				"province_o" => $post["province_o"],
				"district_o" => $post["district_o"],
				"commune_o" => $post["commune_o"]
			);
			
			// if($district_o > 0){
			// 	$data["district_o"] = $district_o;
			// }
			// if($commune_o > 0){
			// 	$data["commune_o"] = $commune_o;
			// }
			if($village_o > 0){
				$data["village_o"] = $village_o;
			}
			if($village > 0){
				$data["village"] = $village;
			}
			
			$result = $this->asylum_seekers->addCounseling($data);
			$id = $result; 
			
			if($result){
				$this->session->set_flashdata('message', lang("counseling_application_has_been_saved"));
				redirect("asylum_seekers/edit_counseling_application/".$id);
			}
			
		}else{
			$this->data['states'] = $this->asylum_seekers->getAllStates();
			$this->data['countries'] = $this->asylum_seekers->getAllCountries();
			$this->data['provinces'] = $this->asylum_seekers->getAllProvinces();
			$this->data['communes'] = $this->asylum_seekers->getAllCommunces();
			$this->data['districts'] = $this->asylum_seekers->getAllDistricts(); 
			$this->data['counseling_no'] = $this->asylum_seekers->getReferenceCounselingNo();
			$this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));			
			$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => site_url("asylum_seekers/counseling"), 'page' => lang('counseling')), array('link' => '#', 'page' => lang('counseling_application_form')));
			$meta = array('page_title' => lang('counseling_application_form'), 'bc' => $bc);	
			$this->page_construct('asylum_seekers/add_counseling_application',$meta, $this->data);
		}
	}	
	
	public function edit_counseling_application($id = false)
	{
		
		$post = $this->input->post();
		$config = array( 
				array(
                     'field'   => 'firstname_kh',
                     'label'   => lang("firstname_kh"),
                     'rules'   => 'required'
                  ), 
				array(
                     'field'   => 'lastname',
                     'label'   => lang("lastname"),
                     'rules'   => 'required'
                  ),
				array(
                     'field'   => 'lastname_kh',
                     'label'   => lang("lastname"),
                     'rules'   => 'required'
                  ),
				array(
                     'field'   => 'nationality',
                     'label'   => lang("nationality"),
                     'rules'   => 'required'
                  ) ,
				array(
                     'field'   => 'nationality_kh',
                     'label'   => lang("nationality_kh"),
                     'rules'   => 'required'
                  ) ,
            ); 
			
		$this->erp->checkPermissions('edit', null, 'counseling');
		$this->form_validation->set_rules($config); 
		if ($this->form_validation->run() == true) {
			
			// $commune_o = $this->asylum_seekers->addTags($post['commune_o'],'commune');
			// $district_o = $this->asylum_seekers->addTags($post['district_o'],'district');
			$village_o = $this->asylum_seekers->addTags($post['village_o'],'village');
			$village = $this->asylum_seekers->addTags($post['village'],'village');
			
			$data = array(
				"firstname" => $post["firstname"],
				"lastname"  =>  $post["lastname"],
				"nationality" => $post["nationality"],
				"firstname_kh" => $post["firstname_kh"],
				"lastname_kh"  =>  $post["lastname_kh"],
				"nationality_kh" => $post["nationality_kh"],
				"nickname_kh" => $post["nickname_kh"],
				"nickname" => $post["nickname"],
				"religion_kh" => $post["religion_kh"],
				"religion" => $post["religion"],
				"ethnicity_kh" => $post["ethnicity_kh"],
				"ethnicity" => $post["ethnicity"],
				"first_arrived_date" => $this->erp->fld($post["first_arrived_date"]),
				"last_entry_date" => $this->erp->fld($post["last_entry_date"]),
				"pob_kh" => $post["pob_kh"],
				"pob" => $post["pob"],
				"travel_documents" => $post["travel_documents"],
				"travel_documents_kh" => $post["travel_documents_kh"],
				"place_of_issued" => $post["place_of_issued"],
				"place_of_issued_kh" => $post["place_of_issued_kh"],
				"issued_date" => $this->erp->fld($post["issued_date"]),
				"expired_date" => $this->erp->fld($post["expired_date"]),
				"dob" => $this->erp->fld($post["dob"]),
				"gender" => $post["gender"],
				"phone" => $post["phone"],
				"original_address" => $post["address_original"],
				"original_address_kh" => $post["address_original_kh"],
				"address" => $post["address"],
				"address_kh" => $post["address_kh"],
				"means_of_transportations" => $post["means_of_transportations"],
				"port_of_entry" => $post["port_of_entry"],
				"port_of_entry_kh" => $post["port_of_entry_kh"],
				"purpose_of_entry" => $post["purpose_of_entry"],
				"purpose_of_entry_kh" => $post["purpose_of_entry_kh"],
				"date_of_entry" => $this->erp->fld($post["date_of_entry"]),
				"status" => "pending",
				"updated_by" => $this->session->userdata("user_id"),
				"updated_date" => $this->erp->fld(date("d/m/Y H:i")),
				"country" => $post["country"],
				"province" => $post["province"],
				"district" => $post["district"],
				"commune" => $post["commune"],
				"country_o" => $post["country_o"],
				"state_o" => $post["state_o"],
				"province_o" => $post["province_o"],
				"district_o" => $post["district_o"],
				"commune_o" => $post["commune_o"]
			);
			
			// if($district_o > 0){
			// 	$data["district_o"] = $district_o;
			// }
			// if($commune_o > 0){
			// 	$data["commune_o"] = $commune_o;
			// }
			if($village_o > 0){
				$data["village_o"] = $village_o;
			}
			if($village > 0){
				$data["village"] = $village;
			}
			
			/*==============================*/			
			
			$config_file = array(
					'upload_path' => 'assets/uploads',
					'allowed_types' => 'gif|jpg|png|doc|pdf',
				);
			$this->load->library('upload', $config_file);
			if($this->upload->do_upload('userfile')){
				$data["photo"] = $this->upload->data('file_name');
			}
			$source_image = $this->upload->data();
			$config_resize['image_library'] = 'gd2';
			$config_resize['source_image'] = $source_image["full_path"];
			$config_resize['create_thumb'] = FALSE;
			$config_resize['maintain_ratio'] = FALSE;
			$config_resize['width']         = 103;
			$config_resize['height']       = 140;
			$this->load->library('image_lib', $config_resize);
			$this->image_lib->resize();
			
			/*==============================*/
			$config_doc = array(
					'upload_path' => 'assets/uploads',
					'allowed_types' => 'gif|jpg|png|doc|pdf',
				);
			$this->load->library('upload', $config_doc);
			if($this->upload->do_upload('document')){
				$data["attachment_file"] = $this->upload->data('file_name');
			}
			/*==============================*/
			
			$result = $this->asylum_seekers->updateCounseling($id,$data);
			if($result){
				$this->session->set_flashdata('message', lang("counseling_application_has_been_updated"));
				redirect("asylum_seekers/edit_counseling_application/".$id);
			}
			
		}else{			
			$this->data['error'] .= (validation_errors() ? validation_errors() : $this->session->flashdata('error'));			
			$result = $this->asylum_seekers->getCounselingById($id);
			// $this->erp->print_arrays($result);
			$this->data['result'] = $result;
			$this->data['states'] = $this->asylum_seekers->getAllStates();
			$this->data['countries'] = $this->asylum_seekers->getAllCountries();
			$this->data['provinces'] = $this->asylum_seekers->getAllProvinces();
			$this->data['communes'] = $this->asylum_seekers->getAllCommunces();
			$this->data['districts'] = $this->asylum_seekers->getAllDistricts();
			$this->data['result_members'] = $this->asylum_seekers->getFamilyMemberAccompanyingByCounseling($result->id);
			$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => site_url("asylum_seekers/counseling"), 'page' => lang('counseling')), array('link' => '#', 'page' => lang('edit_counseling_application')));
			$meta = array('page_title' => lang('edit_counseling_application'), 'bc' => $bc);	
			$this->page_construct('asylum_seekers/edit_counseling_application',$meta, $this->data);
		}
	}
	
	public function delete_counseling_application($id = false)
	{
			$this->erp->checkPermissions('delete', null, 'counseling');
		if($id){ 
			$result = $this->asylum_seekers->deleteCounseling($id);
			if($result){
				$this->session->set_flashdata('message', lang("counseling_application_deleted"));
				redirect("asylum_seekers/counseling");
			}else{
				$this->session->set_flashdata('error', lang("counseling_application_cannot_delete"));
				redirect("asylum_seekers/counseling");
			}
		}
	}
	
	public function counseling_application_form($id = false)
	{
		$this->counseling_application_form_1($id);
	}
	
	public function counseling_application_form_2($id = false)
	{		
		$this->erp->checkPermissions('application_form', null, 'counseling');
		$result = $this->asylum_seekers->getCounselingById($id);
		$this->data['result'] = $result;
		$this->data['result_members'] = $this->asylum_seekers->getCounselingMemberId($result->id);	
		$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('counseling_application_form')));
		$meta = array('page_title' => lang('counseling_application_form'), 'bc' => $bc);	
		$this->page_construct('asylum_seekers/counseling_application_form',$meta, $this->data);
	}
	
	public function counseling_application_form_1($id = false)
	{
		$this->erp->checkPermissions('application_form', null, 'counseling');
		$result = $this->asylum_seekers->getCounselingById($id);
		$this->data['result'] = $result;
		
		$this->data['province'] = $this->asylum_seekers->getProvinceById($result->province);
		$this->data['commune'] = $this->asylum_seekers->getCommunceById($result->commune);
		$this->data['country'] = $this->asylum_seekers->getCountryById($result->country);
		$this->data['district'] = $this->asylum_seekers->getDistrictById($result->district);
		
		$this->data['province_o'] = $this->asylum_seekers->getProvinceById($result->province_o);
		$this->data['commune_o'] = $this->asylum_seekers->getTagsById($result->commune_o);
		$this->data['village_o'] = $this->asylum_seekers->getTagsById($result->village_o);
		$this->data['country_o'] = $this->asylum_seekers->getCountryById($result->country_o);
		$this->data['district_o'] = $this->asylum_seekers->getTagsById($result->district_o);
		$this->data['state_o'] = $this->asylum_seekers->getStateById($result->state_o);
		
		$this->data['result_members'] = $this->asylum_seekers->getFamilyMemberAccompanyingByCounseling($result->id);	
		$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('counseling_application_form')));
		$meta = array('page_title' => lang('counseling_application_form'), 'bc' => $bc);	
		$this->page_construct('asylum_seekers/counseling_application_form_1',$meta, $this->data);
	}
	
	public function approve_counseling($id = false)
	{
		$this->erp->checkPermissions('approve', null, 'counseling');
		if($id){
			$data = array(
							"status"	=> "approved",
							"approved_by" => $this->session->userdata("user_id"),
						 );
			$result = $this->db->update("fa_counseling", $data, array("id"=>$id));
			if($result){
				$this->session->set_flashdata('message', lang("counseling_application_approved"));
				redirect("asylum_seekers/counseling");
			}else{
				$this->session->set_flashdata('error', lang("counseling_application_cannot_approved"));
				redirect("asylum_seekers/counseling");
			}
		}
	}
	
	public function reject_counseling($id = false)
	{
		  if($id){
			$data = array(
							"status"	=> "rejected",
							"rejected_by" => $this->session->userdata("user_id"),
						 );
			$result = $this->db->update("fa_counseling", $data, array("id"=>$id));
			if($result){
				$this->session->set_flashdata('message', lang("counseling_application_rejected"));
				redirect("asylum_seekers/counseling");
			}else{
				$this->session->set_flashdata('error', lang("counseling_application_cannot_rejected"));
				redirect("asylum_seekers/counseling");
			}
		}
	}
	
	
	public function attach_counseling( $id =  false ) 
	{
		 
		$result = $this->asylum_seekers->getCounselingById($id);
		$this->erp->checkPermissions('attachment', null, 'counseling');
		$this->global_model->attach_forms('fa_counseling', $id, 'counseling');
		$this->data['action_method'] = 'attach_counseling';
		$this->data['delete_url'] = 'delete_attach_counseling';
		$this->data['view_folder'] = 'counseling';
		$this->data['contorler'] = $this->uri->segment(1);
		$this->data['id'] = $id;
		$this->data['result'] = $result; 
		$this->data['modal_js'] = $this->site->modal_js();		
		$this->load->view($this->theme . 'global/view_attach_forms', $this->data);	
	} 
	
	public function delete_attach_counseling($id = false, $name = null,$img=null) 
	{
		$result = $this->asylum_seekers->getCounselingById($id);
		$this->global_model->delete_attach_forms('fa_counseling', $id, $name, $img);
	}  
	
	/**
	* 
	* @param undefined $id
	* MEMBERS
	* @return
	*/
	
	public function add_member($id = false)
	{
		$post = $this->input->post(); 
		if($post == true){
			   $village = $this->asylum_seekers->addTags($post['village'],'village');
			$data = array(
							"counseling_id" => $id,
							"firstname" => $post['firstname'],
							"firstname_kh" => $post['firstname_kh'],
							"lastname" => $post['lastname'],
							"lastname_kh" => $post['lastname_kh'],
							"gender" => $post['gender'],
							"occupation_kh" => $post['occupation_kh'],
							"occupation" => $post['occupation'],
							"education_kh" => $post['education_kh'],
							"education" => $post['education'],
							"nationality_kh" => $post['nationality_kh'],
							"ethnicity" => $post['ethnicity'],
							"ethnicity_kh" => $post['ethnicity_kh'],
							"nationality" => $post['nationality'],
							"dob" => $this->erp->fld($post['dob']),
							"relationship" => $post['relationship'],
							
							"address" => $post['address'],  
							"address_kh" => $post['address_kh'],  
							"country" =>  $post['country'],
							"province" => $post['province'],
							"district" => $post['district'],
							"commune" =>  $post['commune'],
							"village"    => $village,
							 
							"pob_kh" => $post['pob_kh'],
							"pob" => $post['pob'],
						);
			
			if(isset($_FILES['userfile'])){
				$config_file = array(
						'upload_path' => 'assets/uploads/members',
						'allowed_types' => 'gif|jpg|png|doc|pdf',
					);
				$this->load->library('upload', $config_file);
				if($this->upload->do_upload('userfile')){
					$data["photo"] = $this->upload->data('file_name');
				}
			}
			$result = $this->db->insert("fa_family_members",$data);
			if($result){
				$this->session->set_flashdata('message', lang("member_been_added"));
				redirect("asylum_seekers/edit_counseling_application/".$id);
			}
		}else{
			 
			$this->data['countries'] = $this->asylum_seekers->getAllCountries();
			$this->data['provinces'] = $this->asylum_seekers->getAllProvinces();
			$this->data['communes'] = $this->asylum_seekers->getAllCommunces();
			$this->data['districts'] = $this->asylum_seekers->getAllDistricts();
			 
			$this->data['relationship']=$this->asylum_seekers->getRelationship();
			$this->data['id'] = $id;
			$this->data['users'] = $this->asylum_seekers->getUsers();
			$this->data['modal_js'] = $this->site->modal_js();		
			$this->load->view($this->theme . 'asylum_seekers/add_member', $this->data);
		} 	
	}
	
	public function edit_member($id = false, $counseling_id = false)
	{
		$post = $this->input->post(); 
		if($post == true){
			$village = $this->asylum_seekers->addTags($post['village'],'village');
			$data = array(
							"firstname" => $post['firstname'],
							"firstname_kh" => $post['firstname_kh'],
							"lastname" => $post['lastname'],
							"lastname_kh" => $post['lastname_kh'],
							"gender" => $post['gender'],
                            "occupation_kh" => $post['occupation_kh'],
							"occupation" => $post['occupation'],
							"education_kh" => $post['education_kh'],
							"education" => $post['education'],
							"ethnicity" => $post['ethnicity'],
							"ethnicity_kh" => $post['ethnicity_kh'],
							"nationality_kh" => $post['nationality_kh'],
							"nationality" => $post['nationality'],
							"dob" => $this->erp->fld($post['dob']),
							"relationship" => $post['relationship'],
							
							"address" => $post['address'],  
							"address_kh" => $post['address_kh'],  
							"country" =>  $post['country'],
							"province" => $post['province'],
							"district" => $post['district'],
							"commune" =>  $post['commune'],
							"village"    => $village,
							 
							"pob_kh" => $post['pob_kh'],
							"pob" => $post['pob'],
						);

			if(isset($_FILES['userfile'])){
				$config_file = array(
						'upload_path' => 'assets/uploads/members',
						'allowed_types' => 'gif|jpg|png|doc|pdf',
					);
				$this->load->library('upload', $config_file);
				if($this->upload->do_upload('userfile')){
					$data["photo"] = $this->upload->data('file_name');
				}
			}
			
			$result = $this->db->where(array("id"=>$id,"counseling_id"=>$counseling_id))->update("fa_family_members",$data);
			if($result){
				$this->session->set_flashdata('message', lang("member_been_updated"));
				redirect("asylum_seekers/edit_counseling_application/".$counseling_id);
			}		
		}else{
			
			$this->data['countries'] = $this->asylum_seekers->getAllCountries();
			$this->data['provinces'] = $this->asylum_seekers->getAllProvinces();
			$this->data['communes'] = $this->asylum_seekers->getAllCommunces();
			$this->data['districts'] = $this->asylum_seekers->getAllDistricts();
			 
			$this->data['relationship']=$this->asylum_seekers->getRelationship();
			$this->data['member'] = $this->asylum_seekers->getMemberId($id);
			$this->data['id'] = $id;
			$this->data['counseling_id'] = $counseling_id;
			$this->data['users'] = $this->asylum_seekers->getUsers();
			$this->data['modal_js'] = $this->site->modal_js();		
			$this->load->view($this->theme . 'asylum_seekers/edit_member', $this->data);
		} 	
	} 
	
	public function delete_member($id)
	{		
		if($id){
			$result = $this->db->where("id",$id)->delete("fa_family_members");
			if($result){
				$this->session->set_flashdata('message', lang("member_been_deleted"));
				redirect($_SERVER['HTTP_REFERER']);
			}	
		} 
	}	
	
	/**
	* 
	* @param undefined $id
	* WITHDRAWAL ASYLUMS
	* @return
	*/

	public function withdrawal_asylum()
	{
		$this->erp->checkPermissions('index', null, 'withdrawal_of_asylum');
		$this->data["result"] = "";
		$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('withdrawal_asylum')));
		$meta = array('page_title' => lang('withdrawal_asylum'), 'bc' => $bc);
		$this->page_construct('asylum_seekers/withdrawal_asylum',$meta, $this->data);
	}

	public function getWithdrawalofAsylum()
	{
		$this->load->library('datatables');
		
		$delete = "<a href='#' class='delete-withdrawal po' title='" . lang("delete_withdrawal_asylum") . "' data-content=\"<p>"
            . lang('r_u_sure') . "</p><a class='btn btn-danger' href='" . site_url('asylum_seekers/delete_withdrawal_asylum/$1') . "'>"
            . lang('i_m_sure') . "</a><button class='btn'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i>"
            . lang('delete_withdrawal_asylum') . "</a>";

        $action_link = '<div class="btn-group text-left"><button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">'.lang("actions").'<span class="caret"></span></button>
					        <ul class="dropdown-menu pull-right" role="menu">					            
								<li><a href="'.site_url('asylum_seekers/withdrawal_of_asylum_application_form/$1').'" ><i class="fa fa-pencil-square-o"></i>'.lang('withdrawal_of_asylum_application_form').'</a></li>
					            <li><a data-backdrop="static" data-keyboard="false"  data-toggle="modal" data-target="#myModal"  href="'.site_url('asylum_seekers/edit_withdrawal_asylum/$1').'" ><i class="fa fa-pencil-square-o"></i>'.lang('edit_withdrawal_asylum').'</a></li>
								<li><a  data-backdrop="static" data-keyboard="false"  data-toggle="modal" data-target="#myModal" href="'.site_url('asylum_seekers/attach_withdrawal_asylum/$1').'" ><i class="fa fa-plus-circle"></i>'.lang('attachment').'</a></li>	
								<li>'.$delete.'</li>
					        </ul>
					    </div>';

        $this->datatables->select("
					fa_asylum_withdrawal.id as id,
					CONCAT(case_prefix,case_no) as case_no,
					UPPER(firstname) AS firstname,
					UPPER(lastname) AS lastname,
					dob,
					UPPER(gender) AS gender,
					UPPER(nationality) AS nationality,
					fa_asylum_withdrawal.created_date,
					fa_asylum_withdrawal.status
					", false)
            ->from("fa_rsd_applications")
			->join("fa_asylum_withdrawal","application_id=fa_rsd_applications.id","right")
            ->where("fa_asylum_withdrawal.status <> 'deleted'");

        $this->datatables->add_column("Actions", $action_link, "id");
        echo $this->datatables->generate();
	}
	
	public function add_withdrawal_asylum()
	{ 
		$post = $this->input->post();
		if($post == true){
			$this->erp->checkPermissions('add', null, 'withdrawal_of_asylum');
			$data = array(
				"application_id" => $post['application_id'],
				"reasons" => $post['reasons'],
				"created_by" => $this->session->userdata("user_id"),
				"created_date" => $this->erp->fld(date("d/m/Y H:i")),
				"status"  => 'active'
			); 			
			$config_file = array(
					'upload_path' => 'assets/uploads/document/withdrawal_asylum',
					'allowed_types' => 'gif|jpg|png|doc|pdf',
				);
			$this->load->library('upload', $config_file);
			if($this->upload->do_upload('attachment')){
				$data["attachment"] = $this->upload->data('file_name');
			}			
			$config1 = array(
					'upload_path' => 'assets/uploads',
					'allowed_types' => 'gif|jpg|png'
				); 
				
			$this->load->library('upload', $config1);
			if($this->upload->do_upload('photo')){  
				$file_name = $this->upload->data('file_name');  
				$data['photo'] = $file_name;
			}
			if(isset($post['photoHidden'])) {
				$data['photo'] = $post['photoHidden']; 
			}			
			if($this->asylum_seekers->addWithdrawalAsylumSeeker($data)){
				$this->session->set_flashdata('message', lang("withdrawal_asylum_saved"));
				redirect("asylum_seekers/withdrawal_asylum");
			}else {
				$this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
				redirect("asylum_seekers/withdrawal_asylum");
			}			
		}			
		$this->data['id'] = $id;
		$this->data['modal_js'] = $this->site->modal_js();
		$this->load->view($this->theme . 'asylum_seekers/add_withdrawal_asylum', $this->data);
	}
	
	public function edit_withdrawal_asylum($id = false,$widrawal_id =false)
	{		
		
		$post = $this->input->post();
		if($post == true){
			$this->erp->checkPermissions('edit', null, 'withdrawal_of_asylum');
			$data = array(			
				"reasons" => $post['reasons'],
				"created_by" => $this->session->userdata("user_id"),
				"created_date" => $this->erp->fld(date("d/m/Y H:i")),
			);
			$config_file = array(
					'upload_path' => 'assets/uploads/document/withdrawal_asylum',
					'allowed_types' => 'gif|jpg|png|doc|pdf',
				);
			$this->load->library('upload');
			$this->load->initialize($config_file);
			if($this->upload->do_upload('attachment')){
				$data["attachment"] = $this->upload->data('file_name');
			} 
			 
			if(isset($_FILES['photo'])){
				$config_file1 = array(
						'upload_path' => 'assets/uploads/',
						'allowed_types' => 'gif|jpg|png',
					); 
				$this->upload->initialize($config_file1);
				if($this->upload->do_upload('photo')){
					$data["photo"] = $this->upload->data('file_name');
				}
			}
			 
			$result = $this->asylum_seekers->updateWithdrawalAsylumSeeker($widrawal_id, $data); 
			if($result){
				$this->session->set_flashdata('message', lang("withdrawal_asylum_edited"));
				redirect("asylum_seekers/withdrawal_asylum");
			}
		}
		$this->data['withdrawal'] = $this->asylum_seekers->getWithdrawalAsylumSeekrById($id);
		$this->data["result"] = $this->asylum_seekers->getRSDApplicationById($this->data['withdrawal']->application_id);
		$this->data['id'] = $id;
		$this->data['modal_js'] = $this->site->modal_js();
		$this->load->view($this->theme . 'asylum_seekers/edit_withdrawal_asylum', $this->data);

	}

	public function delete_withdrawal_asylum($id = false)
	{
		$this->erp->checkPermissions('delete', null, 'withdrawal_of_asylum');
		if($id){			
			if($this->asylum_seekers->deleteWithdrawalAsylumSeeker($id)){
				$this->session->set_flashdata('message', lang("withdrawal_deleted"));
				redirect("asylum_seekers/withdrawal_asylum");
			}
		}

	}
	
	public function withdrawal_of_asylum_application_form($id = false)
	{	
		$this->erp->checkPermissions('application_form', null, 'withdrawal_of_asylum');
		$this->data['withdrawal'] = $this->asylum_seekers->getWithdrawalAsylumSeekrById($id);				
		$this->data["result"] = $this->asylum_seekers->getRSDApplicationById($this->data['withdrawal']->application_id);
		$this->data["members"] = $this->asylum_seekers->getFamilyMemberAccompanyingByCounseling($this->data["result"]->counseling_id);
			
		$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('withdrawal_of_asylum_application_form')));
		$meta = array('page_title' => lang('withdrawal_of_asylum_application_form'), 'bc' => $bc);
		$this->page_construct('asylum_seekers/withdrawal_of_asylum_application_form',$meta, $this->data);
	}
	
	
	public function attach_withdrawal_asylum($id = false) 
	{ 
		$this->erp->checkPermissions('attachment', null, 'withdrawal_of_asylum');
		$result = $this->asylum_seekers->getWithdrawalAsylumSeekrById($id); 
		$this->global_model->attach_forms('fa_asylum_withdrawal', $id, 'withdrawal_asylum');
		$this->data['action_method'] = 'attach_withdrawal_asylum';
		$this->data['delete_url'] = 'delete_attach_withdrawal_asylum';
		$this->data['view_folder'] = 'withdrawal_asylum';
		$this->data['contorler'] = $this->uri->segment(1);
		$this->data['id'] = $id;
		$this->data['result'] = $result; 
		$this->data['modal_js'] = $this->site->modal_js();		
		$this->load->view($this->theme . 'global/view_attach_forms', $this->data);
		
	}
	 
	public function delete_attach_withdrawal_asylum($id = false, $name = null, $img = null) 
	{
		$this->erp->checkPermissions('delete', null, 'withdrawal_of_asylum');
		$this->global_model->delete_attach_forms('fa_asylum_withdrawal', $id, $name, $img);
	}    
	 
	
	/**
	* 
	* RSD Application
	* @return
	*/

	public function rsd_application()
	{
		$post = $this->input->post();
		$this->data["result"] = "";
		$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('rsd_application')));
		$meta = array('page_title' => lang('rsd_application'), 'bc' => $bc);	
		$this->page_construct('asylum_seekers/rsd_applications',$meta, $this->data);
	}
	
	public function rsd_application_form($id = false)
	{ 
		$this->erp->checkPermissions('application_form', null, 'rsd_applications');
		$this->data['application'] = $this->asylum_seekers->getRSDApplicationById($id);	
		$counseling_id = $this->data['application']->counseling_id;
		$this->data["result"] =  $this->asylum_seekers->getRSDApplicationById($id);
		$this->data["office"] = $this->asylum_seekers->getallOfficeById($this->data["result"]->refugee_office_id);
		$this->data["education"] =  $this->asylum_seekers->getEducationByCounseling($counseling_id);		
		$this->data["occupation"] =  $this->asylum_seekers->getOccupationByCounseling($counseling_id); 
		$this->data["family_member"] =  $this->asylum_seekers->getFamilyMemberAccompanyingByCounseling($counseling_id);
		$this->data["family_member_none_accom"] =  $this->asylum_seekers->getFamilyMemberNoneAccompanyingByCounseling($counseling_id);
		$this->data["identification_documents"] =  $this->asylum_seekers->getIdentificationDocumentByCounseling($counseling_id);
		$this->data["country_transit"] =  $this->asylum_seekers->getCountryTransitByCounseling($counseling_id);
		$this->data["family_member_none_accom_outsite_country"] =  $this->asylum_seekers->getFamilyMemberNoneAccompanyingOutsideCountryByCounseling($counseling_id);		
		
		$this->data['province'] = $this->asylum_seekers->getProvinceById($this->data["result"]->province);
		$this->data['commune'] = $this->asylum_seekers->getCommunceById($this->data["result"]->commune);
		$this->data['country'] = $this->asylum_seekers->getCountryById($this->data["result"]->country);
		$this->data['district'] = $this->asylum_seekers->getDistrictById($this->data["result"]->district);
		
		$this->data['province_o'] = $this->asylum_seekers->getProvinceById($this->data["result"]->province_o);
		$this->data['commune_o'] = $this->asylum_seekers->getCommunceById($this->data["result"]->commune_o);
		$this->data['country_o'] = $this->asylum_seekers->getCountryById($this->data["result"]->country_o);
		$this->data['district_o'] = $this->asylum_seekers->getDistrictById($this->data["result"]->district_o);
		
		$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('rsd_application_form')));
		$meta = array('page_title' => lang('rsd_application_form'), 'bc' => $bc);	
		$this->page_construct('asylum_seekers/rsd_application_form',$meta, $this->data); 
	}
	
	public function getAsylumSeekerWithdrawalList($id = false)
	{		
		
		$hidden = "";
		if($this->input->get('k') =="3") {	
			$hidden = "hidden";
		} 
		
        $this->load->library('datatables');	
		$delete = "<a href='#' class='delete-sm po' title='" . lang("delete_rsd_application") . "' data-content=\"<p>"
            . lang('r_u_sure') . "</p><a class='btn btn-danger' href='" . site_url('asylum_seekers/delete_rsd_application/$1') . "'>"
            . lang('i_m_sure') . "</a><button class='btn'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i>"
            . lang('delete_rsd_application') . "</a>";
		
        $action_link = '<div class="btn-group text-left '.$hidden.'"><button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">'.lang("actions").'<span class="caret"></span></button>
					        <ul class="dropdown-menu pull-right" role="menu">					        																
								<li><a target="_blank" href="'.site_url('asylum_seekers/rsd_application_form/$1').'" ><i class="fa fa-newspaper-o"></i>'.lang('rsd_application_form').'</a></li>								
								<li><a href="'.site_url('asylum_seekers/edit_rsd_application/$1').'" ><i class="fa fa-pencil-square-o"></i>'.lang('edit_rsd_application').'</a></li>														
					            <li><a  data-backdrop="static" data-keyboard="false"  data-toggle="modal" data-target="#myModal" href="'.site_url('asylum_seekers/attach_rsd_application/$1').'" ><i class="fa fa-plus-circle"></i>'.lang('attachment').'</a></li>	
								<li>'.$delete.'</li>								
					        </ul>
					    </div>';
		
        $this->datatables->select("
					fa_rsd_applications.id as id,
					CONCAT(case_prefix,case_no) as case_no,
					UPPER(firstname) AS firstname,
					UPPER(lastname) AS lastname,
					dob,
					UPPER(gender) AS gender,
					UPPER(nationality) AS nationality,
					fa_offices.office,
					fa_rsd_applications.created_date,
					fa_rsd_applications.status,
					CONCAT(SUM(erp_fa_interviews.is_appeal),'-',erp_fa_interview_evaluations.status)
					", false)
            ->from("fa_rsd_applications")
			->join("fa_offices","fa_offices.id=fa_rsd_applications.refugee_office_id","left")
			->join("fa_interview_evaluations","fa_rsd_applications.id=fa_interview_evaluations.application_id","left")
			->join("fa_interviews","fa_interview_evaluations.interview_id=fa_interviews.id","left")
			->where("fa_rsd_applications.status =","inactive")
			->where("date(erp_fa_rsd_applications.created_date) >= date(NOW()) - INTERVAL 15 DAY");
		if($this->input->get("type") == "notification"){ 
			$this->datatables->where("erp_fa_rsd_applications.status <> 'closed'
									AND erp_fa_rsd_applications.status <> 'inactive'
									AND erp_fa_rsd_applications.id 
									NOT IN (SELECT application_id FROM erp_fa_interview_appointments)");
		} 
		$this->datatables->group_by("fa_rsd_applications.id");
        $this->datatables->add_column("Actions", $action_link, "id");
        echo $this->datatables->generate();
	}
	
	public function getRSDApplications($id = false)
	{		
	
		$hidden = $_GET['k']=='k4'?'hidden':'';
        $this->load->library('datatables');	
		$delete = "<a href='#' class='delete-sm po' title='" . lang("delete_rsd_application") . "' data-content=\"<p>"
            . lang('r_u_sure') . "</p><a class='btn btn-danger' href='" . site_url('asylum_seekers/delete_rsd_applications/$1') . "'>"
            . lang('i_m_sure') . "</a><button class='btn'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i>"
            . lang('delete_rsd_application') . "</a>";
		
        $action_link = '<div class="btn-group text-left '.$hidden.'"><button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">'.lang("actions").'<span class="caret"></span></button>
					        <ul class="dropdown-menu pull-right" role="menu">					        																
								<li><a target="_blank" href="'.site_url('asylum_seekers/rsd_application_form/$1').'" ><i class="fa fa-newspaper-o"></i>'.lang('rsd_application_form').'</a></li>								
								<li><a href="'.site_url('asylum_seekers/edit_rsd_application/$1').'" ><i class="fa fa-pencil-square-o"></i>'.lang('edit_rsd_application').'</a></li>														
					            <li><a  data-backdrop="static" data-keyboard="false"  data-toggle="modal" data-target="#myModal" href="'.site_url('asylum_seekers/attach_rsd_application/$1').'" ><i class="fa fa-plus-circle"></i>'.lang('attachment').'</a></li>	
								<li>'.$delete.'</li>								
					        </ul>
					    </div>';
		if($_GET['k']=='k4'){
			$this->datatables->select("
					fa_rsd_applications.id as id,
					CONCAT(case_prefix,case_no) as case_no,
					UPPER(firstname) AS firstname,
					UPPER(lastname) AS lastname,
					dob,
					UPPER(gender) AS gender,
					UPPER(nationality) AS nationality,
					fa_offices.office,
					CONCAT(IFNULL(CONCAT(erp_fa_rsd_applications.address,' , '),''),
								IFNULL(CONCAT(erp_fa_communces.native_name,' , '),''),
								IFNULL(CONCAT(erp_fa_districts.native_name,' , '),''),
								IFNULL(CONCAT(erp_fa_provinces.native_name,' , '),''),
								IFNULL(CONCAT(erp_fa_countries.native_name,' , '),'') 
							),
					fa_rsd_applications.phone,
					fa_rsd_applications.created_date,
					fa_rsd_applications.status,
					fa_rsd_applications.level, 
					", false);
		}else {
			$this->datatables->select("
					fa_rsd_applications.id as id,
					CONCAT(case_prefix,case_no) as case_no,
					UPPER(firstname) AS firstname,
					UPPER(lastname) AS lastname,
					dob,
					UPPER(gender) AS gender,
					UPPER(nationality) AS nationality,
					fa_offices.office,
					fa_rsd_applications.created_date,
					fa_rsd_applications.status,
					fa_rsd_applications.level, 
					", false);
		}
        $this->datatables->from("fa_rsd_applications")
						->join("fa_offices","fa_offices.id=fa_rsd_applications.refugee_office_id","left") 
						->join("fa_countries","fa_countries.id=fa_rsd_applications.country","left") 
						->join("fa_provinces","fa_provinces.id=fa_rsd_applications.province","left")  
						->join("fa_districts","fa_districts.id=fa_rsd_applications.district","left")  
						->join("fa_communces","fa_communces.id=fa_rsd_applications.commune","left")  
						->where("fa_rsd_applications.status <>","deleted");
		if($this->input->get("type") == "notification"){ 
			$this->datatables->where("erp_fa_rsd_applications.status <> 'closed'
									AND erp_fa_rsd_applications.status <> 'inactive'
									AND erp_fa_rsd_applications.id 
									NOT IN (SELECT application_id FROM erp_fa_interview_appointments)");
		}  
        $this->datatables->add_column("Actions", $action_link, "id");
        echo $this->datatables->generate();
	}
	
	public function add_rsd_application($id = false)
	{
		$this->data['counseling'] = $this->asylum_seekers->getCounselingById($id);
		$this->erp->checkPermissions('add', null, 'rsd_applications');
		$post = $this->input->post(); 
			$config = array(
			  array(
                     'field'   => 'firstname',
                     'label'   => lang("firstname"),
                     'rules'   => 'required|is_unique[erp_fa_counseling.counseling_no]'
                  ),
               array(
                     'field'   => 'firstname',
                     'label'   => lang("firstname"),
                     'rules'   => 'required'
                  ),
               array(
                     'field'   => 'lastname',
                     'label'   => lang("firstname"),
                     'rules'   => 'required'
                  ),
               array(
                     'field'   => 'nationality',
                     'label'   => lang("nationality"),
                     'rules'   => 'required'
                  ),
               array(
                     'field'   => 'ethnicity',
                     'label'   => lang("ethnicity"),
                     'rules'   => 'required'
                  ),
               array(
                     'field'   => 'full_case',
                     'label'   => lang("case_no"),
                     'rules'   => 'required|is_unique[erp_fa_rsd_applications.full_case]'
                  ),
               array(
                     'field'   => 'counseling_no',
                     'label'   => lang("counseling_no"),
                     'rules'   => 'required'
                  )
            );
			
		$this->form_validation->set_rules($config);
		if ($this->form_validation->run() == true) {
			
			$data = array(
			  'refugee_office_id'=> $post['refugee_office'],
			  'counseling_id' => $post['counseling_id'],
			  "case_prefix" => $post['case_prefix'],
			  'case_no'=> $post['case_no'],
			  'full_case'=> $post['case_prefix'].$post['case_no'], 
			  'arrived_date'=> $this->erp->fld($post['arrived_date']),
			  'registered_date'=> $this->erp->fld($post['reg_date']),
			  'related_case'=> $post['related_case'],
			  'special_need'=> $post['special_need'],
			  'firstname'=> $post['firstname'],
			  'firstname_kh'=> $post['firstname_kh'],
			  'lastname'=> $post['lastname'],
			  'lastname_kh'=> $post['lastname_kh'],
			  'othername'=> $post['othername'],
			  'othername_kh'=> $post['othername_kh'],
			  'fathername'=> $post['fathername'],
			  'fathername_kh'=> $post['fathername_kh'],
			  'mothername'=> $post['mothername'],
			  'mothername_kh'=> $post['mothername_kh'],
			  'gender'=> $post['gender'],
			  'nationality'=> $post['nationality'],
			  'nationality_kh'=> $post['nationality_kh'],
			  'dob'=> $this->erp->fld($post['dob']),
			  'pob'=> $post['pob'],
			  'pob_kh'=> $post['pob_kh'],
			  'marital_status'=> $post['marital_status'],
			  'spouse_name'=> $post['spouse'],
			  'spouse_name_kh'=> $post['spouse_kh'],
			  'religion'=> $post['religion'],
			  'religion_kh'=> $post['religion_kh'],
			  'ethnicity'=> $post['ethnicity'],
			  'ethnicity_kh'=> $post['ethnicity_kh'],
			  'address_o_kh'=> $post['address_o_kh'],
			  'address_o'=> $post['address_o'],
			  'address_kh'=> $post['address_kh'],
			  'address'=> $post['address'],
			  'phone'=> $post['phone'],
			  'status'=> 'active',
			  "created_by" => $this->session->userdata("user_id"),
			  "created_date" => $this->erp->fld(date("d/m/Y H:i")), 
			  "country" => $post["country"],
			  "province" => $post["province"],
			  "district" => $post["district"],
			  "commune" => $post["commune"],
			  "country_o" => $post["country_o"],
			  "province_o" => $post["province_o"],
			  "district_o" => $post["district_o"],
			  "commune_o" => $post["commune_o"],
			  "procedure_sequence" => "registered",
			);
			
			if($_FILES['photo']['size'] > 0) {
				$config = array(
					'upload_path' => 'assets/uploads',					
					'allowed_types' => 'gif|jpg|png|doc|pdf',
					);
				
				$this->load->library('upload', $config);
				if($this->upload->do_upload('photo')){
					$file_name = $this->upload->data('file_name');
					$data['photo'] = $file_name; 
				}
			} else{ 
					$data['photo'] = $this->data['counseling']->photo; 
			} 
			if(!$id){
				$this->session->set_flashdata('error', lang("is_required"));
				redirect("asylum_seekers/add_rsd_application/".$id);
			} 
			
			$counseling = $this->asylum_seekers->updateCounseling($data['counseling_id'],array("dob"=>$data['dob'])) ;
			$application_id = $this->asylum_seekers->addRSDApplication($data);
			if($application_id && $counseling ){  
				$this->asylum_seekers->updateReference("so");
				$this->session->set_flashdata('message', lang("rsd_applications_added"));
				redirect("asylum_seekers/edit_rsd_application/".$application_id);
			}
			
		}
		else{
			$this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
			$this->data['id'] = $id;
			$this->data['case_prefix'] = $this->asylum_seekers->getCasePrefix();
			$this->data['case_no'] = $this->asylum_seekers->getReference("so");
			$this->data["educations"] =  $this->asylum_seekers->getEducationByCounseling($id);			
			$this->data["family_member_accompanying"] =  $this->asylum_seekers->getFamilyMemberAccompanyingByCounseling($id);
			$this->data["family_member_none_accompanying"] =  $this->asylum_seekers->getFamilyMemberNoneAccompanyingByCounseling($id);
			$this->data["family_member_none_accompanying_outside_country"] =  $this->asylum_seekers->getFamilyMemberNoneAccompanyingOutsideCountryByCounseling($id);
			$this->data["country_transit"] =  $this->asylum_seekers->getCountryTransitByCounseling($id);
			$this->data["occupation"] =  $this->asylum_seekers->getOccupationByCounseling($id);
			$this->data["identification"] =  $this->asylum_seekers->getIdentificationDocumentByCounseling($id);
		
			$this->data['offices'] = $this->asylum_seekers->getallOffices();			
			$this->data['applications'] = $this->asylum_seekers->getAllRSDApplications();
			
			$this->data['countries'] = $this->asylum_seekers->getAllCountries();
			$this->data['provinces'] = $this->asylum_seekers->getAllProvinces();
			$this->data['communes'] = $this->asylum_seekers->getAllCommunces();
			$this->data['districts'] = $this->asylum_seekers->getAllDistricts();
			
			$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => site_url("asylum_seekers/rsd_application"), 'page' => lang('rsd_application_form')), array('link' => '#', 'page' => lang('add_rsd_application')));
			$meta = array('page_title' => lang('rsd_application_form'), 'bc' => $bc);	
			$this->page_construct('asylum_seekers/add_rsd_application',$meta, $this->data);
		}
	}
	
	public function edit_rsd_application($id = false)
	{
		$this->erp->checkPermissions('edit', null, 'rsd_applications');
		$this->data['application'] = $this->asylum_seekers->getRSDApplicationById($id);	
		$isCaseUnique = '';
		$post = $this->input->post();  
			if($post["full_case"] != $post["full_case_hidden"]){
				
				$isCaseUnique = '|is_unique[erp_fa_rsd_applications.full_case]';
			}  
			$config1 = array(
			  array(
                     'field'   => 'firstname',
                     'label'   => lang("firstname"),
                     'rules'   => 'required|is_unique[erp_fa_counseling.counseling_no]'
                  ),
               array(
                     'field'   => 'firstname',
                     'label'   => lang("firstname"),
                     'rules'   => 'required'
                  ),
               array(
                     'field'   => 'lastname',
                     'label'   => lang("firstname"),
                     'rules'   => 'required'
                  ),
               array(
                     'field'   => 'nationality',
                     'label'   => lang("nationality"),
                     'rules'   => 'required'
                  ),
               array(
                     'field'   => 'ethnicity',
                     'label'   => lang("ethnicity"),
                     'rules'   => 'required'
                  ),
               array(
                     'field'   => 'full_case',
                     'label'   => lang("case_no"),
                     'rules'   => 'required'.$isCaseUnique
                  )
            );
		$this->form_validation->set_rules($config1); 
		if ($this->form_validation->run() == true) {
			$tmp = substr(date("d/m/Y H:i"), 0, 13);

			$data = array( 
			  'refugee_office_id'			=> $post['refugee_office'], 
			  'case_no'						=> $post['case_no'],
			  "case_prefix" 				=> $post["case_prefix"], 
			  'full_case'=> $post['case_prefix'].$post['case_no'], 
			  'arrived_date'				=> $this->erp->fld($post['arrived_date']),
			  'registered_date'				=> $this->erp->fld($post['reg_date']),
			  'related_case'				=> $post['related_case'],
			  'special_need'				=> $post['special_need'],
			  'firstname'					=> $post['firstname'],
			  'firstname_kh'				=> $post['firstname_kh'],
			  'lastname'					=> $post['lastname'],
			  'lastname_kh'					=> $post['lastname_kh'],
			  'othername'					=> $post['othername'],
			  'othername_kh'				=> $post['othername_kh'],
			  'fathername'					=> $post['fathername'],
			  'fathername_kh'				=> $post['fathername_kh'],
			  'mothername'					=> $post['mothername'],
			  'mothername_kh'				=> $post['mothername_kh'],
			  'gender'						=> $post['gender'],
			  'nationality'					=> $post['nationality'],
			  'nationality_kh'				=> $post['nationality_kh'],
			  'dob'							=> $this->erp->fld($post['dob']),
			  'pob'							=> $post['pob'],
			  'pob_kh'						=> $post['pob_kh'],
			  'marital_status'				=> $post['marital_status'],
			  'spouse_name'					=> $post['spouse'],
			  'spouse_name_kh'				=> $post['spouse_kh'],
			  'religion'					=> $post['religion'],
			  'religion_kh'					=> $post['religion_kh'],
			  'ethnicity'					=> $post['ethnicity'],
			  'ethnicity_kh'				=> $post['ethnicity_kh'],
			  'address_o_kh'				=> $post['address_o_kh'],
			  'address_o'					=> $post['address_o'],
			  'address_kh'					=> $post['address_kh'],
			  'address'						=> $post['address'],
			  'phone'						=> $post['phone'],
			  "updated_by" 					=> $this->session->userdata("user_id"),
			  "updated_date" 				=> $this->erp->fld(date("d/m/Y H:i")),
			  "country" 					=> $post["country"],
			  "province" 					=> $post["province"],
			  "district" 					=> $post["district"],
			  "commune" 					=> $post["commune"],
			  "country_o" 					=> $post["country_o"],
			  "province_o" 					=> $post["province_o"],
			  "district_o" 					=> $post["district_o"],
			  "commune_o" 					=> $post["commune_o"],
			  "means_home_country" 			=> $post["means_home_country"],
			);
		
			$config = array(
					'upload_path' => 'assets/uploads',					
					'allowed_types' => 'gif|jpg|png|doc|pdf',
				);
			
			$this->load->library('upload', $config);
			if($this->upload->do_upload('photo')){
				$file_name = $this->upload->data('file_name');
				$data['photo'] = $file_name; 
			} 
			
			if($this->asylum_seekers->updateRSDApplication($id, $data) && $this->asylum_seekers->updateCounseling($this->data['application']->counseling_id,array("dob"=>$data['dob'])) ){
				$this->session->set_flashdata('message', lang("rsd_applications_edited"));
				redirect("asylum_seekers/edit_rsd_application/".$id);
			} 
		}
		
		else{
			
			
			$this->data['id'] = $id;

			 
			$this->data["educations"] =  $this->asylum_seekers->getEducationByCounseling($this->data['application']->counseling_id);			
			$this->data["family_member_accompanying"] =  $this->asylum_seekers->getFamilyMemberAccompanyingByCounseling($this->data['application']->counseling_id);
			$this->data["family_member_none_accompanying"] =  $this->asylum_seekers->getFamilyMemberNoneAccompanyingByCounseling($this->data['application']->counseling_id);
			$this->data["family_member_none_accompanying_outside_country"] =  $this->asylum_seekers->getFamilyMemberNoneAccompanyingOutsideCountryByCounseling($this->data['application']->counseling_id);
			$this->data["country_transit"] =  $this->asylum_seekers->getCountryTransitByCounseling($this->data['application']->counseling_id);
			$this->data["occupation"] =  $this->asylum_seekers->getOccupationByCounseling($this->data['application']->counseling_id); 
			
			$this->data["identification"] =  $this->asylum_seekers->getIdentificationDocumentByCounseling($this->data['application']->counseling_id);
			$this->data['counseling'] = $this->asylum_seekers->getCounselingById($this->data['application']->counseling_id);
			$this->data['countries'] = $this->asylum_seekers->getAllCountries();
			$this->data['provinces'] = $this->asylum_seekers->getAllProvinces();
			$this->data['communes'] = $this->asylum_seekers->getAllCommunces();
			$this->data['districts'] = $this->asylum_seekers->getAllDistricts();
			$this->data['offices'] = $this->asylum_seekers->getallOffices();
			$this->data['applications'] = $this->asylum_seekers->getAllRSDApplications();
			$this->data['case_prefix'] = $this->asylum_seekers->getCasePrefix();
			
			
			$this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));			
			$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => site_url("asylum_seekers/rsd_application"), 'page' => lang('rsd_application_form')), array('link' => '#', 'page' => lang('edit_rsd_application')));
			$meta = array('page_title' => lang('rsd_application_form'), 'bc' => $bc);	
			$this->page_construct('asylum_seekers/edit_rsd_application',$meta, $this->data);
		}
	}
	   
	public function delete_rsd_application($id = false)
	{ 
		$this->erp->checkPermissions('delete', null, 'rsd_applications');
		if($id){			
			if($this->asylum_seekers->deleteRSDApplication($id)){
				$this->session->set_flashdata('message', lang("rsd_applications_deleted"));
				redirect("asylum_seekers/rsd_application");
			}
		}
	}
    public function delete_rsd_applications($id = false)
    {
        $this->erp->checkPermissions('delete', null, 'rsd_applications');
        if($id){
            $result = $this->asylum_seekers->deleteRSDApplications($id);
            if($result){
                $this->session->set_flashdata('message', lang("rsd_applications_deleted"));
                redirect("asylum_seekers/rsd_application");
            }else{
                $this->session->set_flashdata('error', lang("rsd_applications_cannot_delete"));
                redirect("asylum_seekers/rsd_application");
            }
        }
    }
	 
	public function attach_rsd_application($id = false) 
	{ 
		$this->erp->checkPermissions('attachment', null, 'rsd_applications');
		$result = $this->asylum_seekers->getRSDApplicationById($id); 
		$this->global_model->attach_forms('fa_rsd_applications', $id, 'rsd_application');
		$this->data['action_method'] = 'attach_rsd_application';
		$this->data['delete_url'] = 'delete_attach_rsd_application';
		$this->data['view_folder'] = 'rsd_application';
		$this->data['contorler'] = $this->uri->segment(1);
		$this->data['id'] = $id;
		$this->data['result'] = $result; 
		$this->data['modal_js'] = $this->site->modal_js();		
		$this->load->view($this->theme . 'global/view_attach_forms', $this->data);
	} 
	
	public function delete_attach_rsd_application($id = false, $name = null, $img = null) 
	{
		$this->erp->checkPermissions('delete', null, 'rsd_applications');
		$this->global_model->delete_attach_forms('fa_rsd_applications', $id, $name, $img);
	}
	
	/**
	* 
	* @param undefined $id
	* ADD TRAVEL DETAIL
	* @return
	*/
	
	public function add_travel_detail($id=false) 
	{
		$post = $this->input->post(); 
		
		  if ($post == true){ 
			$data = array(  
							"departured_home_country" 	=> $this->erp->fld($post['departured_home_country']), 
							"means_home_country" 		=>  $post['means_home_country'].$post['means_home_country1'], 
							"exit_point_home_country" 	=>  $post['exit_point_home_country'],  
							"entry_point_country" 		=>  $post['entry_point_country'],  
							"arrived_country" 			=>  $this->erp->fld($post['arrived_country']),  
							"accommodation_been" 		=>  $post['accommodation_been'],  
							"accommodation_date" 		=>  $this->erp->fld($post['accommodation_date']), 
							"accommodation_duration" 	=>  $post['accommodation_duration'],
						
						);  		 		
	 
			$result = $this->db->where('id',$id)->update("fa_rsd_applications",$data);
			if($result){
				$this->session->set_flashdata('message', lang("travel_detail_added"));
				redirect($_SERVER["HTTP_REFERER"]."#content_10");
			}		
		}     
	
	}
	/**
	* 
	* @param undefined $id
	* WRITTEN STATMENT
	* @return
	*/
	
	public function add_written_statement($id=false) 
	{
		$post = $this->input->post(); 
		
		  if ($post == true){ 
			$data = array( 
							"comment_leave_country" => $post['comment_leave_country'],    
							"comment_believe_happened" => $post['comment_believe_happened']     
						);  		 
			$result = $this->db->where('id',$id)->update("fa_rsd_applications",$data);  
			if($result){
				$this->session->set_flashdata('message', lang("written_statements_added"));
				redirect($_SERVER["HTTP_REFERER"]."#content_11");
			}		
		}     
	
	}
	
	/**
	* 
	* @param undefined $id
	* NOTIFICATION REFUGEE
	* @return
	*/
	
	public function add_notification_refugee($id=false) 
	{
		$post = $this->input->post(); 
		  if ($post == true){ 
			$data = array("notification" => $post['notification']);  		 
			$result = $this->db->where('id',$id)->update("fa_rsd_applications",$data); 
			if($result){
				$this->session->set_flashdata('message', lang("rsd_applications_added"));				
				redirect("asylum_seekers/rsd_application");  
			}		
		}     
	} 
	
	public function add_registration_documentation($id=false)
	{
		$post = $this->input->post();   
		if($post == true){ 
			$data = array( 
							"comment_document_obtained_illegally" => $post['comment_document_obtained_illegally'],
							"document_missing" => $post['document_missing'],
							"comment_document_mission" => $post['comment_document_mission'],
						);  		 		
			$result = $this->db->where('id',$id)->update("fa_rsd_applications",$data); 
			if($result){
				$this->session->set_flashdata('message', lang("registration_documentation_added"));
				redirect($_SERVER["HTTP_REFERER"]."#content_5");
			}		
		}  
	}
	
	/**
	* 
	* @param undefined $id
	* REGISTRATION HISTORY
	* @return
	*/	
	
	public function add_registration_history($id=false) 
	{   
		$post = $this->input->post();   
		if($post == true){ 		
			$data = array( 
							"registered_refugee_department" => $post['registered_refugee_department'],
							"registered_refugee_place" => $post['registered_refugee_place'], 
							"registered_refugee_no" => $post['registered_refugee_no'], 
							"registered_refugee_date" => $this->erp->fld($post['registered_refugee_date']), 
							"applied_refugee_protection" => $post['applied_refugee_protection'], 
							"applied_refugee_place" => $post['applied_refugee_place'], 
							"applied_refugee_date" => $this->erp->fld($post['applied_refugee_date']),
							"applied_recognized_date" => $this->erp->fld($post['applied_recognized_date']),
							"recognition_refugee" => $post['recognition_refugee'],
						);
			$result =  $this->asylum_seekers->updateRSDApplication($id, $data);
			if($result){
				$this->session->set_flashdata('message', lang("written_statements_added"));
				redirect($_SERVER["HTTP_REFERER"]."#content_6");
			}		
		}  
	} 
	
	/**
	* 
	* @param undefined $id
	* EDUCATIONS 
	* @return
	*/
	
	public function add_education($id = false)
	{
		$post = $this->input->post();  
		if($post == true){  
			$data = array( 
							"counseling_id" => $id,
							"name_institution" => $post['name_institution'], 
							"country" => $post['country'], 
							"qualification" => $post['qualification'], 
							"from" => $this->erp->fld($post['from']), 
							"to" => $this->erp->fld($post['to']), 
						);  
			$result = $this->db->insert("fa_rsd_application_educations",$data);
			
			if($result){
				$this->session->set_flashdata('message', lang("education_added"));
				redirect($_SERVER["HTTP_REFERER"]."#content_3"); 
			}		
		}else{
			$this->data['pages']  = $_GET['page'];
			$this->data['id'] = $id;
			$this->data['users'] = $this->asylum_seekers->getUsers();
			$this->data['modal_js'] = $this->site->modal_js();		
			$this->load->view($this->theme . 'asylum_seekers/add_education', $this->data);
		} 	
	}
	
	public function edit_education($id = false, $education_id=false)
	{
		$post = $this->input->post();  
		if($post == true){ 
			$data = array(  
							"name_institution" => $post['name_institution'], 
							"country" => $post['country'], 
							"qualification" => $post['qualification'], 
							"from" => $this->erp->fld($post['from']), 
							"to" => $this->erp->fld($post['to']), 
						);  
			$result = $this->db->where(array("counseling_id"=>$id,"id"=>$education_id))
							->update("fa_rsd_application_educations",$data);
			
			if($result){
				$this->session->set_flashdata('message', lang("education_edited"));
				redirect($_SERVER["HTTP_REFERER"]."#content_3"); 
			}		
		}else{
			$this->data['education_id'] = $education_id;
			$this->data['counseling_id'] = $id;
			$this->data['result'] = $this->asylum_seekers->getEducationById($education_id);
			$this->data['users'] = $this->asylum_seekers->getUsers();
			$this->data['modal_js'] = $this->site->modal_js();		
			$this->load->view($this->theme . 'asylum_seekers/edit_education', $this->data);
		}
		
	}
	
	public function delete_education($id)
	{		
		if($id){
			$result = $this->db->where("id",$id)->delete("fa_rsd_application_educations");
			if($result){
				$this->session->set_flashdata('message', lang("education_deleted"));
				redirect($_SERVER["HTTP_REFERER"]."#content_3");  
			}	
		} 
	}
	
	/**
	* 
	* @param undefined $id
	* OCCUPATION LEVEL
	* @return
	*/
	
	public function add_occupation_level($id = false)
	{
		$post = $this->input->post();  
		if($post == true){ 
			$data = array( 
							"counseling_id" => $id,
							"name_employer" => $post['name_employer'], 
							"country" => $post['country'], 
							"job_title" => $post['job_title'], 
							"from" => $this->erp->fld($post['from']), 
							"to" => $this->erp->fld($post['to']), 
						);  
			$result = $this->db->insert("fa_rsd_application_occupations",$data);
			
			if($result){
				$this->session->set_flashdata('message', lang("occupation_added"));
				redirect($_SERVER["HTTP_REFERER"]."#content_4"); 
			}		
		}else{
			$this->data['id'] = $id;
			$this->data['users'] = $this->asylum_seekers->getUsers();
			$this->data['modal_js'] = $this->site->modal_js();		
			$this->load->view($this->theme . 'asylum_seekers/add_occupation_level', $this->data);
		} 	
	}
	
	public function edit_occupation_level($id = false,$occupation_id=false)
	{
		$post = $this->input->post();  
		if($post == true){ 
			$data = array( 
							"counseling_id" => $id,
							"name_employer" => $post['name_employer'], 
							"country" => $post['country'], 
							"job_title" => $post['job_title'], 
							"from" => $this->erp->fld($post['from']), 
							"to" => $this->erp->fld($post['to']), 
						);  
			$result = $this->db->where(array("counseling_id"=>$id,"id"=>$occupation_id))
							->update("fa_rsd_application_occupations",$data);
			
			if($result){
				$this->session->set_flashdata('message', lang("occupation_edited"));
				redirect($_SERVER["HTTP_REFERER"]."#content_4"); 
			}		
		}else{ 
			$this->data['occupation_id'] = $occupation_id;
			$this->data['counseling_id'] = $id; 
			$this->data['result'] = $this->asylum_seekers->getOccupationById($occupation_id);
			$this->data['users'] = $this->asylum_seekers->getUsers();
			$this->data['modal_js'] = $this->site->modal_js();		
			$this->load->view($this->theme . 'asylum_seekers/edit_occupation_level', $this->data);
		} 	
	}
	
	public function delete_occupation_level($id=false)
	{		
		if($id){
			$result = $this->db->where("id",$id)->delete("fa_rsd_application_occupations");
			if($result){
				$this->session->set_flashdata('message', lang("occupation_deleted"));
				redirect($_SERVER["HTTP_REFERER"]."#content_4"); 
			}	
		} 
	}
	
	/**
	* 
	* @param undefined $id
	* IDENTIFICATION DOCUMENT
	* @return
	*/
	
	public function add_identification_document($id = false)
	{
		$post = $this->input->post();  
		if($post == true){ 
			$data = array( 
							"counseling_id" => $id,
							"document_type" => $post['document_type'], 
							"issued_place" => $post['issued_place'], 
							"original_provided" => $post['original_provided'], 
							"expired_date" => $this->erp->fld($post['expired_date']), 
							"issued_date" => $this->erp->fld($post['issued_date']), 
						); 
			$config_file = array(
					'upload_path' => 'assets/uploads/document/identification',
					'allowed_types' => 'gif|jpg|png|doc|pdf',
				);
			$this->load->library('upload', $config_file);
			if($this->upload->do_upload('attachment')){
				$data["attachment"] = $this->upload->data('file_name');
			} 
			$result = $this->db->insert("fa_rsd_application_documents",$data);
			if($result){
				$this->session->set_flashdata('message', lang("identification_document_added"));				
				redirect($_SERVER["HTTP_REFERER"]."#content_5"); 
			}		
		}else{
			$this->data['id'] = $id;
			$this->data['users'] = $this->asylum_seekers->getUsers();
			$this->data['modal_js'] = $this->site->modal_js();		
			$this->load->view($this->theme . 'asylum_seekers/add_identification_document', $this->data);
		} 	
	}
	
	public function edit_identification_document($id = false,$identification_id=false)
	{
		$post = $this->input->post();  
		if($post == true){ 
			$data = array(  
						"document_type" => $post['document_type'], 
						"issued_place" => $post['issued_place'], 
						"original_provided" => $post['original_provided'], 
						"expired_date" => $this->erp->fld($post['expired_date']), 
						"issued_date" => $this->erp->fld($post['issued_date']), 
						);  
			//== config file upload
			$config_file = array(
					'upload_path' => 'assets/uploads/document/identification',
					'allowed_types' => 'gif|jpg|png|doc|pdf',
				);
			$this->load->library('upload', $config_file);
			if($this->upload->do_upload('attachment')){
				$data["attachment"] = $this->upload->data('file_name');
			}  
			$result = $this->db->where(array("counseling_id"=>$id,"id"=>$identification_id))
							->update("fa_rsd_application_documents",$data);
			
			if($result){
				$this->session->set_flashdata('message', lang("identification_document_edited"));
				redirect($_SERVER["HTTP_REFERER"]."#content_5"); 
			}		
		}else{ 
			$this->data['identification_id'] = $identification_id;
			$this->data['counseling_id'] = $id; 
			$this->data['result'] = $this->asylum_seekers->getIdentificationDocumentById($identification_id); 
			$this->data['modal_js'] = $this->site->modal_js();		
			$this->load->view($this->theme . 'asylum_seekers/edit_identification_document', $this->data);
		} 	
	}
	
	public function delete_identification_document($id=false)
	{		
		if($id){
			$result = $this->db->where("id",$id)->delete("fa_rsd_application_documents");
			if($result){
				$this->session->set_flashdata('message', lang("identification_document_deleted"));
				redirect($_SERVER["HTTP_REFERER"]."#content_5"); 
			}	
		} 
	}
	
	/**
	* 
	* @param undefined $id
	* FAMILY DEPENDANT ACCOMPANY
	* @return
	*/
	
	public function add_family_members_dependants_accompanying($id = false)
	{
		$post = $this->input->post();  
		if($post == true){ 
                   $village = $this->asylum_seekers->addTags($post['village'],'village');
			$data = array( 
							"counseling_id" => $id,
							"firstname_kh" => $post['firstname_kh'], 
							"firstname" => $post['firstname'], 
							"lastname_kh" => $post['lastname_kh'], 
							"lastname" => $post['lastname'], 
							"gender" => $post['gender'],  
							"nationality_kh" => $post['nationality_kh'],  
							"nationality" => $post['nationality'],  
							"relationship" => $post['relationship'],  
							"education_kh" => $post['education_kh'],  
							"education" => $post['education'],  
							"individual_case_no" => $post['individual_case_no'], 
							"non_accompanying" => 0,  
							"dob" => $this->erp->fld($post['dob']),  
							"pob_kh" => $post['pob_kh'],  
							"pob" => $post['pob'],  
                            "address" => $post['address'],  
							"address_kh" => $post['address_kh'],  
							"country" =>  $post['country'],
							"province" => $post['province'],
							"district" => $post['district'],
							"commune" =>  $post['commune'],
							"village"    => $village,      

							
						);  
		    if(isset($_FILES['member_photo'])){
				$config_file = array(
						'upload_path' => 'assets/uploads/members',
						'allowed_types' => 'gif|jpg|png',
					);
				$this->load->library('upload', $config_file);
				if($this->upload->do_upload('member_photo')){
					$data["photo"] = $this->upload->data('file_name');
				}
			}
			$result = $this->db->insert("fa_family_members",$data);
			if($result){
				$this->session->set_flashdata('message', lang("family_members_dependants_accompanying_added"));
				redirect($_SERVER["HTTP_REFERER"]."#content_7"); 
			}		
		}else{
			$this->data['relationship']=$this->asylum_seekers->getRelationship();
			$this->data['id'] = $id;
			$this->data['users'] = $this->asylum_seekers->getUsers();
			$this->data['modal_js'] = $this->site->modal_js();	
                     $this->data['countries'] = $this->asylum_seekers->getAllCountries();
			$this->data['provinces'] = $this->asylum_seekers->getAllProvinces();
			$this->data['communes'] = $this->asylum_seekers->getAllCommunces();
			$this->data['districts'] = $this->asylum_seekers->getAllDistricts();            
			$this->load->view($this->theme . 'asylum_seekers/add_family_members_dependants_accompanying', $this->data);
		} 	
	}	
	
	public function edit_family_members_dependants_accompanying($id = false,$member_id=false)
	{
		$post = $this->input->post();  
		if($post == true){ 
                      $village = $this->asylum_seekers->addTags($post['village'],'village');
			$data = array(  
							"firstname_kh" => $post['firstname_kh'], 
							"firstname" => $post['firstname'], 
							"lastname_kh" => $post['lastname_kh'], 
							"lastname" => $post['lastname'], 
							"gender" => $post['gender'],  
							"nationality_kh" => $post['nationality_kh'],  
							"nationality" => $post['nationality'],  
							"relationship" => $post['relationship'],  
							"education_kh" => $post['education_kh'],  
							"education" => $post['education'],   
							"individual_case_no" => $post['individual_case_no'],  
							"non_accompanying" => 0,  
							"dob" => $this->erp->fld($post['dob']), 
                                                        "pob_kh" => $post['pob_kh'],  
							"pob" => $post['pob'],   
                                                        "address" => $post['address'],  
							"address_kh" => $post['address_kh'],  
							"country" =>  $post['country'],
							"province" => $post['province'],
							"district" => $post['district'],
							"commune" =>  $post['commune'],
							"village"    => $village,     
						);
			if(isset($_FILES['member_photo'])){
				$config_file = array(
						'upload_path' => 'assets/uploads/members',
						'allowed_types' => 'gif|jpg|png',
					);
				$this->load->library('upload', $config_file);
				if($this->upload->do_upload('member_photo')){
					$data["photo"] = $this->upload->data('file_name');
				}
			}
			$result = $this->db->where(array("counseling_id"=>$id,"id"=>$member_id))
							->update("fa_family_members",$data);
			
			if($result){
				$this->session->set_flashdata('message', lang("family_members_dependants_accompanying_edited"));
				redirect($_SERVER["HTTP_REFERER"]."#content_7");
			}		
		}else{ 
			$this->data['relationship']=$this->asylum_seekers->getRelationship();
			$this->data['member_id'] = $member_id;
			$this->data['counseling_id'] = $id; 
			$this->data['result'] = $this->asylum_seekers->getFamilyMembersDependantAccompanyingById($member_id); 
			$this->data['modal_js'] = $this->site->modal_js();
			$this->data['modal_js'] = $this->site->modal_js();		
			$this->data['countries'] = $this->asylum_seekers->getAllCountries();
			$this->data['provinces'] = $this->asylum_seekers->getAllProvinces();
			$this->data['communes'] = $this->asylum_seekers->getAllCommunces();
			$this->data['districts'] = $this->asylum_seekers->getAllDistricts();            
			$this->load->view($this->theme . 'asylum_seekers/edit_family_members_dependants_accompanying', $this->data);
		} 	
	}
	
	public function delete_family_members_dependants_accompanying($id=false)
	{		
		if($id){
			$result = $this->db->where(array("id"=>$id,"non_accompanying"=>0))->delete("fa_family_members");
			if($result){
				$this->session->set_flashdata('message', lang("family_members_dependants_accompanying_deleted"));
				redirect($_SERVER["HTTP_REFERER"]."#content_7");
			}	
		} 
	}
	
	/**
	* 
	* @param undefined $id
	* FAMILY DEPENDANTS NOE ACCOMPANY
	* @return
	*/

	public function add_family_members_dependants_none_accompanying($id = false)
	{
		$post = $this->input->post();  
		if($post == true){ 
			$village = $this->asylum_seekers->addTags($post['village'],'village');
			$data = array( 
							"counseling_id" => $id,
							"firstname_kh" => $post['firstname_kh'], 
							"firstname" => $post['firstname'], 
							"lastname_kh" => $post['lastname_kh'], 
							"lastname" => $post['lastname'], 
							"gender" => $post['gender'],  
							"nationality_kh" => $post['nationality_kh'],  
							"nationality" => $post['nationality'],  
							"relationship" => $post['relationship'],  
							"education_kh" => $post['education_kh'],  
							"education" => $post['education'],  
							"individual_case_no" => $post['individual_case_no'], 
							"non_accompanying" => 1,  
							"pob_kh" => $post['pob_kh'],  
							"pob" => $post['pob'],  
							"address" => $post['address'],  
							"address_kh" => $post['address_kh'],  
							"dob" => $this->erp->fld($post['dob']),  
							"country" =>  $post['country'],
							"province" => $post['province'],
							"district" => $post['district'],
							"commune" =>  $post['commune'],
							"village"    => $village,
						);  
			if(isset($_FILES['member_photo'])){
				$config_file = array(
						'upload_path' => 'assets/uploads/members',
						'allowed_types' => 'gif|jpg|png',
					);
				$this->load->library('upload', $config_file);
				if($this->upload->do_upload('member_photo')){
					$data["photo"] = $this->upload->data('file_name');
				}
			}
			$result = $this->db->insert("fa_family_members",$data);
			
			if($result){
				$this->session->set_flashdata('message', lang("family_members_dependants_none_accompanying_added"));				
				redirect($_SERVER["HTTP_REFERER"]."#content_8");
			}		
		}else{
			$this->data['relationship']=$this->asylum_seekers->getRelationship();
			$this->data['id'] = $id;
			$this->data['users'] = $this->asylum_seekers->getUsers();
			$this->data['modal_js'] = $this->site->modal_js();
			$this->data['countries'] = $this->asylum_seekers->getAllCountries();
			$this->data['provinces'] = $this->asylum_seekers->getAllProvinces();
			$this->data['communes'] = $this->asylum_seekers->getAllCommunces();
			$this->data['districts'] = $this->asylum_seekers->getAllDistricts();
			$this->load->view($this->theme . 'asylum_seekers/add_family_members_dependants_none_accompanying', $this->data);
		} 	
	}
	
	public function edit_family_members_dependants_none_accompanying($id = false,$member_id=false)
	{
		$post = $this->input->post();  
		if($post == true){ 
			$village = $this->asylum_seekers->addTags($post['village'],'village');
			$data = array(  
							"firstname_kh" => $post['firstname_kh'], 
							"firstname" => $post['firstname'], 
							"lastname_kh" => $post['lastname_kh'], 
							"lastname" => $post['lastname'], 
							"gender" => $post['gender'],  
							"nationality_kh" => $post['nationality_kh'],  
							"nationality" => $post['nationality'],  
							"relationship" => $post['relationship'],  
							"education_kh" => $post['education_kh'],
							"education" => $post['education'],
                            "pob_kh" => $post['pob_kh'],  
							"pob" => $post['pob'],  							
							"address" => $post['address'],  
							"address_kh" => $post['address_kh'],  
							"individual_case_no" => $post['individual_case_no'],  
							"non_accompanying" => 1,  
							"dob" => $this->erp->fld($post['dob']), 
							"country" =>  $post['country'],
							"province" => $post['province'],
							"district" => $post['district'],
							"commune" =>  $post['commune'],
							"village"    => $village,
						);  
			if(isset($_FILES['member_photo'])){
				$config_file = array(
						'upload_path' => 'assets/uploads/members',
						'allowed_types' => 'gif|jpg|png',
					);
				$this->load->library('upload', $config_file);
				if($this->upload->do_upload('member_photo')){
					$data["photo"] = $this->upload->data('file_name');
				}
			}
			
			$result = $this->db->where(array("counseling_id"=>$id,"id"=>$member_id))->update("fa_family_members",$data);
			if($result){
				$this->session->set_flashdata('message', lang("family_members_dependants_none_accompanying_edited"));
				redirect($_SERVER["HTTP_REFERER"]."#content_8"); 
			}		
		}else{ 
			$this->data['relationship']=$this->asylum_seekers->getRelationship();
			$this->data['member_id'] = $member_id;
			$this->data['counseling_id'] = $id; 
			$this->data['result'] = $this->asylum_seekers->getFamilyMembersDependantNoneAccompanyingById($member_id); 
			$this->data['modal_js'] = $this->site->modal_js();		
			$this->data['countries'] = $this->asylum_seekers->getAllCountries();
			$this->data['provinces'] = $this->asylum_seekers->getAllProvinces();
			$this->data['communes'] = $this->asylum_seekers->getAllCommunces();
			$this->data['districts'] = $this->asylum_seekers->getAllDistricts();
			$this->load->view($this->theme . 'asylum_seekers/edit_family_members_dependants_none_accompanying', $this->data);
		} 	
	}
	
	public function delete_family_members_dependants_none_accompanying($id=false)
	{		
		if($id){
			$result = $this->db->where(array("id"=>$id,"non_accompanying"=>1))->delete("fa_family_members");
			if($result){
				$this->session->set_flashdata('message', lang("family_members_dependants_none_accompanying_deleted"));
				redirect($_SERVER["HTTP_REFERER"]."#content_8");
			}	
		} 
	}
	
	/**
	* 
	* @param undefined $id
	* FAMILY DEPEDANTS NOT ACCOMPANYING OUTSIDE
	* @return
	*/
	
	public function add_family_members_dependants_none_accompanying_outside_country($id = false)
	{
			$post = $this->input->post();  
		if($post == true){ 
                       $village = $this->asylum_seekers->addTags($post['village'],'village');
			$data = array( 
							"counseling_id" => $id,
							"firstname_kh" => $post['firstname_kh'], 
							"firstname" => $post['firstname'], 
							"lastname_kh" => $post['lastname_kh'], 
							"lastname" => $post['lastname'], 
							"gender" => $post['gender'],  
							"nationality_kh" => $post['nationality_kh'],  
							"nationality" => $post['nationality'],  
							"relationship" => $post['relationship'],  
							"education_kh" => $post['education_kh'],   
							"education" => $post['education'],   
							"non_accompanying" => 1,  
							"outside_country" => 1,  
							"pob_kh" => $post['pob_kh'],  
							"pob" => $post['pob'],  
							"address" => $post['address'],  
							"address_kh" => $post['address_kh'],    
							"status" => $post['status'],  
							"ethnicity" => $post['ethnicity'],  
							"dob" => $this->erp->fld($post['dob']),
                                                        "country" =>  $post['country'],
							"province" => $post['province'],
							"district" => $post['district'],
							"commune" =>  $post['commune'],
							"village"    => $village,                            
						);
			if(isset($_FILES['member_photo'])){
				$config_file = array(
						'upload_path' => 'assets/uploads/members',
						'allowed_types' => 'gif|jpg|png',
					);
				$this->load->library('upload', $config_file);
				if($this->upload->do_upload('member_photo')){
					$data["photo"] = $this->upload->data('file_name');
				}
			}
			$result = $this->db->insert("fa_family_members",$data);
			
			if($result){
				$this->session->set_flashdata('message', lang("family_members_dependants_none_accompanying_outside_country_added"));
				redirect($_SERVER["HTTP_REFERER"]."#content_9");
			}		
		}else{
			$this->data['relationship']=$this->asylum_seekers->getRelationship();
			$this->data['id'] = $id;
			$this->data['users'] = $this->asylum_seekers->getUsers();
			$this->data['modal_js'] = $this->site->modal_js();
                        $this->data['countries'] = $this->asylum_seekers->getAllCountries();
			$this->data['provinces'] = $this->asylum_seekers->getAllProvinces();
			$this->data['communes'] = $this->asylum_seekers->getAllCommunces();
			$this->data['districts'] = $this->asylum_seekers->getAllDistricts();            
			$this->load->view($this->theme . 'asylum_seekers/add_family_members_dependants_none_accompanying_outside_country', $this->data);
		} 	
	}
	
	public function edit_family_members_dependants_none_accompanying_outside_country($id = false,$member_id=false)
	{
		$post = $this->input->post();  
		if($post == true){
                      $village = $this->asylum_seekers->addTags($post['village'],'village');            
			$data = array( 
							"firstname_kh" => $post['firstname_kh'], 
							"firstname" => $post['firstname'], 
							"lastname_kh" => $post['lastname_kh'], 
							"lastname" => $post['lastname'], 
							"gender" => $post['gender'],  
							"nationality_kh" => $post['nationality'],  
							"nationality" => $post['nationality_kh'],  
							"relationship" => $post['relationship'],  
							"education_kh" => $post['education_kh'],   
							"education" => $post['education'],   
							"non_accompanying" => 1,  
							"outside_country" => 1,  
							"pob_kh" => $post['pob_kh'],  
							"pob" => $post['pob'],  
							"address" => $post['address'],  
							"address_kh" => $post['address_kh'],    
							"status" => $post['status'],  
							"ethnicity" => $post['ethnicity'],  
							"dob" => $this->erp->fld($post['dob']), 
                                                        "country" =>  $post['country'],
							"province" => $post['province'],
							"district" => $post['district'],
							"commune" =>  $post['commune'],
							"village"    => $village,                            
						);   
			if(isset($_FILES['member_photo'])){
				$config_file = array(
						'upload_path' => 'assets/uploads/members',
						'allowed_types' => 'gif|jpg|png',
					);
				$this->load->library('upload', $config_file);
				if($this->upload->do_upload('member_photo')){
					$data["photo"] = $this->upload->data('file_name');
				}
			}
			$where = array("counseling_id"=>$id,
						"non_accompanying"=>1,
						"outside_country"=>1,
						"id"=>$member_id
						);
			$result = $this->db->where($where)
							->update("fa_family_members",$data);
			
			if($result){
				$this->session->set_flashdata('message', lang("family_members_dependants_none_accompanying_outside_country_edited"));				
				redirect($_SERVER["HTTP_REFERER"]."#content_9");
			}		
		}else{ 
			$this->data['relationship']=$this->asylum_seekers->getRelationship();
			$this->data['member_id'] = $member_id; 
			$this->data['counseling_id'] = $id; 
			$this->data['result'] = $this->asylum_seekers->getFamilyMemberNoneAccompanyingOutsideCountryById($member_id);
			$this->data['modal_js'] = $this->site->modal_js();
                        $this->data['countries'] = $this->asylum_seekers->getAllCountries();
			$this->data['provinces'] = $this->asylum_seekers->getAllProvinces();
			$this->data['communes'] = $this->asylum_seekers->getAllCommunces();
			$this->data['districts'] = $this->asylum_seekers->getAllDistricts();            
			$this->load->view($this->theme . 'asylum_seekers/edit_family_members_dependants_none_accompanying_outside_country', $this->data);
		} 	
	}
		
	public function delete_family_members_dependants_none_accompanying_outside_country($id=false)
	{		 
		if($id){
			$result = $this->db->where(array("id"=>$id,"non_accompanying"=>1,"outside_country"=>1))->delete("fa_family_members");
			if($result){
				$this->session->set_flashdata('message', lang("family_members_dependants_none_accompanying_outside_country_deleted"));					
				redirect($_SERVER["HTTP_REFERER"]."#content_9");
			}	
		} 
	}

	/**
	* 
	* @param undefined $id
	* COUNTRY STRANSIT
	* @return
	*/
	
	public function add_country_transit($id=false) 
	{
		$post = $this->input->post();  
		if($post == true){ 
			$data = array( 
							"counseling_id" => $id,
							"travel_document_used" => $pos ['travel_document_used'],   
							"to" => $this->erp->fld($post['to']),   
							"from" => $this->erp->fld($post['from']),   
							"country_transit" => $post['country_transit'],   
							"travel_document_used" => $post['travel_document_used'],   
						);  
			//=== config
			$config_file = array(
					'upload_path' => 'assets/uploads/document/country_transit',
					'allowed_types' => 'gif|jpg|png|doc|pdf',
				);
			$this->load->library('upload', $config_file);
			if($this->upload->do_upload('attachment')){
				$data["attachment"] = $this->upload->data('file_name');
			}
			
			$result = $this->db->insert("fa_rsd_application_transition",$data);
			if($result){
				$this->session->set_flashdata('message', lang("country_transit_added"));				
				redirect($_SERVER["HTTP_REFERER"]."#content_10");
			}		
		}else{
			$this->data['id'] = $id;
			$this->data['users'] = $this->asylum_seekers->getUsers();
			$this->data['modal_js'] = $this->site->modal_js();		
			$this->load->view($this->theme . 'asylum_seekers/add_country_transit', $this->data);
		} 
	}
	
	public function edit_country_transit($id=false,$country_transit_id=false) 
	{
		$post = $this->input->post();  
		if($post == true){ 
			$data = array( 
							"counseling_id" => $id,
							"travel_document_used" => $pos ['travel_document_used'],   
							"to" => $this->erp->fld($post['to']),   
							"from" => $this->erp->fld($post['from']),   
							"country_transit" => $post['country_transit'],   
							"travel_document_used" => $post['travel_document_used'],   
						);  
			//=== config
			$config_file = array(
					'upload_path' => 'assets/uploads/document/country_transit',
					'allowed_types' => 'gif|jpg|png|doc|pdf',
				);
			$this->load->library('upload', $config_file); 
			if($this->upload->do_upload('attachment')){
				$data["attachment"] = $this->upload->data('file_name');
			}
			
			$result = $this->db->where(array('id'=>$country_transit_id,'counseling_id'=>$id))
								->update("fa_rsd_application_transition",$data);	
			if($result){
				$this->session->set_flashdata('message', lang("country_transit_edited"));			
				redirect($_SERVER["HTTP_REFERER"]."#content_10");				
			}		
		}else{
			$this->data['counseling_id'] = $id; 
			$this->data['country_transit_id'] = $country_transit_id; 
			$this->data['result'] = $this->asylum_seekers->getCountryTransitById($country_transit_id);
			$this->data['users'] = $this->asylum_seekers->getUsers();
			$this->data['modal_js'] = $this->site->modal_js();		
			$this->load->view($this->theme . 'asylum_seekers/edit_country_transit', $this->data);
		} 
	}
	
	public function delete_country_transit($id=false)
	{
		if($id){
			$result = $this->db->where("id",$id)->delete("fa_rsd_application_transition");
			if($result){
				$this->session->set_flashdata('message', lang("country_transit_deleted"));
				redirect($_SERVER["HTTP_REFERER"]."#content_9");
			}	
		} 
	}

	public function case_prefix()
	{ 	
	
		$prefix = $this->input->get("prefix");
		if($prefix){ 
			$this->db->select('erp_fa_rsd_applications.id');
			$this->db->from('erp_fa_rsd_applications');
			$this->db->where('case_prefix',$prefix);
			$result = $this->db->count_all_results();
			
			if($result > 0){
				 if($this->Settings->reference_format == 4) {
					$order = sprintf("%04s", $result+1);
				 } 
			}else{
				if($this->Settings->reference_format == 4) {
					$order = sprintf("%04s", 1);
				 }  
			}
			$result = $this->db->where('case_prefix',$prefix)->update('fa_case_prefix',array('order_ref'=>$order));
			$result = $this->db->where('case_prefix',$prefix)->get('fa_case_prefix')->row();
			echo json_encode($result);
		}
	}
	
	public function getAllTags()
	{
		$q = $this->input->get("q"); $v = $this->input->get("v");
		if(!empty($q)){
			$jsons = array();
			$tags = $this->asylum_seekers->getAllTagsByType($q,$v);
			if(count($tags) > 0){
				foreach($tags as $tag){
					 $jsons[] = array('id' => $tag->id, 'label' => $tag->name, 'row' => $row);
				}
				echo json_encode($jsons);
			}else{		
				echo json_encode(array(array('id' => 0, 'label' => lang('no_match_found'), 'value' => $q)));
			}
		}
	}
	
	/**
	* 
	* PRELIMINARY STARY
	* @return
	*/
	
	public function preliminary_stay()
	{
		$this->erp->checkPermissions('index', null, 'preliminary_stay');
		$this->data["result"] = "";
		$this->load->model('applications_model'); 
		$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('preliminary_stay')));
		$meta = array('page_title' => lang('preliminary_stay'), 'bc' => $bc);
		$this->page_construct('asylum_seekers/preliminary_stay',$meta, $this->data);
	}

	public function getPreliminary()
	{
		$this->load->library('datatables');
		$delete = "<a href='#' class='delete-sm delete-preliminary po' title='" . lang("delete_preliminary") . "' data-content=\"<p>"
            . lang('r_u_sure') . "</p><a class='btn btn-danger' href='" . site_url('asylum_seekers/delete_preliminary/$1') . "'>"
            . lang('i_m_sure') . "</a><button class='btn'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i>"
            . lang('delete_preliminary') . "</a>";
        $action_link = '<div class="btn-group text-left"><button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">'.lang("actions").'<span class="caret"></span></button>
					        <ul class="dropdown-menu pull-right" role="menu">
				            	<li><a target="_blank" href="'.site_url('asylum_seekers/preliminary_stay_form/$1').'" ><i class="fa fa-newspaper-o"></i>'.lang('preliminary_stay_form').'</a></li>
								<li><a data-backdrop="static" data-keyboard="false"  data-toggle="modal" data-target="#myModal"  href="'.site_url('asylum_seekers/edit_preliminary_stay/$1').'" ><i class="fa fa-pencil-square-o"></i>'.lang('edit_preliminary_stay').'</a></li>
								<li><a  data-backdrop="static" data-keyboard="false"  data-toggle="modal" data-target="#myModal" href="'.site_url('asylum_seekers/attach_preliminary_stay/$1').'" ><i class="fa fa-plus-circle"></i>'.lang('attachment').'</a></li>	
								<li>'.$delete.'</li>
					        </ul>
					    </div>';

        $this->datatables->select("
					fa_asylum_preliminary_stay.id as id,
					CONCAT(case_prefix,case_no) AS case_no,
					UPPER(firstname) AS firstname,
					UPPER(lastname) AS lastname,
					fa_asylum_preliminary_stay.contact_number,
					UPPER(gender) AS gender,
					UPPER(nationality) AS nationality,
					fa_asylum_preliminary_stay.created_date,
					fa_asylum_preliminary_stay.valid_until,
					fa_asylum_preliminary_stay.status
					", false)
            ->from("fa_rsd_applications")
			->join("fa_asylum_preliminary_stay","application_id=fa_rsd_applications.id","right")
            ->where("fa_asylum_preliminary_stay.status <> 'deleted' ");
        $this->datatables->add_column("Actions", $action_link, "id");
        echo $this->datatables->generate();
	}

	public function add_preliminary_stay($id = NULL) 
	{ 
		$post = $this->input->post();
		if($post == true){
			$this->erp->checkPermissions('add', null, 'preliminary_stay'); 			
			$village = $this->asylum_seekers->addTags($post['village'],'village');
			$data = array(
				"application_id" => $post['application_id'], 
				"valid_until" => $this->erp->fld($post['valid_until']),
				"contact_number" => $post['contact_number'],
				"occupation" => $post['occupation'],
				"occupation_kh" => $post['occupation_kh'],
				"work_place" => $post['work_place'],
				"manager" => $post['manager'],
				"country" =>  $post['country'],
				"province" => $post['province'],
				"district" => $post['district'],
				"commune" =>  $post['commune'],
				"village" =>  $village,
				"address" => $post['address'],
				"address_kh" => $post['address_kh'],
				"created_by" => $this->session->userdata("user_id"),
				"created_date" => $this->erp->fld(date("d/m/Y H:i")),
				"status"  => 'active',
				"photo"  => 'photo'
			);
			$config_file = array(
					'upload_path' => 'assets/uploads/document/preliminary_stay',
					'allowed_types' => 'gif|jpg|png|doc|pdf',
				);				
			$this->load->library('upload', $config_file);
			if($this->upload->do_upload('attachment')){
				$data["attachment"] = $this->upload->data('file_name');
			}						
			if(isset($post['photoHidden'])) {
				$data['photo'] = $post['photoHidden']; 
			}
			if($this->asylum_seekers->addPreliminaryStay($data)){
				$this->session->set_flashdata('message', lang("preliminary_stay_saved"));
				redirect($_SERVER['HTTP_REFERER']);
			}
		}
		$this->data['id'] = $id;
		$this->data['countries'] = $this->asylum_seekers->getAllCountries();
		$this->data['provinces'] = $this->asylum_seekers->getAllProvinces();
		$this->data['communes'] = $this->asylum_seekers->getAllCommunces();
		$this->data['districts'] = $this->asylum_seekers->getAllDistricts();
		$this->data['users'] = $this->asylum_seekers->getUsers();
		$this->data['offices'] = $this->asylum_seekers->getlocationByOffice();
		$this->data['modal_js'] = $this->site->modal_js();
		$this->load->view($this->theme . 'asylum_seekers/add_preliminary_stay', $this->data);
	}
	
	public function edit_preliminary_stay($id = false)
	{		
		$post = $this->input->post();
		if($post == true){ 
			$this->erp->checkPermissions('edit', null, 'preliminary_stay');
			$village = $this->asylum_seekers->addTags($post['village'],'village');
			
			$data = array(
				"application_id" => $post['application_id'],
				"valid_until" => $this->erp->fld($post['valid_until']),
				"contact_number" => $post['contact_number'],
				"occupation" => $post['occupation'],
				"occupation_kh" => $post['occupation_kh'],
				"work_place" => $post['work_place'],
				"manager" => $post['manager'],
				"country" =>  $post['country'],
				"province" => $post['province'],
				"district" => $post['district'],
				"commune" =>  $post['commune'],
				"village" =>  $village,
				"address" => $post['address'],
				"address_kh" => $post['address_kh'],
				"created_by" => $this->session->userdata("user_id"),
				"created_date" => $this->erp->fld(date("d/m/Y H:i")),
				"status"  => 'pending', 
			);
			
			$config = array(
					'upload_path' => 'assets/uploads/document/preliminary_stay',
					'allowed_types' => 'gif|jpg|png|doc|pdf',
				);
			$this->load->library('upload', $config);
			if($this->upload->do_upload('attachment')){
				$data["attachment"] = $this->upload->data('file_name');
			}
			$configPhoto = array(
					'upload_path' => 'assets/uploads/',
					'allowed_types' => 'gif|jpg|png',
				);
			$this->load->library('upload', $configPhoto);
			if($this->upload->do_upload('photo')){
				$data["photo"] = $this->upload->data('file_name');
			}  
			if($this->asylum_seekers->updatePreliminaryStay($id, $data)){
				$this->session->set_flashdata('message', lang("preliminary_stay_edited"));
				redirect('asylum_seekers/preliminary_stay/');
			}
			
		}
		$this->data['preliminary_stay'] = $this->asylum_seekers->getPreliminaryStayById($id);
		$this->data['result'] = $this->asylum_seekers->getRSDApplicationById($this->data['preliminary_stay']->application_id);		
		$this->data['address'] = $this->asylum_seekers->getAddressByid($id);	
		$this->data['countries'] = $this->asylum_seekers->getAllCountries();
		$this->data['provinces'] = $this->asylum_seekers->getAllProvinces();
		$this->data['communes'] = $this->asylum_seekers->getAllCommunces();
		$this->data['districts'] = $this->asylum_seekers->getAllDistricts();
		$this->data['modal_js'] = $this->site->modal_js();
		$this->data['id'] = $id;
		$this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
		$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('request_form')));
		$this->load->view($this->theme . 'asylum_seekers/edit_preliminary_stay', $this->data);
	}

	public function delete_preliminary($id = false)
	{
		$this->erp->checkPermissions('delete', null, 'preliminary_stay');
		if($id){			
			if($this->asylum_seekers->deletePreliminaryStay($id)){
				$this->session->set_flashdata('message', lang("preliminary_stay_deleted"));
				redirect($_SERVER['HTTP_REFERER']);
			}
		}
	}
	
	public function preliminary_stay_form($id = false)
	{
		$this->erp->checkPermissions('application_form', null, 'preliminary_stay');
		$this->data['preliminary'] = $this->asylum_seekers->getPreliminaryStayById($id);
		$this->data["result"] = $this->asylum_seekers->getRSDApplicationById($this->data['preliminary']->application_id);
		$this->data['departments'] = $this->asylum_seekers->getDepartmentDirector();
		$this->data['address'] = $this->asylum_seekers->getAddressByid();
		$this->data['members'] = $this->asylum_seekers->getFamilyMemberAccompanyingByCounseling($this->data["result"]->counseling_id);
		$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('preliminary_stay_form')));
		$meta = array('page_title' => lang('preliminary_stay_form'), 'bc' => $bc);	
		$this->page_construct('asylum_seekers/preliminary_stay_form',$meta, $this->data);
	}

	public function attach_preliminary_stay($id = false) 
	{ 
		$result = $this->asylum_seekers->getPreliminaryStayById($id);
		$this->erp->checkPermissions('attachment', null, 'preliminary_stay');
		$this->global_model->attach_forms('fa_asylum_preliminary_stay', $id, 'preliminary_stay');
		$this->data['action_method'] = 'attach_preliminary_stay';
		$this->data['delete_url'] = 'delete_attach_preliminary_stay';
		$this->data['view_folder'] = 'preliminary_stay';
		$this->data['contorler'] = $this->uri->segment(1);
		$this->data['id'] = $id;
		$this->data['result'] = $result; 
		$this->data['modal_js'] = $this->site->modal_js();		
		$this->load->view($this->theme . 'global/view_attach_forms', $this->data);
	}  
	
	public function delete_attach_preliminary_stay($id = false, $name = null, $img = null) 
	{
		$this->erp->checkPermissions('delete', null, 'preliminary_stay');
		$this->global_model->delete_attach_forms('fa_asylum_preliminary_stay', $id, $name, $img);
	}    
	
	public function chage_status_item($id = false) {
		$status = $this->input->get("status");
		if(!empty($id)){  
			$result = $this->asylum_seekers->changeStatusItem($id,$status); 
			if($result) {
				 echo "success";
			}else{		
				 echo "unsuccess";
			}
		} 
	}
	
}
	
?>