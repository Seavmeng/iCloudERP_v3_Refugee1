<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Application_notifications extends MY_Controller
{
	
	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->library('pagination');
		$this->load->model('applications_model','applications');
		$this->load->model('application_setup_model');
		$this->load->model('sales_model');
		$this->lang->load('applications', $this->Settings->language);
		$this->lang->load('application_forms', $this->Settings->language);
		$this->load->model('Site','site');
		$this->site->auditLogs();
	}

	public function index()
	{
		
	}
	
	public function refugee_recognized()
	{ 
		$this->data["result"] = "";
		$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('refugee_recognized')));
		$meta = array('page_title' => lang('refugee_recognized'), 'bc' => $bc);	
		$this->page_construct('application_notifications/k1/refugee_recognized',$meta, $this->data);
	} 
	
	public function getRefugeeRecognized()
	{ 
		$this->load->library('datatables');	 
        $action_link = null; 
        $this->datatables->select("
					fa_interview_evaluations.id as id,
					CONCAT(erp_fa_rsd_applications.case_prefix,erp_fa_rsd_applications.case_no) as case_no,
					UPPER(firstname) AS firstname,
					UPPER(lastname) AS lastname,
					dob,
					UPPER(gender) AS gender,
					UPPER(nationality) AS nationality,
					evaluate_date,
					fa_interview_evaluations.status,
					", false)
            ->from("fa_rsd_applications")
			->join("fa_interview_evaluations","application_id=fa_rsd_applications.id","right")
			->where("fa_rsd_applications.status <>","deleted")
			->where("fa_rsd_applications.status","active");
		if($this->input->get("type") == "notification"){ 
			$this->datatables->where("
			erp_fa_interview_evaluations.status = 'approved' 
			AND 
				erp_fa_rsd_applications.id  
			NOT IN (SELECT application_id FROM erp_fa_interview_recognitions)");
		} 
        $this->datatables->add_column("Actions", $action_link, "id");
        echo $this->datatables->generate();
	}
	  
	public function asylum_seeker_register()
	{ 
		$this->data["result"] = "";
		$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('asylum_seeker_register')));
		$meta = array('page_title' => lang('asylum_seeker_register'), 'bc' => $bc);	
		$this->page_construct('application_notifications/k3/asylum_seeker_register',$meta, $this->data);
	} 
	
	public function getAsylumSeekerRegister()
	{ 
		$this->load->library('datatables');	
		 
        $action_link = null; 		
        $this->datatables->select("
					fa_rsd_applications.id as id,
					CONCAT(case_prefix,case_no) as case_no,
					UPPER(firstname) AS firstname,
					UPPER(lastname) AS lastname,
					dob,
					UPPER(gender) AS gender,
					UPPER(nationality) AS nationality,
					fa_offices.office,
					fa_rsd_applications.created_date,
					fa_rsd_applications.status,
					", false)
            ->from("fa_rsd_applications")
			->join("fa_offices","fa_offices.id=fa_rsd_applications.refugee_office_id","left")
			->where("fa_rsd_applications.status <>","deleted");
			
		if($this->input->get("type") == "notification"){ 
			$this->datatables->where("erp_fa_rsd_applications.status <> 'closed'
									AND erp_fa_rsd_applications.status <> 'inactive'
									AND erp_fa_rsd_applications.id 
									NOT IN (SELECT application_id FROM erp_fa_interview_appointments)");
		} 
		
        $this->datatables->add_column("Actions", $action_link, "id");
        echo $this->datatables->generate();
	}
	
	public function interview_refugees_pending() 
	{
		$this->erp->checkPermissions('index', null, 'interview_refugees');
		$this->data["result"] = "";
		$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('interview_refugees_pending')));
		$meta = array('page_title' => lang('interview_refugees'), 'bc' => $bc);	
		$this->page_construct('application_notifications/k3/interview_refugees_pending',$meta, $this->data);
	}
	
	public function getInterviewRefugeesPending()
	{ 
		$this->load->library('datatables');	
		$delete = "<a href='#' class='delete-it po' title='" . lang("delete_interview") . "' data-content=\"<p>"
            . lang('r_u_sure') . "</p><a class='btn btn-danger' href='" . site_url('applications/delete_interview/$1') . "'>"
            . lang('i_m_sure') . "</a><button class='btn'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i>"
            . lang('delete_interview') . "</a>";
		
        $action_link = '<div class="btn-group text-left"><button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">'.lang("actions").'<span class="caret"></span></button>
					        <ul class="dropdown-menu pull-right" role="menu">					        																														       
					            <li><a target="_blank" href="'.site_url('applications/interview_form/$1').'" ><i class="fa fa-newspaper-o"></i>'.lang('interview_form').'</a></li>
								<li><a class="evalue-it" href="'.site_url('applications/add_evaluation_refugee/$1').'" ><i class="fa fa-pencil-square-o"></i>'.lang('add_evaluation_refugee').'</a></li>
								<li><a class="edit-it" href="'.site_url('applications/edit_interview_refugee/$1').'" ><i class="fa fa-pencil-square-o"></i>'.lang('edit_interview_refugee').'</a></li>
								<li><a data-backdrop="static" data-keyboard="false"  data-toggle="modal" data-target="#myModal" href="'.site_url('applications/attach_interview_refugee/$1').'" ><i class="fa fa-plus-circle"></i>'.lang('attachment').'</a></li>
								<li>'.$delete.'</li>								
					        </ul>
					    </div>';
		
        $this->datatables->select("
					fa_interviews.id as ids,
					CONCAT(case_prefix,case_no) as case_no,
					fa_rsd_applications.firstname,
					fa_rsd_applications.lastname,
					fa_rsd_applications.dob,
					UPPER(gender) as gender,
					nationality,
					interview_date,
					fa_interviews.status,
					", false)
            ->from("fa_rsd_applications")
			->join("fa_interviews","application_id=fa_rsd_applications.id","right");
			
		if($this->input->get("type") == "notification"){ 
			$this->datatables->where(" 
							erp_fa_interviews.status = 'pending'
						AND erp_fa_interviews.application_id NOT IN (
						SELECT
							application_id
						FROM
							erp_fa_interview_evaluations)
			
			");
		} 
			
        $this->datatables->add_column("Actions", $action_link, "ids");
        echo $this->datatables->generate();
	}
	
	public function negative_refugee_pending() 
	{	 
		$this->erp->checkPermissions('index', null, 'interview_refugees');
		$this->data["result"] = "";
		$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('negative_refugee_pending')));
		$meta = array('page_title' => lang('negative_refugee_pending'), 'bc' => $bc);	
		$this->page_construct('application_notifications/k3/negative_refugee_pending',$meta, $this->data);
	}
	
	public function getNegativeRefugeePending()
	{
		$this->load->library('datatables');	
		$delete = "<a href='#' class='delete-nv po' title='" . lang("delete_negative") . "' data-content=\"<p>"
            . lang('r_u_sure') . "</p><a class='btn btn-danger' href='" . site_url('applications/delete_negative/$1') . "'>"
            . lang('i_m_sure') . "</a><button class='btn'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i>"
            . lang('delete_negative') . "</a>";
		$action_link = '<div class="btn-group text-left"><button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">'.lang("actions").'<span class="caret"></span></button>
					        <ul class="dropdown-menu pull-right" role="menu">					        																									
					            <li><a target="_blank" href="'.site_url('applications/negative_rsd_form/$1').'" ><i class="fa fa-newspaper-o"></i>'.lang('negative_refugee').'</a></li>					            
					            <li><a class="edit-nv" data-toggle="modal" data-target="#myModal" href="'.site_url('applications/edit_negative/$1').'" ><i class="fa fa-pencil-square-o"></i>'.lang('edit_negative').'</a></li>
					            <li><a class="file-nv" data-backdrop="static" data-keyboard="false"  data-toggle="modal" data-target="#myModal" href="'.site_url('applications/attach_negative_refugee/$1').'" ><i class="fa fa-plus-circle"></i>'.lang('attachment').'</a></li>
								<li>'.$delete.'</li>
							</ul>
					    </div>';
			$this->datatables->select("
					fa_interview_negatives.id as id,
					CONCAT(erp_fa_rsd_applications.case_prefix,erp_fa_rsd_applications.case_no) as case_no,
					UPPER(firstname) AS firstname,
					UPPER(lastname) AS lastname,
					dob,
					UPPER(gender) AS gender,
					UPPER(nationality) AS nationality,
					fa_interview_negatives.created_date,
					fa_interview_negatives.negative_date, 
					fa_interview_negatives.status,
					", false)
            ->from("fa_rsd_applications")
            ->where("fa_interview_negatives.status ", 'pending' )
			->join("fa_interview_negatives","application_id=fa_rsd_applications.id","inner");
		 
        $this->datatables->add_column("Actions", $action_link, "id");
        echo $this->datatables->generate();
	}
	 
	public function asylum_seeker_withdrawal_list()
	{
		$this->erp->checkPermissions('index', null, 'appointment_refugees');
		$this->data["result"] = "";
		$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('appointment_refugees')));
		$meta = array('page_title' => lang('appointment_refugees'), 'bc' => $bc);
		$appointment_notification = $this->applications->getSetting();
		$this->data['notification'] = $appointment_notification->appointment_notification; 
		$this->page_construct('application_notifications/k3/asylum_seeker_withdrawal_list',$meta, $this->data);
	}  
}