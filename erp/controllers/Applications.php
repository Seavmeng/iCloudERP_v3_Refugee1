<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Applications extends MY_Controller
{
	
	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->library('pagination');
		$this->load->model('applications_model','applications');
		$this->load->model('global_model');
		$this->load->model('application_setup_model'); 
		$this->lang->load('applications', $this->Settings->language);
		$this->load->model('Site','site');
		$this->site->auditLogs();
	} 
	
	/**
	* 
	* APPOINTMENT REFUGEE
	* @return
	*/ 
	
	public function appointment_refugees()
	{
		$this->erp->checkPermissions('index', null, 'appointment_refugees');
		$this->data["result"] = "";
		$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('appointment_refugees')));
		$meta = array('page_title' => lang('appointment_refugees'), 'bc' => $bc);
		$appointment_notification = $this->applications->getSetting();
		$this->data['notification'] = $appointment_notification->appointment_notification;
		$this->page_construct('applications/appointment_refugees',$meta, $this->data);
	}
	
	public function interview_appointment_appeal()
	{ 
		$this->erp->checkPermissions('index', null, 'interview_appointment_appeal');
		$this->data["result"] = "";
		$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('interview_appointment_appeal')));
		$meta = array('page_title' => lang('interview_appointment_appeal'), 'bc' => $bc);
		$appointment_notification = $this->applications->getSetting();
		$this->data['notification'] = $appointment_notification->appointment_notification;
		$this->page_construct('applications/interview_appointment_appeal',$meta, $this->data);
	}

	public function getAppointmentRefugees($id = false)
	{
		$where = "(erp_fa_interview_appointments.status = 'active' OR erp_fa_interview_appointments.status ='miss_interview')";

		$type = 'is_interview';
		if($this->input->get("type")=="is_appeal"){ 
			$type = 'is_appeal';
		}
		$this->load->library('datatables');	

$missInterview = "<a href='#' class='delete-sm po hideMissInterview' title='" . lang("missInterview") . "' data-content=\"<p>"
            . lang('r_u_sure') . "</p><a class='btn btn-danger' href='" . site_url('applications/miss_interview_appointment_refugee/$1') . "'>"
            . lang('i_m_sure') . "</a><button class='btn'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-ban\"></i>"
            . lang('missInterview') . "</a>";


		$interview = "<a href='#' class='delete-sm po hideInterview' title='" . lang("interview") . "' data-content=\"<p>"
            . lang('r_u_sure') . "</p><a class='btn btn-danger' href='" . site_url('applications/add_interview_refugee/$1?q='.$type) . "'>"
            . lang('i_m_sure') . "</a><button class='btn'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-newspaper-o\"></i>"
            . lang('interview') . "</a>";
        $action_link = '<div class="btn-group hideActionAppointment text-left"><button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">'.lang("actions").'<span class="caret"></span></button>
					        <ul class="dropdown-menu pull-right" role="menu">					        																									
								<li><a class="hideAppoinmentRefugeeForm" target="_blank" href="'.site_url('applications/appointment_refugee_form/$1').'" ><i class="fa fa-newspaper-o"></i>'.lang('appointment_refugee_form').'</a></li>								
								<li>'.$interview.'</li>		
								<li><a class="hideAttachFile"  data-backdrop="static" data-keyboard="false"  data-toggle="modal" data-target="#myModal" href="'.site_url('applications/attach_appointment_refugee/$1').'" ><i class="fa fa-plus-circle"></i>'.lang('attachment').'</a></li>
							    <li><a class="hideEdit" data-backdrop="static" data-keyboard="false"  data-toggle="modal" data-target="#myModal" href="'.site_url('applications/edit_appointment_refugee/$1').'" ><i class="fa fa-pencil-square-o"></i>'.lang('edit_appointment_refugee').'</a></li>
								<li>'.$missInterview.'</li>								
								<li>'.$delete.'</li>								
					        </ul>
					    </div>'; 
        $this->datatables->select("
					fa_interview_appointments.id as id,					
					CONCAT(case_prefix,case_no) as case_no,
					UPPER(firstname) AS firstname,
					UPPER(lastname) AS lastname,
					fa_rsd_applications.dob,
					UPPER(gender) AS gender,
					UPPER(nationality) AS nationality,
					appointment_date,
					appointment_date as appointment_date1,
					erp_fa_interview_appointments.status,
					", false)
            ->from("fa_rsd_applications")
			->join("fa_interview_appointments","application_id=fa_rsd_applications.id","right") 
			->where($where);
			
			if($this->input->get('date_from') && $this->input->get('date_to')){
				$date_from = $this->input->get("date_from");
				$date_to   = $this->input->get("date_to");
				$this->datatables->where("DATE(appointment_date) >=",$this->erp->fld($date_from));
				$this->datatables->where("DATE(appointment_date) <=",$this->erp->fld($date_to));
			} 
	
			if($this->input->get('case_no')){
				$this->datatables->where("CONCAT(case_prefix, case_no)=",$this->input->get('case_no'));
			}
		
		if($this->input->get("type")=="is_appeal"){ 
			$this->datatables->where("fa_interview_appointments.is_appeal =",1);
		}else {
			$this->datatables->where("fa_interview_appointments.is_appeal <>",1);
		} 

  
		if($this->input->get("id")){
			$ids = explode(",",$this->input->get("id"));
			$negative_id = array();
			foreach($ids as $id){
				$negative_id[] = $id;
			}
			$this->db->where_in("fa_interview_appointments.id",$negative_id);
		}
        $this->datatables->add_column("Actions", $action_link, "id");
        echo $this->datatables->generate();
	}   
 // interview appointment on appeal 
    public function getAppointmentRefugeesOnAppeal($id = false)
	{
		$where = "(erp_fa_interview_appointments.status = 'active' OR erp_fa_interview_appointments.status ='miss_interview')";

		$type = 'is_interview';
		if($this->input->get("type")=="is_appeal"){ 
			$type = 'is_appeal';
		}
		$this->load->library('datatables');	
		$delete = "<a href='#' class='delete-sm po hideDelete' title='" . lang("delete_appointment_refugee") . "' data-content=\"<p>"
            . lang('r_u_sure') . "</p><a class='btn btn-danger' href='" . site_url('applications/delete_appointment_refugee/$1') . "'>"
            . lang('i_m_sure') . "</a><button class='btn'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i>"
            . lang('delete_appointment_refugee') . "</a>";
		$interview = "<a href='#' class='delete-sm po hideInterview' title='" . lang("interview") . "' data-content=\"<p>"
            . lang('r_u_sure') . "</p><a class='btn btn-danger' href='" . site_url('applications/add_interview_refugee/$1?q='.$type) . "'>"
            . lang('i_m_sure') . "</a><button class='btn'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-newspaper-o\"></i>"
            . lang('interview') . "</a>";
        $action_link = '<div class="btn-group hideActionAppointment text-left"><button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">'.lang("actions").'<span class="caret"></span></button>
					        <ul class="dropdown-menu pull-right" role="menu">					        																									
								<li><a class="hideAppoinmentRefugeeForm" target="_blank" href="'.site_url('applications/appointment_refugee_form/$1').'" ><i class="fa fa-newspaper-o"></i>'.lang('appointment_refugee_form').'</a></li>								
								<li>'.$interview.'</li>		
								<li><a class="hideAttachFile"  data-backdrop="static" data-keyboard="false"  data-toggle="modal" data-target="#myModal" href="'.site_url('applications/attach_appointment_refugee/$1').'" ><i class="fa fa-plus-circle"></i>'.lang('attachment').'</a></li>
							    <li><a class="hideEdit" data-backdrop="static" data-keyboard="false"  data-toggle="modal" data-target="#myModal" href="'.site_url('applications/edit_appointment_refugee/$1').'" ><i class="fa fa-pencil-square-o"></i>'.lang('edit_appointment_refugee').'</a></li>
								<li>'.$missInterview.'</li>								
								<li>'.$delete.'</li>								
					        </ul>
					    </div>'; 
        $this->datatables->select("
					fa_interview_appointments.id as id,					
					CONCAT(case_prefix,case_no) as case_no,
					UPPER(firstname) AS firstname,
					UPPER(lastname) AS lastname,
					fa_rsd_applications.dob,
					UPPER(gender) AS gender,
					UPPER(nationality) AS nationality,
					appointment_date,
					appointment_date as appointment_date1,
					", false)
            ->from("fa_rsd_applications")
			->join("fa_interview_appointments","application_id=fa_rsd_applications.id","right") 
			->where($where)
			->order_by("fa_interview_appointments.created_date","desc");
			
			if($this->input->get('date_from') && $this->input->get('date_to')){
				$date_from = $this->input->get("date_from");
				$date_to   = $this->input->get("date_to");
				$this->datatables->where("DATE(appointment_date) >=",$this->erp->fld($date_from));
				$this->datatables->where("DATE(appointment_date) <=",$this->erp->fld($date_to));
			} 
	
			if($this->input->get('case_no')){
				$this->datatables->where("CONCAT(case_prefix, case_no)=",$this->input->get('case_no'));
			}
		
		if($this->input->get("type")=="is_appeal"){ 
			$this->datatables->where("fa_interview_appointments.is_appeal =",1);
		}else {
			$this->datatables->where("fa_interview_appointments.is_appeal <>",1);
		} 

  
		if($this->input->get("id")){
			$ids = explode(",",$this->input->get("id"));
			$negative_id = array();
			foreach($ids as $id){
				$negative_id[] = $id;
			}
			$this->db->where_in("fa_interview_appointments.id",$negative_id);
		}
        $this->datatables->add_column("Actions", $action_link, "id");
        echo $this->datatables->generate();
	}   

	public function appointment_refugees_log()
	{
		$this->data["result"] = "";
		$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('appointment_refugees')));
		$meta = array('page_title' => lang('appointment_refugees'), 'bc' => $bc);	
		$this->page_construct('applications/appointment_refugees_log',$meta, $this->data);
	}
	 
	public function appointment_refugees_log_appeal()
	{
		$this->data["result"] = "";
		$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('interview_appointment_appeal')));
		$meta = array('page_title' => lang('interview_appointment_appeal'), 'bc' => $bc);	
		$this->page_construct('applications/appointment_refugees_log_appeal',$meta, $this->data);
	}
	
	public function getAppointmentRefugeesLog($id = false)
	{
		$this->load->library('datatables');	
		
		$delete = "<a href='#' class='delete-sm po' title='" . lang("delete_appointment_refugee") . "' data-content=\"<p>"
            . lang('r_u_sure') . "</p><a class='btn btn-danger' href='" . site_url('applications/delete_appointment_refugee/$1') . "'>"
            . lang('i_m_sure') . "</a><button class='btn'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i>"
            . lang('delete_appointment_refugee') . "</a>";
		
        $action_link = '<div class="btn-group text-left"><button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">'.lang("actions").'<span class="caret"></span></button>
					        <ul class="dropdown-menu pull-right" role="menu">					        																									
								<li><a target="_blank" href="'.site_url('applications/appointment_refugee_form/$1').'" ><i class="fa fa-newspaper-o"></i>'.lang('appointment_refugee_form').'</a></li>								
								<li>'.$delete.'</li>
					        </ul>
					    </div>'; 
        $this->datatables->select("
					fa_interview_appointments.id as id,					
					CONCAT(case_prefix,case_no) as case_no,
					UPPER(firstname) AS firstname,
					UPPER(lastname) AS lastname,
					fa_rsd_applications.dob,
					UPPER(gender) AS gender,
					UPPER(nationality) AS nationality,
					fa_interview_appointments.appointment_date,
					fa_interview_appointments.modified_date,
					fa_interview_appointments.status,
					", false)
            ->from("fa_rsd_applications")
			->join("fa_interview_appointments","application_id=fa_rsd_applications.id","right")			   
			->where("fa_interview_appointments.status",'inactive');
			if($this->input->get('date_from') && $this->input->get('date_to')){
				$date_from = $this->input->get("date_from");
				$date_to   = $this->input->get("date_to");
				$this->datatables->where("date(erp_fa_interview_appointments.appointment_date) >=",$this->erp->fld($date_from));
				$this->datatables->where("date(erp_fa_interview_appointments.appointment_date) <=",$this->erp->fld($date_to));
			}
			if($this->input->get('case_no')){
				$this->datatables->where("CONCAT(case_prefix, case_no)=",$this->input->get('case_no'));
			}
			if($this->input->get("type")=="is_appeal"){ 
				$this->datatables->where("fa_interview_appointments.is_appeal =",1);
			}else {
				$this->datatables->where("fa_interview_appointments.is_appeal <>",1);
			} 
			
        $this->datatables->add_column("Actions", $action_link, "id");
        echo $this->datatables->generate();
	}   
	
	public function add_appointment_refugee()
	{  
		$post = $this->input->post(); 
		$members = array(); 
		
		if($post == true){   
			if($post['is_appeal'] == "is_appeal") {
				$this->erp->checkPermissions('add', null, 'interview_appointment_appeal');
			}else {
				$this->erp->checkPermissions('add', null, 'appointment_refugees');
			}
			$members1 = array();
			foreach($post['members'] as $member){
				$members1[] = array("member_id" => $member);
			} 			
			$data = array(
						"application_id" => $post['application_id'],
						"appointment_date" => $this->erp->fld($post['appointment_date']),
						"appointment_place_id" => $post['appointment_place'],
						"review_by" => $post['officer'], 
						"interview_by" => $post['interview_by'],
						"evaluation_by" => $post['evaluation_by'],
						"created_by" =>  $this->session->userdata("user_id"),
						"created_date" =>  $this->erp->fld(date('d/m/Y H:i')),
						"appointment_member" => json_encode($post['members']),
						"status" =>  "active",
						"member_id"=> $post['member'],
						"is_appeal"=> $post['is_appeal'] == "is_appeal" ? 1 : 0,
					); 
					
				if($this->applications->addAppointmentRefugee($data,$members1) && $this->applications->updateRSDApplication($post['application_id'], array("procedure_sequence"=>"appointmented_appeal"))){
					$this->session->set_flashdata('message', lang("appointment_interview_saved"));
					redirect($_SERVER['HTTP_REFERER']);
				} 
		}else{
			$this->data['modal_js'] = $this->site->modal_js();	
			$this->data['id'] = $id; 
			$this->data['applications'] =  $this->applications->getAllRSDApplications();
			$this->data['users'] = $this->applications->getUsers();
			$this->data['offices'] = $this->applications->getlocationByOffice();
			$this->load->view($this->theme . 'applications/add_appointment_refugee', $this->data);
		} 	
	}
 // 
    public function add_first_appointment_refugee()
	{  
		$post = $this->input->post(); 
		$members = array(); 
		 
		if($post == true){   
			if($post['is_appeal'] == "is_appeal") {
				$this->erp->checkPermissions('add', null, 'interview_appointment_appeal');
			}else {
				$this->erp->checkPermissions('add', null, 'appointment_refugees');
			}
			$members1 = array();
			foreach($post['members'] as $member){
				$members1[] = array("member_id" => $member);
			} 			
			$data = array(
						"application_id" => $post['application_id'],
						"appointment_date" => $this->erp->fld($post['appointment_date']),
						"appointment_place_id" => $post['appointment_place'],
						"review_by" => $post['officer'], 
						"interview_by" => $post['interview_by'],
						"evaluation_by" => $post['evaluation_by'],
						"created_by" =>  $this->session->userdata("user_id"),
						"created_date" =>  $this->erp->fld(date('d/m/Y H:i')),
						"appointment_member" => json_encode($post['members']),
						"status" =>  "active",
						"member_id"=> $post['member'],
						"is_appeal"=> $post['is_appeal'] == "is_appeal" ? 1 : 0,
					); 
				if($this->applications->addAppointmentRefugee($data,$members1) && $this->applications->updateRSDApplication($post['application_id'], array("procedure_sequence"=>"appointmented"))){ 
					$this->session->set_flashdata('message', lang("appointment_interview_saved"));
					redirect($_SERVER['HTTP_REFERER']);
				} 
		}else{
			$this->data['modal_js'] = $this->site->modal_js();	
			$this->data['id'] = $id; 
			$this->data['applications'] =  $this->applications->getAllRSDApplications();
			$this->data['users'] = $this->applications->getUsers();
			$this->data['offices'] = $this->applications->getlocationByOffice();
			$this->load->view($this->theme . 'applications/add_first_appointment_refugee', $this->data);
		} 	
	}
	public function edit_appointment_refugee($id=false)
	{ 
		$post = $this->input->post();
		if($post == true){ 
			$this->erp->checkPermissions('edit', null, 'appointment_refugees'); 
			$data = array(
						"appointment_date" => $this->erp->fld($post['appointment_date']),
						"appointment_place_id" => $post['appointment_place'],
						"interview_by" => $post['interview_by'],
						"evaluation_by" => $post['evaluation_by'], 
					);
		     $result =  $this->applications->updateAppointmentRefugee($id,$data);
			 if($result) {
				 $this->session->set_flashdata('message', lang("appointment_interview_saved"));
				redirect($_SERVER['HTTP_REFERER']);
			 } 
		}else{
			$this->data['result'] = $this->applications->getAppointmentRefugeeById($id);
			$this->data['id'] = $id;  
			$this->data['users'] = $this->applications->getUsers();
			$this->data['offices'] = $this->applications->getlocationByOffice();
			$this->data['modal_js'] = $this->site->modal_js();		
			$this->load->view($this->theme . 'applications/edit_appointment_refugee', $this->data);
		}
	} 
	
	public function delete_appointment_refugee($id = false) 
	{
		$this->erp->checkPermissions('delete', null, 'appointment_refugees');
		if($id){			
			if($this->applications->deleteAppointmentRefugee($id)){
				$this->session->set_flashdata('message', lang("interview_appointment_deleted"));
				redirect($_SERVER['HTTP_REFERER']);
			}
		} 
	}
	
	public function appointment_refugee_form($id = false)
	{		
		$this->erp->checkPermissions('application_form', null, 'appointment_refugees');
		$this->data['appointment'] = $this->applications->getAppointmentById($id);  
		$this->data['member_dependant'] = $this->applications->getFamilyMembersDependantAccompanyingById($this->data["appointment"]->member_id);
		$this->data['result'] = $this->applications->getRSDApplicationById($this->data['appointment']->application_id);
		$this->data['departments'] = $this->applications->getDepartmentDirector();		
		
		$this->data['result_members'] = $this->applications->getFamilyMemberAccompanyingByCounselingIn($this->data['result']->counseling_id, $this->data['appointment']->appointment_member);		
	
		$this->data['office'] = $this->applications->getOfficeById($this->data['appointment']->appointment_place_id);
		$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('interview_appointment')));
		$meta = array('page_title' => lang('appointment_refugee_form'), 'bc' => $bc);	
		$this->page_construct('applications/appointment_refugee_form',$meta, $this->data);
	}
	
	public function getFamilyAppointmentRefugees()
	{
		$id = $this->input->get('id', TRUE);  
		$result = $this->applications->getRSDApplicationById($id);
		$result_members =  $this->applications->getFamilyMemberAccompanyingByCounseling($result->counseling_id);
		$html = "";
		foreach($result_members as $result){
			$html .="<tr>";
				$html .= "<td>";
					$html .="<input class='checkbox' name='member_id[]' value='".$result->id."' type='checkbox'/>";
				$html  .= "</td>";
				$html .= "<td class='center'>".$result->firstname." ".$result->lastname."</td>";
				$html .= "<td class='center'>".lang($result->gender)."</td>";
				$html .= "<td class='center'>".$result->relationship."</td>";
				$html .= "<td class='center'>".$this->erp->hrsd($result->dob)."</td>";
			$html .="</tr>";
		}
		echo json_encode($html);
	}
	
	public function attach_appointment_refugee($id = false) 
	{ 	  	 
		$this->erp->checkPermissions('attachment', null, 'appointment_refugees');
		$result = $this->applications->getAppointmentById($id);
		$this->global_model->attach_forms('fa_interview_appointments', $id, 'appointment_refugee');
		$this->data['action_method'] = 'attach_appointment_refugee';
		$this->data['delete_url'] = 'delete_attach_appointment_refugee';
		$this->data['view_folder'] = 'appointment_refugee';
		$this->data['contorler'] = $this->uri->segment(1);
		$this->data['id'] = $id;
		$this->data['result'] = $result; 
		$this->data['modal_js'] = $this->site->modal_js();		
		$this->load->view($this->theme . 'global/view_attach_forms', $this->data);		
	}
	
	public function delete_attach_appointment_refugee($id = false, $name = null, $img = null)
	{
		$this->erp->checkPermissions('delete', null, 'appointment_refugees');
		$result = $this->applications->getAppointmentById($id);
		$this->global_model->delete_attach_forms('fa_interview_appointments', $id, $name, $img);
			
	}  
	
	/**
	* 
	* INTERVIEW REFUGEE
	* @return
	*/ 
	
	public function interview_member($id = false)
	{  
		$result = $this->applications->getRSDApplicationById($id);
		$this->data['result_members'] = $this->applications->getCounselingMemberId($result->counseling_id);	 
		$this->data['users'] = $this->applications->getUsers(); 
		$this->data['offices'] = $this->applications->getlocationByOffice();
		$this->data['modal_js'] = $this->site->modal_js();		 
		$this->load->view($this->theme . 'applications/add_interview_member_form', $this->data);  
	}
	
	public function add_interview_refugee($id = false)
	{	
		if($_GET['q'] == "is_appeal") {
			$this->erp->checkPermissions('add', null, 'interview_appeal');	
			$data_status = array("procedure_sequence"=>"interview_appeal");  
		}else {
			$this->erp->checkPermissions('add', null, 'interview_refugees');	
			$data_status = array("procedure_sequence"=>"interviewed"); 
		}
		$post = $this->input->post();
		$config = array(
					array(
						 'field'   => 'case_no',
						 'label'   => lang("case_no"),
						 'rules'   => 'required'
					  )
				);
		$this->form_validation->set_rules($config);
		if ($this->form_validation->run() == true) {
					
			$appointment = $this->applications->getAppointmentById($id);			
			$data = array(
						"appointment_id" => $appointment->id,
						"application_id" => $appointment->application_id,
						"interpreter" => $post['interpreter'],			
						"language" => json_encode($post['language']),
						"interview_date" => $this->erp->fld($post['interview_date']." ".$post['interview_started']),
						"created_date" => $this->erp->fld(date("d/m/Y H:i")),
						"interview_started" => $post['interview_started'],
						"interview_records" => $post['interview_records'], 
						"interview_officer" => $post['interview_officer'],
						"interview_place" => $post['interview_place'],
						"is_appeal"=> $post['is_appeal'] == "is_appeal" ? 1 : 0,
                        "status" => "pending"
					);
			$data2 = array(
					'firstname'=> $post['firstname'],
					'firstname_kh'=> $post['firstname_kh'],
					'lastname'=> $post['lastname'],
					'lastname_kh'=> $post['lastname_kh'],
					'nationality'=> $post['nationality'],
					'nationality_kh'=> $post['nationality_kh'],
					'dob'=> $this->erp->fld($post['dob']),
					'gender'=> $post['gender'],
					'nationality'=> $post['nationality'],
					'nationality_kh'=> $post['nationality_kh'],
					'religion'=> $post['religion'],
					'religion_kh'=> $post['religion_kh'],
					'ethnicity'=> $post['ethnicity'],
					'ethnicity_kh'=> $post['ethnicity_kh'],
				);	 
			if($this->input->get("q") == 'is_appeal'){ 
				 $data["negative_appeal_id"] = $id; 
			}   
			$interview_id = $this->applications->addInterviewRefugee($data, $data2) ; 
			if($interview_id && $this->applications->updateRSDApplication($appointment->application_id,$data_status )){				
				$this->session->set_flashdata('message', lang("interview_saved_records")); 
				redirect("applications/edit_interview_refugee/".$interview_id);
			}
		}else{
			$this->data['id'] = $id;
			$this->data['appointment'] = $this->applications->getAppointmentById($id);
			$this->data['application'] =  $this->applications->getRSDApplicationById($this->data['appointment']->application_id);
			$this->data['users'] = $this->applications->getUsers();
			$this->data['offices'] = $this->applications->getlocationByOffice();
			$this->data['countries'] = $this->applications->getAllCountries();
			$this->data['provinces'] = $this->applications->getAllProvinces();
			$this->data['communes'] = $this->applications->getAllCommunces();
			$this->data['districts'] = $this->applications->getAllDistricts();			
			$this->data['languages'] = $this->applications->getAllLanguages();
			$this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
			$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('add_interview_refugee')));
			$meta = array('page_title' => lang('add_interview_refugee'), 'bc' => $bc);	
			$this->page_construct('applications/add_interview_refugee',$meta, $this->data);
		}
	} 
	
	public function edit_interview_refugee($id = false)
	{ 
		if($_GET['q'] == "is_appeal") {
			$this->erp->checkPermissions('edit', null, 'interview_appeal');	
		}else {
			$this->erp->checkPermissions('edit', null, 'interview_refugees');	
		}
		$post = $this->input->post();
		$config = array(
               array(
                     'field'   => 'case_no',
                     'label'   => lang("case_no"),
                     'rules'   => 'required'
                  )
            );			
		$this->form_validation->set_rules($config);
		if ($this->form_validation->run() == true){
			$data = array(					
						"interview_officer" => $post['interview_officer'],
						"interpreter" => $post['interpreter'],
						"language" => json_encode($post['language']),
						"interview_place" => $post['interview_place'], 
						"interview_date" => $this->erp->fld($post['interview_date']." ".$post['interview_started']),
						"interview_started" => $post['interview_started'],
						"interview_records" => $post['interview_records'],
						"clarification_applicant" => $post['clarification_applicant'],
                        "status" => "pending"
					);
			
			$data2 = array(
					'firstname'=> $post['firstname'],
					'firstname_kh'=> $post['firstname_kh'],
					'lastname'=> $post['lastname'],
					'lastname_kh'=> $post['lastname_kh'],
					'nationality'=> $post['nationality'],
					'nationality_kh'=> $post['nationality_kh'],
					'dob'=> $this->erp->fld($post['dob']),
					'gender'=> $post['gender'],
					'nationality'=> $post['nationality'],
					'nationality_kh'=> $post['nationality_kh'],
					'religion'=> $post['religion'],
					'religion_kh'=> $post['religion_kh'],
					'ethnicity'=> $post['ethnicity'],
					'ethnicity_kh'=> $post['ethnicity_kh'],
				);
				
			if($this->applications->updateInterviewRefugee($id, $data, $data2)){
				$this->session->set_flashdata('message', lang("interview_updated_records"));
				redirect("applications/edit_interview_refugee/".$id);
			} 
		}else{	
			$this->data['id'] = $id;
			$this->data['result'] = $this->applications->getInterviewById($id);
			$this->data['application'] =  $this->applications->getRSDApplicationById($this->data['result']->application_id);
			$this->data['interview_records'] = $this->applications->getInterviewRecords($id);			
			$this->data["identification"] =  $this->applications->getIdentificationDocumentByCounseling($this->data['application']->counseling_id);
			$this->data['counseling'] = $this->applications->getCounselingById($this->data['application']->counseling_id);			
			$this->data['members'] = $this->applications->getAppointmentByIdLists(json_decode($this->data['appointment']->appointment_member));
			
			$this->data['users'] = $this->applications->getUsers();
			$this->data['countries'] = $this->applications->getAllCountries();
			$this->data['provinces'] = $this->applications->getAllProvinces();
			$this->data['communes'] = $this->applications->getAllCommunces();
			$this->data['districts'] = $this->applications->getAllDistricts();
			$this->data['languages'] = $this->applications->getAllLanguages();
			$this->data['offices'] = $this->applications->getlocationByOffice();
		
			$this->data['interview'] = $this->applications->getInterviewById($id);
			$this->data['application'] = $this->applications->getRSDApplicationById($this->data['interview']->application_id); 		
			$this->data['appointment'] = $this->applications->getAppointmentById($this->data['interview']->appointment_id);		
			$this->data['result_members'] = $this->applications->getFamilyMemberAccompanyingByCounselingIn($this->data['application']->counseling_id, $this->data['appointment']->appointment_member);
			$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('edit_interview_refugee')));
			$meta = array('page_title' => lang('edit_interview_refugee'), 'bc' => $bc);	
			$this->page_construct('applications/edit_interview_refugee',$meta, $this->data);
		}
	}
	
	public function interview_refugees()
	{ 
		$this->erp->checkPermissions('index', null, 'interview_refugees');
		$this->data["result"] = "";
		$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('interview_refugees')));
		$meta = array('page_title' => lang('interview_refugees'), 'bc' => $bc);	
		$this->page_construct('applications/interview_refugees',$meta, $this->data);
	
	}
	
	public function interview_appeal()
	{
		$this->erp->checkPermissions('index', null, 'interview_appeal');
		$this->data["result"] = "";
		$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('interview_appeal')));
		$meta = array('page_title' => lang('interview_appeal'), 'bc' => $bc);	
		$this->page_construct('applications/interview_appeal',$meta, $this->data);
	
	}
	
	public function delete_interview($id = false)
	{  
		if($_GET['q'] == "is_appeal") {
			$this->erp->checkPermissions('delete', null, 'interview_appeal');	
		}else {
			$this->erp->checkPermissions('delete', null, 'interview_refugees');	
		}
		if($id){			
			if($this->applications->deleteInterviewRefugee($id)){
				$this->session->set_flashdata('message', lang("interview_deleted"));
				redirect($_SERVER['HTTP_REFERER']);
			}
		}
	}
	
	public function getInterviewRefugees()
	{ 
		if($_GET['type'] == "is_appeal") {
			$is_appeal = "?q=is_appeal";
		}	 
		$this->load->library('datatables');	
		$delete = "<a href='#' class='delete-it po' title='" . lang("delete_interview") . "' data-content=\"<p>"
            . lang('r_u_sure') . "</p><a class='btn btn-danger' href='" . site_url('applications/delete_interview/$1') .$is_appeal."'>"
            . lang('i_m_sure') . "</a><button class='btn'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i>"
            . lang('delete_interview') . "</a>";
		
        $action_link = '<div class="btn-group text-left"><button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">'.lang("actions").'<span class="caret"></span></button>
					        <ul class="dropdown-menu pull-right" role="menu">					        																														       
					            <li><a target="_blank" href="'.site_url('applications/interview_form/$1').$is_appeal.'" ><i class="fa fa-newspaper-o"></i>'.lang('interview_form').'</a></li>
								<li><a class="evalue-it" href="'.site_url('applications/add_evaluation_refugee/$1').$is_appeal.'" ><i class="fa fa-pencil-square-o"></i>'.lang('add_evaluation_refugee').'</a></li>
								<li><a class="edit-it" href="'.site_url('applications/edit_interview_refugee/$1').$is_appeal.'" ><i class="fa fa-pencil-square-o"></i>'.lang('edit_interview_refugee').'</a></li>
								<li><a data-backdrop="static" data-keyboard="false"  data-toggle="modal" data-target="#myModal" href="'.site_url('applications/attach_interview_refugee/$1').$is_appeal.'" ><i class="fa fa-plus-circle"></i>'.lang('attachment').'</a></li>
								<li>'.$delete.'</li>								
					        </ul>
					    </div>';
		
        $this->datatables->select("
					fa_interviews.id as ids,
					CONCAT(case_prefix,case_no) as case_no,
					fa_rsd_applications.firstname,
					fa_rsd_applications.lastname,
					fa_rsd_applications.dob,
					UPPER(gender) as gender,
					nationality,
					interview_date,
					fa_interviews.status
					", false)
            ->from("fa_rsd_applications")
            ->where("fa_interviews.status <> 'deleted'")
			->join("fa_interviews","application_id=fa_rsd_applications.id","right");

			if($this->input->get('date_from') && $this->input->get('date_to')){
				$date_from = $this->input->get("date_from");
				$date_to   = $this->input->get("date_to");
				$this->datatables->where("DATE(interview_date) >=",$this->erp->fld($date_from));
				$this->datatables->where("DATE(interview_date) <=",$this->erp->fld($date_to));
			} 
				
			if($this->input->get('case_no')){
				$this->datatables->where("CONCAT(case_prefix, case_no)=",$this->input->get('case_no'));
			}

		if($this->input->get("type")=="is_appeal"){ 
			$this->datatables->where("fa_interviews.is_appeal =",1);
		}else {
			$this->datatables->where("fa_interviews.is_appeal <>",1);
		}  
			
		if($this->input->get("type") == "notification"){ 
			$this->datatables->where(" 
							erp_fa_interviews.status = 'pending'
						AND erp_fa_interviews.application_id NOT IN (
						SELECT
							application_id
						FROM
							erp_fa_interview_evaluations)
			");
		} 
		if($this->input->get('date_from') && $this->input->get('date_to')){
			$date_from = $this->input->get("date_from");
			$date_to   = $this->input->get("date_to");
			$this->datatables->where("date(erp_fa_interviews.interview_date) >=",$this->erp->fld($date_from));
			$this->datatables->where("date(erp_fa_interviews.interview_date) <=",$this->erp->fld($date_to));
		}

		if($this->input->get('case_no')){
			$this->datatables->where("CONCAT(case_prefix, case_no)=",$this->input->get('case_no'));
		}
			
		$this->datatables->order_by('fa_interviews.id','DESC');	
        $this->datatables->add_column("Actions", $action_link, "ids");
        echo $this->datatables->generate();
	} 
	
	public function interview_form($id = false)
	{ 
		if($_GET['q'] == "is_appeal") {
			$this->erp->checkPermissions('form', null, 'interview_appeal');	
		}else {
			$this->erp->checkPermissions('application_form', null, 'interview_refugees');	
		}
		$this->data['interview'] = $this->applications->getInterviewById($id);
		$this->data['result'] = $this->applications->getRSDApplicationById($this->data['interview']->application_id); 		
		$this->data['appointment'] = $this->applications->getAppointmentById($this->data['interview']->appointment_id);		
		$this->data['result_members'] = $this->applications->getFamilyMemberAccompanyingByCounselingIn($this->data['result']->counseling_id, $this->data['appointment']->appointment_member);				
		
		$this->data['province_o'] = $this->applications->getProvinceById($this->data["result"]->province_o);
		$this->data['commune_o'] = $this->applications->getCommunceById($this->data["result"]->commune_o);
		$this->data['country_o'] = $this->applications->getCountryById($this->data["result"]->country_o);
		$this->data['district_o'] = $this->applications->getDistrictById($this->data["result"]->district_o);
		
		 
		
		$this->data['languages'] = $this->applications->getAllLanguages();
		$this->data['users'] = $this->applications->getUsers();
		$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('interview_form')));
		$meta = array('page_title' => lang('interview_form'), 'bc' => $bc);	
		$this->page_construct('applications/interview_form',$meta, $this->data);
	}
	
	public function add_interview_records($id = false)
	{
		$post = $this->input->post();
		$data = array();
		if($post==true){
			foreach($post['questions'] as $i => $question){ 
				$data[] = array(
					"questions"=>$post['questions'][$i] ,
					"answers" => $post['answers'][$i],
					"notes" => $post['notes'][$i] ,
					"interview_id" => $id,
					"interview_member_id" => $post['member'][$i]
				);  
			}			
			
			if($interview = $this->applications->addInterviewRecord($id, $data)){
				$this->session->set_flashdata('message', lang("interview_updated_records"));
				redirect("applications/edit_interview_refugee/".$id."#content_3");
			} 
		}
	}

	public function add_interview_records_evaluation($id = false)
	{
		$post = $this->input->post();
		$data = array();
		if($post==true){
			foreach($post['questions'] as $i => $question){ 
				$data[] = array(
					"questions"=>$post['questions'][$i] ,
					"answers" => $post['answers'][$i],
					"notes" => $post['notes'][$i] ,
					"interview_id" => $id,
					"interview_member_id" => $post['member'][$i]
				);  
			}			
			
			if($interview = $this->applications->addInterviewRecord($id, $data)){
				$this->session->set_flashdata('message', lang("interview_updated_records"));
				redirect("applications/edit_evaluation_refugee/".$id."#content_8");
			} 
		}
	}
	
	public function delete_interview_records($id =false)
	{
		if($id){			
			if($this->applications->deleteInterviewRecord($id)){
				$this->session->set_flashdata('message', lang("deleted"));				
				redirect($_SERVER['HTTP_REFERER']."#content_3");
			} 
		}
	}

	public function delete_interview_records_evaluation($id =false)
	{
		if($id){			
			if($this->applications->deleteInterviewRecord($id)){
				$this->session->set_flashdata('message', lang("deleted"));				
				redirect($_SERVER['HTTP_REFERER']."#content_8");
			} 
		}
	}
		
	public function add_clarification_applicant($id=false)
	{
		$post= $this->input->post();
		if(isset($post) == true){
			$data = array(
						"clarification_applicant" => $post['clarification_applicant'],
						"interview_ended"=>date("H:i:sa")
					);
			if($this->applications->updateInterviewRefugee($id, $data)){
				$this->session->set_flashdata('message', lang("interview_saved_records"));
				redirect("applications/interview_form/".$id);
			}				
		}
	}
	
	public function attach_interview_refugee($id = false) 
	{ 	  
		$q = isset($_GET['q']) ? $_GET['q'] : '';
		if($q == "is_appeal") { 
			$this->erp->checkPermissions('attachment', null, 'interview_appeal');	
		}else {
			$this->erp->checkPermissions('attachment', null, 'interview_refugees');	
		}
			
		$result = $this->applications->getInterviewById($id);
		$this->global_model->attach_forms('fa_interviews', $id, 'interview_refugee');
		$this->data['action_method'] = 'attach_interview_refugee';
		$this->data['delete_url'] = 'delete_attach_interview_refugee';
		$this->data['view_folder'] = 'interview_refugee';
		$this->data['contorler'] = $this->uri->segment(1);
		$this->data['id'] = $id;
		$this->data['result'] = $result; 
		$this->data['modal_js'] = $this->site->modal_js();		
		$this->load->view($this->theme . 'global/view_attach_forms', $this->data);

	} 
	
	public function delete_attach_interview_refugee($id = false, $name = null, $img = null)
	{
		$result = $this->applications->getInterviewById($id);
		$this->global_model->delete_attach_forms('fa_interviews', $id, $name, $img);
	}  
	 
	/**
	* 
	* EVALUCATION REFUGEE
	* @return
	*/ 
	
	public function interview_evaluate_appeal()
	{		
		$this->erp->checkPermissions('index', null, 'interview_evaluate_appeal');
		$this->data["result"] = "";
		$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('interview_evaluate_appeal')));
		$meta = array('page_title' => lang('interview_evaluate_appeal'), 'bc' => $bc);	
		$this->page_construct('applications/interview_evaluate_appeal',$meta, $this->data);	
	}
	
	public function evaluation_refugee()
	{		
		$this->erp->checkPermissions('index', null, 'evaluation_refugees');
		$this->data["result"] = "";
		$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('evaluation_refugee')));
		$meta = array('page_title' => lang('evaluation_refugee'), 'bc' => $bc);	
		$this->page_construct('applications/evaluation_refugee',$meta, $this->data);	
	}
	
	public function add_evaluation_refugee($id = false)
	{ 
		
		$this->erp->checkPermissions('add', null, 'evaluation_refugees');
		$post = $this->input->post();
		$config = array(
               array(
                     'field'   => 'case_no',
                     'label'   => lang("case_no"),
                     'rules'   => 'required|is_unique[erp_fa_interview_evaluations.id]'
                  ) 
            );
			
		$this->form_validation->set_rules($config);
		
		if ($this->form_validation->run() == true) {
			$is_appeal = $post['is_appeal'];
			$interview = $this->applications->getInterviewById($id);
			$data = array(
					"application_id" => $interview->application_id,
					"interview_id" => $interview->id,
					"number_of_interview" => $post['number_of_application'],
					"member_with" => $post['member_with'],
					"evaluate_officer" => $post['evaluate_officer'],
					"evaluate_date" => $this->erp->fld(date("d/m/Y H:i:s")),
					"created_by" => $this->session->userdata("user_id"),
					"created_date" => $this->erp->fld(date("d/m/Y H:i:s")),
					"status" => "pending",
				);
				$evaluation = $this->applications->addEvaluation($data,$is_appeal);
				$questions = array(); 
				$datas = array();
				if(isset($post['c15'])){ 
					for($i=0;$i<count($post['c15']);$i++){  
						$mcq = $post['c15'][$i];
						$specify_ans = $post['d15'][$i];
						$datas[] =array( 
								"answer" => $post['e15'],
								"multiple_choice" => array( "option" =>$mcq ,"value" => $specify_ans),
						 );
					}  
				}    
				$myJson = json_encode($datas,JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES);
                $data1 = array();
				if(isset($post['c26'])){
					for($i=0;$i<count($post['c26']);$i++){  
						$mcq1 = $post['c26'][$i];
						$specify_ans1 = $post['d26'][$i];
						 $data1[] =array( 
								"answer" => $post['e26'],
								"multiple_choice" => array( "option" =>$mcq1 ,"value" => $specify_ans1),
						  );
					}
				}
				
				$myJson1 =	json_encode($data1,JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES); 
				
				for($i=1; $i<=27; $i++){ 
					$g = $post['g'.$i]; 
					
					if($g==1){	$group=1; }
					else if($g==2){	$group=2; }
					else if($g==3){	$group=3; }
					else if($g==4){	$group=4; }
					else if($g==5){	$group=5; }
					else if($g==6){	$group=6; }
					if($i==15){ 
						$ans = $myJson;
						$other = "";
					}
					else if($i==26){
						$ans = $myJson1;
						$other = "";
					}else {
						 $ans = $post['e'.$i];
						 $other = $post['c'.$i];
					}
					
					$questions[] = array(
									"evaluation_id" => $evaluation,
									"question" => $post['q'.$i],
									"is_yes" => $post['a'.$i],
									"answer" => $ans ,
									"other_answer" => $other,
									"group" => $group, 
								);						
				}
				$result = $this->db->insert_batch("fa_interview_evaluation_detail",$questions); 
				if($result){
					$this->session->set_flashdata('message', lang("evaluation_refugee_saved"));
					redirect('applications/edit_evaluation_refugee/'.$evaluation); 
				} 
		}else{    		
			$this->data['id'] = $id;
			$this->data['interview'] = $this->applications->getInterviewById($id);
			$this->data['interview_records'] = $this->applications->getInterviewRecords($id);
			$this->data['application'] =  $this->applications->getRSDApplicationById($this->data['interview']->application_id);
			$this->data['result_members'] = $this->applications->getFamilyMemberAccompanyingByCounselingIn($this->data['application']->counseling_id, $this->data['appointment']->appointment_member);
			$this->data['languages'] = $this->applications->getAllLanguages();
			$this->data['offices'] = $this->applications->getlocationByOffice();
			$this->data['countries'] = $this->applications->getAllCountries();
			$this->data['provinces'] = $this->applications->getAllProvinces();
			$this->data['communes'] = $this->applications->getAllCommunces();
			$this->data['districts'] = $this->applications->getAllDistricts();
			
			$this->data['users'] = $this->applications->getUsers();			
			$this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));			
			$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('add_evaluation_refugee')));
			$meta = array('page_title' => lang('add_evaluation_refugee'), 'bc' => $bc);	
			$this->page_construct('applications/add_evaluation_refugee',$meta, $this->data);				 
		}
	}

	public function edit_evaluation_refugee($id = false)
	{ 
		$this->erp->checkPermissions('edit', null, 'evaluation_refugees');
		$post = $this->input->post();  
		
		$this->form_validation->set_rules($config);
		if ($post == true) { 
			$data = array(
						"number_of_interview" => $post['number_of_application'], 
						"member_with" => $post['member_with'],
						"evaluate_officer" => $post['evaluate_officer'],
						"evaluate_date" => $this->erp->fld(date("d/m/Y H:i:s")),
						"status" => "pending"
					);
			$result = $this->applications->updateEvaluation($id,$data);
			$questions = array(); 
			$data = array();
			if(isset($post['c15'])){ 
				for($i=0; $i < count($post['c15']);$i++){  
					$mcq = $post['c15'][$i];
					$specify_ans = $post['d15'][$i];
					$data[] =array( 
							"answer" => $post['e15'],
							"multiple_choice" => array("option" =>$mcq ,"value" => $specify_ans),
					);
				} 
			}   
			
			$myJson =	json_encode($data,JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES);
            $data1 = array();
			if(isset($post['c26'])){
				for($i=0; $i < count($post['c26']); $i++){  
					$mcq1 = $post['c26'][$i];
					$specify_ans1 = $post['d26'][$i];
					 $data1[] =array( 
							"answer" => $post['e26'],
							"multiple_choice" => array( "option" =>$mcq1 ,
																"value" => $specify_ans1
															),
					  );
				}
			}
			$myJson1 =	json_encode($data1,JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES);
			for($i=1; $i<=27; $i++){  
				$g=$post['g'.$i];
				if($g==1){	$group=1; }
				else if($g==2){	$group=2; }
				else if($g==3){	$group=3; }
				else if($g==4){	$group=4; }
				else if($g==5){	$group=5; }
				else if($g==6){	$group=6; }
				if($i==15){
					$ans = $myJson;
					$other = "";
				}else if($i==26){
					$ans = $myJson1;
					$other = "";
				}else{
					 $ans = $post['e'.$i];
					 $other = $post['c'.$i];
				}
				$questions[] = array(
								"evaluation_id" => $id,
								"question" => $post['q'.$i],
								"is_yes" => $post['a'.$i],
								"answer" => $ans ,
								"other_answer" => $other,
								"group" => $group, 
							);	
			}
			if(!empty($questions)){
                $this->db->where("evaluation_id",$id)->delete("fa_interview_evaluation_detail");
            }
			if($this->applications->addEvaluationDetail($questions)){
				$this->session->set_flashdata('message', lang("evaluation_refugee_updated"));
				redirect("applications/edit_evaluation_refugee/".$id);
			}
			
		}else{ 
			$this->data['id'] = $id;
			$this->data['users'] = $this->applications->getUsers();
			$this->data['evaluation'] = $this->applications->getEvaluationById($id);
			$this->data['interview_records'] = $this->applications->getInterviewRecords($id);
			$this->data['result_members'] = $this->applications->getFamilyMemberAccompanyingByCounselingIn($this->data['application']->counseling_id, $this->data['appointment']->appointment_member);
			$this->data['evaluation_detail'] = $this->applications->getEvaluationDetail($id); 
			
			$this->data['interview'] = $this->applications->getInterviewById($this->data['evaluation']->interview_id);
			$this->data['application'] =  $this->applications->getRSDApplicationById($this->data['evaluation']->application_id);
			$this->data['languages'] = $this->applications->getAllLanguages();
			$this->data['offices'] = $this->applications->getlocationByOffice();
			$this->data['countries'] = $this->applications->getAllCountries();
			$this->data['provinces'] = $this->applications->getAllProvinces();
			$this->data['communes'] = $this->applications->getAllCommunces();
			$this->data['districts'] = $this->applications->getAllDistricts();
			
			$this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));			
			$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('edit_evaluation_refugee')));
			$meta = array('page_title' => lang('edit_evaluation_refugee'), 'bc' => $bc);	
			$this->page_construct('applications/edit_evaluation_refugee',$meta, $this->data);	
		}
	}
	
	public function getEvaluations()
	{
		$appeal = $this->input->get("type");
		$v = "";
		if($appeal=="evaluate_appeal") {
			$v .= "?v=appeal";
		}
		$this->load->library('datatables');	
		$delete = "<a href='#' class='delete-ev po' title='" . lang("delete_evaluation") . "' data-content=\"<p>"
            . lang('r_u_sure') . "</p><a class='btn btn-danger' href='" . site_url('applications/delete_evaluation/$1') . "'>"
            . lang('i_m_sure') . "</a><button class='btn'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i>"
            . lang('delete_evaluation') . "</a>";
		
		$review = "<a href='#' class='review-ev po' title='" . lang("review_evaluation_refugee") . "' data-content=\"<p>"
            . lang('r_u_sure') . "</p><a class='btn btn-danger' href='" . site_url('applications/review_evaluation/$1') . "'>"
            . lang('i_m_sure') . "</a><button class='btn'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-newspaper-o\"></i>"
            . lang('review_evaluation_refugee') . "</a>";
		
		$approve = "<a href='#' class='approve-ev po' title='" . lang("approve_evaluation_refugee") . "' data-content=\"<p>"
            . lang('r_u_sure') . "</p><a class='btn btn-danger' href='" . site_url('applications/approve_evaluation/$1'.$v) . "'>"
            . lang('i_m_sure') . "</a><button class='btn'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-newspaper-o\"></i>"
            . lang('approve_evaluation_refugee') . "</a>";
			
		$reject = "<a href='#' class='reject-ev po' title='" . lang("reject_evaluation_refugee") . "' data-content=\"<p>"
            . lang('r_u_sure') . "</p><a class='btn btn-danger' href='" . site_url('applications/reject_evaluation/$1'.$v) . "'>"
            . lang('i_m_sure') . "</a><button class='btn'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-newspaper-o\"></i>"
            . lang('reject_evaluation_refugee') . "</a>";
		
        $action_link = '<div class="btn-group text-left"><button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">'.lang("actions").'<span class="caret"></span></button>
					        <ul class="dropdown-menu pull-right" role="menu">					        																									
					            <li><a target="_blank" href="'.site_url('applications/procedure_decision_form/$1').'" ><i class="fa fa-newspaper-o"></i>'.lang('procedure_decision').'</a></li>
								<li><a target="_blank" href="'.site_url('applications/extension_notification/$1').'" ><i class="fa fa-newspaper-o"></i>'.lang('extension_notification').'</a></li>
								<li>'.$review.'</li>
								<li>'.$approve.'</li>
								<li>'.$reject.'</li>
								<li><a class="edit-ev" href="'.site_url('applications/edit_evaluation_refugee/$1').'" ><i class="fa fa-pencil-square-o"></i>'.lang('edit_evaluation_refugee').'</a></li>
								<li><a class="file-ev" data-backdrop="static" data-keyboard="false"  data-toggle="modal" data-target="#myModal" href="'.site_url('applications/attach_evaluation_refugee/$1').'" ><i class="fa fa-plus-circle"></i>'.lang('attachment').'</a></li>
								<li>'.$delete.'</li>								
					        </ul>
					    </div>'; 
        $this->datatables->select("
					fa_interview_evaluations.id as id,
					CONCAT(erp_fa_rsd_applications.case_prefix,erp_fa_rsd_applications.case_no) as case_no,
					UPPER(firstname) AS firstname,
					UPPER(lastname) AS lastname,
					dob,
					UPPER(gender) AS gender,
					UPPER(nationality) AS nationality,
					evaluate_date,
					fa_interview_evaluations.status
					", false)
            ->from("fa_rsd_applications")
            ->where("fa_interview_evaluations.status <> 'deleted'")
			->join("fa_interview_evaluations","application_id=fa_rsd_applications.id","right");

			if($this->input->get('date_from') && $this->input->get('date_to')){
				$date_from = $this->input->get("date_from");
				$date_to   = $this->input->get("date_to");
				$this->datatables->where("DATE(evaluate_date) >=",$this->erp->fld($date_from));
				$this->datatables->where("DATE(evaluate_date) <=",$this->erp->fld($date_to));
			} 
				
			if($this->input->get('case_no')){
				$this->datatables->where("CONCAT(case_prefix, case_no)=",$this->input->get('case_no'));
			}

		if($appeal=="evaluate_appeal") {
			$this->datatables->where("fa_interviews.is_appeal =",1)
							->join("fa_interviews","fa_interview_evaluations.interview_id=fa_interviews.id");
		}else {
			$this->datatables->where("fa_interviews.is_appeal =",0)
							->join("fa_interviews","fa_interview_evaluations.interview_id=fa_interviews.id");
		}
		
		if($this->input->get("type") == "refugee_rejected"){ 
			$this->datatables->where("
			erp_fa_interview_evaluations.status = 'rejected' 
			AND 
				erp_fa_rsd_applications.id  
			NOT IN (SELECT application_id FROM erp_fa_interview_negatives)");
		}  	
		if($this->input->get("type") == "refugee_recognized"){ 
			$this->datatables->where("
			erp_fa_interview_evaluations.status = 'approved' 
			AND 
				erp_fa_rsd_applications.id  
			NOT IN (SELECT application_id FROM erp_fa_interview_negatives)");
		}  
		
		if($this->input->get('date_from') && $this->input->get('date_to')){
			$date_from = $this->input->get("date_from");
			$date_to   = $this->input->get("date_to");
			$this->datatables->where("date(erp_fa_interview_evaluations.evaluate_date) >=",$this->erp->fld($date_from));
			$this->datatables->where("date(erp_fa_interview_evaluations.evaluate_date) <=",$this->erp->fld($date_to));
		} 	

		if($this->input->get('case_no')){
			$this->datatables->where("CONCAT(case_prefix, case_no)=",$this->input->get('case_no'));
		}
			
		$this->datatables->order_by("erp_fa_interview_evaluations.id","desc"); 
        $this->datatables->add_column("Actions", $action_link, "id");
        echo $this->datatables->generate();
	}
		 
	public function procedure_decision_form($id = false)
	{ 
		$this->erp->checkPermissions('procedure_decision', null, 'evaluation_refugees');
		$this->data['id'] = $id;
		$this->data['users'] = $this->applications->getUsers();
		$this->data['evaluation'] = $this->applications->getEvaluationById($id);
		$this->data['interview'] = $this->applications->getInterviewById($this->data['evaluation']->interview_id);
		$this->data['application'] =  $this->applications->getRSDApplicationById($this->data['evaluation']->application_id);
		 
		$this->data['result_members'] = $this->applications->getFamilyMemberAccompanyingByCounseling($this->data["application"]->counseling_id);
		
		$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('evaluation_refugee_form')));
		$meta = array('page_title' => lang('evaluation_refugee_form'), 'bc' => $bc);
		$this->page_construct('applications/procedure_decision_form',$meta, $this->data);
	}
	
	public function extension_notification($id = false)
	{
		$this->erp->checkPermissions('extension_notification', null, 'evaluation_refugees');
		$this->data['evaluation'] = $this->applications->getEvaluationById($id);
		$this->data['departments'] = $this->applications->getDepartmentDirector();
		$this->data["result"] = $this->applications->getRSDApplicationById($this->data['evaluation']->application_id);
		$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('extension_notification')));
		$meta = array('page_title' => lang('extension_notification'), 'bc' => $bc);	
		$this->page_construct('applications/extension_notification',$meta, $this->data);
	}
	
	public function delete_evaluation($id =false)
	{
		$this->erp->checkPermissions('delete', null, 'evaluation_refugees');
		if($id){
			if($this->applications->deleteEvaluation($id)){
				$this->session->set_flashdata('message', lang("evaluation_deleted"));
				redirect($_SERVER['HTTP_REFERER']);
			}
		}
	}
	
	public function review_evaluation($id = false)
	{
		$this->erp->checkPermissions('review_evaluation', null, 'evaluation_refugees');
		if($id){
			$data = array(
						"reviewed_by" => $this->session->userdata("user_id"),
						"reviewed_date" => $this->erp->fld(date("d/m/Y H:i")),
						"status" => "reviewed",
					);
			if($this->applications->updateEvaluation($id,$data)){
				$this->session->set_flashdata('message', lang("evaluation_reviewed"));
				redirect($_SERVER['HTTP_REFERER']);
			}
		}
	}
	
	public function approve_evaluation($id = false)
	{ 
		if($_GET['v'] == "appeal") {  
			$data_status = array("procedure_sequence"=>"approved_appeal");
		}else {  
			$data_status = array("procedure_sequence"=>"approved_1"); 		
		}
		$result = $this->applications->getEvaluationById($id);	
		if($id){			
			$data = array(
						"approved_by" => $this->session->userdata("user_id"),
						"approved_date" => $this->erp->fld(date("d/m/Y H:i")),
						"status" => "approved",
					);
			if($this->applications->updateEvaluation($id,$data) && $this->applications->updateRSDApplication($result->application_id, $data_status)){
				$this->session->set_flashdata('message', lang("approve_evaluation_refugee"));
				redirect($_SERVER['HTTP_REFERER']);
			}
		}
	}
	
	public function reject_evaluation($id = false)
	{
		if($_GET['v'] == "appeal") { 
			$data_status = array("procedure_sequence"=>"rejected_appeal");
		}else {  
			$data_status = array("procedure_sequence"=>"rejected_1");		
		}
		$result = $this->applications->getEvaluationById($id);	
		if($id){			
			$data = array(
							"rejected_by" => $this->session->userdata("user_id"),
							"rejected_date" => $this->erp->fld(date("d/m/Y H:i")),
							"status" => "rejected"
						);
			if($this->applications->updateEvaluation($id,$data)  && $this->applications->updateRSDApplication($result->application_id, $data_status)){
				$this->session->set_flashdata('message', lang("reject_evaluation_refugee"));
				redirect($_SERVER['HTTP_REFERER']);
			}
		}
	}
	
	public function attach_evaluation_refugee($id = false) 
	{ 	  
		$result = $this->applications->getEvaluationById($id);
		$this->erp->checkPermissions('attachment', null, 'evaluation_refugees');	
		$this->global_model->attach_forms('fa_interview_evaluations', $id, 'evaluation_refugee');
		$this->data['action_method'] = 'attach_evaluation_refugee';
		$this->data['delete_url'] = 'delete_attach_evaluation_refugee';
		$this->data['view_folder'] = 'evaluation_refugee';
		$this->data['contorler'] = $this->uri->segment(1);
		$this->data['id'] = $id;
		$this->data['result'] = $result; 
		$this->data['modal_js'] = $this->site->modal_js();		
		$this->load->view($this->theme . 'global/view_attach_forms', $this->data);
	}
	
	public function delete_attach_evaluation_refugee($id = false, $name = null, $img = null)
	{
		$result = $this->applications->getEvaluationById($id);
		$this->global_model->delete_attach_forms('fa_interview_evaluations', $id, $name, $img);
	}  
	
	/**
	* 
	* NEGATIVE REFUGEE
	* @return
	*/ 
	
	public function negative_refugee()
	{
		$this->erp->checkPermissions('index', null, 'negative_refugee');
		$this->data["result"] = "";
		$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('negative_refugee')));
		$meta = array('page_title' => lang('negative_refugee'), 'bc' => $bc);	
		$this->page_construct('applications/negative_refugee',$meta, $this->data); 
	}
	
	public function negative_evaluate_refugee_appeal()
	{
		$this->erp->checkPermissions('index', null, 'negative_evaluate_refugee_appeal');
		$this->data["result"] = "";
		$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('negative_evaluate_refugee_appeal')));
		$meta = array('page_title' => lang('negative_evaluate_refugee_appeal'), 'bc' => $bc);	
		$this->page_construct('applications/negative_evaluate_refugee_appeal',$meta, $this->data); 
	}
	
	public function getNegativeRefugee()
	{
		$this->load->library('datatables');	
		
		$delete = "<a href='#' class='delete-nv po' title='" . lang("delete_negative") . "' data-content=\"<p>"
            . lang('r_u_sure') . "</p><a class='btn btn-danger' href='" . site_url('applications/delete_negative/$1') . "'>"
            . lang('i_m_sure') . "</a><button class='btn'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i>"
            . lang('delete_negative') . "</a>";
			
		$closing_refugee_case = "<a href='#' class='close-nv po' title='" . lang("closing_refugee_case") . "' data-content=\"<p>"
            . lang('r_u_sure') . "</p><a class='btn btn-danger' href='" . site_url('applications/closing_refugee_case/$1') . "'>"
            . lang('i_m_sure') . "</a><button class='btn'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-warning\"></i>"
            . lang('closing_refugee_case') . "</a>";
			
		$action_link = '<div class="btn-group text-left"><button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">'.lang("actions").'<span class="caret"></span></button>
					        <ul class="dropdown-menu pull-right" role="menu">					        																									
					            <li><a target="_blank" href="'.site_url('applications/negative_rsd_form/$1').'" ><i class="fa fa-newspaper-o"></i>'.lang('negative_refugee').'</a></li>					            
					            <li><a class="edit-nv" data-toggle="modal" data-target="#myModal" href="'.site_url('applications/edit_negative/$1').'" ><i class="fa fa-pencil-square-o"></i>'.lang('edit_negative').'</a></li>
					            <li><a class="file-nv" data-backdrop="static" data-keyboard="false"  data-toggle="modal" data-target="#myModal" href="'.site_url('applications/attach_negative_refugee/$1').'" ><i class="fa fa-plus-circle"></i>'.lang('attachment').'</a></li>
								<li>'.$closing_refugee_case.'</li>
								<li>'.$delete.'</li>
							</ul>
					    </div>'; 
			$this->datatables->select("
					fa_interview_negatives.id as id, 
					CONCAT(erp_fa_rsd_applications.case_prefix,erp_fa_rsd_applications.case_no) as case_no,
					UPPER(firstname) AS firstname,
					UPPER(lastname) AS lastname,
					dob,
					UPPER(gender) AS gender,
					UPPER(nationality) AS nationality,
					fa_interview_negatives.created_date,
					fa_interview_negatives.negative_date, 
					fa_interview_negatives.provided_date,
					fa_interview_negatives.status,
					resubmit,
					", false)
            ->from("fa_rsd_applications")
            ->where("fa_interview_negatives.status <> 'deleted' ")
			->join("fa_interview_negatives","application_id=fa_rsd_applications.id","inner")
			->order_by("fa_interview_negatives.created_date",'desc');
			
			if($this->input->get('date_from') && $this->input->get('date_to')){
				$date_from = $this->input->get("date_from");
				$date_to   = $this->input->get("date_to");
				$this->datatables->where("DATE(erp_fa_interview_negatives.created_date) >=",$this->erp->fld($date_from));
				$this->datatables->where("DATE(erp_fa_interview_negatives.created_date) <=",$this->erp->fld($date_to));
			} 
				
			if($this->input->get('case_no')){
				$this->datatables->where("CONCAT(case_prefix, case_no)=",$this->input->get('case_no'));
			}

		if($this->input->get("type")=="is_appeal"){ 
			$this->datatables->where("fa_interview_negatives.resubmit =",1);
		}else {
			$this->datatables->where("fa_interview_negatives.resubmit <>",1);
		} 
		
			
			
		if($this->input->get("id")){
			$ids = explode(",",$this->input->get("id"));
			$negative_id = array();
			foreach($ids as $id){
				$negative_id[] = $id;
			}
			$this->db->where_in("fa_interview_negatives.id",$negative_id);
		}		
		if($this->input->get("type") == "notification"){ 
			$this->datatables->where(" 
						erp_fa_interview_negatives_appeal. STATUS = 'pending'
						WHERE
						AND erp_fa_interview_negatives_appeal.application_id NOT IN (
						SELECT
							application_id
						FROM
							fa_interview_negatives)
			
			");
		} 
        $this->datatables->add_column("Actions", $action_link, "id,ids");
        echo $this->datatables->generate();
	}
	
	public function closing_refugee_case($id = NULL) 
	{
		$applications = $this->applications->getNegative($id);
			
		if($id) {
			$data = array( 
				"status" => "closed",
			);
			if($this->applications->updateNegative($id, $data) && $this->applications->updateRSDApplication($applications->application_id, $data) ) {
				$this->session->set_flashdata('message', lang("edit_negative"));
				redirect($_SERVER['HTTP_REFERER']);
			} 
		}
	}
	
	public function closing_refugee_case_appeal($id = NULL) 
	{ 
		$applications = $this->applications->getNegativeAppeal($id);
		if($id){			
			$data = array(
						"status" => "closed",
					);
			if($this->applications->updateNegativeAppeal($id,$data) && $this->applications->updateRSDApplication($applications->application_id, $data) ){
				$this->session->set_flashdata('message', lang("closed"));
				redirect("applications/negative_refugee_appeal");
			}
		}
	}
	
	public function negative_rsd_on_appeal($id=false)
	{
		$this->data['evaluation'] = $this->applications->getEvaluationById($id);
		$this->data['application'] = $this->applications->getRSDApplicationById($this->data['evaluation']->application_id);
		
		$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('negative_refugee')));
		$meta = array('page_title' => lang('negative_refugee'), 'bc' => $bc);	
		$this->page_construct('applications/negative_rsd_on_appeal',$meta, $this->data);
	} 
	
	public function negative_rsd_form($id = false)
	{
		$this->erp->checkPermissions('application_form', null, 'negative_refugee');
		$this->data['evaluation'] = $this->applications->getNegative($id);
		$this->data['application'] = $this->applications->getRSDApplicationById($this->data['evaluation']->application_id);
		$this->data['departments'] = $this->applications->getDepartmentDirector();		
		$this->data["result"] = $results;
		
		$this->data['result_members'] = $this->applications->getFamilyMemberAccompanyingByCounseling($this->data["application"]->counseling_id);
		
		$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('negative_refugee')));
		$meta = array('page_title' => lang('negative_refugee'), 'bc' => $bc);	
		$this->page_construct('applications/negative_rsd_form',$meta, $this->data);
	}
	
	public function add_negative() 
	{ 
		$post = $this->input->post();		
		if($post) {
			$this->erp->checkPermissions('add', null, 'negative_refugee');
			$data = array( 
				"created_by"   => $this->session->userdata("user_id"),
				"created_date"   => date("Y-m-d H:i"),
				"application_id" => $post['application_id'],
				"resubmit" => $post['resubmit']==1?1:0,
				"status" => "pending",
			); 
			if($this->applications->addNegative($data)) {
				$this->session->set_flashdata('message', lang("add_negative"));
				redirect($_SERVER['HTTP_REFERER']);
			} 
		} 
		$this->data['id'] = $id;
		$this->data['modal_js'] = $this->site->modal_js();		
		$this->load->view($this->theme . 'applications/add_negative', $this->data);		
	}
	
	public function edit_negative($id = false) 
	{
		$post = $this->input->post();		
		if($post) {
			$this->erp->checkPermissions('edit', null, 'negative_refugee');
			$data = array(
				"negative_no"   => $post["negative_no"],
				"negative_date"  => $this->erp->fld($post['negative_date']),
				"provided_date"  => $this->erp->fld($post['provided_date']),
				"status" => "approved",
			);

			if($this->applications->updateNegative($id, $data)) {
				$this->session->set_flashdata('message', lang("edit_negative"));
				redirect($_SERVER['HTTP_REFERER']);
			} 
		} 
		$this->data['id'] = $id;
		$this->data['negative'] = $this->applications->getNegative($id); 
		$this->data['modal_js'] = $this->site->modal_js();		
		$this->load->view($this->theme . 'applications/edit_negative', $this->data);		
	}
	
	public function attach_negative_refugee($id = false) 
	{ 	 	 
		$this->erp->checkPermissions('attachment', null, 'negative_refugee');
		$result = $this->applications->getNegative($id); 
		$this->global_model->attach_forms('fa_interview_negatives', $id, 'negative_refugee');
		$this->data['action_method'] = 'attach_negative_refugee';
		$this->data['delete_url'] = 'delete_attach_negative_refugee';
		$this->data['view_folder'] = 'negative_refugee';
		$this->data['contorler'] = $this->uri->segment(1);
		$this->data['id'] = $id;
		$this->data['result'] = $result; 
		$this->data['modal_js'] = $this->site->modal_js();		
		$this->load->view($this->theme . 'global/view_attach_forms', $this->data);
		
	}
	public function delete_attach_negative_refugee($id = false, $name = null, $img = null) 
	{
		$this->erp->checkPermissions('delete', null, 'negative_refugee_appeal');
		$result = $this->applications->getNegative($id); 
		$this->global_model->delete_attach_forms('fa_interview_negatives', $id, $name, $img);
	}  
	public function attach_negative_refugee_appeal($id = false)
	{
		$result = $this->applications->getNegativeAppeal($id); 
		$this->erp->checkPermissions('attachment', null, 'negative_refugee_appeal');
		$this->global_model->attach_forms('fa_interview_negatives_appeal', $id, 'negative_refugee_appeal');
		$this->data['action_method'] = 'attach_negative_refugee_appeal';
		$this->data['delete_url'] = 'delete_attach_negative_refugee_appeal';
		$this->data['view_folder'] = 'negative_refugee_appeal';
		$this->data['contorler'] = $this->uri->segment(1);
		$this->data['id'] = $id;
		$this->data['result'] = $result; 
		$this->data['modal_js'] = $this->site->modal_js();		
		$this->load->view($this->theme . 'global/view_attach_forms', $this->data);
	}
	
	public function delete_attach_negative_refugee_appeal($id = false, $name = null, $img = null) 
	{
		$result = $this->applications->getNegativeAppeal($id); 
		$this->erp->checkPermissions('delete', null, 'negative_refugee_appeal');
		$this->global_model->delete_attach_forms('fa_interview_negatives_appeal', $id, $name, $img);
	}    
	
	
	
	public function delete_negative($id = false)
	{	
		$this->erp->checkPermissions('delete', null, 'negative_refugee_appeal');
		$this->erp->checkPermissions('delete', null, 'negative_refugee');
		if($id){
			if($this->applications->deleteNegative($id)){
				$this->session->set_flashdata('message', lang("delete_negative"));
				redirect($_SERVER['HTTP_REFERER']);
			}
		}
	}
	
	
	/**
	* 
	* NAGATIVE REFUGEE APPEAL
	* @return
	*/ 
	
	public function negative_refugee_appeal()
	{
		$this->erp->checkPermissions('index', null, 'negative_refugee_appeal');
		$this->data["result"] = "";
		$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('negative_refugee_appeal')));
		$meta = array('page_title' => lang('negative_refugee_appeal'), 'bc' => $bc);	
		$this->page_construct('applications/negative_refugee_appeal',$meta, $this->data); 
	}
		
	public function reject_negative_refugee_appeal($id = false)
	{ 
		$result = $this->applications->getNegativeAppealById($id);
		if($id){			
			$data = array(
						"rejected_by" => $this->session->userdata("user_id"),
						"rejected_date" => $this->erp->fld(date("d/m/Y H:i")),
						"status" => "rejected",
					);
			if($this->applications->updateNegativeAppeal($id,$data) && $this->applications->updateRSDApplication($result->application_id, array("procedure_sequence"=>"rejected_request_appeal"))){
				$this->session->set_flashdata('message', lang("reject_negative_refugee_appeal"));
				redirect("applications/negative_refugee_appeal");
			}
		}
	}
	
	public function approve_negative_refugee_appeal($id = false)
	{
		$result = $this->applications->getNegativeAppealById($id);
		if($id){			
			$data = array(
						"approved_by" => $this->session->userdata("user_id"),
						"approved_date" => $this->erp->fld(date("d/m/Y H:i")),
						"status" => "approved",
					);
			if($this->applications->updateNegativeAppeal($id,$data) && $this->applications->updateRSDApplication($result->application_id, array("procedure_sequence"=>"approved_request_appeal"))){
				$this->session->set_flashdata('message', lang("approve_negative_refugee_appeal"));
				redirect("applications/negative_refugee_appeal");
			}
		}
	}
	
	public function getNegativeRefugeeAppeal()
	{
		$this->load->library('datatables');
		$delete = "<a href='#' class='delete-nv po' title='" . lang("delete_negative") . "' data-content=\"<p>"
            . lang('r_u_sure') . "</p><a class='btn btn-danger' href='" . site_url('applications/delete_negative_appeal/$1') . "'>"
            . lang('i_m_sure') . "</a><button class='btn'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i>"
            . lang('delete_negative_appeal') . "</a>";
		
		$closing_refugee_case = "<a href='#' class='close-nv po' title='" . lang("closing_refugee_case") . "' data-content=\"<p>"
            . lang('r_u_sure') . "</p><a class='btn btn-danger' href='" . site_url('applications/closing_refugee_case_appeal/$1') . "'>"
            . lang('i_m_sure') . "</a><button class='btn'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-warning\"></i>"
            . lang('closing_refugee_case') . "</a>";
			
		$reject = "<a href='#' class='reject-nv po' title='" . lang("reject_negative_refugee_appeal") . "' data-content=\"<p>"
            . lang('r_u_sure') . "</p><a class='btn btn-danger' href='" . site_url('applications/reject_negative_refugee_appeal/$1') . "'>"
            . lang('i_m_sure') . "</a><button class='btn'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-times\"></i>"
            . lang('reject_negative_refugee_appeal') . "</a>";
		
		$approve = "<a href='#' class='approve-nv po' title='" . lang("approve_negative_refugee_appeal") . "' data-content=\"<p>"
            . lang('r_u_sure') . "</p><a class='btn btn-danger' href='" . site_url('applications/approve_negative_refugee_appeal/$1') . "'>"
            . lang('i_m_sure') . "</a><button class='btn'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-check-square-o\"></i>"
            . lang('approve_negative_refugee_appeal') . "</a>";
			 
		$action_link = '<div class="btn-group text-left"><button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">'.lang("actions").'<span class="caret"></span></button>
					        <ul class="dropdown-menu pull-right" role="menu">
								<li>'.$reject.'</li>
								<li>'.$approve.'</li>  
								<li><a class="interview-nv" data-backdrop="static" data-keyboard="false"  data-toggle="modal" data-target="#myModal" href="'.site_url('applications/add_appointment_refugee/$1?q=is_appeal').'" ><i class="fa fa-plus-circle"></i>'.lang('interview_appointment_appeal').'</a></li>
								<li>'.$closing_refugee_case.'</li>
								<li>'.$delete.'</li>
								<li><a data-backdrop="static" data-keyboard="false"  data-toggle="modal" data-target="#myModal" href="'.site_url('applications/attach_negative_refugee_appeal/$1').'" ><i class="fa fa-plus-circle"></i>'.lang('attachment').'</a></li>
							</ul>
					    </div>'; 
			$this->datatables->select("
					fa_interview_negatives_appeal.id as id,
					CONCAT(case_prefix,case_no) as case_no,
					UPPER(firstname) AS firstname,
					UPPER(lastname) AS lastname,
					dob,
					UPPER(gender) AS gender,
					UPPER(nationality) AS nationality,
					fa_interview_negatives_appeal.created_date,					
					fa_interview_negatives_appeal.status,
					", false)
            ->from("fa_rsd_applications")
            ->where("fa_interview_negatives_appeal.status <> 'deleted' ")
			->join("fa_interview_negatives_appeal","application_id=fa_rsd_applications.id","inner")
			->order_by("fa_interview_negatives_appeal.created_date","desc");
			
			if($this->input->get('date_from') && $this->input->get('date_to')){
				$date_from = $this->input->get("date_from");
				$date_to   = $this->input->get("date_to");
				$this->datatables->where("DATE(erp_fa_interview_negatives_appeal.created_date) >=",$this->erp->fld($date_from));
				$this->datatables->where("DATE(erp_fa_interview_negatives_appeal.created_date) <=",$this->erp->fld($date_to));
			} 
				
			if($this->input->get('case_no')){
				$this->datatables->where("CONCAT(case_prefix, case_no)=",$this->input->get('case_no'));
			}

		if($this->input->get("id")){
			$ids = explode(",",$this->input->get("id"));
			$negative_id = array();
			foreach($ids as $id){
				$negative_id[] = $id;
			}
			$this->db->where_in("fa_interview_negatives_appeal.id",$negative_id);
		}		
        $this->datatables->add_column("Actions", $action_link, "id");
        echo $this->datatables->generate();
	}
	
	public function delete_negative_appeal($id = false)
	{	
		$this->erp->checkPermissions('delete', null, 'negative_refugee_appeal');
		if($id){
			if($this->applications->deleteNegativeAppeal($id)){
				$this->session->set_flashdata('message', lang("delete_negative_appeal"));
				redirect($_SERVER['HTTP_REFERER']);
			}
		}
	}
	
	public function add_negative_appeal() 
	{
		$post = $this->input->post();		
		if($post) {
			$this->erp->checkPermissions('add', null, 'negative_refugee_appeal');
			$data = array(				
				"created_by"   => $this->session->userdata("user_id"),
				"created_date"   => date("Y-m-d H:i"),
				"appeal_date"   => $this->erp->fld($post['appeal_date']),
				"application_id" => $post['application_id'],				
				"status" => "pending",
			);
			$config =array(
				'upload_path' => 'assets/uploads/',
				'allowed_types' => 'doc|pdf'
			); 						
			$this->load->library('upload', $config);			  			
			if($this->upload->do_upload('file')){ 
				$data['attachment'] = $this->upload->data("file_name");
			}			
			if($this->applications->addNegativeAppeal($data)  && $this->applications->updateRSDApplication($post['application_id'], array("procedure_sequence"=>"request_appeal"))) {
				$this->session->set_flashdata('message', lang("add_negative_appeal"));
				redirect($_SERVER['HTTP_REFERER']);
			} 
		} 
		$this->data['id'] = $id;
		$this->data['modal_js'] = $this->site->modal_js();		
		$this->load->view($this->theme . 'applications/add_negative_appeal', $this->data);		
	}
	
	/**
	* 
	* SUGGESTION REFUGEE
	* @return
	*/ 
	public function capture()
	{
		$filename =  time() . '.jpg';
		$filepath = 'assets/uploads/photos/';
		move_uploaded_file($_FILES['webcam']['tmp_name'], $filepath.$filename);
		echo $filepath.$filename;
	}
	
	public function suggestions($q = false)
	{
		$q = $this->input->get('q', TRUE);
		if(!empty($q)){
			$jsons = array();
			$results = $this->applications->getSuggestionsCounseling($q);
			if(count($results) > 0){
				foreach($results as $row){
					 $jsons[] = array('id' => $row->id, 'label' => " (".$row->counseling_no.") ".$row->firstname . " " . $row->lastname, 'row' => $row);
				}
				echo json_encode($jsons);
			}else{		
				echo json_encode(array(array('id' => 0, 'label' => lang('no_match_found'), 'value' => $q)));
			}
		}
	} 	
	
	public function family_suggestions_caseno($id = false)
	{
		$id = $this->input->get('id', TRUE);  
		$result = $this->applications->getRSDApplicationById($id);
		$result_members=  $this->applications->getFamilyMemberAccompanyingByCounseling($result->counseling_id);
 		echo json_encode($result_members); 
	} 
	 
	public function suggestions_caseno($q = false)
	{
		$q = $this->input->get('q', TRUE);
		if(!empty($q)){
			$jsons = array(); 
			$results = $this->applications->getSuggestionsCaseNo($q); 
			if(count($results) > 0){
				foreach($results as $row){
					$village = $this->applications->getTagsById($row->village);
					$row->application_id = $row->id;
					$row->village = $village;
					$row->dob = $this->erp->hrsd($row->dob); 
					$jsons[] = array('id' => $row->id, 'label' => " (".$row->case_prefix.$row->case_no.") ".$row->firstname . " " . $row->lastname, 'row' => $row);
				}
				echo json_encode($jsons);
			}else{		
				echo json_encode(array(array('id' => 0, 'label' => lang('no_match_found'), 'value' => $q)));
			}
		}
	}
	 
	public function suggestions_caseno_withdrawal($q = false) {
		$q = $this->input->get('q', TRUE);
		if(!empty($q)){
			$jsons = array(); 
			$results = $this->applications->getSuggestionsCaseNoWithdrawal($q); 
			if(count($results) > 0){
				foreach($results as $row){
					$village = $this->applications->getTagsById($row->village);
					$row->application_id = $row->id;
					$row->village = $village;
					$row->dob = $this->erp->hrsd($row->dob); 
					$jsons[] = array('id' => $row->id, 'label' => " (".$row->case_prefix.$row->case_no.") ".$row->firstname . " " . $row->lastname, 'row' => $row);
				}
				echo json_encode($jsons);
			}else{		
				echo json_encode(array(array('id' => 0, 'label' => lang('no_match_found'), 'value' => $q)));
			}
		}
		 
	 }
	
	public function suggestions_caseno_social_affair_add_form($q = false)
	{
		$q = $this->input->get('q', TRUE);
		$type = $this->input->get('type', TRUE);
		if(!empty($q)){
			$jsons = array(); 
			$results = $this->applications->getSuggestionsCaseNoSocialAffairForAddRe($q,$type); 
			if(count($results) > 0){
				foreach($results as $row){
					$village = $this->applications->getTagsById($row->village);
					$row->application_id = $row->id;
					$row->village = $village;
					$row->dob = $this->erp->hrsd($row->dob); 
					$jsons[] = array('id' => $row->id, 'label' => " (".$row->case_prefix.$row->case_no.") ".$row->firstname . " " . $row->lastname, 'row' => $row);
				}
				echo json_encode($jsons);
			}else{		
				echo json_encode(array(array('id' => 0, 'label' => lang('no_match_found'), 'value' => $q)));
			}
		}
	}
	public function suggestions_caseno_social_affair($q = false)
	{
		$q = $this->input->get('q', TRUE);
		$type = $this->input->get('type', TRUE);
		if(!empty($q)){
			$jsons = array(); 
			$results = $this->applications->getSuggestionsCaseNoSocialAffair($q,$type); 
			if(count($results) > 0){
				foreach($results as $row){
					$village = $this->applications->getTagsById($row->village);
					$row->application_id = $row->id;
					$row->village = $village;
					$row->dob = $this->erp->hrsd($row->dob); 
					$jsons[] = array('id' => $row->id, 'label' => " (".$row->case_prefix.$row->case_no.") ".$row->firstname . " " . $row->lastname, 'row' => $row);
				}
				echo json_encode($jsons);
			}else{		
				echo json_encode(array(array('id' => 0, 'label' => lang('no_match_found'), 'value' => $q)));
			}
		}
	}
	 
	public function suggestions_caseno_appeal($q = false)
	{
		$q = $this->input->get('q', TRUE);
		if(!empty($q)){
			$jsons = array(); 
			$results = $this->applications->getSuggestionsCaseNoAppeal($q); 
			if(count($results) > 0){
				foreach($results as $row){
					$village = $this->applications->getTagsById($row->village);
					$row->application_id = $row->id;
					$row->village = $village;
					$row->dob = $this->erp->hrsd($row->dob); 
					$jsons[] = array('id' => $row->id, 'label' => " (".$row->case_prefix.$row->case_no.") ".$row->firstname . " " . $row->lastname, 'row' => $row);
				}
				echo json_encode($jsons);
			}else{		
				echo json_encode(array(array('id' => 0, 'label' => lang('no_match_found'), 'value' => $q)));
			}
		}
	}
	
	public function suggestions_caseno_preliminary_stay($q = false)
	{
		$q = $this->input->get('q', TRUE);
		if(!empty($q)){
			$jsons = array();
			$results = $this->applications->getSuggestionsCaseNoPreliminaryStay($q);
			if(count($results) > 0){
				foreach($results as $row){
					$village = $this->applications->getTagsById($row->village);
					$row->application_id = $row->id;
					$row->village = $village;
					$row->dob = $this->erp->hrsd($row->dob); 
					$jsons[] = array('id' => $row->id, 'label' => " (".$row->case_prefix.$row->case_no.") ".$row->firstname . " " . $row->lastname, 'row' => $row);
				}
				echo json_encode($jsons);
			}else{		
				echo json_encode(array(array('id' => 0, 'label' => lang('no_match_found'), 'value' => $q)));
			}
		}
	}
	
	public function suggestions_caseno_recognition($q = false)
	{
		$q = $this->input->get('q', TRUE);
		if(!empty($q)){
			$jsons = array();
			$results = $this->applications->getSuggestionsCaseNoRecognition($q);
			if(count($results) > 0){
				foreach($results as $row){
					$village = $this->applications->getTagsById($row->village);
					 $row->application_id = $row->id;
					 $row->village = $village;
					 $row->dob = $this->erp->hrsd($row->dob); 
					$jsons[] = array('id' => $row->id, 'label' => " (".$row->case_prefix.$row->case_no.") ".$row->firstname . " " . $row->lastname, 'row' => $row);
				}
				echo json_encode($jsons);
			}else{		
				echo json_encode(array(array('id' => 0, 'label' => lang('no_match_found'), 'value' => $q)));
			}
		}
	}
	
	public function suggestions_caseno_refugee_card($q = false)
	{
		$q = $this->input->get('q', TRUE);
		if(!empty($q)){
			$jsons = array();
			$results = $this->applications->getSuggestionsCaseNoRefugeeCard($q);
			if(count($results) > 0){
				foreach($results as $row){
					$village = $this->applications->getTagsById($row->village);
					 $row->application_id = $row->id;
					 $row->village = $village;
					 $row->dob = $this->erp->hrsd($row->dob); 
					$jsons[] = array('id' => $row->id, 'label' => " (".$row->case_prefix.$row->case_no.") ".$row->firstname . " " . $row->lastname, 'row' => $row);
				}
				echo json_encode($jsons);
			}else{		
				echo json_encode(array(array('id' => 0, 'label' => lang('no_match_found'), 'value' => $q)));
			}
		}
	}
	
	public function suggestions_caseno_rejected($q = false)
	{
		$q = $this->input->get('q', TRUE);
		if(!empty($q)){
			$jsons = array();
			$results = $this->applications->getSuggestionsCaseNoRejected($q);
			if(count($results) > 0){
				foreach($results as $row){
					$village = $this->applications->getTagsById($row->village);
					 $row->application_id = $row->id;
					 $row->village = $village;
					 $row->dob = $this->erp->hrsd($row->dob); 
					$jsons[] = array('id' => $row->id, 'label' => " (".$row->case_prefix.$row->case_no.") ".$row->firstname . " " . $row->lastname, 'row' => $row);
				}
				echo json_encode($jsons);
			}else{		
				echo json_encode(array(array('id' => 0, 'label' => lang('no_match_found'), 'value' => $q)));
			}
		}
	}
	
	public function suggestions_caseno_negative($q = false)
	{
		$q = $this->input->get('q', TRUE);
		if(!empty($q)){
			$jsons = array();
			$results = $this->applications->getSuggestionsCaseNoNegative($q);
			if(count($results) > 0){
				foreach($results as $row){
					$village = $this->applications->getTagsById($row->village);
					$row->application_id = $row->id;
					$row->village = $village;
					$row->dob = $this->erp->hrsd($row->dob); 
					$jsons[] = array('id' => $row->id, 'label' => " (".$row->case_prefix.$row->case_no.") ".$row->firstname . " " . $row->lastname, 'row' => $row);
				}
				echo json_encode($jsons);
			}else{		
				echo json_encode(array(array('id' => 0, 'label' => lang('no_match_found'), 'value' => $q)));
			}
		}
	}
	 
	public function suggestions_caseno_negative_appeal($q = false)
	{
		$q = $this->input->get('q', TRUE);
		if(!empty($q)){
			$jsons = array();
			$results = $this->applications->getSuggestionsCaseNoNegativeAppeal($q);
			if(count($results) > 0){
				foreach($results as $row){
					$village = $this->applications->getTagsById($row->village);
					$row->application_id = $row->id;
					$row->village = $village;
					$row->dob = $this->erp->hrsd($row->dob); 
					$jsons[] = array('id' => $row->id, 'label' => " (".$row->case_prefix.$row->case_no.") ".$row->firstname . " " . $row->lastname, 'row' => $row);
				}
				echo json_encode($jsons);
			}else{		
				echo json_encode(array(array('id' => 0, 'label' => lang('no_match_found'), 'value' => $q)));
			}
		}
	} 
	
	public function suggestions_caseno_declare_appeal($q = false)
	{
		$q = $this->input->get('q', TRUE);
		if(!empty($q)){
			$jsons = array();
			$results = $this->applications->getSuggestionsCaseNoDeclareAppeal($q);
			if(count($results) > 0){
				foreach($results as $row){
					$village = $this->applications->getTagsById($row->village);
					$row->application_id = $row->id;
					$row->village = $village;
					$row->dob = $this->erp->hrsd($row->dob); 
					$jsons[] = array('id' => $row->id, 'label' => " (".$row->case_prefix.$row->case_no.") ".$row->firstname . " " . $row->lastname, 'row' => $row);
				}
				echo json_encode($jsons);
			}else{		
				echo json_encode(array(array('id' => 0, 'label' => lang('no_match_found'), 'value' => $q)));
			}
		}
	} 
	
	public function suggestions_appointments($q = false)
	{
		$q = $this->input->get('q', TRUE);
		if(!empty($q)){
			$jsons = array();
			$results = $this->applications->getSuggestionsAllAppointments($q);
			if(count($results) > 0){
				foreach($results as $row){
					$appointment = $this->applications->getAppointmentByApplicationId($row->id);
					$row->application_id = $row->id;
					$row->appointment_id = $appointment->id;
					$row->appointment_date = $appointment->appointment_date;
					$jsons[] = array('id' => $row->id, 'label' => " (".$row->case_prefix.$row->case_no.") ".$row->firstname . " " . $row->lastname ." | ". $this->erp->hrsd($row->appointment_date), 'row' => $row);
				}
				echo json_encode($jsons);
			}else{		
				echo json_encode(array(array('id' => 0, 'label' => lang('no_match_found'), 'value' => $q)));
			}
		}
	}
	
	public function suggestions_interview_caseno($q = false)
	{
		$q = $this->input->get('q', TRUE);
		if(!empty($q)){
			$jsons = array();
			$results = $this->applications->getSuggestionsInterviewCaseNo($q);
			if(count($results) > 0){
				foreach($results as $row){
					$interview = $this->applications->getInterviewByApplicationId($row->id);
					$row->application_id = $row->id;
					$row->interview_id = $interview->id;
					$row->interview_date = $interview->interview_date;
					
					$jsons[] = array('id' => $row->id, 'label' => " (".$row->case_prefix.$row->case_no.") ".$row->firstname . " " . $row->lastname . " | Interview :" . $this->erp->hrsd($row->interview_date) , 'row' => $row);
				}
				echo json_encode($jsons);
			}else{		
				echo json_encode(array(array('id' => 0, 'label' => lang('no_match_found'), 'value' => $q)));
			}
		}
	}
	
	public function getFamilyRecognitionRefugees()
	{
		$id = $this->input->get('id', TRUE);  
		$result = $this->applications->getRSDApplicationById($id);
		$result_members =  $this->applications->getFamilyMemberAccompanyingByCounseling($result->counseling_id);
		$html = "";
		foreach($result_members as $result){
			$html .="<tr>";
				$html .= "<td>";
					$html .="<input class='checkbox' name='members[]' value='".$result->id."' type='checkbox'/>";
				$html  .= "</td>";
				$html .= "<td class='center'>".$result->lastname_kh." ".$result->firstname_kh."</td>";
				$html .= "<td class='center'>".lang($result->gender)."</td>";
				$html .= "<td class='center'>".$result->relationship."</td>";
				$html .= "<td class='center'>".$this->erp->hrsd($result->dob)."</td>";
			$html .="</tr>";
		}
		echo json_encode($html);
	}

	public function family_come_status($id = false) 
	{   
		$status = $this->input->get("status");
		if(!empty($id)){ 
			$result = $this->applications->updateHomeVisitStatus($id,$status); 
			if($result) {
				 echo "success";
			}else{		
				 
				 echo "unsuccess";
			}
		} 
	}
 	
/*
	Description: To update status to miss_interview
*/
 	public function miss_interview_appointment_refugee($id = false) 
	{
		$this->erp->checkPermissions('delete', null, 'appointment_refugees');
		if($id){			
			if($this->applications->missInterviewAppointmentRefugee($id)){
				$this->session->set_flashdata('message', lang("missInterview"));
				redirect($_SERVER['HTTP_REFERER']);
			}
		} 
	}
	
}
	
?>