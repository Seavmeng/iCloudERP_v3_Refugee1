<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Application_forms2 extends MY_Controller
{
	
	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->library('pagination');
		$this->load->model('applications_model','applications');
		$this->load->model('global_model');
		$this->load->model('application_setup_model');
		$this->lang->load('applications', $this->Settings->language);
		$this->lang->load('application_forms', $this->Settings->language);
		$this->load->model('Site','site');
		$this->site->auditLogs();
	}

	/**
	* 
	* REFUGEE HOUSE ADDRESS CHANGE REQUEST
	* @return
	*/ 
	
	public function refugee_house_address_change_request() 
	{				 		
		$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('refugee_house_address_change_request')));
		$meta = array('page_title' => lang('refugee_house_address_change_request'), 'bc' => $bc);	
		$this->page_construct('application_forms2/refugee_house_address_change_request',$meta, $this->data);
	}
	
	public function refugee_house_address_change_request_form($id = false) 
	{  
		$this->data['row'] = $this->applications->getRefugeeAddressChangeById($id);
		$this->data["result"] = $this->applications->getRSDApplicationById($this->data['row']->application_id);
		$this->data["decision"] = $this->applications->getRefugeeRecognitionByApplicationId($this->data['row']->application_id);
		
		$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('refugee_house_address_change_request_form')));
		$meta = array('page_title' => lang('refugee_house_address_change_request_form'), 'bc' => $bc);	
		$this->page_construct('application_forms2/refugee_house_address_change_request_form',$meta, $this->data);
	}
	
	public function getRefugeeHouseAddressChangeRequest()
	{
		$this->load->library('datatables');
		
		$delete = "<a href='#' class='delete-refugee po' title='" . lang("delete_refugee_house_address_change") . "' data-content=\"<p>"
            . lang('r_u_sure') . "</p><a class='btn btn-danger' href='" . site_url('application_forms2/delete_refugee_house_address_change/$1') . "'>"
            . lang('i_m_sure') . "</a><button class='btn'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i>"
            . lang('delete_refugee_house_address_change') . "</a>";

        $action_link = '<div class="btn-group text-left"><button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">'.lang("actions").'<span class="caret"></span></button>
					        <ul class="dropdown-menu pull-right" role="menu">
					            <li><a target="_blank" href="'.site_url('application_forms2/refugee_house_address_change_request_form/$1').'" ><i class="fa fa-newspaper-o"></i>'.lang('refugee_house_address_change_request_form').'</a></li>   
					            <li><a data-backdrop="static" data-keyboard="false"  data-toggle="modal" data-target="#myModal" href="'.site_url('application_forms2/edit_refugee_house_address_change_request/$1').'" ><i class="fa fa-pencil-square-o"></i>'.lang('edit_refugee_house_address_change').'</a></li>   								
								<li>'.$delete.'</li>
					        </ul>
					    </div>';
		   $this->datatables->select("
					fa_refugee_change_address_request.id as id,
					CONCAT(case_prefix,case_no) as case_no,
					UPPER(firstname) AS firstname,
					UPPER(lastname) AS lastname,
					dob,
					UPPER(gender) AS gender,
					UPPER(nationality) AS nationality,
					fa_refugee_change_address_request.created_date,
					fa_refugee_change_address_request.status,
					fa_refugee_change_address_request.attachment  
					", false)
            ->from("fa_rsd_applications")
			->join("fa_refugee_change_address_request","application_id=fa_rsd_applications.id","right");

        $this->datatables->add_column("Actions", $action_link, "id");
        echo $this->datatables->generate();
	}
	
	public function edit_refugee_house_address_change_request($id = false) 
	{
		$post = $this->input->post();
		if($post){ 
		
			$country = $post['country'];
			$province = $post['province'];
			$district = $post['district'];
			$commune = $post['commune'];				
			$address = $post['address'];
			$address_kh = $post['address_kh'];
		 	$occupation = $post['occupation']; 
		 	$contact_number = $post['contact_number'];
		 	
		 	$village = $this->applications->addTags($post['village'],'village');
		 	
			$data = array( 
				"occupation"  => $occupation,
				"country" => $country,
				"province" => $province,
				"district" => $district,
				"commune" => $commune,
				"village" => $village,					
				"address" => $address,
				"address_kh" => $address_kh,
				"created_by"    => $this->session->userdata("user_id"),
				"created_date"  => $this->erp->fld(date("d/m/Y H:i")), 
				"contact_number" => $contact_number,
				"status" => 'pending',
			);   
			
			$config = array(
					'upload_path' => 'assets/uploads/document/refugee_change_address',
					'allowed_types' => 'gif|jpg|png|doc|pdf',
				);				
			$this->load->library('upload', $config);			
			if($this->upload->do_upload('attachment')){ 
				$data['attachment']=$this->upload->data('file_name');
			}  			  
			if($this->applications->updateRefugeeAddressChange($id, $data)){
				$this->session->set_flashdata('message', lang("refugee_house_address_change_request_edited"));
				redirect("application_forms2/refugee_house_address_change_request/");
			}
		}
		
		$this->data['id'] = $id;
		$this->data['countries'] = $this->applications->getAllCountries();
		$this->data['provinces'] = $this->applications->getAllProvinces();
		$this->data['communes'] = $this->applications->getAllCommunces();
		$this->data['districts'] = $this->applications->getAllDistricts();
		$this->data['result'] = $this->applications->getRefugeeAddressChangeById($id);
		$this->data['application'] = $this->applications->getRSDApplicationById($this->data['result']->application_id);
		$this->data['offices'] = $this->applications->getlocationByOffice();
		$this->data['modal_js'] = $this->site->modal_js();		
		$this->load->view($this->theme . 'application_forms2/edit_refugee_house_address_change_request', $this->data);
 
	}  
	
	public function add_refugee_house_address_change_request($id = false) 
	{
		$post = $this->input->post();
		
		if($post == true){
			
			$application_id = $post['application_id'];
			$country = $post['country'];
			$province = $post['province'];
			$district = $post['district'];
			$commune = $post['commune'];				
			$address = $post['address'];
			$address_kh = $post['address_kh'];
		 	$occupation = $post['occupation']; 
		 	$contact_number = $post['contact_number'];
		 	
		 	$village = $this->applications->addTags($post['village'],'village');
		 	
			$data = array( 
				"application_id"  => $application_id,
				"occupation"  => $occupation,
				"country" => $country,
				"province" => $province,
				"district" => $district,
				"commune" => $commune,
				"village" => $village,					
				"address" => $address,
				"address_kh" => $address_kh,
				"created_by"    => $this->session->userdata("user_id"),
				"created_date"  => $this->erp->fld(date("d/m/Y H:i")), 
				"contact_number" => $contact_number,
				"status" => 'pending',
			); 
				
			if($this->applications->addRefugeeAddressChange($data)){
				$this->session->set_flashdata('message', lang("refugee_house_address_change_saved"));
				redirect("application_forms2/refugee_house_address_change_request/");
			}
		}
		
		$this->data['id'] = $id;
		$this->data['countries'] = $this->applications->getAllCountries();
		$this->data['provinces'] = $this->applications->getAllProvinces();
		$this->data['communes'] = $this->applications->getAllCommunces();
		$this->data['districts'] = $this->applications->getAllDistricts();
		$this->data['users'] = $this->applications->getUsers();
		$this->data['offices'] = $this->applications->getlocationByOffice();
		$this->data['modal_js'] = $this->site->modal_js();		
		$this->load->view($this->theme . 'application_forms2/add_refugee_house_address_change_request', $this->data);
	} 
	
	public function delete_refugee_house_address_change($id=false) 
	{
		if($id){
			if($this->applications->deleteRefugeeAddressChange($id)){
				$this->session->set_flashdata('message', lang("refugee_house_address_change_deleted"));
				redirect($_SERVER['HTTP_REFERER']);
			}
		}
	}
	
	/**
	* 
	* FAMILY GUARANTE REQUEST
	* @return
	*/
	
	public function family_guarantee_request() 
	{ 
		$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('family_guarantee_request')));
		$meta = array('page_title' => lang('family_guarantee_request'), 'bc' => $bc);
		$this->page_construct('application_forms2/family_guarantee_request',$meta, $this->data);
	} 
	
	public function getFamilyGuranteeRequest() 
	{  

		$this->load->library('datatables'); 
		$delete = "<a href='#' class='delete-refugee po' title='" . lang("delete_family_guarantee_request") . "' data-content=\"<p>"
            . lang('r_u_sure') . "</p><a class='btn btn-danger' href='" . site_url('application_forms2/delete_family_guarantee_request/$1') . "'>"
            . lang('i_m_sure') . "</a><button class='btn'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i>"
            . lang('delete_family_guarantee_request') . "</a>";
			
        $action_link = '<div class="btn-group text-left"><button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">'.lang("actions").'<span class="caret"></span></button>
					        <ul class="dropdown-menu pull-right" role="menu">
					            <li><a data-backdrop="static" data-keyboard="false"  data-toggle="modal" data-target="#myModal" href="'.site_url('application_forms2/family_guarantee_request_option/$1').'" ><i class="fa fa-newspaper-o"></i>'.lang('family_guarantee_request_option').'</a></li>   
								<li><a target="_blank" href="'.site_url('application_forms2/family_guarantee_request_form/$1').'" ><i class="fa fa-newspaper-o"></i>'.lang('family_guarantee_request_form').'</a></li>   
								<li><a class="edit-refugee"  href="'.site_url('application_forms2/edit_family_guarantee_request/$1').'" ><i class="fa fa-newspaper-o"></i>'.lang('edit_family_guarantee_request').'</a></li>							
								<li><a  data-backdrop="static" data-keyboard="false"  data-toggle="modal" data-target="#myModal" href="'.site_url('application_forms2/attach_family_guarantee/$1').'" ><i class="fa fa-plus-circle"></i>'.lang('attachment').'</a></li>	
								<li>'.$delete.'</li>
					        </ul>
					    </div>';
						
		$this->datatables->select("
					fa_refugee_family_guarantee_request.id as id,
					CONCAT(erp_fa_rsd_applications.case_prefix,case_no) as case_no,
					IF(erp_fa_refugee_family_guarantee_request.member_id > 0,UPPER(erp_fa_family_members.firstname),UPPER(erp_fa_rsd_applications.firstname)) as firstname,
					IF(erp_fa_refugee_family_guarantee_request.member_id > 0,UPPER(erp_fa_family_members.lastname),UPPER(erp_fa_rsd_applications.lastname)) as lastname,
					IF(erp_fa_refugee_family_guarantee_request.member_id > 0,UPPER(erp_fa_family_members.dob),UPPER(erp_fa_rsd_applications.dob)) as dob,
					IF(erp_fa_refugee_family_guarantee_request.member_id > 0,UPPER(erp_fa_family_members.gender),UPPER(erp_fa_rsd_applications.gender)) as gender,
					IF(erp_fa_refugee_family_guarantee_request.member_id > 0,UPPER(erp_fa_family_members.nationality),UPPER(erp_fa_rsd_applications.nationality)) as nationality,
					IF(erp_fa_refugee_family_guarantee_request.member_id > 0,fa_family_relationship.relationship_kh,'ម្ចាស់ករណី' ) as relationship_kh,	
					fa_refugee_family_guarantee_request.created_date, 
					fa_refugee_family_guarantee_request.status,
				    CONCAT(IFNULL(DATE_FORMAT(sign_date1 , '%d/%m/%Y'),''),'<br/>',IFNULL(DATE_FORMAT(sign_date2, '%d/%m/%Y'),''),'<br/>',IFNULL(DATE_FORMAT(sign_date3, '%d/%m/%Y'),'')) as sign_date_all, 	
					", false)
            ->from("fa_rsd_applications")
			->join("fa_refugee_family_guarantee_request","application_id=fa_rsd_applications.id","right")
			->join("fa_refugee_family_guarantee_members", "fa_refugee_family_guarantee_request.member_id = fa_refugee_family_guarantee_members.guarantee_id", "left")
			->join("fa_family_members", "fa_family_members.id = fa_refugee_family_guarantee_request.member_id", "left")
			->join("fa_family_relationship", "fa_family_members.relationship = fa_family_relationship.id", "left")
			->where("fa_refugee_family_guarantee_request.status <> 'deleted' ");
			if ($this->erp->input->get('case_no')) {
				$this->datatables->where('concat(erp_fa_rsd_applications.case_prefix, erp_fa_rsd_applications.case_no)=',$this->input->get('case_no'));
			}
			if($this->erp->input->get('firstname')){
				$this->datatables->where('erp_fa_family_members.firstname',$this->input->get('firstname'))
				->or_where('erp_fa_rsd_applications.firstname',$this->input->get('firstname'));
			} 
			if($this->erp->input->get('lastname')){
				$this->datatables->where('erp_fa_family_members.lastname',$this->input->get('lastname'))
				->or_where('erp_fa_rsd_applications.lastname', $this->input->get('lastname'));
			}
			if($this->erp->input->get('register_from') && $this->erp->input->get('register_to')){
				$this->datatables->where('fa_refugee_family_guarantee_request.created_date >=',$this->erp->fld($this->erp->input->get('register_from')));
				$this->datatables->where('fa_refugee_family_guarantee_request.created_date <=',$this->erp->fld($this->erp->input->get('register_to')));
			}
			$this->datatables->order_by('fa_refugee_family_guarantee_request.id','desc');
        $this->datatables->add_column("Actions", $action_link, "id");
        echo $this->datatables->generate();
	}  

	public function add_family_guarantee_request() 
	{ 
		$post = $this->input->post();  
		 $this->erp->checkPermissions('add', null, 'family_guarantee_request'); 
			$config = array(
				array(
					'field'   => 'case_no',
					 'label'   => lang("case_no"),
					 'rules'   => 'required'
				),
			);
			
			$this->form_validation->set_rules($config);
			if ($this->form_validation->run() == true) {
				$application_id = $post['application_id'];
				$country = $post['country'];
				$province = $post['province'];
				$district = $post['district'];
				$commune = $post['commune'];				
				$address = $post['address'];
				$address_kh = $post['address_kh'];
			 	$occupation = $post['occupation']; 
				$occupation_kh = $post['occupation_kh']; 
			 	$contact_number = $post['contact_number'];			 	
			 	$village = $this->applications->addTagsGurantee($post['village'], $post['village_kh'],'village');
			 	
				$data = array( 
					"application_id"  => $application_id,
					"member_id"  => $post['member'],
					"occupation"  => $occupation,
					"occupation_kh"  => $occupation_kh,
					"country" => $country,
					"province" => $province,
					"district" => $district,
					"commune" => $commune,
					"village" => $village,					
					"address" => $address,
					"address_kh" => $address_kh,
					"created_by"    => $this->session->userdata("user_id"),
					"created_date"  => date("Y-m-d H:i"), 
					"contact_number" => $contact_number,
					"status" => 'pending',
				); 
				
			}
		
		
		if($this->form_validation->run()==true){
			$return_id = $this->applications->addRefugeeGuarantee($data); 
			$this->session->set_flashdata('message', lang("family_guarantee_request_saved"));
			redirect("application_forms2/edit_family_guarantee_request/".$return_id);
			
		}else{
			$this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));			
			$this->data['states'] = $this->applications->getAllStates();
			$this->data['countries'] = $this->applications->getAllCountries();
			$this->data['provinces'] = $this->applications->getAllProvinces();
			$this->data['communes'] = $this->applications->getAllCommunces();
			$this->data['districts'] = $this->applications->getAllDistricts();
			$this->data['relationship'] = $this->applications->getRelationship();
			$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('add_family_guarantee_request')));
			$meta = array('page_title' => lang('add_family_guarantee_request'), 'bc' => $bc);
			$this->page_construct('application_forms2/add_family_guarantee_request',$meta, $this->data);
		}		 
	} 

	public function edit_family_guarantee_request($id = false) 
	{
		$post = $this->input->post(); 
		 $this->erp->checkPermissions('edit', null, 'family_guarantee_request'); 
			$config = array(
				array(
					'field'   => 'case_no',
					 'label'   => lang("case_no"),
					 'rules'   => 'required'
				),
				array(
					'field'   => 'occupation_kh',
					 'label'   => lang("occupation_kh"),
					 'rules'   => 'required'
				),
				array(
					'field'   => 'occupation',
					 'label'   => lang("occupation_en"),
					 'rules'   => 'required'
				)
			);
			
			$this->form_validation->set_rules($config);
			if ($this->form_validation->run() == true) {
				
				$application_id = $post['application_id'];
				$country = $post['country'];
				$province = $post['province'];
				$district = $post['district'];
				$commune = $post['commune'];
				$address = $post['address'];
				$address_kh = $post['address_kh'];
			 	$occupation = $post['occupation'];
				$occupation_kh = $post['occupation_kh'];
			 	$contact_number = $post['contact_number'];
			 	$member_id = $post['member'];
			 	
			 	$village = $this->applications->addTagsGurantee($post['village'], $post['village_kh'],'village'); 
				$data = array( 
					"application_id"  => $application_id,
					"occupation"  => $occupation,
					"occupation_kh"  => $occupation_kh,
					"country" => $country,
					"province" => $province,
					"district" => $district,
					"commune" => $commune,
					"village" => $village,
					"address" => $address,
					"address_kh" => $address_kh,
					"created_by"    => $this->session->userdata("user_id"),
					// "created_date"  => $this->erp->fld(date("d/m/Y H:i")), 
					"contact_number" => $contact_number,
					"member_id" => $member_id,
					// "status" => 'pending',
				); 
				
			}
			
		if($this->form_validation->run()==true && $this->applications->updateRefugeeGuarantee($id, $data)){
			
			$this->session->set_flashdata('message', lang("family_guarantee_request_saved"));
			redirect("application_forms2/edit_family_guarantee_request/".$id);
			
		}else{
			 
			$this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));			
			$this->data['states'] = $this->applications->getAllStates();
			$this->data['countries'] = $this->applications->getAllCountries();
			$this->data['provinces'] = $this->applications->getAllProvinces();
			$this->data['communes'] = $this->applications->getAllCommunces();
			$this->data['districts'] = $this->applications->getAllDistricts();
			$this->data['relationship'] = $this->applications->getRelationship();
			$this->data['row'] = $this->applications->getAllRefugeeGuaranteeById($id);
			$this->data['application'] = $this->applications->getRSDApplicationById($this->data['row']->application_id);
			$this->data['members'] = $this->applications->getAllFamilyRefugeeByGuaranteeId($this->data['row']->id);
			$this->data['default_members'] = $this->applications->getFamilyMemberAll($this->data["application"]->counseling_id);
			$this->data['id'] = $id;
			
			$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('edit_family_guarantee_request')));
			$meta = array('page_title' => lang('edit_family_guarantee_request'), 'bc' => $bc);			
			$this->page_construct('application_forms2/edit_family_guarantee_request',$meta, $this->data); 
		}
	}
	
	public function delete_family_guarantee_request($id = false) 
	{
		$this->erp->checkPermissions('delete', null, 'family_guarantee_request'); 
		if($id) {			
			if($this->applications->deleteRefugeeGuarantee($id)) {
				$this->session->set_flashdata('message', lang("family_guarantee_request_deleted"));
				redirect("application_forms2/family_guarantee_request/");
			}
		}
	}
	
	public function family_guarantee_request_form($id=false) 
	{   
		$this->erp->checkPermissions('application_form', null, 'family_guarantee_request'); 
		$this->data['row'] = $this->applications->getAllRefugeeGuaranteeById($id);
		$this->data['application'] = $this->applications->getRSDApplicationById($this->data['row']->application_id);
		$this->data['member'] = $this->applications->getMemberId($this->data["row"]->member_id);
		$this->data['members'] = $this->applications->getAllFamilyRefugeeByGuaranteeId($this->data['row']->id);
		$this->data['relationship'] = $this->applications->getRelationship();
		$this->data['recognition'] = $this->applications->getRecognizationByApplicationId($this->data["row"]->application_id);
		 
		$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('family_guarantee_request_form')));
		$meta = array('page_title' => lang('family_guarantee_request_form'), 'bc' => $bc);
		$this->page_construct('application_forms2/family_guarantee_request_form',$meta, $this->data);
	}
	
	public function family_guarantee_request_option($id = false)
	{ 
		$this->erp->checkPermissions('request_option', null, 'family_guarantee_request'); 
		$post = $this->input->post();
		if($post){
			if($post['status1']){				
				$data = array(						
						"sign_date1" => $this->erp->fld($post['date']),					
						"status" => $post['status1'],
					);
			}
			if($post['status2']){				
				$data = array(						
						"sign_date2" => $this->erp->fld($post['date']),					
						"status" => $post['status2'],
					);
			}
			if($post['status3']){				
				$data = array(						
						"sign_date3" => $this->erp->fld($post['date']),					
						"status" => $post['status3'],
					);
			}
			if($this->applications->updateRefugeeGuarantee($id,$data)){
				$this->session->set_flashdata('success', lang("family_guarantee_request_option"));
				redirect("application_forms2/family_guarantee_request");
			}
		}
		$this->data['id'] = $id;
		$this->data['guarantee'] = $this->applications->getAllRefugeeGuaranteeById($id);
		$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('family_guarantee_request_option')));
		$this->data['modal_js'] = $this->site->modal_js();		
		$this->load->view($this->theme . 'application_forms2/family_guarantee_request_option', $this->data);
	}	
	
	public function attach_family_guarantee($id = false) 
	{
		$result = $this->applications->getAllRefugeeGuaranteeById($id);
		$this->global_model->attach_forms('fa_refugee_family_guarantee_request', $id, 'family_guarantee');
		$this->data['action_method'] = 'attach_family_guarantee';
		$this->data['delete_url'] = 'delete_attach_family_guarantee';
		$this->data['view_folder'] = 'family_guarantee';
		$this->data['contorler'] = $this->uri->segment(1);
		$this->data['id'] = $id;
		$this->data['result'] = $result; 
		$this->data['modal_js'] = $this->site->modal_js();		
		$this->load->view($this->theme . 'global/view_attach_forms', $this->data);		
	 
	} 
	  
	public function delete_attach_family_guarantee($id = false, $name = null, $img = null)
	{
		$result = $this->applications->getAllRefugeeGuaranteeById($id);
		$this->erp->checkPermissions('delete', null, 'family_guarantee_request'); 
		$this->global_model->delete_attach_forms('fa_refugee_family_guarantee_request', $id, $name, $img);
	}   

	/**
	* 
	* @param undefined $application_id
	* FAMILY GURANTEE MEMBER
	* @return
	*/
	
	public function add_family_guarantee_member($guarantee_id = false) 
	{
		$post=$this->input->post();
		if($post) {
			$this->erp->checkPermissions('add', null, 'family_guarantee_request'); 
			$firstname = $post['firstname'];
			$firstname_kh = $post['firstname_kh'];
			$lastname = $post['lastname'];
			$lastname_kh = $post['lastname_kh'];
			$gender = $post['gender'];
			$occupation = $post['occupation'];
			$education = $post['education'];
			$nationality = $post['nationality']; 
			$occupation_kh = $post['occupation_kh'];
			$education_kh = $post['education_kh'];
			$nationality_kh = $post['nationality_kh'];
			$religion = $post['religion'];			
			$religion_kh = $post['religion_kh'];
			$ethnicity = $post['ethnicity'];			
			$ethnicity_kh = $post['ethnicity_kh'];
			$relationship = $post['relationship'];
			$country = $post['country'];
			$province = $post['province'];
			$state = $post['state'];
			$district = $post['district'];
			$commune = $post['commune'];
			$address = $post['address'];
			$address_kh = $post['address_kh'];
			$pob = $post['pob'];
			$pob_kh = $post['pob_kh'];
			$village = $this->applications->addTagsGurantee($post['village'], $post['village_kh'],'village');
			
			$data = array(						
							"guarantee_id" => $guarantee_id,
							"firstname" => $firstname,
							"firstname_kh" => $firstname_kh,
							"lastname" => $lastname,
							"lastname_kh" => $lastname_kh,
							"gender" => $gender,
							"occupation" => $occupation,
							"occupation_kh" => $occupation_kh,
							"education_kh" => $education_kh,
							"education" => $education,
							"nationality_kh" => $nationality_kh,
							"nationality" => $nationality,
							"religion" => $religion,
							"religion_kh" => $religion_kh,
							"ethnicity" => $ethnicity,
							"ethnicity_kh" => $ethnicity_kh,
							"dob" => $this->erp->fld($post['dob']),
							"relationship" => $relationship,
							"country" => $country,
							"province" => $province,
							"state" => $state,
							"district" => $district,
							"commune" => $commune,
							"village" => $village,
							"address" => $address,
							"address_kh" => $address_kh,
							"pob" => $pob,
							"pob_kh" => $pob_kh,
							'status' => $post['status'],
						);

			if(isset($_FILES['userfile'])){
				$config_file = array(
						'upload_path' => 'assets/uploads/members',
						'allowed_types' => 'gif|jpg|png|doc|pdf',
					);
				$this->load->library('upload', $config_file);
				if($this->upload->do_upload('userfile')){
					$data["photo"] = $this->upload->data('file_name');
				}
			}
			if($this->applications->addFamilyGuaranteeRefugee($data)){
				$this->session->set_flashdata('message', lang("family_guarantee_member_added"));
				redirect($_SERVER['HTTP_REFERER'].'#content_2');
			}	
		} 
		$this->data['id'] = $guarantee_id;
		$this->data['modal_js'] = $this->site->modal_js();
		$this->data['relationship'] = $this->applications->getRelationship();
		$this->data['countries'] = $this->applications->getAllCountries();
		$this->data['provinces'] = $this->applications->getAllProvinces();
		$this->data['communes'] = $this->applications->getAllCommunces();
		$this->data['districts'] = $this->applications->getAllDistricts();
		$this->data['states'] = $this->applications->getAllStates();
		$this->load->view($this->theme . 'application_forms2/add_family_guarantee_member', $this->data);
	}
	
	public function edit_family_guarantee_member($id = false) 
	{
		$post=$this->input->post();
		if($post) {
			$this->erp->checkPermissions('edit', null, 'family_guarantee_request'); 
			$firstname = $post['firstname'];
			$firstname_kh = $post['firstname_kh'];
			$lastname = $post['lastname'];
			$lastname_kh = $post['lastname_kh'];
			$gender = $post['gender'];
			$occupation = $post['occupation'];
			$education = $post['education'];
			$nationality = $post['nationality'];
			$occupation_kh = $post['occupation_kh'];
			$education_kh = $post['education_kh'];
			$nationality_kh = $post['nationality_kh'];			
			$religion = $post['religion'];			
			$religion_kh = $post['religion_kh'];
			$ethnicity = $post['ethnicity'];			
			$ethnicity_kh = $post['ethnicity_kh'];			
			$relationship = $post['relationship'];
			$country = $post['country'];
			$province = $post['province'];
			$state = $post['state'];
			$district = $post['district'];
			$commune = $post['commune'];
			$address = $post['address'];
			$address_kh = $post['address_kh'];
			$pob = $post['pob'];
			$pob_kh = $post['pob_kh'];
			$village = $this->applications->addTagsGurantee($post['village'], $post['village_kh'],'village');
			$data = array(
							"firstname" => $firstname,
							"firstname_kh" => $firstname_kh,
							"lastname" => $lastname,
							"lastname_kh" => $lastname_kh,
							"gender" => $gender,
							"occupation" => $occupation,
							"education" => $education,
							"nationality" => $nationality,
							"occupation_kh" => $occupation_kh,
							"education_kh" => $education_kh,
							"nationality_kh" => $nationality_kh,
							"religion" => $religion,
							"religion_kh" => $religion_kh,
							"ethnicity" => $ethnicity,
							"ethnicity_kh" => $ethnicity_kh,
							"dob" => $this->erp->fld($post['dob']),
							"relationship" => $relationship,
							"country" => $country,
							"province" => $province,
							"state" => $state,
							"district" => $district,
							"commune" => $commune,
							"village" => $village,
							"address" => $address,
							"address_kh" => $address_kh,
							"pob" => $pob,
							"pob_kh" => $pob_kh,
							"status" => $post['status'],
						);
						
			if(isset($_FILES['userfile'])){
				$config_file = array(
								'upload_path' => 'assets/uploads/members',
								'allowed_types' => 'gif|jpg|png|doc|pdf',
								);
				$this->load->library('upload', $config_file);
				if($this->upload->do_upload('userfile')){
					$data["photo"] = $this->upload->data('file_name');
				}
			}
			
			if($this->applications->updateFamilyGuaranteeRefugee($id, $data)){
				$this->session->set_flashdata('message', lang("family_guarantee_member_updated"));
				redirect($_SERVER['HTTP_REFERER'].'#content_2');
			}	
		} 
		$this->data['id'] = $id;
		$this->data['modal_js'] = $this->site->modal_js();
		$this->data['member'] = $this->applications->getFamilyGuaranteeRefugeeById($id);
		$this->data['relationship'] = $this->applications->getRelationship();
		$this->data['countries'] = $this->applications->getAllCountries();
		$this->data['provinces'] = $this->applications->getAllProvinces();
		$this->data['communes'] = $this->applications->getAllCommunces();
		$this->data['districts'] = $this->applications->getAllDistricts();
		$this->data['states'] = $this->applications->getAllStates();
		$this->load->view($this->theme . 'application_forms2/edit_family_guarantee_member', $this->data);
	}	
	
	public function delete_family_guarantee_member($id = false)
	{
			$this->erp->checkPermissions('delete', null, 'family_guarantee_request'); 
		if($id){
			if($this->applications->deleteFamilyGuaranteeRefugee($id)){
				$this->session->set_flashdata('message', lang("family_guarantee_member_deleted"));
				redirect($_SERVER['HTTP_REFERER'].'#content_2');
			}
		}
	}
	
	public function add_family_guarantee_member_exiting($id=NULL) 
	{	
		$post = $this->input->post();
		if($post) { 
			$this->erp->checkPermissions('add', null, 'family_guarantee_request'); 
			$member_id = $post["member_id"];
			for ($i=0;$i<count($member_id);$i++){
				$results =  $this->applications->getFamilyMemberById($member_id[$i]);				
				foreach($results as $value ) {  
					$data = array(			
						"guarantee_id" => $id,
						"firstname" => $value->firstname,
						"firstname_kh" => $value->firstname_kh,
						"lastname" => $value->lastname,
						"lastname_kh" =>$value->lastname_kh,
						"gender" => $value->gender,
						"occupation" => $value->occupation,
						"occupation_kh" =>$value->occupation_kh,
						"education_kh" => $value->education_kh,
						"education" => $value->education,
						"nationality_kh" => $value->nationality_kh,
						"nationality" => $value->nationality,
						"dob" => $value->dob,
						"relationship" => $value->relationship,
						"country" => $value->country,
						"province" => $value->province,
						"state" => $value->state,
						"district" => $value->district,
						"commune" => $value->commune,
						"village" =>$this->applications->addTags($value->village,'village'),
						"address" => $value->address,
						"address_kh" => $value->address_kh,
						"pob" => $value->pob,
						"pob_kh" => $value->pob_kh
					);
				}
				$result = $this->db->insert("fa_refugee_family_guarantee_members",$data);
			} 		  
			if($result){ 
				$this->session->set_flashdata('message', lang("family_guarantee_member_added"));
				redirect($_SERVER['HTTP_REFERER'].'#content_2');
			}
		}
		$this->data['id'] = $id;
		$this->data['modal_js'] = $this->site->modal_js();		
		$this->data['row'] = $this->applications->getAllRefugeeGuaranteeById($id);
		$this->data['application'] = $this->applications->getRSDApplicationById($this->data['row']->application_id);
		$this->data["members"] = $this->applications->getCounselingMemberId($this->data['application']->counseling_id);
		$this->load->view($this->theme . 'application_forms2/add_family_guarantee_member_exiting', $this->data);
	}

	public function getLastDataByApplicationFamilyGuranteeRequest(){
		$application_id = $this->input->get("application_id");
		$member_id = $this->input->get("member_id");
		$result = "";
		if($member_id == 0 || $member_id == null){
			$result = $this->applications->getOwnerLastDataFamilyGuranteeRequest($application_id);
		}
		elseif($member_id > 0){
			$result = $this->applications->getMemberLastDataFamilyGuranteeRequest($member_id);
		}
		else{
			return false;
		}
		$village_name = $this->applications->getTagsById($result->village);
		$village_name_kh = $this->applications->getTagsKhmerById($result->village);
		echo json_encode(array('result'=>$result,'vill'=>$village_name,'vill_kh'=>$village_name_kh));
	}
	
	/**
	* 
	* @param undefined $id
	* @param undefined $home_visit_id
	* REFUGEE HOME VISIT
	* @return
	*/
	
	public function home_visit_form($id=false)
	{ 
		$this->erp->checkPermissions('application_form', null, 'home_visits'); 
		$this->data["home_visit"] = $this->applications->getHomeVisitById($id); 
		$this->data["result"] = $this->applications->getRSDApplicationById($this->data["home_visit"]->application_id);
		$this->data['member'] = $this->applications->getMemberId($this->data["home_visit"]->member_id);
		$this->data['result_members'] = $this->applications->getAllFamilyRefugeeByHomeVisitId($this->data["home_visit"]->id);	  		
		$this->data['default_members'] = $this->applications->getFamilyMemberAccompanyingByCounseling($this->data["result"]->counseling_id);
		  
		
		$this->data['relationship'] = $this->applications->getRelationship();
		$this->data['recognition'] = $this->applications->getRecognizationByApplicationId($this->data["home_visit"]->application_id);
		$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('home_visit_form')));
		$meta = array('page_title' => lang('home_visit_form'), 'bc' => $bc);	
		$this->page_construct('application_forms2/home_visit_form',$meta, $this->data);
	}
	
	public function home_visit_form_sample($id=false)
	{  
		$this->erp->checkPermissions('application_form_simple', null, 'home_visits');
		$this->data["home_visit"] = $this->applications->getHomeVisitById($id); 
		$this->data["result"] = $this->applications->getRSDApplicationById($this->data["home_visit"]->application_id);
		$this->data['result_members'] = $this->applications->getAllFamilyRefugeeByHomeVisitId($this->data["home_visit"]->id);	  		
		$this->data['default_members'] = $this->applications->getFamilyMemberAccompanyingByCounseling($this->data["result"]->counseling_id);
		$this->data['member'] = $this->applications->getMemberId($this->data["home_visit"]->member_id);
		$this->data['relationship'] = $this->applications->getRelationship();
		$this->data['recognition'] = $this->applications->getRecognizationByApplicationId($this->data["home_visit"]->application_id);
		$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('home_visit_form')));
		$meta = array('page_title' => lang('home_visit_form'), 'bc' => $bc);	
		$this->page_construct('application_forms2/home_visit_form_sample',$meta, $this->data);
	}
	
	public function home_visit()
	{
		$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('home_visit')));
		$meta = array('page_title' => lang('home_visit'), 'bc' => $bc);
		$this->page_construct('application_forms2/home_visit',$meta, $this->data);
	}
	
	public function list_of_refugees()
	{
		$bc = array(array('link' => base_url(), 'page' => lang('list_of_refugees')), array('link' => '#', 'page' => lang('list_of_refugees')));
		$meta = array('page_title' => lang('list_of_refugees'), 'bc' => $bc);
		$this->page_construct('application_forms2/list_of_refugees',$meta, $this->data);
	}

	public function getListOfRefugee()
	{
		$this->load->library('datatables');	 
		
		$action_link = '<div class="btn-group text-left"><button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">'.lang("actions").'<span class="caret"></span></button>
			<ul class="dropdown-menu pull-right" role="menu">					        																							
				<li><a class="home-visit" target="_blank" href="'.site_url('application_forms2/add_family_guarantee_request/').'" ><i class="fa fa-newspaper-o"></i>'.lang('family_guarantee_request').'</a></li>
				<li><a class="family-gurantee" target="_blank" href="'.site_url('application_forms2/add_home_visit/').'" ><i class="fa fa-newspaper-o"></i>'.lang('home_visit').'</a></li>
			</ul>
		</div>';
		
   //      $this->datatables->select("
			// 		fa_interview_recognitions.id as id,
			// 		CONCAT(erp_fa_rsd_applications.case_prefix,erp_fa_rsd_applications.case_no) as case_no,
			// 		UPPER(firstname) AS firstname,
			// 		UPPER(lastname) AS lastname,
			// 		dob,
			// 		UPPER(gender) AS gender,
			// 		UPPER(nationality) AS nationality,
			// 		fa_interview_recognitions.created_date,
			// 		fa_interview_recognitions.status,
			// 		", false)
   //          ->from("fa_rsd_applications")
			// ->join("fa_interview_recognitions","application_id=fa_rsd_applications.id","right")
			// ->where("fa_interview_recognitions.status <>","deleted");
			
			$this->datatables->select("
							erp_v_fa_in_mem.id as id,
							erp_v_fa_in_mem.case_no,
							IF(erp_v_fa_in_mem.member_id > 0,UPPER(fa_mem.lastname),UPPER(erp_v_fa_in_mem.firstname) ) as firstname,
							IF(erp_v_fa_in_mem.member_id > 0,UPPER(fa_mem.firstname),UPPER(erp_v_fa_in_mem.lastname) ) as lastname,
							IF(erp_v_fa_in_mem.member_id > 0,fa_mem.dob,erp_v_fa_in_mem.dob ) as dob,
							IF(erp_v_fa_in_mem.member_id > 0,UPPER(fa_mem.gender),UPPER(erp_v_fa_in_mem.gender) ) as gender,
							erp_v_fa_in_mem.nationality,
							IF(erp_v_fa_in_mem.member_id > 0,fa_relation.relationship_kh,'ម្ចាស់ករណី' ) as relationship,
							erp_v_fa_in_mem.created_date,
							erp_v_fa_in_mem.recognized_date,
							erp_v_fa_in_mem.status", false)
			->from("v_fa_inerview_recongnitions erp_v_fa_in_mem")
			->join("fa_family_members fa_mem","fa_mem.id = erp_v_fa_in_mem.member_id","left")
			->join("fa_family_relationship fa_relation","fa_relation.id = fa_mem.relationship","left")
			->where("erp_v_fa_in_mem.status <>","deleted");


        $this->datatables->add_column("Actions", $action_link, "id");
        echo $this->datatables->generate();
	}
	
	public function getHomeVisit()
	{
		$this->load->library('datatables');
		$delete = "<a href='#' class='delete-home-visit po' title='" . lang("delete_home_visit") . "' data-content=\"<p>"
            . lang('r_u_sure') . "</p><a class='btn btn-danger' href='" . site_url('application_forms2/delete_home_visit/$1') . "'>"
            . lang('i_m_sure') . "</a><button class='btn'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i>"
            . lang('delete_home_visit') . "</a>";
     
        $action_link = '<div class="btn-group text-left"><button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">'.lang("actions").'<span class="caret"></span></button>
					        <ul class="dropdown-menu pull-right" role="menu">
								 <li><a target="_blank" href="'.site_url('application_forms2/home_visit_form_sample/$1').'" ><i class="fa fa-newspaper-o"></i>'.lang('home_visit_form_sample').'</a></li>
					            <li><a href="'.site_url('application_forms2/edit_home_visit/$1').'" ><i class="fa fa-pencil-square-o"></i>'.lang('edit_home_visit').'</a></li>
					            <li><a target="_blank" href="'.site_url('application_forms2/home_visit_form/$1').'" ><i class="fa fa-newspaper-o"></i>'.lang('home_visit_form').'</a></li>
								<li><a  data-backdrop="static" data-keyboard="false"  data-toggle="modal" data-target="#myModal" href="'.site_url('application_forms2/attach_home_visit/$1').'" ><i class="fa fa-plus-circle"></i>'.lang('attachment').'</a></li>	
								<li>'.$delete.'</li>
					        </ul>
					    </div>';
					    
        $this->datatables->select("
        						id,
        						UPPER(full_case),
        						UPPER(firstname),
        						UPPER(lastname),
        						dob,
        						UPPER(gender),
        						UPPER(nationality),
        						relationship_kh,
        						created_date,
        						day_visit
							", false)
            ->from("erp_v_list_home_visit")
			->where("erp_v_list_home_visit.status <> 'deleted'")
			->order_by("created_date","desc");

			// $lastname  = 'IF(fa_refugee_home_visit.member_id > 0, fa_family_members.lastname, fa_rsd_applications.lastname)';
			// $firstname = 'IF(fa_refugee_home_visit.member_id > 0,fa_family_members.firstname, fa_rsd_applications.firstname)';
			 if($this->erp->input->get('case_no')){
				$this->datatables->where('full_case=',$this->input->get('case_no'));
			} 
			if($this->erp->input->get('firstname')){
				$this->datatables->where('firstname',$this->input->get('firstname'));
			} 
			if($this->erp->input->get('lastname')){
				$this->datatables->where('lastname',$this->input->get('lastname'));
			}
			if($this->erp->input->get('register_from') && $this->erp->input->get('register_to')){
				$this->datatables->where('day_visit >=',$this->erp->fld($this->erp->input->get('register_from')));
				$this->datatables->where('day_visit <=',$this->erp->fld($this->erp->input->get('register_to')));
			}

        $this->datatables->add_column("Actions", $action_link, "id");		
        echo $this->datatables->generate();
	}
 
	public function delete_home_visit($id = false)
	{   
		$this->erp->checkPermissions('delete', null, 'home_visits');	
		if($id){
			if($this->applications->deleteRefugeeHomeVisit($id)){ 
				$this->session->set_flashdata('message', lang("home_visit_deleted"));
				redirect("application_forms2/home_visit");
			}
		} 
	}
		
	public function add_home_visit($id = false)
	{
		$this->load->library("form_validation");
		$this->erp->checkPermissions('add', null, 'home_visits');	
		$post = $this->input->post(); 			
		$config = array(
					array(
	                     'field'   => 'case_no',
	                     'label'   => lang("case_no"),
	                     'rules'   => 'required'
	                  ) 
            	);
            	
		$this->form_validation->set_rules($config);
		if ($this->form_validation->run() == true) {
			
					
			$refugee = $this->applications->getRSDApplicationById($id);
			
			$country = $post['country'];
			$province = $post['province'];
			$district = $post['district'];
			$commune = $post['commune'];
			$status = "pending";
			$group = $post['group'];
			$street = $post['street'];
			$house = $post['house'];
			$address = $post['address'];
			$address_kh = $post['address_kh'];			
			$contact_number = $post['contact_number'];
			$contact_reason = $post['contact_reason'];
			$occupation = $post['occupation'];
			$education = $post['education'];
			$occupation_kh = $post['occupation_kh'];
			$education_kh = $post['education_kh'];
			$family_status = $post['family_status'];
			$health_status = $post['health_status'];			
			$sponsorship_support = $post['sponsorship_support'];
			$sponsorship_support_by = $post['sponsorship_support_by'];
			if ($post['sponsorship_support'] > 0) {
				// $sponsorship_support_money = $post['sponsorship_support_money'];
				$sponsorship_support_money = implode(',',$post['sponsorship_support_money']);

			}
			$sponsorship_support_other = $post['sponsorship_support_other'];			
			$insurance_service = $post['insurance_service'];
			$insurance_service_by = $post['insurance_service_by'];
			$insurance_service_on = implode(',',$post['insurance_service_on']);
			$insurance_service_other = $post['insurance_service_other'];
			$suggestions = $post['suggestions'];			
			$local_authorities_name = $post['local_authorities_name'];
			$local_authorities_contact = $post['local_authorities_contact'];
			$local_authorities_opinion = $post['local_authorities_opinion'];
			$local_authorities_comment = $post['local_authorities_comment'];
			$feedback_officials = $post['feedback_officials'];
			$family_members = $post['family_members'];
			$member_id = $post['member'];
			$is_contact = $post['is_contact'];
			
			
			
			$village = $this->applications->addTags($post['village'],'village');
			
			$data = array(
						"application_id" => $id,
						"country" => $country,
						"province" => $province,
						"district" => $district,
						"commune" => $commune,
						"village" => $village,
						"group" => $group,
						"street" => $street,
						"house" => $house,
						"address" => $address,
						"address_kh" => $address_kh,
						"contact_number" => $contact_number,						
						"contact_reason" => $contact_reason,
						"occupation" => $occupation,
						"education" => $education, 
						"occupation_kh" => $occupation_kh,
						"education_kh" => $education_kh,
						"family_status" => $family_status,
						"family_members" => $family_members,
						"health_status" => $health_status,
						"sponsorship_support" => $sponsorship_support,
						//"sponsorship_support_by" => $sponsorship_support_by,
						// "sponsorship_support_money" => $sponsorship_support_money,
						// "sponsorship_support_other" => $sponsorship_support_other,
						"insurance_service" => $insurance_service,
						// "insurance_service_by" => $insurance_service_by,
						// "insurance_service_on" => $insurance_service_on,
						// "insurance_service_other" => $insurance_service_other,
						"suggestions" => $suggestions,
						"local_authorities_name" => $local_authorities_name,
						"local_authorities_contact" => $local_authorities_contact,
						"local_authorities_opinion" => $local_authorities_opinion,
						"local_authorities_comment" => $local_authorities_comment,
						"feedback_officials" => $feedback_officials,
						"created_date" =>  $this->erp->fld(date("d/m/Y H:i")),
						"member_id" => $member_id,
						"is_contact" => $is_contact,
                		"status" => $status
					);	 
						if($post['sponsorship_support'] > 0 ){
							$data['sponsorship_support_by'] = $sponsorship_support_by;
							$data['sponsorship_support_money'] = $sponsorship_support_money;
							$data['sponsorship_support_other'] = $sponsorship_support_other;
						}
						if ($post['insurance_service'] > 0) {
							$data['insurance_service_by'] = $insurance_service_by;
							$data['insurance_service_on'] = $insurance_service_on;
							$data['insurance_service_other'] = $insurance_service_other;
						}
						// $this->erp->print_arrays($data['sponsorship_support_money']);
		}

		if($this->form_validation->run() == true && $this->applications->addRefugeeHomeVisit($data)){
			$this->session->set_flashdata("message", lang("home_visit_added"));
			redirect("application_forms2/home_visit");
		}
		else{
			$this->data['id'] = $id;
			$this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
			$this->data['states'] = $this->applications->getAllStates();
			$this->data['countries'] = $this->applications->getAllCountries();
			$this->data['provinces'] = $this->applications->getAllProvinces();
			$this->data['communes'] = $this->applications->getAllCommunces();
			$this->data['districts'] = $this->applications->getAllDistricts();
			$getLastRefugee = $this->applications->getLastRefugeeHomeVisit($id);
			if ($getLastRefugee != false){
				$this->data["row"] = $this->applications->getHomeVisitById($getLastRefugee->id);
			}
			//$this->erp->print_arrays($this->data["row"]);
			$this->data['application'] = $this->applications->getRSDApplicationById($id);
			$this->data['default_members'] =  $this->applications->getFamilyMemberAll($this->data["application"]->counseling_id);
			$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('home_visit')));
			$meta = array('page_title' => lang('home_visit'), 'bc' => $bc);
			$this->page_construct('application_forms2/add_home_visit', $meta, $this->data);
		}
	} 
	
	public function edit_home_visit($id = false)
	{		
		$this->erp->checkPermissions('edit', null, 'home_visits');	
		$this->load->library("form_validation");		
		$post = $this->input->post(); 			
		$config = array(
					array(
	                     'field'   => 'case_no',
	                     'label'   => lang("case_no"),
	                     'rules'   => 'required'
	                  )
            	);
            	
		$this->form_validation->set_rules($config);
		if ($this->form_validation->run() == true) {
			
			$application_id = trim($post['application_id']);
			$refugee = $this->applications->getRSDApplicationById($application_id);
			
			$country = $post['country'];
			$province = $post['province'];
			$district = $post['district'];
			$commune = $post['commune'];
			
			$group = $post['group'];
			$street = $post['street'];
			$house = $post['house'];
			$address = $post['address'];
			$address_kh = $post['address_kh'];
			$contact_number = $post['contact_number'];
			$contact_reason = $post['contact_reason'];
			$occupation = $post['occupation'];
			$education = $post['education'];
			$occupation_kh = $post['occupation_kh'];
			$education_kh = $post['education_kh'];
			$family_status = $post['family_status'];
			$health_status = $post['health_status'];			
			$sponsorship_support = $post['sponsorship_support'];
			if ($post['sponsorship_support'] > 0) {
				$sponsorship_support_by = $post['sponsorship_support_by'];
				$sponsorship_support_money = implode(',',$post['sponsorship_support_money']);
				$sponsorship_support_other = $post['sponsorship_support_other'];
			}
			$insurance_service = $post['insurance_service'];
			if ($post['insurance_service'] > 0) {
				$insurance_service_by = $post['insurance_service_by'];
				$insurance_service_on = implode(',',$post['insurance_service_on']);
				$insurance_service_other = $post['insurance_service_other'];
			}
			$suggestions = $post['suggestions'];			
			$local_authorities_name = $post['local_authorities_name'];
			$local_authorities_contact = $post['local_authorities_contact'];
			$local_authorities_opinion = $post['local_authorities_opinion'];
			$local_authorities_comment = $post['local_authorities_comment'];
			$feedback_officials = $post['feedback_officials'];
			$status = "pending";
			$family_members = $post['family_members'];			
			$is_contact = $post['is_contact']; 			
			$village = $this->applications->addTags($post['village'],'village');
			
			$data = array(
						"application_id" => $application_id,
						"country" => $country,
						"province" => $province,
						"district" => $district,
						"commune" => $commune,
						"village" => $village,
						"group" => $group,
						"street" => $street,
						"house" => $house,
						"address" => $address,
						"address_kh" => $address_kh,
						"contact_number" => $contact_number,						
						"contact_reason" => $contact_reason,
						"occupation" => $occupation,
						"education" => $education,
						"occupation_kh" => $occupation_kh,
						"education_kh" => $education_kh,
						"family_status" => $family_status,
						"family_members" => $family_members,
						"health_status" => $health_status,
						"sponsorship_support" => $sponsorship_support,
						"sponsorship_support_by" => $sponsorship_support_by,
						"sponsorship_support_money" => $sponsorship_support_money,
						"sponsorship_support_other" => $sponsorship_support_other,
						"insurance_service" => $insurance_service,
						"insurance_service_by" => $insurance_service_by,
						"insurance_service_on" => $insurance_service_on,
						"insurance_service_other" => $insurance_service_other,
						"suggestions" => $suggestions,
						"local_authorities_name" => $local_authorities_name,
						"local_authorities_contact" => $local_authorities_contact,
						"local_authorities_opinion" => $local_authorities_opinion,
						"local_authorities_comment" => $local_authorities_comment,
						"feedback_officials" => $feedback_officials,
						"status" => $status,
						"member_id" => $post['member'],
						"is_contact" => $is_contact,
						"day_visit"  => $this->erp->fld($post['day_visit']),
					);		
					// if($post['sponsorship_support'] > 0 ){
					// 	$data['sponsorship_support_by'] = $sponsorship_support_by;
					// 	$data['sponsorship_support_money'] = $sponsorship_support_money;
					// 	$data['sponsorship_support_other'] = $sponsorship_support_other;
					// }
					// if ($post['insurance_service'] > 0) {
					// 	$data['insurance_service_by'] = $insurance_service_by;
					// 	$data['insurance_service_on'] = $insurance_service_on;
					// 	$data['insurance_service_other'] = $insurance_service_other;
					// }

					 
		}
		if($this->form_validation->run() == true && $this->applications->updateRefugeeHomeVisit($id, $data)){
			
			$this->session->set_flashdata("message", lang("home_visit_added"));
			redirect("application_forms2/edit_home_visit/".$id);
		}
		else{
			
			$this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
			$this->data['states'] = $this->applications->getAllStates();
			$this->data['countries'] = $this->applications->getAllCountries();
			$this->data['provinces'] = $this->applications->getAllProvinces();
			$this->data['communes'] = $this->applications->getAllCommunces();
			$this->data['districts'] = $this->applications->getAllDistricts();
			$this->data['relationship'] = $this->applications->getRelationship();
			
			
			$this->data["id"] = $id;
			$this->data["row"]= $this->applications->getHomeVisitById($id);
			// $this->erp->print_arrays($this->data["row"]->insurance_service_on);
			$this->data['application'] = $this->applications->getRSDApplicationById($this->data["row"]->application_id);
			$this->data['members'] = $this->applications->getAllFamilyRefugeeByHomeVisitId($this->data['row']->id);
			$this->data['default_members'] =  $this->applications->getFamilyMemberAll($this->data["application"]->counseling_id);
			$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('home_visit')));
			$meta = array('page_title' => lang('home_visit'), 'bc' => $bc);
			$this->page_construct('application_forms2/edit_home_visit',$meta, $this->data);
		} 
	}
	
	public function add_home_visit_member_exiting($id=null)
	{ 
		$this->erp->checkPermissions('add', null, 'home_visits');	
		$post=$this->input->post(); 
		if($post) { 
			$member_id = $post["member_id"];
			for ($i=0;$i<count($member_id);$i++){
				$results =  $this->applications->getFamilyMemberById($member_id[$i]);				
				foreach($results as $value ) {  
					$data = array(			
						"visit_id" => $id,
						"firstname" => $value->firstname,
						"firstname_kh" => $value->firstname_kh,
						"lastname" => $value->lastname,
						"lastname_kh" =>$value->lastname_kh,
						"gender" => $value->gender,
						"occupation" => $value->occupation,
						"occupation_kh" =>$value->occupation_kh,
						"education_kh" => $value->education_kh,
						"education" => $value->education,
						"nationality_kh" => $value->nationality_kh,
						"nationality" => $value->nationality,
						"dob" => $value->dob,
						"relationship" => $value->relationship,
						"country" => $value->country,
						"province" => $value->province,
						"state" => $value->state,
						"district" => $value->district,
						"commune" => $value->commune,
						"village" =>$this->applications->addTags($value->village,'village'),
						"address" => $value->address,
						"address_kh" => $value->address_kh,
						"pob" => $value->pob,
						"pob_kh" => $value->pob_kh
					);
				}
				$result = $this->db->insert("fa_refugee_home_visit_members",$data);
			} 		  
			if($result){ 
				$this->session->set_flashdata('message', lang("family_guarantee_member_added"));
				redirect($_SERVER['HTTP_REFERER'].'#content_4');
			}
		}
		$this->data['id'] = $id;
		$this->data['modal_js'] = $this->site->modal_js(); 
		$this->data['row'] = $this->applications->getHomeVisitById($id);
		$this->data['application'] = $this->applications->getRSDApplicationById($this->data['row']->application_id);
		$this->data["members"] = $this->applications->getCounselingMemberId($this->data['application']->counseling_id);
		$this->load->view($this->theme . 'application_forms2/add_home_visit_member_exiting', $this->data);		
	} 
	
	public function attach_home_visit($id = false) 
	{
		$result = $this->applications->getHomeVisitById($id);
		$this->erp->checkPermissions('attachment', null, 'home_visits');
		$this->global_model->attach_forms('fa_refugee_home_visit', $id, 'home_visit');
		$this->data['action_method'] = 'attach_home_visit';
		$this->data['delete_url'] = 'delete_attach_home_visit';
		$this->data['view_folder'] = 'home_visit';
		$this->data['contorler'] = $this->uri->segment(1);
		$this->data['id'] = $id;
		$this->data['result'] = $result; 
		$this->data['modal_js'] = $this->site->modal_js();		
		$this->load->view($this->theme . 'global/view_attach_forms', $this->data);
	} 
	  
	public function delete_attach_home_visit($id = false, $name = null, $img = null)
	{
		$this->erp->checkPermissions('delete', null, 'home_visits');	
		$this->global_model->delete_attach_forms('fa_refugee_home_visit', $id, $name, $img);
	}   

	/**
	* 
	* @param undefined $application_id
	* HOME VISIT NEW MEMBERS
	* @return
	*/
	
	public function add_home_visit_member($visit_id =false) 
	{
		$post=$this->input->post();
		if($post) { 
			$this->erp->checkPermissions('add', null, 'home_visits');	
			$firstname = $post['firstname'];
			$firstname_kh = $post['firstname_kh'];
			$lastname = $post['lastname'];
			$lastname_kh = $post['lastname_kh'];
			$gender = $post['gender'];
			$occupation = $post['occupation'];
			$education = $post['education'];
			$nationality = $post['nationality'];
			
			$occupation_kh = $post['occupation_kh'];
			$education_kh = $post['education_kh'];
			$nationality_kh = $post['nationality_kh'];
			
			$relationship = $post['relationship'];
			$country = $post['country'];
			$province = $post['province'];
			$state = $post['state'];
			$district = $post['district'];
			$commune = $post['commune'];
			$address = $post['address'];
			$address_kh = $post['address_kh'];
			$pob = $post['pob'];
			$pob_kh = $post['pob_kh']; 
			$religion_kh = $post['religion_kh'];
			$religion = $post['religion'];
			$ethnicity_kh = $post['ethnicity_kh'];
			$ethnicity = $post['ethnicity'];
			$phone = $post['phone'];
			$village = $this->applications->addTags($post['village'],'village');
			
			$data = array(						
							"visit_id" => $visit_id,
							"firstname" => $firstname,
							"firstname_kh" => $firstname_kh,
							"lastname" => $lastname,
							"lastname_kh" => $lastname_kh,
							"gender" => $gender,
							"occupation" => $occupation,
							"education" => $education,
							"nationality" => $nationality,
							"occupation_kh" => $occupation_kh,
							"education_kh" => $education_kh,
							"nationality_kh" => $nationality_kh,
							"dob" => $this->erp->fld($post['dob']),
							"relationship" => $relationship,
							"country" => $country,
							"province" => $province,
							"state" => $state,
							"district" => $district,
							"commune" => $commune,
							"village" => $village,
							"address" => $address,
							"address_kh" => $address_kh,
							"pob" => $pob,
							"pob_kh" => $pob_kh, 
							"religion_kh" => $religion_kh,
							"religion" => $religion,
							"ethnicity_kh" => $ethnicity_kh,
							"ethnicity" => $ethnicity,
							"phone" => $phone,
						);

			if(isset($_FILES['userfile'])){
				$config_file = array(
						'upload_path' => 'assets/uploads/members',
						'allowed_types' => 'doc|pdf',
					);
				$this->load->library('upload', $config_file);
				if($this->upload->do_upload('userfile')){
					$data["photo"] = $this->upload->data('file_name');
				}
			}
			if($this->applications->addFamilyHomeVisit($data)){
				$this->session->set_flashdata('message', lang("member_added"));
				redirect($_SERVER['HTTP_REFERER'].'#content_4');
			}	
		} 
		
		$this->data['id'] = $visit_id;
		$this->data['modal_js'] = $this->site->modal_js();
		$this->data['relationship'] = $this->applications->getRelationship();
		$this->data['countries'] = $this->applications->getAllCountries();
		$this->data['provinces'] = $this->applications->getAllProvinces();
		$this->data['communes'] = $this->applications->getAllCommunces();
		$this->data['districts'] = $this->applications->getAllDistricts();
		$this->data['states'] = $this->applications->getAllStates();
		$this->load->view($this->theme . 'application_forms2/add_home_visit_member', $this->data);
	}
	
	public function delete_home_visit_member($id = false) 
	{
		$this->erp->checkPermissions('delete', null, 'home_visits');	
		if($id){
			if($this->applications->deleteFamilyHomeVisitRefugee($id)){
				$this->session->set_flashdata('message', lang("new_member_deleted"));
				redirect($_SERVER['HTTP_REFERER'].'#content_4');
			}
		} 
	}
	
	public function edit_home_visit_member($id = false) 
	{
		$post=$this->input->post();
		if($post) { 
			$this->erp->checkPermissions('edit', null, 'home_visits');	
			$firstname = $post['firstname'];
			$firstname_kh = $post['firstname_kh'];
			$lastname = $post['lastname'];
			$lastname_kh = $post['lastname_kh'];
			$gender = $post['gender'];
			$occupation = $post['occupation'];
			$education = $post['education'];
			$nationality = $post['nationality'];			
			$relationship = $post['relationship'];
			
			$occupation_kh = $post['occupation_kh'];
			$education_kh = $post['education_kh'];
			$nationality_kh = $post['nationality_kh'];
			
			$country = $post['country'];
			$province = $post['province'];
			$state = $post['state'];
			$district = $post['district'];
			$commune = $post['commune'];
			$address = $post['address'];
			$address_kh = $post['address_kh'];
			$pob = $post['pob'];
			$pob_kh = $post['pob_kh'];
			$religion_kh = $post['religion_kh'];
			$religion = $post['religion'];
			$ethnicity_kh = $post['ethnicity_kh'];
			$ethnicity = $post['ethnicity'];
			$phone = $post['phone'];
			$village = $this->applications->addTags($post['village'],'village');
			$data = array(
							"firstname" => $firstname,
							"firstname_kh" => $firstname_kh,
							"lastname" => $lastname,
							"lastname_kh" => $lastname_kh,
							"gender" => $gender,
							"occupation" => $occupation,
							"education" => $education,
							"nationality" => $nationality,
							"occupation_kh" => $occupation_kh,
							"education_kh" => $education_kh,
							"nationality_kh" => $nationality_kh,
							"dob" => $this->erp->fld($post['dob']),
							"relationship" => $relationship,
							"country" => $country,
							"province" => $province,
							"state" => $state,
							"district" => $district,
							"commune" => $commune,
							"village" => $village,
							"address" => $address,
							"address_kh" => $address_kh,
							"pob" => $pob,
							"pob_kh" => $pob_kh,
							"religion_kh" => $religion_kh,
							"religion" => $religion,
							"ethnicity_kh" => $ethnicity_kh,
							"ethnicity" => $ethnicity,
							"phone" => $phone,
						); 
			if(isset($_FILES['userfile'])){
				$config_file = array(
						'upload_path' => 'assets/uploads/members',
						'allowed_types' => 'gif|jpg|png|doc|pdf',
					);
				$this->load->library('upload', $config_file);
				if($this->upload->do_upload('userfile')){
					$data["photo"] = $this->upload->data('file_name');
				}
			}
			
			if($this->applications->updateFamilyHomeVisit($id, $data)){
				$this->session->set_flashdata('message', lang("new_member_edited"));
				redirect($_SERVER['HTTP_REFERER'].'#content_4');
			}	
		} 
		$this->data['id'] = $id;
		$this->data['modal_js'] = $this->site->modal_js();
		$this->data['member'] = $this->applications->getFamilyHomeVisitRefugeeById($id); 
		$this->data['relationship'] = $this->applications->getRelationship();
		$this->data['countries'] = $this->applications->getAllCountries();
		$this->data['provinces'] = $this->applications->getAllProvinces();
		$this->data['communes'] = $this->applications->getAllCommunces();
		$this->data['districts'] = $this->applications->getAllDistricts();
		$this->data['states'] = $this->applications->getAllStates();
		$this->load->view($this->theme . 'application_forms2/edit_home_visit_member', $this->data);
	}	
	
	/**
	* 
	* @param undefined $id
	* @param undefined $refugee_id
	* INTERGRATION COMMUNITY
	* @return
	*/
 
	public function refugee_integration_community_request($id=false,$refugee_id=false) 
	{ 
		$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('refugee_integration_community_request_form')));
		$meta = array('page_title' => lang('refugee_integration_community_request_form'), 'bc' => $bc);	
		$this->page_construct('application_forms2/refugee_integration_community_request',$meta, $this->data);
	}

	public function refugee_integration_community_request_form($id = false) 
	{   
		$this->data['application'] = $this->applications->getRefugeeIntergrationById($id); 
		$this->data["decision"] = $this->applications->getRefugeeRecognitionByApplicationId($this->data['application']->application_id);
		$this->data["result"] =  $this->applications->getRSDApplicationById($this->data['application']->application_id);   
		$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('refugee_integration_community_request_form')));
		$meta = array('page_title' => lang('refugee_integration_community_request_form'), 'bc' => $bc);	
		$this->page_construct('application_forms2/refugee_integration_community_request_form',$meta, $this->data);
	}
	
	public function getRefugeeIntegrationIntoCambodiaCommunityRequest()
	{		
		$this->load->library('datatables');		
		$delete = "<a href='#' class='delete-refugee po' title='" . lang("delete_refugee_integration_community_request") . "' data-content=\"<p>"
            . lang('r_u_sure') . "</p><a class='btn btn-danger' href='" . site_url('application_forms2/delete_refugee_integration_community_request/$1') . "'>"
            . lang('i_m_sure') . "</a><button class='btn'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i>"
            . lang('delete_refugee_integration_community_request') . "</a>";		
			
        $action_link = '<div class="btn-group text-left"><button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">'.lang("actions").'<span class="caret"></span></button>
					        <ul class="dropdown-menu pull-right" role="menu">
					             <li><a target="_blank" href="'.site_url('application_forms2/refugee_integration_community_request_form/$1').'" ><i class="fa fa-newspaper-o"></i>'.lang('refugee_integration_community_request_form').'</a></li>   
					             <li><a class="edit-refugee" data-backdrop="static" data-keyboard="false"  data-toggle="modal" data-target="#myModal" href="'.site_url('application_forms2/edit_refugee_integration_community_request/$1').'" ><i class="fa fa-pencil-square-o"></i>'.lang('edit_refugee_integration_community_request').'</a></li>  								 
								 <li>'.$delete.'</li>
					        </ul>
					    </div>';
						
		   $this->datatables->select("
					fa_refugee_intergration_request.id as id,
					CONCAT(case_prefix,case_no) as case_no,
					UPPER(firstname) AS firstname,
					UPPER(lastname) AS lastname,
					dob,
					UPPER(gender) AS gender,
					UPPER(nationality) AS nationality,
					fa_refugee_intergration_request.created_date,
					fa_refugee_intergration_request.status,
					fa_refugee_intergration_request.attachment
					", false)
            ->from("fa_rsd_applications")
			->join("fa_refugee_intergration_request","application_id=fa_rsd_applications.id","right");

        $this->datatables->add_column("Actions", $action_link, "id");
        echo $this->datatables->generate();
		
	}
	
	public function add_refugee_integration_community_request() 
	{ 
	
		$post = $this->input->post();
		
		if($post == true){ 
			
			$application_id = $post['application_id'];
			$country = $post['country'];
			$province = $post['province'];
			$district = $post['district'];
			$commune = $post['commune'];				
			$address = $post['address'];
			$address_kh = $post['address_kh'];
		 	$occupation = $post['occupation']; 
		 	$contact_number = $post['contact_number'];
		 	
		 	$village = $this->applications->addTags($post['village'],'village');
		 	
			$data = array( 
				"application_id"  => $application_id,
				"occupation"  => $occupation,
				"country" => $country,
				"province" => $province,
				"district" => $district,
				"commune" => $commune,
				"village" => $village,					
				"address" => $address,
				"address_kh" => $address_kh,
				"created_by"    => $this->session->userdata("user_id"),
				"created_date"  => $this->erp->fld(date("d/m/Y H:i")), 
				"contact_number" => $contact_number,
			);
			
			if($this->applications->addRefugeeIntergration($data)){
				$this->session->set_flashdata('message', lang("refugee_integration_request_saved"));
				redirect("application_forms2/refugee_integration_community_request");
			}
		}
			$this->data['id'] = $id;
			$this->data['users'] = $this->applications->getUsers();
			$this->data['offices'] = $this->applications->getlocationByOffice();
			$this->data['states'] = $this->applications->getAllStates();
			$this->data['countries'] = $this->applications->getAllCountries();
			$this->data['provinces'] = $this->applications->getAllProvinces();
			$this->data['communes'] = $this->applications->getAllCommunces();
			$this->data['districts'] = $this->applications->getAllDistricts();
			$this->data['modal_js'] = $this->site->modal_js();		
			$this->load->view($this->theme . 'application_forms2/add_refugee_integration_community_request', $this->data);
	}
	
	public function edit_refugee_integration_community_request($id = false) 
	{
		$post = $this->input->post();
		if($post == true ) {
						
			$country = $post['country'];
			$province = $post['province'];
			$district = $post['district'];
			$commune = $post['commune'];				
			$address = $post['address'];
			$address_kh = $post['address_kh'];
		 	$occupation = $post['occupation']; 
		 	$contact_number = $post['contact_number'];
		 	
		 	$village = $this->applications->addTags($post['village'],'village');
		 	
			$data = array( 				
				"occupation"  => $occupation,
				"country" => $country,
				"province" => $province,
				"district" => $district,
				"commune" => $commune,
				"village" => $village,					
				"address" => $address,
				"address_kh" => $address_kh,
				"created_by"    => $this->session->userdata("user_id"),
				"created_date"  => $this->erp->fld(date("d/m/Y H:i")), 
				"contact_number" => $contact_number,
			);  
			
			if($this->applications->updateRefugeeIntergration($id, $data)){
				$this->session->set_flashdata('message', lang("refugee_integration_request_updated"));			
				redirect("application_forms2/refugee_integration_community_request");	
			}
			
		}
		
		$this->data['result'] = $this->applications->getRefugeeIntergrationById($id); 
		$this->data['application'] = $this->applications->getRSDApplicationById($this->data['result']->application_id);
		$this->data['users'] = $this->applications->getUsers();
		$this->data['offices'] = $this->applications->getlocationByOffice();
		$this->data['states'] = $this->applications->getAllStates();
		$this->data['countries'] = $this->applications->getAllCountries();
		$this->data['provinces'] = $this->applications->getAllProvinces();
		$this->data['communes'] = $this->applications->getAllCommunces();
		$this->data['districts'] = $this->applications->getAllDistricts();
		$this->data['modal_js'] = $this->site->modal_js();
		$this->data['id'] = $id;
		$this->load->view($this->theme . 'application_forms2/edit_refugee_integration_community_request', $this->data);
	}
	
	public function delete_refugee_integration_community_request($id=false) 
	{
		if($id){
			if($this->applications->deleteRefugeeIntergration($id)){
				$this->session->set_flashdata('message', lang("refugee_integration_request_deleted"));
				redirect("application_forms2/refugee_integration_community_request");
			}
		}
	}
	
	/**
	* 
	* EXPELLATION REFUGEE
	* @return
	*/
	
	public function expellation_refugee() 
	{
		$this->data["result"] = "";
		$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('expellation_refugee')));
		$this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
		$meta = array('page_title' => lang('expellation_refugee'), 'bc' => $bc);
		$this->page_construct('application_forms2/expellation_refugee',$meta, $this->data);
	}
	
	public function getExpellationRefugee() 
	{
		$this->load->library('datatables');		
		$delete = "<a href='#' class='delete-refugee po' title='" . lang("delete_expellation_refugee") . "' data-content=\"<p>"
            . lang('r_u_sure') . "</p><a class='btn btn-danger' href='" . site_url('application_forms2/delete_expellation_refugee/$1') . "'>"
            . lang('i_m_sure') . "</a><button class='btn'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i>"
            . lang('delete_expellation_refugee') . "</a>";

        $action_link = '<div class="btn-group text-left"><button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">'.lang("actions").'<span class="caret"></span></button>
					        <ul class="dropdown-menu pull-right" role="menu">
					            <li><a target="_blank" href="'.site_url('application_forms2/expellation_form/$1').'" ><i class="fa fa-newspaper-o"></i>'.lang('expellation_form').'</a></li>				             					       
								<li>'.$delete.'</li>
					        </ul>
					    </div>';
		$this->datatables->select("
					fa_refugee_expellation_declaration.id as id,
					CONCAT(case_prefix,case_no) as case_no,
					UPPER(firstname) AS firstname,
					UPPER(lastname) AS lastname,
					dob,
					UPPER(gender) AS gender,
					UPPER(nationality) AS nationality,
					fa_refugee_expellation_declaration.created_date,
					fa_refugee_expellation_declaration.status,
					fa_refugee_expellation_declaration.attachment
					", false)
            ->from("fa_rsd_applications")
			->join("fa_refugee_expellation_declaration","application_id=fa_rsd_applications.id","right");

        $this->datatables->add_column("Actions", $action_link, "id");
        echo $this->datatables->generate();
	}
	
	public function add_expellation_refugee() 
	{ 
		$post = $this->input->post();
		if($post == true){ 
			$data = array(
				"application_id" => $post['application_id'], 
				"created_date" => $this->erp->fld(date("d/m/Y H:i")),
				"created_by" =>  $this->session->userdata("user_id"),
				"status"  => 'pending'
			);   			
			if($this->applications->addExpellationDeclaration($data)){
				$this->session->set_flashdata('message', lang("expellation_refugee_saved"));
				redirect("application_forms2/expellation_refugee/");
			}
		}
		$this->data["result"] = "";
		$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('add_expellation_refugee')));
		$meta = array('page_title' => lang('add_expellation_refugee'), 'bc' => $bc);
		$this->data['modal_js'] = $this->site->modal_js();
		$this->load->view($this->theme . 'application_forms2/add_expellation_refugee', $this->data); 
	}
	
	public function delete_expellation_refugee($id= false) 
	{
		if($id){			
			if($this->applications->deleteExpellationDeclaration($id)){
				$this->session->set_flashdata('message', lang("expellation_refugee_deleted"));
				redirect("application_forms2/expellation_refugee");
			} 
		}
	} 
	
	public function expellation_form($id = false)
	{
		$this->data['row'] = $this->applications->getExpellationDeclarationById($id);
		$this->data["result"] = $this->applications->getRSDApplicationById($this->data['row']->application_id);
		$this->data["recognition"] = $this->application_setup_model->getRegconitionTemplateByType($this->data["result"]->case_prefix);
		$this->data['member'] =  $this->applications->getFamilyMemberAccompanyingByCounseling($this->data["result"]->counseling_id);
		$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('expellation_form')));
		$meta = array('page_title' => lang('expellation_form'), 'bc' => $bc);	
		$this->page_construct('application_forms2/expellation_form',$meta, $this->data);
	}
	
	public function update_status($id = false)
	{
		$status = $this->input->get("status");
		$other = $this->input->get("q");
		if(!empty($id)){  
			 if(!empty($other)){
				 $data= array(
					"other"=>$other
				 );
			 }else {
				  $data= array(
					"other"=>""
				 );
			 }
			 if($status=="is_legal"){
				 $data= array(
					"is_legal"=>"is_legal"
				 );
			 }
			 if($status=="is_legal1"){
				 $data= array(
					"is_legal"=>""
				 );
			 }   
			 if($status=="is_not_legal"){
				 $data= array(
					"is_legal"=>"is_not_legal"
				 );
			 }  
			 if($status=="is_not_legal1"){
				 $data= array(
					"is_legal"=>""
				 );
			 } 

			 
			 if($status=="is_not_divorce"){
				 $data= array(
					"is_divorce"=>"is_not_divorce"
				 );
			 }  
			 if($status=="is_divorce"){
				 $data= array(
					"is_divorce"=>"is_divorce"
				 );
			 }  
			 
			 
			 $result = $this->applications->updateFamilyHomeVisit($id, $data); 
			 if($result) {
				 echo "success";
			}else{		
				 echo "unsuccess";
			}    
		} 
		
		
	}
 
	public function list_of_asylum_seekers() 
	{
		$this->data["result"] = "";
		$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('list_of_asylum_seekers')));
		$this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
		$meta = array('page_title' => lang('list_of_asylum_seekers'), 'bc' => $bc);
		$this->page_construct('application_forms2/list_of_asylum_seekers',$meta, $this->data);
	}

	public function update_member_guarantee($id = false) 
	{
		$post = $this->input->post();
		if($post==true){
			$come = $post["come"];
			$status = $post["status"]; 
			
			
			$data = array(
				'come_option'=>$come,
				'status_option'=>$status
			); 
			$result = $this->applications->updateFamilyGuaranteeRefugee($id, $data);
			 if($result) {
				 echo "success";
			}else{		
				 echo "unsuccess";
			}    
		}  
	}
}

?>
