<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Application_setup extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->library('pagination');
		$this->lang->load('application_setup', $this->Settings->language);
		
		$this->load->model('application_setup_model','application_setup');
		$this->load->model('Site','site');
		$this->site->auditLogs();
	}
	
	public function index()
	{		
		$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('application_setup')));
		$meta = array('page_title' => lang('application_setup'), 'bc' => $bc);	
		$this->page_construct('application_setup/questions',$meta,$this->data);	
	}
	
	public function questions()
	{
		$post = $this->input->post();
		$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('questions')));
		$meta = array('page_title' => lang('questions'), 'bc' => $bc);
		$this->page_construct('application_setup/questions',$meta, $this->data);
	}

	public function getQuestions($id = false)
	{				
        $this->load->library('datatables');	
		$delete = "<a href='#' class='delete-sm po' title='" . lang("delete_question") . "' data-content=\"<p>"
            . lang('r_u_sure') . "</p><a class='btn btn-danger' href='" . site_url('application_setup/delete_question/$1') . "'>"
            . lang('i_m_sure') . "</a><button class='btn'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i>"
            . lang('delete_question') . "</a>";
		
		$action_link = '<div class="btn-group text-left"><button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">'.lang("actions").'<span class="caret"></span></button>
						<ul class="dropdown-menu pull-right" role="menu">					        																	
							<li><a data-backdrop="static" data-keyboard="false"  data-toggle="modal" data-target="#myModal" href="'.site_url('application_setup/edit_question/$1').'" ><i class="fa fa-pencil-square-o"></i>'.lang('edit_question').'</a></li>
						</ul>
					</div>';
	
		 $this->datatables->select("
						fa_questions.id, 
						fa_questions.id as ids, 
						level,
						CONCAT(SUBSTRING(erp_fa_questions.question_kh,1,110),'...') as question_kh, 
						fa_questions.status")
		 ->from("fa_questions");
		 
		$this->datatables->add_column("Actions", $action_link, "fa_questions.id");
        echo $this->datatables->generate();
	} 
	
	public function add_question()
	{
		$post = $this->input->post();		
		if($post == true){
			$data = array(
				'level'   	 => htmlspecialchars($post['level']),
				'question' 	 => htmlspecialchars($post['question']),
				'question_kh'=> htmlspecialchars($post['question_kh']),
				'status'	 =>	htmlspecialchars($post['status'])
 			);
			$result = $this->application_setup->save_question($data);
			if($result){
				$this->session->set_flashdata('message', lang("question_added"));
				redirect($_SERVER["HTTP_REFERER"]);  
			}else{				
				$this->session->set_flashdata('error', validation_errors());
				redirect($_SERVER['HTTP_REFERER']);
			}
		}else{
            $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
			$this->data['modal_js'] = $this->site->modal_js();
		 	$this->load->view($this->theme . 'application_setup/add_question',$this->data);
		}
	}
	
	public function edit_question($id=null)
	{
		$post = $this->input->post();		
		if($post == true)
		{
			$data = array(
				'level'   	 => htmlspecialchars($post['level']),
				'question' 	 => htmlspecialchars($post['question']),
				'question_kh'=> htmlspecialchars($post['question_kh']),
				'status'	 =>	htmlspecialchars($post['status']),
			);
			
			$result = $this->application_setup->save_question($data,$id);
			if(result){
				$this->session->set_flashdata('message', lang("question_updated"));
				redirect($_SERVER["HTTP_REFERER"]);  
			}else{				
				$this->session->set_flashdata('error', validation_errors());
				redirect($_SERVER['HTTP_REFERER']);
			}
			
		}else{
			$this->data['row'] = $this->application_setup->getQuestionByid($id);
            $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
			$this->data['id'] = $id;
			$this->data['modal_js'] = $this->site->modal_js();
		 	$this->load->view($this->theme . 'application_setup/edit_question', $this->data);
		}
	}

	public function delete_question($id=null)
	{
		if (isset($id) || $id != null){        	
        	$delete = $this->application_setup->delete_question($id);
        	if($delete){
        		$this->session->set_flashdata('message', lang("question_deleted"));
            	redirect($_SERVER['HTTP_REFERER']);
        	}else{
            	redirect($_SERVER['HTTP_REFERER']);
            }
        }
	}
	
	public function question_actions($id = null)
    {
    	$post = $this->input->post();
    	if($post['form_action']=="delete" && $_POST['val']){
    		foreach ($_POST['val'] as $id){	    		
				$this->application_setup->delete_question($id);
	        }
	        $this->session->set_flashdata('message', lang("question_deleted"));
	        redirect($_SERVER["HTTP_REFERER"]);    		
    	}else{
			redirect($_SERVER["HTTP_REFERER"]);
		}
    }
	
	public function recognition_templete()
	{		 
		$this->data["result"] = "";
		$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('recognition_templete')));
		$meta = array('page_title' => lang('recognition_templete'), 'bc' => $bc);	
		$this->page_construct('application_setup/recognition_template',$meta,$this->data);
		 		
	}
	
	public function getRecognitionTemplate($id=false) 
	{
		$this->load->library('datatables');	
		$delete = "<a href='#' class='delete-sm po' title='" . lang("delete_recognition_template") . "' data-content=\"<p>"
            . lang('r_u_sure') . "</p><a class='btn btn-danger' href='" . site_url('application_setup/delete_recognition_template/$1') . "'>"
            . lang('i_m_sure') . "</a><button class='btn'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i>"
            . lang('delete_recognition_template') . "</a>";
		
		$action_link = '<div class="btn-group text-left"><button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">'.lang("actions").'<span class="caret"></span></button>
						<ul class="dropdown-menu pull-right" role="menu">					        																	
							<li><a   href="'.site_url('application_setup/edit_recognition_template/$1').'" ><i class="fa fa-pencil-square-o"></i>'.lang('edit_recognition_template').'</a></li>
							<li>'.$delete.'</li>
						</ul>
					</div>'; 
		 $this->datatables->select("fa_recognition.id,
									fa_recognition.id as ids, 
									fa_recognition.name,
									fa_recognition.type   
									")
							->from("fa_recognition"); 
		$this->datatables->add_column("Actions", $action_link, "fa_recognition.id");
        echo $this->datatables->generate();
	}
	
	public function add_recognition_template() 
	{	
		$post = $this->input->post();
		$config = array(
			    array(
                     'field'   => 'case_type',
                     'label'   => lang("case_type"),
                     'rules'   => 'required|is_unique[erp_fa_recognition.type]'
                  )
            ); 
		$this->form_validation->set_rules($config);
		
		if ($this->form_validation->run() == true) {
			if($post == true){
				$data = array(
							"description"   => $post['recognition'],
							"modified_date" => $this->erp->fld(date("d/m/Y H:i")), 
							"type"    		=> $post['case_type'], 
							"name" 			=> $post['note'],
							"brokar2"		=> $post['brokar2'],
							"brokar3" 		=> $post['brokar3'],
							"modified_by" 	=> $this->session->userdata("user_id"),
				);
				$result = $this->application_setup->addRecognition($data);
				if($result){
					$this->session->set_flashdata('message', lang("recognition_template_added"));
					redirect("application_setup/recognition_templete");
				}
			}
		}
		  $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
		$this->data["result"] =  $this->application_setup->getTypeCaseNumber();
		$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('add_recognition_template')));
		$meta = array('page_title' => lang('add_recognition_template'), 'bc' => $bc);	
		$this->page_construct('application_setup/add_recognition_template',$meta,$this->data);
	}
	
	public function edit_recognition_template($id =false) 
	{	 
		$post = $this->input->post();
		if($post == true){
			$data = array(
						"description" 	=> $post['recognition'],
						"modified_date" => $this->erp->fld(date("d/m/Y H:i")), 
						"type"			=> $post['case_type'], 
						"name"			=> $post['note'],
						"brokar2" 		=> $post['brokar2'],
						"brokar3" 		=> $post['brokar3'],
						"modified_by" 	=> $this->session->userdata("user_id"),
						); 
			$result = $this->application_setup->editRecognition($id,$data);
			
			if($result){
				$this->session->set_flashdata('message', lang("recognition_template_edited"));
				redirect("application_setup/recognition_templete");
			}
		}
		$this->data['id'] = $id;
		$this->data["result"] =  $this->application_setup->getTypeCaseNumber();
		$this->data["recognition_template"] =  $this->application_setup->getRegconitionTemplate($id);
		$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('edit_recognition_template')));
		$meta = array('page_title' => lang('edit_recognition_template'), 'bc' => $bc);	
		$this->page_construct('application_setup/edit_recognition_template',$meta,$this->data);
	}
	
	public function delete_recognition_template($id = null) 
	{
		if (isset($id) || $id != null){        	
        	$delete = $this->application_setup->delete_recognition_template($id);
        	if($delete){
        		$this->session->set_flashdata('message', lang("recognition_template_deleted"));
            	redirect($_SERVER['HTTP_REFERER']);
        	}else{
            	redirect($_SERVER['HTTP_REFERER']);
            }
        }
	}
	
	public function  offices()
	{ 
		$this->data["result"] = "";
		$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('office')));
		$meta = array('page_title' => lang('Application_setup'), 'bc' => $bc);	
		$this->page_construct('application_setup/offices',$meta,$this->data);
	}  
	
	public function getOffices($id = false)
	{		
        $this->load->library('datatables');	
		$delete = "<a href='#' class='delete-sm po' title='" . lang("delete_office") . "' data-content=\"<p>"
            . lang('r_u_sure') . "</p><a class='btn btn-danger' href='" . site_url('application_setup/delete_office/$1') . "'>"
            . lang('i_m_sure') . "</a><button class='btn'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i>"
            . lang('delete_office') . "</a>";
		
		$action_link = '<div class="btn-group text-left"><button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">'.lang("actions").'<span class="caret"></span></button>
						<ul class="dropdown-menu pull-right" role="menu">					        																	
							<li><a data-backdrop="static" data-keyboard="false"  data-toggle="modal" data-target="#myModal" href="'.site_url('application_setup/edit_office/$1').'" ><i class="fa fa-pencil-square-o"></i>'.lang('edit_office').'</a></li>
							<li>'.$delete.'</li>
						</ul>
					</div>';
	
		 $this->datatables->select("fa_offices.id,
									fa_offices.id as ids,
									fa_offices.office, 
									fa_offices.location,
									fa_offices.location_kh,
									fa_offices.status 
									")
							->from("fa_offices");
		 
		$this->datatables->add_column("Actions", $action_link, "fa_offices.id");
        echo $this->datatables->generate();
	}
	
	public function add_office() 
	{ 
		$post = $this->input->post();  
		if($post == true){
			$data = array(
				'office'   	 => htmlspecialchars($post['office_en']),
				'office_kh'	 => htmlspecialchars($post['office_kh']),
				'location' 	 => htmlspecialchars($post['location']),
				'location_kh'=> htmlspecialchars($post['location_kh']),
				'status'	 =>	htmlspecialchars($post['status'])
 			); 
			$result = $this->application_setup->save_office($data);
			if($result){
				$this->session->set_flashdata('message', lang("office_added"));
				redirect($_SERVER["HTTP_REFERER"]);  
			}else{				
				$this->session->set_flashdata('error', validation_errors());
				redirect($_SERVER['HTTP_REFERER']);
			}
		}else{
			
            $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
			$this->data['modal_js'] = $this->site->modal_js();
		 	$this->load->view($this->theme . 'application_setup/add_office',$this->data);
		} 
	}
	
	public function edit_office($id=null) 
	{ 
		$post = $this->input->post();	 
		if($post == true)
		{
			$data = array(
				'office'   => htmlspecialchars($post['office_en']),
				'office_kh'   => htmlspecialchars($post['office_kh']),
				'location' 	 => htmlspecialchars($post['location']),
				'location_kh'=> htmlspecialchars($post['location_kh']),
				'status'	 =>	htmlspecialchars($post['status'])
			); 
			$result = $this->application_setup->save_office($data,$id);
			if(result){
				$this->session->set_flashdata('message', lang("office_edited"));
				redirect($_SERVER["HTTP_REFERER"]);  
			}else{				
				$this->session->set_flashdata('error', validation_errors());
				redirect($_SERVER['HTTP_REFERER']);
			}
			
		}else{
			$this->data['row'] = $this->application_setup->getOfficeByid($id);
            $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
			$this->data['id'] = $id;
			$this->data['modal_js'] = $this->site->modal_js();
		 	$this->load->view($this->theme . 'application_setup/edit_office', $this->data);
		}
	}
	
	public function delete_office($id=null)
	{
		if (isset($id) || $id != null){        	
        	$delete = $this->application_setup->delete_office($id);
        	if($delete){
        		$this->session->set_flashdata('message', lang("office_deleted"));
            	redirect($_SERVER['HTTP_REFERER']);
        	}else{
            	redirect($_SERVER['HTTP_REFERER']);
            }
        }
	}
	
	//****address****//
	public function address()
	{ 
		$this->data["result"] = "";
		$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('address')));
		$meta = array('page_title' => lang('Application_setup'), 'bc' => $bc);	
		$this->page_construct('application_setup/address',$meta,$this->data);
	}  
	
	public function getAddress($id = false)
	{		
        $this->load->library('datatables');	
		$delete = "<a href='#' class='delete-sm po' title='" . lang("delete_address") . "' data-content=\"<p>"
            . lang('r_u_sure') . "</p><a class='btn btn-danger' href='" . site_url('application_setup/delete_address/$1') . "'>"
            . lang('i_m_sure') . "</a><button class='btn'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i>"
            . lang('delete_address') . "</a>";
		
		$action_link = '<div class="btn-group text-left"><button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">'.lang("actions").'<span class="caret"></span></button>
						<ul class="dropdown-menu pull-right" role="menu">					        																	
							<li><a data-backdrop="static" data-keyboard="false"  data-toggle="modal" data-target="#myModal" href="'.site_url('application_setup/edit_address/$1').'" ><i class="fa fa-pencil-square-o"></i>'.lang('edit_address').'</a></li>
							<li>'.$delete.'</li>
						</ul>
					</div>';
	
		 $this->datatables->select("fa_address_refugee.id,
									fa_address_refugee.id as ids,
									fa_address_refugee.location,
									fa_address_refugee.location_kh,
									fa_address_refugee.status 
									")
							->from("fa_address_refugee");
		 
		$this->datatables->add_column("Actions", $action_link, "fa_address_refugee.id");
        echo $this->datatables->generate();
	}
	
	public function add_address() 
	{ 
		$post = $this->input->post();  
		if($post == true){
			$data = array(
				'location' 	 => htmlspecialchars($post['location']),
				'location_kh'=> htmlspecialchars($post['location_kh']),
				'status'	 =>	htmlspecialchars($post['status'])
 			); 
			$result = $this->application_setup->save_address($data);
			if($result){
				$this->session->set_flashdata('message', lang("address_added"));
				redirect($_SERVER["HTTP_REFERER"]);  
			}else{				
				$this->session->set_flashdata('error', validation_errors());
				redirect($_SERVER['HTTP_REFERER']);
			}
		}else{
			
            $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
			$this->data['modal_js'] = $this->site->modal_js();
		 	$this->load->view($this->theme . 'application_setup/add_address',$this->data);
		} 
	}
	
	public function edit_address($id=null) 
	{ 
		$post = $this->input->post();	 
		if($post == true)
		{
			$data = array(
				'location' 	 => htmlspecialchars($post['location']),
				'location_kh'=> htmlspecialchars($post['location_kh']),
				'status'	 =>	htmlspecialchars($post['status'])
			); 
			$result = $this->application_setup->save_address($data,$id);
			if(result){
				$this->session->set_flashdata('message', lang("address_edited"));
				redirect($_SERVER["HTTP_REFERER"]);  
			}else{				
				$this->session->set_flashdata('error', validation_errors());
				redirect($_SERVER['HTTP_REFERER']);
			}
			
		}else{
			$this->data['row'] = $this->application_setup->getAddressByid($id);
            $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
			$this->data['id'] = $id;
			$this->data['modal_js'] = $this->site->modal_js();
		 	$this->load->view($this->theme . 'application_setup/edit_address', $this->data);
		}
	}
	
	public function delete_address($id=null)
	{
		if (isset($id) || $id != null){        	
        	$delete = $this->application_setup->delete_address($id);
        	if($delete){
        		$this->session->set_flashdata('message', lang("address_deleted"));
            	redirect($_SERVER['HTTP_REFERER']);
        	}else{
            	redirect($_SERVER['HTTP_REFERER']);
            }
        }
	}
	public function audit_logs()
	{
		$this->data["result"] = "";
		$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('audit_logs')));
		$meta = array('page_title' => lang('application_setup'), 'bc' => $bc);	
		$this->page_construct('application_setup/audit_logs',$meta,$this->data);
	}
	
	public function getAuditLogs($id = false)
	{		
        $this->load->library('datatables');	
		$delete = "<a href='#' class='delete-sm po' title='" . lang("delete_audit_logs") . "' data-content=\"<p>"
            . lang('r_u_sure') . "</p><a class='btn btn-danger' href='" . site_url('application_setup/delete_audit_logs/$1') . "'>"
            . lang('i_m_sure') . "</a><button class='btn'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i>"
            . lang('delete_audit_logs') . "</a>";
		
		$action_link = '<div class="btn-group text-left"><button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">'.lang("actions").'<span class="caret"></span></button>
						<ul class="dropdown-menu pull-right" role="menu">					        																	
							<li>'.$delete.'</li>
						</ul>
					</div>';
					
		 $this->datatables->select("
						fa_audit_logs.ID, 
						UPPER(erp_users.username) as USER_ID, 
						REQUEST_METHOD,
						UPPER(PATH_CONTROLLER) AS PATH_CONTROLLER,
						UPPER(MESSAGES) AS MESSAGES, 
						REQUEST_DATE")
		 ->from("fa_audit_logs")
		 ->join("erp_users","erp_users.id = fa_audit_logs.USER_ID","left");
		 
		$this->datatables->add_column("Actions", $action_link, "fa_audit_logs.ID");
        echo $this->datatables->generate();
	}
	public function delete_audit_logs($id=null)
	{
		if (isset($id) || $id != null){        	
        	$delete = $this->application_setup->delete_audit_logs($id);
        	if($delete){        		
            	redirect($_SERVER['HTTP_REFERER']);
        	}else{
            	redirect($_SERVER['HTTP_REFERER']);
            }
        }
	}
	
	public function case_prefix() 
	{ 
		$this->data["result"] = "";
		$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('case_prefix')));
		$meta = array('page_title' => lang('application_setup'), 'bc' => $bc);	
		$this->page_construct('application_setup/case_prefix',$meta,$this->data);
	}
	
	public function getCaseNoPrefix()  
	{
		$this->load->library('datatables');	
		$delete = "<a href='#' class='delete-sm po' title='" . lang("delete_case_prefix") . "' data-content=\"<p>"
            . lang('r_u_sure') . "</p><a class='btn btn-danger' href='" . site_url('application_setup/delete_case_prefix/$1') . "'>"
            . lang('i_m_sure') . "</a><button class='btn'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i>"
            . lang('delete_case_prefix') . "</a>";
		
		$action_link = '<div class="btn-group text-left"><button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">'.lang("actions").'<span class="caret"></span></button>
						<ul class="dropdown-menu pull-right" role="menu">					        																	
							<li><a data-backdrop="static" data-keyboard="false"  data-toggle="modal" data-target="#myModal" href="'.site_url('application_setup/edit_case_prefix/$1').'" ><i class="fa fa-pencil-square-o"></i>'.lang('edit_case_prefix').'</a></li>
							<li>'.$delete.'</li>
						</ul>
					</div>';
	
		 $this->datatables->select("fa_case_prefix.id,
									fa_case_prefix.id as ids, 
									fa_case_prefix.case_prefix,
									fa_case_prefix.status 
									")
							->from("fa_case_prefix");
		 
		$this->datatables->add_column("Actions", $action_link, "fa_case_prefix.id");
        echo $this->datatables->generate();
	}
	
	public function add_case_prefix() 
	{
		$post = $this->input->post();		
		if($post == true){
			$data = array( 
				'case_prefix' 	 => htmlspecialchars($post['case_prefix']), 
				'status'	 =>	htmlspecialchars($post['status'])
 			);
			$result = $this->application_setup->save_case_prefix($data);
			if($result){
				$this->session->set_flashdata('message', lang("case_prefix_added"));
				redirect($_SERVER["HTTP_REFERER"]);  
			}else{				
				$this->session->set_flashdata('error', validation_errors());
				redirect($_SERVER['HTTP_REFERER']);
			}
		}else{
            $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
			$this->data['modal_js'] = $this->site->modal_js();
		 	$this->load->view($this->theme . 'application_setup/add_case_prefix',$this->data);
		}
	}

	public function edit_case_prefix($id=null) 
	{
		$post = $this->input->post();		
		if($post == true){
			$data = array( 
				'case_prefix' 	 => htmlspecialchars($post['case_prefix']), 
				'status'	 =>	htmlspecialchars($post['status'])
 			);
			$result = $this->application_setup->save_case_prefix($data,$id);
			if($result){
				$this->session->set_flashdata('message', lang("case_prefix_edited"));
				redirect($_SERVER["HTTP_REFERER"]);  
			}else{				
				$this->session->set_flashdata('error', validation_errors());
				redirect($_SERVER['HTTP_REFERER']);
			}
		}else{
			$this->data['id'] = $id;
			$this->data['result'] = $this->application_setup->getCasePrefixById($id);
            $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
			$this->data['modal_js'] = $this->site->modal_js();
		 	$this->load->view($this->theme . 'application_setup/edit_case_prefix',$this->data);
		}
	}
	
	public function delete_case_prefix($id = null)
	{
		if (isset($id) || $id != null){        	
        	$delete = $this->application_setup->delete_case_prefix($id);
        	if($delete){
        		$this->session->set_flashdata('message', lang("case_prefix_deleted"));
            	redirect($_SERVER['HTTP_REFERER']);
        	}else{
            	redirect($_SERVER['HTTP_REFERER']);
            }
        }
	}


	public function family_relationship() 
	{ 
		$this->data["result"] = "";
		$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('family_relationship')));
		$meta = array('page_title' => lang('application_setup'), 'bc' => $bc);	
		$this->page_construct('application_setup/family_relationship',$meta,$this->data);
	} 
	
	
	public function getFamilyRelationship()  
	{
		$this->load->library('datatables');	
		$delete = "<a href='#' class='delete-sm po' title='" . lang("delete_case_prefix") . "' data-content=\"<p>"
            . lang('r_u_sure') . "</p><a class='btn btn-danger' href='" . site_url('application_setup/delete_family_relationship/$1') . "'>"
            . lang('i_m_sure') . "</a><button class='btn'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i>"
            . lang('delete_family_relationship') . "</a>"; 
		$action_link = '<div class="btn-group text-left"><button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">'.lang("actions").'<span class="caret"></span></button>
						<ul class="dropdown-menu pull-right" role="menu">					        																	
							<li><a data-backdrop="static" data-keyboard="false"  data-toggle="modal" data-target="#myModal" href="'.site_url('application_setup/edit_family_relationship/$1').'" ><i class="fa fa-pencil-square-o"></i>'.lang('edit_family_relationship').'</a></li>
							<li>'.$delete.'</li>
						</ul>
					</div>';
		 $this->datatables->select("fa_family_relationship.id,
									fa_family_relationship.id as ids, 
									upper(relationship),
									fa_family_relationship.relationship_kh,
									fa_family_relationship.status 
									")
							->from("fa_family_relationship");
		 
		$this->datatables->add_column("Actions", $action_link, "fa_family_relationship.id");
        echo $this->datatables->generate();
	}
	
	public function edit_family_relationship($id = null) 
	{
		$post = $this->input->post();		
		if($post == true){
			$data = array( 
				'relationship' 	 => htmlspecialchars($post['relationship']), 
				'relationship_kh'  => htmlspecialchars($post['relationship_kh']), 
				'status'	 =>	htmlspecialchars($post['status'])
 			);
			$result = $this->application_setup->save_family_relationship($data,$id);
			if($result){
				$this->session->set_flashdata('message', lang("family_relationship_edited"));
				redirect($_SERVER["HTTP_REFERER"]);  
			}else{				
				$this->session->set_flashdata('error', validation_errors());
				redirect($_SERVER['HTTP_REFERER']);
			}
		}else{
			$this->data['id'] = $id;
			$this->data['result'] = $this->application_setup->getFamilyRelationshipById($id);
            $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
			$this->data['modal_js'] = $this->site->modal_js();
		 	$this->load->view($this->theme . 'application_setup/edit_family_relationship',$this->data);
		}
	}
	
	public function add_family_relationship() 
	{ 
		$post = $this->input->post();		
		if($post == true){
			$data = array( 
				'relationship' 	 => htmlspecialchars($post['relationship']), 
				'relationship_kh'  => htmlspecialchars($post['relationship_kh']), 
				'status'	 =>	htmlspecialchars($post['status'])
 			); 
			$result = $this->application_setup->save_family_relationship($data);
			if($result){
				$this->session->set_flashdata('message', lang("family_relationship_added"));
				redirect($_SERVER["HTTP_REFERER"]);  
			}else{				
				$this->session->set_flashdata('error', validation_errors());
				redirect($_SERVER['HTTP_REFERER']);
			}
			
		}else{
            $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
			$this->data['modal_js'] = $this->site->modal_js();
		 	$this->load->view($this->theme . 'application_setup/add_family_relationship',$this->data);
		}
	}
	
	public function delete_family_relationship($id=null) 
	{
		if (isset($id) || $id != null){        	
        	$delete = $this->application_setup->deleteFamilyRelationship($id);
        	if($delete){
        		$this->session->set_flashdata('message', lang("family_relationship_deleted"));
            	redirect($_SERVER['HTTP_REFERER']);
        	}else{
            	redirect($_SERVER['HTTP_REFERER']);
            }
        } 	
	}
	 
	public function department_refugee() 
	{ 
		$this->data["result"] = "";
		$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('department_refugee')));
		$meta = array('page_title' => lang('application_setup'), 'bc' => $bc);	
		$this->page_construct('application_setup/department_refugee',$meta,$this->data);
	} 
	
	public function getDepartmentRefugee()  
	{
		$this->load->library('datatables');	
		$delete = "<a href='#' class='delete-sm po' title='" . lang("delete_department_refugee") . "' data-content=\"<p>"
            . lang('r_u_sure') . "</p><a class='btn btn-danger' href='" . site_url('application_setup/delete_department_refugee/$1') . "'>"
            . lang('i_m_sure') . "</a><button class='btn'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i>"
            . lang('delete_department_refugee') . "</a>"; 
		$action_link = '<div class="btn-group text-left"><button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">'.lang("actions").'<span class="caret"></span></button>
						<ul class="dropdown-menu pull-right" role="menu">					        																	
							<li><a data-backdrop="static" data-keyboard="false"  data-toggle="modal" data-target="#myModal" href="'.site_url('application_setup/edit_department_refugee/$1').'" ><i class="fa fa-pencil-square-o"></i>'.lang('edit_department_refugee').'</a></li>
							<li>'.$delete.'</li>
						</ul>
					</div>';
		 $this->datatables->select("fa_signature.id,
									fa_signature.id as ids, 
									fa_signature.department_kh,
									fa_signature.department_en ,
									fa_signature.status 
									")
							->from("fa_signature");
		 
		$this->datatables->add_column("Actions", $action_link, "fa_signature.id");
        echo $this->datatables->generate();
	}
	
	public function add_department_refugee() 
	{ 
		$post = $this->input->post();		
		if($post == true){
			$data = array( 
				'department_kh' 	 => htmlspecialchars($post['department_kh']), 
				'department_en'  => htmlspecialchars($post['department_en']), 
				'status'	 =>	htmlspecialchars($post['status'])
 			); 
			$result = $this->application_setup->save_department_refugee($data);
			if($result){
				$this->session->set_flashdata('message', lang("department_refugee_added"));
				redirect($_SERVER["HTTP_REFERER"]);  
			}else{				
				$this->session->set_flashdata('error', validation_errors());
				redirect($_SERVER['HTTP_REFERER']);
			}
			
		}else{
            $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
			$this->data['modal_js'] = $this->site->modal_js();
		 	$this->load->view($this->theme . 'application_setup/add_department_refugee',$this->data);
		}
	}	
	
	public function edit_department_refugee($id=false) 
	{ 
		$post = $this->input->post(); 
		if($post == true){
			$data = array( 
				'department_kh' 	 => htmlspecialchars($post['department_kh']), 
				'department_en'  => htmlspecialchars($post['department_en']), 
				'status'	 =>	htmlspecialchars($post['status'])
 			); 
			$result = $this->application_setup->save_department_refugee($data,$id);
			if($result){
				$this->session->set_flashdata('message', lang("department_refugee_edited"));
				redirect($_SERVER["HTTP_REFERER"]);  
			}else{				
				$this->session->set_flashdata('error', validation_errors());
				redirect($_SERVER['HTTP_REFERER']);
			}
			
		}else{
			$this->data['id'] = $id;
			$this->data['result'] = $this->application_setup->getDepartmentRefugeeById($id);	  
			$this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
			$this->data['modal_js'] = $this->site->modal_js();
		 	$this->load->view($this->theme . 'application_setup/edit_department_refugee',$this->data);
		}
	}
	
	public function delete_department_refugee($id=null) 
	{
		if (isset($id) || $id != null){        	
        	$delete = $this->application_setup->deleteDepartmentRefugee($id);
        	if($delete){
        		$this->session->set_flashdata('message', lang("department_refugee_deleted"));
            	redirect($_SERVER['HTTP_REFERER']);
        	}else{
            	redirect($_SERVER['HTTP_REFERER']);
            }
        } 	
	}
   
	public function countries()
    {
        $this->data["result"] = "";
		$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('countries')));
		$meta = array('page_title' => lang('application_setup'), 'bc' => $bc);	
		$this->page_construct('application_setup/countries',$meta,$this->data);
    }
    
	public function getCountries()
    {
        $this->load->library('datatables');	
		$delete = "<a href='#' class='delete-sm po' title='" . lang("delete_country") . "' data-content=\"<p>"
            . lang('r_u_sure') . "</p><a class='btn btn-danger' href='" . site_url('application_setup/delete_Country/$1') . "'>"
            . lang('i_m_sure') . "</a><button class='btn'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i>"
            . lang('delete_country') . "</a>"; 
		$action_link = '<div class="btn-group text-left"><button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">'.lang("actions").'<span class="caret"></span></button>
						<ul class="dropdown-menu pull-right" role="menu">					        																	
							<li><a data-backdrop="static" data-keyboard="false"  data-toggle="modal" data-target="#myModal" href="'.site_url('application_setup/edit_Country/$1').'" ><i class="fa fa-pencil-square-o"></i>'.lang('edit_country').'</a></li>
							<li>'.$delete.'</li>
						</ul>
					</div>';
		 $this->datatables->select("fa_countries.id,
									fa_countries.id as ids, 
									fa_countries.country_code,
									fa_countries.iso_code,
                                          fa_countries.country,
                                          fa_countries.native_name,
                                          fa_countries.region
								  ")
							->from("fa_countries");
		 
		$this->datatables->add_column("Actions", $action_link, "fa_countries.id");
        echo $this->datatables->generate();
    }
    
	public function add_country()
    {
       $post = $this->input->post();		
		if($post == true){
			$data = array( 
				'country_code' 	    =>htmlspecialchars($post['country_code']), 
				'iso_code'              =>htmlspecialchars($post['iso_code']), 
				'country'	          =>htmlspecialchars($post['country']),
				'native_name'	   =>htmlspecialchars($post['native_name']),
				'region'	                 =>htmlspecialchars($post['region'])
 			); 
			$result = $this->application_setup->save_country($data);
			if($result){
				$this->session->set_flashdata('message', lang("countries_added"));
				redirect($_SERVER["HTTP_REFERER"]);  
			}else{				
				$this->session->set_flashdata('error', validation_errors());
				redirect($_SERVER['HTTP_REFERER']);
			}
			
		}else{
            $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
			$this->data['modal_js'] = $this->site->modal_js();
		 	$this->load->view($this->theme . 'application_setup/add_country',$this->data);
		} 
    }	
    
	public function edit_country($id=null)
    {
        $post = $this->input->post();		
		if($post == true){
			$data = array( 
				'country_code' 	  =>htmlspecialchars($post['country_code']), 
				'iso_code'             =>htmlspecialchars($post['iso_code']), 
				'country'	             =>htmlspecialchars($post['country']),
				'native_name'	  =>htmlspecialchars($post['native_name']),
				'region'	             =>htmlspecialchars($post['region'])
 			);
			$result = $this->application_setup->save_country($data,$id);
			if($result){
				$this->session->set_flashdata('message', lang("countries_edited"));
				redirect($_SERVER["HTTP_REFERER"]);  
			}else{				
				$this->session->set_flashdata('error', validation_errors());
				redirect($_SERVER['HTTP_REFERER']);
			}
		}else{
			$this->data['id'] = $id;
			$this->data['result'] = $this->application_setup->getCountriesById($id);
            $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
			$this->data['modal_js'] = $this->site->modal_js();
		 	$this->load->view($this->theme . 'application_setup/edit_country',$this->data);
		}
    }
    
	public function delete_country($id=null)
    {
        if (isset($id) || $id != null){        	
        	$delete = $this->application_setup->deleteCounrty($id);
        	if($delete){
        		$this->session->set_flashdata('message', lang("country_deleted"));
            	redirect($_SERVER['HTTP_REFERER']);
        	}else{
            	redirect($_SERVER['HTTP_REFERER']);
            }
        } 	
    }
	//****states****//
	public function states()
	{
		$this->data["result"] = "";
		$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('states')));
		$meta = array('page_title' => lang('application_setup'), 'bc' => $bc);	
		$this->page_construct('application_setup/states',$meta,$this->data);
	}
	
	public function getStates()
	{
		$this->load->library('datatables');	
		$delete = "<a href='#' class='delete-sm po' title='" . lang("delete_state") . "' data-content=\"<p>"
            . lang('r_u_sure') . "</p><a class='btn btn-danger' href='" . site_url('application_setup/delete_state/$1') . "'>"
            . lang('i_m_sure') . "</a><button class='btn'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i>"
            . lang('delete_state') . "</a>"; 
		$action_link = '<div class="btn-group text-left"><button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">'.lang("actions").'<span class="caret"></span></button>
						<ul class="dropdown-menu pull-right" role="menu">					        																	
							<li><a data-backdrop="static" data-keyboard="false"  data-toggle="modal" data-target="#myModal" href="'.site_url('application_setup/edit_state/$1').'" ><i class="fa fa-pencil-square-o"></i>'.lang('edit_state').'</a></li>
							<li>'.$delete.'</li>
						</ul>
					</div>';
		 $this->datatables->select("fa_states.id,
									fa_states.id as ids, 
									fa_countries.country,
									fa_states.code,	
									fa_states.name,
                                    fa_states.native_name
								  ")
							->from("fa_states")
							->join("fa_countries","fa_countries.id = fa_states.country_id","left");
		$this->datatables->add_column("Actions", $action_link, "fa_states.id");
        echo $this->datatables->generate();
	}
	
	public function add_state()
	{
		$post = $this->input->post();		
		if($post == true){
			$data = array( 
			  'country_id'  =>htmlspecialchars($post['countries']),
				'code' 	    =>htmlspecialchars($post['code']), 
				'name'              =>htmlspecialchars($post['name']), 
				'native_name'	   =>htmlspecialchars($post['native_name'])

 			); 
			$result = $this->application_setup->save_state($data);
			if($result){
				$this->session->set_flashdata('message', lang("state_added"));
				redirect($_SERVER["HTTP_REFERER"]);  
			}else{				
				$this->session->set_flashdata('error', validation_errors());
				redirect($_SERVER['HTTP_REFERER']);
			} 
		}else{
         $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
			$this->data['modal_js'] = $this->site->modal_js();
			$this->data['country'] = $this->application_setup->getAllCountries();
		 	$this->load->view($this->theme . 'application_setup/add_state',$this->data);
		} 
	}
	
	public function edit_state($id=null)
	{
		$post = $this->input->post();		
		if($post == true){
			$data = array( 
			 'country_id'  =>htmlspecialchars($post['countries']),
				'code' 	    =>htmlspecialchars($post['code']), 
				'name'              =>htmlspecialchars($post['name']), 
				'native_name'	   =>htmlspecialchars($post['native_name'])
 			);
			$result = $this->application_setup->save_state($data,$id);
			if($result){
				$this->session->set_flashdata('message', lang("state_edited"));
				redirect($_SERVER["HTTP_REFERER"]);  
			}else{				
				$this->session->set_flashdata('error', validation_errors());
				redirect($_SERVER['HTTP_REFERER']);
			}
		}else{
			$this->data['id'] = $id;
			$this->data['result'] = $this->application_setup->getStatesById($id);
			$this->data['country'] = $this->application_setup->getAllCountries();
			$this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
			$this->data['modal_js'] = $this->site->modal_js();
		 	$this->load->view($this->theme . 'application_setup/edit_state',$this->data);
		}
	}
    
	public function delete_state($id=null)
    {
	 
	  if (isset($id) || $id != null){        	
        	$delete = $this->application_setup->deleteState($id);
        	if($delete){
        		$this->session->set_flashdata('message', lang("state_deleted"));
            	redirect($_SERVER['HTTP_REFERER']);
        	}else{
            	redirect($_SERVER['HTTP_REFERER']);
            }
        } 	
   }
  
	public function provinces()
    {
		$this->data["result"] = "";
			$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('Provinces')));
			$meta = array('page_title' => lang('application_setup'), 'bc' => $bc);	
			$this->page_construct('application_setup/provinces',$meta,$this->data); 
	}
	
	public function getProvinces()
	{
	$this->load->library('datatables');	
		$delete = "<a href='#' class='delete-sm po' title='" . lang("delete_province") . "' data-content=\"<p>"
            . lang('r_u_sure') . "</p><a class='btn btn-danger' href='" . site_url('application_setup/delete_province/$1') . "'>"
            . lang('i_m_sure') . "</a><button class='btn'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i>"
            . lang('delete_province') . "</a>"; 
		$action_link = '<div class="btn-group text-left"><button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">'.lang("actions").'<span class="caret"></span></button>
						<ul class="dropdown-menu pull-right" role="menu">					        																	
							<li><a data-backdrop="static" data-keyboard="false"  data-toggle="modal" data-target="#myModal" href="'.site_url('application_setup/edit_province/$1').'" ><i class="fa fa-pencil-square-o"></i>'.lang('edit_province').'</a></li>
							<li>'.$delete.'</li>
						</ul>
					</div>';
		 $this->datatables->select("fa_provinces.id,
									fa_provinces.id as ids, 									
									fa_countries.country,
									fa_states.name as state,
									fa_provinces.code,
									fa_provinces.name,	
									fa_provinces.native_name
								  ")
							->from("fa_provinces") 
							->join("fa_countries","fa_provinces.country_id = fa_countries.id","left")
							->join("fa_states","fa_provinces.state_id = fa_states.id","left");
		$this->datatables->add_column("Actions", $action_link, "fa_provinces.id");
        echo $this->datatables->generate(); 
	}
	
	public function add_province()
	{
		$post = $this->input->post();		
		if($post == true){
			$data = array( 
			  'country_id'  =>htmlspecialchars($post['countries']),
			  'state_id'  =>htmlspecialchars($post['states']),
				'code' 	    =>htmlspecialchars($post['code']), 
				'name'              =>htmlspecialchars($post['name']), 
				'native_name'	   =>htmlspecialchars($post['native_name'])

 			); 
			$result = $this->application_setup->save_province($data,$id);
			if($result){
				$this->session->set_flashdata('message', lang("province_added"));
				redirect($_SERVER["HTTP_REFERER"]);  
			}else{				
				$this->session->set_flashdata('error', validation_errors());
				redirect($_SERVER['HTTP_REFERER']);
			} 
		}else{
         $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
			$this->data['modal_js'] = $this->site->modal_js();
			$this->data['country'] = $this->application_setup->getAllCountries();
			$this->data['state'] = $this->application_setup->getAllStates();
		 	$this->load->view($this->theme . 'application_setup/add_province',$this->data);
		} 
	}
	
	public function edit_province($id=null)
	{
		$post = $this->input->post();		
		if($post == true){
			$data = array( 
			  'country_id'  =>htmlspecialchars($post['countries']),
			  'state_id'  =>htmlspecialchars($post['states']),
				'code' 	    =>htmlspecialchars($post['code']), 
				'name'              =>htmlspecialchars($post['name']), 
				'native_name'	   =>htmlspecialchars($post['native_name'])

 			); 
			$result = $this->application_setup->save_province($data,$id);
			if($result){
				$this->session->set_flashdata('message', lang("province_edited"));
				redirect($_SERVER["HTTP_REFERER"]);  
			}else{				
				$this->session->set_flashdata('error', validation_errors());
				redirect($_SERVER['HTTP_REFERER']);
			} 
		}else{
			$this->data['id'] = $id;
			$this->data['result'] = $this->application_setup->getProvincesById($id);
         $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
			$this->data['modal_js'] = $this->site->modal_js();
			$this->data['country'] = $this->application_setup->getAllCountries();
			$this->data['state'] = $this->application_setup->getAllStates();
		 	$this->load->view($this->theme . 'application_setup/edit_province',$this->data);
		} 
	}
	
	public function delete_province($id=null)
	{
		 
	  if (isset($id) || $id != null){        	
        	$delete = $this->application_setup->delete_province($id);
        	if($delete){
        		$this->session->set_flashdata('message', lang("province_deleted"));
            	redirect($_SERVER['HTTP_REFERER']);
        	}else{
            	redirect($_SERVER['HTTP_REFERER']);
            }
        } 	
	}
	
	public function communces()
	{
		$this->data["result"] = "";
			$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('Communces')));
			$meta = array('page_title' => lang('application_setup'), 'bc' => $bc);	
			$this->page_construct('application_setup/communces',$meta,$this->data); 
	}
	
	public function getCommunces()
	{
		$this->load->library('datatables');	
		$delete = "<a href='#' class='delete-sm po' title='" . lang("delete_communce") . "' data-content=\"<p>"
            . lang('r_u_sure') . "</p><a class='btn btn-danger' href='" . site_url('application_setup/delete_communce/$1') . "'>"
            . lang('i_m_sure') . "</a><button class='btn'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i>"
            . lang('delete_communce') . "</a>"; 
		$action_link = '<div class="btn-group text-left"><button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">'.lang("actions").'<span class="caret"></span></button>
						<ul class="dropdown-menu pull-right" role="menu">					        																	
							<li><a data-backdrop="static" data-keyboard="false"  data-toggle="modal" data-target="#myModal" href="'.site_url('application_setup/edit_communce/$1').'" ><i class="fa fa-pencil-square-o"></i>'.lang('edit_communce').'</a></li>
							<li>'.$delete.'</li>
						</ul>
					</div>';
					 $this->datatables->select("fa_communces.id,
					                                 fa_communces.id as ids,
									  			fa_districts.name,
												fa_communces.code,
												fa_communces.name as communces,
							 					fa_communces.native_name, 
							                       	  ")
							->from("fa_communces") 
							->join("erp_fa_districts","erp_fa_districts.id=erp_fa_communces.district_id","inner");
						   //->join("fa_states","fa_provinces.state_id = fa_states.id","inner");
						   //->join("fa_provinces","fa_communces.district_id = fa_province.id","inner");
		$this->datatables->add_column("Actions", $action_link, "fa_communces.id");
        echo $this->datatables->generate(); 
	}
	
	public function add_communce()
	{
		$post = $this->input->post();		
		if($post == true){
			$data = array( 
			  'district_id'  =>htmlspecialchars($post['communces']),
				'code' 	    =>htmlspecialchars($post['code']), 
				'name'              =>htmlspecialchars($post['name']), 
				'native_name'	   =>htmlspecialchars($post['native_name'])

 			); 
			$result = $this->application_setup->save_communce($data,$id);
			if($result){
				$this->session->set_flashdata('message', lang("communce_added"));
				redirect($_SERVER["HTTP_REFERER"]);  
			}else{				
				$this->session->set_flashdata('error', validation_errors());
				redirect($_SERVER['HTTP_REFERER']);
			} 
		}else{
         $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
			$this->data['modal_js'] = $this->site->modal_js();
			$this->data['districts'] = $this->application_setup->getAllDistrict();
		 	$this->load->view($this->theme . 'application_setup/add_communce',$this->data);
		} 
	}
	
	public function edit_communce($id=null)
	{
		$post = $this->input->post();		
		if($post == true){
			$data = array( 
			  'district_id'  =>htmlspecialchars($post['communces']),
				'code' 	    =>htmlspecialchars($post['code']), 
				'name'              =>htmlspecialchars($post['name']), 
				'native_name'	   =>htmlspecialchars($post['native_name'])

 			); 
			$result = $this->application_setup->save_communce($data,$id);
			if($result){
				$this->session->set_flashdata('message', lang("communce_edited"));
				redirect($_SERVER["HTTP_REFERER"]);  
			}else{				
				$this->session->set_flashdata('error', validation_errors());
				redirect($_SERVER['HTTP_REFERER']);
			} 
		}else{
			$this->data['id'] = $id;
			$this->data['result'] = $this->application_setup->getCommuncesById($id);
			$this->data['districts'] = $this->application_setup->getAllDistrict();
         $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
			$this->data['modal_js'] = $this->site->modal_js();
		 	$this->load->view($this->theme . 'application_setup/edit_communce',$this->data);
		} 
	}
	
	public function delete_communce($id=null)
	{
	   if (isset($id) || $id != null){        	
        	$delete = $this->application_setup->delete_communce($id);
        	if($delete){
        		$this->session->set_flashdata('message', lang("communce_deleted"));
            	redirect($_SERVER['HTTP_REFERER']);
        	}else{
            	redirect($_SERVER['HTTP_REFERER']);
            }
        } 		
	}
	
	public function districts()
	{
	  $this->data["result"] = "";
			$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('Districts')));
			$meta = array('page_title' => lang('application_setup'), 'bc' => $bc);	
			$this->page_construct('application_setup/districts',$meta,$this->data); 	
	}
	
	public function getDistricts()
	{
		$this->load->library('datatables');	
		$delete = "<a href='#' class='delete-sm po' title='" . lang("delete_district") . "' data-content=\"<p>"
            . lang('r_u_sure') . "</p><a class='btn btn-danger' href='" . site_url('application_setup/delete_district/$1') . "'>"
            . lang('i_m_sure') . "</a><button class='btn'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i>"
            . lang('delete_district') . "</a>"; 
		$action_link = '<div class="btn-group text-left"><button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">'.lang("actions").'<span class="caret"></span></button>
						<ul class="dropdown-menu pull-right" role="menu">					        																	
							<li><a data-backdrop="static" data-keyboard="false"  data-toggle="modal" data-target="#myModal" href="'.site_url('application_setup/edit_district/$1').'" ><i class="fa fa-pencil-square-o"></i>'.lang('edit_district').'</a></li>
							<li>'.$delete.'</li>
						</ul>
					</div>';
					 $this->datatables->select("fa_districts.id,
					                                 fa_districts.id as ids,
									  			fa_provinces.name,
												fa_districts.code,
												fa_districts.name as communces,
												fa_districts.native_name, 
							                       	  ")
							->from("fa_districts") 
							->join("fa_provinces","fa_provinces.id=fa_districts.province_id","inner");
						   //->join("fa_states","fa_provinces.state_id = fa_states.id","inner");
						   //->join("fa_provinces","fa_communces.district_id = fa_province.id","inner");
		$this->datatables->add_column("Actions", $action_link, "fa_districts.id");
        echo $this->datatables->generate(); 
	}
	
	public function add_district()
	{
		$post = $this->input->post();		
		if($post == true){
			$data = array( 
			  'province_id'  =>htmlspecialchars($post['districts']),
				'code' 	    =>htmlspecialchars($post['code']), 
				'name'              =>htmlspecialchars($post['name']), 
				'native_name'	   =>htmlspecialchars($post['native_name'])

 			); 
			$result = $this->application_setup->save_districts($data,$id);
			if($result){
				$this->session->set_flashdata('message', lang("district_added"));
				redirect($_SERVER["HTTP_REFERER"]);  
			}else{				
				$this->session->set_flashdata('error', validation_errors());
				redirect($_SERVER['HTTP_REFERER']);
			} 
		}else{
         $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
			$this->data['modal_js'] = $this->site->modal_js();
			$this->data['provinces'] = $this->application_setup->getAllProvince();
		 	$this->load->view($this->theme . 'application_setup/add_district',$this->data);
		} 
	}
	
	public function edit_district($id=null)
	{
		$post = $this->input->post();		
		if($post == true){
			$data = array( 
			  'province_id'  =>htmlspecialchars($post['districts']),
				'code' 	    =>htmlspecialchars($post['code']), 
				'name'              =>htmlspecialchars($post['name']), 
				'native_name'	   =>htmlspecialchars($post['native_name'])

 			); 
			$result = $this->application_setup->save_districts($data,$id);
			if($result){
				$this->session->set_flashdata('message', lang("district_edited"));
				redirect($_SERVER["HTTP_REFERER"]);  
			}else{				
				$this->session->set_flashdata('error', validation_errors());
				redirect($_SERVER['HTTP_REFERER']);
			} 
		}else{
			$this->data['id'] = $id;
			$this->data['result'] = $this->application_setup->getDistrictById($id);
			$this->data['provinces'] = $this->application_setup->getAllProvince();
         $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
			$this->data['modal_js'] = $this->site->modal_js();
		 	$this->load->view($this->theme . 'application_setup/edit_district',$this->data);
		} 
	}
	
	public function delete_district($id=null)
	{
		 if (isset($id) || $id != null){        	
        	$delete = $this->application_setup->delete_district($id);
        	if($delete){
        		$this->session->set_flashdata('message', lang("district_deleted"));
            	redirect($_SERVER['HTTP_REFERER']);
        	}else{
            	redirect($_SERVER['HTTP_REFERER']);
            }
        } 		
	}
	
	public function tags()
	{
		$this->data["result"] = "";
			$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('tags')));
			$meta = array('page_title' => lang('application_setup'), 'bc' => $bc);	
			$this->page_construct('application_setup/tags',$meta,$this->data); 	
	}
	
	public function getTag()
	{
		$this->load->library('datatables');	
		$delete = "<a href='#' class='delete-sm po' title='" . lang("delete_tag") . "' data-content=\"<p>"
            . lang('r_u_sure') . "</p><a class='btn btn-danger' href='" . site_url('application_setup/delete_tag/$1') . "'>"
            . lang('i_m_sure') . "</a><button class='btn'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i>"
            . lang('delete_tag') . "</a>"; 
		$action_link = '<div class="btn-group text-left"><button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">'.lang("actions").'<span class="caret"></span></button>
						<ul class="dropdown-menu pull-right" role="menu">					        																	
							<li><a data-backdrop="static" data-keyboard="false"  data-toggle="modal" data-target="#myModal" href="'.site_url('application_setup/edit_tag/$1').'" ><i class="fa fa-pencil-square-o"></i>'.lang('edit_tag').'</a></li>
							<li>'.$delete.'</li>
						</ul>
					</div>';
					 $this->datatables->select("fa_tags.id,
					                                 fa_tags.id as ids,
									  			fa_tags.name,
												fa_tags.native_name, 
							                       	  ")
							->from("fa_tags") ;
		  $this->datatables->add_column("Actions", $action_link, "fa_tags.id");
        echo $this->datatables->generate(); 
	}
	
	public function add_tag()
	{
		$post = $this->input->post();		
		if($post == true){
			$data = array( 
				'name'              =>htmlspecialchars($post['name']), 
				'native_name'	   =>htmlspecialchars($post['native_name'])

 			); 
			$result = $this->application_setup->save_tags($data,$id);
			if($result){
				$this->session->set_flashdata('message', lang("tag_added"));
				redirect($_SERVER["HTTP_REFERER"]);  
			}else{				
				$this->session->set_flashdata('error', validation_errors());
				redirect($_SERVER['HTTP_REFERER']);
			} 
		}else{
         $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
			$this->data['modal_js'] = $this->site->modal_js();
		 	$this->load->view($this->theme . 'application_setup/add_tag',$this->data);
		} 
	}
	
	public function edit_tag($id=null)
	{
		$post = $this->input->post();		
		if($post == true){
			$data = array( 
				'name'              =>htmlspecialchars($post['name']), 
				'native_name'	   =>htmlspecialchars($post['native_name'])

 			); 
			$result = $this->application_setup->save_tags($data,$id);
			if($result){
				$this->session->set_flashdata('message', lang("tag_edited"));
				redirect($_SERVER["HTTP_REFERER"]);  
			}else{				
				$this->session->set_flashdata('error', validation_errors());
				redirect($_SERVER['HTTP_REFERER']);
			} 
		}else{
			$this->data['id'] = $id;
			$this->data['result'] = $this->application_setup->getTagById($id);
         $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
			$this->data['modal_js'] = $this->site->modal_js();
		 	$this->load->view($this->theme . 'application_setup/edit_tag',$this->data);
		} 
	}
	
	public function delete_tag($id=null)
	{
		if (isset($id) || $id != null){        	
        	$delete = $this->application_setup->delete_tag($id);
        	if($delete){
        		$this->session->set_flashdata('message', lang("tag_deleted"));
            	redirect($_SERVER['HTTP_REFERER']);
        	}else{
            	redirect($_SERVER['HTTP_REFERER']);
            }
        } 		
	}

	
	public function reasons()
	{ 
		$this->data["result"] = "";
		$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('reason')));
		$meta = array('page_title' => lang('application_setup'), 'bc' => $bc);	
		$this->page_construct('application_setup/reasons',$meta,$this->data);
	}  

	public function getReasons($id = false)
	{		
		$this->load->library('datatables');	
		$delete = "<a href='#' class='delete-sm po' title='" . lang("delete_reason") . "' data-content=\"<p>"
			. lang('r_u_sure') . "</p><a class='btn btn-danger' href='" . site_url('application_setup/delete_reason/$1') . "'>"
			. lang('i_m_sure') . "</a><button class='btn'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i>"
			. lang('delete_reason') . "</a>";
		
		$action_link = '<div class="btn-group text-left"><button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">'.lang("actions").'<span class="caret"></span></button>
						<ul class="dropdown-menu pull-right" role="menu">					        																	
							<li><a data-backdrop="static" data-keyboard="false"  data-toggle="modal" data-target="#myModal" href="'.site_url('application_setup/edit_reason/$1').'" ><i class="fa fa-pencil-square-o"></i>'.lang('edit_reason').'</a></li>
							<li>'.$delete.'</li>
						</ul>
					</div>';

		 $this->datatables->select("fa_reasons.id,
									fa_reasons.id as ids,
									fa_reasons.name,
									fa_reasons.name_kh,
									fa_reasons.status 
									")
							->from("fa_reasons");
		 
		$this->datatables->add_column("Actions", $action_link, "fa_reasons.id");
		echo $this->datatables->generate();
	}

	public function add_reason() 
	{ 
		$post = $this->input->post();  
		if($post == true){
			$data = array(
				'name' 	 => htmlspecialchars($post['reason']),
				'name_kh'=> htmlspecialchars($post['reason_kh']),
				'status'	 =>	htmlspecialchars($post['status'])
			); 
			$result = $this->application_setup->save_reason($data);
			if($result){
				$this->session->set_flashdata('message', lang("reason_added"));
				redirect($_SERVER["HTTP_REFERER"]);  
			}else{				
				$this->session->set_flashdata('error', validation_errors());
				redirect($_SERVER['HTTP_REFERER']);
			}
		}else{
			
			$this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
			$this->data['modal_js'] = $this->site->modal_js();
			$this->load->view($this->theme . 'application_setup/add_reason',$this->data);
		} 
	}

	public function edit_reason($id=null) 
	{ 
		$post = $this->input->post();	 
		if($post == true)
		{
			$data = array(
				'name' 	 => htmlspecialchars($post['reason']),
				'name_kh'=> htmlspecialchars($post['reason_kh']),
				'status'	 =>	htmlspecialchars($post['status'])
			); 
			$result = $this->application_setup->save_reason($data,$id);
			if(result){
				$this->session->set_flashdata('message', lang("reason_edited"));
				redirect($_SERVER["HTTP_REFERER"]);  
			}else{				
				$this->session->set_flashdata('error', validation_errors());
				redirect($_SERVER['HTTP_REFERER']);
			}
			
		}else{
			$this->data['row'] = $this->application_setup->getReasonByid($id);
			$this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
			$this->data['id'] = $id;
			$this->data['modal_js'] = $this->site->modal_js();
			$this->load->view($this->theme . 'application_setup/edit_reason', $this->data);
		}
	}

	public function delete_reason($id=null)
	{
		if (isset($id) || $id != null){        	
			$delete = $this->application_setup->delete_reason($id);
			if($delete){
				$this->session->set_flashdata('message', lang("reason_deleted"));
				redirect($_SERVER['HTTP_REFERER']);
			}else{
				redirect($_SERVER['HTTP_REFERER']);
			}
		}
	}
  
  public function categories()
    {
       $this->data["result"] = "";
		$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('categories')));
		$meta = array('page_title' => lang('application_setup'), 'bc' => $bc);	
		$this->page_construct('application_setup/categories',$meta,$this->data); 
    }
    public function getCategory($id=false)
    {
      $this->load->library('datatables');	
		$delete = "<a href='#' class='delete-sm po' title='" . lang("delete_category") . "' data-content=\"<p>"
			. lang('r_u_sure') . "</p><a class='btn btn-danger' href='" . site_url('application_setup/delete_category/$1') . "'>"
			. lang('i_m_sure') . "</a><button class='btn'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i>"
			. lang('delete_category') . "</a>";
		
		$action_link = '<div class="btn-group text-left"><button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">'.lang("actions").'<span class="caret"></span></button>
						<ul class="dropdown-menu pull-right" role="menu">					        																	
							<li><a data-backdrop="static" data-keyboard="false"  data-toggle="modal" data-target="#myModal" href="'.site_url('application_setup/edit_category/$1').'" ><i class="fa fa-pencil-square-o"></i>'.lang('edit_category').'</a></li>
							<li>'.$delete.'</li>
						</ul>
					</div>'; 
		 $this->datatables->select("erp_categories.id,
									erp_categories.id as ids,
									erp_categories.code,
									erp_categories.name,
									erp_categories.description,
									erp_categories.status 
                                                                   ")
							->from("erp_categories");
		 
		$this->datatables->add_column("Actions", $action_link, "erp_categories.id");
		echo $this->datatables->generate();  
    }
	
	
    public function add_category()
    {
        $post = $this->input->post();  
		if($post == true){
			$data = array(
				'name' 	 => htmlspecialchars($post['name']),
				'code' 	 => htmlspecialchars($post['code']),
				'description'=> htmlspecialchars($post['description']),
				'status'	 =>	htmlspecialchars($post['status'])
			); 
			$result = $this->application_setup->save_category($data);
			if($result){
				$this->session->set_flashdata('message', lang("categorie_added"));
				redirect($_SERVER["HTTP_REFERER"]);  
			}else{				
				$this->session->set_flashdata('error', validation_errors());
				redirect($_SERVER['HTTP_REFERER']);
			}
		}else{
			
			$this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
			$this->data['modal_js'] = $this->site->modal_js();
			$this->load->view($this->theme . 'application_setup/add_category',$this->data);
		} 
    }
    public function edit_category($id=null)
    {
       $post = $this->input->post();	 
		if($post == true)
		{
			$data = array(
				'name' 	 => htmlspecialchars($post['name']),
				'code' 	 => htmlspecialchars($post['code']),
				'description'=> htmlspecialchars($post['description']),
				'status'	 =>	htmlspecialchars($post['status'])
			); 
			$result = $this->application_setup->save_category($data,$id);
			if(result){
				$this->session->set_flashdata('message', lang("categorie_edited"));
				redirect($_SERVER["HTTP_REFERER"]);  
			}else{				
				$this->session->set_flashdata('error', validation_errors());
				redirect($_SERVER['HTTP_REFERER']);
			}
			
		}else{
			$this->data['result'] = $this->application_setup->getCategoryById($id);
			$this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
			$this->data['id'] = $id;
			$this->data['modal_js'] = $this->site->modal_js();
			$this->load->view($this->theme . 'application_setup/edit_category', $this->data);
		} 
    }
   

	public function delete_category($id=null)
    {
        if (isset($id) || $id != null){        	
			$delete = $this->application_setup->delete_category($id);
			if($delete){
				$this->session->set_flashdata('message', lang("categorie_deleted"));
				redirect($_SERVER['HTTP_REFERER']);
			}else{
				redirect($_SERVER['HTTP_REFERER']);
			}
		}
    }

}
?>