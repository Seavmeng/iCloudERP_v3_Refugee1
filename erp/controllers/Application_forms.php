<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Application_forms extends MY_Controller
{
	
	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->library('pagination');
		$this->load->model('applications_model','applications');
		$this->load->model('application_setup_model');
		$this->load->model('sales_model');
		$this->load->model('global_model');
		$this->lang->load('applications', $this->Settings->language);
		$this->lang->load('application_forms', $this->Settings->language);
		$this->load->model('settings_model');
		$this->load->model('Site','site');
		$this->site->auditLogs();
	}

	public function index()
	{
		
	}

	/**
	* 
	* @param undefined $id
	* @param undefined $refugee_id
	* REFUGEE REQUEST CARD
	* @return
	*/
	
	public function refugee_card_form($id = false) 
	{ 
		$this->erp->checkPermissions('form', null, 'refugee_card'); 
		$this->data["request_card"] =  $this->applications->getRequestById($id);
		$this->data["result"] = $this->applications->getRSDApplicationById($this->data["request_card"]->application_id);		
		$this->data['member'] = $this->applications->getMemberId($this->data["request_card"]->member_id);
		//$this->erp->print_arrays($this->data["member"]);
		$this->data['reason'] = $this->applications->getReasonById($this->data["request_card"]->reasons);
		$this->data["decision"] = $this->applications->getRefugeeRecognitionByApplicationId($this->data["request_card"]->application_id); 		
		$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('refugee_card_form')));
		$meta = array('page_title' => lang('refugee_card_form'), 'bc' => $bc);	
		$this->page_construct('application_forms/refugee_card_form',$meta, $this->data);
	} 
	
	public function print_refugee_card($id = false)
	{		
		$this->erp->checkPermissions('print', null, 'refugee_card'); 
		$this->data["request_card"] =  $this->applications->getRequestById($id);
		$this->data["application"] =  $this->applications->getRSDApplicationById($this->data["request_card"]->application_id);
		$this->data["recognization"] = $this->applications->getRefugeeRecognitionByApplicationId($this->data["request_card"]->application_id); 
		$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('print_refugee_card')));
		$meta = array('page_title' => lang('print_refugee_card'), 'bc' => $bc);
		$this->data['modal_js'] = $this->site->modal_js();
		$this->load->view($this->theme . 'application_forms/print_refugee_card', $this->data); 
	}
    
	public function print_permanent_resident_card_refugee($id = false)
	{		
		$this->erp->checkPermissions('print', null, 'permanent_resident'); 
		$this->data["request_card"] =  $this->applications->getRequestById($id);
		$this->data["application"] =  $this->applications->getRSDApplicationById($this->data["request_card"]->application_id);
		$this->data["recognization"] = $this->applications->getRefugeeRecognitionByApplicationId($this->data["request_card"]->application_id); 
		$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('print_permanent_resident_card_refugee')));
		$meta = array('page_title' => lang('print_permanent_resident_card_refugee'), 'bc' => $bc);
		$this->data['modal_js'] = $this->site->modal_js();
		$this->load->view($this->theme . 'application_forms/print_permanent_resident_card_refugee', $this->data); 
	}
    
    public function print_card($id = false)
	{		
		$this->erp->checkPermissions('print', null, 'refugee_card'); 	
		$result = $this->applications->getRequestById($id); 
		$this->data['inv'] = $result; 
		$this->data["request_card"] =  $this->applications->getRequestById($id);
		$this->data["application"] =  $this->applications->getRSDApplicationById($this->data["request_card"]->application_id);		
		$this->data["recognization"] = $this->applications->getRefugeeRecognitionByApplicationId($this->data["request_card"]->application_id);		
		$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('print_card')));
		$meta = array('page_title' => lang('print_card'), 'bc' => $bc); 
		$this->load->view($this->theme . 'application_forms/print_card', $this->data); 
	} 

	public function print_card_blank($id = false)
	{	
		$this->erp->checkPermissions('print', null, 'refugee_card'); 
		$this->data["request_card"] =  $this->applications->getRequestById($id);
		$this->data["setting"] =  $this->applications->getSetting();
		$this->data["application"] =  $this->applications->getRSDApplicationPrintCardBlankById($this->data["request_card"]->id);		
		$this->data["recognization"] = $this->applications->getRefugeeRecognitionByApplicationId($this->data["request_card"]->application_id);		
		$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('print_card')));
		$meta = array('page_title' => lang('print_card'), 'bc' => $bc); 
		$this->load->view($this->theme . 'application_forms/print_card_blank', $this->data); 
	}   
	
	public function update_card($id = false)
	{
		$post = $this->input->post();
		if($post == true){
				$data = array(
					"prefix_name_officer" =>  $post['prefix_name_officer'] ,
					"name_officer" =>  $post['name_officer'] 
					);
				$result = $this->applications->updateSetting($data);
				if($result){ 
					redirect("application_forms/print_card_blank/".$id);
				}
		} 
	}
	
	public function add_refugee_card()
	{
		$post = $this->input->post();
		$p = $this->site->get_setting();
		if($post == true){
			$this->erp->checkPermissions('add', null, 'refugee_card'); 
			$village = $this->applications->addTags($post['village'],'village'); 
			
			$barcode_no_start = $p->refugee_card_no;
			$last_card = $this->applications->getLastRefugeeCard();
			$barcode_no=1;
			if($last_card->barcode_no){
				$barcode_no = $last_card->barcode_no + 1;
			}else {
				$barcode_no = $barcode_no_start;
			}
			
			 
			$data = array(
					"application_id" =>  $post['application_id'],					
					"occupation" =>  $post['occupation'],				
					"occupation_kh" =>  $post['occupation_kh'],				
					"contact_number" =>  $post['contact_number'],
					"country" =>  $post['country'],
					"province" => $post['province'],
					"district" => $post['district'],
					"commune" =>  $post['commune'],
					"address" =>  $post['address'],
					"address_kh" =>  $post['address_kh'],
					"reasons" =>  $post['reasons'],
					"village" =>  $village,
					"status" => 'pending',
					"created_by" => $this->session->userdata("user_id"),
					"created_date" => $this->erp->fld(date("d/m/Y H:i")),
					"repeat" => $post['repeat'],
					"member_id" => $post['member'],
					"barcode_no" => $barcode_no ,
					"recognized_no" => $post['recognized_no'],
					"recognized_date" => $this->erp->fld($post["recognized_date"])
				);
				
				$config1 = array(
					'upload_path' => 'assets/uploads',
					'allowed_types' => 'gif|jpg|png'
				); 
				
				$this->load->library('upload', $config1);
				if($this->upload->do_upload('photo')){  
					$file_name = $this->upload->data('file_name');  
					$data['photo'] = $file_name;
				}
				if($data['photo']==""){
					$data['photo'] = $post['photoHidden']; 	
				}
				if($this->applications->addRefugeeCardRequest($data)){
					$this->session->set_flashdata('message', lang("refugee_card_request_saved"));
					redirect("application_forms/refugee_card");
				}

		}
		$this->data['countries'] = $this->applications->getAllCountries();
		$this->data['provinces'] = $this->applications->getAllProvinces();
		$this->data['communes'] = $this->applications->getAllCommunces();
		$this->data['districts'] = $this->applications->getAllDistricts();
		$this->data['reasons'] = $this->applications->getAllReasons();
		$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('add_refugee_card')));
		$meta = array('page_title' => lang('add_refugee_card'), 'bc' => $bc);
		$this->data['modal_js'] = $this->site->modal_js();
		$this->load->view($this->theme . 'application_forms/add_refugee_card', $this->data); 
	}
			
	public function refugee_card()
	{				
		$this->erp->checkPermissions('index', true, 'refugee_card');
		$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('refugee_card')));
		$meta = array('page_title' => lang('refugee_card'), 'bc' => $bc);
		$this->page_construct('application_forms/refugee_card',$meta, $this->data);
	}

	public function getRefugeeCards()
	{
		$p = $this->site->get_setting();
		
		$this->load->library('datatables');
		$delete = "<a href='#' class='delete-refugee po' title='" . lang("delete_refugee_card") . "' data-content=\"<p>"
            . lang('r_u_sure') . "</p><a class='btn btn-danger' href='" . site_url('application_forms/delete_refugee_card/$1') . "'>"
            . lang('i_m_sure') . "</a><button class='btn'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i>"
            . lang('delete_refugee_card') . "</a>";
 
        $action_link = '<div class="btn-group text-left"><button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">'.lang("actions").'<span class="caret"></span></button>
					        <ul class="dropdown-menu pull-right" role="menu">
								 <li><a data-backdrop="static" data-keyboard="false"  data-toggle="modal" data-target="#myModal" href="'.site_url('application_forms/request_card_option/$1').'" ><i class="fa fa-newspaper-o"></i>'.lang('request_card_option').'</a></li>
					             <li><a class="remove_refugee_card_print" target="_blank" href="'.site_url('application_forms/print_card_blank/$1').'" ><i class="fa fa-newspaper-o"></i>'.lang('print_card').'</a></li>					             
					             <li class="hidden"><a target="_blank" class="remove_refugee_card_print" href="'.site_url('application_forms/print_card/$1').'" ><i class="fa fa-newspaper-o"></i>'.lang('print_card').'</a></li>					             
					             <li><a target="_blank" href="'.site_url('application_forms/refugee_card_form/$1').'" ><i class="fa fa-newspaper-o"></i>'.lang('refugee_card_form').'</a></li>					             
					             <li><a data-backdrop="static" data-keyboard="false"  data-toggle="modal" class="edit-refugee" data-target="#myModal" href="'.site_url('application_forms/edit_refugee_card/$1').'" ><i class="fa fa-newspaper-o"></i>'.lang('edit_refugee_card_request_form').'</a></li>   							
								<li><a  data-backdrop="static" data-keyboard="false"  data-toggle="modal" data-target="#myModal" href="'.site_url('application_forms/attach_refugee_card/$1').'" ><i class="fa fa-plus-circle"></i>'.lang('attachment').'</a></li>	
								<li>'.$delete.'</li>
					        </ul>
					    </div>';
	   $this->datatables->select("
				fa_refugee_card_request.id as id,
				CONCAT(case_prefix,case_no) as case_no,
				IF(erp_fa_refugee_card_request.member_id > 0,UPPER(erp_fa_family_members.firstname),UPPER(erp_fa_rsd_applications.firstname)) as firstname,
				IF(erp_fa_refugee_card_request.member_id > 0,UPPER(erp_fa_family_members.lastname),UPPER(erp_fa_rsd_applications.lastname)) as lastname,
				IF(erp_fa_refugee_card_request.member_id > 0,UPPER(erp_fa_family_members.dob),UPPER(erp_fa_rsd_applications.dob)) as dob,
				IF(erp_fa_refugee_card_request.member_id > 0,UPPER(erp_fa_family_members.gender),UPPER(erp_fa_rsd_applications.gender)) as gender,
				IF(erp_fa_refugee_card_request.member_id > 0,UPPER(erp_fa_family_members.nationality),UPPER(erp_fa_rsd_applications.nationality)) as nationality,
				IF(erp_fa_refugee_card_request.member_id > 0,UPPER(erp_fa_family_relationship.relationship_kh),'ម្ចាស់ករណី') as relationship_kh,
				contact_number,
				fa_refugee_card_request.created_date,
				fa_refugee_card_request.status,
				fa_refugee_card_request.expiry_date 			
				", false)
        ->from("fa_rsd_applications")
		->join("fa_refugee_card_request","application_id=fa_rsd_applications.id","right")
		->join("fa_family_members", "fa_family_members.id = fa_refugee_card_request.member_id", "left")
		->join("fa_family_relationship", "fa_family_relationship.id = fa_family_members.relationship", "left")
		->where("fa_refugee_card_request.status <>","deleted");
		if($this->erp->input->get('case_no')){
			$this->datatables->where('concat(erp_fa_rsd_applications.case_prefix, erp_fa_rsd_applications.case_no)=',$this->input->get('case_no'));
		} 
		if($this->erp->input->get('date_from') && $this->erp->input->get('date_to')){
			$this->datatables->where('fa_refugee_card_request.created_date >=',$this->erp->fld($this->erp->input->get('date_from')));
			$this->datatables->where('fa_refugee_card_request.created_date <=',$this->erp->fld($this->erp->input->get('date_to')));
		}
		if($this->input->get("v")=="expired"){
			$this->db->where("expiry_date BETWEEN NOW() AND DATE_ADD(NOW(), INTERVAL {$p->request_card_notification} DAY)");
		}

        $this->datatables->add_column("Actions", $action_link, "id");
        echo $this->datatables->generate();
	}
	
	public function edit_refugee_card($id = false)
	{ 
		$post = $this->input->post();
		if($post == true){ 
			$this->erp->checkPermissions('edit', null, 'refugee_card'); 
			$village = $this->applications->addTags($post['village'],'village');
			$data = array( 					
					"occupation"=>  $post['occupation'], 					
					"occupation_kh"=>  $post['occupation_kh'], 					
					"contact_number"=>  $post['contact_number'], 
					"country" =>  $post['country'],
					"province" => $post['province'],
					"district" => $post['district'],
					"commune" =>  $post['commune'],
					"address"=>  $post['address'],
					"address_kh" =>  $post['address_kh'],
					"reasons" =>  $post['reasons'],
					"village" =>  $village,
					"status"=> 'pending',					
					"repeat" => $post['repeat'],
					"recognized_no" => $post['recognized_no'],
					"recognized_date" => $this->erp->fld($post["recognized_date"])
				); 
				
			$config_file = array(
				'upload_path' => 'assets/uploads',
				'allowed_types' => 'gif|jpg|png' 
			); 
			$this->load->library('upload', $config_file); 
			$this->upload->initialize($config_file);  
			if($this->upload->do_upload('photo')){  
				$data['photo'] = $this->upload->data('file_name');  
			}
			$config_document =array(
				'upload_path' => 'assets/uploads/document/refugee_card',
				'allowed_types' => 'doc|pdf'
			); 
			$this->load->library('upload', $config_document);
			$this->upload->initialize($config_document); 
			if($this->upload->do_upload('attachment')){  
				$data['attachment'] = $this->upload->data('file_name'); 
			} 
			
			if($this->applications->updateRefugeeCardRequest($id, $data)){
				$this->session->set_flashdata('message', lang("refugee_card_request_edited"));
				redirect("application_forms/refugee_card");
			}
		}
		$this->data["request_card"] =  $this->applications->getRequestById($id);
		$this->data["result"] =  $this->applications->getRSDApplicationById($this->data["request_card"]->application_id);
		$this->data['members'] = $this->applications->getAllMembersByApplicationId($this->data["request_card"]->application_id);
		$this->data['countries'] = $this->applications->getAllCountries();
		$this->data['provinces'] = $this->applications->getAllProvinces();
		$this->data['communes'] = $this->applications->getAllCommunces();
		$this->data['districts'] = $this->applications->getAllDistricts();
		$this->data['reasons'] = $this->applications->getAllReasons();
		$this->data['id'] = $id;		
		$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('edit_refugee_card')));
		$meta = array('page_title' => lang('edit_refugee_card'), 'bc' => $bc);
		$this->data['modal_js'] = $this->site->modal_js();
		$this->load->view($this->theme . 'application_forms/edit_refugee_card', $this->data); 
	}
	
	public function delete_refugee_card($id = false)
	{
		if($id){
			$this->erp->checkPermissions('delete', null, 'refugee_card'); 			
			if($this->applications->deleteRefugeeCardRequest($id)){
				$this->session->set_flashdata('message', lang("refugee_cards_deleted"));
				redirect("application_forms/refugee_card");
			}
		}
	}
	
	public function getMemberRefugees()
	{
		$application_id = $this->input->get("id");
		if($application_id){
			$result = $this->applications->getRSDApplicationById($application_id);
			$members =  $this->applications->getFamilyMemberAccompanyingByCounseling($result->counseling_id);
			$applicant = $result->lastname_kh . " " . $result->firstname_kh.lang(" ( ម្ចាស់ករណី ) ");
			$members_ = array($applicant);
			foreach($members as $member){
				$members_[$member->id] = $member->lastname_kh . " " . $member->firstname_kh.lang(" ( សមាជិកក្នុងបន្ទុក ) ");
			}
			echo json_encode(form_dropdown('member', $members_, 0, ' class="form-control" id="member_id"'));
		}
		return false;
	}

	public function getMemberRefugeesAll()
	{
		$application_id = $this->input->get("id");
		if($application_id){
			$result = $this->applications->getRSDApplicationById($application_id);
			$members =  $this->applications->getFamilyMemberAll($result->counseling_id);
			$applicant = $result->lastname_kh . " " . $result->firstname_kh.lang(" ( ម្ចាស់ករណី ) ");
			$members_ = array($applicant);
			foreach($members as $member){
				$members_[$member->id] = $member->lastname_kh . " " . $member->firstname_kh.lang(" ( សមាជិកក្នុងបន្ទុក ) ");
			}
			echo json_encode(form_dropdown('member', $members_, 0, ' class="form-control" id="member_id"'));
		}
		return false;
	}
	
	public function getLastDataByApplication(){
		$application_id = $this->input->get("application_id");
		$member_id = $this->input->get("member_id");
		$result = "";
		if($member_id == 0 || $member_id == null){
			$result = $this->applications->getOwnerLastData($application_id);
		}
		elseif($member_id > 0){
			$result = $this->applications->getMemberLastData($member_id);
		}
		else{
			return false;
		}
		//$this->erp->print_arrays($result);
		$village_name = $this->applications->getTagsById($result->village);
		echo json_encode(array('result'=>$result,'vill'=>$village_name));
	}

	public function getLastDataByApplicationCardRequest(){
		$application_id = $this->input->get("application_id");
		$member_id = $this->input->get("member_id");
		$result = "";
		if($member_id == 0 || $member_id == null){
			$result = $this->applications->getOwnerLastDataCardRequest($application_id);
		}
		elseif($member_id > 0){
			$result = $this->applications->getMemberLastDataCardRequest($member_id);
		}
		else{
			return false;
		}
		//$this->erp->print_arrays($result);
		$village_name = $this->applications->getTagsById($result->village);
		echo json_encode(array('result'=>$result,'vill'=>$village_name));
	}

	public function getLastDataByApplicationTravelDocumentRequest(){
		$application_id = $this->input->get("application_id");
		$member_id = $this->input->get("member_id");
		$result = "";
		if($member_id == 0 || $member_id == null){
			$result = $this->applications->getOwnerLastDataTravelDocumentRequest($application_id);
		}
		elseif($member_id > 0){
			$result = $this->applications->getMemberLastDataTravelDocumentRequest($member_id);
		}
		else{
			return false;
		}
		//$this->erp->print_arrays($result);
		$village_name = $this->applications->getTagsById($result->village);
		echo json_encode(array('result'=>$result,'vill'=>$village_name));
	}

	public function getLastDataByApplicationCessationEliminationRefugee(){
		$application_id = $this->input->get("application_id");
		$member_id = $this->input->get("member_id");
		$result = "";
		if($member_id == 0 || $member_id == null){
			$result = $this->db->where("id", $application_id)->get('fa_rsd_applications')->row();
		}
		elseif($member_id > 0){
			$result = $this->db->where("id", $member_id)->get('fa_family_members')->row();
		}
		else{
			return false;
		}
		//$this->erp->print_arrays($result);
		$village_name = $this->applications->getTagsById($result->village);
		echo json_encode(array('result'=>$result,'vill'=>$village_name));
	}

	public function getLastDataByApplicationEliminateRefugee(){
		$application_id = $this->input->get("application_id");
		$member_id = $this->input->get("member_id");
		$result = "";
		if($member_id == 0 || $member_id == null){
			$result = $this->db->where("id", $application_id)->get('fa_rsd_applications')->row();
		}
		elseif($member_id > 0){
			$result = $this->db->where("id", $member_id)->get('fa_family_members')->row();
		}
		else{
			return false;
		}
		//$this->erp->print_arrays($result);
		$village_name = $this->applications->getTagsById($result->village);
		echo json_encode(array('result'=>$result,'vill'=>$village_name));
	}
	
	public function request_card_option($id = false)
	{ 
		$post = $this->input->post(); 
		$members = array(); 
		if($post['submit']){
			$this->erp->checkPermissions('approved', null, 'refugee_card'); 
			$data = array();
			if ($post['status'] == 'approved'){
				$data = array(			
						"approved_by" => $this->session->userdata("user_id"),
						"approved_date" => $this->erp->fld($post['date']),
						"expiry_date" => $this->erp->fld($post['expiry_date']),
						"date_sign"   => $this->erp->fld($post['date_sign']),
						"status" => $post['status']
				);
			} else {
				$data = array(			
						"rejected_by" => $this->session->userdata("user_id"),
						"rejected_date" => $this->erp->fld($post['date']),
						"expiry_date" => $this->erp->fld($post['expiry_date']),
						"date_sign"   => $this->erp->fld($post['date_sign']),
						"status" => $post['status']
				);
			}
			if($this->applications->updateRefugeeCardRequest($id, $data)){
				$this->session->set_flashdata('success', lang("request_card_option"));
				redirect("application_forms/refugee_card");
			}
			
		}else{
			$this->data['id'] = $id;
			$this->data["request_card"] =  $this->applications->getRequestById($id);
			$this->data['modal_js'] = $this->site->modal_js();		
			$this->load->view($this->theme . 'application_forms/request_card_option', $this->data);
		} 	
	}
	
	
	public function attach_refugee_card($id = false) 
	{ 
		$result = $this->applications->getRequestById($id);
		$this->global_model->attach_forms('fa_refugee_card_request', $id, 'refugee_card');
		$this->data['action_method'] = 'attach_refugee_card';
		$this->data['delete_url'] = 'delete_attach_refugee_card';
		$this->data['view_folder'] = 'refugee_card';
		$this->data['contorler'] = $this->uri->segment(1);
		$this->data['id'] = $id; 
		$this->data['result'] = $result; 
		$this->data['modal_js'] = $this->site->modal_js();		
		$this->load->view($this->theme . 'global/view_attach_forms', $this->data);	
	}
	 
	public function delete_attach_refugee_card($id = false, $name = null, $img = null)
	{
		$this->erp->checkPermissions('delete', true, 'refugee_card');
		$this->global_model->delete_attach_forms('fa_refugee_card_request', $id, $name, $img);
	}   
	  
	/**
	* 
	* Resident CARD REFUGEE
	* @return
	*/
	
	public function permanent_resident_card_refugee()
	{
		$this->data["result"] = "";
		$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('permanent_resident_card_refugee')));
		$meta = array('page_title' => lang('refugee_card'), 'bc' => $bc);
		$this->page_construct('application_forms/permanent_resident_card_refugee',$meta, $this->data);
	}
	
	public function getPermanentResidentCardRefugees($id=false)
	{
		$p = $this->site->get_setting();
		
		$this->load->library('datatables');
		$delete = "<a href='#' class='delete-permanent_resident po' title='" . lang("delete_permanent_resident_card_refugee") . "' data-content=\"<p>"
            . lang('r_u_sure') . "</p><a class='btn btn-danger' href='" . site_url('application_forms/delete_permanent_resident_card_refugee/$1') . "'>"
            . lang('i_m_sure') . "</a><button class='btn'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i>"
            . lang('delete_permanent_resident_card_refugee') . "</a>";
		
         $action_link = '<div class="btn-group text-left"><button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">'.lang("actions").'<span class="caret"></span></button>
					        <ul class="dropdown-menu pull-right" role="menu">
					           <li><a data-backdrop="static" data-keyboard="false"  data-toggle="modal" data-target="#myModal" href="'.site_url('application_forms/permanent_resident_card_option/$1').'" ><i class="fa fa-newspaper-o"></i>'.lang('permanent_resident_card_option').'</a></li>
							   <li><a target="_blank" href="'.site_url('application_forms/permanent_resident_card_refugee_form/$1').'" ><i class="fa fa-newspaper-o"></i>'.lang('permanent_resident_card_refugee_form').'</a></li> 					         
					           <li><a  href="'.site_url('application_forms/print_permanent_resident_card_refugee/$1').'" ><i class="fa fa-newspaper-o"></i>'.lang('print_permanent_resident_card_refugee').'</a></li>
							   <li><a data-backdrop="static" data-keyboard="false"  data-toggle="modal" data-target="#myModal" href="'.site_url('application_forms/edit_permanent_resident_card_refugee/$1').'" ><i class="fa fa-newspaper-o"></i>'.lang('edit_permanent_resident_card_refugee').'</a></li> 								
								<li><a  data-backdrop="static" data-keyboard="false"  data-toggle="modal" data-target="#myModal" href="'.site_url('application_forms/attach_permanent_resident_card/$1').'" ><i class="fa fa-plus-circle"></i>'.lang('attachment').'</a></li>	
								<li>'.$delete.'</li>
					        </ul>
					    </div>';
						
		   $this->datatables->select("
					fa_refugee_permanent_card_request.id as id,
					CONCAT(erp_fa_rsd_applications.case_prefix,case_no) as case_no,
					IF(erp_fa_refugee_permanent_card_request.member_id > 0,UPPER(erp_fa_family_members.firstname),UPPER(erp_fa_rsd_applications.firstname)) as firstname,
					IF(erp_fa_refugee_permanent_card_request.member_id > 0,UPPER(erp_fa_family_members.lastname),UPPER(erp_fa_rsd_applications.lastname) ) as lastname,
					IF(erp_fa_refugee_permanent_card_request.member_id > 0,UPPER(erp_fa_family_members.dob),UPPER(erp_fa_rsd_applications.dob) ) as dob,
					IF(erp_fa_refugee_permanent_card_request.member_id > 0,UPPER(erp_fa_family_members.gender),UPPER(erp_fa_rsd_applications.gender) ) as gender,	
					IF(erp_fa_refugee_permanent_card_request.member_id > 0,UPPER(erp_fa_family_members.nationality),UPPER(erp_fa_rsd_applications.nationality) ) as nationality,			
					IF(erp_fa_refugee_permanent_card_request.member_id > 0,fa_family_relationship.relationship_kh,'ម្ចាស់ករណី' ) as relationship_kh,	
					fa_refugee_permanent_card_request.created_date,
					fa_refugee_permanent_card_request.status,
					fa_refugee_permanent_card_request.expiry_date
					", false)
            ->from("fa_rsd_applications")
			->join("fa_refugee_permanent_card_request","application_id=fa_rsd_applications.id","right")
			->join("fa_family_members","fa_family_members.id=fa_refugee_permanent_card_request.member_id","left")
			->join("fa_family_relationship", "fa_family_relationship.id = fa_family_members.relationship", "left")
		    ->where("fa_refugee_permanent_card_request.status <> 'deleted' ");
			if($this->erp->input->get('case_no')){
				$this->datatables->where('concat(erp_fa_rsd_applications.case_prefix, erp_fa_rsd_applications.case_no)=',$this->input->get('case_no'));
			} 
			if($this->erp->input->get('date_from') && $this->erp->input->get('date_to')){
				$this->datatables->where('date(erp_fa_refugee_permanent_card_request.created_date) >=',$this->erp->fld($this->erp->input->get('date_from')));
				$this->datatables->where('date(erp_fa_refugee_permanent_card_request.created_date) <=',$this->erp->fld($this->erp->input->get('date_to')));
			}

		if($this->input->get("v")=="expired"){
			$this->db->where("expiry_date BETWEEN NOW() AND DATE_ADD(NOW(), INTERVAL {$p->resident_card_notification} DAY)");
		}
        $this->datatables->add_column("Actions", $action_link, "id");
        echo $this->datatables->generate();
	}
	
	public function add_permanent_resident_card_refugee($id=false)
	{ 
		$post = $this->input->post();
		
		if($post == true){
			$this->erp->checkPermissions('add', null, 'permanent_resident');
			$village = $this->applications->addTags($post['village'],'village');
			$data = array(
					"application_id" => $post['application_id'],
					"created_date" => $this->erp->fld(date("d/m/Y H:i")),
					"created_date" => $this->erp->fld(date("d/m/Y H:i")),
					"contact_number" => $post['contact_number'],
					"created_by"=> $this->session->userdata("user_id"),
					"occupation" => $post['occupation'],
					"occupation_kh" => $post['occupation_kh'],
					"address" => $post["address"],
					"address_kh" => $post["address_kh"],
					"village"   => $village,
					"country" =>  $post['country'],
					"province" => $post['province'],
					"district" => $post['district'],
					"commune" =>  $post['commune'],
					"reasons" =>  $post['reasons'],
					"status"=> 'pending',
					"recognized_date" => $this->erp->fld($post['recognized_date']),
					"repeat" => $post['repeat'],
					"member_id" => $post['member'],
				);
			$config_file = array(
				'upload_path' => 'assets/uploads',
				'allowed_types' => 'gif|jpg|png'
			);
			$this->load->library('upload', $config_file);
			
			if($this->upload->do_upload('photo')){  
				$file_name = $this->upload->data('file_name'); 
				$data['photo'] = $file_name; 
			}
			if($data['photo']=="") {
				$data['photo'] = $post['photoHidden']; 
			}
			if($this->applications->addRefugeePermanent($data)){
				
				$this->session->set_flashdata('message', lang("permanent_resident_card_refugee_saved"));
				redirect("application_forms/permanent_resident_card_refugee");
			}
		}
	
		$this->data['countries'] = $this->applications->getAllCountries();
		$this->data['provinces'] = $this->applications->getAllProvinces();
		$this->data['communes'] = $this->applications->getAllCommunces();
		$this->data['districts'] = $this->applications->getAllDistricts();
		$this->data['reasons'] = $this->applications->getAllReasons();
		$this->data['id'] = $id; 
		$this->data['modal_js'] = $this->site->modal_js();
		$this->load->view($this->theme . 'application_forms/add_permanent_resident_card_refugee', $this->data);
	} 

	public function edit_permanent_resident_card_refugee($id = false)
	{		
		$post = $this->input->post(); 
		if($post == true ) {
			$this->erp->checkPermissions('edit', null, 'permanent_resident');
			$village = $this->applications->addTags($post['village'],'village');
			$data = array(
					"contact_number" => $post['contact_number'],
					"occupation" => $post['occupation'],
					"occupation_kh" => $post['occupation_kh'],
					"country" =>  $post['country'],
					"province" => $post['province'],
					"district" => $post['district'],
					"commune" =>  $post['commune'],
					"status"=> 'pending',
					"address" => $post["address"],
					"address_kh" => $post["address_kh"],
					"reasons" =>  $post['reasons'],
					"village"    => $village,
					"expiry_date" => $this->erp->fld($post['expiry_date']),
					"repeat" => $post['repeat'],
					"recognized_date" => $this->erp->fld($post['recognized_date'])
				);
			
			$config_file = array(
				'upload_path' => 'assets/uploads',
				'allowed_types' => 'gif|jpg|png'
			); 
			$this->load->library('upload', $config_file); 
			$this->upload->initialize($config_file); 
			if($this->upload->do_upload('photo')){  
				$data['photo'] = $this->upload->data('file_name');  
			}  
			
			$config_document =array(
				'upload_path' => 'assets/uploads/document/permanent_request_card',
				'allowed_types' => 'doc|pdf'
			); 
			$this->load->library('upload', $config_document);
			$this->upload->initialize($config_document); 
			if($this->upload->do_upload('attachment')){  
				$data['attachment'] = $this->upload->data('file_name'); 
			} 
			
			if($this->applications->updateRefugeePermanent($id,$data)){
				$this->session->set_flashdata('message', lang("permanent_resident_card_of_refugee_request_updated"));
				redirect("application_forms/permanent_resident_card_refugee");
			}
			
		}
		$this->data['request_card'] = $this->applications->getPermanentById($id); 
		$this->data["result"] = $this->applications->getApplicationById($this->data['request_card']->application_id);
		$this->data['members'] = $this->applications->getAllMembersByApplicationId($this->data["request_card"]->application_id);
		$this->data['countries'] = $this->applications->getAllCountries();
		$this->data['provinces'] = $this->applications->getAllProvinces();
		$this->data['communes'] = $this->applications->getAllCommunces();
		$this->data['districts'] = $this->applications->getAllDistricts();
		$this->data['users'] = $this->applications->getUsers();
		$this->data['reasons'] = $this->applications->getAllReasons();
		$this->data['modal_js'] = $this->site->modal_js();
		$this->data['id'] = $id;
		$this->load->view($this->theme . 'application_forms/edit_permanent_resident_card_refugee', $this->data);
	}
	
	public function delete_permanent_resident_card_refugee($id = false)
	{
		if($id){	
			$this->erp->checkPermissions('delete', null, 'permanent_resident');
			if($this->applications->deleteRefugeePermanent($id)){
				$this->session->set_flashdata('message', lang("permanent_request_card_refugee_deleted"));
				redirect("application_forms/permanent_resident_card_refugee");
			}
		}
	}
	
	public function permanent_resident_card_refugee_form($id=false)
	{  
		$this->erp->checkPermissions('form', null, 'permanent_resident'); 
		// $this->erp->checkPermissions('edit', null, 'permanent_resident');
		// $this->erp->checkPermissions('add', null, 'permanent_resident');
		// $this->erp->checkPermissions('delete', null, 'permanent_resident');
		$this->data["permanent_resident"] =  $this->applications->getPermanentById($id); 		
		$this->data["result"] =  $this->applications->getRSDApplicationById($this->data["permanent_resident"]->application_id);   		
		$this->data["decision"] = $this->applications->getRefugeeRecognitionByApplicationId($this->data["permanent_resident"]->application_id);		
		$this->data['reason'] = $this->applications->getReasonById($this->data["permanent_resident"]->reasons);
		$this->data['member'] = $this->applications->getMemberId($this->data["permanent_resident"]->member_id);
		
		$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('permanent_resident_card_refugee_form')));
		$meta = array('page_title' => lang('permanent_resident_card_refugee_form'), 'bc' => $bc);	
		$this->page_construct('application_forms/permanent_resident_card_refugee_form',$meta, $this->data);
	}
	
	public function permanent_resident_card_option($id = false)
	{ 
		$post = $this->input->post(); 
		$members = array(); 
		if($post['submit']){
			$this->erp->checkPermissions('approved', null, 'permanent_resident'); 
			$data = array();
			
			if ($post['status'] == 'approved'){
				$data = array(			
						"approved_by" => $this->session->userdata("user_id"),
						"approved_date" => $this->erp->fld($post['date']),
						"expiry_date" => $this->erp->fld($post['expiry_date']),
						"status" => $post['status']
				);
			} else {
				$data = array(			
						"rejected_by" => $this->session->userdata("user_id"),
						"rejected_date" => $this->erp->fld($post['date']),
						"expiry_date" => $this->erp->fld($post['expiry_date']),
						"status" => $post['status']
				);
			}
			if($this->applications->updateRefugeePermanent($id, $data)){
				$this->session->set_flashdata('success', lang("permanent_resident_card_option"));
				redirect("application_forms/permanent_resident_card_refugee");
			}
			
		}else{
			$this->data['id'] = $id;
			$this->data["request_card"] =  $this->applications->getPermanentById($id);
			$this->data['modal_js'] = $this->site->modal_js();		
			$this->load->view($this->theme . 'application_forms/permanent_resident_card_option', $this->data);
		} 	
	}

	public function attach_permanent_resident_card($id = false) 
	{ 
		$result = $this->applications->getPermanentById($id);
		// permanent_resident
		$this->global_model->attach_forms('fa_refugee_permanent_card_request' ,$id, 'permanent_resident');
		$this->data['id'] = $id; 
		$this->data['result'] = $result; 
		$this->data['action_method'] = 'attach_permanent_resident_card';
		$this->data['delete_url'] = 'delete_attach_permanent_resident_card';
		$this->data['view_folder'] = 'permanent_resident';
		$this->data['contorler'] = $this->uri->segment(1);
		$this->data['modal_js'] = $this->site->modal_js();		
		$this->load->view($this->theme . 'global/view_attach_forms', $this->data);		
	 				
	}   
	public function delete_attach_permanent_resident_card($id = false, $name = null, $img = null)
	{
		$this->global_model->delete_attach_forms('fa_refugee_permanent_card_request', $id, $name, $img);
	}  
	
	/**
	* 
	* @param undefined $id
	* @param undefined $travel_id
	* TRAVAEL DOCUMENT REFUGEE
	* @return
	*/
	
	public function travel_document_option($id = false)
	{ 
		$post = $this->input->post(); 
		$members = array(); 
		if($post['submit']){
			$this->erp->checkPermissions('approved', null, 'travel_document_refugee');
			$data = array();
			if ($post['status'] == 'approved'){
				$data = array(			
						"approved_by" => $this->session->userdata("user_id"),
						"approved_date" => $this->erp->fld($post['date']),
						"status" => $post['status']
				);
			} else {
				$data = array(			
						"rejected_by" => $this->session->userdata("user_id"),
						"rejected_date" => $this->erp->fld($post['date']),
						"status" => $post['status']
				);
			}
			if($this->applications->updateRefugeeTravelDocument($id, $data)){
				$this->session->set_flashdata('success', lang("travel_document_option"));
				redirect("application_forms/travel_document_refugee");
			}
			
		}else{
			$this->data['id'] = $id;
			$this->data["travel"] =  $this->applications->getTravelDocumentId($id);
			$this->data['modal_js'] = $this->site->modal_js();		
			$this->load->view($this->theme . 'application_forms/travel_document_option', $this->data);
		} 	
	}
	
	public function travel_document_of_refugee_request_form($id=false)
	{ 
		$this->erp->checkPermissions('form', null, 'travel_document_refugee');
		$this->data["travel_doc"] = $this->applications->getTravelDocumentId($id);
		$this->data["decision"] = $this->applications->getRefugeeRecognitionByApplicationId($this->data["travel_doc"]->application_id);
		$this->data["result"] =  $this->applications->getRSDApplicationById($this->data["travel_doc"]->application_id);   
		$this->data['member'] = $this->applications->getMemberId($this->data["travel_doc"]->member_id);
		$this->data["occupation"] =  $this->applications->getOccupationApplication($this->data["travel_doc"]->application_id); 
		$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('travel_document_of_refugee_request_form')));
		$meta = array('page_title' => lang('travel_document_of_refugee_request_form'), 'bc' => $bc);	
		$this->page_construct('application_forms/travel_document_of_refugee_request_form',$meta, $this->data);
	}
		
	public function travel_document_refugee()
	{
		$this->erp->checkPermissions('index', null, 'travel_document_refugee');
		// $this->erp->checkPermissions('add', null, 'travel_document_refugee');
		// $this->erp->checkPermissions('edit', null, 'travel_document_refugee');
		// $this->erp->checkPermissions('delete', null, 'travel_document_refugee');
		// $this->erp->checkPermissions('export', null, 'travel_document_refugee');
		$this->data["result"] = "";
		$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('travel_document_refugee')));
		$meta = array('page_title' => lang('travel_document_refugee'), 'bc' => $bc);
		$this->page_construct('application_forms/travel_document_refugee',$meta, $this->data);
	}
	
	public function getTravelDocumentRefugees($id=false) 
	{
		$this->load->library('datatables');
		$delete = "<a href='#' class='delete-travel_document po' title='" . lang("delete_travel_document_refugee") . "' data-content=\"<p>"
            . lang('r_u_sure') . "</p><a class='btn btn-danger' href='" . site_url('application_forms/delete_travel_document_refugee/$1') . "'>"
            . lang('i_m_sure') . "</a><button class='btn'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i>"
            . lang('delete_travel_document_refugee') . "</a>";
		
        $action_link = '<div class="btn-group text-left"><button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">'.lang("actions").'<span class="caret"></span></button>
					        <ul class="dropdown-menu pull-right" role="menu">
					             <li><a data-backdrop="static" data-keyboard="false"  data-toggle="modal" data-target="#myModal" href="'.site_url('application_forms/travel_document_option/$1').'" ><i class="fa fa-newspaper-o"></i>'.lang('travel_document_option').'</a></li>
								 <li><a target="_blank" href="'.site_url('application_forms/travel_document_of_refugee_request_form/$1').'" ><i class="fa fa-newspaper-o"></i>'.lang('travel_document_of_refugee_request_form').'</a></li>   
					             <li><a href="'.site_url('application_forms/edit_travel_document_refugee/$1').'" ><i class="fa fa-newspaper-o"></i>'.lang('edit_travel_document_refugee').'</a></li>   								
								 <li><a  data-backdrop="static" data-keyboard="false"  data-toggle="modal" data-target="#myModal" href="'.site_url('application_forms/attach_travel_document_refugee/$1').'" ><i class="fa fa-plus-circle"></i>'.lang('attachment').'</a></li>	
								 <li>'.$delete.'</li>
					        </ul>
					    </div>';
		$this->datatables->select("
				fa_refugee_travel_document_request.id as id,
				CONCAT(erp_fa_rsd_applications.case_prefix,case_no) as case_no,
				IF(erp_fa_refugee_travel_document_request.member_id > 0,UPPER(erp_fa_family_members.firstname),UPPER(erp_fa_rsd_applications	.firstname) ) as firstname,
				IF(erp_fa_refugee_travel_document_request.member_id > 0,UPPER(erp_fa_family_members.lastname),UPPER(erp_fa_rsd_applications	.lastname) ) as lastname,
				IF(erp_fa_refugee_travel_document_request.member_id > 0,UPPER(erp_fa_family_members.dob),UPPER(erp_fa_rsd_applications	.dob) ) as dob,
				IF(erp_fa_refugee_travel_document_request.member_id > 0,UPPER(erp_fa_family_members.gender),UPPER(erp_fa_rsd_applications	.gender) ) as gender,
				IF(erp_fa_refugee_travel_document_request.member_id > 0,UPPER(erp_fa_family_members.nationality),UPPER(erp_fa_rsd_applications	.nationality) ) as nationality,

                IF(erp_fa_refugee_travel_document_request.member_id > 0,fa_family_relationship.relationship_kh,'ម្ចាស់ករណី' ) as relationship_kh,
				fa_refugee_travel_document_request.emergencies_contact, 
				fa_refugee_travel_document_request.created_date,
				fa_refugee_travel_document_request.status
				", false)
		->from("fa_rsd_applications")
		->join("fa_refugee_travel_document_request","application_id=fa_rsd_applications.id","right")
	    ->join("fa_family_members", "fa_family_members.id = fa_refugee_travel_document_request.member_id", "left")
	    ->join("fa_family_relationship", "fa_family_relationship.id = fa_family_members.relationship", "left")
		->where("fa_refugee_travel_document_request.status <> 'deleted' ");
        if($this->erp->input->get('case_no')){
			$this->datatables->where('concat(erp_fa_rsd_applications.case_prefix, erp_fa_rsd_applications.case_no)=',$this->input->get('case_no'));
		} 
		if($this->erp->input->get('date_from') && $this->erp->input->get('date_to')){
			$this->datatables->where('date(erp_fa_refugee_travel_document_request.created_date) >=',$this->erp->fld($this->erp->input->get('date_from')));
			$this->datatables->where('date(erp_fa_refugee_travel_document_request.created_date) <=',$this->erp->fld($this->erp->input->get('date_to')));
		}
		$this->datatables->add_column("Actions", $action_link, "id");
        echo $this->datatables->generate();
	}
	
	public function add_travel_document_refugee($id=false) 
	{ 
		$post = $this->input->post(); 
			if($post == true ){ 
				$this->erp->checkPermissions('add', null, 'travel_document_refugee');
				$config = array(
					array(
						 'field'   => 'case_no',
						 'label'   => lang("case_no"),
						 'rules'   => 'required'
					),array(
						 'field'   => 'height',
						 'label'   => lang("height"),
						 'rules'   => 'required'
					), array(
						 'field'   => 'complexion',
						 'label'   => lang("complexion"),
						 'rules'   => 'required'
					), array(
						 'field'   => 'shape_of_face',
						 'label'   => lang("shape_of_face"),
						 'rules'   => 'required'
					), array(
						 'field'   => 'color_of_eyes',
						 'label'   => lang("color_of_eyes"),
						 'rules'   => 'required'
					), array(
						 'field'   => 'distinguishing_marks',
						 'label'   => lang("distinguishing_marks"),
						 'rules'   => 'required'
					), array(
						 'field'   => 'hair',
						 'label'   => lang("hair"),
						 'rules'   => 'required'
					), array(
						 'field'   => 'to_country',
						 'label'   => lang("to_country"),
						 'rules'   => 'required'
					), array(
						 'field'   => 'travel_for',
						 'label'   => lang("travel_for"),
						 'rules'   => 'required'
					),array(
						 'field'   => 'emergencies_contact',
						 'label'   => lang("emergencies_contact"),
						 'rules'   => 'required'
					),array(
						 'field'   => 'emergencies_address',
						 'label'   => lang("emergencies_address"),
						 'rules'   => 'required'
					) ,array(
						 'field'   => 'phone',
						 'label'   => lang("phone"),
						 'rules'   => 'required'
					)
				);
				$this->form_validation->set_rules($config);
				if ($this->form_validation->run() == true) {
					
					$village = $this->applications->addTags($post['village'],'village');
					$data = array(
						"application_id" => $post['application_id'],
						"height" => $post['height'],						
						"complexion" => $post['complexion'],
						"shape_of_face" => $post['shape_of_face'],
						"color_of_eyes" => $post['color_of_eyes'],
						"hair" => $post['hair'],						
						"to_country_kh" => $post['to_country_kh'],
						"to_country" => $post['to_country'],
						"travel_for_kh" => $post['travel_for_kh'],
						"travel_for" => $post['travel_for'],
						"emergencies_contact" => $post['emergencies_contact'],
						"emergencies_address" => $post['emergencies_address'],
						"phone" => $post['phone'],						
						"distinguishing_marks" => $post['distinguishing_marks'], 
						"occupation" => $post['occupation'], 
						"occupation_kh" => $post['occupation_kh'], 
						"country" =>  $post['country'],
						"province" => $post['province'],
						"district" => $post['district'],
						"commune" =>  $post['commune'],
						"address" => $post['address'],
						"address_kh" =>  $post['address_kh'],
						"village" =>  $village,
						"status" => 'pending',
						"member_id" => $post['member'],
						"created_by" => $this->session->userdata("user_id"),
						"created_date" => $this->erp->fld(date("d/m/Y H:i")),
						
					); 
					$config_file = array(
						'upload_path' => 'assets/uploads',
						'allowed_types' => 'gif|jpg|png',
					); 
					
					$this->load->library('upload', $config_file);
					if($this->upload->do_upload('photo')){  
						$file_name = $this->upload->data('file_name');
						$data['photo'] = $file_name;
					}  
						
					if($data['photo']==""){
						$data['photo'] = $post['photoHidden']; 	
					}

					if($this->applications->addRefugeeTravelDocument($data)){
						$this->session->set_flashdata('message', lang("travel_document_refugee_saved"));
						redirect("application_forms/travel_document_refugee");
					}else{
						$this->session->set_flashdata('message', lang("travel_document_refugee_cannot_saved"));
						redirect("application_forms/travel_document_refugee");
					}
				}
			}
		$this->data['countries'] = $this->applications->getAllCountries();
		$this->data['provinces'] = $this->applications->getAllProvinces();
		$this->data['communes'] = $this->applications->getAllCommunces();
		$this->data['districts'] = $this->applications->getAllDistricts();
		$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('add_travel_document_refugee')));
		$this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
		$meta = array('page_title' => lang('add_travel_document_refugee'), 'bc' => $bc);
		$this->page_construct('application_forms/add_travel_document_refugee',$meta, $this->data);
	}
	
	public function edit_travel_document_refugee($id=false) 
	{  
		$post = $this->input->post(); 
			if($post == true ){
				$this->erp->checkPermissions('edit', null, 'travel_document_refugee');
				$config = array(
					array(
						 'field'   => 'height',
						 'label'   => lang("height"),
						 'rules'   => 'required'
					), array(
						 'field'   => 'complexion',
						 'label'   => lang("complexion"),
						 'rules'   => 'required'
					), array(
						 'field'   => 'shape_of_face',
						 'label'   => lang("shape_of_face"),
						 'rules'   => 'required'
					), array(
						 'field'   => 'color_of_eyes',
						 'label'   => lang("color_of_eyes"),
						 'rules'   => 'required'
					), array(
						 'field'   => 'distinguishing_marks',
						 'label'   => lang("distinguishing_marks"),
						 'rules'   => 'required'
					), array(
						 'field'   => 'hair',
						 'label'   => lang("hair"),
						 'rules'   => 'required'
					),array(
						 'field'   => 'to_country',
						 'label'   => lang("to_country"),
						 'rules'   => 'required'
					), array(
						 'field'   => 'travel_for',
						 'label'   => lang("travel_for"),
						 'rules'   => 'required'
					),array(
						 'field'   => 'emergencies_contact',
						 'label'   => lang("emergencies_contact"),
						 'rules'   => 'required'
					),array(
						 'field'   => 'emergencies_address',
						 'label'   => lang("emergencies_address"),
						 'rules'   => 'required'
					) ,array(
						 'field'   => 'phone',
						 'label'   => lang("phone"),
						 'rules'   => 'required'
					)
				);
				$this->form_validation->set_rules($config);
				
				if ($this->form_validation->run() == true) { 
					
					$village = $this->applications->addTags($post['village'],'village');
					
					$data = array( 
						"height" => $post['height'],						
						"complexion" => $post['complexion'],
						"shape_of_face" => $post['shape_of_face'],
						"color_of_eyes" => $post['color_of_eyes'],
						"hair" => $post['hair'],						
						"to_country_kh" => $post['to_country_kh'],
						"to_country" => $post['to_country'],
						"travel_for_kh" => $post['travel_for_kh'],
						"travel_for" => $post['travel_for'],
						"emergencies_contact" => $post['emergencies_contact'],
						"emergencies_address" => $post['emergencies_address'],
						"phone" => $post['phone'],						
						"distinguishing_marks" => $post['distinguishing_marks'], 
						"occupation" => $post['occupation'], 
						"occupation_kh" => $post['occupation_kh'], 
						"country" =>  $post['country'],
						"province" => $post['province'],
						"district" => $post['district'],
						"commune" =>  $post['commune'],
						"address" => $post['address'],
						"address_kh" =>  $post['address_kh'],
						"village" =>  $village,
						"status" => 'pending',
						"created_by" => $this->session->userdata("user_id"),
						"created_date" => $this->erp->fld(date("d/m/Y")),
					); 
					 					
					
					$config1 = array(
						'upload_path' => 'assets/uploads',
						'allowed_types' => 'gif|jpg|png', 
					); 
					$this->load->library('upload', $config1);
					$this->upload->initialize($config1); 
					if($this->upload->do_upload('photo')){  
						$data['photo'] = $this->upload->data('file_name');
					}   
					/*=== config document*/
					$config2 =array(
						'upload_path' => 'assets/uploads/document/travel_document',
						'allowed_types' => 'doc|pdf'
					); 
					$this->load->library('upload', $config2);
					$this->upload->initialize($config2); 
					if($this->upload->do_upload('attachment')){  
						$data['attachment'] = $this->upload->data('file_name'); 
					}    
					
					if($this->applications->updateRefugeeTravelDocument($id, $data)){
						$this->session->set_flashdata('message', lang("travel_document_refugee_edited"));
						redirect($_SERVER['HTTP_REFERER']);
					}
				}
			}
		
		$this->data['travel_id'] = $id;
		$this->data['countries'] = $this->applications->getAllCountries();
		$this->data['provinces'] = $this->applications->getAllProvinces();
		$this->data['communes'] = $this->applications->getAllCommunces();
		$this->data['districts'] = $this->applications->getAllDistricts();
		$this->data["result"] = $this->applications->getTravelDocumentId($id);
		$this->data["application"] = $this->applications->getApplicationById($this->data['result']->application_id);
		$this->data['members'] = $this->applications->getAllMembersByApplicationId($this->data["result"]->application_id);
		$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('edit_travel_document_refugee')));
		$this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
		$meta = array('page_title' => lang('edit_travel_document_refugee'), 'bc' => $bc);
		$this->page_construct('application_forms/edit_travel_document_refugee',$meta, $this->data);
	}

	public function delete_travel_document_refugee($id = false)
	{
		if($id){
			$this->erp->checkPermissions('delete', null, 'travel_document_refugee');			
			if($this->applications->deleteRefugeeTravelDocument($id)){
				$this->session->set_flashdata('message', lang("travel_document_refugee_deleted"));
				redirect($_SERVER['HTTP_REFERER']);
			}
		}
	}
	 
	public function attach_travel_document_refugee($id = false) 
	{ 
		$result = $this->applications->getTravelDocumentId($id);
		$this->erp->checkPermissions('delete', null, 'travel_document_refugee');
		$this->global_model->attach_forms('fa_refugee_travel_document_request', $id, 'travel_document');
		$this->data['action_method'] = 'attach_travel_document_refugee';
		$this->data['delete_url'] = 'delete_attach_travel_document_refugee';
		$this->data['view_folder'] = 'travel_document';
		$this->data['contorler'] = $this->uri->segment(1);
		$this->data['id'] = $id; 
		$this->data['result'] = $result; 
		$this->data['modal_js'] = $this->site->modal_js();		
		$this->load->view($this->theme . 'global/view_attach_forms', $this->data);		
	}    
	
	public function delete_attach_travel_document_refugee($id = false, $name = null, $img = null)
	{
		//$this->erp->checkPermissions('delete', null, 'travel_document_refugee');
		$this->global_model->delete_attach_forms('fa_refugee_travel_document_request', $id, $name, $img);
	}   
	 
   
	/**
	* 
	* CESSATION ELIMINATION REFUGEE
	* @return
	*/
	 
	public function cessation_elimination_refugee() 
	{ 
		
		$this->data["result"] = "";
		$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('cessation_elimination_refugee')));
		$meta = array('page_title' => lang('cessation_elimination_refugee'), 'bc' => $bc);	
		$this->page_construct('application_forms/cessation_elimination_refugee',$meta, $this->data); 
	}
	
	public function getCessationEliminationRefugees()
	{
		$this->load->library('datatables');	

		$delete = "<a href='#' class='delete-ce po' title='" . lang("delete_cessation_elimination_refugee") . "' data-content=\"<p>"
            . lang('r_u_sure') . "</p><a class='btn btn-danger' href='" . site_url('application_forms/delete_cessation_elimination_refugee/$1') . "'>"
            . lang('i_m_sure') . "</a><a class='btn btn-default'>" . lang('no') . "</a>\"  rel='popover'><i class=\"fa fa-trash-o\"></i>"
            . lang('delete_cessation_elimination_refugee') . "</a>";
             
		 
		$action_link = '<div class="btn-group text-left"><button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">'.lang("actions").'<span class="caret"></span></button>
					        <ul class="dropdown-menu pull-right" role="menu">					        																									
					            <li><a  class="approve-ce" data-backdrop="static" data-keyboard="false"  data-toggle="modal" data-target="#myModal" href="'.site_url('application_forms/approve_cessation_refugee/$1').'" ><i class="fa fa-newspaper-o"></i>'.lang('approve_cessation_refugee').'</a></li>
								<li><a   target="_blank" href="'.site_url('application_forms/cessation_elimination_refugee_form/$1').'" ><i class="fa fa-newspaper-o"></i>'.lang('cessation_elimination_refugee').'</a></li>
					            <li><a   target="_blank" href="'.site_url('application_forms/cessation_refugee_declaration_form/$1').'" ><i class="fa fa-newspaper-o"></i>'.lang('cessation_refugee_declaration_form').'</a></li>
								<li><a  class="edit-ce"  data-backdrop="static" data-keyboard="false"  data-toggle="modal" data-target="#myModal"  href="'.site_url('application_forms/edit_cessation_elimination_refugee/$1').'" ><i class="fa fa-pencil-square-o"></i>'.lang('edit_cessation_elimination_refugee').'</a></li>
								<li><a  data-backdrop="static" data-keyboard="false"  data-toggle="modal" data-target="#myModal" href="'.site_url('application_forms/attach_cessation_refugee/$1').'" ><i class="fa fa-plus-circle"></i>'.lang('attachment').'</a></li>	
								<li>'.$delete.'</li>
							</ul>
					    </div>'; 

        	$this->datatables->select("id,
									full_case,
									firstname,
									lastname,
									dob,
									gender,
									nationality,
									relationship_kh,
									phone,
									created_date,
									STATUS ", false)
			->from("erp_v_list_refugee_cessation")
			->where("erp_v_list_refugee_cessation.STATUS <> 'deleted'")
			->order_by("created_date","desc");

	        $this->datatables->add_column("Actions", $action_link, "id");
	        echo $this->datatables->generate();
	}
	
	public function add_cessation_elimination_refugee()
	{
		$post = $this->input->post();
		if($post == true){ 
			$this->erp->checkPermissions('add', null, 'cessation_refugee_declaration');
			$village = $this->applications->addTags($post['village'],'village');
			
			$data = array(
				"application_id" => $post['application_id'], 
				"reasons" => $post['reasons'],
				"contact_number" => $post['contact_number'],							
				"country" =>  $post['country'],
				"province" => $post['province'],
				"district" => $post['district'],
				"commune" =>  $post['commune'],
				"village"   =>   $village,
				"address" => $post['address'],
				"address_kh" =>   $post['address_kh'],
				"status"    =>   'pending',
				"created_by" => $this->session->userdata("user_id"),
				"created_date" => $this->erp->fld(date("d/m/Y H:i")),
				"member_id"=> $post['member'],
			); 
			
			$members = array();
			foreach($post['members'] as $member){
				$members[] = array("member_id" => $member);
			}
			
			$config1 = array(
				'upload_path' => 'assets/uploads',
				'allowed_types' => 'gif|jpg|png', 
			);   
			$this->load->library('upload', $config1);
			if($this->upload->do_upload('photo')){  
				$file_name = $this->upload->data('file_name'); 
				$data['photo'] = $file_name;
			}
			if($data['photo']=="") {
				$data['photo'] = $post['photoHidden']; 
			}	 
			if($this->applications->addCessationEliminatinRefugee($data, $members)){
				$this->session->set_flashdata('message', lang("cessation_elimination_refugee_saved"));
				redirect("application_forms/cessation_elimination_refugee/");
			}
		}
		$this->data['id'] = $id;
		$this->data['countries'] = $this->applications->getAllCountries();
		$this->data['provinces'] = $this->applications->getAllProvinces();
		$this->data['communes'] = $this->applications->getAllCommunces();
		$this->data['districts'] = $this->applications->getAllDistricts();
		$this->data['users'] = $this->applications->getUsers(); 
		$this->data['modal_js'] = $this->site->modal_js();
		$this->load->view($this->theme . 'application_forms/add_cessation_elimination_refugee', $this->data);
	}
	
	public function edit_cessation_elimination_refugee($id=false) 
	{  		
		$post = $this->input->post();
		if($post == true){ 
			$this->erp->checkPermissions('edit', null, 'cessation_refugee_declaration');
			$village = $this->applications->addTags($post['village'],'village');
			$data = array(
				"application_id" => $post['application_id'], 
				"reasons" => $post['reasons'],
				"contact_number" => $post['contact_number'],
				"country" =>  $post['country'],
				"province" => $post['province'],
				"district" => $post['district'],
				"commune" =>  $post['commune'],
				"village"  =>   $village,
				"address" => $post['address'],
				"address_kh" =>   $post['address_kh'],
				"status"  => 'pending',
				"created_by" => $this->session->userdata("user_id"),
				"created_date" => $this->erp->fld(date("d/m/Y")),
			);    
			
			$config1 = array(
					'upload_path' => 'assets/uploads',
					'allowed_types' => 'gif|jpg|png', 
				);   
			$this->load->library('upload', $config1);
			$this->upload->initialize($config1);
			
			if($this->upload->do_upload('photo')){  
				$file_name = $this->upload->data('file_name'); 
				$data['photo'] = $file_name;
			} 
			
			$config2 =array(
					'upload_path' => 'assets/uploads/document/cessation_elimination',
					'allowed_types' => 'doc|pdf'
			); 
			$this->load->library('upload', $config2);
			$this->upload->initialize($config2);
			if($this->upload->do_upload('attachment')){  
				$file_name = $this->upload->data('file_name');
				$data['attachment'] = $file_name; 
			}
			
			if($this->applications->updateCessationEliminatinRefugee($id,$data)){
				$this->session->set_flashdata('message', lang("cessation_elimination_refugee_edited"));
				redirect("application_forms/cessation_elimination_refugee/");
			}
		}
		
		
		$this->data['id'] = $id;
		$this->data['application'] =$this->applications->getCessationEliminatinRefugeeById($id);
		$this->data['result'] = $this->applications->getRSDApplicationById($this->data['application']->application_id);
		
		$this->data['countries'] = $this->applications->getAllCountries();
		$this->data['provinces'] = $this->applications->getAllProvinces();
		$this->data['communes'] = $this->applications->getAllCommunces();
		$this->data['districts'] = $this->applications->getAllDistricts();
		$this->data['users'] = $this->applications->getUsers(); 
		$this->data['modal_js'] = $this->site->modal_js();
		$this->load->view($this->theme . 'application_forms/edit_cessation_elimination_refugee', $this->data);
	}
	
	public function delete_cessation_elimination_refugee($id = false)
	{	
		if($id){
			$this->erp->checkPermissions('delete', null, 'cessation_refugee_declaration');
			if($this->applications->deleteCessationEliminatinRefugee($id)){
				$this->session->set_flashdata('message', lang("cessation_elimination_refugee_deleted"));
				redirect("application_forms/cessation_elimination_refugee");
			}
		}
	}
			
	public function cessation_elimination_refugee_form($id = false) 
	{    
		$this->erp->checkPermissions('form', null, 'cessation_refugee_declaration');
		$this->data['application'] =$this->applications->getCessationEliminatinRefugeeById($id); 
		$this->data["result"] =  $this->applications->getRSDApplicationById($this->data['application']->application_id);
		$this->data["decision"] = $this->applications->getRefugeeRecognitionByApplicationId($this->data["application"]->application_id);
		$this->data['relationships'] = $this->applications->getRelationship();
		$this->data['members'] =  $this->applications->getCessationEliminationMembersByCessationId($this->data["application"]->id);
		$this->data['member_declare'] = $this->applications->getFamilyMembersDependantAccompanyingById($this->data["application"]->member_id);
		// $this->erp->print_arrays($this->data['members']);
		$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('cessation_elimination_refugee_form')));
		$meta = array('page_title' => lang('cessation_elimination_refugee_form'), 'bc' => $bc);	
		$this->page_construct('application_forms/cessation_elimination_refugee_form',$meta, $this->data);
	}
	
	public function cessation_refugee_declaration_form($id = false)
	{ 		
		$this->erp->checkPermissions('form', null, 'cessation_refugee_declaration'); 
		$this->data["cessation"] = $this->applications->getCessationEliminatinRefugeeById($id); 
		$this->data["result"] = $this->applications->getRSDApplicationById($this->data["cessation"]->application_id);
		$this->data["recognition"] = $this->application_setup_model->getRegconitionTemplateByType($this->data["result"]->case_prefix);
		$this->data['relationships'] = $this->applications->getRelationship();
		$this->data['members'] =  $this->applications->getCessationEliminationMembersByCessationId($this->data["cessation"]->id);
		$this->data['member_declare'] = $this->applications->getFamilyMembersDependantAccompanyingById($this->data["cessation"]->member_id);
		$this->data['departments'] = $this->applications->getDepartmentDirector();
		
		$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('cessation_refugee_declaration_form')));
		$meta = array('page_title' => lang('cessation_refugee_declaration_form'), 'bc' => $bc);	
		$this->page_construct('application_forms/cessation_refugee_declaration_form',$meta, $this->data);
	}
	
	public function approve_cessation_refugee($id = false)
	{ 
		$post = $this->input->post(); 
		$members = array(); 
		if($post == true){ 
			$this->erp->checkPermissions('approved', null, 'cessation_refugee_declaration'); 
			$data = array(			
						"recognized_no" => $post['recognized_no'],
						"recognized_date" => $this->erp->fld($post['recognized_date']),
						"provided_date" => $this->erp->fld($post['provided_date']),
						"status" => "approved",
					); 
			if($this->applications->updateCessationEliminatinRefugee($id, $data)){
				$this->session->set_flashdata('success', lang("approve_cessation_refugee"));
				redirect("application_forms/cessation_elimination_refugee");
			}
			
		}else{
			
			$this->data['id'] = $id;
			$this->data['cessation'] = $this->applications->getCessationEliminatinRefugeeById($id);
			$this->data['applications'] =  $this->applications->getAllRSDApplications();
			$this->data['users'] = $this->applications->getUsers();
			$this->data['offices'] = $this->applications->getlocationByOffice();
			$this->data['modal_js'] = $this->site->modal_js();		
			$this->load->view($this->theme . 'application_forms/approve_cessation_refugee', $this->data);
		} 	
	}
	
	public function attach_cessation_refugee($id = false )
	{
		$result = $this->applications->getCessationEliminatinRefugeeById($id);
		$this->global_model->attach_forms('fa_refugee_cessation_elimination_request', $id, 'cessation_elimination');
		$this->data['action_method'] = 'attach_cessation_refugee';
		$this->data['delete_url'] = 'delete_attach_cessation_refugee';
		$this->data['view_folder'] = 'cessation_elimination';
		$this->data['contorler'] = $this->uri->segment(1);
		$this->data['id'] = $id;
		$this->data['result'] = $result; 
		$this->data['modal_js'] = $this->site->modal_js();		
		$this->load->view($this->theme . 'global/view_attach_forms', $this->data);
	}    
	
	public function delete_attach_cessation_refugee($id = false, $name = null, $img = null)
	{
		$this->erp->checkPermissions('deleted', null, 'cessation_refugee_declaration'); 
		$this->global_model->delete_attach_forms('fa_refugee_cessation_elimination_request', $id, $name, $img);
	}   
	 
	 
	/**
	* 
	* @param undefined $id
	* WITHDRAWAL REFUGEE
	* @return
	*/
	
	public function withdrawal_refugee()
	{
		$this->erp->checkPermissions('index', null, 'withdrawal_refugee'); 
		$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('withdrawal_refugee')));
		$meta = array('page_title' => lang('withdrawal_refugee'), 'bc' => $bc);
		$this->page_construct('application_forms/withdrawal_refugee',$meta, $this->data);
	}
			
	public function getWithdrawalRefugees()
	{
		$this->load->library('datatables');
		
		$delete = "<a href='#' class='delete-refugee po' title='" . lang("delete_withdrawal_refugee") . "' data-content=\"<p>"
            . lang('r_u_sure') . "</p><a class='btn btn-danger' href='" . site_url('application_forms/delete_withdrawal_refugee/$1') . "'>"
            . lang('i_m_sure') . "</a><a class='btn btn-default'>" . lang('no') . "</a>\"  rel='popover'><i class=\"fa fa-trash-o\"></i>"
            . lang('delete_withdrawal_refugee') . "</a>";
            
        $action_link = '<div class="btn-group text-left"><button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">'.lang("actions").'<span class="caret"></span></button>
					        <ul class="dropdown-menu pull-right" role="menu">
					             <li><a data-backdrop="static" data-keyboard="false"  data-toggle="modal" data-target="#myModal" href="'.site_url('application_forms/approve_withdrawal_refugee/$1').'" ><i class="fa fa-newspaper-o"></i>'.lang('approve_withdrawal_refugee').'</a></li>
								 <li><a target="_blank" href="'.site_url('application_forms/withdrawal_refugee_form/$1').'" ><i class="fa fa-newspaper-o"></i>'.lang('withdrawal_refugee_form').'</a></li>
					             <li><a target="_blank" href="'.site_url('application_forms/withdrawal_refugee_declaration_form/$1').'" ><i class="fa fa-newspaper-o"></i>'.lang('withdrawal_refugee_declaration_form').'</a></li>
					             <li><a class="edit-refugee po" data-backdrop="static" data-keyboard="false"  data-toggle="modal" data-target="#myModal" href="'.site_url('application_forms/edit_withdrawal_refugee/$1').'" ><i class="fa fa-pencil-square-o"></i>'.lang('edit_withdrawal_refugee').'</a></li>
								 <li><a  data-backdrop="static" data-keyboard="false"  data-toggle="modal" data-target="#myModal" href="'.site_url('application_forms/attach_withdrawal_refugee/$1').'" ><i class="fa fa-plus-circle"></i>'.lang('attachment').'</a></li>	
								 <li>'.$delete.'</li>
					        </ul>
					    </div>';
					    
           $this->datatables->select("id,
									full_case,
									firstname,
									lastname,
									dob,
									gender,
									nationality,
									relationship_kh,
									phone,
									created_date,
									STATUS ", false)
			->from("erp_v_list_withdrawal_refugee")
			->where("erp_v_list_withdrawal_refugee.status <> 'deleted'")
			->order_by("created_date","desc");
        $this->datatables->add_column("Actions", $action_link, "id");
        echo $this->datatables->generate();
	}
	
	public function add_withdrawal_refugee()
	{
		$post = $this->input->post();
		if($post == true){
			$this->erp->checkPermissions('add', null, 'withdrawal_refugee'); 
			$village = $this->applications->addTags($post['village'],'village');			
			$data = array(
					"application_id" =>  $post['application_id'],
					"occupation"=>  $post['occupation'], 
					"photo"=>  $new_name,
					"contact_number"=>  $post['contact_number'], 					
					"country" =>  $post['country'],
					"province" => $post['province'],
					"district" => $post['district'],
					"commune" =>  $post['commune'],
					"village" 	 =>  $village,
					"address"=>  $post['address'],
					"address_kh" =>  $post['address_kh'],
					"created_by" => $this->session->userdata("user_id"),
					"created_date" => $this->erp->fld(date("d/m/Y H:i")),
					"status"=> 'pending',
					"member_id"=> $post['member'],
					"reasons" => $post['reasons']
				);
				
				$members = array();
				foreach($post['members'] as $member){
					$members[] = array("member_id" => $member);
				}
				
				$config1 = array(
					'upload_path' => 'assets/uploads',
					'allowed_types' => 'gif|jpg|png', 
				);   
				$this->load->library('upload', $config1);
				if($this->upload->do_upload('photo')){  
					$file_name = $this->upload->data('file_name'); 
					$data['photo'] = $file_name;
				} 
				if($data['photo']=="") {
					$data['photo'] = $post['photoHidden']; 
				}
				if($this->applications->addWithdrawalRefugee($data, $members)){
					$this->session->set_flashdata('message', lang("withdrawal_refugee_saved"));
					redirect("application_forms/withdrawal_refugee");
				}
		}
		$this->data['countries'] = $this->applications->getAllCountries();
		$this->data['provinces'] = $this->applications->getAllProvinces();
		$this->data['communes'] = $this->applications->getAllCommunces();
		$this->data['districts'] = $this->applications->getAllDistricts();
		$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('add_withdrawal_refugee')));
		$meta = array('page_title' => lang('add_withdrawal_refugee'), 'bc' => $bc);
		$this->data['modal_js'] = $this->site->modal_js();
		$this->load->view($this->theme . 'application_forms/add_withdrawal_refugee', $this->data);
	}
	
	public function edit_withdrawal_refugee($id=false)
	{
		$post = $this->input->post();
		if($post == true){  
			$this->erp->checkPermissions('edit', null, 'withdrawal_refugee'); 
			$village = $this->applications->addTags($post['village'],'village');		
			$data = array(
					"application_id" =>  $post['application_id'],
					"occupation"=>  $post['occupation'], 
					"photo"=>  $new_name,
					"contact_number"=>  $post['contact_number'], 					
					"country" =>  $post['country'],
					"province" => $post['province'],
					"district" => $post['district'],
					"commune" =>  $post['commune'],
					"village" 	 =>  $village,
					"address"=>  $post['address'],
					"address_kh" =>  $post['address_kh'],
					"status"=> 'pending',
					"created_by" => $this->session->userdata("user_id"),
					"created_date" => $this->erp->fld(date("d/m/Y H:i")),
				);
			
			$config1 = array(
					'upload_path' => 'assets/uploads',
					'allowed_types' => 'gif|jpg|png', 
				);   
			$this->load->library('upload', $config1);
			$this->upload->initialize($config1);
			
			if($this->upload->do_upload('photo')){  
				$file_name = $this->upload->data('file_name'); 
				$data['photo'] = $file_name;
			} 
			
			$config2 =array(
					'upload_path' => 'assets/uploads/document/withdrawal_refugee_request',
					'allowed_types' => 'doc|pdf'
			); 
			
			$this->load->library('upload', $config2);
			$this->upload->initialize($config2);
			if($this->upload->do_upload('attachment')){  
				$file_name = $this->upload->data('file_name');
				$data['attachment'] = $file_name; 
			}
				
			if($this->applications->updateWithdrawalRefugee($id,$data)){
				$this->session->set_flashdata('message', lang("withdrawal_refugee_edited"));
				redirect("application_forms/withdrawal_refugee");
			}
			
		}
		
		$this->data["withdrawal"] = $this->applications->getWithdrawalById($id);
		$this->data["result"] = $this->applications->getRSDApplicationById($this->data["withdrawal"]->application_id);
		$this->data['countries'] = $this->applications->getAllCountries();
		$this->data['provinces'] = $this->applications->getAllProvinces();
		$this->data['communes'] = $this->applications->getAllCommunces();
		$this->data['districts'] = $this->applications->getAllDistricts();
		$this->data['id'] = $id;
		$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('edit_withdrawal_refugee')));
		$meta = array('page_title' => lang('edit_withdrawal_refugee'), 'bc' => $bc);
		$this->data['modal_js'] = $this->site->modal_js();
		$this->load->view($this->theme . 'application_forms/edit_withdrawal_refugee', $this->data);
	
	}
	
	public function delete_withdrawal_refugee($id = false) 
	{		
		if($id) {	
			$this->erp->checkPermissions('delete', null, 'withdrawal_refugee'); 
			if($this->applications->deleteWithdrawalRefugee($id)) {
				$this->session->set_flashdata('message', lang("withdrawal_refugee_deleted"));
				redirect("application_forms/withdrawal_refugee/");
			}
		}
		
	}
	
	public function withdrawal_refugee_form($id=false)
	{	
		$this->erp->checkPermissions('form', null, 'withdrawal_refugee'); 
		$this->data["withdrawal"] = $this->applications->getWithdrawalById($id);
		$this->data["result"] = $this->applications->getRSDApplicationById($this->data["withdrawal"]->application_id);
		$this->data["decision"] = $this->applications->getRefugeeRecognitionByApplicationId($this->data["withdrawal"]->application_id);
		
		$this->data['relationships'] = $this->applications->getRelationship();
		$this->data['members'] =  $this->applications->getWithdrawalMembersByWithdrawalId($this->data["withdrawal"]->id);
		$this->data['member_declare'] = $this->applications->getFamilyMembersDependantAccompanyingById($this->data["withdrawal"]->member_id);
		
		$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('withdrawal_refugee')));
		$meta = array('page_title' => lang('withdrawal_refugee'), 'bc' => $bc);
		$this->page_construct('application_forms/withdrawal_refugee_form',$meta, $this->data);
	}
	
	public function withdrawal_refugee_declaration_form($id = false)
	{	
		$this->erp->checkPermissions('form_declaration', null, 'withdrawal_refugee'); 
		$this->data["withdrawal"] = $this->applications->getWithdrawalById($id);
		$this->data["result"] = $this->applications->getRSDApplicationById($this->data["withdrawal"]->application_id);
		$this->data["recognition"] = $this->application_setup_model->getRegconitionTemplateByType($this->data["result"]->case_prefix);
		$this->data['relationships'] = $this->applications->getRelationship();
		$this->data['members'] =  $this->applications->getWithdrawalMembersByWithdrawalId($this->data["withdrawal"]->id);
		$this->data['member_declare'] = $this->applications->getFamilyMembersDependantAccompanyingById($this->data["withdrawal"]->member_id);
		$this->data['departments'] = $this->applications->getDepartmentDirector();
		
		$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('withdrawal_refugee_declaration_form')));
		$meta = array('page_title' => lang('withdrawal_refugee_declaration_form'), 'bc' => $bc);	
		$this->page_construct('application_forms/withdrawal_refugee_declaration_form',$meta, $this->data);
	}

	public function approve_withdrawal_refugee($id = false)
	{ 
		$post = $this->input->post(); 
		$members = array(); 
		if($post == true){ 
			$this->erp->checkPermissions('approve', null, 'withdrawal_refugee'); 
			$data = array(			
						"recognized_no" => $post['recognized_no'],
						"recognized_date" => $this->erp->fld($post['recognized_date']),
						"provided_date" => $this->erp->fld($post['provided_date']),
						"status" => "approved",
					);
			
			if($this->applications->updateWithdrawalRefugee($id, $data)){
				$this->session->set_flashdata('success', lang("approve_withdrawal_refugee"));
				redirect("application_forms/withdrawal_refugee");
			}
		}else{
			$this->data['id'] = $id;
			$this->data['withdrawal'] = $this->applications->getWithdrawalById($id);
			$this->data['applications'] =  $this->applications->getAllRSDApplications();
			$this->data['users'] = $this->applications->getUsers();
			$this->data['offices'] = $this->applications->getlocationByOffice();
			$this->data['modal_js'] = $this->site->modal_js();		
			$this->load->view($this->theme . 'application_forms/approve_withdrawal_refugee', $this->data);
		} 	
	}
	
	public function attach_withdrawal_refugee($id = false )
	{
 		$this->erp->checkPermissions('attachment', null, 'withdrawal_refugee'); 
		$result = $this->applications->getWithdrawalById($id);
		$this->global_model->attach_forms('fa_refugee_withdrawal_request', $id, 'withdrawal_refugee');
		$this->data['action_method'] = 'attach_withdrawal_refugee';
		$this->data['delete_url'] = 'delete_attach_withdrawal_refugee';
		$this->data['view_folder'] = 'withdrawal_refugee';
		$this->data['contorler'] = $this->uri->segment(1);
		$this->data['id'] = $id;
		$this->data['result'] = $result; 
		$this->data['modal_js'] = $this->site->modal_js();		
		$this->load->view($this->theme . 'global/view_attach_forms', $this->data);		
	}    
	
	public function delete_attach_withdrawal_refugee($id = false, $name = null, $img = null)
	{
		$this->erp->checkPermissions('delete', null, 'withdrawal_refugee');
		$this->global_model->delete_attach_forms('fa_refugee_withdrawal_request', $id, $name, $img);

		
	}   
	    
	
	/**
	* 
	* RECOGNIZATION REFUGEE
	* @return
	*/ 
	
	public function recognition_refugee_form($id = false)
	{  
		$this->erp->checkPermissions('form', true, 'recognition_refugees');
		$this->data['departments'] = $this->applications->getDepartmentDirector();
		$this->data["reconitions"] = $this->applications->getRefugeeRecognitionById($id);		
		$this->data["result"] = $this->applications->getRSDApplicationById($this->data["reconitions"]->application_id);
		$this->data["recognition_content"] = $this->application_setup_model->getRegconitionTemplateByType($this->data["result"]->case_prefix);
		$this->data['relationships'] = $this->applications->getRelationship();
		$this->data['members'] =  $this->applications->getRecognitionMembersByRecognitionId($this->data["reconitions"]->id);
		$this->data['member_declare'] = $this->applications->getFamilyMembersDependantAccompanyingById($this->data["reconitions"]->member_id);
		
		$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('recognition_refugee_form')));
		$meta = array('page_title' => $this->data["result"]->case_prefix.$this->data["result"]->case_no);	
		$this->page_construct('application_forms/recognition_refugee_form',$meta, $this->data);
	}
	
	public function recognition_refugee()
	{
		$this->data["result"] = "";
		$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('recognition_refugee')));
		$meta = array('page_title' => lang('recognition_refugee'), 'bc' => $bc);	
		$this->page_construct('application_forms/recognition_refugee',$meta, $this->data); 
	}
	
	public function getRecognizedRefugee()
	{
		$this->load->library('datatables');	
		
		$delete = "<a href='#' class='delete-sm po' title='" . lang("delete_recognition_refugee") . "' data-content=\"<p>"
            . lang('r_u_sure') . "</p><a class='btn btn-danger' href='" . site_url('application_forms/delete_recognition_refugee/$1') . "'>"
            . lang('i_m_sure') . "</a><button class='btn'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i>"
            . lang('delete_recognition_refugee') . "</a>";
			
		$action_link = '<div class="btn-group text-left"><button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">'.lang("actions").'<span class="caret"></span></button>
					        <ul class="dropdown-menu pull-right" role="menu">					        																							
								<li><a data-backdrop="static" data-keyboard="false"  data-toggle="modal" data-target="#myModal" href="'.site_url('application_forms/approve_recognition_refugee/$1').'" ><i class="fa fa-newspaper-o"></i>'.lang('approve_recognition_refugee').'</a></li>
					            <li><a target="_blank" href="'.site_url('application_forms/recognition_refugee_form/$1').'" ><i class="fa fa-newspaper-o"></i>'.lang('recognition_form').'</a></li>
								<li><a data-backdrop="static" data-keyboard="false"  data-backdrop="static" data-keyboard="false"  data-toggle="modal" data-target="#myModal" href="'.site_url('application_forms/attach_recognition_refugee/$1').'" ><i class="fa fa-plus-circle"></i>'.lang('attachment').'</a></li>	
								<li>'.$delete.'</li>
							</ul>
					    </div>';
			
			$this->datatables->select("id,
									full_case,
									firstname,
									lastname,
									dob,
									gender,
									nationality,
									relationship_kh,
									created_date,
									recognized_date,
									STATUS ", false)
			->from("erp_v_list_fa_recognition_refugee")
			->where("erp_v_list_fa_recognition_refugee.STATUS <> 'deleted'");
			
			if($this->erp->input->get('case_no')){
				$this->datatables->where('full_case',$this->input->get('case_no'));
			} 
			if($this->erp->input->get('date_from') && $this->erp->input->get('date_to')){
				$this->datatables->where('date(created_date) >=',$this->erp->fld($this->erp->input->get('date_from')));
				$this->datatables->where('date(created_date) <=',$this->erp->fld($this->erp->input->get('date_to')));
			}
			$this->datatables->order_by("created_date","desc");
			$this->datatables->add_column("Actions", $action_link, "id");
        echo $this->datatables->generate();
	}	
	
	public function add_recognition_refugee()
	{   
		$post = $this->input->post(); 
		$members = array(); 
		if($post == true){
			$config = array(
		        array(
		                'field' => 'application_id',
		                'label' => 'application_id',
		                'rules' => 'required'
		        ),
			);

			$this->form_validation->set_rules($config);
			if ($this->form_validation->run() == FALSE)
	        {
	             $this->session->set_flashdata('error', lang("error_fill_application_id"));
					redirect("application_forms/recognition_refugee");
	        }

			$this->erp->checkPermissions('add', true, 'recognition_refugees');
			$data = array(
						  "application_id" => $post['application_id'],
						  "member_id" => $post['member'],	
						  "created_by" => $this->session->userdata("user_id"),
						  "created_date" => $this->erp->fld(date("d/m/Y H:i")),
						  "status" => "pending",
						);
			$members = array();
			foreach($post['members'] as $member){
				$members[] = array("member_id" => $member);
			}
						
			if($this->applications->addRefugeeRecognition($data, $members)){
				$this->session->set_flashdata('success', lang("add_recognition_refugee"));
				redirect("application_forms/recognition_refugee");
			}
			
		}else{
	
			$this->data['applications'] =  $this->applications->getAllRSDApplications();
			$this->data['users'] = $this->applications->getUsers();
			$this->data['offices'] = $this->applications->getlocationByOffice();
			$this->data['modal_js'] = $this->site->modal_js();		
			$this->load->view($this->theme . 'application_forms/add_recognition_refugee', $this->data);
		} 	
	}
	
	public function delete_recognition_refugee($id =false)
	{
		if($id){
			$this->erp->checkPermissions('delete', true, 'recognition_refugees');
			if($this->applications->deleteRefugeeRecognition($id)){
				$this->session->set_flashdata('message', lang("delete_recognition_refugee"));
				redirect("application_forms/recognition_refugee");
			}
		}
	}
	
	public function approve_recognition_refugee($id = false)
	{ 
		$post = $this->input->post(); 
		$members = array(); 
		if($post == true){ 
			$this->erp->checkPermissions('approved', true, 'recognition_refugees');	
			$data = array(			
						"recognized_no" => $post['recognized_no'],
						"recognized_date" => $this->erp->fld($post['recognized_date']),
						"provided_date" => $this->erp->fld($post['provided_date']),
						"status" => 'approved',
					);
			
			if($this->applications->updateRefugeeRecognition($id, $data)){
				$this->session->set_flashdata('success', lang("approve_recognition_refugee"));
				redirect("application_forms/recognition_refugee");
			}
		}else{
			$this->data['id'] = $id;
			$this->data['recognition'] = $this->applications->getRecognitionMembersById($id);
			$this->data['applications'] =  $this->applications->getAllRSDApplications();
			$this->data['users'] = $this->applications->getUsers();
			$this->data['offices'] = $this->applications->getlocationByOffice();
			$this->data['modal_js'] = $this->site->modal_js();		
			$this->load->view($this->theme . 'application_forms/approve_recognition_refugee', $this->data);
		} 	
	}
	
	public function attach_recognition_refugee($id = false) 
	{ 
		$result = $this->applications->getRefugeeRecognitionById($id);
		$this->global_model->attach_forms('fa_interview_recognitions', $id, 'recognition_refugee');
		$this->data['id'] = $id;
		$this->data['result'] = $result; 
		$this->data['action_method'] = 'attach_recognition_refugee';
		$this->data['delete_url'] = 'delete_attach_recognition_refugee';
		$this->data['view_folder'] = 'recognition_refugee';
		$this->data['contorler'] = $this->uri->segment(1);
		$this->data['modal_js'] = $this->site->modal_js();		
		$this->load->view($this->theme . 'global/view_attach_forms', $this->data);
	} 
	
	public function delete_attach_recognition_refugee($id = false, $name = null, $img = null)
	{
		$this->global_model->delete_attach_forms('fa_interview_recognitions', $id, $name, $img);
	}
	public function test($id = false, $name = null){
		$result = $this->db->where("id", '4')->get('fa_refugee_permanent_card_request')->row();
		// $result = $this->applications->getRefugeeRecognitionById('11');
		// $data = array();
		// if($result->attachment != null ) {
		// 	$a=json_decode($result->attachment);
		// 	foreach ($a as $key => $rows) {
		// 		if ($rows->file_name != $name){
		// 			$data['attachment'][] =  $rows;
		// 		}
		// 	}
		// 	$data['attachment'] = json_encode($data['attachment']);
		// 	// $pos = array_search($name, $a); 
		// 	// unset($a[$pos]);// Remove from array
		// 	// $data["attachment"] =  json_encode(array_values($a)); 
		// } 
		echo json_encode($result, true);
	}
	/**
	**  Report
	**
	*/ 
	public function social_affairs_report()
	{
		$this->data["result"] = "";
		$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('social_affairs_report')));
		$meta = array('page_title' => lang('social_affairs_report'), 'bc' => $bc);	
		$this->page_construct('application_forms/social_affairs_report',$meta, $this->data);	
	} 
	
	public function getDataByApplicationWithdral(){
		$application_id = $this->input->get("application_id");
		$member_id = $this->input->get("member_id");
		$result = "";
		if($member_id == 0 || $member_id == null){
			$result = $this->db->where("id",$application_id)->get("fa_rsd_applications")->row();
		}
		elseif($member_id > 0){
			$result = $this->db->where("id",$member_id)->get("fa_family_members")->row();
		}
		else{
			return false;
		}
		$village_name = $this->applications->getTagsById($result->village);
		echo json_encode(array('result'=>$result,'vill'=>$village_name));
	}

	
}
?>
