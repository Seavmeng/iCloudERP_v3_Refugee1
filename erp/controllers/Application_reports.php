<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Application_reports extends MY_Controller
{
	
	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->library('pagination');
		$this->load->model('Site','site');
		$this->lang->load('applications', $this->Settings->language);
		$this->lang->load('application_forms', $this->Settings->language);
		$this->load->model('applications_model','applications');
		$this->site->auditLogs();
	} 
	
	public function index()
	{ 
		redirect("application_reports/index");
	}
	 
	/*=============REPORT K3===================*/
	public function research_sections()
	{ 
		if(isset($_POST['export'])){ 
			if($_GET['sector'] == "register") {
				$this->register_export(); 	 
			}else if($_GET['sector'] == "interview_appointment") {
				$this->interview_appointment_export(); 		 
			}else if($_GET['sector'] == "interviewed") {
				$this->interviewed_export(); 		 
			}else if($_GET['sector'] == "interview_evaluate") {
				$this->interview_evaluate_export(); 		 
			}else if($_GET['sector'] == "interview_negative_appeal"){
				$this->interview_negative_appeal_export();
			}else if($_GET['sector'] == "all"){
				 $this->all_register_report_export();
			}else if($_GET['sector'] == "interview_request_on_appeal"){
				 $this->interview_request_on_appeal_export();
			}else if($_GET['sector'] == "interview_on_appeal"){
				 $this->interview_on_appeal_export();
			}else if($_GET['sector'] == "interview_evaluate_on_appeal"){
				 $this->interview_evaluate_on_appeal_export();
			} 
			// Add more export excel file follow WhatApp image
			else if ($_GET['sector'] == "first_reject"){
				$this->first_reject_export();
			}
			else if ($_GET['sector'] == "first_recognise"){
				$this->first_recognise_export();
			}
			else if ($_GET['sector'] == "check_out_request_on_appeal"){
				$this->check_out_request_on_appeal_export();
			}
            else if($_GET['sector'] == "interview_appointment_on_appeal"){
                $this->interview_appointment_on_appeal_export();
            }
            else if($_GET['sector'] == "reject_on_appeal"){
                $this->reject_on_appeal_export();
            }
            else if($_GET['sector'] == "accept_on_appeal"){
                $this->accept_on_appeal_export();
            }


		} else if(isset($_POST['print_research'])){ 
			if($_GET['sector'] == "all") {
				$this->all_register_report_print(); 	 
			}else if($_GET['sector'] == "interview_appointment") {
				$this->interview_appointment_print(); 		 
			}else if($_GET['sector'] == "interviewed"){
				$this->interviewed_print();
			}else if($_GET['sector'] == "interview_on_appeal"){
				$this->interview_on_appeal_print();
			}else if($_GET['sector'] == "interview_evaluate"){
				$this->interview_evaluate_print();
			}else if($_GET['sector'] == "interview_request_on_appeal"){
				$this->interview_request_on_appeal_print();
			}else if($_GET['sector'] == "interview_evaluate_on_appeal"){
				$this->interview_evaluate_on_appeal_print();
			}
			else if($_GET['sector'] == "register"){
				$this->register_report_print();
			}

			else if($_GET['sector'] == "first_reject"){
				$this->first_reject_print();
			}
			else if($_GET['sector'] == "first_recognise"){
				$this->first_recoginse_print();
			}
			else if($_GET['sector'] == "check_out_request_on_appeal"){
				$this->check_out_request_on_appeal_print();
			}
			else if($_GET['sector'] == "reject_on_appeal"){
				$this->reject_on_appeal_print();
			}
			else if($_GET['sector'] == "interview_appointment_on_appeal"){
				$this->interview_appointment_on_appeal_print();
			}
			else if($_GET['sector'] == "accept_on_appeal"){
				$this->accept_on_appeal_print();
			}
		}
		else{		
			$this->data["result"] = "";
			$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('research_sections')));
			$meta = array('page_title' => lang('research_sections'), 'bc' => $bc);	
			$this->page_construct('application_reports/research_sections',$meta, $this->data);	
		}
	}
	// Print all register report
	public function all_register_report_print()
	{				
		$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('បង្ហាញទាំងអស់')));
		$meta = array('page_title' => lang('បង្ហាញទាំងអស់'), 'bc' => $bc);	
		$this->page_construct('application_reports/all_report_print',$meta, $this->data);
	} 
    // Print interview appointment report
	public function interview_appointment_print()
	{				
		$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('បានណាត់សម្ភាសន៍')));
		$meta = array('page_title' => lang('បានណាត់សម្ភាសន៍'), 'bc' => $bc);	
		$this->page_construct('application_reports/interview_appointment_print',$meta, $this->data);	
	} // Print reports interviewed
	public function interviewed_print()
	{				
		$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('បានសម្ភាសន៍រួច')));
		$meta = array('page_title' => lang('បានសម្ភាសន៍រួច'), 'bc' => $bc);
		$this->page_construct('application_reports/interviewed_print',$meta, $this->data);	
	} // Print reports interview on appeal
	public function interview_on_appeal_print()
	{				
		$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('សម្ភាសន៍លើបណ្តឹងឧទ្ធរណ៍')));
		$meta = array('page_title' => lang('សម្ភាសន៍លើបណ្តឹងឧទ្ធរណ៍'), 'bc' => $bc);	
		$this->page_construct('application_reports/interview_on_appeal_print',$meta, $this->data);	
	} // Print reports interview on appeal
	public function interview_evaluate_print()
	{				
		$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('វិនិច័្ឆយលើការសម្ភាសន៍')));
		$meta = array('page_title' => lang('វិនិច័្ឆយលើការសម្ភាសន៍'), 'bc' => $bc);	
		$this->page_construct('application_reports/interview_evaluate_print',$meta, $this->data);	
	} // Print reports interview on appeal
	public function interview_request_on_appeal_print()
	{				
		$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('ដាក់ពាក្យស្នើសុំលើបណ្តឹងឧទ្ធរណ៍')));
		$meta = array('page_title' => lang('ដាក់ពាក្យស្នើសុំលើបណ្តឹងឧទ្ធរណ៍'), 'bc' => $bc);	
		$this->page_construct('application_reports/interview_request_on_appeal_print',$meta, $this->data);	
	} // Print reports interview evaluate on appeal
	public function interview_evaluate_on_appeal_print()
	{				
		$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang("វិនិច័្ឆយលើបណ្តឹងឧទ្ធរណ៍")));
		$meta = array('page_title' => lang("វិនិច័្ឆយលើបណ្តឹងឧទ្ធរណ៍"), 'bc' => $bc);
		$this->page_construct('application_reports/interview_evaluate_on_appeal_print',$meta, $this->data);	
	} // Print reports interview evaluate on appeal
	public function register_report_print()
	{				
		$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('ទើបចុះឈ្មោះ')));
		$meta = array('page_title' => lang('ទើបចុះឈ្មោះ'), 'bc' => $bc);	
		$this->page_construct('application_reports/register_report_print',$meta, $this->data);	
	} 
	// Link to view
	public function first_reject_print()
	{				
		$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('បដិសេធជាលើកទីមួយ')));
		$meta = array('page_title' => lang('បដិសេធជាលើកទីមួយ'), 'bc' => $bc);	
		$this->page_construct('application_reports/first_reject_print',$meta, $this->data);	
	} // Link to view
	public function first_recoginse_print()
	{				
		$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang("ទទួលស្គាល់លើកទីមួយ")));
		$meta = array('page_title' => lang("ទទួលស្គាល់លើកទីមួយ"), 'bc' => $bc);
		$this->page_construct('application_reports/first_recognise_print',$meta, $this->data);	
	} // Link to view
	public function check_out_request_on_appeal_print()
	{				
		$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang("ពិនិត្យលេីការស្នេីសុំបណ្តឹងឧទ្ធរណ៏")));
		$meta = array('page_title' => lang("ពិនិត្យលេីការស្នេីសុំបណ្តឹងឧទ្ធរណ៏"), 'bc' => $bc);
		$this->page_construct('application_reports/check_out_request_on_appeal_print',$meta, $this->data);	
	} 
	public function reject_on_appeal_print()
	{				
		$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang("បដិសេធនៅលើបណ្តឹងឧទ្ធរណ៍")));
		$meta = array('page_title' => lang("បដិសេធនៅលើបណ្តឹងឧទ្ធរណ៍"), 'bc' => $bc);
		$this->page_construct('application_reports/reject_on_appeal_print',$meta, $this->data);	
	} 

	public function interview_appointment_on_appeal_print()
	{				
		$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang("ណាត់សម្ភាសន៍លើបណ្តឹងឧទ្ធរណ៍")));
		$meta = array('page_title' => lang("ណាត់សម្ភាសន៍លើបណ្តឹងឧទ្ធរណ៍"), 'bc' => $bc);
		$this->page_construct('application_reports/interview_appointment_on_appeal_print',$meta, $this->data);	
	} 
	public function accept_on_appeal_print()
	{				
		$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang("ទទួលស្គាល់លើបណ្តឹងឧទ្ធរណ៏")));
		$meta = array('page_title' => lang("ទទួលស្គាល់លើបណ្តឹងឧទ្ធរណ៏"), 'bc' => $bc);
		$this->page_construct('application_reports/accept_on_appeal_print',$meta, $this->data);	
	} 



	public function register_export()
	{
	 
	   $this->load->library('excel');
        
		$styleArray = array(
			'font'  => array(
				//'bold'  => true,
				//'color' => array('rgb' => 'FF0000'),
				'size'  => 8,
				'name'  => 'Khmer OS Siemreap',
			));		
		$this->excel->getActiveSheet()->getStyle('A1:O1')->applyFromArray($styleArray);
		$this->excel->getActiveSheet()->getRowDimension('1')->setRowHeight(20);		
		$this->excel->getActiveSheet()->getStyle('A1:O1')->getFont()->setBold(true)->setSize(10);
		$this->excel->getActiveSheet()->getStyle('A1:O1')->getFont()->getColor()->setRGB('FFFFFF'); 
		$this->excel->getActiveSheet()->getStyle('A1:O1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('A1:O1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('A1:O1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('555555');
			
		$this->excel->setActiveSheetIndex(0);
		$title = lang("របាយការណ៍បុគ្គលទើបចុះឈ្មោះ");		
        $this->excel->getActiveSheet()->setTitle($title);
        $this->excel->getActiveSheet()->SetCellValue('A1', lang('ល.រ'));
        $this->excel->getActiveSheet()->SetCellValue('B1', lang('លេខករណី'));
        $this->excel->getActiveSheet()->SetCellValue('C1', lang('ឈ្មោះ'));
        $this->excel->getActiveSheet()->SetCellValue('D1', lang('ឈ្មោះជាអក្សរទ្បាតាំង'));
        $this->excel->getActiveSheet()->SetCellValue('E1', lang('ភេទ'));
        $this->excel->getActiveSheet()->SetCellValue('F1', lang('ថ្ងៃខែឆ្នាំកំណើត'));
        $this->excel->getActiveSheet()->SetCellValue('G1', lang('សញ្ជាតិ'));
		$this->excel->getActiveSheet()->SetCellValue('H1', lang('សាសនា'));
		$this->excel->getActiveSheet()->SetCellValue('I1', lang('ជាតិពន្ធុ'));
        $this->excel->getActiveSheet()->SetCellValue('J1', lang('ទំនាក់'));
		$this->excel->getActiveSheet()->SetCellValue('K1', lang('អាសយដ្ឋាន'));	
		$this->excel->getActiveSheet()->SetCellValue('L1', lang('លេខទំនាក់ទំនង'));
		$this->excel->getActiveSheet()->SetCellValue('M1', lang('ទីកន្លែងកំណើត'));
		$this->excel->getActiveSheet()->SetCellValue('N1', lang('ថ្ងៃខែចុះបញ្ជី'));
		$results=$this->applications->getAsylumRegisterReports();
		$i=2;
		foreach($results as $row){
			$relationship = $this->applications->getRelationshipById($row->relationship);
			$country = $this->applications->getCountryById($row->country);
			$province = $this->applications->getProvinceById($row->province);
			$commune = $this->applications->getCommunceById($row->commune);						
			$district = $this->applications->getDistrictById($row->district);
				
			$relationship1_detail = lang("ម្ចាស់ករណី");
			if($relationship){
				$relationship1_detail = $relationship->relationship_kh;
			}
			$this->excel->getActiveSheet()->SetCellValue('A'.$i, $i-1);
			$this->excel->getActiveSheet()->SetCellValue('B'.$i, $row->case_prefix.$row->case_no);
			$this->excel->getActiveSheet()->SetCellValue('C'.$i, $row->firstname_kh." ".$row->lastname_kh);
			$this->excel->getActiveSheet()->SetCellValue('D'.$i, $row->firstname." ".$row->lastname);
			$this->excel->getActiveSheet()->SetCellValue('E'.$i, lang($row->gender));
			$this->excel->getActiveSheet()->SetCellValue('F'.$i, $this->erp->toKhmer($row->dob));
			$this->excel->getActiveSheet()->SetCellValue('G'.$i, $row->nationality_kh." / ".$row->nationality);
			$this->excel->getActiveSheet()->SetCellValue('H'.$i, $row->religion_kh." / ".$row->religion);
			$this->excel->getActiveSheet()->SetCellValue('I'.$i, $row->ethnicity_kh." / ".$row->religion);
			$this->excel->getActiveSheet()->SetCellValue('J'.$i, $relationship1_detail);
			$this->excel->getActiveSheet()->SetCellValue('K'.$i, $commune->native_name." ".$district->native_name." ".$province->native_name);			
			$this->excel->getActiveSheet()->SetCellValue('L'.$i, $this->erp->toKhmer($row->phone));
			$this->excel->getActiveSheet()->SetCellValue('M'.$i, $row->pob_kh." / ".$row->pob);
			$this->excel->getActiveSheet()->SetCellValue('N'.$i, $this->erp->toKhmer($this->erp->hrsd($row->created_date)));
			$this->excel->getActiveSheet()->getStyle('A'.$i.':O'.$i.'')->applyFromArray($styleArray);
			$i++;
		}
		
		
		$this->excel->getActiveSheet()->getStyle('A'.$i.':P'.$i)->applyFromArray($styleArray);
		$this->excel->getActiveSheet()->mergeCells('A'.$i.':P'.$i);
		$this->excel->getActiveSheet()->SetCellValue('A'.$i, "យោងតាមដំណរភ្ជាប់៖".$_SERVER['HTTP_REFERER']."  , ប្រព័ន្ធគ្រប់គ្រង៖ © 2018 អគ្គនាយកដ្ឋានអន្តោប្រវេសន៍ (v1.0 ) ");
		
        $this->excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
		
        $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);                
		
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$title.'.xls"');
        header('Cache-Control: max-age=0');                
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        $objWriter->save('php://output');exit();
 
	}
	
	public function all_register_report_export()
	{ 
	   $this->load->library('excel');
        
		$styleArray = array(
			'font'  => array(
				//'bold'  => true,
				//'color' => array('rgb' => 'FF0000'),
				'size'  => 8,
				'name'  => 'Khmer OS Siemreap',
			));		
		$this->excel->getActiveSheet()->getStyle('A1:O1')->applyFromArray($styleArray);
		$this->excel->getActiveSheet()->getRowDimension('1')->setRowHeight(20);		
		$this->excel->getActiveSheet()->getStyle('A1:O1')->getFont()->setBold(true)->setSize(10);
		$this->excel->getActiveSheet()->getStyle('A1:O1')->getFont()->getColor()->setRGB('FFFFFF'); 
		$this->excel->getActiveSheet()->getStyle('A1:O1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('A1:O1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('A1:O1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('555555');
			
		$this->excel->setActiveSheetIndex(0);
		$title = lang("របាយការណ៍រួម");		
        $this->excel->getActiveSheet()->setTitle($title);
        $this->excel->getActiveSheet()->SetCellValue('A1', lang('ល.រ'));
        $this->excel->getActiveSheet()->SetCellValue('B1', lang('លេខករណី'));
        $this->excel->getActiveSheet()->SetCellValue('C1', lang('ឈ្មោះ'));
        $this->excel->getActiveSheet()->SetCellValue('D1', lang('ឈ្មោះជាអក្សរទ្បាតាំង'));
        $this->excel->getActiveSheet()->SetCellValue('E1', lang('ភេទ'));
        $this->excel->getActiveSheet()->SetCellValue('F1', lang('ថ្ងៃខែឆ្នាំកំណើត'));
        $this->excel->getActiveSheet()->SetCellValue('G1', lang('សញ្ជាតិ'));
		$this->excel->getActiveSheet()->SetCellValue('H1', lang('សាសនា'));
		$this->excel->getActiveSheet()->SetCellValue('I1', lang('ជាតិពន្ធុ'));
        $this->excel->getActiveSheet()->SetCellValue('J1', lang('ទំនាក់'));
		$this->excel->getActiveSheet()->SetCellValue('K1', lang('អាសយដ្ឋាន'));	
		$this->excel->getActiveSheet()->SetCellValue('L1', lang('លេខទំនាក់ទំនង'));
		$this->excel->getActiveSheet()->SetCellValue('M1', lang('ទីកន្លែងកំណើត'));
		$this->excel->getActiveSheet()->SetCellValue('N1', lang('ថ្ងៃខែចុះបញ្ជី'));
		$this->excel->getActiveSheet()->SetCellValue('O1', lang('លំដាប់ដំណាក់កាលនីតិវិធី'));
		$results=$this->applications->getAllRegisterReports();
		$i=2;
		foreach($results as $row){
			$relationship = $this->applications->getRelationshipById($row->relationship);
			$country = $this->applications->getCountryById($row->country);
			$province = $this->applications->getProvinceById($row->province);
			$commune = $this->applications->getCommunceById($row->commune);						
			$district = $this->applications->getDistrictById($row->district);
				
			$relationship1_detail = lang("ម្ចាស់ករណី");
			if($relationship){
				$relationship1_detail = $relationship->relationship_kh;
			}
			$this->excel->getActiveSheet()->SetCellValue('A'.$i, $i-1);
			$this->excel->getActiveSheet()->SetCellValue('B'.$i, $row->case_prefix.$row->case_no);
			$this->excel->getActiveSheet()->SetCellValue('C'.$i, $row->firstname_kh." ".$row->lastname_kh);
			$this->excel->getActiveSheet()->SetCellValue('D'.$i, $row->firstname." ".$row->lastname);
			$this->excel->getActiveSheet()->SetCellValue('E'.$i, lang($row->gender));
			$this->excel->getActiveSheet()->SetCellValue('F'.$i, $this->erp->toKhmer($row->dob));
			$this->excel->getActiveSheet()->SetCellValue('G'.$i, $row->nationality_kh." / ".$row->nationality);
			$this->excel->getActiveSheet()->SetCellValue('H'.$i, $row->religion_kh." / ".$row->religion);
			$this->excel->getActiveSheet()->SetCellValue('I'.$i, $row->ethnicity_kh." / ".$row->ethnicity);
			$this->excel->getActiveSheet()->SetCellValue('J'.$i, $relationship1_detail);
			$this->excel->getActiveSheet()->SetCellValue('K'.$i, $commune->native_name." ".$district->native_name." ".$province->native_name);			
			$this->excel->getActiveSheet()->SetCellValue('L'.$i, $this->erp->toKhmer($row->phone));
			$this->excel->getActiveSheet()->SetCellValue('M'.$i, $row->pob_kh." / ".$row->pob);
			$this->excel->getActiveSheet()->SetCellValue('N'.$i, $this->erp->toKhmer($this->erp->hrsd($row->created_date)));
			$this->excel->getActiveSheet()->SetCellValue('O'.$i, lang($row->procedure_sequence));
			$this->excel->getActiveSheet()->getStyle('A'.$i.':O'.$i.'')->applyFromArray($styleArray);
			$i++;
		}
		$this->excel->getActiveSheet()->getStyle('A'.$i.':P'.$i)->applyFromArray($styleArray);
		$this->excel->getActiveSheet()->mergeCells('A'.$i.':P'.$i);
		$this->excel->getActiveSheet()->SetCellValue('A'.$i, "យោងតាមដំណរភ្ជាប់៖".$_SERVER['HTTP_REFERER']."  , ប្រព័ន្ធគ្រប់គ្រង៖ © 2018 អគ្គនាយកដ្ឋានអន្តោប្រវេសន៍ (v1.0 ) ");
		
        $this->excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);
		
        $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);                
		
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$title.'.xls"');
        header('Cache-Control: max-age=0');                
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        $objWriter->save('php://output');exit();


	
	}
	
	public function interview_appointment_export()
	{
	 
	   $this->load->library('excel');
        
		$styleArray = array(
			'font'  => array(
				//'bold'  => true,
				//'color' => array('rgb' => 'FF0000'),
				'size'  => 8,
				'name'  => 'Khmer OS Siemreap',
			));		
		$this->excel->getActiveSheet()->getStyle('A1:O1')->applyFromArray($styleArray);
		$this->excel->getActiveSheet()->getRowDimension('1')->setRowHeight(20);		
		$this->excel->getActiveSheet()->getStyle('A1:O1')->getFont()->setBold(true)->setSize(10);
		$this->excel->getActiveSheet()->getStyle('A1:O1')->getFont()->getColor()->setRGB('FFFFFF'); 
		$this->excel->getActiveSheet()->getStyle('A1:O1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('A1:O1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('A1:O1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('555555');
			
		$this->excel->setActiveSheetIndex(0);
		$title = lang("របាយការណ៍បុគ្គលបានណាត់សម្ភាសន៍");		
        $this->excel->getActiveSheet()->setTitle($title);
        $this->excel->getActiveSheet()->SetCellValue('A1', lang('ល.រ'));
        $this->excel->getActiveSheet()->SetCellValue('B1', lang('លេខករណី'));
        $this->excel->getActiveSheet()->SetCellValue('C1', lang('ឈ្មោះ'));
        $this->excel->getActiveSheet()->SetCellValue('D1', lang('ឈ្មោះជាអក្សរទ្បាតាំង'));
        $this->excel->getActiveSheet()->SetCellValue('E1', lang('ភេទ'));
        $this->excel->getActiveSheet()->SetCellValue('F1', lang('ថ្ងៃខែឆ្នាំកំណើត'));
        $this->excel->getActiveSheet()->SetCellValue('G1', lang('សញ្ជាតិ'));
		$this->excel->getActiveSheet()->SetCellValue('H1', lang('សាសនា'));
		$this->excel->getActiveSheet()->SetCellValue('I1', lang('ជាតិពន្ធុ')); 
		$this->excel->getActiveSheet()->SetCellValue('J1', lang('អាសយដ្ឋាន'));	
		$this->excel->getActiveSheet()->SetCellValue('K1', lang('លេខទំនាក់ទំនង'));
		$this->excel->getActiveSheet()->SetCellValue('L1', lang('ទីកន្លែងកំណើត'));
		$this->excel->getActiveSheet()->SetCellValue('M1', lang('កាលបរិច្ឆេទណាត់សម្ភាសន៏'));
		$this->excel->getActiveSheet()->SetCellValue('N1', lang('ថ្ងៃខែចុះបញ្ជី'));
		$results=$this->applications->getInteviewAppointmentReports();
		$i=2;
		foreach($results as $row){
			$relationship = $this->applications->getRelationshipById($row->relationship);
			$country = $this->applications->getCountryById($row->country);
			$province = $this->applications->getProvinceById($row->province);
			$commune = $this->applications->getCommunceById($row->commune);						
			$district = $this->applications->getDistrictById($row->district); 
			$this->excel->getActiveSheet()->SetCellValue('A'.$i, $i-1);
			$this->excel->getActiveSheet()->SetCellValue('B'.$i, $row->case_prefix.$row->case_no);
			$this->excel->getActiveSheet()->SetCellValue('C'.$i, $row->firstname_kh." ".$row->lastname_kh);
			$this->excel->getActiveSheet()->SetCellValue('D'.$i, $row->firstname." ".$row->lastname);
			$this->excel->getActiveSheet()->SetCellValue('E'.$i, lang($row->gender));
			$this->excel->getActiveSheet()->SetCellValue('F'.$i, $this->erp->toKhmer($row->dob));
			$this->excel->getActiveSheet()->SetCellValue('G'.$i, $row->nationality_kh." / ".$row->nationality);
			$this->excel->getActiveSheet()->SetCellValue('H'.$i, $row->religion_kh." / ".$row->religion);
			$this->excel->getActiveSheet()->SetCellValue('I'.$i, $row->ethnicity_kh." / ".$row->religion); 
			$this->excel->getActiveSheet()->SetCellValue('J'.$i, $commune->native_name." ".$district->native_name." ".$province->native_name);			
			$this->excel->getActiveSheet()->SetCellValue('K'.$i, $this->erp->toKhmer($row->phone));
			$this->excel->getActiveSheet()->SetCellValue('L'.$i, $row->pob_kh." / ".$row->pob);
			$this->excel->getActiveSheet()->SetCellValue('M'.$i,  $this->erp->toKhmer($this->erp->hrsd($row->appointment_date)));
			$this->excel->getActiveSheet()->SetCellValue('N'.$i, $this->erp->toKhmer($this->erp->hrsd($row->created_date)));
			$this->excel->getActiveSheet()->getStyle('A'.$i.':O'.$i.'')->applyFromArray($styleArray);
			$i++;
		}
		$this->excel->getActiveSheet()->getStyle('A'.$i.':P'.$i)->applyFromArray($styleArray);
		$this->excel->getActiveSheet()->mergeCells('A'.$i.':P'.$i);
		$this->excel->getActiveSheet()->SetCellValue('A'.$i, "យោងតាមដំណរភ្ជាប់៖".$_SERVER['HTTP_REFERER']."  , ប្រព័ន្ធគ្រប់គ្រង៖ © 2018 អគ្គនាយកដ្ឋានអន្តោប្រវេសន៍ (v1.0 ) ");
		
        $this->excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true); 
		$this->excel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true); 
		$this->excel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true); 
		
        $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);                
		
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$title.'.xls"');
        header('Cache-Control: max-age=0');                
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        $objWriter->save('php://output');exit();


	
	}
	
	public function interviewed_export()
	{
	   $this->load->library('excel');
        
		$styleArray = array(
			'font'  => array(
				//'bold'  => true,
				//'color' => array('rgb' => 'FF0000'),
				'size'  => 8,
				'name'  => 'Khmer OS Siemreap',
			));		
		$this->excel->getActiveSheet()->getStyle('A1:O1')->applyFromArray($styleArray);
		$this->excel->getActiveSheet()->getRowDimension('1')->setRowHeight(20);		
		$this->excel->getActiveSheet()->getStyle('A1:O1')->getFont()->setBold(true)->setSize(10);
		$this->excel->getActiveSheet()->getStyle('A1:O1')->getFont()->getColor()->setRGB('FFFFFF'); 
		$this->excel->getActiveSheet()->getStyle('A1:O1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('A1:O1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('A1:O1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('555555');
			
		$this->excel->setActiveSheetIndex(0);
		$title = lang("របាយការណ៍បុគ្គលសម្ភាសន៍រួច");		
        $this->excel->getActiveSheet()->setTitle($title);
        $this->excel->getActiveSheet()->SetCellValue('A1', lang('ល.រ'));
        $this->excel->getActiveSheet()->SetCellValue('B1', lang('លេខករណី'));
        $this->excel->getActiveSheet()->SetCellValue('C1', lang('ឈ្មោះ'));
        $this->excel->getActiveSheet()->SetCellValue('D1', lang('ឈ្មោះជាអក្សរទ្បាតាំង'));
        $this->excel->getActiveSheet()->SetCellValue('E1', lang('ភេទ'));
        $this->excel->getActiveSheet()->SetCellValue('F1', lang('ថ្ងៃខែឆ្នាំកំណើត'));
        $this->excel->getActiveSheet()->SetCellValue('G1', lang('សញ្ជាតិ'));
		$this->excel->getActiveSheet()->SetCellValue('H1', lang('សាសនា'));
		$this->excel->getActiveSheet()->SetCellValue('I1', lang('ជាតិពន្ធុ'));
        $this->excel->getActiveSheet()->SetCellValue('J1', lang('ទំនាក់'));
		$this->excel->getActiveSheet()->SetCellValue('K1', lang('អាសយដ្ឋាន'));	
		$this->excel->getActiveSheet()->SetCellValue('L1', lang('លេខទំនាក់ទំនង'));
		$this->excel->getActiveSheet()->SetCellValue('M1', lang('ទីកន្លែងកំណើត'));
		$this->excel->getActiveSheet()->SetCellValue('N1', lang('ថ្ងៃខែចុះបញ្ជី'));
		$this->excel->getActiveSheet()->SetCellValue('O1', lang('កាលបរិចេ្ឆទសម្ភាសន៍'));
		$results=$this->applications->getInteviewedReports();
		$i=2;
		foreach($results as $row){
			$relationship = $this->applications->getRelationshipById($row->relationship);
			$country = $this->applications->getCountryById($row->country);
			$province = $this->applications->getProvinceById($row->province);
			$commune = $this->applications->getCommunceById($row->commune);						
			$district = $this->applications->getDistrictById($row->district);
			$intervie_appeal = '';
			if($row->negative_appeal_id > 0) {
				$intervie_appeal = lang("លើបណ្តឹងឧទ្ធរណ៍");
			}
			$relationship1_detail = lang("ម្ចាស់ករណី");
			if($relationship){
				$relationship1_detail = $relationship->relationship_kh;
			}
			$this->excel->getActiveSheet()->SetCellValue('A'.$i, $i-1);
			$this->excel->getActiveSheet()->SetCellValue('B'.$i, $row->case_prefix.$row->case_no);
			$this->excel->getActiveSheet()->SetCellValue('C'.$i, $row->firstname_kh." ".$row->lastname_kh);
			$this->excel->getActiveSheet()->SetCellValue('D'.$i, $row->firstname." ".$row->lastname);
			$this->excel->getActiveSheet()->SetCellValue('E'.$i, lang($row->gender));
			$this->excel->getActiveSheet()->SetCellValue('F'.$i, $this->erp->toKhmer($row->dob));
			$this->excel->getActiveSheet()->SetCellValue('G'.$i, $row->nationality_kh." / ".$row->nationality);
			$this->excel->getActiveSheet()->SetCellValue('H'.$i, $row->religion_kh." / ".$row->religion);
			$this->excel->getActiveSheet()->SetCellValue('I'.$i, $row->ethnicity_kh." / ".$row->religion);
			$this->excel->getActiveSheet()->SetCellValue('J'.$i, $relationship1_detail);
			$this->excel->getActiveSheet()->SetCellValue('K'.$i, $commune->native_name." ".$district->native_name." ".$province->native_name);			
			$this->excel->getActiveSheet()->SetCellValue('L'.$i, $this->erp->toKhmer($row->phone));
			$this->excel->getActiveSheet()->SetCellValue('M'.$i, $row->pob_kh." / ".$row->pob);
			$this->excel->getActiveSheet()->SetCellValue('N'.$i, $this->erp->toKhmer($this->erp->hrsd($row->created_date)));
			$this->excel->getActiveSheet()->SetCellValue('O'.$i, $this->erp->toKhmer($this->erp->hrsd($row->interview_date)));
			
			$this->excel->getActiveSheet()->getStyle('A'.$i.':O'.$i.'')->applyFromArray($styleArray);
			$i++;
		}
		$this->excel->getActiveSheet()->getStyle('A'.$i.':P'.$i)->applyFromArray($styleArray);
		$this->excel->getActiveSheet()->mergeCells('A'.$i.':P'.$i);
		$this->excel->getActiveSheet()->SetCellValue('A'.$i, "យោងតាមដំណរភ្ជាប់៖".$_SERVER['HTTP_REFERER']."  , ប្រព័ន្ធគ្រប់គ្រង៖ © 2018 អគ្គនាយកដ្ឋានអន្តោប្រវេសន៍ (v1.0 ) ");
		
        $this->excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);
		
        $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);                
		
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$title.'.xls"');
        header('Cache-Control: max-age=0');                
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        $objWriter->save('php://output');exit();


	
	}
	
	public function interview_evaluate_export()
	{
	   $this->load->library('excel');
        
		$styleArray = array(
			'font'  => array(
				//'bold'  => true,
				//'color' => array('rgb' => 'FF0000'),
				'size'  => 8,
				'name'  => 'Khmer OS Siemreap',
			));		
		$this->excel->getActiveSheet()->getStyle('A1:P1')->applyFromArray($styleArray);
		$this->excel->getActiveSheet()->getRowDimension('1')->setRowHeight(20);		
		$this->excel->getActiveSheet()->getStyle('A1:P1')->getFont()->setBold(true)->setSize(10);
		$this->excel->getActiveSheet()->getStyle('A1:P1')->getFont()->getColor()->setRGB('FFFFFF'); 
		$this->excel->getActiveSheet()->getStyle('A1:P1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('A1:P1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('A1:P1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('555555');
			
		$this->excel->setActiveSheetIndex(0);
		$title = lang("របាយការណ៍វិនិច្ឆ័យលើការសម្ភាសន៍");		
        $this->excel->getActiveSheet()->setTitle($title);
        $this->excel->getActiveSheet()->SetCellValue('A1', lang('ល.រ'));
        $this->excel->getActiveSheet()->SetCellValue('B1', lang('លេខករណី'));
        $this->excel->getActiveSheet()->SetCellValue('C1', lang('ឈ្មោះ'));
        $this->excel->getActiveSheet()->SetCellValue('D1', lang('ឈ្មោះជាអក្សរទ្បាតាំង'));
        $this->excel->getActiveSheet()->SetCellValue('E1', lang('ភេទ'));
        $this->excel->getActiveSheet()->SetCellValue('F1', lang('ថ្ងៃខែឆ្នាំកំណើត'));
        $this->excel->getActiveSheet()->SetCellValue('G1', lang('សញ្ជាតិ'));
		$this->excel->getActiveSheet()->SetCellValue('H1', lang('សាសនា'));
		$this->excel->getActiveSheet()->SetCellValue('I1', lang('ជាតិពន្ធុ'));
        $this->excel->getActiveSheet()->SetCellValue('J1', lang('ទំនាក់'));
		$this->excel->getActiveSheet()->SetCellValue('K1', lang('អាសយដ្ឋាន'));	
		$this->excel->getActiveSheet()->SetCellValue('L1', lang('លេខទំនាក់ទំនង'));
		$this->excel->getActiveSheet()->SetCellValue('M1', lang('ទីកន្លែងកំណើត'));
		$this->excel->getActiveSheet()->SetCellValue('N1', lang('ថ្ងៃខែចុះបញ្ជី'));
		$this->excel->getActiveSheet()->SetCellValue('O1', lang('ថ្ងៃខែការសម្រេច'));
		$this->excel->getActiveSheet()->SetCellValue('P1', lang('ការសម្រេច')); 
		$results=$this->applications->getInteviewedEvaluateReports();
		$i=2;
		foreach($results as $row){
			$relationship = $this->applications->getRelationshipById($row->relationship);
			$country = $this->applications->getCountryById($row->country);
			$province = $this->applications->getProvinceById($row->province);
			$commune = $this->applications->getCommunceById($row->commune);						
			$district = $this->applications->getDistrictById($row->district);
			$intervie_appeal = '';
			if($row->negative_appeal_id > 0) {
				$intervie_appeal = lang("លើបណ្តឹងឧទ្ធរណ៍");
			}
			$relationship1_detail = lang("ម្ចាស់ករណី");
			if($relationship){
				$relationship1_detail = $relationship->relationship_kh;
			}
			$this->excel->getActiveSheet()->SetCellValue('A'.$i, $i-1);
			$this->excel->getActiveSheet()->SetCellValue('B'.$i, $row->case_prefix.$row->case_no);
			$this->excel->getActiveSheet()->SetCellValue('C'.$i, $row->firstname_kh." ".$row->lastname_kh);
			$this->excel->getActiveSheet()->SetCellValue('D'.$i, $row->firstname." ".$row->lastname);
			$this->excel->getActiveSheet()->SetCellValue('E'.$i, lang($row->gender));
			$this->excel->getActiveSheet()->SetCellValue('F'.$i, $this->erp->toKhmer($row->dob));
			$this->excel->getActiveSheet()->SetCellValue('G'.$i, $row->nationality_kh." / ".$row->nationality);
			$this->excel->getActiveSheet()->SetCellValue('H'.$i, $row->religion_kh." / ".$row->religion);
			$this->excel->getActiveSheet()->SetCellValue('I'.$i, $row->ethnicity_kh." / ".$row->religion);
			$this->excel->getActiveSheet()->SetCellValue('J'.$i, $relationship1_detail);
			$this->excel->getActiveSheet()->SetCellValue('K'.$i, $commune->native_name." ".$district->native_name." ".$province->native_name);			
			$this->excel->getActiveSheet()->SetCellValue('L'.$i, $this->erp->toKhmer($row->phone));
			$this->excel->getActiveSheet()->SetCellValue('M'.$i, $row->pob_kh." / ".$row->pob);
			$this->excel->getActiveSheet()->SetCellValue('N'.$i, $this->erp->toKhmer($this->erp->hrsd($row->created_date)));
			$this->excel->getActiveSheet()->SetCellValue('O'.$i, $this->erp->toKhmer($this->erp->hrsd($row->status_date)));
			$this->excel->getActiveSheet()->SetCellValue('P'.$i, lang($row->status)); 
			
			$this->excel->getActiveSheet()->getStyle('A'.$i.':P'.$i.'')->applyFromArray($styleArray);
			$i++;
		}
		$this->excel->getActiveSheet()->getStyle('A'.$i.':P'.$i)->applyFromArray($styleArray);
		$this->excel->getActiveSheet()->mergeCells('A'.$i.':P'.$i);
		$this->excel->getActiveSheet()->SetCellValue('A'.$i, "យោងតាមដំណរភ្ជាប់៖".$_SERVER['HTTP_REFERER']."  , ប្រព័ន្ធគ្រប់គ្រង៖ © 2018 អគ្គនាយកដ្ឋានអន្តោប្រវេសន៍ (v1.0 ) ");
		
        $this->excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('p')->setAutoSize(true);
		
        $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);                
		
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$title.'.xls"');
        header('Cache-Control: max-age=0');                
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        $objWriter->save('php://output');exit();


	
	}
	
	public function interview_negative_appeal_export()
	{
	   $this->load->library('excel');
        
		$styleArray = array(
			'font'  => array(
				//'bold'  => true,
				//'color' => array('rgb' => 'FF0000'),
				'size'  => 8,
				'name'  => 'Khmer OS Siemreap',
			));		
		$this->excel->getActiveSheet()->getStyle('A1:P1')->applyFromArray($styleArray);
		$this->excel->getActiveSheet()->getRowDimension('1')->setRowHeight(20);		
		$this->excel->getActiveSheet()->getStyle('A1:P1')->getFont()->setBold(true)->setSize(10);
		$this->excel->getActiveSheet()->getStyle('A1:P1')->getFont()->getColor()->setRGB('FFFFFF'); 
		$this->excel->getActiveSheet()->getStyle('A1:P1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('A1:P1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('A1:P1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('555555');
			
		$this->excel->setActiveSheetIndex(0);
		$title = lang("ដាក់ពាក្យស្នើសុំលើបណ្តឹងឧទ្ធរណ៍");	
        $this->excel->getActiveSheet()->setTitle($title);
        $this->excel->getActiveSheet()->SetCellValue('A1', lang('ល.រ'));
        $this->excel->getActiveSheet()->SetCellValue('B1', lang('លេខករណី'));
        $this->excel->getActiveSheet()->SetCellValue('C1', lang('ឈ្មោះ'));
        $this->excel->getActiveSheet()->SetCellValue('D1', lang('ឈ្មោះជាអក្សរទ្បាតាំង'));
        $this->excel->getActiveSheet()->SetCellValue('E1', lang('ភេទ'));
        $this->excel->getActiveSheet()->SetCellValue('F1', lang('ថ្ងៃខែឆ្នាំកំណើត'));
        $this->excel->getActiveSheet()->SetCellValue('G1', lang('សញ្ជាតិ'));
		$this->excel->getActiveSheet()->SetCellValue('H1', lang('សាសនា'));
		$this->excel->getActiveSheet()->SetCellValue('I1', lang('ជាតិពន្ធុ'));
        $this->excel->getActiveSheet()->SetCellValue('J1', lang('ទំនាក់'));
		$this->excel->getActiveSheet()->SetCellValue('K1', lang('អាសយដ្ឋាន'));	
		$this->excel->getActiveSheet()->SetCellValue('L1', lang('លេខទំនាក់ទំនង'));
		$this->excel->getActiveSheet()->SetCellValue('M1', lang('ទីកន្លែងកំណើត'));
		$this->excel->getActiveSheet()->SetCellValue('N1', lang('ថ្ងៃខែចុះបញ្ជី'));
		$this->excel->getActiveSheet()->SetCellValue('O1', lang('ការសម្រេច')); 
		$results=$this->applications->getInterviewNegativeAppealReports();
		$i=2;
		foreach($results as $row){
			$relationship = $this->applications->getRelationshipById($row->relationship);
			$country = $this->applications->getCountryById($row->country);
			$province = $this->applications->getProvinceById($row->province);
			$commune = $this->applications->getCommunceById($row->commune);						
			$district = $this->applications->getDistrictById($row->district); 
			$relationship1_detail = lang("ម្ចាស់ករណី");
			if($relationship){
				$relationship1_detail = $relationship->relationship_kh;
			}
			$this->excel->getActiveSheet()->SetCellValue('A'.$i, $i-1);
			$this->excel->getActiveSheet()->SetCellValue('B'.$i, $row->case_prefix.$row->case_no);
			$this->excel->getActiveSheet()->SetCellValue('C'.$i, $row->firstname_kh." ".$row->lastname_kh);
			$this->excel->getActiveSheet()->SetCellValue('D'.$i, $row->firstname." ".$row->lastname);
			$this->excel->getActiveSheet()->SetCellValue('E'.$i, lang($row->gender));
			$this->excel->getActiveSheet()->SetCellValue('F'.$i, $this->erp->toKhmer($row->dob));
			$this->excel->getActiveSheet()->SetCellValue('G'.$i, $row->nationality_kh." / ".$row->nationality);
			$this->excel->getActiveSheet()->SetCellValue('H'.$i, $row->religion_kh." / ".$row->religion);
			$this->excel->getActiveSheet()->SetCellValue('I'.$i, $row->ethnicity_kh." / ".$row->religion);
			$this->excel->getActiveSheet()->SetCellValue('J'.$i, $relationship1_detail);
			$this->excel->getActiveSheet()->SetCellValue('K'.$i, $commune->native_name." ".$district->native_name." ".$province->native_name);			
			$this->excel->getActiveSheet()->SetCellValue('L'.$i, $this->erp->toKhmer($row->phone));
			$this->excel->getActiveSheet()->SetCellValue('M'.$i, $row->pob_kh." / ".$row->pob);
			$this->excel->getActiveSheet()->SetCellValue('N'.$i, $this->erp->toKhmer($this->erp->hrsd($row->created_date))); 
			$this->excel->getActiveSheet()->SetCellValue('O'.$i, lang($row->status));
			
			$this->excel->getActiveSheet()->getStyle('A'.$i.':P'.$i.'')->applyFromArray($styleArray);
			$i++;
		}
		
		$this->excel->getActiveSheet()->getStyle('A'.$i.':P'.$i)->applyFromArray($styleArray);
		$this->excel->getActiveSheet()->mergeCells('A'.$i.':P'.$i);
		$this->excel->getActiveSheet()->SetCellValue('A'.$i, "យោងតាមដំណរភ្ជាប់៖".$_SERVER['HTTP_REFERER']."  , ប្រព័ន្ធគ្រប់គ្រង៖ © 2018 អគ្គនាយកដ្ឋានអន្តោប្រវេសន៍ (v1.0 ) ");
		
        $this->excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true); 
		
        $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);                
		
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$title.'.xls"');
        header('Cache-Control: max-age=0');                
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        $objWriter->save('php://output');exit();


	
	}

	public function interview_request_on_appeal_export()
	{
	   $this->load->library('excel');
        
		$styleArray = array(
			'font'  => array(
				//'bold'  => true,
				//'color' => array('rgb' => 'FF0000'),
				'size'  => 8,
				'name'  => 'Khmer OS Siemreap',
			));		
		$this->excel->getActiveSheet()->getStyle('A1:P1')->applyFromArray($styleArray);
		$this->excel->getActiveSheet()->getRowDimension('1')->setRowHeight(20);		
		$this->excel->getActiveSheet()->getStyle('A1:P1')->getFont()->setBold(true)->setSize(10);
		$this->excel->getActiveSheet()->getStyle('A1:P1')->getFont()->getColor()->setRGB('FFFFFF'); 
		$this->excel->getActiveSheet()->getStyle('A1:P1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('A1:P1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('A1:P1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('555555');
			
		$this->excel->setActiveSheetIndex(0);
		$title = lang("ដាក់ពាក្យស្នើសុំលើបណ្តឹងឧទ្ធរណ៍");	
        $this->excel->getActiveSheet()->setTitle($title);
        $this->excel->getActiveSheet()->SetCellValue('A1', lang('ល.រ'));
        $this->excel->getActiveSheet()->SetCellValue('B1', lang('លេខករណី'));
        $this->excel->getActiveSheet()->SetCellValue('C1', lang('ឈ្មោះ'));
        $this->excel->getActiveSheet()->SetCellValue('D1', lang('ឈ្មោះជាអក្សរទ្បាតាំង'));
        $this->excel->getActiveSheet()->SetCellValue('E1', lang('ភេទ'));
        $this->excel->getActiveSheet()->SetCellValue('F1', lang('ថ្ងៃខែឆ្នាំកំណើត'));
        $this->excel->getActiveSheet()->SetCellValue('G1', lang('សញ្ជាតិ'));
		$this->excel->getActiveSheet()->SetCellValue('H1', lang('សាសនា'));
		$this->excel->getActiveSheet()->SetCellValue('I1', lang('ជាតិពន្ធុ'));
        $this->excel->getActiveSheet()->SetCellValue('J1', lang('ទំនាក់'));
		$this->excel->getActiveSheet()->SetCellValue('K1', lang('អាសយដ្ឋាន'));	
		$this->excel->getActiveSheet()->SetCellValue('L1', lang('លេខទំនាក់ទំនង'));
		$this->excel->getActiveSheet()->SetCellValue('M1', lang('ទីកន្លែងកំណើត'));
		$this->excel->getActiveSheet()->SetCellValue('N1', lang('ថ្ងៃខែចុះបញ្ជី')); 
		$this->excel->getActiveSheet()->SetCellValue('O1', lang('ថ្ងៃខែការសម្រេច'));
		$this->excel->getActiveSheet()->SetCellValue('P1', lang('ការសម្រេច')); 
		$results=$this->applications->getInterviewRequestOnAppealReports();
		$i=2;
		foreach($results as $row){
			$relationship = $this->applications->getRelationshipById($row->relationship);
			$country = $this->applications->getCountryById($row->country);
			$province = $this->applications->getProvinceById($row->province);
			$commune = $this->applications->getCommunceById($row->commune);						
			$district = $this->applications->getDistrictById($row->district); 
			$relationship1_detail = lang("ម្ចាស់ករណី");
			if($relationship){
				$relationship1_detail = $relationship->relationship_kh;
			}
			$this->excel->getActiveSheet()->SetCellValue('A'.$i, $i-1);
			$this->excel->getActiveSheet()->SetCellValue('B'.$i, $row->case_prefix.$row->case_no);
			$this->excel->getActiveSheet()->SetCellValue('C'.$i, $row->firstname_kh." ".$row->lastname_kh);
			$this->excel->getActiveSheet()->SetCellValue('D'.$i, $row->firstname." ".$row->lastname);
			$this->excel->getActiveSheet()->SetCellValue('E'.$i, lang($row->gender));
			$this->excel->getActiveSheet()->SetCellValue('F'.$i, $this->erp->toKhmer($row->dob));
			$this->excel->getActiveSheet()->SetCellValue('G'.$i, $row->nationality_kh." / ".$row->nationality);
			$this->excel->getActiveSheet()->SetCellValue('H'.$i, $row->religion_kh." / ".$row->religion);
			$this->excel->getActiveSheet()->SetCellValue('I'.$i, $row->ethnicity_kh." / ".$row->religion);
			$this->excel->getActiveSheet()->SetCellValue('J'.$i, $relationship1_detail);
			$this->excel->getActiveSheet()->SetCellValue('K'.$i, $commune->native_name." ".$district->native_name." ".$province->native_name);			
			$this->excel->getActiveSheet()->SetCellValue('L'.$i, $this->erp->toKhmer($row->phone));
			$this->excel->getActiveSheet()->SetCellValue('M'.$i, $row->pob_kh." / ".$row->pob);
			$this->excel->getActiveSheet()->SetCellValue('N'.$i, $this->erp->toKhmer($this->erp->hrsd($row->created_date))); 
			$this->excel->getActiveSheet()->SetCellValue('O'.$i, $this->erp->toKhmer($this->erp->hrsd($row->status_date)));
			$this->excel->getActiveSheet()->SetCellValue('P'.$i, lang($row->status));
			$this->excel->getActiveSheet()->getStyle('A'.$i.':P'.$i.'')->applyFromArray($styleArray);
			$i++;
		}
		$this->excel->getActiveSheet()->getStyle('A'.$i.':P'.$i)->applyFromArray($styleArray);
		$this->excel->getActiveSheet()->mergeCells('A'.$i.':P'.$i);
		$this->excel->getActiveSheet()->SetCellValue('A'.$i, "យោងតាមដំណរភ្ជាប់៖".$_SERVER['HTTP_REFERER']."  , ប្រព័ន្ធគ្រប់គ្រង៖ © 2018 អគ្គនាយកដ្ឋានអន្តោប្រវេសន៍ (v1.0 ) ");
		
        $this->excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true); 
		
        $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);                
		
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$title.'.xls"');
        header('Cache-Control: max-age=0');                
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        $objWriter->save('php://output');exit();


	
	}

	public function interview_on_appeal_export()
	{
	   $this->load->library('excel');
        
		$styleArray = array(
			'font'  => array(
				//'bold'  => true,
				//'color' => array('rgb' => 'FF0000'),
				'size'  => 8,
				'name'  => 'Khmer OS Siemreap',
			));		
		$this->excel->getActiveSheet()->getStyle('A1:P1')->applyFromArray($styleArray);
		$this->excel->getActiveSheet()->getRowDimension('1')->setRowHeight(20);		
		$this->excel->getActiveSheet()->getStyle('A1:P1')->getFont()->setBold(true)->setSize(10);
		$this->excel->getActiveSheet()->getStyle('A1:P1')->getFont()->getColor()->setRGB('FFFFFF'); 
		$this->excel->getActiveSheet()->getStyle('A1:P1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('A1:P1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('A1:P1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('555555');
			
		$this->excel->setActiveSheetIndex(0);
		$title = lang("សម្ភាសន៍លើបណ្តឹងឧទ្ធរណ៍");	
        $this->excel->getActiveSheet()->setTitle($title);
        $this->excel->getActiveSheet()->SetCellValue('A1', lang('ល.រ'));
        $this->excel->getActiveSheet()->SetCellValue('B1', lang('លេខករណី'));
        $this->excel->getActiveSheet()->SetCellValue('C1', lang('ឈ្មោះ'));
        $this->excel->getActiveSheet()->SetCellValue('D1', lang('ឈ្មោះជាអក្សរទ្បាតាំង'));
        $this->excel->getActiveSheet()->SetCellValue('E1', lang('ភេទ'));
        $this->excel->getActiveSheet()->SetCellValue('F1', lang('ថ្ងៃខែឆ្នាំកំណើត'));
        $this->excel->getActiveSheet()->SetCellValue('G1', lang('សញ្ជាតិ'));
		$this->excel->getActiveSheet()->SetCellValue('H1', lang('សាសនា'));
		$this->excel->getActiveSheet()->SetCellValue('I1', lang('ជាតិពន្ធុ'));
        $this->excel->getActiveSheet()->SetCellValue('J1', lang('ទំនាក់'));
		$this->excel->getActiveSheet()->SetCellValue('K1', lang('អាសយដ្ឋាន'));	
		$this->excel->getActiveSheet()->SetCellValue('L1', lang('លេខទំនាក់ទំនង'));
		$this->excel->getActiveSheet()->SetCellValue('M1', lang('ទីកន្លែងកំណើត'));
		$this->excel->getActiveSheet()->SetCellValue('N1', lang('ថ្ងៃខែចុះបញ្ជី'));
		$this->excel->getActiveSheet()->SetCellValue('O1', lang('កាលបរិច្ឆេទសម្ភាសន៍'));
		$results=$this->applications->getInteviewOnAppealReports();
		$i=2;
		foreach($results as $row){
			$relationship = $this->applications->getRelationshipById($row->relationship);
			$country = $this->applications->getCountryById($row->country);
			$province = $this->applications->getProvinceById($row->province);
			$commune = $this->applications->getCommunceById($row->commune);						
			$district = $this->applications->getDistrictById($row->district); 
			$relationship1_detail = lang("ម្ចាស់ករណី");
			if($relationship){
				$relationship1_detail = $relationship->relationship_kh;
			}
			$this->excel->getActiveSheet()->SetCellValue('A'.$i, $i-1);
			$this->excel->getActiveSheet()->SetCellValue('B'.$i, $row->case_prefix.$row->case_no);
			$this->excel->getActiveSheet()->SetCellValue('C'.$i, $row->firstname_kh." ".$row->lastname_kh);
			$this->excel->getActiveSheet()->SetCellValue('D'.$i, $row->firstname." ".$row->lastname);
			$this->excel->getActiveSheet()->SetCellValue('E'.$i, lang($row->gender));
			$this->excel->getActiveSheet()->SetCellValue('F'.$i, $this->erp->toKhmer($row->dob));
			$this->excel->getActiveSheet()->SetCellValue('G'.$i, $row->nationality_kh." / ".$row->nationality);
			$this->excel->getActiveSheet()->SetCellValue('H'.$i, $row->religion_kh." / ".$row->religion);
			$this->excel->getActiveSheet()->SetCellValue('I'.$i, $row->ethnicity_kh." / ".$row->religion);
			$this->excel->getActiveSheet()->SetCellValue('J'.$i, $relationship1_detail);
			$this->excel->getActiveSheet()->SetCellValue('K'.$i, $commune->native_name." ".$district->native_name." ".$province->native_name);			
			$this->excel->getActiveSheet()->SetCellValue('L'.$i, $this->erp->toKhmer($row->phone));
			$this->excel->getActiveSheet()->SetCellValue('M'.$i, $row->pob_kh." / ".$row->pob);
			$this->excel->getActiveSheet()->SetCellValue('N'.$i, $this->erp->toKhmer($this->erp->hrsd($row->created_date))); 
			$this->excel->getActiveSheet()->SetCellValue('O'.$i, $this->erp->toKhmer($this->erp->hrsd($row->interview_date))); 
			
			$this->excel->getActiveSheet()->getStyle('A'.$i.':P'.$i.'')->applyFromArray($styleArray);
			$i++;
		}
		$this->excel->getActiveSheet()->getStyle('A'.$i.':P'.$i)->applyFromArray($styleArray);
		$this->excel->getActiveSheet()->mergeCells('A'.$i.':P'.$i);
		$this->excel->getActiveSheet()->SetCellValue('A'.$i, "យោងតាមដំណរភ្ជាប់៖".$_SERVER['HTTP_REFERER']."  , ប្រព័ន្ធគ្រប់គ្រង៖ © 2018 អគ្គនាយកដ្ឋានអន្តោប្រវេសន៍ (v1.0 ) ");
		
        $this->excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true); 
		
        $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);                
		
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$title.'.xls"');
        header('Cache-Control: max-age=0');                
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        $objWriter->save('php://output');exit();


	
	}

	public function interview_evaluate_on_appeal_export()
	{
	   $this->load->library('excel');
        
		$styleArray = array(
			'font'  => array(
				//'bold'  => true,
				//'color' => array('rgb' => 'FF0000'),
				'size'  => 8,
				'name'  => 'Khmer OS Siemreap',
			));		
		$this->excel->getActiveSheet()->getStyle('A1:P1')->applyFromArray($styleArray);
		$this->excel->getActiveSheet()->getRowDimension('1')->setRowHeight(20);		
		$this->excel->getActiveSheet()->getStyle('A1:P1')->getFont()->setBold(true)->setSize(10);
		$this->excel->getActiveSheet()->getStyle('A1:P1')->getFont()->getColor()->setRGB('FFFFFF'); 
		$this->excel->getActiveSheet()->getStyle('A1:P1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('A1:P1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('A1:P1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('555555');
			
		$this->excel->setActiveSheetIndex(0);
		$title = lang("វិនិច័្ឆយលើបណ្តឹងឧទ្ធរណ៍");	
        $this->excel->getActiveSheet()->setTitle($title);
        $this->excel->getActiveSheet()->SetCellValue('A1', lang('ល.រ'));
        $this->excel->getActiveSheet()->SetCellValue('B1', lang('លេខករណី'));
        $this->excel->getActiveSheet()->SetCellValue('C1', lang('ឈ្មោះ'));
        $this->excel->getActiveSheet()->SetCellValue('D1', lang('ឈ្មោះជាអក្សរទ្បាតាំង'));
        $this->excel->getActiveSheet()->SetCellValue('E1', lang('ភេទ'));
        $this->excel->getActiveSheet()->SetCellValue('F1', lang('ថ្ងៃខែឆ្នាំកំណើត'));
        $this->excel->getActiveSheet()->SetCellValue('G1', lang('សញ្ជាតិ'));
		$this->excel->getActiveSheet()->SetCellValue('H1', lang('សាសនា'));
		$this->excel->getActiveSheet()->SetCellValue('I1', lang('ជាតិពន្ធុ'));
        $this->excel->getActiveSheet()->SetCellValue('J1', lang('ទំនាក់'));
		$this->excel->getActiveSheet()->SetCellValue('K1', lang('អាសយដ្ឋាន'));	
		$this->excel->getActiveSheet()->SetCellValue('L1', lang('លេខទំនាក់ទំនង'));
		$this->excel->getActiveSheet()->SetCellValue('M1', lang('ទីកន្លែងកំណើត'));
		$this->excel->getActiveSheet()->SetCellValue('N1', lang('ថ្ងៃខែចុះបញ្ជី'));
		$this->excel->getActiveSheet()->SetCellValue('O1', lang('ថ្ងៃខែការសម្រេច'));
		$this->excel->getActiveSheet()->SetCellValue('P1', lang('ការសម្រេច'));
		$results=$this->applications->getInteviewedEvaluateOnAppealReports();
		$i=2;
		foreach($results as $row){
			$relationship = $this->applications->getRelationshipById($row->relationship);
			$country = $this->applications->getCountryById($row->country);
			$province = $this->applications->getProvinceById($row->province);
			$commune = $this->applications->getCommunceById($row->commune);						
			$district = $this->applications->getDistrictById($row->district); 
			$relationship1_detail = lang("ម្ចាស់ករណី");
			if($relationship){
				$relationship1_detail = $relationship->relationship_kh;
			}
			$this->excel->getActiveSheet()->SetCellValue('A'.$i, $i-1);
			$this->excel->getActiveSheet()->SetCellValue('B'.$i, $row->case_prefix.$row->case_no);
			$this->excel->getActiveSheet()->SetCellValue('C'.$i, $row->firstname_kh." ".$row->lastname_kh);
			$this->excel->getActiveSheet()->SetCellValue('D'.$i, $row->firstname." ".$row->lastname);
			$this->excel->getActiveSheet()->SetCellValue('E'.$i, lang($row->gender));
			$this->excel->getActiveSheet()->SetCellValue('F'.$i, $this->erp->toKhmer($row->dob));
			$this->excel->getActiveSheet()->SetCellValue('G'.$i, $row->nationality_kh." / ".$row->nationality);
			$this->excel->getActiveSheet()->SetCellValue('H'.$i, $row->religion_kh." / ".$row->religion);
			$this->excel->getActiveSheet()->SetCellValue('I'.$i, $row->ethnicity_kh." / ".$row->religion);
			$this->excel->getActiveSheet()->SetCellValue('J'.$i, $relationship1_detail);
			$this->excel->getActiveSheet()->SetCellValue('K'.$i, $commune->native_name." ".$district->native_name." ".$province->native_name);			
			$this->excel->getActiveSheet()->SetCellValue('L'.$i, $this->erp->toKhmer($row->phone));
			$this->excel->getActiveSheet()->SetCellValue('M'.$i, $row->pob_kh." / ".$row->pob);
			$this->excel->getActiveSheet()->SetCellValue('N'.$i, $this->erp->toKhmer($this->erp->hrsd($row->created_date))); 
			$this->excel->getActiveSheet()->SetCellValue('O'.$i, $this->erp->toKhmer($this->erp->hrsd($row->status_date))); 
			$this->excel->getActiveSheet()->SetCellValue('P'.$i, lang($row->status)); 
			
			$this->excel->getActiveSheet()->getStyle('A'.$i.':P'.$i.'')->applyFromArray($styleArray);
			$i++;
		}
		$this->excel->getActiveSheet()->getStyle('A'.$i.':P'.$i)->applyFromArray($styleArray);
		$this->excel->getActiveSheet()->mergeCells('A'.$i.':P'.$i);
		$this->excel->getActiveSheet()->SetCellValue('A'.$i, "យោងតាមដំណរភ្ជាប់៖".$_SERVER['HTTP_REFERER']."  , ប្រព័ន្ធគ្រប់គ្រង៖ © 2018 អគ្គនាយកដ្ឋានអន្តោប្រវេសន៍ (v1.0 ) ");
		
        $this->excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true); 
		
        $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);                
		
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$title.'.xls"');
        header('Cache-Control: max-age=0');                
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        $objWriter->save('php://output');exit();


	
	}

	/*=============General Report===================*/
	public function general_report_print()
	{	
		$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('general_report_print')));
		$meta = array('page_title' => lang('general_report_print'), 'bc' => $bc);	
		$this->page_construct('application_reports/general_report_print',$meta, $this->data);	
	} 
	
	public function general_report()
	{		 
		if(isset($_POST['export'])){ 
			$this->general_report_export(); 			 
		}else if(isset($_POST['print'])){ 
			$this->general_report_print(); 			 
		}else{ 
			$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('general_report')));
			$meta = array('page_title' => lang('general_report'), 'bc' => $bc);	
			$this->page_construct('application_reports/general_report',$meta, $this->data);	
		}
	}
	
	public function general_report_export()
	{
	 
	   $this->load->library('excel');
        
		$styleArray = array(
			'font'  => array(
				//'bold'  => true,
				//'color' => array('rgb' => 'FF0000'),
				'size'  => 8,
				'name'  => 'Khmer OS Siemreap',
			));		
		$this->excel->getActiveSheet()->getStyle('A1:O1')->applyFromArray($styleArray);
		$this->excel->getActiveSheet()->getRowDimension('1')->setRowHeight(20);		
		$this->excel->getActiveSheet()->getStyle('A1:O1')->getFont()->setBold(true)->setSize(10);
		$this->excel->getActiveSheet()->getStyle('A1:O1')->getFont()->getColor()->setRGB('FFFFFF'); 
		$this->excel->getActiveSheet()->getStyle('A1:O1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('A1:O1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('A1:O1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('555555');
			
		$this->excel->setActiveSheetIndex(0);
		$title = lang("របាយការណ៍ទូទៅ");		
        $this->excel->getActiveSheet()->setTitle($title);
        $this->excel->getActiveSheet()->SetCellValue('A1', lang('ល.រ'));
        $this->excel->getActiveSheet()->SetCellValue('B1', lang('លេខករណី'));
        $this->excel->getActiveSheet()->SetCellValue('C1', lang('ឈ្មោះ'));
        $this->excel->getActiveSheet()->SetCellValue('D1', lang('ឈ្មោះជាអក្សរទ្បាតាំង'));
        $this->excel->getActiveSheet()->SetCellValue('E1', lang('ភេទ'));
        $this->excel->getActiveSheet()->SetCellValue('F1', lang('ថ្ងៃខែឆ្នាំកំណើត'));
        $this->excel->getActiveSheet()->SetCellValue('G1', lang('សញ្ជាតិ'));
		$this->excel->getActiveSheet()->SetCellValue('H1', lang('សាសនា'));
		$this->excel->getActiveSheet()->SetCellValue('I1', lang('ជាតិពន្ធុ'));
        $this->excel->getActiveSheet()->SetCellValue('J1', lang('ទំនាក់'));
		$this->excel->getActiveSheet()->SetCellValue('K1', lang('អាសយដ្ឋាន'));	
		$this->excel->getActiveSheet()->SetCellValue('L1', lang('លេខទំនាក់ទំនង'));
		$this->excel->getActiveSheet()->SetCellValue('M1', lang('ទីកន្លែងកំណើត'));
		$this->excel->getActiveSheet()->SetCellValue('N1', lang('ឋានៈ'));
		$results=$this->applications->getAsylumSeekerRegisterReports();
		$i=2;
		foreach($results as $row){
			$relationship = $this->applications->getRelationshipById($row->relationship);
			$country = $this->applications->getCountryById($row->country);
			$province = $this->applications->getProvinceById($row->province);
			$commune = $this->applications->getCommunceById($row->commune);						
			$district = $this->applications->getDistrictById($row->district);
				
			$relationship1_detail = lang("ម្ចាស់ករណី");
			if($relationship){
				$relationship1_detail = $relationship->relationship_kh;
			}
			$this->excel->getActiveSheet()->SetCellValue('A'.$i, $i);
			$this->excel->getActiveSheet()->SetCellValue('B'.$i, $row->case_prefix.$row->case_no);
			$this->excel->getActiveSheet()->SetCellValue('C'.$i, $row->firstname_kh." ".$row->lastname_kh);
			$this->excel->getActiveSheet()->SetCellValue('D'.$i, $row->firstname." ".$row->lastname);
			$this->excel->getActiveSheet()->SetCellValue('E'.$i, lang($row->gender));
			$this->excel->getActiveSheet()->SetCellValue('F'.$i, $this->erp->toKhmer($row->dob));
			$this->excel->getActiveSheet()->SetCellValue('G'.$i, $row->nationality_kh." / ".$row->nationality);
			$this->excel->getActiveSheet()->SetCellValue('H'.$i, $row->religion_kh." / ".$row->religion);
			$this->excel->getActiveSheet()->SetCellValue('I'.$i, $row->ethnicity_kh." / ".$row->religion);
			$this->excel->getActiveSheet()->SetCellValue('J'.$i, $relationship1_detail);
			$this->excel->getActiveSheet()->SetCellValue('K'.$i, $commune->native_name." ".$district->native_name." ".$province->native_name);			
			$this->excel->getActiveSheet()->SetCellValue('L'.$i, $this->erp->toKhmer($row->phone));
			$this->excel->getActiveSheet()->SetCellValue('M'.$i, $row->pob_kh." / ".$row->pob);
			$this->excel->getActiveSheet()->SetCellValue('N'.$i, lang($row->level));
			$this->excel->getActiveSheet()->getStyle('A'.$i.':O'.$i.'')->applyFromArray($styleArray);
			$i++;
		}
		$this->excel->getActiveSheet()->getStyle('A'.$i.':P'.$i)->applyFromArray($styleArray);
		$this->excel->getActiveSheet()->mergeCells('A'.$i.':P'.$i);
		$this->excel->getActiveSheet()->SetCellValue('A'.$i, "យោងតាមដំណរភ្ជាប់៖".$_SERVER['HTTP_REFERER']."  , ប្រព័ន្ធគ្រប់គ្រង៖ © 2018 អគ្គនាយកដ្ឋានអន្តោប្រវេសន៍ (v1.0 ) ");
		
        $this->excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
		
        $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);                
		
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$title.'.xls"');
        header('Cache-Control: max-age=0');                
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        $objWriter->save('php://output');exit();


	
	}
	
	/*=============REPORT K2===================*/
	 
	public function asylum_seekers_report() 
	{
		if(isset($_POST['export'])){
			if($_GET['sector']=='asylum_seekers'){
				$this->asylum_seeker_registered_export();
			}else if($_GET['sector']=='preliminary_stay'){ 
				$this->asylum_preliminary_export();
			}else if($_GET['sector']=='withdrawal_asylum'){
				$this->asylum_widthdrawal_export();
			}else{ 
				$this->asylum_seeker_export();
			}
		}else if(isset($_POST['print'])){ 
			if($_GET['sector']=='counseling'){				
				$this->asylum_seekers_counseling_print();
			}else if($_GET['sector']=='preliminary_stay'){				
				$this->asylum_seekers_preliminary_print();
			}else if($_GET['sector']=='asylum_seekers'){				
				$this->asylum_seekers_register_print();
			}else if($_GET['sector']=='withdrawal_asylum'){				
				$this->asylum_seekers_withdrawal_print();
			}else{ 
				$this->asylum_seekers_counseling_print();
			}
		}
		else
		{
			$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('asylum_seekers_report')));
			$meta = array('page_title' => lang('asylum_seekers_report'), 'bc' => $bc);	
			$this->page_construct('application_reports/asylum_seekers_report',$meta, $this->data); 
		}
	}
	
	public function asylum_seeker_registered()
	{
		$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('asylum_seeker_registered')));
		$meta = array('page_title' => lang('asylum_seeker_registered'), 'bc' => $bc); 
		$this->load->view($this->theme . 'application_forms/asylum_seeker_registered_print', $this->data); 
	}

	public function asylum_seeker_export()
	{	
		$this->load->library('excel');
        
		$styleArray = array(
			'font'  => array(
				//'bold'  => true,
				//'color' => array('rgb' => 'FF0000'),
				'size'  => 8,
				'name'  => 'Khmer OS Siemreap',
			));		
		$this->excel->getActiveSheet()->getStyle('A1:O1')->applyFromArray($styleArray);
		$this->excel->getActiveSheet()->getRowDimension('1')->setRowHeight(20);		
		$this->excel->getActiveSheet()->getStyle('A1:O1')->getFont()->setBold(true)->setSize(10);
		$this->excel->getActiveSheet()->getStyle('A1:O1')->getFont()->getColor()->setRGB('FFFFFF'); 
		$this->excel->getActiveSheet()->getStyle('A1:O1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('A1:O1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('A1:O1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('555555');
			
		$this->excel->setActiveSheetIndex(0);
		$title = lang("ពាក្យសុំផ្តល់ការប្រឹក្សាបឋម");		
        $this->excel->getActiveSheet()->setTitle($title);
        $this->excel->getActiveSheet()->SetCellValue('A1', lang('ល.រ'));
        $this->excel->getActiveSheet()->SetCellValue('B1', lang('លេខបឋម'));
        $this->excel->getActiveSheet()->SetCellValue('C1', lang('ឈ្មោះ'));
        $this->excel->getActiveSheet()->SetCellValue('D1', lang('ឈ្មោះជាអក្សរទ្បាតាំង'));
        $this->excel->getActiveSheet()->SetCellValue('E1', lang('ភេទ'));
        $this->excel->getActiveSheet()->SetCellValue('F1', lang('ថ្ងៃខែឆ្នាំកំណើត'));
        $this->excel->getActiveSheet()->SetCellValue('G1', lang('សញ្ជាតិ'));
		$this->excel->getActiveSheet()->SetCellValue('H1', lang('សាសនា'));
		$this->excel->getActiveSheet()->SetCellValue('I1', lang('ជាតិពន្ធុ'));
        $this->excel->getActiveSheet()->SetCellValue('J1', lang('ទំនាក់'));
		$this->excel->getActiveSheet()->SetCellValue('K1', lang('អាសយដ្ឋាន'));	
		$this->excel->getActiveSheet()->SetCellValue('L1', lang('លេខទំនាក់ទំនង'));
		$this->excel->getActiveSheet()->SetCellValue('M1', lang('ទីកន្លែងកំណើត'));
		$results=$this->applications->getAsylumSeekerReports();
		$i=2;
		foreach($results as $row){
			$relationship = $this->applications->getRelationshipById($row->relationship);
			$country = $this->applications->getCountryById($row->country);
			$province = $this->applications->getProvinceById($row->province);
			$commune = $this->applications->getCommunceById($row->commune);						
			$district = $this->applications->getDistrictById($row->district);
				
			$relationship1_detail = lang("ម្ចាស់ករណី");
			if($relationship){
				$relationship1_detail = $relationship->relationship_kh;
			}
			$this->excel->getActiveSheet()->SetCellValue('A'.$i, $i);
			$this->excel->getActiveSheet()->SetCellValue('B'.$i, "+".$row->counseling_no);
			$this->excel->getActiveSheet()->SetCellValue('C'.$i, $row->firstname_kh." ".$row->lastname_kh);
			$this->excel->getActiveSheet()->SetCellValue('D'.$i, $row->firstname." ".$row->lastname);
			$this->excel->getActiveSheet()->SetCellValue('E'.$i, lang($row->gender));
			$this->excel->getActiveSheet()->SetCellValue('F'.$i, $this->erp->toKhmer($row->dob));
			$this->excel->getActiveSheet()->SetCellValue('G'.$i, $row->nationality_kh." / ".$row->nationality);
			$this->excel->getActiveSheet()->SetCellValue('H'.$i, $row->religion_kh." / ".$row->religion);
			$this->excel->getActiveSheet()->SetCellValue('I'.$i, $row->ethnicity_kh." / ".$row->religion);
			$this->excel->getActiveSheet()->SetCellValue('J'.$i, $relationship1_detail);
			$this->excel->getActiveSheet()->SetCellValue('K'.$i, $commune->native_name." ".$district->native_name." ".$province->native_name);			
			$this->excel->getActiveSheet()->SetCellValue('L'.$i, $this->erp->toKhmer($row->phone));
			$this->excel->getActiveSheet()->SetCellValue('M'.$i, $row->pob_kh." / ".$row->pob);
			$this->excel->getActiveSheet()->getStyle('A'.$i.':O'.$i.'')->applyFromArray($styleArray);
			$i++;
		}
		$this->excel->getActiveSheet()->getStyle('A'.$i.':P'.$i)->applyFromArray($styleArray);
		$this->excel->getActiveSheet()->mergeCells('A'.$i.':P'.$i);
		$this->excel->getActiveSheet()->SetCellValue('A'.$i, "យោងតាមដំណរភ្ជាប់៖".$_SERVER['HTTP_REFERER']."  , ប្រព័ន្ធគ្រប់គ្រង៖ © 2018 អគ្គនាយកដ្ឋានអន្តោប្រវេសន៍ (v1.0 ) ");
		
        $this->excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
		
        $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);                
		
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$title.'.xls"');
        header('Cache-Control: max-age=0');                
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        $objWriter->save('php://output');exit();
	}
		
	public function asylum_seeker_registered_export()
	{	
		$this->load->library('excel');
        
		$styleArray = array(
			'font'  => array(
				//'bold'  => true,
				//'color' => array('rgb' => 'FF0000'),
				'size'  => 8,
				'name'  => 'Khmer OS Siemreap',
			));		
		$this->excel->getActiveSheet()->getStyle('A1:O1')->applyFromArray($styleArray);
		$this->excel->getActiveSheet()->getStyle('A1:O1')->getFont()->setBold(true)->setSize(10);
		$this->excel->getActiveSheet()->getStyle('A1:O1')->getFont()->getColor()->setRGB('FFFFFF'); 
		$this->excel->getActiveSheet()->getStyle('A1:O1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('A1:O1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('A1:O1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('555555');
		$this->excel->setActiveSheetIndex(0);
		$title = lang("ចុះឈ្មោះជនសុំសិទ្ធិជ្រកកោន");		
        $this->excel->getActiveSheet()->setTitle($title);
        $this->excel->getActiveSheet()->SetCellValue('A1', lang('ល.រ'));
        $this->excel->getActiveSheet()->SetCellValue('B1', lang('លេខបឋម'));
		$this->excel->getActiveSheet()->SetCellValue('C1', lang('លេខករណី'));        
        $this->excel->getActiveSheet()->SetCellValue('D1', lang('ឈ្មោះ'));
        $this->excel->getActiveSheet()->SetCellValue('E1', lang('ឈ្មោះជាអក្សរទ្បាតាំង'));
        $this->excel->getActiveSheet()->SetCellValue('F1', lang('ភេទ'));
        $this->excel->getActiveSheet()->SetCellValue('G1', lang('ថ្ងៃខែឆ្នាំកំណើត'));
		$this->excel->getActiveSheet()->SetCellValue('H1', lang('សញ្ជាតិ'));
		$this->excel->getActiveSheet()->SetCellValue('I1', lang('សាសនា'));
        $this->excel->getActiveSheet()->SetCellValue('J1', lang('ជាតិពន្ធុ'));
		$this->excel->getActiveSheet()->SetCellValue('K1', lang('ទំនាក់'));	
		$this->excel->getActiveSheet()->SetCellValue('L1', lang('អាសយដ្ឋាន'));
		$this->excel->getActiveSheet()->SetCellValue('M1', lang('លេខទំនាក់ទំនង'));
		$this->excel->getActiveSheet()->SetCellValue('N1', lang('ទីកន្លែងកំណើត'));
		$results=$this->applications->getAsylumSeekerRegisterReports();
		$i=2;
		foreach($results as $row){
			$relationship = $this->applications->getRelationshipById($row->relationship);
			$country = $this->applications->getCountryById($row->country);
			$province = $this->applications->getProvinceById($row->province);
			$commune = $this->applications->getCommunceById($row->commune);						
			$district = $this->applications->getDistrictById($row->district);
				
			$relationship1_detail = lang("ម្ចាស់ករណី");
			if($relationship){
				$relationship1_detail = $relationship->relationship_kh;
			}
			$this->excel->getActiveSheet()->SetCellValue('A'.$i, $i);
			$this->excel->getActiveSheet()->SetCellValue('B'.$i, "+".$row->counseling_no);
			$this->excel->getActiveSheet()->SetCellValue('C'.$i, $row->case_prefix."".$row->case_no);
			$this->excel->getActiveSheet()->SetCellValue('D'.$i, $row->firstname_kh." ".$row->lastname_kh);
			$this->excel->getActiveSheet()->SetCellValue('E'.$i, $row->firstname." ".$row->lastname);
			$this->excel->getActiveSheet()->SetCellValue('F'.$i, lang($row->gender));
			$this->excel->getActiveSheet()->SetCellValue('G'.$i, $this->erp->toKhmer($row->dob));
			$this->excel->getActiveSheet()->SetCellValue('H'.$i, $row->nationality_kh." / ".$row->nationality);
			$this->excel->getActiveSheet()->SetCellValue('I'.$i, $row->religion_kh." / ".$row->religion);
			$this->excel->getActiveSheet()->SetCellValue('J'.$i, $row->ethnicity_kh." / ".$row->religion);
			$this->excel->getActiveSheet()->SetCellValue('K'.$i, $relationship1_detail);
			$this->excel->getActiveSheet()->SetCellValue('L'.$i, $commune->native_name." ".$district->native_name." ".$province->native_name);			
			$this->excel->getActiveSheet()->SetCellValue('M'.$i, $this->erp->toKhmer($row->phone));
			$this->excel->getActiveSheet()->SetCellValue('N'.$i, $row->pob_kh." / ".$row->pob);			
			$this->excel->getActiveSheet()->getStyle('A'.$i.':O'.$i.'')->applyFromArray($styleArray);
			$i++;
		}
		$this->excel->getActiveSheet()->getStyle('A'.$i.':P'.$i)->applyFromArray($styleArray);
		$this->excel->getActiveSheet()->mergeCells('A'.$i.':P'.$i);
		$this->excel->getActiveSheet()->SetCellValue('A'.$i, "យោងតាមដំណរភ្ជាប់៖".$_SERVER['HTTP_REFERER']."  , ប្រព័ន្ធគ្រប់គ្រង៖ © 2018 អគ្គនាយកដ្ឋានអន្តោប្រវេសន៍ (v1.0 ) ");
		
        $this->excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
        $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);                
		
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$title.'.xls"');
        header('Cache-Control: max-age=0');                
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        $objWriter->save('php://output');exit();
	}
	
	public function asylum_preliminary_export()
	{	
		$this->load->library('excel');
        
		$styleArray = array(
			'font'  => array(
				//'bold'  => true,
				//'color' => array('rgb' => 'FF0000'),
				'size'  => 8,
				'name'  => 'Khmer OS Siemreap',
			));		
		$this->excel->getActiveSheet()->getStyle('A1:O1')->applyFromArray($styleArray);
		$this->excel->getActiveSheet()->getRowDimension('1')->setRowHeight(20);		
		$this->excel->getActiveSheet()->getStyle('A1:O1')->getFont()->setBold(true)->setSize(10);
		$this->excel->getActiveSheet()->getStyle('A1:O1')->getFont()->getColor()->setRGB('FFFFFF'); 
		$this->excel->getActiveSheet()->getStyle('A1:O1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('A1:O1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('A1:O1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('555555');
			
		$this->excel->setActiveSheetIndex(0);
		$title = lang("ស្នាក់នៅបណ្តោះអាសន្ន ");		
        $this->excel->getActiveSheet()->setTitle($title);
        $this->excel->getActiveSheet()->SetCellValue('A1', lang('ល.រ'));
        $this->excel->getActiveSheet()->SetCellValue('B1', lang('លេខករណី'));
        $this->excel->getActiveSheet()->SetCellValue('C1', lang('ឈ្មោះ'));
        $this->excel->getActiveSheet()->SetCellValue('D1', lang('ឈ្មោះជាអក្សរទ្បាតាំង'));
        $this->excel->getActiveSheet()->SetCellValue('E1', lang('ភេទ'));
        $this->excel->getActiveSheet()->SetCellValue('F1', lang('ថ្ងៃខែឆ្នាំកំណើត'));
        $this->excel->getActiveSheet()->SetCellValue('G1', lang('សញ្ជាតិ'));
		$this->excel->getActiveSheet()->SetCellValue('H1', lang('សាសនា'));
		$this->excel->getActiveSheet()->SetCellValue('I1', lang('ជាតិពន្ធុ'));
        $this->excel->getActiveSheet()->SetCellValue('J1', lang('ទំនាក់'));
		$this->excel->getActiveSheet()->SetCellValue('K1', lang('អាសយដ្ឋាន'));	
		$this->excel->getActiveSheet()->SetCellValue('L1', lang('លេខទំនាក់ទំនង'));
		$this->excel->getActiveSheet()->SetCellValue('M1', lang('ទីកន្លែងកំណើត'));
		$results=$this->applications->getAsylumSeekerPreliminaryReports();
		$i=2;
		foreach($results as $row){
			$relationship = $this->applications->getRelationshipById($row->relationship);
			$country = $this->applications->getCountryById($row->country);
			$province = $this->applications->getProvinceById($row->province);
			$commune = $this->applications->getCommunceById($row->commune);						
			$district = $this->applications->getDistrictById($row->district);
			$relationship1_detail = lang("ម្ចាស់ករណី");
			if($relationship){
				$relationship1_detail = $relationship->relationship_kh;
			}
			$this->excel->getActiveSheet()->SetCellValue('A'.$i, $i);
			$this->excel->getActiveSheet()->SetCellValue('B'.$i, $row->case_prefix."".$row->case_no);
			$this->excel->getActiveSheet()->SetCellValue('C'.$i, $row->firstname_kh." ".$row->lastname_kh);
			$this->excel->getActiveSheet()->SetCellValue('D'.$i, $row->firstname." ".$row->lastname);
			$this->excel->getActiveSheet()->SetCellValue('E'.$i, lang($row->gender));
			$this->excel->getActiveSheet()->SetCellValue('F'.$i, $this->erp->toKhmer($row->dob));
			$this->excel->getActiveSheet()->SetCellValue('G'.$i, $row->nationality_kh." / ".$row->nationality);
			$this->excel->getActiveSheet()->SetCellValue('H'.$i, $row->religion_kh." / ".$row->religion);
			$this->excel->getActiveSheet()->SetCellValue('I'.$i, $row->ethnicity_kh." / ".$row->religion);
			$this->excel->getActiveSheet()->SetCellValue('J'.$i, $relationship1_detail);
			$this->excel->getActiveSheet()->SetCellValue('K'.$i, $commune->native_name." ".$district->native_name." ".$province->native_name);			
			$this->excel->getActiveSheet()->SetCellValue('L'.$i, $this->erp->toKhmer($row->phone));
			$this->excel->getActiveSheet()->SetCellValue('M'.$i, $row->pob_kh." / ".$row->pob);
			$this->excel->getActiveSheet()->getStyle('A'.$i.':O'.$i.'')->applyFromArray($styleArray);
			$i++;
		}
        $this->excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
        $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);                		
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$title.'.xls"');
        header('Cache-Control: max-age=0');                
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        $objWriter->save('php://output');exit();
	}
	
	public function asylum_widthdrawal_export()
	{	
		$this->load->library('excel');
        
		$styleArray = array(
			'font'  => array(
				//'bold'  => true,
				//'color' => array('rgb' => 'FF0000'),
				'size'  => 8,
				'name'  => 'Khmer OS Siemreap',
			));		
		$this->excel->getActiveSheet()->getStyle('A1:O1')->applyFromArray($styleArray);
		$this->excel->getActiveSheet()->getRowDimension('1')->setRowHeight(20);		
		$this->excel->getActiveSheet()->getStyle('A1:O1')->getFont()->setBold(true)->setSize(10);
		$this->excel->getActiveSheet()->getStyle('A1:O1')->getFont()->getColor()->setRGB('FFFFFF'); 
		$this->excel->getActiveSheet()->getStyle('A1:O1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('A1:O1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('A1:O1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('555555');
			
		$this->excel->setActiveSheetIndex(0);
		$title = lang("ស្នាក់នៅបណ្តោះអាសន្ន ");		
        $this->excel->getActiveSheet()->setTitle($title);
        $this->excel->getActiveSheet()->SetCellValue('A1', lang('ល.រ'));
        $this->excel->getActiveSheet()->SetCellValue('B1', lang('លេខករណី'));
        $this->excel->getActiveSheet()->SetCellValue('C1', lang('ឈ្មោះ'));
        $this->excel->getActiveSheet()->SetCellValue('D1', lang('ឈ្មោះជាអក្សរទ្បាតាំង'));
        $this->excel->getActiveSheet()->SetCellValue('E1', lang('ភេទ'));
        $this->excel->getActiveSheet()->SetCellValue('F1', lang('ថ្ងៃខែឆ្នាំកំណើត'));
        $this->excel->getActiveSheet()->SetCellValue('G1', lang('សញ្ជាតិ'));
		$this->excel->getActiveSheet()->SetCellValue('H1', lang('សាសនា'));
		$this->excel->getActiveSheet()->SetCellValue('I1', lang('ជាតិពន្ធុ'));
        $this->excel->getActiveSheet()->SetCellValue('J1', lang('ទំនាក់'));
		$this->excel->getActiveSheet()->SetCellValue('K1', lang('អាសយដ្ឋាន'));	
		$this->excel->getActiveSheet()->SetCellValue('L1', lang('លេខទំនាក់ទំនង'));
		$this->excel->getActiveSheet()->SetCellValue('M1', lang('ទីកន្លែងកំណើត'));
		$results=$this->applications->getAsylumSeekerWithdrawalReports();
		$i=2;
		foreach($results as $row){
			$relationship = $this->applications->getRelationshipById($row->relationship);
			$country = $this->applications->getCountryById($row->country);
			$province = $this->applications->getProvinceById($row->province);
			$commune = $this->applications->getCommunceById($row->commune);						
			$district = $this->applications->getDistrictById($row->district);
			$relationship1_detail = lang("ម្ចាស់ករណី");
			if($relationship){
				$relationship1_detail = $relationship->relationship_kh;
			}
			$this->excel->getActiveSheet()->SetCellValue('A'.$i, $i);
			$this->excel->getActiveSheet()->SetCellValue('B'.$i, $row->case_prefix."".$row->case_no);
			$this->excel->getActiveSheet()->SetCellValue('C'.$i, $row->firstname_kh." ".$row->lastname_kh);
			$this->excel->getActiveSheet()->SetCellValue('D'.$i, $row->firstname." ".$row->lastname);
			$this->excel->getActiveSheet()->SetCellValue('E'.$i, lang($row->gender));
			$this->excel->getActiveSheet()->SetCellValue('F'.$i, $this->erp->toKhmer($row->dob));
			$this->excel->getActiveSheet()->SetCellValue('G'.$i, $row->nationality_kh." / ".$row->nationality);
			$this->excel->getActiveSheet()->SetCellValue('H'.$i, $row->religion_kh." / ".$row->religion);
			$this->excel->getActiveSheet()->SetCellValue('I'.$i, $row->ethnicity_kh." / ".$row->religion);
			$this->excel->getActiveSheet()->SetCellValue('J'.$i, $relationship1_detail);
			$this->excel->getActiveSheet()->SetCellValue('K'.$i, $commune->native_name." ".$district->native_name." ".$province->native_name);			
			$this->excel->getActiveSheet()->SetCellValue('L'.$i, $this->erp->toKhmer($row->phone));
			$this->excel->getActiveSheet()->SetCellValue('M'.$i, $row->pob_kh." / ".$row->pob);
			$this->excel->getActiveSheet()->getStyle('A'.$i.':O'.$i.'')->applyFromArray($styleArray);
			$i++;
		}
		$this->excel->getActiveSheet()->getStyle('A'.$i.':P'.$i)->applyFromArray($styleArray);
		$this->excel->getActiveSheet()->mergeCells('A'.$i.':P'.$i);
		$this->excel->getActiveSheet()->SetCellValue('A'.$i, "យោងតាមដំណរភ្ជាប់៖".$_SERVER['HTTP_REFERER']."  , ប្រព័ន្ធគ្រប់គ្រង៖ © 2018 អគ្គនាយកដ្ឋានអន្តោប្រវេសន៍ (v1.0 ) ");
		
        $this->excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
        $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);                		
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$title.'.xls"');
        header('Cache-Control: max-age=0');                
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        $objWriter->save('php://output');exit();
	}
	
	public function asylum_seekers_counseling_print()
	{				
		$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('asylum_seekers_counseling_print')));
		$meta = array('page_title' => lang('asylum_seekers_counseling_print'), 'bc' => $bc);	
		$this->page_construct('application_reports/asylum_seekers_counseling_print',$meta, $this->data);	
	} 
	
	public function asylum_seekers_register_print()
	{				
		$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('asylum_seekers_register_print')));
		$meta = array('page_title' => lang('asylum_seekers_register_print'), 'bc' => $bc);	
		$this->page_construct('application_reports/asylum_seekers_register_print',$meta, $this->data);	
	} 
	
	public function asylum_seekers_preliminary_print()
	{				
		$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('asylum_seekers_preliminary_print')));
		$meta = array('page_title' => lang('asylum_seekers_preliminary_print'), 'bc' => $bc);	
		$this->page_construct('application_reports/asylum_seekers_preliminary_print',$meta, $this->data);	
	}
	
	public function asylum_seekers_withdrawal_print()
	{				
		$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('asylum_seekers_withdrawal_print')));
		$meta = array('page_title' => lang('asylum_seekers_print'), 'bc' => $bc);	
		$this->page_construct('application_reports/asylum_seekers_withdrawal_print',$meta, $this->data);	
	} 
		
	
	/*=============REPORT K4===================*/
	
	public function social_affairs()
	{
		if(isset($_POST['export'])){ 
			if($_GET['sector']=='family_guarantee'){
				$this->refugee_family_guarantee_export();
			}
			else if($_GET['sector']=='home_visit'){ 
				$this->refugee_home_visit_export();
			}
		}
		else if(isset($_POST['print_research'])){
		    if($_GET['sector']=='family_guarantee'){
		        $this->refugee_family_guarantee_print();
            }
            else if($_GET['sector']=='home_visit'){
		        $this->refugee_home_visit_print();
            }
        }else {
            $this->data["result"] = "";
            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('social_affairs')));
            $meta = array('page_title' => lang('social_affairs'), 'bc' => $bc);
            $this->page_construct('application_reports/social_affairs', $meta, $this->data);
        }
	}
		
	public function refugee_family_guarantee_export()
	{
		$this->load->library('excel');
		$styleArray = array(
			'font'  => array(
				'size'  => 8,
				'name'  => 'Khmer OS Siemreap',
			));		
		$this->excel->getActiveSheet()->getStyle('A1:P1')->applyFromArray($styleArray);
		$this->excel->getActiveSheet()->getRowDimension('1')->setRowHeight(20);		
		$this->excel->getActiveSheet()->getStyle('A1:P1')->getFont()->setBold(true)->setSize(10);
		$this->excel->getActiveSheet()->getStyle('A1:P1')->getFont()->getColor()->setRGB('FFFFFF'); 
		$this->excel->getActiveSheet()->getStyle('A1:P1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('A1:P1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('A1:P1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('555555');
			
		$this->excel->setActiveSheetIndex(0);
		$title = lang(" ពាក្យសុំធានាគ្រួសារ ");		
        $this->excel->getActiveSheet()->setTitle($title);
        $this->excel->getActiveSheet()->SetCellValue('A1', lang('ល.រ'));
        $this->excel->getActiveSheet()->SetCellValue('B1', lang('លេខករណី'));
        $this->excel->getActiveSheet()->SetCellValue('C1', lang('ឈ្មោះ'));
        $this->excel->getActiveSheet()->SetCellValue('D1', lang('ឈ្មោះជាអក្សរទ្បាតាំង'));
        $this->excel->getActiveSheet()->SetCellValue('E1', lang('ភេទ'));
        $this->excel->getActiveSheet()->SetCellValue('F1', lang('ថ្ងៃខែឆ្នាំកំណើត'));
        $this->excel->getActiveSheet()->SetCellValue('G1', lang('សញ្ជាតិ'));
		$this->excel->getActiveSheet()->SetCellValue('H1', lang('សាសនា'));
		$this->excel->getActiveSheet()->SetCellValue('I1', lang('ជាតិពន្ធុ'));
        $this->excel->getActiveSheet()->SetCellValue('J1', lang('ទំនាក់ទំនង'));
		$this->excel->getActiveSheet()->SetCellValue('K1', lang('អាសយដ្ឋាន'));	
		$this->excel->getActiveSheet()->SetCellValue('L1', lang('មុខរបរ'));
		$this->excel->getActiveSheet()->SetCellValue('M1', lang('ទីកន្លែងកំណើត'));
		$this->excel->getActiveSheet()->SetCellValue('N1', lang('ថ្ងៃដែលស្នើរធានា'));
		$this->excel->getActiveSheet()->SetCellValue('O1', lang('ការសម្រេច'));
		$this->excel->getActiveSheet()->SetCellValue('P1', lang('ការមកដល់'));
		$results=$this->applications->getRefugeeGuaranteeReports();
		// var_dump($results);exit;
		$i=2;
		foreach($results as $row){
			$relationship = $this->applications->getRelationshipById($row->relationship);
			$country = $this->applications->getCountryById($row->country);
			$state = $this->applications->getStateById($row->state);
			$province = $this->applications->getProvinceById($row->province);
			$commune = $this->applications->getCommunceById($row->commune);
			$district = $this->applications->getDistrictById($row->district);
            $village = $this->applications->getTagsKhmerById($row->village);
            $relationship1_detail = lang("ម្ចាស់ករណី");
			if($relationship){
				$relationship1_detail = $relationship->relationship_kh;
			} 
			$this->excel->getActiveSheet()->SetCellValue('A'.$i, $i-1);
			$this->excel->getActiveSheet()->SetCellValue('B'.$i, $row->case_prefix."".$row->case_no);
			$this->excel->getActiveSheet()->SetCellValue('C'.$i, $row->firstname_kh." ".$row->lastname_kh);
			$this->excel->getActiveSheet()->SetCellValue('D'.$i, $row->firstname." ".$row->lastname);
			$this->excel->getActiveSheet()->SetCellValue('E'.$i, lang($row->gender));
			$this->excel->getActiveSheet()->SetCellValue('F'.$i, $this->erp->toKhmer($this->erp->hrsd($row->dob))); 
			$this->excel->getActiveSheet()->SetCellValue('G'.$i, $row->nationality_kh." / ".$row->nationality);
			$this->excel->getActiveSheet()->SetCellValue('H'.$i, $row->religion_kh." / ".$row->religion);
			$this->excel->getActiveSheet()->SetCellValue('I'.$i, $row->ethnicity_kh." / ".$row->ethnicity);
			$this->excel->getActiveSheet()->SetCellValue('J'.$i, $relationship1_detail);
			$custom_village = $village ? "ភៈ".$village.", ":"";
			$custom_commune = ($commune->native_name)?"ឃ.សៈ".$commune->native_name.", ":"";
			$custom_district = ($district->native_name)?"ស្រ.ខៈ".$district->native_name.", ":"";
			$custom_province = ($province->native_name)?"ខ.ក្រៈ".$province->native_name.", ":"";
			$custom_state = ($state->native_name)?"រៈ".$state->native_name.", ":"";
			$custom_country = ($country->native_name)?"ប្រៈ".$country->native_name:"";
			$this->excel->getActiveSheet()->SetCellValue('K'.$i,
				$row->address_kh." ".$custom_village." ".$custom_commune." ".$custom_district." ".$custom_province." ".$custom_state." ".$custom_country
			);
			$this->excel->getActiveSheet()->SetCellValue('L'.$i, $row->occupation_kh." / ".$row->occupation);
			$this->excel->getActiveSheet()->SetCellValue('M'.$i, $row->pob_kh." / ".$row->pob);
			$this->excel->getActiveSheet()->SetCellValue('N'.$i, $this->erp->toKhmer($this->erp->hrsd($row->created_date)));
			$this->excel->getActiveSheet()->SetCellValue('O'.$i, $row->status_option );
			$this->excel->getActiveSheet()->SetCellValue('P'.$i, $row->come_option );
			$this->excel->getActiveSheet()->getStyle('A'.$i.':P'.$i.'')->applyFromArray($styleArray);
			$i++;
		}
		//$this->excel->getActiveSheet()->getStyle('A'.$i.':P'.$i)->applyFromArray($styleArray);
		//$this->excel->getActiveSheet()->mergeCells('A'.$i.':P'.$i);
		//$this->excel->getActiveSheet()->SetCellValue('A'.$i, "យោងតាមដំណរភ្ជាប់៖".$_SERVER['HTTP_REFERER']."  , ប្រព័ន្ធគ្រប់គ្រង៖ © 2018 អគ្គនាយកដ្ឋានអន្តោប្រវេសន៍ (v1.0 ) ");
		
        $this->excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('P')->setAutoSize(true);
        $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);                		
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$title.'.xls"');
        header('Cache-Control: max-age=0');                
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        $objWriter->save('php://output');exit();
	}
	
	public function refugee_home_visit_export()
	{
		$this->load->library('excel');
		$styleArray = array(
			'font'  => array(			
				'size'  => 8,
				'name'  => 'Khmer OS Siemreap',
			));		
		$this->excel->getActiveSheet()->getStyle('A1:AF1')->applyFromArray($styleArray);
		$this->excel->getActiveSheet()->getRowDimension('1')->setRowHeight(20);		
		$this->excel->getActiveSheet()->getStyle('A1:AF1')->getFont()->setBold(true)->setSize(10);
		$this->excel->getActiveSheet()->getStyle('A1:AF1')->getFont()->getColor()->setRGB('FFFFFF'); 
		$this->excel->getActiveSheet()->getStyle('A1:AF1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('A1:AF1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('A1:AF1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('555555');
		$this->excel->setActiveSheetIndex(0);
		$title = lang("កំណត់ត្រាការចុះសួរសុខទុក្ខ ");		
        $this->excel->getActiveSheet()->setTitle($title);
        $this->excel->getActiveSheet()->SetCellValue('A1', lang('ល.រ'));
        $this->excel->getActiveSheet()->SetCellValue('B1', lang('លេខករណី'));
        $this->excel->getActiveSheet()->SetCellValue('C1', lang('ឈ្មោះ'));
        $this->excel->getActiveSheet()->SetCellValue('D1', lang('ឈ្មោះជាអក្សរទ្បាតាំង'));
        $this->excel->getActiveSheet()->SetCellValue('E1', lang('ភេទ'));
        $this->excel->getActiveSheet()->SetCellValue('F1', lang('ថ្ងៃខែឆ្នាំកំណើត'));
		$this->excel->getActiveSheet()->SetCellValue('G1', lang('ថ្ងៃចុះសួរសុខទុក្'));
        $this->excel->getActiveSheet()->SetCellValue('H1', lang('សញ្ជាតិ'));
		$this->excel->getActiveSheet()->SetCellValue('I1', lang('សាសនា'));
		$this->excel->getActiveSheet()->SetCellValue('J1', lang('ជាតិពន្ធុ'));
        $this->excel->getActiveSheet()->SetCellValue('K1', lang('ទំនាក់ទំនង'));
		$this->excel->getActiveSheet()->SetCellValue('L1', lang('អាសយដ្ឋាន'));	
		$this->excel->getActiveSheet()->SetCellValue('M1', lang('មុខរបរ'));
		$this->excel->getActiveSheet()->SetCellValue('N1', lang('ទីកន្លែងកំណើត'));
		$this->excel->getActiveSheet()->SetCellValue('O1', lang('ថ្ងៃចុះសួរសុខទុក្ខ'));
		$this->excel->getActiveSheet()->SetCellValue('P1', lang('ទាក់ទងបាន?'));
		$this->excel->getActiveSheet()->SetCellValue('Q1', lang('មូលហេតុ'));
		$this->excel->getActiveSheet()->SetCellValue('R1', lang('មានជំនួយ?'));
		$this->excel->getActiveSheet()->SetCellValue('S1', lang('ជំនួយឧបត្ថម្ភ (ពី)'));
		$this->excel->getActiveSheet()->SetCellValue('T1', lang('ប្រភេទជំនួយ'));
		$this->excel->getActiveSheet()->SetCellValue('U1', lang('បើសិន (​​ជាសម្ភារៈផ្សេងៗ)'));
		$this->excel->getActiveSheet()->SetCellValue('V1', lang('សេវាធានារ៉ាប់រង'));
		$this->excel->getActiveSheet()->SetCellValue('W1', lang('សេវាធានារ៉ាប់រង (ពីអង្គការៈ)'));
		$this->excel->getActiveSheet()->SetCellValue('X1', lang('លើផ្នែក'));
		$this->excel->getActiveSheet()->SetCellValue('Y1', lang('បើសិន (ផ្សេងទៀត)'));
		$this->excel->getActiveSheet()->SetCellValue('Z1', lang('ធ្លាប់មានពិរុទ្ធ?'));
		$this->excel->getActiveSheet()->SetCellValue('AA1', lang('បើ​សិន (ធ្លាប់មានពិរុទ្ធ)'));

		$results=$this->applications->getRefugeeHomeVisitReports();
		$i=2;
		foreach($results as $row){
			$relationship = $this->applications->getRelationshipById($row->relationship);
			$country = $this->applications->getCountryById($row->country);
            $state = $this->applications->getStateById($row->state);
			$province = $this->applications->getProvinceById($row->province);
			$commune = $this->applications->getCommunceById($row->commune);						
			$district = $this->applications->getDistrictById($row->district);
			$village = $this->applications->getTagsById($row->village);
			$visit = $this->applications->getHomeVisitById($row->id);
			$relationship1_detail = lang("ម្ចាស់ករណី"); 
			$status = array("គ្មាន","មាន");
			$contact = array("មិនបាន","បាន");
			$card = "";
			$local_authorities_opinion_array = array("មិនធ្លាប់មានពិរុទ្ធ","ធ្លាប់មានពិរុទ្ធ");
			$sponsor_money = array("ជាសម្ភារៈផ្សេងៗ","ជាថវិកា");

			$sponsor_data_detail = "";
			$sponsor_money1 = explode(',', $visit->sponsorship_support_money);
			foreach($sponsor_money1 as $sm){
				$sponsor_data_detail .= $sponsor_money[$sm].' ';
			}

			$insurance_service = array('health'=>'សុខភាព','life'=>'អាយុជីវិត ','other'=>'ផ្សេងទៀត');

			$insurance_data_detail='';
			$insurance_service1 = explode(',', $visit->insurance_service_on);

			foreach($insurance_service1 as $is){
				$insurance_data_detail .= $insurance_service[$is].' ';
			}
			// echo $insurance_data_detail;exit;
			if($relationship){
				$relationship1_detail = $relationship->relationship_kh;
			}			
			if($row->relationship != 0){
				$status = array("","");
			}		

			$this->excel->getActiveSheet()->SetCellValue('A'.$i, $i-1);
			$this->excel->getActiveSheet()->SetCellValue('B'.$i, $row->case_prefix."".$row->case_no);
			$this->excel->getActiveSheet()->SetCellValue('C'.$i, $row->firstname_kh." ".$row->lastname_kh);
			$this->excel->getActiveSheet()->SetCellValue('D'.$i, $row->firstname." ".$row->lastname);
			$this->excel->getActiveSheet()->SetCellValue('E'.$i, lang($row->gender));
			$this->excel->getActiveSheet()->SetCellValue('F'.$i, $this->erp->toKhmer($this->erp->hrsd($row->dob)));
			$this->excel->getActiveSheet()->SetCellValue('G'.$i, $this->erp->toKhmer($this->erp->hrsd($row->day_visit)));
			$this->excel->getActiveSheet()->SetCellValue('H'.$i, $row->nationality_kh." / ".$row->nationality);
			$this->excel->getActiveSheet()->SetCellValue('I'.$i, $row->religion_kh." / ".$row->religion);
			$this->excel->getActiveSheet()->SetCellValue('J'.$i, $row->ethnicity_kh." / ".$row->ethnicity);
			$this->excel->getActiveSheet()->SetCellValue('K'.$i, $relationship1_detail);
            $custom_village = $village ? "ភៈ".$village.", ":"";
            $custom_commune = ($commune->native_name)?"ឃ.សៈ".$commune->native_name.", ":"";
            $custom_district = ($district->native_name)?"ស្រ.ខៈ".$district->native_name.", ":"";
            $custom_province = ($province->native_name)?"ខ.ក្រៈ".$province->native_name.", ":"";
            $custom_state = ($state->native_name)?"រៈ".$state->native_name.", ":"";
            $custom_country = ($country->native_name)?"ប្រៈ".$country->native_name:"";
			$this->excel->getActiveSheet()->SetCellValue('L'.$i, $row->address_kh." ".$custom_village." ".$custom_commune." ".$custom_district." ".$custom_province." ".$custom_state." ".$custom_country);
			$this->excel->getActiveSheet()->SetCellValue('M'.$i, $row->occupation_kh);
			$this->excel->getActiveSheet()->SetCellValue('N'.$i, $row->pob_kh." / ".$row->pob);
			$this->excel->getActiveSheet()->SetCellValue('O'.$i, $this->erp->toKhmer($this->erp->hrsd($row->day_visit)));
			$this->excel->getActiveSheet()->SetCellValue('P'.$i, ($relationship1_detail=='ម្ចាស់ករណី'?$contact[$visit->is_contact]:''));
			$this->excel->getActiveSheet()->SetCellValue('Q'.$i, ($relationship1_detail=='ម្ចាស់ករណី'?$visit->contact_reason:''));
			$this->excel->getActiveSheet()->SetCellValue('R'.$i, ($relationship1_detail=='ម្ចាស់ករណី'?$status[$visit->sponsorship_support]:''));
			$this->excel->getActiveSheet()->SetCellValue('S'.$i, ($relationship1_detail=='ម្ចាស់ករណី'?$visit->sponsorship_support_by:''));
			$this->excel->getActiveSheet()->SetCellValue('T'.$i, ($relationship1_detail=='ម្ចាស់ករណី'?$sponsor_data_detail:''));//add
			$this->excel->getActiveSheet()->SetCellValue('U'.$i, ($relationship1_detail=='ម្ចាស់ករណី'?$visit->sponsorship_support_other:''));//add

			$this->excel->getActiveSheet()->SetCellValue('V'.$i, $status[$visit->insurance_service]);
			$this->excel->getActiveSheet()->SetCellValue('W'.$i, ($relationship1_detail=='ម្ចាស់ករណី'?$visit->insurance_service_by:''));
			$this->excel->getActiveSheet()->SetCellValue('X'.$i, ($relationship1_detail=='ម្ចាស់ករណី'?$insurance_data_detail:''));
			$this->excel->getActiveSheet()->SetCellValue('Y'.$i, ($relationship1_detail=='ម្ចាស់ករណី'?$visit->insurance_service_other:''));
			$this->excel->getActiveSheet()->SetCellValue('Z'.$i, ($relationship1_detail=='ម្ចាស់ករណី'?$status[$visit->local_authorities_opinion]:''));
			$this->excel->getActiveSheet()->SetCellValue('AA'.$i, ($relationship1_detail=='ម្ចាស់ករណី'?$visit->local_authorities_comment:''));

			$this->excel->getActiveSheet()->getStyle('A'.$i.':AF'.$i.'')->applyFromArray($styleArray);
			$i++;
		}
		//$this->excel->getActiveSheet()->getStyle('A'.$i.':AF'.$i)->applyFromArray($styleArray);
		//$this->excel->getActiveSheet()->mergeCells('A'.$i.':AF'.$i);
		//$this->excel->getActiveSheet()->SetCellValue('A'.$i, "យោងតាមដំណរភ្ជាប់៖".$_SERVER['HTTP_REFERER']."  , ប្រព័ន្ធគ្រប់គ្រង៖ © 2018 អគ្គនាយកដ្ឋានអន្តោប្រវេសន៍ (v1.0 ) ");
		
        $this->excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('P')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('Q')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('R')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('S')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('T')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('U')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('V')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('W')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('X')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('Y')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('Z')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('AA')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('AB')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('AC')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('AD')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('AE')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('AF')->setAutoSize(true);
        $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$title.'.xls"');
        header('Cache-Control: max-age=0');                
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        $objWriter->save('php://output');exit();
	}

    public function refugee_family_guarantee_print()
    {
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('របាយការណ៌ពាក្យសុំធានាគ្រួសារ')));
        $meta = array('page_title' => lang('របាយការណ៌ពាក្យសុំធានាគ្រួសារ'), 'bc' => $bc);
        $this->page_construct('application_reports/refugee_family_guarantee_print',$meta, $this->data);
    } // Print reports refugee_family_guarantee

    public function refugee_home_visit_print()
    {
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('របាយការណ៌កំណត់ត្រាការចុះសួរសុខទុក្ខ ')));
        $meta = array('page_title' => lang('របាយការណ៌កំណត់ត្រាការចុះសួរសុខទុក្ខ '), 'bc' => $bc);
        $this->page_construct('application_reports/refugee_home_visit_print',$meta, $this->data);
    } // Print reports refugee_home_visit
	
	/*=============REPORT K1===================*/
	
	public function administration_control_report()
	{	
	
		if(isset($_POST['export'])){  
			if($_GET['sector']=='refugee_recognition'){
				$this->refugee_recognition_export();
			}else if($_GET['sector']=='refugee_permanent_card'){
				$this->refugee_permanent_card_export();
			}else if($_GET['sector']=='refugee_card'){
				$this->refugee_card_export();
			}else if($_GET['sector']=='refugee_travel_document'){
				$this->refugee_travel_document_export();
			}else if($_GET['sector']=='refugee_cessation'){
				$this->refugee_cessation_export();
			}else if($_GET['sector'] == 'refugee_withdrawal'){
				$this->refugee_withdrawal_export();
			}
			else{
				$this->refugee_recognition_export();
			}
		}else if(isset($_POST['print'])){
			if($_GET['sector'] == 'refugee_recognition'){
				$this->refugee_recognition_print();
			}else if($_GET['sector'] == 'refugee_permanent_card'){
				$this->refugee_permanent_card_print();
			}else if($_GET['sector'] == 'refugee_card'){
				$this->refugee_card_print();
			}else if($_GET['sector'] == 'refugee_travel_document'){
				$this->refugee_travel_document_print();
			}else if($_GET['sector'] == 'refugee_cessation'){
				$this->refugee_cessation_print();
			}else if($_GET['sector'] == 'refugee_withdrawal'){
				$this->refugee_withdrawal_print();
			}else {
				$this->refugee_recognition_print();
			}
			
		}else{
		$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('administration_control_report')));
		$meta = array('page_title' => lang('administration_control_report'), 'bc' => $bc);	
		$this->page_construct('application_reports/administration_control_report',$meta, $this->data);	
		}
	}
	
	public function refugee_recognition_print()
	{				
		$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('refugee_recognition_print')));
		$meta = array('page_title' => lang('refugee_recognition_print'), 'bc' => $bc);	
		$this->page_construct('application_reports/refugee_recognition_print',$meta, $this->data);	
	}
	
	public function refugee_permanent_card_print()
	{				
		$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('refugee_permanent_card_print')));
		$meta = array('page_title' => lang('refugee_permanent_card_print'), 'bc' => $bc);	
		$this->page_construct('application_reports/refugee_permanent_card_print',$meta, $this->data);	
	} 
	
	public function refugee_card_print()
	{				
		$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('refugee_card_print')));
		$meta = array('page_title' => lang('refugee_card_print'), 'bc' => $bc);	
		$this->page_construct('application_reports/refugee_card_print',$meta, $this->data);	
	} 
	
	public function refugee_travel_document_print()
	{				
		$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('refugee_travel_document_print')));
		$meta = array('page_title' => lang('refugee_travel_document_print'), 'bc' => $bc);	
		$this->page_construct('application_reports/refugee_travel_document_print',$meta, $this->data);	
	} 
	
	public function refugee_cessation_print()
	{				
		$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('refugee_cessation_print')));
		$meta = array('page_title' => lang('refugee_cessation_print'), 'bc' => $bc);	
		$this->page_construct('application_reports/refugee_cessation_print',$meta, $this->data);	
	} 
	
	public function refugee_withdrawal_print()
	{				
		$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('refugee_withdrawal_print')));
		$meta = array('page_title' => lang('refugee_withdrawal_print'), 'bc' => $bc);	
		$this->page_construct('application_reports/refugee_withdrawal_print',$meta, $this->data);	
	} 
	
	public function refugee_recognition_export() 
	{
		$this->load->library('excel');
		$styleArray = array(
			'font'  => array(
				'size'  => 8,
				'name'  => 'Khmer OS Siemreap',
			));		
		$this->excel->getActiveSheet()->getStyle('A1:O1')->applyFromArray($styleArray);
		$this->excel->getActiveSheet()->getRowDimension('1')->setRowHeight(20);		
		$this->excel->getActiveSheet()->getStyle('A1:O1')->getFont()->setBold(true)->setSize(10);
		$this->excel->getActiveSheet()->getStyle('A1:O1')->getFont()->getColor()->setRGB('FFFFFF'); 
		$this->excel->getActiveSheet()->getStyle('A1:O1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('A1:O1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('A1:O1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('555555');
			
		$this->excel->setActiveSheetIndex(0);
		$title = lang("ការទទួលស្គាល់ជនភៀសខ្លួន");		
        $this->excel->getActiveSheet()->setTitle($title);
        $this->excel->getActiveSheet()->SetCellValue('A1', lang('ល.រ'));
        $this->excel->getActiveSheet()->SetCellValue('B1', lang('លេខករណី'));
        $this->excel->getActiveSheet()->SetCellValue('C1', lang('ឈ្មោះ'));
        $this->excel->getActiveSheet()->SetCellValue('D1', lang('ឈ្មោះជាអក្សរទ្បាតាំង'));
        $this->excel->getActiveSheet()->SetCellValue('E1', lang('ភេទ'));
        $this->excel->getActiveSheet()->SetCellValue('F1', lang('ថ្ងៃខែឆ្នាំកំណើត'));
        $this->excel->getActiveSheet()->SetCellValue('G1', lang('សញ្ជាតិ'));
		$this->excel->getActiveSheet()->SetCellValue('H1', lang('សាសនា'));
		$this->excel->getActiveSheet()->SetCellValue('I1', lang('ជាតិពន្ធុ'));
        $this->excel->getActiveSheet()->SetCellValue('J1', lang('ទំនាក់'));
		$this->excel->getActiveSheet()->SetCellValue('K1', lang('អាសយដ្ឋាន')); 
		$this->excel->getActiveSheet()->SetCellValue('L1', lang('ទីកន្លែងកំណើត'));
		$this->excel->getActiveSheet()->SetCellValue('M1', lang('ថ្ងៃប្រគល់ប្រកាស'));
		$this->excel->getActiveSheet()->SetCellValue('N1', lang('ថ្ងៃចេញប្រកាស'));
		$results=$this->applications->getRefugeeRecognitionReports();
		$i=2;
		foreach($results as $row){
			$relationship = $this->applications->getRelationshipById($row->relationship);
			$country = $this->applications->getCountryById($row->country);
			$province = $this->applications->getProvinceById($row->province);
			$commune = $this->applications->getCommunceById($row->commune);						
			$district = $this->applications->getDistrictById($row->district);
			$relationship1_detail = lang("ម្ចាស់ករណី");
			if($relationship){
				$relationship1_detail = $relationship->relationship_kh;
			}
			$this->excel->getActiveSheet()->SetCellValue('A'.$i, $i);
			$this->excel->getActiveSheet()->SetCellValue('B'.$i, $row->case_prefix."".$row->case_no);
			$this->excel->getActiveSheet()->SetCellValue('C'.$i, $row->firstname_kh." ".$row->lastname_kh);
			$this->excel->getActiveSheet()->SetCellValue('D'.$i, $row->firstname." ".$row->lastname);
			$this->excel->getActiveSheet()->SetCellValue('E'.$i, lang($row->gender));
			$this->excel->getActiveSheet()->SetCellValue('F'.$i, $this->erp->toKhmer($row->dob));
			$this->excel->getActiveSheet()->SetCellValue('G'.$i, $row->nationality_kh." / ".$row->nationality);
			$this->excel->getActiveSheet()->SetCellValue('H'.$i, $row->religion_kh." / ".$row->religion);
			$this->excel->getActiveSheet()->SetCellValue('I'.$i, $row->ethnicity_kh." / ".$row->ethnicity);
			$this->excel->getActiveSheet()->SetCellValue('J'.$i, $relationship1_detail);
			$this->excel->getActiveSheet()->SetCellValue('K'.$i, $commune->native_name." ".$district->native_name." ".$province->native_name);			
			$this->excel->getActiveSheet()->SetCellValue('L'.$i, $row->pob_kh." / ".$row->pob);
			$this->excel->getActiveSheet()->SetCellValue('M'.$i, $this->erp->toKhmer($row->recognized_date));
			$this->excel->getActiveSheet()->SetCellValue('N'.$i, $this->erp->toKhmer($row->provided_date));
			$this->excel->getActiveSheet()->getStyle('A'.$i.':O'.$i.'')->applyFromArray($styleArray);
			$i++;
		}
		$this->excel->getActiveSheet()->getStyle('A'.$i.':P'.$i)->applyFromArray($styleArray);
		$this->excel->getActiveSheet()->mergeCells('A'.$i.':P'.$i);
		$this->excel->getActiveSheet()->SetCellValue('A'.$i, "យោងតាមដំណរភ្ជាប់៖".$_SERVER['HTTP_REFERER']."  , ប្រព័ន្ធគ្រប់គ្រង៖ © 2018 អគ្គនាយកដ្ឋានអន្តោប្រវេសន៍ (v1.0 ) ");
		
        $this->excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);
        $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);                		
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$title.'.xls"');
        header('Cache-Control: max-age=0');                
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        $objWriter->save('php://output');exit();
	}
	 
	public function refugee_permanent_card_export() 
	{
		$this->load->library('excel');
		$styleArray = array(
			'font'  => array(
				'size'  => 8,
				'name'  => 'Khmer OS Siemreap',
			));		
		$this->excel->getActiveSheet()->getStyle('A1:O1')->applyFromArray($styleArray);
		$this->excel->getActiveSheet()->getRowDimension('1')->setRowHeight(20);		
		$this->excel->getActiveSheet()->getStyle('A1:O1')->getFont()->setBold(true)->setSize(10);
		$this->excel->getActiveSheet()->getStyle('A1:O1')->getFont()->getColor()->setRGB('FFFFFF'); 
		$this->excel->getActiveSheet()->getStyle('A1:O1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('A1:O1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('A1:O1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('555555');
			
		$this->excel->setActiveSheetIndex(0);
		$title = lang("ពាក្យសុំប័ណ្ណស្នាក់នៅ");		
        $this->excel->getActiveSheet()->setTitle($title);
        $this->excel->getActiveSheet()->SetCellValue('A1', lang('ល.រ'));
        $this->excel->getActiveSheet()->SetCellValue('B1', lang('លេខករណី'));
        $this->excel->getActiveSheet()->SetCellValue('C1', lang('ឈ្មោះ'));
        $this->excel->getActiveSheet()->SetCellValue('D1', lang('ឈ្មោះជាអក្សរទ្បាតាំង'));
        $this->excel->getActiveSheet()->SetCellValue('E1', lang('ភេទ'));
        $this->excel->getActiveSheet()->SetCellValue('F1', lang('ថ្ងៃខែឆ្នាំកំណើត'));
        $this->excel->getActiveSheet()->SetCellValue('G1', lang('សញ្ជាតិ'));
		$this->excel->getActiveSheet()->SetCellValue('H1', lang('សាសនា'));
		$this->excel->getActiveSheet()->SetCellValue('I1', lang('ជាតិពន្ធុ'));
        $this->excel->getActiveSheet()->SetCellValue('J1', lang('ទំនាក់'));
		$this->excel->getActiveSheet()->SetCellValue('K1', lang('អាសយដ្ឋាន'));	
		$this->excel->getActiveSheet()->SetCellValue('L1', lang('មុខរបរ'));
		$this->excel->getActiveSheet()->SetCellValue('M1', lang('ទីកន្លែងកំណើត')); 
		$this->excel->getActiveSheet()->SetCellValue('N1', lang('ថ្ងៃផុតកំណត់')); 
		$results=$this->applications->getRefugeePermanentReports();
		$i=2;
		foreach($results as $row){
			$relationship = $this->applications->getRelationshipById($row->relationship);
			$country = $this->applications->getCountryById($row->country);
			$province = $this->applications->getProvinceById($row->province);
			$commune = $this->applications->getCommunceById($row->commune);						
			$district = $this->applications->getDistrictById($row->district);
			$relationship1_detail = lang("ម្ចាស់ករណី");
			if($relationship){
				$relationship1_detail = $relationship->relationship_kh;
			}
			$this->excel->getActiveSheet()->SetCellValue('A'.$i, $i);
			$this->excel->getActiveSheet()->SetCellValue('B'.$i, $row->case_prefix."".$row->case_no);
			$this->excel->getActiveSheet()->SetCellValue('C'.$i, $row->firstname_kh." ".$row->lastname_kh);
			$this->excel->getActiveSheet()->SetCellValue('D'.$i, $row->firstname." ".$row->lastname);
			$this->excel->getActiveSheet()->SetCellValue('E'.$i, lang($row->gender));
			$this->excel->getActiveSheet()->SetCellValue('F'.$i, $this->erp->toKhmer($row->dob));
			$this->excel->getActiveSheet()->SetCellValue('G'.$i, $row->nationality_kh." / ".$row->nationality);
			$this->excel->getActiveSheet()->SetCellValue('H'.$i, $row->religion_kh." / ".$row->religion);
			$this->excel->getActiveSheet()->SetCellValue('I'.$i, $row->ethnicity_kh." / ".$row->religion);
			$this->excel->getActiveSheet()->SetCellValue('J'.$i, $relationship1_detail);
			$this->excel->getActiveSheet()->SetCellValue('K'.$i, $commune->native_name." ".$district->native_name." ".$province->native_name);			
			$this->excel->getActiveSheet()->SetCellValue('L'.$i, $row->occupation_kh);
			$this->excel->getActiveSheet()->SetCellValue('M'.$i, $row->pob_kh." / ".$row->pob); 
			$this->excel->getActiveSheet()->SetCellValue('N'.$i, $this->erp->toKhmer($row->expiry_date)); 
			$this->excel->getActiveSheet()->getStyle('A'.$i.':O'.$i.'')->applyFromArray($styleArray);
			$i++;
		}
		$this->excel->getActiveSheet()->getStyle('A'.$i.':P'.$i)->applyFromArray($styleArray);
		$this->excel->getActiveSheet()->mergeCells('A'.$i.':P'.$i);
		$this->excel->getActiveSheet()->SetCellValue('A'.$i, "យោងតាមដំណរភ្ជាប់៖".$_SERVER['HTTP_REFERER']."  , ប្រព័ន្ធគ្រប់គ្រង៖ © 2018 អគ្គនាយកដ្ឋានអន្តោប្រវេសន៍ (v1.0 ) ");
		
        $this->excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);
        $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);                		
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$title.'.xls"');
        header('Cache-Control: max-age=0');                
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        $objWriter->save('php://output');exit();
	}
	
	public function refugee_card_export() 
	{
		$this->load->library('excel');
		$styleArray = array(
			'font'  => array(
				'size'  => 8,
				'name'  => 'Khmer OS Siemreap',
			));		
		$this->excel->getActiveSheet()->getStyle('A1:Q1')->applyFromArray($styleArray);
		$this->excel->getActiveSheet()->getRowDimension('1')->setRowHeight(20);		
		$this->excel->getActiveSheet()->getStyle('A1:Q1')->getFont()->setBold(true)->setSize(10);
		$this->excel->getActiveSheet()->getStyle('A1:Q1')->getFont()->getColor()->setRGB('FFFFFF'); 
		$this->excel->getActiveSheet()->getStyle('A1:Q1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('A1:Q1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('A1:Q1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('555555');
			
		$this->excel->setActiveSheetIndex(0);
		$title = lang("ពាក្យសុំប័ណ្ណសំគាល់ជនភៀសខ្លួន");		
        $this->excel->getActiveSheet()->setTitle($title);
        $this->excel->getActiveSheet()->SetCellValue('A1', lang('ល.រ'));
        $this->excel->getActiveSheet()->SetCellValue('B1', lang('លេខករណី'));
        $this->excel->getActiveSheet()->SetCellValue('C1', lang('ឈ្មោះ'));
        $this->excel->getActiveSheet()->SetCellValue('D1', lang('ឈ្មោះជាអក្សរទ្បាតាំង'));
        $this->excel->getActiveSheet()->SetCellValue('E1', lang('ភេទ'));
        $this->excel->getActiveSheet()->SetCellValue('F1', lang('ថ្ងៃខែឆ្នាំកំណើត'));
        $this->excel->getActiveSheet()->SetCellValue('G1', lang('សញ្ជាតិ'));
		$this->excel->getActiveSheet()->SetCellValue('H1', lang('សាសនា'));
		$this->excel->getActiveSheet()->SetCellValue('I1', lang('ជាតិពន្ធុ'));
        $this->excel->getActiveSheet()->SetCellValue('J1', lang('ទំនាក់'));
		$this->excel->getActiveSheet()->SetCellValue('K1', lang('អាសយដ្ឋាន'));	
		$this->excel->getActiveSheet()->SetCellValue('L1', lang('មុខរបរ'));
		$this->excel->getActiveSheet()->SetCellValue('M1', lang('ទីកន្លែងកំណើត')); 
		$this->excel->getActiveSheet()->SetCellValue('N1', lang('ការបរិច្ឆេទសុំ'));
		$this->excel->getActiveSheet()->SetCellValue('O1', lang('ថ្ងៃចុះហត្ថលេខា')); 
		$this->excel->getActiveSheet()->SetCellValue('P1', lang('ថ្ងៃផុតកំណត់')); 
		$this->excel->getActiveSheet()->SetCellValue('Q1', lang('ហេតុផល')); 
		$results=$this->applications->getRefugeeCardReports();
		$i=2;
		foreach($results as $row){
			$relationship = $this->applications->getRelationshipById($row->relationship);
			$country = $this->applications->getCountryById($row->country);
			$province = $this->applications->getProvinceById($row->province);
			$commune = $this->applications->getCommunceById($row->commune);						
			$district = $this->applications->getDistrictById($row->district);
			$relationship1_detail = lang("ម្ចាស់ករណី");
			if($relationship){
				$relationship1_detail = $relationship->relationship_kh;
			}
			$this->excel->getActiveSheet()->SetCellValue('A'.$i, $i);
			$this->excel->getActiveSheet()->SetCellValue('B'.$i, $row->case_prefix."".$row->case_no);
			$this->excel->getActiveSheet()->SetCellValue('C'.$i, $row->firstname_kh." ".$row->lastname_kh);
			$this->excel->getActiveSheet()->SetCellValue('D'.$i, $row->firstname." ".$row->lastname);
			$this->excel->getActiveSheet()->SetCellValue('E'.$i, lang($row->gender));
			$this->excel->getActiveSheet()->SetCellValue('F'.$i, $this->erp->toKhmer($row->dob));
			$this->excel->getActiveSheet()->SetCellValue('G'.$i, $row->nationality_kh." / ".$row->nationality);
			$this->excel->getActiveSheet()->SetCellValue('H'.$i, $row->religion_kh." / ".$row->religion);
			$this->excel->getActiveSheet()->SetCellValue('I'.$i, $row->ethnicity_kh." / ".$row->ethnicity);
			$this->excel->getActiveSheet()->SetCellValue('J'.$i, $relationship1_detail);
			$this->excel->getActiveSheet()->SetCellValue('K'.$i, $commune->native_name." ".$district->native_name." ".$province->native_name);			
			$this->excel->getActiveSheet()->SetCellValue('L'.$i, $row->occupation_kh);
			$this->excel->getActiveSheet()->SetCellValue('M'.$i, $row->pob_kh." / ".$row->pob); 
			$this->excel->getActiveSheet()->SetCellValue('N'.$i, $this->erp->toKhmer($row->created_date1)); 
			$this->excel->getActiveSheet()->SetCellValue('O'.$i, $this->erp->toKhmer($row->date_sign)); 
			$this->excel->getActiveSheet()->SetCellValue('P'.$i, $this->erp->toKhmer($row->expiry_date)); 
			$this->excel->getActiveSheet()->SetCellValue('Q'.$i, $row->name_kh); 
			$this->excel->getActiveSheet()->getStyle('A'.$i.':Q'.$i.'')->applyFromArray($styleArray);
			$i++;
		}
		$this->excel->getActiveSheet()->getStyle('A'.$i.':Q'.$i)->applyFromArray($styleArray);
		$this->excel->getActiveSheet()->mergeCells('A'.$i.':Q'.$i);
		$this->excel->getActiveSheet()->SetCellValue('A'.$i, "យោងតាមដំណរភ្ជាប់៖".$_SERVER['HTTP_REFERER']."  , ប្រព័ន្ធគ្រប់គ្រង៖ © 2018 អគ្គនាយកដ្ឋានអន្តោប្រវេសន៍ (v1.0 ) ");
		
        $this->excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('P')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('P')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('Q')->setAutoSize(true);
        $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);                		
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$title.'.xls"');
        header('Cache-Control: max-age=0');                
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        $objWriter->save('php://output');exit();
	}
	 
	public function refugee_travel_document_export() 
	{
		$this->load->library('excel');
		$styleArray = array(
			'font'  => array(
				'size'  => 8,
				'name'  => 'Khmer OS Siemreap',
			));		
		$this->excel->getActiveSheet()->getStyle('A1:X1')->applyFromArray($styleArray);
		$this->excel->getActiveSheet()->getRowDimension('1')->setRowHeight(20);		
		$this->excel->getActiveSheet()->getStyle('A1:X1')->getFont()->setBold(true)->setSize(10);
		$this->excel->getActiveSheet()->getStyle('A1:X1')->getFont()->getColor()->setRGB('FFFFFF'); 
		$this->excel->getActiveSheet()->getStyle('A1:X1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('A1:X1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('A1:X1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('555555');
			
		$this->excel->setActiveSheetIndex(0);
		$title = lang("ឯកសារធ្វើដំណើរជនភៀសខ្លួន");		
        $this->excel->getActiveSheet()->setTitle($title);
        $this->excel->getActiveSheet()->SetCellValue('A1', lang('ល.រ'));
        $this->excel->getActiveSheet()->SetCellValue('B1', lang('លេខករណី'));
        $this->excel->getActiveSheet()->SetCellValue('C1', lang('ឈ្មោះ'));
        $this->excel->getActiveSheet()->SetCellValue('D1', lang('ឈ្មោះជាអក្សរទ្បាតាំង'));
        $this->excel->getActiveSheet()->SetCellValue('E1', lang('ភេទ'));
        $this->excel->getActiveSheet()->SetCellValue('F1', lang('ថ្ងៃខែឆ្នាំកំណើត'));
        $this->excel->getActiveSheet()->SetCellValue('G1', lang('សញ្ជាតិ'));
		$this->excel->getActiveSheet()->SetCellValue('H1', lang('សាសនា'));
		$this->excel->getActiveSheet()->SetCellValue('I1', lang('ជាតិពន្ធុ'));
        $this->excel->getActiveSheet()->SetCellValue('J1', lang('ទំនាក់'));
		$this->excel->getActiveSheet()->SetCellValue('K1', lang('អាសយដ្ឋាន'));	
		$this->excel->getActiveSheet()->SetCellValue('L1', lang('លេខទំនាក់ទំនង សាមុីខ្លួន'));	
		$this->excel->getActiveSheet()->SetCellValue('M1', lang('មុខរបរ'));
		$this->excel->getActiveSheet()->SetCellValue('N1', lang('ទីកន្លែងកំណើត')); 
		$this->excel->getActiveSheet()->SetCellValue('O1', lang('កម្ពស់'));  
		$this->excel->getActiveSheet()->SetCellValue('P1', lang('សម្បុរ'));  
		$this->excel->getActiveSheet()->SetCellValue('Q1', lang('ផ្ទៃមុខ'));  
		$this->excel->getActiveSheet()->SetCellValue('R1', lang('សក់'));  
		$this->excel->getActiveSheet()->SetCellValue('S1', lang('ស្លាកស្នាមពិសេស'));  
		$this->excel->getActiveSheet()->SetCellValue('T1', lang('ទៅប្រទេស'));  
		$this->excel->getActiveSheet()->SetCellValue('U1', lang('ដើម្បី'));  
		$this->excel->getActiveSheet()->SetCellValue('V1', lang('លេខទំនាក់ទំនងពេលមានអាសន្ន'));  
		$this->excel->getActiveSheet()->SetCellValue('W1', lang('អាសយដ្ឋានពេលមានអាសន្ន'));   
		$results=$this->applications->getRefugeeTravelDocumentReports();
		$i=2;
		foreach($results as $row){
			$relationship = $this->applications->getRelationshipById($row->relationship);
			$country = $this->applications->getCountryById($row->country);
			$province = $this->applications->getProvinceById($row->province);
			$commune = $this->applications->getCommunceById($row->commune);						
			$district = $this->applications->getDistrictById($row->district);
			$relationship1_detail = lang("ម្ចាស់ករណី");
			if($relationship){
				$relationship1_detail = $relationship->relationship_kh;
			}
			$this->excel->getActiveSheet()->SetCellValue('A'.$i, $i);
			$this->excel->getActiveSheet()->SetCellValue('B'.$i, $row->case_prefix."".$row->case_no);
			$this->excel->getActiveSheet()->SetCellValue('C'.$i, $row->firstname_kh." ".$row->lastname_kh);
			$this->excel->getActiveSheet()->SetCellValue('D'.$i, $row->firstname." ".$row->lastname);
			$this->excel->getActiveSheet()->SetCellValue('E'.$i, lang($row->gender));
			$this->excel->getActiveSheet()->SetCellValue('F'.$i, $row->dob);
			$this->excel->getActiveSheet()->SetCellValue('G'.$i, $row->nationality_kh." / ".$row->nationality);
			$this->excel->getActiveSheet()->SetCellValue('H'.$i, $row->religion_kh." / ".$row->religion);
			$this->excel->getActiveSheet()->SetCellValue('I'.$i, $row->ethnicity_kh." / ".$row->ethnicity_kh);
			$this->excel->getActiveSheet()->SetCellValue('J'.$i, $relationship1_detail);
			$this->excel->getActiveSheet()->SetCellValue('K'.$i, $commune->native_name." ".$district->native_name." ".$province->native_name);			
			$this->excel->getActiveSheet()->SetCellValue('L'.$i, $row->phone1);
			$this->excel->getActiveSheet()->SetCellValue('M'.$i, $row->occupation_kh." / ".$row->occupation); 
			$this->excel->getActiveSheet()->SetCellValue('N'.$i, $row->pob); 
			$this->excel->getActiveSheet()->SetCellValue('O'.$i, $row->height); 
			$this->excel->getActiveSheet()->SetCellValue('P'.$i, $row->complexion); 
			$this->excel->getActiveSheet()->SetCellValue('Q'.$i, $row->shape_of_face); 
			$this->excel->getActiveSheet()->SetCellValue('R'.$i, $row->hair); 
			$this->excel->getActiveSheet()->SetCellValue('S'.$i, $row->distinguishing_marks); 
			$this->excel->getActiveSheet()->SetCellValue('T'.$i, $row->country); 
			$this->excel->getActiveSheet()->SetCellValue('U'.$i, $row->travel); 
			$this->excel->getActiveSheet()->SetCellValue('V'.$i, $row->emergencies_contact); 
			$this->excel->getActiveSheet()->SetCellValue('W'.$i, $row->emergencies_address); 
			$this->excel->getActiveSheet()->getStyle('A'.$i.':X'.$i.'')->applyFromArray($styleArray);
			$i++;
		}
		$this->excel->getActiveSheet()->getStyle('A'.$i.':P'.$i)->applyFromArray($styleArray);
		$this->excel->getActiveSheet()->mergeCells('A'.$i.':P'.$i);
		$this->excel->getActiveSheet()->SetCellValue('A'.$i, "យោងតាមដំណរភ្ជាប់៖".$_SERVER['HTTP_REFERER']."  , ប្រព័ន្ធគ្រប់គ្រង៖ © 2018 អគ្គនាយកដ្ឋានអន្តោប្រវេសន៍ (v1.0 ) ");
		
        $this->excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('P')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('Q')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('R')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('S')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('T')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('U')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('V')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('W')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('X')->setAutoSize(true);
        $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);                		
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$title.'.xls"');
        header('Cache-Control: max-age=0');                
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        $objWriter->save('php://output');exit();
	}
	 
	public function refugee_cessation_export() 
	{
	$this->load->library('excel');
        
		$styleArray = array(
			'font'  => array(
				//'bold'  => true,
				//'color' => array('rgb' => 'FF0000'),
				'size'  => 8,
				'name'  => 'Khmer OS Siemreap',
			));		
		$this->excel->getActiveSheet()->getStyle('A1:O1')->applyFromArray($styleArray);
		$this->excel->getActiveSheet()->getRowDimension('1')->setRowHeight(20);		
		$this->excel->getActiveSheet()->getStyle('A1:O1')->getFont()->setBold(true)->setSize(10);
		$this->excel->getActiveSheet()->getStyle('A1:O1')->getFont()->getColor()->setRGB('FFFFFF'); 
		$this->excel->getActiveSheet()->getStyle('A1:O1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('A1:O1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('A1:O1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('555555');
			
		$this->excel->setActiveSheetIndex(0);
		$title = lang(" បែបបទសុំបញ្ឈប់ឋានៈជនភៀសខ្លួន");		
        $this->excel->getActiveSheet()->setTitle($title);
        $this->excel->getActiveSheet()->SetCellValue('A1', lang('ល.រ'));
        $this->excel->getActiveSheet()->SetCellValue('B1', lang('លេខករណី'));
        $this->excel->getActiveSheet()->SetCellValue('C1', lang('ឈ្មោះ'));
        $this->excel->getActiveSheet()->SetCellValue('D1', lang('ឈ្មោះជាអក្សរទ្បាតាំង'));
        $this->excel->getActiveSheet()->SetCellValue('E1', lang('ភេទ'));
        $this->excel->getActiveSheet()->SetCellValue('F1', lang('ថ្ងៃខែឆ្នាំកំណើត'));
        $this->excel->getActiveSheet()->SetCellValue('G1', lang('សញ្ជាតិ'));
		$this->excel->getActiveSheet()->SetCellValue('H1', lang('សាសនា'));
		$this->excel->getActiveSheet()->SetCellValue('I1', lang('ជាតិពន្ធុ'));
        $this->excel->getActiveSheet()->SetCellValue('J1', lang('ទំនាក់'));
		$this->excel->getActiveSheet()->SetCellValue('K1', lang('អាសយដ្ឋាន'));	
		$this->excel->getActiveSheet()->SetCellValue('L1', lang('លេខទំនាក់ទំនង'));
		$this->excel->getActiveSheet()->SetCellValue('M1', lang('ទីកន្លែងកំណើត'));
		$results=$this->applications->getRefugeeCessationReports();
		$i=2;
		foreach($results as $row){
			$relationship = $this->applications->getRelationshipById($row->relationship);
			$country = $this->applications->getCountryById($row->country);
			$province = $this->applications->getProvinceById($row->province);
			$commune = $this->applications->getCommunceById($row->commune);						
			$district = $this->applications->getDistrictById($row->district);

			$relationship1_detail = lang("ម្ចាស់ករណី");
			if($relationship){
				$relationship1_detail = $relationship->relationship_kh;
			}
			$this->excel->getActiveSheet()->SetCellValue('A'.$i, $i);
			$this->excel->getActiveSheet()->SetCellValue('B'.$i, $row->case_prefix."".$row->case_no);
			$this->excel->getActiveSheet()->SetCellValue('C'.$i, $row->firstname_kh." ".$row->lastname_kh);
			$this->excel->getActiveSheet()->SetCellValue('D'.$i, $row->firstname." ".$row->lastname);
			$this->excel->getActiveSheet()->SetCellValue('E'.$i, lang($row->gender));
			$this->excel->getActiveSheet()->SetCellValue('F'.$i, $this->erp->toKhmer($row->dob));
			$this->excel->getActiveSheet()->SetCellValue('G'.$i, $row->nationality_kh." / ".$row->nationality);
			$this->excel->getActiveSheet()->SetCellValue('H'.$i, $row->religion_kh." / ".$row->religion);
			$this->excel->getActiveSheet()->SetCellValue('I'.$i, $row->ethnicity_kh." / ".$row->ethnicity);
			$this->excel->getActiveSheet()->SetCellValue('J'.$i, $relationship1_detail);
			$this->excel->getActiveSheet()->SetCellValue('K'.$i, $commune->native_name." ".$district->native_name." ".$province->native_name);			
			$this->excel->getActiveSheet()->SetCellValue('L'.$i, $this->erp->toKhmer($row->phone));
			$this->excel->getActiveSheet()->SetCellValue('M'.$i, $row->pob_kh." / ".$row->pob);
			$this->excel->getActiveSheet()->getStyle('A'.$i.':O'.$i.'')->applyFromArray($styleArray);
			$i++;
		}
		$this->excel->getActiveSheet()->getStyle('A'.$i.':P'.$i)->applyFromArray($styleArray);
		$this->excel->getActiveSheet()->mergeCells('A'.$i.':P'.$i);
		$this->excel->getActiveSheet()->SetCellValue('A'.$i, "យោងតាមដំណរភ្ជាប់៖".$_SERVER['HTTP_REFERER']."  , ប្រព័ន្ធគ្រប់គ្រង៖ © 2018 អគ្គនាយកដ្ឋានអន្តោប្រវេសន៍ (v1.0 ) ");
		
        $this->excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
		
        $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);                
		
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$title.'.xls"');
        header('Cache-Control: max-age=0');                
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        $objWriter->save('php://output');exit();
	}
	 
	/* 
		Description: Export Excel Report For K3 ( Add report follow image in WhatApp)
		Athor:       Bunthean

	 */

	public function refugee_withdrawal_export(){
		$this->load->library('excel');
        
		$styleArray = array(
			'font'  => array(
				//'bold'  => true,
				//'color' => array('rgb' => 'FF0000'),
				'size'  => 8,
				'name'  => 'Khmer OS Siemreap',
			));		
		$this->excel->getActiveSheet()->getStyle('A1:O1')->applyFromArray($styleArray);
		$this->excel->getActiveSheet()->getRowDimension('1')->setRowHeight(20);		
		$this->excel->getActiveSheet()->getStyle('A1:O1')->getFont()->setBold(true)->setSize(10);
		$this->excel->getActiveSheet()->getStyle('A1:O1')->getFont()->getColor()->setRGB('FFFFFF'); 
		$this->excel->getActiveSheet()->getStyle('A1:O1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('A1:O1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('A1:O1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('555555');
			
		$this->excel->setActiveSheetIndex(0);
		$title = lang(" បែបបទលុបឋានៈពីជនភៀសខ្លួន");		
        $this->excel->getActiveSheet()->setTitle($title);
        $this->excel->getActiveSheet()->SetCellValue('A1', lang('ល.រ'));
        $this->excel->getActiveSheet()->SetCellValue('B1', lang('លេខករណី'));
        $this->excel->getActiveSheet()->SetCellValue('C1', lang('ឈ្មោះ'));
        $this->excel->getActiveSheet()->SetCellValue('D1', lang('ឈ្មោះជាអក្សរទ្បាតាំង'));
        $this->excel->getActiveSheet()->SetCellValue('E1', lang('ភេទ'));
        $this->excel->getActiveSheet()->SetCellValue('F1', lang('ថ្ងៃខែឆ្នាំកំណើត'));
        $this->excel->getActiveSheet()->SetCellValue('G1', lang('សញ្ជាតិ'));
		$this->excel->getActiveSheet()->SetCellValue('H1', lang('សាសនា'));
		$this->excel->getActiveSheet()->SetCellValue('I1', lang('ជាតិពន្ធុ'));
        $this->excel->getActiveSheet()->SetCellValue('J1', lang('ទំនាក់'));
		$this->excel->getActiveSheet()->SetCellValue('K1', lang('អាសយដ្ឋាន'));	
		$this->excel->getActiveSheet()->SetCellValue('L1', lang('លេខទំនាក់ទំនង'));
		$this->excel->getActiveSheet()->SetCellValue('M1', lang('ទីកន្លែងកំណើត'));
		$results=$this->applications->getRefugeeWithdrawalReports();
		$i=2;
		foreach($results as $row){
			$relationship = $this->applications->getRelationshipById($row->relationship);
			$country = $this->applications->getCountryById($row->country);
			$province = $this->applications->getProvinceById($row->province);
			$commune = $this->applications->getCommunceById($row->commune);						
			$district = $this->applications->getDistrictById($row->district);

			$relationship1_detail = lang("ម្ចាស់ករណី");
			if($relationship){
				$relationship1_detail = $relationship->relationship_kh;
			}
			$this->excel->getActiveSheet()->SetCellValue('A'.$i, $i);
			$this->excel->getActiveSheet()->SetCellValue('B'.$i, $row->case_prefix."".$row->case_no);
			$this->excel->getActiveSheet()->SetCellValue('C'.$i, $row->firstname_kh." ".$row->lastname_kh);
			$this->excel->getActiveSheet()->SetCellValue('D'.$i, $row->firstname." ".$row->lastname);
			$this->excel->getActiveSheet()->SetCellValue('E'.$i, lang($row->gender));
			$this->excel->getActiveSheet()->SetCellValue('F'.$i, $this->erp->toKhmer($row->dob));
			$this->excel->getActiveSheet()->SetCellValue('G'.$i, $row->nationality_kh." / ".$row->nationality);
			$this->excel->getActiveSheet()->SetCellValue('H'.$i, $row->religion_kh." / ".$row->religion);
			$this->excel->getActiveSheet()->SetCellValue('I'.$i, $row->ethnicity_kh." / ".$row->ethnicity);
			$this->excel->getActiveSheet()->SetCellValue('J'.$i, $relationship1_detail);
			$this->excel->getActiveSheet()->SetCellValue('K'.$i, $commune->native_name." ".$district->native_name." ".$province->native_name);			
			$this->excel->getActiveSheet()->SetCellValue('L'.$i, $this->erp->toKhmer($row->phone));
			$this->excel->getActiveSheet()->SetCellValue('M'.$i, $row->pob_kh." / ".$row->pob);
			$this->excel->getActiveSheet()->getStyle('A'.$i.':O'.$i.'')->applyFromArray($styleArray);
			$i++;
		}
		$this->excel->getActiveSheet()->getStyle('A'.$i.':P'.$i)->applyFromArray($styleArray);
		$this->excel->getActiveSheet()->mergeCells('A'.$i.':P'.$i);
		$this->excel->getActiveSheet()->SetCellValue('A'.$i, "យោងតាមដំណរភ្ជាប់៖".$_SERVER['HTTP_REFERER']."  , ប្រព័ន្ធគ្រប់គ្រង៖ © 2018 អគ្គនាយកដ្ឋានអន្តោប្រវេសន៍ (v1.0 ) ");
		
        $this->excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
		
        $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);                
		
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$title.'.xls"');
        header('Cache-Control: max-age=0');                
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        $objWriter->save('php://output');exit();
	}
	
	public function first_reject_export()
	{
	 
	   $this->load->library('excel');
        
		$styleArray = array(
			'font'  => array(
				'size'  => 8,
				'name'  => 'Khmer OS Siemreap',
			));		
		$this->excel->getActiveSheet()->getStyle('A1:O1')->applyFromArray($styleArray);
		$this->excel->getActiveSheet()->getRowDimension('1')->setRowHeight(20);		
		$this->excel->getActiveSheet()->getStyle('A1:O1')->getFont()->setBold(true)->setSize(10);
		$this->excel->getActiveSheet()->getStyle('A1:O1')->getFont()->getColor()->setRGB('FFFFFF'); 
		$this->excel->getActiveSheet()->getStyle('A1:O1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('A1:O1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('A1:O1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('555555');
			
		$this->excel->setActiveSheetIndex(0);
		$title = lang("របាយការណ៍បដិសេធជាលើកទីមួយ");		
        $this->excel->getActiveSheet()->setTitle($title);
        $this->excel->getActiveSheet()->SetCellValue('A1', lang('ល.រ'));
        $this->excel->getActiveSheet()->SetCellValue('B1', lang('លេខករណី'));
        $this->excel->getActiveSheet()->SetCellValue('C1', lang('ឈ្មោះ'));
        $this->excel->getActiveSheet()->SetCellValue('D1', lang('ឈ្មោះជាអក្សរទ្បាតាំង'));
        $this->excel->getActiveSheet()->SetCellValue('E1', lang('ភេទ'));
        $this->excel->getActiveSheet()->SetCellValue('F1', lang('ថ្ងៃខែឆ្នាំកំណើត'));
        $this->excel->getActiveSheet()->SetCellValue('G1', lang('សញ្ជាតិ'));
		$this->excel->getActiveSheet()->SetCellValue('H1', lang('សាសនា'));
		$this->excel->getActiveSheet()->SetCellValue('I1', lang('ជាតិពន្ធុ')); 
		$this->excel->getActiveSheet()->SetCellValue('J1', lang('អាសយដ្ឋាន'));	
		$this->excel->getActiveSheet()->SetCellValue('K1', lang('លេខទំនាក់ទំនង'));
		$this->excel->getActiveSheet()->SetCellValue('L1', lang('ទីកន្លែងកំណើត'));
		// $this->excel->getActiveSheet()->SetCellValue('M1', lang('កាលបរិច្ឆេទណាត់សម្ភាសន៏'));
		$this->excel->getActiveSheet()->SetCellValue('N1', lang('ថ្ងៃខែចុះបញ្ជី'));
		// $results=$this->applications->getInteviewAppointmentReports();
		$results=$this->applications->getFirstReject();
		
		$i=2;
		foreach($results as $row){
			$relationship = $this->applications->getRelationshipById($row->relationship);
			$country = $this->applications->getCountryById($row->country);
			$province = $this->applications->getProvinceById($row->province);
			$commune = $this->applications->getCommunceById($row->commune);						
			$district = $this->applications->getDistrictById($row->district); 
			$this->excel->getActiveSheet()->SetCellValue('A'.$i, $i-1);
			$this->excel->getActiveSheet()->SetCellValue('B'.$i, $row->case_prefix.$row->case_no);
			$this->excel->getActiveSheet()->SetCellValue('C'.$i, $row->firstname_kh." ".$row->lastname_kh);
			$this->excel->getActiveSheet()->SetCellValue('D'.$i, $row->firstname." ".$row->lastname);
			$this->excel->getActiveSheet()->SetCellValue('E'.$i, lang($row->gender));
			$this->excel->getActiveSheet()->SetCellValue('F'.$i, $this->erp->toKhmer($row->dob));
			$this->excel->getActiveSheet()->SetCellValue('G'.$i, $row->nationality_kh." / ".$row->nationality);
			$this->excel->getActiveSheet()->SetCellValue('H'.$i, $row->religion_kh." / ".$row->religion);
			$this->excel->getActiveSheet()->SetCellValue('I'.$i, $row->ethnicity_kh." / ".$row->religion); 
			$this->excel->getActiveSheet()->SetCellValue('J'.$i, $commune->native_name."  ".$district->native_name." ".$province->native_name);			
			$this->excel->getActiveSheet()->SetCellValue('K'.$i, $this->erp->toKhmer($row->phone));
			$this->excel->getActiveSheet()->SetCellValue('L'.$i, $row->pob_kh." / ".$row->pob);
			// $this->excel->getActiveSheet()->SetCellValue('M'.$i,  $this->erp->toKhmer($this->erp->hrsd($row->appointment_date)));
			$this->excel->getActiveSheet()->SetCellValue('N'.$i, $this->erp->toKhmer($this->erp->hrsd($row->created_date)));
			$this->excel->getActiveSheet()->getStyle('A'.$i.':O'.$i.'')->applyFromArray($styleArray);
			$i++;
		}
		$this->excel->getActiveSheet()->getStyle('A'.$i.':P'.$i)->applyFromArray($styleArray);
		$this->excel->getActiveSheet()->mergeCells('A'.$i.':P'.$i);
		$this->excel->getActiveSheet()->SetCellValue('A'.$i, "យោងតាមដំណរភ្ជាប់៖".$_SERVER['HTTP_REFERER']."  , ប្រព័ន្ធគ្រប់គ្រង៖ © 2018 អគ្គនាយកដ្ឋានអន្តោប្រវេសន៍ (v1.0 ) ");
		
        $this->excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true); 
		// $this->excel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true); 
		$this->excel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true); 
		
        $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);                
		
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$title.'.xls"');
        header('Cache-Control: max-age=0');                
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        $objWriter->save('php://output');exit();
	}

	/* 
		Description: Export Excel Report For K3 ( Add report follow image in WhatApp)
		Athor:       Bunthean

	 */
	
	public function first_recognise_export()
	{
	 
	   $this->load->library('excel');
        
		$styleArray = array(
			'font'  => array(
				'size'  => 8,
				'name'  => 'Khmer OS Siemreap',
			));		
		$this->excel->getActiveSheet()->getStyle('A1:O1')->applyFromArray($styleArray);
		$this->excel->getActiveSheet()->getRowDimension('1')->setRowHeight(20);		
		$this->excel->getActiveSheet()->getStyle('A1:O1')->getFont()->setBold(true)->setSize(10);
		$this->excel->getActiveSheet()->getStyle('A1:O1')->getFont()->getColor()->setRGB('FFFFFF'); 
		$this->excel->getActiveSheet()->getStyle('A1:O1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('A1:O1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('A1:O1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('555555');
			
		$this->excel->setActiveSheetIndex(0);
		$title = lang("របាយការណ៍ទទួលស្គាល់លើកទីមួយ");		
        $this->excel->getActiveSheet()->setTitle($title);
        $this->excel->getActiveSheet()->SetCellValue('A1', lang('ល.រ'));
        $this->excel->getActiveSheet()->SetCellValue('B1', lang('លេខករណី'));
        $this->excel->getActiveSheet()->SetCellValue('C1', lang('ឈ្មោះ'));
        $this->excel->getActiveSheet()->SetCellValue('D1', lang('ឈ្មោះជាអក្សរទ្បាតាំង'));
        $this->excel->getActiveSheet()->SetCellValue('E1', lang('ភេទ'));
        $this->excel->getActiveSheet()->SetCellValue('F1', lang('ថ្ងៃខែឆ្នាំកំណើត'));
        $this->excel->getActiveSheet()->SetCellValue('G1', lang('សញ្ជាតិ'));
		$this->excel->getActiveSheet()->SetCellValue('H1', lang('សាសនា'));
		$this->excel->getActiveSheet()->SetCellValue('I1', lang('ជាតិពន្ធុ')); 
		$this->excel->getActiveSheet()->SetCellValue('J1', lang('អាសយដ្ឋាន'));	
		$this->excel->getActiveSheet()->SetCellValue('K1', lang('លេខទំនាក់ទំនង'));
		$this->excel->getActiveSheet()->SetCellValue('L1', lang('ទីកន្លែងកំណើត'));
		// $this->excel->getActiveSheet()->SetCellValue('M1', lang('កាលបរិច្ឆេទណាត់សម្ភាសន៏'));
		$this->excel->getActiveSheet()->SetCellValue('N1', lang('ថ្ងៃខែចុះបញ្ជី'));

		// $results=$this->applications->getInteviewAppointmentReports();
		$results=$this->applications->getFirstRecognise();
		
		$i=2;
		foreach($results as $row){
			$relationship = $this->applications->getRelationshipById($row->relationship);
			$country = $this->applications->getCountryById($row->country);
			$province = $this->applications->getProvinceById($row->province);
			$commune = $this->applications->getCommunceById($row->commune);						
			$district = $this->applications->getDistrictById($row->district); 
			$this->excel->getActiveSheet()->SetCellValue('A'.$i, $i-1);
			$this->excel->getActiveSheet()->SetCellValue('B'.$i, $row->case_prefix.$row->case_no);
			$this->excel->getActiveSheet()->SetCellValue('C'.$i, $row->firstname_kh." ".$row->lastname_kh);
			$this->excel->getActiveSheet()->SetCellValue('D'.$i, $row->firstname." ".$row->lastname);
			$this->excel->getActiveSheet()->SetCellValue('E'.$i, lang($row->gender));
			$this->excel->getActiveSheet()->SetCellValue('F'.$i, $this->erp->toKhmer($row->dob));
			$this->excel->getActiveSheet()->SetCellValue('G'.$i, $row->nationality_kh." / ".$row->nationality);
			$this->excel->getActiveSheet()->SetCellValue('H'.$i, $row->religion_kh." / ".$row->religion);
			$this->excel->getActiveSheet()->SetCellValue('I'.$i, $row->ethnicity_kh." / ".$row->religion); 
			$this->excel->getActiveSheet()->SetCellValue('J'.$i, $commune->native_name."  ".$district->native_name." ".$province->native_name);			
			$this->excel->getActiveSheet()->SetCellValue('K'.$i, $this->erp->toKhmer($row->phone));
			$this->excel->getActiveSheet()->SetCellValue('L'.$i, $row->pob_kh." / ".$row->pob);
			// $this->excel->getActiveSheet()->SetCellValue('M'.$i,  $this->erp->toKhmer($this->erp->hrsd($row->appointment_date)));
			$this->excel->getActiveSheet()->SetCellValue('N'.$i, $this->erp->toKhmer($this->erp->hrsd($row->created_date)));
			$this->excel->getActiveSheet()->getStyle('A'.$i.':O'.$i.'')->applyFromArray($styleArray);
			$i++;
		}
		$this->excel->getActiveSheet()->getStyle('A'.$i.':P'.$i)->applyFromArray($styleArray);
		$this->excel->getActiveSheet()->mergeCells('A'.$i.':P'.$i);
		$this->excel->getActiveSheet()->SetCellValue('A'.$i, "យោងតាមដំណរភ្ជាប់៖".$_SERVER['HTTP_REFERER']."  , ប្រព័ន្ធគ្រប់គ្រង៖ © 2018 អគ្គនាយកដ្ឋានអន្តោប្រវេសន៍ (v1.0 ) ");
		
        $this->excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
		$this->excel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true); 
		// $this->excel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true); 
		$this->excel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true); 
		
        $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);                
		
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$title.'.xls"');
        header('Cache-Control: max-age=0');                
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        $objWriter->save('php://output');exit();
	}
	/* 
		Description: Export Excel Report For K3 ( Add report follow image in WhatApp )  check out request on appeal export
	 */
    public function check_out_request_on_appeal_export()
    {

        $this->load->library('excel');

        $styleArray = array(
            'font'  => array(
                'size'  => 8,
                'name'  => 'Khmer OS Siemreap',
            ));
        $this->excel->getActiveSheet()->getStyle('A1:O1')->applyFromArray($styleArray);
        $this->excel->getActiveSheet()->getRowDimension('1')->setRowHeight(20);
        $this->excel->getActiveSheet()->getStyle('A1:O1')->getFont()->setBold(true)->setSize(10);
        $this->excel->getActiveSheet()->getStyle('A1:O1')->getFont()->getColor()->setRGB('FFFFFF');
        $this->excel->getActiveSheet()->getStyle('A1:O1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('A1:O1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('A1:O1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('555555');

        $this->excel->setActiveSheetIndex(0);
        $title = lang("ពិនិត្យលើការស្នើសុំបណ្ដឹង");
        $this->excel->getActiveSheet()->setTitle($title);
        $this->excel->getActiveSheet()->SetCellValue('A1', lang('ល.រ'));
        $this->excel->getActiveSheet()->SetCellValue('B1', lang('លេខករណី'));
        $this->excel->getActiveSheet()->SetCellValue('C1', lang('ឈ្មោះ'));
        $this->excel->getActiveSheet()->SetCellValue('D1', lang('ឈ្មោះជាអក្សរទ្បាតាំង'));
        $this->excel->getActiveSheet()->SetCellValue('E1', lang('ភេទ'));
        $this->excel->getActiveSheet()->SetCellValue('F1', lang('ថ្ងៃខែឆ្នាំកំណើត'));
        $this->excel->getActiveSheet()->SetCellValue('G1', lang('សញ្ជាតិ'));
        $this->excel->getActiveSheet()->SetCellValue('H1', lang('សាសនា'));
        $this->excel->getActiveSheet()->SetCellValue('I1', lang('ជាតិពន្ធុ'));
        $this->excel->getActiveSheet()->SetCellValue('J1', lang('អាសយដ្ឋាន'));
        $this->excel->getActiveSheet()->SetCellValue('K1', lang('លេខទំនាក់ទំនង'));
        $this->excel->getActiveSheet()->SetCellValue('L1', lang('ទីកន្លែងកំណើត'));
        $this->excel->getActiveSheet()->SetCellValue('M1', lang('ថ្ងៃពិនិត្យ'));
        $this->excel->getActiveSheet()->SetCellValue('N1', lang('ថ្ងៃខែចុះបញ្ជី'));
        $this->excel->getActiveSheet()->SetCellValue('O1', lang('ដំណាក់កាលនៃនីតិវិធី'));

        // $results=$this->applications->getInteviewAppointmentReports();
        $results=$this->applications->getCheckOutRequestOnAppeal();

        $i=2;
        foreach($results as $row){
            $relationship = $this->applications->getRelationshipById($row->relationship);
            $country = $this->applications->getCountryById($row->country);
            $province = $this->applications->getProvinceById($row->province);
            $commune = $this->applications->getCommunceById($row->commune);
            $district = $this->applications->getDistrictById($row->district);
            $this->excel->getActiveSheet()->SetCellValue('A'.$i, $i-1);
            $this->excel->getActiveSheet()->SetCellValue('B'.$i, $row->case_prefix.$row->case_no);
            $this->excel->getActiveSheet()->SetCellValue('C'.$i, $row->firstname_kh." ".$row->lastname_kh);
            $this->excel->getActiveSheet()->SetCellValue('D'.$i, $row->firstname." ".$row->lastname);
            $this->excel->getActiveSheet()->SetCellValue('E'.$i, lang($row->gender));
            $this->excel->getActiveSheet()->SetCellValue('F'.$i, $this->erp->toKhmer($row->dob));
            $this->excel->getActiveSheet()->SetCellValue('G'.$i, $row->nationality_kh." / ".$row->nationality);
            $this->excel->getActiveSheet()->SetCellValue('H'.$i, $row->religion_kh." / ".$row->religion);
            $this->excel->getActiveSheet()->SetCellValue('I'.$i, $row->ethnicity_kh." / ".$row->religion);
            $this->excel->getActiveSheet()->SetCellValue('J'.$i, $commune->native_name."  ".$district->native_name." ".$province->native_name);
            $this->excel->getActiveSheet()->SetCellValue('K'.$i, $this->erp->toKhmer($row->phone));
            $this->excel->getActiveSheet()->SetCellValue('L'.$i, $row->pob_kh." / ".$row->pob);
            $this->excel->getActiveSheet()->SetCellValue('M'.$i,  $this->erp->toKhmer($this->erp->hrsd($row->appeal_date)));
            $this->excel->getActiveSheet()->SetCellValue('N'.$i, $this->erp->toKhmer($this->erp->hrsd($row->created_date)));
            $this->excel->getActiveSheet()->SetCellValue('O'.$i, lang($row->procedure_sequence));
            $this->excel->getActiveSheet()->getStyle('A'.$i.':O'.$i.'')->applyFromArray($styleArray);
            $i++;
        }
        $this->excel->getActiveSheet()->getStyle('A'.$i.':O'.$i)->applyFromArray($styleArray);
        $this->excel->getActiveSheet()->mergeCells('A'.$i.':O'.$i);
        $this->excel->getActiveSheet()->SetCellValue('A'.$i, "យោងតាមដំណរភ្ជាប់៖".$_SERVER['HTTP_REFERER']."  , ប្រព័ន្ធគ្រប់គ្រង៖ © 2018 អគ្គនាយកដ្ឋានអន្តោប្រវេសន៍ (v1.0 ) ");

        $this->excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);

        $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$title.'.xls"');
        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        $objWriter->save('php://output');exit();
    }
    public function interview_appointment_on_appeal_export()
    {

        $this->load->library('excel');

        $styleArray = array(
            'font'  => array(
                'size'  => 8,
                'name'  => 'Khmer OS Siemreap',
            ));
        $this->excel->getActiveSheet()->getStyle('A1:O1')->applyFromArray($styleArray);
        $this->excel->getActiveSheet()->getRowDimension('1')->setRowHeight(20);
        $this->excel->getActiveSheet()->getStyle('A1:O1')->getFont()->setBold(true)->setSize(10);
        $this->excel->getActiveSheet()->getStyle('A1:O1')->getFont()->getColor()->setRGB('FFFFFF');
        $this->excel->getActiveSheet()->getStyle('A1:O1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('A1:O1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('A1:O1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('555555');

        $this->excel->setActiveSheetIndex(0);
        $title = lang("ណាត់ជួបសម្ភាសន៏លើបណ្តឹង");
        $this->excel->getActiveSheet()->setTitle($title);
        $this->excel->getActiveSheet()->SetCellValue('A1', lang('ល.រ'));
        $this->excel->getActiveSheet()->SetCellValue('B1', lang('លេខករណី'));
        $this->excel->getActiveSheet()->SetCellValue('C1', lang('ឈ្មោះ'));
        $this->excel->getActiveSheet()->SetCellValue('D1', lang('ឈ្មោះជាអក្សរទ្បាតាំង'));
        $this->excel->getActiveSheet()->SetCellValue('E1', lang('ភេទ'));
        $this->excel->getActiveSheet()->SetCellValue('F1', lang('ថ្ងៃខែឆ្នាំកំណើត'));
        $this->excel->getActiveSheet()->SetCellValue('G1', lang('សញ្ជាតិ'));
        $this->excel->getActiveSheet()->SetCellValue('H1', lang('សាសនា'));
        $this->excel->getActiveSheet()->SetCellValue('I1', lang('ជាតិពន្ធុ'));
        $this->excel->getActiveSheet()->SetCellValue('J1', lang('អាសយដ្ឋាន'));
        $this->excel->getActiveSheet()->SetCellValue('K1', lang('លេខទំនាក់ទំនង'));
        $this->excel->getActiveSheet()->SetCellValue('L1', lang('ទីកន្លែងកំណើត'));
        $this->excel->getActiveSheet()->SetCellValue('M1', lang('ថ្ងៃណាត់ជួបសម្ភាសន៏'));
        $this->excel->getActiveSheet()->SetCellValue('N1', lang('ថ្ងៃខែចុះបញ្ជី'));
        $this->excel->getActiveSheet()->SetCellValue('O1', lang('ដំណាក់កាលនៃនីតិវិធី'));

        // $results=$this->applications->getInteviewAppointmentReports();
        $results=$this->applications->getInterviewAppointmentOnAppeal();

        $i=2;
        foreach($results as $row){
            $relationship = $this->applications->getRelationshipById($row->relationship);
            $country = $this->applications->getCountryById($row->country);
            $province = $this->applications->getProvinceById($row->province);
            $commune = $this->applications->getCommunceById($row->commune);
            $district = $this->applications->getDistrictById($row->district);
            $this->excel->getActiveSheet()->SetCellValue('A'.$i, $i-1);
            $this->excel->getActiveSheet()->SetCellValue('B'.$i, $row->case_prefix.$row->case_no);
            $this->excel->getActiveSheet()->SetCellValue('C'.$i, $row->firstname_kh." ".$row->lastname_kh);
            $this->excel->getActiveSheet()->SetCellValue('D'.$i, $row->firstname." ".$row->lastname);
            $this->excel->getActiveSheet()->SetCellValue('E'.$i, lang($row->gender));
            $this->excel->getActiveSheet()->SetCellValue('F'.$i, $this->erp->toKhmer($row->dob));
            $this->excel->getActiveSheet()->SetCellValue('G'.$i, $row->nationality_kh." / ".$row->nationality);
            $this->excel->getActiveSheet()->SetCellValue('H'.$i, $row->religion_kh." / ".$row->religion);
            $this->excel->getActiveSheet()->SetCellValue('I'.$i, $row->ethnicity_kh." / ".$row->religion);
            $this->excel->getActiveSheet()->SetCellValue('J'.$i, $commune->native_name."  ".$district->native_name." ".$province->native_name);
            $this->excel->getActiveSheet()->SetCellValue('K'.$i, $this->erp->toKhmer($row->phone));
            $this->excel->getActiveSheet()->SetCellValue('L'.$i, $row->pob_kh." / ".$row->pob);
            $this->excel->getActiveSheet()->SetCellValue('M'.$i,  $this->erp->toKhmer($this->erp->hrsd($row->appointment_date)));
            $this->excel->getActiveSheet()->SetCellValue('N'.$i, $this->erp->toKhmer($this->erp->hrsd($row->created_date)));
            $this->excel->getActiveSheet()->SetCellValue('O'.$i, lang($row->procedure_sequence));
            $this->excel->getActiveSheet()->getStyle('A'.$i.':O'.$i.'')->applyFromArray($styleArray);
            $i++;
        }
        $this->excel->getActiveSheet()->getStyle('A'.$i.':O'.$i)->applyFromArray($styleArray);
        $this->excel->getActiveSheet()->mergeCells('A'.$i.':O'.$i);
        $this->excel->getActiveSheet()->SetCellValue('A'.$i, "យោងតាមដំណរភ្ជាប់៖".$_SERVER['HTTP_REFERER']."  , ប្រព័ន្ធគ្រប់គ្រង៖ © 2018 អគ្គនាយកដ្ឋានអន្តោប្រវេសន៍ (v1.0 ) ");

        $this->excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);

        $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$title.'.xls"');
        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        $objWriter->save('php://output');exit();
    }
    public function reject_on_appeal_export()
    {

        $this->load->library('excel');

        $styleArray = array(
            'font'  => array(
                'size'  => 8,
                'name'  => 'Khmer OS Siemreap',
            ));
        $this->excel->getActiveSheet()->getStyle('A1:O1')->applyFromArray($styleArray);
        $this->excel->getActiveSheet()->getRowDimension('1')->setRowHeight(20);
        $this->excel->getActiveSheet()->getStyle('A1:O1')->getFont()->setBold(true)->setSize(10);
        $this->excel->getActiveSheet()->getStyle('A1:O1')->getFont()->getColor()->setRGB('FFFFFF');
        $this->excel->getActiveSheet()->getStyle('A1:O1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('A1:O1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('A1:O1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('555555');

        $this->excel->setActiveSheetIndex(0);
        $title = lang("បដិសេធនៅលើបណ្តឹងឧទ្ធរណ៍");
        $this->excel->getActiveSheet()->setTitle($title);
        $this->excel->getActiveSheet()->SetCellValue('A1', lang('ល.រ'));
        $this->excel->getActiveSheet()->SetCellValue('B1', lang('លេខករណី'));
        $this->excel->getActiveSheet()->SetCellValue('C1', lang('ឈ្មោះ'));
        $this->excel->getActiveSheet()->SetCellValue('D1', lang('ឈ្មោះជាអក្សរទ្បាតាំង'));
        $this->excel->getActiveSheet()->SetCellValue('E1', lang('ភេទ'));
        $this->excel->getActiveSheet()->SetCellValue('F1', lang('ថ្ងៃខែឆ្នាំកំណើត'));
        $this->excel->getActiveSheet()->SetCellValue('G1', lang('សញ្ជាតិ'));
        $this->excel->getActiveSheet()->SetCellValue('H1', lang('សាសនា'));
        $this->excel->getActiveSheet()->SetCellValue('I1', lang('ជាតិពន្ធុ'));
        $this->excel->getActiveSheet()->SetCellValue('J1', lang('អាសយដ្ឋាន'));
        $this->excel->getActiveSheet()->SetCellValue('K1', lang('លេខទំនាក់ទំនង'));
        $this->excel->getActiveSheet()->SetCellValue('L1', lang('ទីកន្លែងកំណើត'));
        $this->excel->getActiveSheet()->SetCellValue('M1', lang('ថ្ងៃបដិសេធនៅលើបណ្តឹងឧទ្ធរណ៍'));
        $this->excel->getActiveSheet()->SetCellValue('N1', lang('ថ្ងៃខែចុះបញ្ជី'));
        $this->excel->getActiveSheet()->SetCellValue('O1', lang('ដំណាក់កាលនៃនីតិវិធី'));

        // $results=$this->applications->getInteviewAppointmentReports();
        $results=$this->applications->getRejectOnAppeal();

        $i=2;
        foreach($results as $row){
            $relationship = $this->applications->getRelationshipById($row->relationship);
            $country = $this->applications->getCountryById($row->country);
            $province = $this->applications->getProvinceById($row->province);
            $commune = $this->applications->getCommunceById($row->commune);
            $district = $this->applications->getDistrictById($row->district);
            $this->excel->getActiveSheet()->SetCellValue('A'.$i, $i-1);
            $this->excel->getActiveSheet()->SetCellValue('B'.$i, $row->case_prefix.$row->case_no);
            $this->excel->getActiveSheet()->SetCellValue('C'.$i, $row->firstname_kh." ".$row->lastname_kh);
            $this->excel->getActiveSheet()->SetCellValue('D'.$i, $row->firstname." ".$row->lastname);
            $this->excel->getActiveSheet()->SetCellValue('E'.$i, lang($row->gender));
            $this->excel->getActiveSheet()->SetCellValue('F'.$i, $this->erp->toKhmer($row->dob));
            $this->excel->getActiveSheet()->SetCellValue('G'.$i, $row->nationality_kh." / ".$row->nationality);
            $this->excel->getActiveSheet()->SetCellValue('H'.$i, $row->religion_kh." / ".$row->religion);
            $this->excel->getActiveSheet()->SetCellValue('I'.$i, $row->ethnicity_kh." / ".$row->religion);
            $this->excel->getActiveSheet()->SetCellValue('J'.$i, $commune->native_name."  ".$district->native_name." ".$province->native_name);
            $this->excel->getActiveSheet()->SetCellValue('K'.$i, $this->erp->toKhmer($row->phone));
            $this->excel->getActiveSheet()->SetCellValue('L'.$i, $row->pob_kh." / ".$row->pob);
            $this->excel->getActiveSheet()->SetCellValue('M'.$i,  $this->erp->toKhmer($this->erp->hrsd($row->negative_date)));
            $this->excel->getActiveSheet()->SetCellValue('N'.$i, $this->erp->toKhmer($this->erp->hrsd($row->created_date)));
            $this->excel->getActiveSheet()->SetCellValue('O'.$i, lang($row->procedure_sequence));
            $this->excel->getActiveSheet()->getStyle('A'.$i.':O'.$i.'')->applyFromArray($styleArray);
            $i++;
        }
        $this->excel->getActiveSheet()->getStyle('A'.$i.':O'.$i)->applyFromArray($styleArray);
        $this->excel->getActiveSheet()->mergeCells('A'.$i.':O'.$i);
        $this->excel->getActiveSheet()->SetCellValue('A'.$i, "យោងតាមដំណរភ្ជាប់៖".$_SERVER['HTTP_REFERER']."  , ប្រព័ន្ធគ្រប់គ្រង៖ © 2018 អគ្គនាយកដ្ឋានអន្តោប្រវេសន៍ (v1.0 ) ");

        $this->excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);

        $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$title.'.xls"');
        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        $objWriter->save('php://output');exit();
    }
    public function accept_on_appeal_export()
    {

        $this->load->library('excel');

        $styleArray = array(
            'font'  => array(
                'size'  => 8,
                'name'  => 'Khmer OS Siemreap',
            ));
        $this->excel->getActiveSheet()->getStyle('A1:O1')->applyFromArray($styleArray);
        $this->excel->getActiveSheet()->getRowDimension('1')->setRowHeight(20);
        $this->excel->getActiveSheet()->getStyle('A1:O1')->getFont()->setBold(true)->setSize(10);
        $this->excel->getActiveSheet()->getStyle('A1:O1')->getFont()->getColor()->setRGB('FFFFFF');
        $this->excel->getActiveSheet()->getStyle('A1:O1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('A1:O1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('A1:O1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('555555');

        $this->excel->setActiveSheetIndex(0);
        $title = lang("ទទួលស្គាល់លើបណ្តឹងឧទ្ធរណ៏");
        $this->excel->getActiveSheet()->setTitle($title);
        $this->excel->getActiveSheet()->SetCellValue('A1', lang('ល.រ'));
        $this->excel->getActiveSheet()->SetCellValue('B1', lang('លេខករណី'));
        $this->excel->getActiveSheet()->SetCellValue('C1', lang('ឈ្មោះ'));
        $this->excel->getActiveSheet()->SetCellValue('D1', lang('ឈ្មោះជាអក្សរទ្បាតាំង'));
        $this->excel->getActiveSheet()->SetCellValue('E1', lang('ភេទ'));
        $this->excel->getActiveSheet()->SetCellValue('F1', lang('ថ្ងៃខែឆ្នាំកំណើត'));
        $this->excel->getActiveSheet()->SetCellValue('G1', lang('សញ្ជាតិ'));
        $this->excel->getActiveSheet()->SetCellValue('H1', lang('សាសនា'));
        $this->excel->getActiveSheet()->SetCellValue('I1', lang('ជាតិពន្ធុ'));
        $this->excel->getActiveSheet()->SetCellValue('J1', lang('អាសយដ្ឋាន'));
        $this->excel->getActiveSheet()->SetCellValue('K1', lang('លេខទំនាក់ទំនង'));
        $this->excel->getActiveSheet()->SetCellValue('L1', lang('ទីកន្លែងកំណើត'));
        $this->excel->getActiveSheet()->SetCellValue('M1', lang('ថ្ងៃទទួលស្គាល់លើបណ្តឹងឧទ្ធរណ៏'));
        $this->excel->getActiveSheet()->SetCellValue('N1', lang('ថ្ងៃខែចុះបញ្ជី'));
        $this->excel->getActiveSheet()->SetCellValue('O1', lang('ដំណាក់កាលនៃនីតិវិធី'));

        // $results=$this->applications->getInteviewAppointmentReports();
        $results=$this->applications->getAcceptOnAppeal();

        $i=2;
        foreach($results as $row){
            $relationship = $this->applications->getRelationshipById($row->relationship);
            $country = $this->applications->getCountryById($row->country);
            $province = $this->applications->getProvinceById($row->province);
            $commune = $this->applications->getCommunceById($row->commune);
            $district = $this->applications->getDistrictById($row->district);
            $this->excel->getActiveSheet()->SetCellValue('A'.$i, $i-1);
            $this->excel->getActiveSheet()->SetCellValue('B'.$i, $row->case_prefix.$row->case_no);
            $this->excel->getActiveSheet()->SetCellValue('C'.$i, $row->firstname_kh." ".$row->lastname_kh);
            $this->excel->getActiveSheet()->SetCellValue('D'.$i, $row->firstname." ".$row->lastname);
            $this->excel->getActiveSheet()->SetCellValue('E'.$i, lang($row->gender));
            $this->excel->getActiveSheet()->SetCellValue('F'.$i, $this->erp->toKhmer($row->dob));
            $this->excel->getActiveSheet()->SetCellValue('G'.$i, $row->nationality_kh." / ".$row->nationality);
            $this->excel->getActiveSheet()->SetCellValue('H'.$i, $row->religion_kh." / ".$row->religion);
            $this->excel->getActiveSheet()->SetCellValue('I'.$i, $row->ethnicity_kh." / ".$row->religion);
            $this->excel->getActiveSheet()->SetCellValue('J'.$i, $commune->native_name."  ".$district->native_name." ".$province->native_name);
            $this->excel->getActiveSheet()->SetCellValue('K'.$i, $this->erp->toKhmer($row->phone));
            $this->excel->getActiveSheet()->SetCellValue('L'.$i, $row->pob_kh." / ".$row->pob);
            $this->excel->getActiveSheet()->SetCellValue('M'.$i,  $this->erp->toKhmer($this->erp->hrsd($row->created_date)));
            $this->excel->getActiveSheet()->SetCellValue('N'.$i, $this->erp->toKhmer($this->erp->hrsd($row->created_date)));
            $this->excel->getActiveSheet()->SetCellValue('O'.$i, lang('ទទួលស្គាល់លើបណ្តឹងឧទ្ធរណ៏'));
            $this->excel->getActiveSheet()->getStyle('A'.$i.':O'.$i.'')->applyFromArray($styleArray);
            $i++;
        }
        $this->excel->getActiveSheet()->getStyle('A'.$i.':O'.$i)->applyFromArray($styleArray);
        $this->excel->getActiveSheet()->mergeCells('A'.$i.':O'.$i);
        $this->excel->getActiveSheet()->SetCellValue('A'.$i, "យោងតាមដំណរភ្ជាប់៖".$_SERVER['HTTP_REFERER']."  , ប្រព័ន្ធគ្រប់គ្រង៖ © 2018 អគ្គនាយកដ្ឋានអន្តោប្រវេសន៍ (v1.0 ) ");

        $this->excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
        $this->excel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);

        $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$title.'.xls"');
        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        $objWriter->save('php://output');exit();
    }

}
	
?>