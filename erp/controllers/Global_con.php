<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Global_con extends MY_Controller
{
	public function __construct(){
			parent::__construct();
		$this->load->model('global_model');
		$this->load->helper('global_helper');
	}
	public function index(){
		
	}
	public function ajax(){
		$action =  $this->input->get('action');
		if ($action != '' AND $action != 'NULL' AND $action != NULL){
			$a = 0;
			call_user_func('ajax_'.$action, $a);
		}
	}
}