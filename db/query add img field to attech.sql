ALTER TABLE erp_fa_interview_recognitions
ADD img text  DEFAULT NULL;

ALTER TABLE erp_fa_refugee_permanent_card_request
ADD img text  DEFAULT NULL;

ALTER TABLE erp_fa_refugee_card_request
ADD img text  DEFAULT NULL;

ALTER TABLE erp_fa_refugee_travel_document_request
ADD img text  DEFAULT NULL;

ALTER TABLE erp_fa_refugee_cessation_elimination_request
ADD img text  DEFAULT NULL;

ALTER TABLE erp_fa_refugee_withdrawal_request
ADD img text  DEFAULT NULL;

ALTER TABLE erp_fa_counseling
ADD img text  DEFAULT NULL;

ALTER TABLE erp_fa_rsd_applications
ADD img text  DEFAULT NULL;

ALTER TABLE erp_fa_asylum_withdrawal
ADD img text  DEFAULT NULL;

ALTER TABLE erp_fa_asylum_preliminary_stay
ADD img text  DEFAULT NULL;

ALTER TABLE erp_fa_interviews
ADD img text  DEFAULT NULL;

ALTER TABLE erp_fa_interview_negatives
ADD img text  DEFAULT NULL;

ALTER TABLE erp_fa_refugee_family_guarantee_request
ADD img text  DEFAULT NULL;

ALTER TABLE erp_fa_refugee_home_visit
ADD img text  DEFAULT NULL;

ALTER TABLE erp_fa_interview_appointments
ADD img text  DEFAULT NULL;
