<?php  
	///var_dump($result);
	$icon_check='<span style="font-size:17px;">&#x2611;</span>';
	$icon_uncheck='<span style="font-size:17px;">&#x2610;</span>'; 
	
?>

<div class="box a4"> 
		<div class="box-content"> 
				<?php $this->load->view($this->theme."applications/head-cover_1"); ?>
				
				<div class="col-sm-12 text-center" >
					<h2 class='text-cover'>ពាក្យធ្វើសមាហរណកម្មក្នុងសហគមកម្ពុជារបស់ជនភៀសខ្លួន</h2> 
					<h3>REQUEST FOR  REFUGEE'S INTERGRATION INTO CAMBODIA COMMUNITY</h3>
				</div>
				
				<div class='wrap-content'>
					<table width="100%" class="tbl" style='white-space:nowrap;'>
						<tr>   
							<td width='30'>១</td>
							<td width='30'>
								ជាម្ចាស់ករណីលេខ​​​​​​​​ / Priciple Applicant Case Number: <?= $result->case_prefix.$result->case_no ?>&nbsp; 
							</td>
							<td rowspan="4" class='text-right'>
								<?php 
									if(!empty($result->photo) || !file_exists("assets/uploads/".$result->photo)){
										echo "<img src='assets/uploads/".$result->photo."' class='thumb' width='100' height='125' />";
									}else {
										echo "<div class='thumb text-center' >រូបថត<br/>4X6<br/>Photo</div>";
									}
								?> 
							</td>
						</tr> 
						<tr>
							<td  width='30'>២</td>
							<td>សមាជិកនៅក្នុងបន្ទុកចំនួន  /   Number members in charge: <?= 0;?></td> 
						</tr> 
						<tr> 
							<td  width='30'>៣</td>
							<td>នាមត្រកូលនិងនាមខ្លួន / Surname and Given Name: </td>
						</tr>   
						<tr>
							<td width='30'></td> 
							<td colspan='2'>&nbsp;&nbsp;<?= $result->lastname_kh.' '.$result->firstname_kh." / ".strtoupper($result->lastname.' '.$result->firstname) ?></td>
						</tr>
					</table>
					<table width="100%" class="tbl" style='white-space:nowrap;'>
						<tr>
							<td  width='30'>៤</td>
							<td>ថ្ងៃខែឆ្នាំ​កំណើត / ​​ Date of Birth:&nbsp;<?= $this->erp->hrsd($result->dob) ?> 
							</td> 
							<td> ភេទ / Sex: ប្រុស / Male:&nbsp;<?php echo  ($result->gender == 'male' ? $icon_check : $icon_uncheck);?>
						&nbsp;&nbsp;ស្រី / Female:&nbsp;<?php echo  ($result->gender == 'female' ? $icon_check : $icon_uncheck);?></td>
						</tr> 
						<tr>
							<td  width='30'>៥</td>
							<td colspan='2'> ទីកន្លែងកំណើត / Place of Birth: &nbsp; 
							<?= $result->pob_kh ?>
							</td>
						</tr>  
						<tr>
							<td  width='30'>៦</td>
							<td >សញ្ញាតិ / Nationality: &nbsp;  <?= $result->nationality_kh." / ".$result->nationality ?></td>
							<td >ថ្ងៃខែឆ្នាំ​ ទទួលឋានៈ / Recognized date:&nbsp;  <?= $this->erp->hrsd($decision->recognized_date); ?></td>
						</tr> 
						<tr>
							<td  width='30'>៧</td>
							<td colspan='2' >
								មុខរបរបច្ចុប្បន្ន / Current Occupation:  
								<?= $application->occupation  ?> 
							</td>
						</tr> 
						<tr>
							<td  width='30'>៨</td>
							<td colspan='2'>
								អាស័យដ្ធានបច្ចុប្បន្ន / Current Address: &nbsp;
								<?= $this->applications->getAllAddressKH($application->country,"country"); ?> 
								<?= $this->applications->getAllAddressKH($application->province,"province"); ?>
								<?= $this->applications->getAllAddressKH($application->district,"district"); ?>
								<?= $this->applications->getAllAddressKH($application->communce,"commune"); ?>
								<?= $this->applications->getTagsById($application->village); ?> , 
								<?= $row->address_kh ?> 
							</td>
						</tr> 
						<tr>
							<td  width='30'>៩</td>
							<td colspan='2'>
								លេខទូរស័ព្ទទំនាកទំនង / Contact Number: &nbsp; 
								<?= $application->contact_number  ?>
							</td>
						</tr>
						
					</table> 
					<table width="100%"  class="space-size" width='100%'>
						<tr>
							<td>
								<p> 
								ខ្ញុំបាទ / នាងខ្ញុំសូមគោរពស្នើសុំនាយកដ្ឋានជនភៀសខ្លួន នៃអគ្គនាយកដ្ឋានអន្តោប្រវេសន៍អនុញ្ញាតិអោយខ្ញុំបាទ/នាងខ្ញុំបានធ្វើ មាហរណកម្ម ក្នុងសហគមន៍កម្ពុជាដោយក្តីអនុគ្រោះ។
								<br/>I hereby to request the Refugee Department, General Department of Immigration pleasae kindly allow me to integrate into the Cambodian community with favor.
								</p>
							</td>
						</tr>
					</table>
					 
				<div class='clearfix'></div>
			
				<div class="col-sm-12 "> 
					<div class='div-left' style='width:30%;font-size:9px;padding:0px !important;'> 
						<center><u>ភ្ជាប់មកជាមួយនូវ​ <br/> Attach with</u></center>
						<ul class="list ">
							<li>លិខិតបញ្ជក់របស់អង្គការ IOM<br/> អំពីលទ្ធផលតម្រង់ទិសជនភៀសខ្លួន
							<li>​IOM's orientation for refugee certificate
							<li>លិខិតបញ្ជាក់សុខភាព
							<li>Health certificate
						</ul> 
					</div>
					<div class='div-right text-center'>
						<?= $this->erp->footer_date_khmer("រាជធានីភ្នំពេញ");?>
						<?= $this->erp->thumb_and_name()?>
					</div> 
				</div>
				<div class='clearfix'></div>
				<br/>
				<div class='col-sm-6 tbl-left'>
					<div style="border:1px solid #000;">
						<table width='100%' class='text-center'>
							<tr>
								<td>
									<h4>សម្រាប់មន្រ្តី /Officer Use Only</h4>
									<i>
									<?= $this->erp->footer_date_khmer("រាជធានីភ្នំពេញ");?>
									</i>
								</td>
							</tr>
						</table>
						<br/>
						<br/>
						<br/> 
					</div>		
				</div>
	
</div> 
<style type="text/css"> 
	 @media print{
		.bblack{ background: white !important; }   
		.modal-content, .box, 
		.modal-header{ border:none!important; }
		.table { border:none !important; }
	}
	.wrap-content {
		min-height:800px;
		font-size:14px;
	}
	.wrap-content table td {
		padding:6px;
	}
	.thumb {
		margin-top:-20px;
	}
</style>