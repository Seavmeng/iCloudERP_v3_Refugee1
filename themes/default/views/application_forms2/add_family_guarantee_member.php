<div class="modal-dialog modal-lg">
	<div class="modal-content">
		<div class="modal-header">				
			<button type="button" class="close" data-dismiss="modal">
				<span aria-hidden="true"><i class="fa fa-2x">&times;</i></span>
				<span class="sr-only"><?=lang('close');?></span>
			</button>
			<h4 class="modal-title" id="payModalLabel"><?= lang('add_member'); ?></h4>
		</div>
		
		<?php $attributes = array('id' => 'form1');?>
		
		<?php  echo form_open_multipart("application_forms2/add_family_guarantee_member/".$id, $attributes); ?> 
		<?php 
			$countries_ = array(lang("select"));
			foreach($countries as $country){
				$countries_[$country->id] = $country->country;
			} 
			?>
			<?php 
			$states_ = array(lang("select"));
			foreach($states as $state){
				$states_[$state->id] = $state->name;
			} 
			?>
			<?php 
			$provinces_ = array(lang("select"));
			foreach($provinces as $province){
				$provinces_[$province->id] = $province->name;
			} 
			?>
			<?php 
			$districts_ = array(lang("select"));
			foreach($districts as $district){
				$districts_[$district->id] = $district->name;
			} 
			?>
			<?php 
			$communes_ = array(lang("select"));
			foreach($communes as $commune){
				$communes_[$commune->id] = $commune->name;
			} 
			?>
					
		<div class="modal-body">
			
			
			
				<div class="col-sm-6">
					
					<div class="row">
					
						<div class="col-sm-6" >
							<div class="form-group">
								<?php echo lang('firstname', 'firstname'); ?>​
								<span class="red">*</span>
								<div class="controls">					
									<input type="text"​ class="form-control input-sm" name="firstname_kh" />
								</div>
							</div>
						</div>
						
						<div class="col-sm-6" >
							<div class="form-group">
								<?php echo lang('firstname', 'firstname'); ?>​ (EN)
								<span class="red">*</span>
								<div class="controls">					
									<input type="text" class="form-control input-sm" name="firstname" />
								</div>
							</div>
						</div>
						
						<div class="col-sm-6" >
							<div class="form-group">
								<?php echo lang('lastname', 'lastname'); ?>​
								<span class="red">*</span>
								<div class="controls">					
									<input type="text" class="form-control input-sm" name="lastname_kh" />
								</div>
							</div>
						</div>
						
						<div class="col-sm-6" >
							<div class="form-group">
								<?php echo lang('lastname', 'lastname'); ?>​ (EN)
								<span class="red">*</span>
								<div class="controls">					
									<input type="text" class="form-control input-sm" name="lastname" />
								</div>
							</div>
						</div>
						
						<div class="col-sm-6">
							<div class="form-group">
								<?php echo lang('gender', 'gender'); ?>​
								<span class="red">*</span>
								<div class="controls">
									<select class="form-control selectskip" name="gender">
										<option value="male" ><?= lang("male") ?></option>
										<option value="female" ><?= lang("female") ?></option>
									</select>
								</div>
							</div>
						</div>
						
						<div class="col-sm-6" >
							<div class="form-group">
								<?php echo lang('dob', 'dob'); ?>​
								<span class="red">*</span>
								<div class="controls">
									<input type="text" class="form-control input-sm date"  name="dob" />
								</div>
							</div>
						</div>
						
						<div class="clearfix"></div>
						
						<div class="col-sm-6">
							<div class="form-group">
								<?php echo lang('nationality', 'nationality'); ?>​
								<span class="red">*</span>
								<div class="controls">
									<input type="text" class="form-control input-sm "  name="nationality_kh" />
								</div>
							</div>
						</div>
						
						<div class="col-sm-6">
							<div class="form-group">
								<?php echo lang('nationality', 'nationality'); ?>​ (EN)
								<span class="red">*</span>
								<div class="controls">
									<input type="text" class="form-control input-sm "  name="nationality" />
								</div>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<?php echo lang('religion', 'religion'); ?>​
								<span class="red">*</span>
								<div class="controls">
									<input type="text"  class="form-control input-sm "  name="religion_kh" />
								</div>
							</div>
						</div>
						
						<div class="col-sm-6">
							<div class="form-group">
								<?php echo lang('religion', 'religion'); ?>​ (EN)
								<span class="red">*</span>
								<div class="controls">
									<input type="text"   class="form-control input-sm "  name="religion" />
								</div>
							</div>
						</div>

						<div class="col-sm-6">
							<div class="form-group">
								<?php echo lang('ethnicity', 'ethnicity'); ?>​
								<span class="red">*</span>
								<div class="controls">
									<input type="text"  class="form-control input-sm "  name="ethnicity_kh" />
								</div>
							</div>
						</div>
						
						<div class="col-sm-6">
							<div class="form-group">
								<?php echo lang('ethnicity', 'ethnicity'); ?>​ (EN)
								<span class="red">*</span>
								<div class="controls">
									<input type="text"   class="form-control input-sm "  name="ethnicity" />
								</div>
							</div>
						</div>

						<div class="col-sm-6">
							<div class="form-group">
								<?php echo lang('occupation', 'occupation'); ?>​ 
								<span class="red">*</span>
								<div class="controls">
									<input type="text" class="form-control input-sm "  name="occupation_kh" />
								</div>
							</div>
						</div>
						
						<div class="col-sm-6">
							<div class="form-group">
								<?php echo lang('occupation', 'occupation'); ?>​ (EN)
								<span class="red">*</span>
								<div class="controls">
									<input type="text" class="form-control input-sm "  name="occupation" />
								</div>
							</div>
						</div>
						
						<div class="col-sm-6">
							<div class="form-group">
								<?php echo lang('education', 'education'); ?>​
								<span class="red">*</span>
								<div class="controls">
									<input type="text" class="form-control input-sm "  name="education_kh" />
								</div>
							</div>
						</div>
												
						<div class="col-sm-6">
							<div class="form-group">
								<?php echo lang('education', 'education'); ?>​ (EN)
								<span class="red">*</span>
								<div class="controls">
									<input type="text" class="form-control input-sm "  name="education" />
								</div>
							</div>
						</div>
						
						<div class="col-sm-12">
							<div class="form-group">
								<?php echo lang('relationship', 'relationship'); ?>​
								<span class="red">*</span>
								<div class="controls">
									<select class="form-control" name="relationship">
										<?php foreach($relationship as $row){?>
											<option value="<?= $row->id ?>" <?= ($member->relationship==1?'selected':'') ?>><?= $row->relationship_kh ?></option>  
										<?php }?>
									</select>
								</div>
							</div>
						</div>
						
						<div class="col-sm-6">
							<div class="form-group">
								<?php echo lang('pob', 'pob'); ?>​
								<div class="controls">
									<input type="text" class="form-control input-sm "  name="pob_kh" />
								</div>
							</div>
						</div>
						
						<div class="col-sm-6">
							<div class="form-group">
								<?php echo lang('pob', 'pob'); ?>​ (EN)
								<div class="controls">
									<input type="text" class="form-control input-sm "  name="pob" />
								</div>
							</div>
						</div>
						
						<div class="col-sm-12 hidden">							
							<div class="form-group">
								<?php echo lang('status', 'status'); ?>
								<span class='red'>*</span>
								<div class="controls">
									<?php
									$status = array('no_come'=>lang("no_come"), 'come' =>lang('come'));
									echo form_dropdown('status', $status, 0, 'id="status" class="form-control" ');
									?>
								</div>
							</div> 
						</div> 					
					</div> 
				</div>
				
				<div class="col-sm-6">
					
					<div class="panel panel-warning">
						<div class="panel-heading"><?php echo lang("អាសយដ្ឋានចុងក្រោយមុនពេលមកដល់កម្ពុជា","អាសយដ្ឋានចុងក្រោយមុនពេលមកដល់កម្ពុជា") ?></div>
						<div class="panel-body" style="padding: 5px;">
							<div class="col-lg-6">
								<div class="form-group">
									<?php echo lang('country', 'country'); ?>
									<div class="controls">
										<?php echo form_dropdown('country', $countries_, 0, ' class="form-control" '); ?>												
									</div>
								</div>
							</div>
							
							<div class="col-lg-6">
								<div class="form-group">
									<?php echo lang('state', 'state'); ?>
									<div class="controls">
										<?php echo form_dropdown('state', $states_, 0, ' class="form-control" '); ?>												
									</div>
								</div>
							</div>
							
							<div class="col-lg-6">
								<div class="form-group">
									<?php echo lang('province', 'province'); ?>
									<div class="controls">
										<?php echo form_dropdown('province', $provinces_, 0, ' class="form-control" '); ?>												
									</div>
								</div>
							</div>
							
							<div class="col-lg-6">
								<div class="form-group">
									<?php echo lang('district', 'district'); ?>
									<div class="controls">
										<?php echo form_dropdown('district', $districts_, 0, ' class="form-control" '); ?>												
									</div>
								</div>
							</div>
							
							<div class="col-lg-12">
								<div class="form-group">
									<?php echo lang('commune', 'commune'); ?>
									<div class="controls">
										<?php echo form_dropdown('commune', $communes_, 0, ' class="form-control" '); ?>												
									</div>
								</div>
							</div>
							
							<div class="col-lg-6">
								<div class="form-group">
									<?php echo lang('village', 'village')." (KH)"; ?> 
									<div class="controls">
										<?php 
										echo form_input('village_kh', set_value('village_kh', $this->applications->getTagsKhmerById($member->village)),'class="form-control village_kh" '); ?>											
									</div>
								</div>
							</div>

							<div class="col-lg-6">
								<div class="form-group">
									<?php echo lang('village', 'village')." (EN)"; ?> 
									<div class="controls">
										<?php 
										echo form_input('village', set_value('village', $this->applications->getTagsById($member->village)),'class="form-control village" '); ?>											
									</div>
								</div>
							</div>

							<div class="col-lg-12">
								<div class="form-group"> 
									<?php echo lang('other', 'other'); ?>​ 
									<span>(KH)</span> 
									<div class="control">
										<input type="text" value="<?= set_value('address_kh');?>" class="form-control input-sm" name="address_kh"  id="address_kh" />
									</div> 
								</div> 
							</div> 
							
							<div class="col-lg-12">
								<div class="form-group"> 
									<?php echo lang('other', 'other'); ?>​ 
									<span>(EN)</span> 
									<div class="control">
										<input type="text" value="<?= set_value('address');?>" class="form-control input-sm" name="address"  id="address" />
									</div> 
								</div> 
							</div>
							
							
							
							
						</div>
					</div>
					
				</div>
				
			</div>
			
			<div class="clearfix"></div>
			
		
		
		 <div class="modal-footer">
			<?php echo form_submit('submit', lang('submit'), 'class="btn btn-primary save"'); ?>
		 </div>
		
		<?php  echo form_close(); ?>
			
	</div>
</div>
<?= $modal_js ?>
<style type="text/css">
	ul.ui-autocomplete {
		z-index: 1100;
	}
</style>
<script type="text/javascript">
	$(function(){
		$("select").select2();
		getAllAutoComplete(".village, .village","/getAllTags?v=village");
		function getAllAutoComplete(element, base_url){
			$(element).autocomplete({
				source: function (request, response) {
					var q = request.term;
					$.ajax({
						type: 'GET',
						url: '<?= site_url('asylum_seekers/'); ?>'+base_url,
						dataType: "json",
						data: {
							q : q
						},
						success: function (data) {
							response(data);
						}
					});
				},
				minLength: 1,
				autoFocus: false,
				delay: 200,
				response: function (event, ui) {
					if (ui.content[0].id == 0) {
						$(this).removeClass('ui-autocomplete-loading');
						$(this).removeClass('ui-autocomplete-loading');
					}
				},
				select: function (event, ui) {
					event.preventDefault();
					$(this).val(ui.item.label);
				}
			});
		}
	});   
</script>
