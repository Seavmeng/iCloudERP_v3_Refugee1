<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">				
			<button type="button" class="close" data-dismiss="modal">
				<span aria-hidden="true"><i class="fa fa-2x">&times;</i></span>
				<span class="sr-only"><?=lang('close');?></span>
			</button>
			<h4 class="modal-title" id="payModalLabel"><?= lang("add_expellation_refugee") ?></h4>
		</div>
		<?php  echo form_open_multipart("application_forms2/add_expellation_refugee/"); ?>
		
		<div class="modal-body"> 
			<div class="col-sm-12 form-group">
				<?php echo lang('case_no', 'case_no'); ?>​ 
				<span class="red">*</span>
				<div class="control">
					<input type="text" class="form-control input-sm case_no" id="case_no">
					<input type="hidden" name="application_id" id="application_id" value="<?= $result->id;?>">
				</div>
			</div>    
			<div class="clearfix"></div> 
		</div> 
		<div class="modal-footer">
			<?php echo form_submit('form1', lang('submit'), 'class="btn btn-primary save-data"'); ?>
		</div>
		<?php  echo form_close(); ?>
	</div>
</div>
<script type="text/javascript">
	$(".save-data").on('click',function(event){	    
		var case_no= $('#case_no').val();
		var application_id= $('#application_id').val(); 
		if(case_no=='' || application_id==''){ 
			bootbox.alert('<?= lang("please_select_all");?>');
			return false;
		}  
	});
</script>
<style type="text/css"> 
	ul.ui-autocomplete {
	    z-index: 1100;
	}
</style>
<script type="text/javascript" src="<?= $assets ?>styles/helpers/components/js/bootstrap-formhelpers.js"></script>	
<link href="<?= $assets ?>styles/helpers/components/js/bootstrap-formhelpers.min.css" rel="stylesheet" />
<script type="text/javascript">
	$(function(){ 
		$(".case_no").autocomplete({
			source: function (request, response) {
				var q = request.term;  
				$.ajax({
					type: 'get',
					url: '<?= site_url('applications/suggestions_caseno'); ?>',
					dataType: "json",
					data: {
						q : q
					},
					success: function (data) {
						response(data);
					}
				});
			},
			minLength: 1,
			autoFocus: false,
			delay: 200,
			response: function (event, ui) {
				if (ui.content[0].id == 0) {
					$(this).removeClass('ui-autocomplete-loading');
					$(this).removeClass('ui-autocomplete-loading');
				}
			},
			select: function (event, ui) {
				event.preventDefault();
				if (ui.item.id !== 0) {  
					$("input[name='application_id'").val(ui.item.row.application_id);
				} 
			}
		});
		
	}); 
	function pad(str) {
	  str = str.toString();
	  return str.length < 2 ? pad("0" + str, 2) : str;
	}
</script>