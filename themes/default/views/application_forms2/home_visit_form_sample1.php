<?php   
	$icon_check='<span style="font-size:17px;">&#x2611;</span>';
	$icon_uncheck='<span style="font-size:17px;">&#x2610;</span>'; 
	
	$icon_check1='<span style="font-size:17px;">&#x2610;</span>';
?>

<div class="box a4">
		
	<div class="box-content">    
		<?php $this->load->view($this->theme."applications/head-cover_1"); ?>
		 
		<div class="col-sm-12 text-center">
			<h2 class='text-cover' style="margin-top:0px;">កំណត់ត្រាការចុះសួរសុខទុក្ខ</h2> 
		</div>  
			<table width='100%' >
				<tr>
					<td>
						<center><h2 class='text-cover' style="margin-top:10px;">ផ្នែក ក. ព័ត៌មានបុគ្គល</h2></center>
					</td>
				</tr>
			</table> 
			<table width='100%'>
				<tr>
					<td width="20">-</td>
					<td>
						<?= lang('លេខករណី : ')?>
						<?= $this->erp->toKhmer($result->case_prefix.$result->case_no); ?>
					</td>
					<td> 
						<?php  if($member){ ?>
							 - &nbsp;&nbsp;<?= lang('ជាអ្នកសុំសិទ្ធិជ្រកកោន')."&nbsp;".$icon_uncheck;?>
							&nbsp;&nbsp;&nbsp;
							<?= lang('ជាជនភៀសខ្លួន')."&nbsp;".$icon_uncheck;?>
						<?php }else { ?> 
							 - &nbsp;&nbsp;<?= lang('ជាអ្នកសុំសិទ្ធិជ្រកកោន')."&nbsp;".(empty($recognition)?$icon_check:$icon_uncheck);?>
							&nbsp;&nbsp;&nbsp;
							<?= lang('ជាជនភៀសខ្លួន')."&nbsp;".(!empty($recognition)?$icon_check:$icon_uncheck);?>
						<?php } ?>
					
					
					
					</td>
				</tr>
				
				<tr> 
					<td>-</td>
					<td colspan="2">		
					ជាម្ចាស់ករណី / Principle Applicant: <?php echo ($home_visit->member_id == 0 ? $icon_check : $icon_uncheck);?>
					&nbsp;&nbsp;  
					ឬ ជាសមាជិកក្នុងបន្ទុក / Dependent Member: <?php echo ($home_visit->member_id >= 1 ? $icon_check : $icon_uncheck);?>
					</td> 
				</tr>
				
				<tr>
					<td>-</td>
					<td>
						<?= lang('នាមត្រកូល និងនាមខ្លួន  : ')?>
						<?php   
							 if($member){ 
								echo $member->lastname_kh.' '.$member->firstname_kh." / ".strtoupper($member->lastname." ".$member->firstname);
							}else {
								echo $result->lastname_kh.' '.$result->firstname_kh." / ".strtoupper($result->lastname." ".$result->firstname);
							}  
						?>
					</td>
					<td>
						- &nbsp;&nbsp;<?= lang('ភេទ : ')?> 
						<?php 	 
							if($member){ 
								 echo lang($member->gender);
							}else {
								 echo lang($result->gender);
							}
						?>
					</td>
				</tr>  
				<tr>
					<td>-</td>
					<td>
						<?= lang('ថ្ងៃខែឆ្នាំកំណើត : ')?> 
						<?php 	 
							if($member){ 
								 echo $this->erp->toKhmer($this->erp->hrsd($member->dob));
							}else { 
								 echo $this->erp->toKhmer($this->erp->hrsd($result->dob));
							}
						?>
					</td>
					
					<td>
						- &nbsp;&nbsp;<?= lang('សញ្ជាតិ : ')?>  
						<?php 	 
							if($member){ 
								 echo lang($member->nationality_kh);
							}else { 
								 echo lang($result->nationality_kh);
							}
						?>
					</td>
				</tr> 
				<tr>
					<td>-</td>
					<td>
						<?= lang('មុខរបរ : ')?>
						<?=  $home_visit->occupation_kh ?>
					</td>
					<td>
						- &nbsp;&nbsp;<?= lang('ការសិក្សាអប់រំ : ')?>
						<?=  $home_visit->education_kh ?>
					</td>
				</tr> 
				<tr> 
					<td width="20px">-</td>
					<td colspan="2"> 
						<?= lang('អាសយដ្ឋានស្នាក់នៅ : ')?>
						<?=  $home_visit->address_kh ?>
						&nbsp;&nbsp;&nbsp;
						<?= lang('ភូមិ :')?> 
						<?= $this->applications->getTagsById($home_visit->village);  ?>
						&nbsp;&nbsp;&nbsp;
						<?= lang('ឃុំ / សង្កាត់ :')?> 
						<?= $this->applications->getAllAddressKH($home_visit->commune ,"commune"); ?>
					</td>
				</tr> 
				<tr>
					<td></td>  
					<td colspan="2">
						<?= lang('ស្រុក / ខ័ណ្ឌ :')?>    
						<?= $this->applications->getAllAddressKH($home_visit->district,"district"); ?>
						&nbsp;&nbsp;
						<?= lang('រាជធានី / ខេត្ត :')?>     
						<?= $this->applications->getAllAddressKH($home_visit->province,"province"); ?>
					</td>
				</tr> 
			</table>
			
			<table width='100%'>
				<tr>
					<td width="20">-</td>
					<td>មានសមាជិកនៅក្នុងបន្ទុក : </td>
				</tr> 
			</table>
				
			<center>
				<table width='100%' class="relationship_table">
					<tr>
						<td>ឈ្មោះ<br/>Name</td>
						<td style="width:80px;" class='text-center'>ភេទ<br/>Sex</td>
						<td  class='text-center'>ថ្ងៃខែឆ្នាំកំណើត<br/> Date of Birth</td>
						<td  class='text-center'>សញ្ជាតិ<br/> Nationality</td>	
						<td  class='text-center'>ទំនាក់ទំនង<br/>Relationship</td>										
						<td  class='text-center'>មុខរបរបច្ចុប្បន្ន <br/>Current Occupation</td> 
					</tr>
					 
					 <?php  
					 $relationships = array();
					 foreach($relationship as $relation){
						$relationships[$relation->id] = $relation->relationship_kh;
					 }
					 foreach($default_members as $key => $member){
					 ?>
						<tr> 
							<td class="bold">
								<?= $this->erp->toKhmer($key+1); ?>. 
								<?= $member->lastname_kh." ".$member->firstname_kh; ?> / <?= $member->lastname." ".$member->firstname; ?> 
							</td>
							<td class='center'><?= lang($member->gender) ?></td>  
							<td class='center'><?= $this->erp->toKhmer($this->erp->hrsd($member->dob))?></td>
							<td class='center'><?= $member->nationality_kh ?></td>
							<td class='center'><?= $relationships[$member->relationship] ?></td>
							<td class='center'><?= $member->occupation_kh ?></td> 
						</tr>
						<tr> 
							<td colspan='6'>អាសយដ្ឋាន / Address:&nbsp;
								<?= $this->applications->getAllAddressKH($member->country,"country"); ?> 
								<?= $this->applications->getAllAddressKH($member->province,"province"); ?>
								<?= $this->applications->getAllAddressKH($member->district,"district"); ?>
								<?= $this->applications->getAllAddressKH($member->commune,"commune"); ?>
								<?= $this->applications->getTagsById($member->village); ?> ,
								<?= $member->address_kh ?> 
							</td> 
						</tr> 
						<tr> 
							<td colspan='6'>លេខទូរស័ព្ទទំនាក់ទំនង / Contact Number :&nbsp;<?= $member->contact_number?> </td> 
						</tr> 
						
					<?php } ?> 
				</table>
				
			
			</center>
				
				<style type="text/css">
					.relationship_table td{
						white-space: nowrap;
						border: 1px solid #000;
						
					}
					.relationship_table:last-child{
						border:none !important;
					}
					.break_page{
						page-break-after: always;
					}
				</style> 
			<table width='100%'> 
				<tr>
					<td>-</td>
					<td colspan='5'>
						<?= lang('លេខទូរស័ព្ទទំនាក់ទំនង :')?>    
						<?= $this->erp->toKhmer($home_visit->contact_number) ?></span>
					</td> 
				</tr>
				<tr>
					<td>-</td>
					<td>
						<b><?= lang('លទ្ធផល');?></b>:
						ទំនាក់ទំនងបាន  
						<?= (empty($home_visit->contact_reason))?$icon_check:$icon_uncheck;?>
						&nbsp;&nbsp;
						ទំនាក់ទំនងមិន​បាន  
						<?= (!empty($home_visit->contact_reason))?$icon_check:$icon_uncheck;?>
						&nbsp;&nbsp;
						មូលហេតុ : 
						&nbsp;&nbsp;
						<?= strip_tags($home_visit->contact_reason);?></span>
					</td> 
				</tr>
			</table> 
			
			
				<table width='100%' >
					<tr>
						<td>
							<center><h2 class='text-cover'>ផ្នែក ខ. ស្ថានភាពរស់នៅ</h2></center>
						</td>
					</tr>
				</table>
				
				<table width='80%' >
					<tr>
						<td width="20px;">-</td>
						<td>
							<?= lang('ស្ថានភាពគ្រួសារ  : ')?>
							&nbsp;&nbsp;
							<?= lang('មាន')?>
							&nbsp;
							<?= $icon_uncheck; ?>
							&nbsp;
							<?= lang('មធ្យម')?>
							&nbsp;
							<?= $icon_uncheck ; ?>
							&nbsp;
							<?= lang('ក្រ')?>
							&nbsp;&nbsp;
							<?= $icon_uncheck ; ?>
						</td>
					</tr> 
				</table>
				
				<table width='100%' >
					<tr>
						<td width="20px;">-</td>
						<td>
							<?= lang('ស្ថានភាពសុខភាព : ')?> 
							<?= $home_visit->health_status; ?></span>
						</td>
					</tr> 
					<tr>
						<td>-</td>
						<td >
							<?= lang('ជំនួយឧបត្ថម្ភ : ')?>
							&nbsp;&nbsp;
							<?= lang('គ្មាន')."&nbsp;".$icon_uncheck?> 
							&nbsp;&nbsp;
							<?= lang('មាន')."&nbsp;".$icon_uncheck?>
							&nbsp;&nbsp;
							<?= lang('ពី :') ?>
							&nbsp;&nbsp;							
						</td>
					</tr> 
					<tr>
						<td>-</td>
						<td>
							<?= lang('ជាថវិកា')."&nbsp;&nbsp;".$icon_uncheck?>
							&nbsp;&nbsp;
							<?= lang('ជាសម្ភារៈផ្សេងៗ')."&nbsp;&nbsp;".$icon_uncheck?>
							&nbsp;&nbsp;
							<?= lang('ជា :') ?>
							&nbsp;&nbsp;</span>
						</td> 
					</tr>
					<tr>
						<td>-</td>
						<td>
							<?= lang('សេវាធានារ៉ាប់រង : ')?> 
							&nbsp;&nbsp;
							<?= lang('គ្មាន')."&nbsp;".$icon_uncheck?> 
							&nbsp;&nbsp;
							<?= lang('មាន')."&nbsp;".$icon_uncheck?>
							&nbsp;&nbsp;
							<?= lang('ពីអង្គការ : ');?>​
							&nbsp;&nbsp;</span>
						</td>
					</tr> 
					<tr>	
						<td>-</td>
						 <td>
						 <?= lang('លើផ្នែក : ');?>​
						​​​​​​​ &nbsp;&nbsp;​​
						 <?= lang('សុខភាព')."&nbsp;".$icon_uncheck?>
						 &nbsp;&nbsp;
						 <?= lang('អាយុជីវិត')."&nbsp;".$icon_uncheck?>
						 &nbsp;&nbsp;
						 <?= lang('ផ្សេងទៀត')."&nbsp;".$icon_uncheck?>						 
						  &nbsp;&nbsp;</span>
						 </td>
					</tr>
					<tr>	
						<td>-</td>
						<td>
							<?= lang('សំណូមពរផ្សេងៗ : ');?>
							<span style="width:800px; height:1px; border-bottom:1px solid #000;"></span>
						</td>		
					</tr>
					<tr>
						<td colspan="2">
							&nbsp;<hr/>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							&nbsp;<hr/>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							&nbsp;<hr/>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							&nbsp;<hr/>
						</td>
					</tr>
					
				</table>
				
				<div class="clearfix"></div>
				
				<table width='100%'>
					<tr>
						<td>
							<center><h2 class='text-cover'>ផ្នែក គ.  ព័ត៌មានអាជ្ញាធរមូលដ្ឋាន</h2></center>
						</td>
					</tr>
				</table>
				
				<table width='100%' style='white-space:nowrap;' >
					<tr>
						<td width="20px;">-</td>
						<td>
							<?= lang('ឈ្មោះមេភូមិ / នាយប៉ុស្តិ៍នគរបាលរដ្ឋបាល : ')?></span>
						</td>
					</tr>
					<tr>
						<td>-</td>
						<td> 
							<?= lang('លេខទូរស័ព្ទទំនាក់ទំនង : ')?></span>
						</td>
					</tr> 
					<tr>
						<td>-</td>
						<td>
							<?= lang('មតិរបស់មេភូមិ / នាយប៉ុស្តិ៍នគរបាលរដ្ឋបាល : ')?>
							&nbsp;&nbsp;
							<?= lang('មិនធ្លាប់មានពិរុទ្ធ')."&nbsp;".$icon_uncheck ?>
							&nbsp;&nbsp;
							<?= lang('ធ្លាប់មានពិរុទ្ធ')."&nbsp;".$icon_uncheck?> : 
							&nbsp;&nbsp;
						</td>
					</tr>
					<tr>
						<td></td>
						<td></td>
					</tr>
					
					<tr>
						<td></td>
						<td colspan='3' class="bold">
							<?= lang('មតិសម្រាប់មន្ត្រីចុះសួរសុខទុក្ខ : ')?>
						</td>
					</tr>
					
					<tr>
						<td colspan="2">
							&nbsp;<hr/>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							&nbsp;<hr/>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							&nbsp;<hr/>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							&nbsp;<hr/>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							&nbsp;<hr/>
						</td>
					</tr> 
				</table>  
				
				<br/>
				
				<div class="clearfix"></div>
				
				<div  class='div-right'>
					<table width='100%'  class='text-center'>
						<tr>
							<td> 
								<i>
								<?= $this->erp->footer_date_khmer("រាជធានីភ្នំពេញ");?>
								</i>
								<h4>ហត្ថលេខា និងឈ្មោះមន្ត្រីចុះសួរសុខទុក្ខ</h4>
							</td>
						</tr>
					</table>
				</div>
				
				<div class="clearfix"></div>
				
			</div>	 
		</div> 
	</div>
</div>

<style type="text/css">
	@media print{
		.bblack{ background: white !important; }   
		.modal-content, .box, 
		.modal-header{ border:none!important; }
		.table { border:none !important; }
		.bordered {border:1.5px solid black;}
	} 
	table td {
		padding: 6px;		
	}	
	hr{
		padding:0px;
		margin:0px;
		border-top:none !important;
		border-bottom:dotted 1px !important;
	}   
</style>