<?php   
	$icon_check='<span style="font-size:17px;">&#x2611;</span>';
	$icon_uncheck='<span style="font-size:17px;">&#x2610;</span>'; 
	
?>

<div class="box a4">
		
	<div class="box-content">    
		<?php $this->load->view($this->theme."applications/head-cover_1"); ?>
		 
		<div class="col-sm-12 text-center">
			<h2 class='text-cover' style="margin-top:0px;">កំណត់ត្រាការចុះសួរសុខទុក្ខ</h2> 
		</div>   
		  
			<table width='100%' >
				<tr>
					<td>
						<center><h2 class='text-cover' style="margin-top:10px;">ផ្នែក ក. ព័ត៌មានបុគ្គល</h2></center>
					</td>
				</tr>
			</table> 
			<table width='100%'>
				<tr>
					<td width="20">-</td>
					<td>
						<?= lang('លេខករណី : ')?>
						<?= $result->case_prefix.$result->case_no; ?>
					</td>
					<td>
						<?php  if($member){ ?>
                            - &nbsp;&nbsp;<?= lang('ជាអ្នកសុំសិទ្ធិជ្រកកោន')."&nbsp;".(empty($recognition)?$icon_check:$icon_uncheck);?>
							&nbsp;&nbsp;&nbsp;
							<?= lang('ជាជនភៀសខ្លួន')."&nbsp;".(!empty($recognition)?$icon_check:$icon_uncheck);?>
						<?php }else { ?> 
                            - &nbsp;&nbsp;<?= lang('ជាអ្នកសុំសិទ្ធិជ្រកកោន')."&nbsp;".(empty($recognition)?$icon_check:$icon_uncheck);?>
							&nbsp;&nbsp;&nbsp;
							<?= lang('ជាជនភៀសខ្លួន')."&nbsp;".(!empty($recognition)?$icon_check:$icon_uncheck);?>
						<?php } ?>
					</td>
				</tr> 
				<tr> 
					<td>-</td>
					<td colspan="2">		
					ជាម្ចាស់ករណី / Principle Applicant: <?php echo ($home_visit->member_id == 0 ? $icon_check : $icon_uncheck);?>
					&nbsp;&nbsp;
					ឬ ជាសមាជិកក្នុងបន្ទុក / Dependent Member: <?php echo ($home_visit->member_id >= 1 ? $icon_check : $icon_uncheck);?>
					</td> 
				</tr>
				<tr>
					<td>-</td>
					<td colspan="2">
						<?= lang('នាមត្រកូល និងនាមខ្លួន  : ')?>
						<?php 
							if($member){ 
								echo $member->lastname_kh.' '.$member->firstname_kh." / ".$member->lastname.' '.$member->firstname;
							}else {
								echo $result->lastname_kh.' '.$result->firstname_kh." / ".$result->lastname.' '.$result->firstname;
							}
						?>
					</td>

				</tr>
                <tr>
                    <td colspan="3">
                        <?php
                        if($member){
                            $gender = $member->gender;
                        }else {
                            $gender = $result->gender;
                        }
                        ?>
                        - &nbsp;&nbsp;&nbsp;&nbsp;<?= lang('ភេទ : ')?>
                        <?= lang($gender) ?>
                    </td>
                </tr>
				<tr>
					<td>-</td>
					<td>
						<?= lang('ថ្ងៃខែឆ្នាំកំណើត : ')?>
						<?php 
							if($member){ 
								echo $this->erp->toKhmer($this->erp->hrsd($member->dob));
							}else {
								echo $this->erp->toKhmer($this->erp->hrsd($result->dob));
							}
						?>
					</td>
					
					<td>
						- &nbsp;&nbsp;<?= lang('សញ្ជាតិ : ')?> 
						<?php 
							if($member){ 
								echo $member->nationality_kh;
							}else {
								echo $result->nationality_kh;
							}
						?>
					</td>
				</tr> 
				
				<tr>
					<td>-</td>
					<td>
						<?= lang('មុខរបរ : ')?>
						<?=  $home_visit->occupation_kh ?>
					</td>
					<td>
						- &nbsp;&nbsp;<?= lang('ការសិក្សាអប់រំ : ')?>
						<?=  $home_visit->education_kh ?>
					</td>
				</tr>

				<tr> 
					<td width="20" style="position: absolute;">-</td>
					<td colspan="2"> 
						<?= lang('អាសយដ្ឋានស្នាក់នៅ : ')?>
						<?=  $home_visit->address_kh." , "?>
						&nbsp;&nbsp;&nbsp;  
						<?php 
							$village = $this->applications->getTagsById($home_visit->village);  
							echo !empty($village)?"ភូមិៈ ".$village." , ":""; 
						?> 
						&nbsp;&nbsp;&nbsp; 
						<?php
							$commune = $this->applications->getAllAddressKH($home_visit->commune ,"commune");
							echo !empty($commune)?"ឃុំ / សង្កាត់: ".$commune." ":"";
						?>
						&nbsp;&nbsp;&nbsp; 
						<?php
							$district = $this->applications->getAllAddressKH($home_visit->district,"district");
							echo !empty($district)?"ស្រុក / ខណ្ឌ: ".$district." ":"";
						?>
						&nbsp;&nbsp;     
						<?php
							$province = $this->applications->getAllAddressKH($home_visit->province,"province_r");
							echo !empty($province)?"ខេត្ត / ក្រុង: ".$province." ":"";
						?>
					</td>	
				</tr> 
	 
				<tr>
					<td>-</td>
					<td colspan='5'>
						<?= lang('លេខទូរស័ព្ទទំនាក់ទំនង :')?>    
						<span ><?= $this->erp->toKhmer($home_visit->contact_number); ?></span>
					</td> 
				</tr>				
			</table>
			
			
			<table width='100%'>
				<tr>
					<td width="20">-</td>
					<td>
						មានសមាជិកនៅក្នុងបន្ទុក : 
					</td>
				</tr> 
			</table>
				
			<center>
				<table width='100%' class="relationship_table">
					<tr>
						<td>ឈ្មោះ<br/>Name</td>
						<td style="width:80px;" class='text-center'>ភេទ<br/>Sex</td>
						<td  class='text-center'>ថ្ងៃខែឆ្នាំកំណើត<br/> Date of Birth</td>
						<td  class='text-center'>សញ្ជាតិ<br/> Nationality</td>	
						<td  class='text-center'>សាសនា<br/>Religion</td>										
						<td  class='text-center'>ទំនាក់ទំនង<br/>Relationship</td>										
						<td  class='text-center'>ស្ថានភាពគ្រួសារ<br/>Family Status</td> 
					</tr>
					 
					 <?php  
					 $relationships = array();
					 foreach($relationship as $relation){
						$relationships[$relation->id] = $relation->relationship_kh;
					 }
					 foreach($result_members as $key => $member){
					 ?>
						<tr> 
							<td >
								<?= $key+1; ?>. 
								<?= $member->lastname_kh." ".$member->firstname_kh; ?> / <?= $member->lastname." ".$member->firstname; ?> 
							</td>
							<td class='center'><?= lang($member->gender) ?></td>  
							<td class='center'><?= $this->erp->toKhmer($this->erp->hrsd($member->dob)); ?></td>
							<td class='center'><?= $member->nationality_kh ?></td> 
							<td class='center'><?= $member->religion_kh ?></td> 
							<td class='center'><?= $relationships[$member->relationship] ?></td> 
							<td class='center'>
								<?php echo ($member->is_legal=="is_legal")?lang("is_legal"):" ";?>
								<?php echo ($member->is_legal=="is_not_legal")?lang("is_not_legal"):" ";?>
								<?php echo ($member->is_divorce=="is_divorce")?lang("is_divorce"):" ";?>
								<?php echo !empty($member->other)?$member->other:"";?>
							</td> 
						</tr>
						<tr>
							<td colspan="7">មុខរបរបច្ចុប្បន្ន / Current Occupation :&nbsp;<?= $member->occupation_kh ?></td> 
						</tr>
						<tr> 
							<td colspan='7'>អាសយដ្ឋាន / Address:&nbsp;  								

								<?php 
									/*$village = $this->applications->getTagsById($member->village);
									echo !empty($village)?"ភូមិៈ ".$village." , ":"​​ ";

									$commune = $this->applications->getAllAddressKH($member->commune,"commune");
									echo !empty($commune)?"ឃុំ / សង្កាត់: ".$commune." ":"​ ";

									$district = $this->applications->getAllAddressKH($member->district,"district");
									echo !empty($district)?"ស្រុក / ខណ្ឌ: ".$district." ":"​ ";

									$province = $this->applications->getAllAddressKH($member->province,"province");
									echo !empty($province)?"ខេត្ត​/ ក្រុង: ".$province." ":" ";

									$country = $this->applications->getAllAddressKH($member->country,"country");
									echo !empty($country)?"ប្រទេស: ".$country." ":" ";*/

                                $mem = $member->address_kh;
                                $village = $this->applications->getTagsById($member->village);
                                $commune = $this->applications->getAllAddressKH($member->commune,"commune_r");
                                $district = $this->applications->getAllAddressKH($member->district,"district_r");
                                $province = $this->applications->getAllAddressKH($member->province,"province_r");
                                $country = $this->applications->getAllAddressKH($member->country,"country");

                                if(!empty($country))
                                {
                                    echo !empty($mem)?$mem." , ":"";
                                    echo !empty($village)?"ភូមិៈ ".$village." , ":"​​ ";
                                    echo !empty($commune)?"ឃុំ / សង្កាត់: ".$commune." , ":"​ ";
                                    echo !empty($district)?"ស្រុក / ខណ្ឌ: ".$district." , ":"​ ";
                                    echo !empty($province)?"ខេត្ត​/ ក្រុង: ".$province." , ":" ";
                                    echo !empty($country)?"ប្រទេស: ".$country." ":" ";
                                }
                                elseif (!empty($province))
                                {
                                    echo !empty($mem)?$mem." , ":"";
                                    echo !empty($village)?"ភូមិៈ ".$village." , ":"​​ ";
                                    echo !empty($commune)?"ឃុំ / សង្កាត់: ".$commune." , ":"​ ";
                                    echo !empty($district)?"ស្រុក / ខណ្ឌ: ".$district." , ":"​ ";
                                    echo !empty($province)?"ខេត្ត​/ ក្រុង: ".$province."  ":" ";
                                    echo !empty($country)?"ប្រទេស: ".$country." ":" ";
                                }
                                elseif (!empty($district))
                                {
                                    echo !empty($mem)?$mem." , ":"";
                                    echo !empty($village)?"ភូមិៈ ".$village." , ":"​​ ";
                                    echo !empty($commune)?"ឃុំ / សង្កាត់: ".$commune." , ":"​ ";
                                    echo !empty($district)?"ស្រុក / ខណ្ឌ: ".$district."  ":"​ ";
                                    echo !empty($province)?"ខេត្ត​/ ក្រុង: ".$province."  ":" ";
                                    echo !empty($country)?"ប្រទេស: ".$country." ":" ";
                                }
                                elseif (!empty($commune))
                                {
                                    echo !empty($mem)?$mem." , ":"";
                                    echo !empty($village)?"ភូមិៈ ".$village." , ":"​​ ";
                                    echo !empty($commune)?"ឃុំ / សង្កាត់: ".$commune." ":"​ ";
                                    echo !empty($district)?"ស្រុក / ខណ្ឌ: ".$district."  ":"​ ";
                                    echo !empty($province)?"ខេត្ត​/ ក្រុង: ".$province."  ":" ";
                                    echo !empty($country)?"ប្រទេស: ".$country." ":" ";
                                }
                                elseif (!empty($village))
                                {
                                    echo !empty($mem)?$mem." , ":"";
                                    echo !empty($village)?"ភូមិៈ ".$village."  ":"​​ ";
                                    echo !empty($commune)?"ឃុំ / សង្កាត់: ".$commune." ":"​ ";
                                    echo !empty($district)?"ស្រុក / ខណ្ឌ: ".$district."  ":"​ ";
                                    echo !empty($province)?"ខេត្ត​/ ក្រុង: ".$province."  ":" ";
                                    echo !empty($country)?"ប្រទេស: ".$country." ":" ";
                                }
                                elseif (!empty($mem))
                                {
                                    echo !empty($mem)?$mem."  ":"";
                                }
                                else
                                {
                                    echo "";
                                }
                                ?>
							</td> 
						</tr> 
						<tr> 
							<td colspan='7'>លេខទូរស័ព្ទទំនាក់ទំនង / Contact Number :&nbsp;<?= $this->erp->toKhmer($member->phone);?> </td> 
						</tr> 
						
					<?php } ?> 
				</table>
				
				<style type="text/css">
					.relationship_table td{
						/* white-space: nowrap; */
						border: 1px solid #000;
						
					}
					.relationship_table:last-child{
						border:none !important;
					}
					.break_page{
						page-break-after: always;
					}
				</style> 
			
			</center>
				
			<table width='100%'>
				
				<tr>
                    <td style="margin-top: 3px;position: absolute;">-</td>
					<td style="padding-left: 25px;">
						<u><?= lang('លទ្ធផល');?></u>:
						ទំនាក់ទំនងបាន  
						<?= ($home_visit->is_contact==1)?$icon_check:$icon_uncheck;?>
						&nbsp;&nbsp;
						ទំនាក់ទំនងមិន​បាន  
						<?= ($home_visit->is_contact==0)?$icon_check:$icon_uncheck;?>
						&nbsp;&nbsp;
						មូលហេតុ : 
						&nbsp;&nbsp;
						<span class="break-word"><?= strip_tags($home_visit->contact_reason);?></span>
					</td> 
				</tr>
			</table> 
			
			
				<table width='100%'>
					<tr>
						<td>
							<center><h2 class='text-cover'>ផ្នែក ខ. ស្ថានភាពរស់នៅ</h2></center>
						</td>
					</tr>
				</table>
				
				<table width='80%' >
					<tr>
						<td width="20">-</td>
						<td>
							<?= lang('ជីវភាពគ្រួសារ  : ')?>
							&nbsp;&nbsp;
							<?= lang('មាន')?>
							&nbsp;
							<?= ($home_visit->family_status=='rich')?$icon_check:$icon_uncheck; ?>
							&nbsp;
							<?= lang('មធ្យម')?>
							&nbsp;
							<?= ($home_visit->family_status=='medium')?$icon_check:$icon_uncheck; ?>
							&nbsp;
							<?= lang('ក្រ')?>
							&nbsp;&nbsp;
							<?= ($home_visit->family_status=='poor')?$icon_check:$icon_uncheck; ?>
						</td>
					</tr> 
				</table>
				
				<table width='100%' >
					<tr>
						<td width="20">-</td>
						<td>
							<?= lang('ស្ថានភាពសុខភាព : ')?> 
							<span ><?= $home_visit->health_status; ?></span>
						</td>
					</tr> 
					<tr>
						<td>-</td>
						<td >
							<?= lang('ជំនួយឧបត្ថម្ភ : ')?>
							&nbsp;&nbsp;
							<?= lang('គ្មាន')."&nbsp;".($home_visit->sponsorship_support==0?$icon_check:$icon_uncheck);?> 
							&nbsp;&nbsp;
							<?= lang('មាន')."&nbsp;".($home_visit->sponsorship_support==1?$icon_check:$icon_uncheck);?>
							&nbsp;&nbsp;
							<?= lang('ពី :') ?>
							&nbsp;&nbsp;
							<span ><?= $home_visit->sponsorship_support_by; ?></span>
						</td>
					</tr> 
					<tr>
						<td>-</td>
						<td>
 							<?php

							$spon_sup_mon = explode(',',$home_visit->sponsorship_support_money);
							$in_sur_on = explode(',',$home_visit->insurance_service_on);

 							echo lang('ជាថវិកា')."&nbsp;&nbsp;";
 							echo (in_array('1', $spon_sup_mon))?$icon_check:$icon_uncheck;
 							echo "&nbsp;&nbsp;";
 							echo lang('ជាសម្ភារៈផ្សេងៗ')."&nbsp;&nbsp;";

 							echo (in_array('0', $spon_sup_mon))?$icon_check:$icon_uncheck;

 							 ?>

							
							&nbsp;&nbsp;
							<?= lang(':') ?>
							&nbsp;&nbsp;
							<span ><?= $home_visit->sponsorship_support_other; ?></span>
						</td> 
					</tr>
					<tr>
						<td>-</td>
						<td>
							<?= lang('សេវាធានារ៉ាប់រង : ')?> 
							&nbsp;&nbsp;
							<?= lang('គ្មាន')."&nbsp;".($home_visit->insurance_service==0?$icon_check:$icon_uncheck);?>
							&nbsp;&nbsp;
							<?= lang('មាន')."&nbsp;".($home_visit->insurance_service==1?$icon_check:$icon_uncheck);?>
							&nbsp;&nbsp;
							<?= lang('ពីអង្គការ : ');?>​
							&nbsp;&nbsp;
							<span ><?= $home_visit->insurance_service_by; ?></span>
						</td>
					</tr> 
					<tr>	
						<td style="position: absolute; white-space: normal; margin-top: 4px;">-</td>
						 <td>
						 <?= lang('លើផ្នែក : ');?>​
						​​​​​​​ &nbsp;&nbsp;​​
						 <?= lang('សុខភាព')."&nbsp;".((in_array('health', $in_sur_on))? $icon_check : $icon_uncheck)?>
						 &nbsp;&nbsp;
						 <?= lang('អាយុជីវិត')."&nbsp;".((in_array('life', $in_sur_on))? $icon_check : $icon_uncheck)?>
						 &nbsp;&nbsp;
						 <?= lang('ផ្សេងទៀត')."&nbsp;".((in_array('other', $in_sur_on))? $icon_check : $icon_uncheck)?>						 
						  &nbsp;&nbsp;

						  <span ><?= strip_tags($home_visit->insurance_service_other); ?></span>
						 </td>
					</tr>
					<tr>	
						<td style="position: absolute;">-</td>
						<td>
							<?= lang('សំណូមពរផ្សេងៗ : ');?>
							<span class="break-word"><?= $home_visit->suggestions; ?></span>
						</td>		
					</tr>
				</table>
				
				<div class="clearfix"></div>
				
				<table width='100%'>
					<tr>
						<td>
							<center><h2 class='text-cover'>ផ្នែក គ.  ព័ត៌មានអាជ្ញាធរមូលដ្ឋាន</h2></center>
						</td>
					</tr>
				</table>
				
				<table width='100%'  style='white-space:nowrap;' >
					<tr>
						<td width="20">-</td>
						<td>
							<?= lang('ឈ្មោះមេភូមិ / នាយប៉ុស្តិ៍នគរបាលរដ្ឋបាល : ')?>  
							<span ><?= strip_tags($home_visit->local_authorities_name) ?></span>
						</td>
					</tr>
					<tr>
						<td></td>
						<td> 
							<?= lang('លេខទូរស័ព្ទទំនាក់ទំនង : ')?>  
							<span ><?= $this->erp->toKhmer($home_visit->local_authorities_contact); ?></span>
						</td>
					</tr> 
					<tr>
						<td style="position: absolute; margin-top: 4px;">-</td>
						<td style="white-space: normal;">
							<?= lang('មតិរបស់មេភូមិ / នាយប៉ុស្តិ៍នគរបាលរដ្ឋបាល : ')?>
							&nbsp;&nbsp;
							<?= lang('មិនធ្លាប់មានពិរុទ្ធ')."&nbsp;".($home_visit->local_authorities_opinion == 0 ? $icon_check : $icon_uncheck)?>
							&nbsp;&nbsp;
							<?= lang('ធ្លាប់មានពិរុទ្ធ')."&nbsp;".($home_visit->local_authorities_opinion == 1 ? $icon_check : $icon_uncheck)?> : 
							&nbsp;&nbsp;
							<span class="break-word"><?= strip_tags($home_visit->local_authorities_comment) ?></span>
						</td>
					</tr>
					<tr>
						<td></td>
						<td></td>
					</tr>
					
					<tr>
						<td style="position: absolute; white-space: normal;">-</td>
						<td colspan='3' style="white-space: normal;">
							<?= lang('មតិសម្រាប់មន្ត្រីចុះសួរសុខទុក្ខ : ')?> <span class="break-word"><?= strip_tags($home_visit->feedback_officials) ?></span>
						</td>
					</tr>
				</table>  
				
				<br/>
				
				<div class="clearfix"></div>
				
				<div  class='div-right'>
					<table width='100%'  class='text-center'>
						<tr>
							<td> 
								<i>
								<?= $this->erp->footer_date_khmer("រាជធានីភ្នំពេញ");?>
								</i>
								<h4>ហត្ថលេខា និងឈ្មោះមន្ត្រីចុះសួរសុខទុក្ខ</h4>
							</td>
						</tr>
					</table>
				</div>
				
				<div class="clearfix"></div>
				
			</div>	 
		</div> 
	</div>
</div>

<style type="text/css">
	@media print{
		.bblack{ background: white !important; }   
		.modal-content, .box, 
		.modal-header{ border:none!important; }
		.table { border:none !important; }
		.bordered {border:1.5px solid black;}
		.box-content{
			padding: 20px !important;
		}
	} 
	table td{
		padding: 6px;		
	}	
	hr{
		padding:0px;
		margin:0px;
		border-top:none !important;
		border-bottom:dotted 1px !important;
	}   
</style>