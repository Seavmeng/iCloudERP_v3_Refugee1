<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">				
			<button type="button" class="close" data-dismiss="modal">
				<span aria-hidden="true"><i class="fa fa-2x">&times;</i></span>
				<span class="sr-only"><?=lang('close');?></span>
			</button>
			<h4 class="modal-title" id="payModalLabel"><?= lang('family_guarantee_request_option'); ?></h4>
		</div>
		<?php $attributes = array('id' => 'form1'); ?>
		<?php  echo form_open_multipart("application_forms2/family_guarantee_request_option/". $id, $attributes); ?> 
		<div class="modal-body">		
			<div class="col-sm-12"> 
				<?php $enabled = 0;  ?>
				<?php if($guarantee->status == "pending"){ $enabled = 1; ?>
					<div class="form-group">
						<?php echo lang('date','date'); ?>​ 
						<span class="red">*</span>
						<div class="control">
							<input type="text" id='date' name='date'  class="form-control input-sm date"​ />						
						</div>
					</div>
					<div class="form-group">
						<?php echo lang('status', 'status'); ?> 
						<div class="controls">
							<?php $status1 = array("sign1"=>lang("sign1"), "unsign1"=>lang("unsign1")); ?>
							<?php echo form_dropdown('status1', $status1, 0, ' class="form-control" '); ?>												
						</div>
					</div>
				<?php } ?> 
				<?php if($guarantee->status == "sign1"){ $enabled = 1; ?>
					<div class="form-group">
						<?php echo lang('date','date'); ?>​ 
						<span class="red">*</span>
						<div class="control">
							<input type="text" id='date' name='date'  class="form-control input-sm date"​ />						
						</div>
					</div>
					<div class="form-group">
						<?php echo lang('status', 'status'); ?> 
						<div class="controls">
							<?php $status2 = array( "sign2"=>lang("sign2"), "unsign2"=>lang("unsign2")); ?>
							<?php echo form_dropdown('status2', $status2, 0, ' class="form-control" '); ?>												
						</div>
					</div>
				<?php } ?>
				
				<?php if($guarantee->status == "sign2"){ $enabled = 1; ?>
					<div class="form-group">
						<?php echo lang('date','date'); ?>​ 
						<span class="red">*</span>
						<div class="control">
							<input type="text" id='date' name='date'  class="form-control input-sm date"​ />						
						</div>
					</div>
					<div class="form-group">
						<?php echo lang('status', 'status'); ?> 
						<div class="controls">
							<?php $status3 = array("sign3"=>lang("sign3"), "unsign3"=>lang("unsign3")); ?>
							<?php echo form_dropdown('status3', $status3, 0, ' class="form-control" '); ?>												
						</div>
					</div>
				<?php } ?>  
			</div>​
		</div>
		<div class="clearfix"></div>
		<?php if($enabled == 1){ ?>
			<div class="modal-footer">		
			   <?php echo form_submit('submit', lang('submit'), 'class="btn btn-primary save"'); ?>
			</div>
		<?php } ?>
		
		<?php  echo form_close(); ?>
	</div>
</div>
<?= $modal_js ?>


