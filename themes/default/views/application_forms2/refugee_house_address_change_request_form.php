<?php   
	$icon_check='<span style="font-size:17px;">&#x2611;</span>';
	$icon_uncheck='<span style="font-size:17px;">&#x2610;</span>'; 	
?>
 
<div class="box a4"> 
		<div class="box-content" >  		 
				<?php $this->load->view($this->theme."applications/head-cover_1"); ?> 
				
				<div class="col-sm-12 text-center">
					<h2 class='text-cover'>ពាក្យស្នើសុំផ្លាស់ប្តូរទីលំនៅរបស់ជនភៀសខ្លួន</h2> 
					<h3>REQUEST FOR REFUGEE'S HOUSE ADDRESS CHANGE</h3>
				</div>
				
				<div class='wrap-content'>  				
					<table width='100%' class='' style='white-space:nowrap;'> 
						<tr>
							<td width='30'>១.</td>
							<td>ជាម្ចាស់ករណីលេខ​​​​​​​​ / Priciple Applicant Case Number:
								<?= $result->case_prefix.$result->case_no; ?> 								
							 </td>
						</tr>
						
						<tr>
							<td width='30'>២.</td>
							<td colspan='2'>នាមត្រកូលនិងនាមខ្លួន / Surname and Given Name: &nbsp;
							<?= $result->lastname_kh.' '.$result->firstname_kh." / ". strtoupper($result->lastname.' '.$result->firstname) ?>
							</td>
						</tr>
						
						<tr>
							<td width='30'>៣.</td>
							<td colspan='2'>សមាជិកនៅក្នុងបន្ទុកចំនួន / Number of Dependents: &nbsp;<?=  $this->erp->toKhmer(0);?>នាក់</td>							
						</tr> 						
						<tr>
							<td width='30'>៤.</td>
							<td>ថ្ងៃខែឆ្នាំកំណើត / ​​ Date of Birth: &nbsp; 
							<?= $this->erp->toKhmer($this->erp->hrsd($result->dob)) ?>
							&nbsp;&nbsp; 
							ភេទ / Sex: ប្រុស / Male: &nbsp;
							<?php echo  ($result->gender == 'male' ? $icon_check : $icon_uncheck);?>
							&nbsp;&nbsp; 
							ស្រី / Female:&nbsp; 
							<?php echo ($result->gender == 'female' ? $icon_check : $icon_uncheck);?></td>
						</tr> 
						<tr>
							<td width='30'>៥.</td>
							<td>ទីកន្លែងកំណើត / Place of Birth: &nbsp;<?= $result->pob_kh ?> </td> 
						</tr>  
						<tr>
							<td width='30'>៦.</td>
							<td  >សញ្ញាតិ / Nationality: &nbsp; <?= $result->nationality_kh." / ".$result->nationality ?> </td>
						</tr> 
						<tr>
							<td width='30'>៧.</td>
							<td >ថ្ងៃខែឆ្នាំ​ទទួលឋានៈ / Date of Status Recognition:&nbsp;  <?= $this->erp->hrsd($decision->recognized_date); ?> </td>
						</tr> 
						<tr>
							<td width='30'>៨.</td>
							<td>មុខរបរបច្ចុប្បន្ន / Current Occupation: 
								<?= $row->occupation  ?>
							</td>
						</tr> 
						<tr>
							<td width='30'>៩.</td>
							<td>លេខទូរស័ព្ទទំនាកទំនង / Contact Number: <?= $row->contact_number ?> </td>
						</tr>  
						<tr>
							<td width='30'>១១.</td>
							<td>សូមផ្លាស់ប្តូរមកអាស័យដ្ឋានថ្មី / New Address:  &nbsp;
							<?= $this->applications->getAllAddressKH($row->country,"country"); ?> 
							<?= $this->applications->getAllAddressKH($row->province,"province"); ?>
							<?= $this->applications->getAllAddressKH($row->district,"district"); ?>
							<?= $this->applications->getAllAddressKH($row->communce,"commune"); ?>
							<?= $this->applications->getTagsById($row->village); ?> ,
							<?= $row->address_kh ?> 
							</td>
						</tr> 
					</table>
					
					<table width='100%' class="space-size">
						<tr>
							<td>
							ខ្ញុំបាទ / នាងខ្ញុំសូមគោរពស្នើសុំនាយកដ្ឋានជនភៀសខ្លួន នៃអគ្គនាយកដ្ឋានអន្តោប្រវេសន៍មេត្តាអនុញ្ញាតិអោយខ្ញុំបាទ / នាងខ្ញុំ បានទៅរស់នៅ អាសយដ្ឋានថ្មីដោយក្តីអនុគ្រោះ។
							<br/>
							I hereby would like to request the Refugee Department, General Department of Immigration to kindly allow me to live in the new address as indicated above.
							</td>
						</tr>
					</table> 
				<div class='clearfix'></div> 
					<?php $this->load->view($this->theme."application_forms/form_footer"); ?> 
				<div class='clearfix'></div>
			</div>
		
		</div>  
		
</div>

<style type="text/css"> 
	@media print{
		.bblack{ background: white !important; }   
		.modal-content, .box, 
		.modal-header{ border:none!important; }
		.table { border:none !important; }
	}
	.wrap-content {
		min-height:800px;
		font-size:14px;
	}
	.wrap-content table td {
		padding:6px; 
	}
</style>