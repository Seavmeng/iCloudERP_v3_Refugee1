<ul id="myTab" class="nav nav-tabs">
	<li class="">
		<a href="#content_1" class="tab-grey">
			 <?= lang('ផ្នែក ក.ព័ត៌មានបុគ្គល'); ?>​ 
			<i class="fa fa-chevron-right" aria-hidden="true"></i>
		</a>
	</li>
	<li class="">
		<a href="#content_2" class="tab-grey">
			<?= lang('ផ្នែក ខ.ស្ថានភាពរស់នៅ');?>
			<i class="fa fa-chevron-right" aria-hidden="true"></i>
		</a>
	</li> 
	<li class="">
		<a href="#content_3" class="tab-grey">
			<?= lang('ផ្នែក គ.ព័ត៌មានអាជ្ញាធរមូលដ្ឋាន');?>
			<i class="fa fa-chevron-right" aria-hidden="true"></i>
		</a>
	</li> 
	<li class="">
		<a href="#content_4" class="tab-grey">
			<?= lang('សមាជិកគ្រួសារ');?>
			<i class="fa fa-chevron-right" aria-hidden="true"></i>
		</a>
	</li>
</ul>
<div class="tab-content">
	<?php 
		$countries_ = array(lang("select"));
		foreach($countries as $country){
			$countries_[$country->id] = $country->country;
		} 
		?>
		<?php 
		$provinces_ = array(lang("select"));
		foreach($provinces as $province){
			$provinces_[$province->id] = $province->name;
		} 
		?>
		<?php 
		$districts_ = array(lang("select"));
		foreach($districts as $district){
			$districts_[$district->id] = $district->name;
		} 
		?>
		<?php 
		$communes_ = array(lang("select"));
		foreach($communes as $commune){
			$communes_[$commune->id] = $commune->name;
		} 
	?>
	<div id="content_1" class="tab-pane fade in">
		<div class="box">
			<div class="box-content"> 
				
				<div class="row">
					
					<?php echo form_open("application_forms2/edit_home_visit/".$id);  ?>
					
					<div class="col-sm-6">
						
						<div class="col-sm-12">
							<div class="form-group">
								<?php echo lang('លេខករណីៈ', 'លេខករណីៈ'); ?>
								<span class="red">*</span>
								<input type="text" readonly="readonly" name="case_no" id="case_no" class="form-control input-sm" value="<?= set_value("case_no", $application->case_prefix.$application->case_no) ?>" />
								<input type="hidden" name="application_id" id="application_id" value="<?= set_value("application_id", $row->application_id) ?>" />
							</div>
							
							<div class="form-group">
								<?php echo lang('ឈ្មោះ', 'ឈ្មោះ'); ?>​ 
								<?php
									$members_ = array($application->firstname_kh . " " . $application->lastname_kh." ".lang("( ម្ចាស់ករណី)"));
									foreach($default_members as $member){
										$members_[$member->id] = $member->firstname_kh . " " . $member->lastname_kh." ".lang("( សមាជិកក្នុងបន្ទុក)");
									}						
								?>
								<span class="red">*</span>
								<div class="control" id="members">
									<?php echo form_dropdown('member', $members_, $row->member_id, ' class="form-control" '); ?>
								</div>
							</div> 							
						</div>						
						
						<div class="clearfix"></div>
						
						<div class="col-sm-6">
							<div class="form-group">
								<?php echo lang('firstname', 'firstname'); ?>
								<small class="red">* (<?= lang('just_information'); ?>)</small>
								<input class="form-control input-sm" type="text" name="firstname" value="<?= set_value("firstname", $application->firstname_kh) ?>" />
							</div>
						</div>
						
						<div class="col-sm-6">
							<div class="form-group">
								<?php echo lang('lastname', 'lastname'); ?>
								<small class="red">* (<?= lang('just_information'); ?>)</small>
								<input class="form-control input-sm" type="text" name="lastname" value="<?= set_value("lastname", $application->lastname_kh) ?>" />
							</div>
						</div>
						
						<div class="col-sm-6">
							<div class="form-group">
								<?php echo lang('ភេទ', 'ភេទ'); ?>
								<small class="red">* (<?= lang('just_information'); ?>)</small>
								<div class="controls">
									<label class="radio-inline">  
										<input type="radio" value="male" <?= ($application->gender=='male'?'checked':'') ?> name="gender" /> <?= lang('male'); ?>
									</label>
									<label class="radio-inline">
										<input type="radio" value="female" <?= ($application->gender=='female'?'checked':'') ?> name="gender"/> <?= lang('female'); ?>
									</label> 
								</div>
							</div>
						</div>
						
						<div class="col-sm-6">
							<div class="form-group">
								<?php echo lang('ថ្ងៃខែឆ្នាំកំណើត', 'ថ្ងៃខែឆ្នាំកំណើត'); ?>
								<small class="red">* (<?= lang('just_information'); ?>)</small>
								<div class="controls">
									<input type="text" value="<?= set_value('dob', $this->erp->hrsd($application->dob)) ?>" name="dob" class="form-control input-sm date" />
								</div>
							</div>
						</div>
						
						<div class="col-sm-6">
							<div class="form-group">
								<?php echo lang('មុខរបរ ', 'មុខរបរ '); ?>
								<input class="form-control input-sm" value="<?= set_value('occupation_kh', $row->occupation_kh) ?>" type="text" name="occupation_kh" />
							</div>
						</div>
						
						
						<div class="col-sm-6">
							<div class="form-group">
								<?php echo lang('មុខរបរ ', 'មុខរបរ '); ?> (EN)
								<input class="form-control input-sm" value="<?= set_value('occupation', $row->occupation) ?>" type="text" name="occupation" />
							</div>
						</div>
						
						<div class="col-sm-6">
							<div class="form-group">
								<?php echo lang('ការសិក្សាអប់រំ', 'ការសិក្សាអប់រំ '); ?>
								<input class="form-control input-sm" value="<?= set_value('education_kh', $row->education_kh) ?>" type="text" name="education_kh" />
							</div>							
						</div>
						
						<div class="col-sm-6">
							<div class="form-group">
								<?php echo lang('ការសិក្សាអប់រំ', 'ការសិក្សាអប់រំ '); ?> (EN)
								<input class="form-control input-sm" value="<?= set_value('education', $row->education) ?>" type="text" name="education" />
							</div>							
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<?php echo lang('លេខទូរស័ព្ទទំនាក់ទំនង   ', 'លេខទូរស័ព្ទទំនាក់ទំនង'); ?>
								<span class="red">*</span>
								<input class="form-control input-sm bfh-phone" value="<?= set_value('contact_number', $row->contact_number) ?>" data-format="+855 (ddd) ddd-dddd"  type="text" name="contact_number" />
							</div>							
						</div> 
						<div class="col-sm-6">
							<div class="form-group">
								<?php echo lang('ថ្ងៃចុះសួរសុខទុក្ខ​   ', 'ថ្ងៃចុះសួរសុខទុក្ខ​'); ?>
								<span class="red">*</span>
								<input type="text" value="<?= ($row->day_visit?$this->erp->hrsd($row->day_visit):date("d/m/Y")) ?>" class="form-control input-sm date" name="day_visit" />
							</div>							
						</div> 
						
						<div class="col-sm-12">
							<div class="form-group">
							<?php echo lang('មូលហេតុ ', 'មូលហេតុ'); ?> 						
							<span class="red">*</span>
							<div class="controls">
								<label class="radio-inline">
									<input type="radio" value="1" name="is_contact" <?= $row->is_contact==1?"checked":""?> class="skip"/> <?= lang('ទំនាក់ទំនង​បាន '); ?>
								</label>
								<label class="radio-inline">
									<input type="radio" value="0" name="is_contact" <?= $row->is_contact==0?"checked":""?> class="skip" /> <?= lang('ទំនាក់ទំនងមិន​បាន'); ?>
								</label> 
								</div>
							</div>
							<textarea name="contact_reason" class=" form-control"><?= set_value('contact_reason', strip_tags($row->contact_reason)) ?></textarea>
						</div>  
					</div>
					
					<div class="col-sm-6">
					
					<div class="panel panel-warning">
						
						<div class="panel-heading"><?php echo lang("address","address") ?></div>
						
						<div class="panel-body" style="padding: 5px;">
						
							<div class="col-lg-12">
								<div class="form-group">
									<?php echo lang('country', 'country'); ?> 
									<div class="controls">
										<?php echo form_dropdown('country', $countries_, set_value('country', $row->country), ' class="form-control" '); ?>												
									</div>
								</div>
							</div>
							
							<div class="col-lg-6">
								<div class="form-group">
									<?php echo lang('province', 'province'); ?> 
									<div class="controls">
										<?php echo form_dropdown('province', $provinces_, set_value('province', $row->province), ' class="form-control" '); ?>												
									</div>
								</div>
							</div>
							
							<div class="col-lg-6">
								<div class="form-group">
									<?php echo lang('district', 'district'); ?> 
									<div class="controls">
										<?php echo form_dropdown('district', $districts_, set_value('district', $row->district), ' class="form-control" '); ?>												
									</div>
								</div>
							</div>
							
							<div class="col-lg-6">
								<div class="form-group">
									<?php echo lang('commune', 'commune'); ?> 
									<div class="controls">
										<?php echo form_dropdown('commune', $communes_, set_value('commune', $row->commune), ' class="form-control" '); ?>												
									</div>
								</div>
							</div>
							
							<div class="col-lg-6">
								<div class="form-group">
									<?php echo lang('village', 'village'); ?> 
									<div class="controls">
										<?php 
										echo form_input('village', set_value('village', $this->applications->getTagsById($row->village)),'class="form-control village" '); ?>											
									</div>
								</div>
							</div>
							
							<div class="col-lg-6 hidden">
								<div class="form-group">
									<?php echo lang('ក្រុម', 'ក្រុម'); ?>
									<span class="red">*</span>
									<div class="controls">
										<?php 
										echo form_input('group', set_value('group', $row->group),'class="form-control group_no" '); ?>											
									</div>
								</div>
							</div>
							
							<div class="col-lg-6 hidden">
								<div class="form-group">
									<?php echo lang('ផ្លូវ', 'ផ្លូវ'); ?>
									<span class="red">*</span>
									<div class="controls">
										<?php 
										echo form_input('street', set_value('street', $row->street),'class="form-control street_no" '); ?>											
									</div>
								</div>
							</div>
							
							<div class="col-lg-6 hidden">
								<div class="form-group">
									<?php echo lang(' ផ្ទះលេខ ', ' ផ្ទះលេខ '); ?>
									<span class="red">*</span>
									<div class="controls">
										<?php echo form_input('house', set_value('house', $row->house),'class="form-control house_no" '); ?>											
									</div>
								</div>
							</div>
							
							<div class="col-lg-12">
								<div class="form-group"> 
									<?php echo lang('other', 'other'); ?>​ (KH) 
									<div class="control">
										<input type="text" value="<?= set_value('address_kh', $row->address_kh) ?>" class="form-control input-sm" name="address_kh"  id="address_kh" />
									</div> 
								</div> 
							</div> 
							
							<div class="col-lg-12">
								<div class="form-group"> 
									<?php echo lang('other', 'other'); ?>​ (EN) 
									<div class="control">
										<input type="text" value="<?= set_value('address', $row->address) ?>" class="form-control input-sm" name="address"  id="address" />
									</div> 
								</div> 
							</div> 
							
						</div>
						
					</div>
					
				</div>					
			
				</div>				
			
			</div>
		</div>
	</div>
	<div id="content_2" class="tab-pane fade in">
		<div class="box">
			<div class="box-content"> 
				<div class="row">
					<div class="col-lg-6">
						<div class="form-group">
							<?php echo lang('ជីវភាពគ្រួសារ', 'ជីវភាពគ្រួសារ'); ?>
							<span class="red">*</span>
							<div class="controls">
								<label class="radio-inline">
									<input type="radio" value="rich" <?= ($row->family_status=='rich'?'checked':'') ?> name="family_status" class="skip"/> <?= lang('មាន '); ?>
								</label>
								<label class="radio-inline">
									<input type="radio" value="medium" <?= ($row->family_status=='medium'?'checked':'') ?> name="family_status" class="skip" /> <?= lang('មធ្យម'); ?>
								</label> 
								<label class="radio-inline">
									<input type="radio" value="poor" <?= ($row->family_status=='poor'?'checked':'') ?> name="family_status" class="skip" /> <?= lang('ក្រ'); ?>
								</label> 
							</div>
						</div>
					</div>
					
					<div class="col-lg-6">
						<div class="form-group"> 
							<?php echo lang('ស្ថានភាពសុខភាព', 'ស្ថានភាពសុខភាព'); ?>​ 
							<span class="red">*</span>
							<div class="control">
								<input type="text" name="health_status" value="<?= set_value('health_status', $row->health_status) ?>" class="form-control input-sm" />
							</div> 
						</div> 
					</div> 
					
					<div class="col-lg-6">
					
						<div class="panel panel-warning">
							
							<div class="panel-heading"><?php echo lang("ជំនួយឧបត្ថម្ភ","ជំនួយឧបត្ថម្ភ") ?></div>
							
							<div class="panel-body" style="padding: 5px;">
							
								<div class="col-lg-12">
									<div class="form-group">
										<div class="controls">
											<label class="radio-inline">
												<input type="radio" value="0" <?= ($row->sponsorship_support==0?'checked':'') ?> name="sponsorship_support" id="have_sponsorship_support" class="skip"> <?= lang('គ្មាន'); ?>
											</label>
											<label class="radio-inline" >
												<input type="radio" value="1" <?= ($row->sponsorship_support==1?'checked':'') ?> name="sponsorship_support" id="no_have_sponsorship_support" class="skip"> <?= lang('មាន'); ?>
											</label>
										</div>
									</div>
									
									<div class="form-group">
										<?php echo lang('ជំនួយឧបត្ថម្ភ (ពី)', 'ជំនួយឧបត្ថម្ភ'); ?>
										<span class="red">*</span>
										<div class="controls">
											<input type="text" value="<?= set_value("sponsorship_support_by", $row->sponsorship_support_by) ?>" name="sponsorship_support_by" class="form-control input-sm">
										</div>
									</div>
									<?php
										$in_sur_on = explode(',',$row->insurance_service_on);
										$spon_sup_mon = explode(',',$row->sponsorship_support_money);
									?>
									<div name="sponsor_hide">
										<div class="form-group">
											<?php echo lang('ប្រភេទជំនួយ', 'ប្រភេទជំនួយ'); ?>
											<span class="red">*</span>
											<div class="controls">
												<label class="checkbox-inline">
													<input type="checkbox" value="1"  name="sponsorship_support_money[]" class="skip" <?= (in_array('1', $spon_sup_mon))?'checked':'' ?>> <?= lang('ជាថវិកា '); ?>
												</label>
												<label class="checkbox-inline">
													<input type="checkbox" value="0"  name="sponsorship_support_money[]" class="skip" <?= (in_array('0', $spon_sup_mon))?'checked':'' ?>> <?= lang('ជាសម្ភារៈផ្សេងៗ '); ?>
												</label>
											</div>
										</div>
										<div class="form-group">
											<?php echo lang('បើសិន (​​ជាសម្ភារៈផ្សេងៗ)', 'ជំនួយឧបត្ថម្ភ'); ?>
											<span class="red">*</span>
											<div class="controls">
												<input type="text" value="<?= set_value("sponsorship_support_other", $row->sponsorship_support_other) ?>" name="sponsorship_support_other" class="form-control input-sm">
											</div>
										</div>
									</div>
								</div>
							</div>						
						
						</div>
						
						
						<div class="form-group">
							<?php echo lang("សំណូមពរផ្សេងៗ","សំណូមពរផ្សេងៗ"); ?>
							<div class="controls">
								<textarea name="suggestions" class="form-control skip"><?= set_value("suggestions",$row->suggestions) ?></textarea>
							</div>							
						</div>
						
					</div>
					
					
					
					<div class="col-lg-6">
					
						<div class="panel panel-warning">
							
							<div class="panel-heading"><?php echo lang("សេវាធានារ៉ាប់រង","សេវាធានារ៉ាប់រង") ?></div>
							
							<div class="panel-body" style="padding: 5px;">
								
								<div class="col-lg-12">
									<div class="form-group">
										<div class="controls">
											<label class="radio-inline">
												<input type="radio" id="have_insurance_service" value="0" <?= ($row->insurance_service==0?'checked':'') ?> name="insurance_service" class="skip"> <?= lang('គ្មាន'); ?>
											</label>
											<label class="radio-inline">
												<input type="radio" id="no_have_insurance_service" value="1" <?= ($row->insurance_service==1?'checked':'') ?> name="insurance_service" class="skip"> <?= lang('មាន'); ?>
											</label>
										</div>
									</div>
									
									<div class="form-group">
										<?php echo lang('សេវាធានារ៉ាប់រង (ពីអង្គការ)', 'សេវាធានារ៉ាប់រង'); ?>
										<span class="red">*</span>
										<div class="controls">
											<input type="text"​ value="<?= set_value("insurance_service_by",$row->insurance_service_by) ?>" name="insurance_service_by" class="form-control input-sm">
										</div>
									</div>
									
									<div name="hide_insurance_service">

										<div class="form-group">
											<?php echo lang('លើផ្នែក', 'លើផ្នែក'); ?> 
											<div class="controls">
												<label class="checkbox-inline">
													<input type="checkbox" name="insurance_service_on[]"  value="health" class="skip" <?= (in_array("health", $in_sur_on))?'checked':'' ?>> <?= lang('សុខភាព'); ?>
												</label>
												<label class="checkbox-inline">
													<input type="checkbox" name="insurance_service_on[]"  value="life" class="skip" <?= (in_array("life", $in_sur_on))?'checked':'' ?>> <?= lang('អាយុជីវិត'); ?>
												</label>
												<label class="checkbox-inline">
													<input type="checkbox" name="insurance_service_on[]"  value="other" class="skip" <?= (in_array("other", $in_sur_on))?'checked':'' ?>> <?= lang('ផ្សេងទៀត'); ?>
												</label>
											</div>
										</div>
										
										<div class="form-group">
											<?php echo lang('បើសិន (ផ្សេងទៀត)', 'ជំនួយឧបត្ថម្ភ'); ?>
											<span class="red">*</span>
											<div class="controls">
												<input type="text" value="<?= set_value("insurance_service_other",$row->insurance_service_other) ?>" name="insurance_service_other" class="form-control input-sm">
											</div>
										</div>

									</div>
								</div>
								
							</div>
							
						</div>
						
					</div>
				
				</div>
				
			</div>
		</div>
	</div>
	
	<div id="content_3" class="tab-pane fade in">
	
		<div class="box">
		
			<div class="box-content"> 
				
				<div class="row">
				
					<div class="col-lg-6">
					
						<div class="panel panel-warning">
							
							<div class="panel-heading"><?php echo lang("ព័ត៌មានអាជ្ញាធរមូលដ្ឋាន"," ព័ត៌មានអាជ្ញាធរមូលដ្ឋាន") ?></div>
							
							<div class="panel-body" style="padding: 5px;">
								
								<div class="col-lg-12">
								
									<div class="form-group">
										<?php echo lang('ឈ្មោះមេភូមិ / នាយប៉ុស្តិ៍នគរបាលរដ្ឋបាលៈ', 'ឈ្មោះមេភូមិ / នាយប៉ុស្តិ៍នគរបាលរដ្ឋបាលៈ'); ?>
										<span class="red">*</span>
										<div class="controls">
											<input type="text" value="<?= set_value("local_authorities_name",$row->local_authorities_name) ?>" name="local_authorities_name" class="form-control input-sm" />
										</div>
									</div>
									
									<div class="form-group">
										<?php echo lang('លេខទូរស័ព្ទទំនាក់ទំនងៈ', 'លេខទូរស័ព្ទទំនាក់ទំនងៈ'); ?>
										<span class="red">*</span>
										<div class="controls">
											<input type="text" value="<?= set_value("local_authorities_contact",$row->local_authorities_contact) ?>" name="local_authorities_contact" class="form-control input-sm" />
										</div>
									</div>
									
									<div class="form-group">
										<?php echo lang('មតិរបស់មេភូមិ / នាយប៉ុស្តិ៍នគរបាលរដ្ឋបាលៈ', 'មតិរបស់មេភូមិ / នាយប៉ុស្តិ៍នគរបាលរដ្ឋបាលៈ'); ?>
										<span class="red">*</span>
										<div class="controls">
											<label class="radio-inline">
												<input type="radio" name="local_authorities_opinion" <?= ($row->local_authorities_opinion==0?'checked':'') ?> value="0" class="skip"> <?= lang('មិនធ្លាប់មានពិរុទ្ធ  '); ?>
											</label>
											<label class="radio-inline">
												<input type="radio" name="local_authorities_opinion" <?= ($row->local_authorities_opinion==1?'checked':'') ?> value="1" class="skip"> <?= lang('ធ្លាប់មានពិរុទ្ធ '); ?>
											</label>
										</div>
									</div>
									
									<div class="form-group">
										<?php echo lang('បើ​សិន (ធ្លាប់មានពិរុទ្ធ)', 'បើ​ធ្លាប់មានពិរុទ្ធ'); ?>
										<span class="red">*</span>
										<div class="controls">
											<textarea name="local_authorities_comment" class="skip form-control" ><?= set_value("local_authorities_comment",$row->local_authorities_comment) ?></textarea>
										</div>
									</div>
									
								</div>
								
							</div>
							
						</div>
						
						<div class="col-sm-12">	
							<div class="form-group">
								<input type="submit" class="btn btn-danger" name="submit" value="<?= lang("submit") ?>" />																			
							</div>						
						</div>
						
					</div>
				
				<div class="col-sm-6">
					<div class="form-group">
						<?php echo lang('មតិសម្រាប់មន្ត្រីចុះសួរសុខទុក្ខៈ', 'មតិសម្រាប់មន្ត្រីចុះសួរសុខទុក្ខៈ'); ?>
						<span class="red">*</span>
						<div class="controls">
							<textarea name="feedback_officials" class="skip form-control"><?= $row->feedback_officials ?></textarea>
						</div>
					</div>
				</div>
								
			</div>
			
		</div>
			
	</div>	
	<?php echo form_close(); ?> 
</div> 
	
	<div id="content_4" class="tab-pane fade in">
		<div class="box">
			<div class="box-content">
				<div class="row">   
					<div class="col-sm-12">
						<div class="form-group">
							<label><?= lang("បើសិនជាមានសមាជិកថ្មីនៅក្នុងបន្ទុក","បើសិនជាមានសមាជិកថ្មីនៅក្នុងបន្ទុក");?></label>
							<a data-backdrop="static" data-keyboard="false"  data-toggle="modal" data-target="#myModal" href="<?= site_url("application_forms2/add_home_visit_member/".$id); ?>" class="btn btn-danger"><?= lang("add_member") ?></a>
							<a data-backdrop="static" data-keyboard="false"  data-toggle="modal" data-target="#myModal" href="<?= site_url("application_forms2/add_home_visit_member_exiting/".$id); ?>" class="btn btn-info"><?= lang("add_member_exiting") ?></a>
						</div>
						<div class="form-group"> 
							<table class="table table-condensed table-bordered table-hover table-striped" width="100%">
							​
									<tr>
										<th class="text-center" rowspan="2" class="text-center" width='80px'>#</th>
										<th class="text-center" rowspan="2"><?= lang('full_name') ?></th> 
										<th class="text-center" rowspan="2"><?= lang('sex') ?></th>
										<th class="text-center" rowspan="2"><?= lang('relationship') ?></th>
										<th class="text-center" rowspan="2"><?= lang('occupation') ?></th>
										<th class="text-center" rowspan="2"><?= lang('education') ?></th>  
										<th class="text-center" rowspan="2"><?= lang('dob') ?> </th>							
										<th class="text-center" colspan="4"><?= lang('marital_status') ?> </th>							
									</tr> 					
									<tr>  
										<th class="text-center"><?= lang('ស្របច្បាប់') ?> </th>							
										<th class="text-center"><?= lang('មិនស្របច្បាប់') ?> </th>							
										<th class="text-center"><?= lang('លែងលះ') ?> </th>							
										<th class="text-center"><?= lang('ផ្សេងៗ') ?> </th>							
									</tr>​
								<tbody>
								<?php 
									$relationships = array();
									foreach($relationship as $relation){
										$relationships[$relation->id] = $relation->relationship_kh;
									} 								
									foreach($members as $key => $member){ 																
									?>
									<tr>
										<td class="text-center">
											<a href="<?= site_url("application_forms2/delete_home_visit_member/".$member->id) ?>" onclick="return confirm('Are you sure?');" class="btn btn-danger btn-xs">
												<i class="fa fa-trash"></i>
											</a>
											<a href="<?= site_url("application_forms2/edit_home_visit_member/".$member->id) ?>" class="btn btn-success btn-xs" data-backdrop="static" data-keyboard="false"  data-toggle="modal" data-target="#myModal">
												<i class="fa fa-edit"></i>
											</a>
										</td> 
										<td><?= $member->lastname_kh.' '.$member->firstname_kh ?></td> 
										<td class="text-center"><?= lang($member->gender) ?></td> 
										<td class="text-center"><?= $relationships[$member->relationship]; ?></td>
										<td class="text-center"><?= $member->occupation_kh; ?></td> 
										<td class="text-center"><?= $member->education_kh; ?></td> 
										<td class="text-center"><?= $this->erp->toKhmer($this->erp->hrsd($member->dob)) ?></td>
										<td class="text-center">
											<input type="checkbox" <?php echo $member->is_legal=="is_legal"?"checked":""?> name="is_legal" value="<?php echo $member->id;?>" > 
										</td>
										<td class="text-center">
											<input type="checkbox" <?php echo $member->is_legal=="is_not_legal"?"checked":""?>  name="is_not_legal" value="<?php echo $member->id;?>" > 
										</td>
										<td class="text-center">
											<input type="checkbox"  <?php echo $member->is_divorce=="is_divorce"?"checked":""?>  name="is_divorce" value="<?php echo $member->id;?>" > 
										</td>
										<td class="text-center">
											<div class="col-sm-8">
												<input id="other_<?php echo $member->id?>" type="text" name="other_<?php echo $member->id?>" onkeyup="myFunction(<?php echo $member->id?>)" value="<?php echo $member->other;?>" class="form-control" > 
											</div>
											<div class="col-sm-1" >
											    <button type="button" id="button_<?php echo $member->id?>" onclick="saveText(<?php echo $member->id?>)" class="btn btn-warning hidden"><?= lang("save");?></button>
											</div>
										</td>
									</tr>	
								<?php }?>										
								</tbody>
							</table> 
						</div>
					</div>
				</div> 
			</div>
		</div>
	</div>  
</div> 
<script type="text/javascript" src="<?= $assets ?>styles/helpers/components/js/bootstrap-formhelpers.js"></script>	
<link href="<?= $assets ?>styles/helpers/components/js/bootstrap-formhelpers.min.css" rel="stylesheet" />
<script type="text/javascript">
	function myFunction(num) {
		var x = document.getElementById("other_"+num);
		if(x.value != ""){
			$("#button_"+num).removeClass("hidden");
		}else{
			$("#button_"+num).addClass("hidden");
		}
	} 
	function saveText(n) {
		var id = n;
		var other = $('input[name="other_'+n+'"]').val();
		if(other != "") {
				$.ajax({
					type: 'post',
					url: '<?= site_url('application_forms2/update_status'); ?>'+"/"+id+"?q="+other, 
					data: { 
						func: 'update_status',
						'<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'
					}, 
					success: function (response) {
						bootbox.alert("<?php echo lang('success_change_status');?>", function(){ 
							window.location.href= " <?= site_url('application_forms2/edit_home_visit/'.$id); ?>"+"#content_4";
							location.reload();
						})
					},
					error:function (){
						alert("error");
					}
				});   
		}
		
	} 
	$(function(){
		$("input[name='is_legal']").on("ifChecked",function(){ 
			var id= $(this).val();
			if(id) { 
				$.ajax({
					type: 'post',
					url: '<?= site_url('application_forms2/update_status'); ?>'+"/"+id+"?status=is_legal", 
					data: { 
						func: 'update_status',
						'<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'
					}, 
					success: function (response) {
						bootbox.alert("<?php echo lang('success_change_status');?>", function(){ 
							window.location.href= " <?= site_url('application_forms2/edit_home_visit/'.$id); ?>"+"#content_4";
							location.reload();
						})
					},
					error:function (){
						alert("error");
					}
				});   
			}    
		});
		$("input[name='is_legal']").on("ifUnchecked",function(){ 
			var id= $(this).val();
			if(id) { 
				$.ajax({
					type: 'post',
					url: '<?= site_url('application_forms2/update_status'); ?>'+"/"+id+"?status=is_legal1", 
					data: { 
						func: 'update_status',
						'<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'
					}, 
					success: function (response) {
						bootbox.alert("<?php echo lang('success_change_status');?>", function(){ 
							window.location.href= " <?= site_url('application_forms2/edit_home_visit/'.$id); ?>"+"#content_4";
							location.reload();
						})
					},
					error:function (){
						alert("error");
					}
				});   
			}    
		});
		 
		$("input[name='is_not_legal']").on("ifChecked",function(){ 
			var id= $(this).val();
			if(id) { 
				$.ajax({
					type: 'post',
					url: '<?= site_url('application_forms2/update_status'); ?>'+"/"+id+"?status=is_not_legal", 
					data: { 
						func: 'update_status',
						'<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'
					}, 
					success: function (response) {
						bootbox.alert("<?php echo lang('success_change_status');?>", function(){ 
							window.location.href= " <?= site_url('application_forms2/edit_home_visit/'.$id); ?>"+"#content_4";
							location.reload();
						})
					},
					error:function (){
						alert("error");
					}
				});   
			}    
		});
		$("input[name='is_not_legal']").on("ifUnchecked",function(){ 
			var id= $(this).val();
			if(id) { 
				$.ajax({
					type: 'post',
					url: '<?= site_url('application_forms2/update_status'); ?>'+"/"+id+"?status=is_not_legal1", 
					data: { 
						func: 'update_status',
						'<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'
					}, 
					success: function (response) {
						bootbox.alert("<?php echo lang('success_change_status');?>", function(){ 
							window.location.href= " <?= site_url('application_forms2/edit_home_visit/'.$id); ?>"+"#content_4";
							location.reload();
						})
					},
					error:function (){
						alert("error");
					}
				});   
			}    
		});
		 
		 
		 
		 
		 $("input[name='is_divorce']").on("ifChecked",function(){ 
			var id= $(this).val();
			if(id) { 
				$.ajax({
					type: 'post',
					url: '<?= site_url('application_forms2/update_status'); ?>'+"/"+id+"?status=is_divorce", 
					data: { 
						func: 'update_status',
						'<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'
					}, 
					success: function (response) {
						bootbox.alert("<?php echo lang('success_change_status');?>", function(){ 
							window.location.href= " <?= site_url('application_forms2/edit_home_visit/'.$id); ?>"+"#content_4";
							location.reload();
						})
					},
					error:function (){
						alert("error");
					}
				});   
			}    
		});
		$("input[name='is_divorce']").on("ifUnchecked",function(){ 
			var id= $(this).val();
			if(id) { 
				$.ajax({
					type: 'post',
					url: '<?= site_url('application_forms2/update_status'); ?>'+"/"+id+"?status=is_not_divorce", 
					data: { 
						func: 'update_status',
						'<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'
					}, 
					success: function (response) {
						bootbox.alert("<?php echo lang('success_change_status');?>", function(){ 
							window.location.href= " <?= site_url('application_forms2/edit_home_visit/'.$id); ?>"+"#content_4";
							location.reload();
						})
					},
					error:function (){
						alert("error");
					}
				});   
			}    
		});
		 
		 
		 
		  
		 
		$("#case_no").autocomplete({
            source: function (request, response) {
                var q = request.term;
				$.ajax({
					type: 'get',
					url: '<?= site_url('applications/suggestions_caseno'); ?>',
					dataType: "json",
					data: {
						q : q
					},
					success: function (data) {
						response(data);
					}
				});
            },
            minLength: 1,
            autoFocus: false,
            delay: 200,
            response: function (event, ui) {
                if (ui.content[0].id == 0) {
                    $(this).removeClass('ui-autocomplete-loading');
                    $(this).removeClass('ui-autocomplete-loading');
                }
            },
            select: function (event, ui) {
                event.preventDefault();
                if (ui.item.id !== 0) {
                	var date = new Date(ui.item.row.dob);
					var dob = (date.getDate() + 1) + '/' + pad(date.getMonth()) + '/' +  date.getFullYear();
					
					$("input[name='dob'").val(dob); 
               	 	$("input[name='application_id'").val(ui.item.row.application_id);
                	$("input[name='nationality_kh'").val(ui.item.row.nationality_kh);                	
                	$("input[name='firstname'").val(ui.item.row.firstname_kh);
                	$("input[name='lastname'").val(ui.item.row.lastname_kh);
                	
				}
            }
        });
        
        
        getAllAutoComplete(".village, .village","/getAllTags?v=village");
		
		function getAllAutoComplete(element, base_url){
			$(element).autocomplete({
				source: function (request, response) {
					var q = request.term;
					$.ajax({
						type: 'GET',
						url: '<?= site_url('asylum_seekers/'); ?>'+base_url,
						dataType: "json",
						data: {
							q : q
						},
						success: function (data) {
							response(data);
						}
					});
				},
				minLength: 1,
				autoFocus: false,
				delay: 200,
				response: function (event, ui) {
					if (ui.content[0].id == 0) {
						$(this).removeClass('ui-autocomplete-loading');
						$(this).removeClass('ui-autocomplete-loading');
					}
				},
				select: function (event, ui) {
					event.preventDefault();
					$(this).val(ui.item.label);
				}
			});
		}
        
        function pad(str) {
		  str = str.toString();
		  return str.length < 2 ? pad("0" + str, 2) : str;
		}
		
	});	
// For check OR uncheck sponsor radio button
	if ($("#have_sponsorship_support").prop('checked')){
			$('[name="sponsor_hide"]').hide();
			$('[name="sponsorship_support_by"]').prop('disabled', true);
	} else if($("#no_have_sponsorship_support").prop('checked')){
		$('[name="sponsor_hide"]').show();
		$('[name="sponsorship_support_by"]').prop('disabled', false);
	}

	$('body').on('click','[name="sponsorship_support"]',function(e){
	  if( $(this).val() == 0 ){
	  	$('[name="sponsor_hide"]').hide();
	  	$('[name="sponsorship_support_by"]').prop('disabled', true);
	  	
	  }else{
	  	$('[name="sponsor_hide"]').show();
	  	$('[name="sponsorship_support_by"]').prop('disabled', false);

	  }
	});
// For check OR uncheck guarantee check radio button
	if ($("#have_insurance_service").prop('checked')){
		$('[name="hide_insurance_service"]').hide();
		$('[name="insurance_service_by"]').prop('disabled', true);
	} else if(($("#no_have_insurance_service").prop('checked'))){
		$('[name="hide_insurance_service"]').show();
		$('[name="insurance_service_by"]').prop('disabled', false);
	}
	
	$('body').on('click','[name="insurance_service"]',function(e){
	  if( $(this).val() == 0 ){
	  	$('[name="hide_insurance_service"]').hide();
	  	$('[name="insurance_service_by"]').prop('disabled', true);
	  }else{
	  	$('[name="hide_insurance_service"]').show();
	  	$('[name="insurance_service_by"]').prop('disabled', false);
	  }
	});
</script>