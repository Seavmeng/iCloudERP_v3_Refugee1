<ul id="myTab" class="nav nav-tabs">
	<li class=""><a href="#content_1" class="tab-grey"><?= lang("list_of_refugee"); ?></a></li>
</ul>
	<div class="tab-content">
		<div id="content_1" class="tab-pane fade in">
			
			<script>
				$(document).ready(function () {
					var oTable = $('#dataTable2').dataTable({
						"aaSorting": [[1, "asc"]],
						"aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?= lang('all') ?>"]],
						"iDisplayLength": <?= $Settings->rows_per_page ?>,
						'bProcessing': true, 'bServerSide': true,
						'sAjaxSource': "<?= site_url('application_forms2/getListOfRefugee/?v=1'.$v) ?>",
						'fnServerData': function (sSource, aoData, fnCallback) {
							aoData.push({
								"name": "<?= $this->security->get_csrf_token_name() ?>",
								"value": "<?= $this->security->get_csrf_hash() ?>"
							});
							$.ajax({
									'dataType': 'json', 
									'type': 'POST', 
									'url': sSource, 
									'data': aoData, 
									'success': fnCallback
									});
						},
						"aoColumns": [
						{"bSortable": false, "mRender": checkbox}, 			
						{"sClass" : "center"},
						{"sClass" : "center"},
						{"sClass" : "center"},	
						{"sClass" : "center", "mRender": fld},
						{"sClass" : "center"},
						{"sClass" : "center"},
						{"sClass" : "center"},
						{"sClass" : "center", "mRender" : fld},
						{"sClass" : "center", "mRender" : fld},
						{"mRender" : row_status},
						{"bSortable": false, "sClass" : "center"}]
						,'fnRowCallback': function (nRow, aData, iDisplayIndex) {
								var oSettings = oTable.fnSettings();
								var action = $('td:eq(11)', nRow);

								if (aData[10] == 'pending'){
									action.find('.home-visit').remove(); 
									action.find('.family-gurantee').remove(); 
								}

								return nRow;
							},
							"fnFooterCallback": function (nRow, aaData, iStart, iEnd, aiDisplay) {
								var total = 0, total_kh = 0;
								for (var i = 0; i < aaData.length; i++) 
								{
								}
								var nCells = nRow.getElementsByTagName('th');					
							}
						}).fnSetFilteringDelay().dtFilter([            				
							{column_number: 0, filter_default_label: "[<?= lang("#") ?>]", filter_type: "text", data: []},
							{column_number: 1, filter_default_label: "[<?= lang("id") ?>]", filter_type: "text", data: []},
							{column_number: 2, filter_default_label: "[<?= lang("firstname") ?>]", filter_type: "text", data: []},
							{column_number: 3, filter_default_label: "[<?= lang("lastname") ?>]", filter_type: "text", data: []},
							{column_number: 4, filter_default_label: "[<?= lang("dob") ?>]", filter_type: "text", data: []},								
							{column_number: 5, filter_default_label: "[<?= lang("gender") ?>]", filter_type: "text", data: []},
							{column_number: 6, filter_default_label: "[<?= lang("nationality") ?>]", filter_type: "text", data: []},
							{column_number: 7, filter_default_label: "[<?= lang("relationship") ?>]", filter_type: "text", data: []},
							{column_number: 8, filter_default_label: "[<?= lang("create_recognized_date") ?>]", filter_type: "text", data: []},				
							{column_number: 9, filter_default_label: "[<?= lang("recognized_date") ?>]", filter_type: "text", data: []},				
							{column_number: 10, filter_default_label: "[<?= lang("status") ?>]", filter_type: "text", data: []},				
						], "footer");
				});
			</script>
			<div class="box">
				<div class="box-content">
					<div class="row">
						<?= form_open_multipart("application_forms2/rsd_application"); ?>
						<div class="col-sm-12">
							<p class="introtext"><?= lang('list_results'); ?></p>
							<div class="form">				
								<div class="row">
									<div class="col-sm-2">
										<div class="form-group">
											<?php echo lang('case_no', 'case_no'); ?>
											<input name="case_no" class="form-control input-sm" type="text">
										</div>
									</div>
                                    <div class="col-sm-2">
										<div class="form-group">
											<?php echo lang('firstname', 'firstname'); ?>
											<input name="firstname" class="form-control input-sm" type="text">
										</div>
									</div>
                                    <div class="col-sm-2">
										<div class="form-group">
											<?php echo lang('lastname', 'lastname'); ?>
											<input name="lastname" class="form-control input-sm" type="text">
										</div>
									</div>
									<div class="col-sm-2">
										<div class="form-group">
											<?php echo lang('កាលបរិច្ឆេទ', 'កាលបរិច្ឆេទ'); ?>
											<input name="date_from" value="<?= date("d/m/Y") ?>" class="form-control input-sm" type="text">
										</div>
									</div>
									<div class="col-sm-2">
										<div class="form-group">
											<?php echo lang('កាលបរិច្ឆេទ', 'កាលបរិច្ឆេទ'); ?>
											<input name="date_to" value="<?= date("d/m/Y") ?>" class="form-control date input-sm" type="text">
										</div>
									</div>
									<div class="col-sm-12">
										<div class="form-group">
											<input type="submit" value="<?= lang("search") ?>" name="search" class="btn btn-default btn-info" />											
										</div>
									</div>
								</div>
							</div>				
							
							<div class="table-responsive">
								<table id="dataTable2" class="table table-condensed table-bordered table-hover table-striped">
									<thead>
										<tr>
											<th style="width:3%;"><?= lang("#") ?></th>
											<th><?= lang("case_no") ?></th>
											<th><?= lang("firstname") ?></th>
											<th><?= lang("lastname") ?></th>
											<th><?= lang("dob") ?></th>
											<th><?= lang("gender") ?></th>
											<th><?= lang("nationality") ?></th>
											<th><?= lang("relationship") ?></th>
											<th><?= lang("create_recognized_date") ?></th>
											<th><?= lang("recognized_date") ?></th>
											<th><?= lang("status") ?></th>
											<th><?= lang("action") ?></th>
										</tr>
									</thead>
									<tbody>
										
									</tbody>					
									<tfoot>
										<tr>
											<th></th>
											<th></th>
											<th></th>
											<th></th>
											<th></th>
											<th></th>											
											<th></th>
											<th></th>
											<th></th>
											<th></th>
											<th></th>
											<th></th>
										</tr>
									</tfoot>
								</table>
							</div>
							<?php
                            $results = $this->db->query("select * from erp_fa_interview_recognitions where erp_fa_interview_recognitions.status != 'deleted'")->result();
                            $c_no = array();
                            $t = 'សរុបចំនួន';
                            $k = 'ករណី';
                            foreach ($results as $row){
                                $c_no[$row->application_id] = $row->application_id;
                            }
                            echo $t." ".$this->erp->toKhmer(count($c_no))." ".$k;
                            ?>
						</div>
						<?= form_close(); ?>
					</div>
				</div>
				
			</div>
			
		</div>
</div>
<style type="text/css">
	.table{ 
		white-space: nowrap; 
		width:100%;
	}
</style>
<script language="javascript">
    $(document).ready(function () {
		$(".form").slideUp();
        $('.toggle_down').click(function () {
            $(".form").slideDown();
            return false;
        });
        $('.toggle_up').click(function () {
            $(".form").slideUp();
            return false;
        });
    });
</script>	
	
	