
<?php   
	$icon_check='<span style="font-size:17px;">&#x2611;</span>';
	$icon_uncheck='<span style="font-size:17px;">&#x2610;</span>'; 
?>
<div class="box a4" >
	<div class="box-content"> 		
			<?php $this->load->view($this->theme."applications/head-cover_1"); ?> 
			
			<div class='clearfix'></div> 
			
			<div class="col-sm-12 text-center">
				<h2 class='text-cover' style="margin-top:0px;">ពាក្យស្នើសុំធានាសមាជិកគ្រួសារ</h2>
				<h3>REQUEST FOR FAMILY REUNIFICATION</h3>
			</div>  
			
			<div class='wrap-content'> 
			
				<table width='100%' ​>
					<tr>
						<td width='20'>-</td>
						<td>
							 លេខករណី / Case Number:
							<?= $application->case_prefix.$application->case_no ?>
						</td> 
					</tr>
					<tr>
						<td>-</td>
						<td>
						ជាម្ចាស់ករណី / Principle Applicant: <?php echo ($row->member_id == 0 ? $icon_check : $icon_uncheck);?>
						&nbsp;&nbsp;
						ឬ ជាសមាជិកក្នុងបន្ទុក / Dependent Member: <?php echo ($row->member_id >= 1 ? $icon_check : $icon_uncheck);?>
						</td> 
					</tr> 
					<tr>
						<td >-</td>
						<td>
							នាមត្រកូល និងនាមខ្លួន / Surname and Given Name :
							<?php 
								if($member){ 
									echo $member->lastname_kh.' '.$member->firstname_kh." / ".strtoupper($member->lastname." ".$member->firstname);
								}else {
									echo $application->lastname_kh.' '.$application->firstname_kh." / ".strtoupper($application->lastname." ".$application->firstname);
								}
							?>
						</td>
					</tr>
					<tr>
						<?php 								
							if($member){ 
								$gender = $member->gender;
							}else {
								$gender = $application->gender;
							}
						?>
						<td>- </td>
						<td>​ភេទ/ Sex:							
							ប្រុស / Male: <?php echo($gender == 'male' ? $icon_check : $icon_uncheck);?>
							&nbsp;&nbsp;
							ស្រី / Female:<?php echo ($gender == 'female' ? $icon_check : $icon_uncheck); ?>																			
						</td>
					</tr> 
​​​​​​​​​​​​​​​​​​​​​					<tr>
						<td>-</td>
						<td>
							 ថ្ងៃខែឆ្នាំកំណើត / Date of  Birth : &nbsp;
							<?php
                            $sdate = explode("-", $member->dob);
                            $fdate = explode("-", $application->dob);

                            $s = $sdate[2]."-".$sdate[1]."-".$sdate[0];
                            $t = $fdate[2]."-".$fdate[1]."-".$fdate[0];
								if($member){ 
									echo $this->erp->toKhmer($s).' / '.$s;
								}else {
									echo $this->erp->toKhmer($t).' / '.$t;
								}
							?>		
						</td> 
					</tr> 
					<tr>
						<td>-</td>
						<td>
							ទីកន្លែងកំណើត / Place of Birth : 
							<?php 
								if($member){ 
									echo $member->pob_kh.' / '.$member->pob;
								}else {
									echo $application->pob_kh.' / '.$application->pob;
								}
							?>
						</td> 
					</tr>  
					<tr>
						<td>-</td>
						<td>សញ្ជាតិ / Nationality : &nbsp; 
							<?php 
								if($member){ 
									echo $member->nationality_kh." / ".$member->nationality;
								}else {
									echo $application->nationality_kh." / ".$application->nationality;
								}
							?>
						</td> 
						</tr>  
						<tr>
							<td >- </td>
							<td>​ថ្ងៃខែឆ្នាំទទួលឋានៈ / Date of Status Recognition: &nbsp;
                                 <?php
                                     $jdate = explode("-", $recognition->recognized_date);
                                     $j = $jdate[2]."-".$jdate[1]."-".$jdate[0];
                                 ?>
								 <?= ($recognition->recognized_date != NULL)?$this->erp->toKhmer($j).' / '.$j:''?>
							</td> 
						</tr>  
						<tr>
							<td>-</td>
							<td> 
								​មុខរបរបច្ចុប្បន្ន / Current Occupation: &nbsp; 
								<?= $row->occupation_kh  ?> / <?= $row->occupation  ?>  
							</td> 
						</tr>  
						<tr>
							<td>-</td>
							<td>
								​អាសយដ្ឋាននៅកម្ពុជា / Address in Cambodia:&nbsp;
								<?php //$row->address_kh.','?>
								 
								
								<?php 
									/*$village = $this->applications->getTagsKhmerById($row->village);
									echo !empty($village)?"ភៈ ".$village." , ":"";
									$commune = $this->applications->getAllAddressKH($row->commune,"commune"); 
									echo !empty($commune)?"ឃ.សៈ ".$commune:"";
									$district = $this->applications->getAllAddressKH($row->district,"district"); 
									echo !empty($district)?"ស្រ.ខៈ ".$district:"";
									$province = $this->applications->getAllAddressKH($row->province,"province"); 
									echo !empty($province)?"ខ.ក្រៈ ".$province:"";
									$country = $this->applications->getAllAddressKH($row->country,"country"); 
									echo !empty($country)?"ប្រៈ ".$country:"";
									echo " / ";
									echo $row->address.', ';
									$village1 = $this->applications->getTagsById($row->village);
									echo !empty($village1)?$village1." , ":"";
									$commune1 = $this->applications->getAllAddress($row->commune,"commune"); 
									echo !empty($commune1)?$commune1:"";
									$district1 = $this->applications->getAllAddress($row->district,"district"); 
									echo !empty($district1)?$district1:"";
									$province1 = $this->applications->getAllAddress($row->province,"province"); 
									echo !empty($province1)?$province1:"";
									$country1 = $this->applications->getAllAddress($row->country,"country"); 
									echo !empty($country1)?$country1:"";*/

                                $mem = $row->address_kh;
                                $village = $this->applications->getTagsKhmerById($row->village);
                                $commune = $this->applications->getAllAddressKH($row->commune,"commune_r");
                                $district = $this->applications->getAllAddressKH($row->district,"district_r");
                                $province = $this->applications->getAllAddressKH($row->province,"province_r");
                                $country = $this->applications->getAllAddressKH($row->country,"country");

                                if(!empty($country))
                                {
                                    echo !empty($mem)?$mem." , ":"";
                                    echo !empty($village)?"ភៈ ".$village." , ":"​​ ";
                                    echo !empty($commune)?"ឃ.សៈ ".$commune." , ":"​ ";
                                    echo !empty($district)?"ស្រ.ខៈ ".$district." , ":"​ ";
                                    echo !empty($province)?"ខ.ក្រៈ ".$province." , ":" ";
                                    echo !empty($country)?"ប្រៈ ".$country." ":" ";
                                }
                                elseif (!empty($province))
                                {
                                    echo !empty($mem)?$mem." , ":"";
                                    echo !empty($village)?"ភៈ ".$village." , ":"​​ ";
                                    echo !empty($commune)?"ឃ.សៈ ".$commune." , ":"​ ";
                                    echo !empty($district)?"ស្រ.ខៈ ".$district." , ":"​ ";
                                    echo !empty($province)?"ខ.ក្រៈ ".$province."  ":" ";
                                    echo !empty($country)?"ប្រៈ ".$country." ":" ";
                                }
                                elseif (!empty($district))
                                {
                                    echo !empty($mem)?$mem." , ":"";
                                    echo !empty($village)?"ភៈ ".$village." , ":"​​ ";
                                    echo !empty($commune)?"ឃ.សៈ ".$commune." , ":"​ ";
                                    echo !empty($district)?"ស្រ.ខៈ ".$district."  ":"​ ";
                                    echo !empty($province)?"ខ.ក្រៈ ".$province."  ":" ";
                                    echo !empty($country)?"ប្រៈ ".$country." ":" ";
                                }
                                elseif (!empty($commune))
                                {
                                    echo !empty($mem)?$mem." , ":"";
                                    echo !empty($village)?"ភៈ ".$village." , ":"​​ ";
                                    echo !empty($commune)?"ឃ.សៈ ".$commune."  ":"​ ";
                                    echo !empty($district)?"ស្រ.ខៈ ".$district."  ":"​ ";
                                    echo !empty($province)?"ខ.ក្រៈ ".$province."  ":" ";
                                    echo !empty($country)?"ប្រៈ ".$country." ":" ";
                                }
                                elseif (!empty($village))
                                {
                                    echo !empty($mem)?$mem." , ":"";
                                    echo !empty($village)?"ភៈ ".$village."  ":"​​ ";
                                    echo !empty($commune)?"ឃ.សៈ ".$commune."  ":"​ ";
                                    echo !empty($district)?"ស្រ.ខៈ ".$district."  ":"​ ";
                                    echo !empty($province)?"ខ.ក្រៈ ".$province."  ":" ";
                                    echo !empty($country)?"ប្រៈ ".$country." ":" ";
                                }
                                elseif (!empty($mem))
                                {
                                    echo !empty($mem)?$mem."  ":"";
                                }
                                else
                                {
                                    echo "";
                                }
                                echo " / ";

                                $mem1 = $row->address;
                                $village1 = $this->applications->getTagsById($row->village);
                                $commune1 = $this->applications->getAllAddress($row->commune,"commune_r");
                                $district1 = $this->applications->getAllAddress($row->district,"district_r");
                                $province1 = $this->applications->getAllAddress($row->province,"province_r");
                                $country1 = $this->applications->getAllAddress($row->country,"country");

                                if(!empty($country1))
                                {
                                    echo !empty($mem1)?$mem1." , ":"";
                                    echo !empty($village1)?$village1." , ":"​​ ";
                                    echo !empty($commune1)?$commune1." , ":"​ ";
                                    echo !empty($district1)?$district1." , ":"​ ";
                                    echo !empty($province1)?$province1." , ":" ";
                                    echo !empty($country1)?$country1." ":" ";
                                }
                                elseif (!empty($province1))
                                {
                                    echo !empty($mem1)?$mem1." , ":"";
                                    echo !empty($village1)?$village1." , ":"​​ ";
                                    echo !empty($commune1)?$commune1." , ":"​ ";
                                    echo !empty($district1)?$district1." , ":"​ ";
                                    echo !empty($province1)?$province1."  ":" ";
                                    echo !empty($country1)?$country1." ":" ";
                                }
                                elseif (!empty($district1))
                                {
                                    echo !empty($mem1)?$mem1." , ":"";
                                    echo !empty($village1)?$village1." , ":"​​ ";
                                    echo !empty($commune1)?$commune1." , ":"​ ";
                                    echo !empty($district1)?$district1."  ":"​ ";
                                    echo !empty($province1)?$province1."  ":" ";
                                    echo !empty($country1)?$country1." ":" ";
                                }
                                elseif (!empty($commune1))
                                {
                                    echo !empty($mem1)?$mem1." , ":"";
                                    echo !empty($village1)?$village1." , ":"​​ ";
                                    echo !empty($commune1)?$commune1." ":"​ ";
                                    echo !empty($district1)?$district1."  ":"​ ";
                                    echo !empty($province1)?$province1."  ":" ";
                                    echo !empty($country1)?$country1." ":" ";
                                }
                                elseif (!empty($village1))
                                {
                                    echo !empty($mem1)?$mem1." , ":"";
                                    echo !empty($village1)?$village1."  ":"​​ ";
                                    echo !empty($commune1)?$commune1." ":"​ ";
                                    echo !empty($district1)?$district1."  ":"​ ";
                                    echo !empty($province1)?$province1."  ":" ";
                                    echo !empty($country1)?$country1." ":" ";
                                }
                                elseif (!empty($mem1))
                                {
                                    echo !empty($mem1)?$mem1."  ":"";
                                }
                                else
                                {
                                    echo "";
                                }
                                ?>
							</td> 
						</tr>
						<tr>
							<td width="20">- </td>
							<td> លេខទូរស័ព្ទទំនាក់ទំនង / Contact Number: &nbsp;
								<?= $this->erp->toKhmer($row->contact_number).' / '.$row->contact_number ?> 
							</td> 
						</tr>  
				</table>
				
			<div class="col-sm-12 text-center ">
				<h2 class='text-cover'>ខ្ញុំសូមធ្វើការធានា</h2>
				<span>I WOULD LIKE TO GUARANTEE</span>   
			</div>
			
			<div class='clearfix'></div>
			<br/> 
			<table width='100%' class="relationship_table">
				<tr>
					<td>ឈ្មោះ<br/>Name</td>
					<td style="width:80px;" class='text-center'>ភេទ<br/>Sex</td>
					<td  class='text-center'>ថ្ងៃខែឆ្នាំកំណើត<br/> Date of Birth</td>
					<td  class='text-center'>សញ្ជាតិ<br/> Nationality</td>	
					<td  class='text-center'>សាសនា<br/>Religion</td>	
					<td  class='text-center'>ទំនាក់ទំនង<br/>Relationship</td>				 
				</tr>
				 
				 <?php  
				 $relationships = array();
				 foreach($relationship as $relation){
				 	$relationships[$relation->id] = $relation->relationship_kh;
				 	$relationships1[$relation->id] = $relation->relationship;
				 		
				 }
				 foreach($members as $key => $member){
				 ?>
					<tr> 
						<td>
							<?= $this->erp->toKhmer($key+1); ?>. 
							<?= $member->lastname_kh." ".$member->firstname_kh; ?> <br> <?= $member->lastname." ".$member->firstname; ?> 
						</td>
						<!-- <td class='center'><?= lang($member->gender) ?></td> -->
						<td class='center'>
							<?= ($member->gender == 'male')?'ប្រុស':'ស្រី' ?><br>
							<?= ucfirst($member->gender) ?>	
						</td>
						<td class='center'><?= $this->erp->toKhmer($this->erp->hrsd($member->dob))."<br>".$this->erp->hrsd($member->dob)?></td>
						<td class='center'><?= $member->nationality_kh."<br>".$member->nationality ?></td>
						<td class='center'><?= $member->religion_kh."<br>".$member->religion ?></td>
						<td class='center'><?= $relationships[$member->relationship].'<br>'.$relationships1[$member->relationship] ?></td>
					</tr>
					<tr> 
						<td colspan='6'>ទីកន្លែងកំណើត / Place of Birth:&nbsp;<?= $member->pob_kh.' / '.$member->pob ?> </td> 
					</tr> 
					<tr> 
						<td colspan='6'>មុខរបរបច្ចុប្បន្ន  / Current Occupation:&nbsp;<?= $member->occupation_kh.' / '.$member->occupation ?> </td> 
					</tr>
					<tr> 
						<td colspan='6'>អាសយដ្ឋាន / Address:&nbsp; 
							<?php //$member->address_kh.', ' ?>
							<?php /*
								$village = $this->applications->getTagsKhmerById($member->village);  
								echo !empty($village)?"ភៈ ".$village." , ":"";
								$commune = $this->applications->getAllAddressKH($member->commune,"commune"); 
								echo !empty($commune)?"ឃ.សៈ ".$commune:"";
								$district = $this->applications->getAllAddressKH($member->district,"district"); 
								echo !empty($district)?"ស្រ.ខៈ ".$district:"";
								$province = $this->applications->getAllAddressKH($member->province,"province"); 
								echo !empty($province)?"ខ.ក្រៈ ".$province:"";
								$state = $this->applications->getAllAddressKH($member->state,"state"); 
								echo !empty($state)?"រៈ ".$state.", ":"";
								$country = $this->applications->getAllAddressKH($member->country,"country"); 
								echo !empty($country)?"ប្រៈ ".$country:"";
								echo " / ";
								echo $member->address.', ';
								$village1 = $this->applications->getTagsById($member->village);  
								echo !empty($village1)?$village1." , ":"";
								$commune1 = $this->applications->getAllAddress($member->commune,"commune"); 
								echo !empty($commune1)?$commune1:"";
								$district1 = $this->applications->getAllAddress($member->district,"district"); 
								echo !empty($district1)?$district1:"";
								$province1 = $this->applications->getAllAddress($member->province,"province"); 
								echo !empty($province1)?$province1:"";
								$state1 = $this->applications->getAllAddress($member->state,"state"); 
								echo !empty($state1)?$state1.", ":"";
								$country1 = $this->applications->getAllAddress($member->country,"country"); 
								echo !empty($country1)?$country1:""; */
                            $mem = $member->address_kh;
                            $village = $this->applications->getTagsKhmerById($member->village);
                            $commune = $this->applications->getAllAddressKH($member->commune,"commune_r");
                            $district = $this->applications->getAllAddressKH($member->district,"district_r");
                            $province = $this->applications->getAllAddressKH($member->province,"province_r");
                            $state = $this->applications->getAllAddressKH($member->state,"state");
                            $country = $this->applications->getAllAddressKH($member->country,"country");

                            if(!empty($country))
                            {
                                echo !empty($mem)?$mem." , ":"";
                                echo !empty($village)?"ភៈ ".$village." , ":"​​ ";
                                echo !empty($commune)?"ឃ.សៈ ".$commune." , ":"​ ";
                                echo !empty($district)?"ស្រ.ខៈ ".$district." , ":"​ ";
                                echo !empty($province)?"ខ.ក្រៈ ".$province." , ":" ";
                                echo !empty($state)?"រៈ ".$state." , ":"";
                                echo !empty($country)?"ប្រៈ ".$country."  ":" ";
                            }
                            elseif(!empty($state))
                            {
                                echo !empty($mem)?$mem." , ":"";
                                echo !empty($village)?"ភៈ ".$village." , ":"​​ ";
                                echo !empty($commune)?"ឃ.សៈ ".$commune." , ":"​ ";
                                echo !empty($district)?"ស្រ.ខៈ ".$district." , ":"​ ";
                                echo !empty($province)?"ខ.ក្រៈ ".$province." , ":" ";
                                echo !empty($state)?"រៈ ".$state."  ":"";
                                echo !empty($country)?"ប្រៈ ".$country."  ":" ";
                            }
                            elseif (!empty($province))
                            {
                                echo !empty($mem)?$mem." , ":"";
                                echo !empty($village)?"ភៈ ".$village." , ":"​​ ";
                                echo !empty($commune)?"ឃ.សៈ ".$commune." , ":"​ ";
                                echo !empty($district)?"ស្រ.ខៈ ".$district." , ":"​ ";
                                echo !empty($province)?"ខ.ក្រៈ ".$province."  ":" ";
                                echo !empty($state)?"រៈ ".$state."  ":"";
                                echo !empty($country)?"ប្រៈ ".$country."  ":" ";
                            }
                            elseif (!empty($district))
                            {
                                echo !empty($mem)?$mem." , ":"";
                                echo !empty($village)?"ភៈ ".$village." , ":"​​ ";
                                echo !empty($commune)?"ឃ.សៈ ".$commune." , ":"​ ";
                                echo !empty($district)?"ស្រ.ខៈ ".$district."  ":"​ ";
                                echo !empty($province)?"ខ.ក្រៈ ".$province."  ":" ";
                                echo !empty($state)?"រៈ ".$state."  ":"";
                                echo !empty($country)?"ប្រៈ ".$country."  ":" ";
                            }
                            elseif (!empty($commune))
                            {
                                echo !empty($mem)?$mem." , ":"";
                                echo !empty($village)?"ភៈ ".$village." , ":"​​ ";
                                echo !empty($commune)?"ឃ.សៈ ".$commune."  ":"​ ";
                                echo !empty($district)?"ស្រ.ខៈ ".$district."  ":"​ ";
                                echo !empty($province)?"ខ.ក្រៈ ".$province."  ":" ";
                                echo !empty($state)?"រៈ ".$state."  ":"";
                                echo !empty($country)?"ប្រៈ ".$country."  ":" ";
                            }
                            elseif (!empty($village))
                            {
                                echo !empty($mem)?$mem." , ":"";
                                echo !empty($village)?"ភៈ ".$village."  ":"​​ ";
                                echo !empty($commune)?"ឃ.សៈ ".$commune."  ":"​ ";
                                echo !empty($district)?"ស្រ.ខៈ ".$district."  ":"​ ";
                                echo !empty($province)?"ខ.ក្រៈ ".$province."  ":" ";
                                echo !empty($state)?"រៈ ".$state."  ":"";
                                echo !empty($country)?"ប្រៈ ".$country."  ":" ";
                            }
                            elseif (!empty($mem))
                            {
                                echo !empty($mem)?$mem."  ":"";
                            }
                            else
                            {
                                echo "";
                            }
                            echo " / ";

                            $mem1 = $member->address;
                            $village1 = $this->applications->getTagsById($member->village);
                            $commune1 = $this->applications->getAllAddress($member->commune,"commune_r");
                            $district1 = $this->applications->getAllAddress($member->district,"district_r");
                            $province1 = $this->applications->getAllAddress($member->province,"province_r");
                            $state1 = $this->applications->getAllAddress($member->state,"state");
                            $country1 = $this->applications->getAllAddress($member->country,"country");

                            if(!empty($country1))
                            {
                                echo !empty($mem1)?$mem1." , ":"";
                                echo !empty($village1)?$village1." , ":"​​ ";
                                echo !empty($commune1)?$commune1." , ":"​ ";
                                echo !empty($district1)?$district1." , ":"​ ";
                                echo !empty($province1)?$province1." , ":" ";
                                echo !empty($state1)?$state1." , ":"";
                                echo !empty($country1)?$country1."  ":" ";
                            }
                            elseif (!empty($state1))
                            {
                                echo !empty($mem1)?$mem1." , ":"";
                                echo !empty($village1)?$village1." , ":"​​ ";
                                echo !empty($commune1)?$commune1." , ":"​ ";
                                echo !empty($district1)?$district1." , ":"​ ";
                                echo !empty($province1)?$province1." , ":" ";
                                echo !empty($state1)?$state1."  ":"";
                                echo !empty($country1)?$country1."  ":" ";
                            }
                            elseif (!empty($province1))
                            {
                                echo !empty($mem1)?$mem1." , ":"";
                                echo !empty($village1)?$village1." , ":"​​ ";
                                echo !empty($commune1)?$commune1." , ":"​ ";
                                echo !empty($district1)?$district1." , ":"​ ";
                                echo !empty($province1)?$province1."  ":" ";
                                echo !empty($state1)?$state1."  ":"";
                                echo !empty($country1)?$country1."  ":" ";
                            }
                            elseif (!empty($district1))
                            {
                                echo !empty($mem1)?$mem1." , ":"";
                                echo !empty($village1)?$village1." , ":"​​ ";
                                echo !empty($commune1)?$commune1." , ":"​ ";
                                echo !empty($district1)?$district1."  ":"​ ";
                                echo !empty($province1)?$province1."  ":" ";
                                echo !empty($state1)?$state1."  ":"";
                                echo !empty($country1)?$country1."  ":" ";
                            }
                            elseif (!empty($commune1))
                            {
                                echo !empty($mem1)?$mem1." , ":"";
                                echo !empty($village1)?$village1." , ":"​​ ";
                                echo !empty($commune1)?$commune1."  ":"​ ";
                                echo !empty($district1)?$district1."  ":"​ ";
                                echo !empty($province1)?$province1."  ":" ";
                                echo !empty($state1)?$state1."  ":"";
                                echo !empty($country1)?$country1."  ":" ";
                            }
                            elseif (!empty($village1))
                            {
                                echo !empty($mem1)?$mem1." , ":"";
                                echo !empty($village1)?$village1."  ":"​​ ";
                                echo !empty($commune1)?$commune1."  ":"​ ";
                                echo !empty($district1)?$district1."  ":"​ ";
                                echo !empty($province1)?$province1."  ":" ";
                                echo !empty($state1)?$state1."  ":"";
                                echo !empty($country1)?$country1."  ":" ";
                            }
                            elseif (!empty($mem1))
                            {
                                echo !empty($mem1)?$mem1."  ":"";
                            }
                            else
                            {
                                echo "";
                            }
							?> 
						</td> 
					</tr> 
				<?php } ?> 
			</table>
			<style type="text/css">
				.relationship_table td{
				/* 	white-space: nowrap; */
					border: 1px solid #000;
				}
				.relationship_table:last-child{
					border:none !important;
				}
				.break_page{
					page-break-after: always;
				}
			</style> 
			<!-- <?php  if($key == 2){?>
			<div class="break_page"></div>
			<?php }?>
			
			<?php  if($key == 1){?>
			<div class="break_page"></div>
			<?php }?>
			<?php  if($key == 0){?>
			<div class="break_page"></div>
			<?php }?> -->
			<table width='100%'> 
					<tr class="space-size">
						<td colspan="2">
							ដើម្បី​អោយ​មក​រួម​រស់​ជា​មួយខ្ញុំបាទ / នាងខ្ញុំនៅក្នុង​ព្រះរាជាណាចក្រកម្ពុជា​។ រាល់ព័ត៌​មាន​ខាង​លើ​ពិតជាច្បាស់លាស់ និង​ត្រឹមត្រូវ បើខុសពី​ការពិតខ្ញុំបាទ / នាង​ខ្ញុំ​សូម​ទទួល​ខុស​ត្រូវចំពោះ​មុខច្បាប់​។
							To live with me in Cambodia. The information provided above is true and accurate. If it is misrepresented or false, I will hold responsibility to the law.									
						</td>						
					</tr>
			</table>    
				<br/>  
			<div class='col-sm-12'>
				<div class='col-sm-6 tbl-left'>
					<div style="border:2px solid #999;">
						<table width='100%' class='text-center'>
							<tr>
								<td>
									<h4>សម្រាប់មន្រ្តី /  Officer Use Only</h4>
									<i>
									<?= $this->erp->footer_date_khmer("រាជធានីភ្នំពេញ");?>
									</i>
								</td>
							</tr>
						</table>
						<br/> 
						<br/> 
						<br/> 
						<br/>   
					</div>		
				</div> 
				<div class='col-sm-6 tbl-right'>
					<table width='100%' class="text-center">
						<tr>
							<td>
								<?= $this->erp->footer_date_khmer("រាជធានីភ្នំពេញ");?>
								<?= $this->erp->thumb_and_name()?>
							</td>
						</tr>
					</table>
				</div>
			</div>  
 
			<div class="clearfix"></div>
			<br/>
			<div class="attachment">			
				<center>
					<u>ភ្ជាប់មកជាមួយនូវឯកសារច្បាប់ដើម (បើមាន)</u>
					<u>
						<br/>Attach with original files.(If applicable)
					</u>
				</center>
                <br>
				<ul class="list " style="width:40%; float:left; margin-right: 20px;"  >
					<li>សំបុត្រកំណើត<br/>Birth Certificate</li>
					<li>​សំបុត្រអាពាហ៍ពិពាហ៍<br/> Marriage Certificate</li>
					<li>លិខិតបញ្ជាក់ទីលំនៅ​<br/> Resident Certificate</li>
					
				</ul> 
				<ul class="list" style="width:50%; float:right;margin:0 auto;"  >
					<li>លិខិតថ្កោលទោស<br/>Criminal Record</li>
					<li>លិខិតបញ្ជាក់សុខភាព<br/> Health Certificate</li>
					<li>លិខិតបញ្ជាក់ប្រាក់ចំណូលប្រចាំឆ្នាំអ្នកធានា<br/>Guarantor’s Annual Income Certificate</li>
				</ul> 
			</div> 
			<div class="clearfix"></div>  			
			<br/> 
		</div>
		
	</div>
	
</div> 
<style type="text/css"> 
	@media print{
		.bblack{ background: white !important; }   
		.modal-content, .box, 
		.modal-header{ border:none!important; }
		.table { border:none !important; }
		.box-content{
			padding: 20px !important;
		}
	}
	.wrap-content {
		min-height:800px;
		font-size:14px;
	}
	.wrap-content table td {
		padding:6px;
		vertical-align: top;
	}
	.attachment {
		width:30%; 
		font-size:9px; 
		padding:0px;
		margin-left: 30px;
	}
	.list{
		white-space:nowrap;
	}
</style>