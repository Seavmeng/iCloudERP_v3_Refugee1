<ul id="myTab" class="nav nav-tabs">
	<li class="">
		<a href="#content_1" class="tab-grey">
			 <?= lang('ផ្នែក ក.ព័ត៌មានបុគ្គល'); ?>​ 
			<i class="fa fa-chevron-right" aria-hidden="true"></i>
		</a>
	</li>
	<li class="">
		<a href="#content_2" class="tab-grey">
			<?= lang('ផ្នែក ខ.ស្ថានភាពរស់នៅ');?>
			<i class="fa fa-chevron-right" aria-hidden="true"></i>
		</a>
	</li> 
	<li class="">
		<a href="#content_3" class="tab-grey">
			<?= lang('ផ្នែក គ.ព័ត៌មានអាជ្ញាធរមូលដ្ឋាន');?>
			<i class="fa fa-chevron-right" aria-hidden="true"></i>
		</a>
	</li>
</ul>
<div class="tab-content">
	<?php 
		$countries_ = array(lang("select"));
		foreach($countries as $country){
			$countries_[$country->id] = $country->country;
		} 
		?>
		<?php 
		$provinces_ = array(lang("select"));
		foreach($provinces as $province){
			$provinces_[$province->id] = $province->name;
		} 
		?>
		<?php 
		$districts_ = array(lang("select"));
		foreach($districts as $district){
			$districts_[$district->id] = $district->name;
		} 
		?>
		<?php 
		$communes_ = array(lang("select"));
		foreach($communes as $commune){
			$communes_[$commune->id] = $commune->name;
		} 
	?>
	<div id="content_1" class="tab-pane fade in">
		<div class="box">
			<div class="box-content"> 
				
				<div class="row">
					
					<?php echo form_open("application_forms2/add_home_visit/".$id);  ?>
					
					<div class="col-sm-6">
						
						<div class="col-sm-12">
							<div class="form-group">
								<?php // json_encode($row);?>
								<?php echo lang('លេខករណីៈ', 'លេខករណីៈ'); ?>
								<span class="red">*</span>
								<input type="text" name="case_no" id="case_no" class="form-control input-sm" value="<?= set_value("case_no", $application->case_prefix.$application->case_no) ?>" />								
							</div>
							
							<div class="form-group">
								<?php echo lang('ឈ្មោះ', 'ឈ្មោះ'); ?>​ 
								<?php 
									$members_ = array($application->firstname_kh?$application->firstname_kh . " " . $application->lastname_kh." ".lang("( ម្ចាស់ករណី)"):"");
									foreach($default_members as $member){
										$members_[$member->id] = $member->firstname_kh . " " . $member->lastname_kh." ".lang("( សមាជិកក្នុងបន្ទុក)");
									}					
								?>
								<span class="red">*</span>
								<div class="control" id="members">
									<?php echo form_dropdown('member', $members_, $row->member_id, ' class="form-control" onchange="memberChange(this)" '); ?>
								</div>
							</div>
							
						</div>						
						
						<div class="clearfix"></div>
						
						<div class="col-sm-6">
							<div class="form-group">
								<?php echo lang('firstname', 'firstname'); ?>
								<small class="red">* (<?= lang('just_information'); ?>)</small>
								<input id="firstname" class="form-control input-sm" type="text" name="firstname" value="<?= set_value("firstname", $application->firstname_kh) ?>" />
							</div>
						</div> 
						<div class="col-sm-6">
							<div class="form-group">
								<?php echo lang('lastname', 'lastname'); ?>
								<small class="red">* (<?= lang('just_information'); ?>)</small>
								<input id="lastname" class="form-control input-sm" type="text" name="lastname" value="<?= set_value("lastname", $application->lastname_kh) ?>" />
							</div>
						</div>
						
						<div class="col-sm-6">
							<div class="form-group">
								<?php echo lang('ភេទ', 'ភេទ'); ?>
								<small class="red">* (<?= lang('just_information'); ?>)</small>
								<div class="controls">
									<label class="radio-inline">
										<input id="gender_male" type="radio" value="male" <?php echo ($application->gender=='male'?'checked':'') ?> name="gender" /> <?= lang('male'); ?>
									</label>
									<label class="radio-inline">
										<input id="gender_female" type="radio" value="female" <?php echo ($application->gender=='female'?'checked':'') ?> name="gender"/> <?= lang('female'); ?>
									</label> 
								</div>
							</div>
						</div>
						
						<div class="col-sm-6">
							<div class="form-group">
								<?php echo lang('ថ្ងៃខែឆ្នាំកំណើត', 'ថ្ងៃខែឆ្នាំកំណើត'); ?>
								<small class="red">* (<?= lang('just_information'); ?>)</small>
								<div class="controls">
									<input type="text" autocomplete="off" value="<?= set_value('dob', $this->erp->hrsd($application->dob)) ?>" name="dob" class="form-control input-sm date" />
								</div>
							</div>
						</div>
						
						<div class="col-sm-6">
							<div class="form-group">
								<?php echo lang('មុខរបរ ', 'មុខរបរ '); ?>
								<input class="form-control input-sm" value="<?= set_value('occupation_kh', $row->occupation_kh) ?>" type="text" name="occupation_kh" />
							</div>
						</div>
						
						<div class="col-sm-6">
							<div class="form-group">
								<?php echo lang('មុខរបរ ', 'មុខរបរ '); ?> (EN)
								<input class="form-control input-sm" value="<?= set_value('occupation', $row->occupation) ?>" type="text" name="occupation" />
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<?php echo lang('ការសិក្សាអប់រំ', 'ការសិក្សាអប់រំ '); ?>
								<input class="form-control input-sm" value="<?= set_value('education_kh', $row->education_kh) ?>" type="text" name="education_kh" />
							</div>							
						</div>
						
						<div class="col-sm-6">
							<div class="form-group">
								<?php echo lang('ការសិក្សាអប់រំ', 'ការសិក្សាអប់រំ '); ?> (EN)
								<input class="form-control input-sm" value="<?= set_value('education', $row->education) ?>" type="text" name="education" />
							</div>							
						</div>
						
						<div class="col-sm-12">
							<div class="form-group">
								<?php echo lang('លេខទូរស័ព្ទទំនាក់ទំនង   ', 'លេខទូរស័ព្ទទំនាក់ទំនង'); ?>
								<span class="red">*</span>
								<input class="form-control input-sm bfh-phone" value="<?= set_value('contact_number', $row->contact_number) ?>" data-format="+855 (ddd) ddd-dddd"  type="text" name="contact_number" />
							</div>							
						</div>
						 
						<div class="col-sm-12">
							<div class="form-group">
							<?php echo lang('មូលហេតុ ', 'មូលហេតុ'); ?> 						
							<span class="red">*</span>
							<div class="controls">
								<label class="radio-inline">
									<input type="radio" value="1" name="is_contact" <?= $row->is_contact==1?"checked":""?> class="skip"/> <?= lang('ទំនាក់ទំនង​បាន '); ?>
								</label>
								<label class="radio-inline">
									<input type="radio" value="0" name="is_contact" <?= $row->is_contact==0?"checked":""?> class="skip" /> <?= lang('ទំនាក់ទំនងមិន​បាន'); ?>
								</label> 
								</div>
							</div>
							 <textarea name="contact_reason" class=" form-control"><?= set_value('contact_reason', strip_tags($row->contact_reason)) ?></textarea>
						</div>   
						<div class="clearfix"></div>
						<br/>
						<div class="col-sm-12">	
							<div class="form-group">
								<input type="submit" class="btn btn-danger" name="submit" value="<?= lang("submit") ?>" />																			
							</div>						
						</div> 
					</div> 
					<div class="col-sm-6">
					
					<div class="panel panel-warning">
						
						<div class="panel-heading"><?php echo lang("address","address") ?></div>
						
						<div class="panel-body" style="padding: 5px;">
						
							<div class="col-lg-12">
								<div class="form-group">
									<?php echo lang('country', 'country'); ?> 
									<div class="controls">
										<?php echo form_dropdown('country', $countries_, set_value('country', $row->country), ' class="form-control country" '); ?>												
									</div>
								</div>
							</div>
							
							<div class="col-lg-6">
								<div class="form-group">
									<?php echo lang('province', 'province'); ?> 
									<div class="controls">
										<?php echo form_dropdown('province', $provinces_, set_value('province', $row->province), ' class="form-control province" '); ?>												
									</div>
								</div>
							</div>
							
							<div class="col-lg-6">
								<div class="form-group">
									<?php echo lang('district', 'district'); ?> 
									<div class="controls">
										<?php echo form_dropdown('district', $districts_, set_value('district', $row->district), ' class="form-control district" '); ?>												
									</div>
								</div>
							</div>
							
							<div class="col-lg-6">
								<div class="form-group">
									<?php echo lang('commune', 'commune'); ?> 
									<div class="controls">
										<?php echo form_dropdown('commune', $communes_, set_value('commune', $row->commune), ' class="form-control commune" '); ?>												
									</div>
								</div>
							</div>
							
							<div class="col-lg-6">
								<div class="form-group">
									<?php echo lang('village', 'village'); ?> 
									<div class="controls">
										<?php 
										echo form_input('village', set_value('village', $this->applications->getTagsById($row->village)),'class="form-control village" '); ?>											
									</div>
								</div>
							</div>
							
							<div class="col-lg-6 hidden">
								<div class="form-group">
									<?php echo lang('ក្រុម', 'ក្រុម'); ?>
									<span class="red">*</span>
									<div class="controls">
										<?php 
										echo form_input('group', set_value('group', $row->group),'class="form-control group_no" '); ?>											
									</div>
								</div>
							</div>
							
							<div class="col-lg-6 hidden">
								<div class="form-group">
									<?php echo lang('ផ្លូវ', 'ផ្លូវ'); ?>
									<span class="red">*</span>
									<div class="controls">
										<?php 
										echo form_input('street', set_value('street', $row->street),'class="form-control street_no" '); ?>											
									</div>
								</div>
							</div>
							
							<div class="col-lg-6 hidden">
								<div class="form-group">
									<?php echo lang(' ផ្ទះលេខ ', ' ផ្ទះលេខ '); ?>
									<span class="red">*</span>
									<div class="controls">
										<?php echo form_input('house', set_value('house', $row->house),'class="form-control house_no" '); ?>											
									</div>
								</div>
							</div>
							
							<div class="col-lg-12">
								<div class="form-group"> 
									<?php echo lang('other', 'other'); ?>​ (KH) 
									<div class="control">
										<input type="text" value="<?= set_value('address_kh', $row->address_kh) ?>" class="form-control input-sm" name="address_kh"  id="address_kh" />
									</div> 
								</div> 
							</div> 
							
							<div class="col-lg-12">
								<div class="form-group"> 
									<?php echo lang('other', 'other'); ?>​ (EN) 
									<div class="control">
										<input type="text" value="<?= set_value('address', $row->address) ?>" class="form-control input-sm" name="address"  id="address" />
									</div> 
								</div> 
							</div>  
						</div> 
					</div>
					
				</div>					
			
				</div>				
			
			</div>
		</div>
	</div>
	<div id="content_2" class="tab-pane fade in">
		<div class="box">
			<div class="box-content"> 
				
				<div class="row">
					
					<div class="col-lg-6">
						<div class="form-group">
							<?php echo lang('ជីវភាពគ្រួសារ', 'ជីវភាពគ្រួសារ'); ?>
							<span class="red">*</span>
							<div class="controls">
								<label class="radio-inline">
									<input type="radio" value="rich" <?= ($row->family_status=='rich'?'checked':'') ?> name="family_status" class="skip"/> <?= lang('មាន '); ?>
								</label>
								<label class="radio-inline">
									<input type="radio" value="medium" <?= ($row->family_status=='medium'?'checked':'') ?> name="family_status" class="skip" /> <?= lang('មធ្យម'); ?>
								</label> 
								<label class="radio-inline">
									<input type="radio" value="poor" <?= ($row->family_status=='poor'?'checked':'') ?>  name="family_status" class="skip" /> <?= lang('ក្រ'); ?>
								</label> 
							</div>
						</div>
					</div>
					
					<div class="col-lg-6">
						<div class="form-group"> 
							<?php echo lang('ស្ថានភាពសុខភាព', 'ស្ថានភាពសុខភាព'); ?>​ 
							<span class="red">*</span>
							<div class="control">
								<input type="text" name="health_status" value="<?= set_value('health_status', $row->health_status) ?>" class="form-control input-sm" />
							</div> 
						</div> 
					</div> 
					
					<div class="col-lg-6">
					
						<div class="panel panel-warning">
							
							<div class="panel-heading"><?php echo lang("ជំនួយឧបត្ថម្ភ","ជំនួយឧបត្ថម្ភ") ?></div>
							
							<div class="panel-body" style="padding: 5px;">
							
								<div class="col-lg-12">
									<div class="form-group">
										<div class="controls">
											<label class="radio-inline">
												<input type="radio" value="0" <?= ($row->sponsorship_support==0?'checked':'') ?> name="sponsorship_support" class="skip"> <?= lang('គ្មាន'); ?>
											</label>
											<label class="radio-inline">
												<input type="radio" value="1" <?= ($row->sponsorship_support==1?'checked':'') ?> name="sponsorship_support" class="skip"> <?= lang('មាន'); ?>
											</label>
										</div>
									</div>
									
										<div class="form-group">
											<?php echo lang('ជំនួយឧបត្ថម្ភ (ពី)', 'ជំនួយឧបត្ថម្ភ'); ?>
											<span class="red">*</span>
											<div class="controls">
												<input type="text" value="<?= set_value("sponsorship_support_by", $row->sponsorship_support_by) ?>" name="sponsorship_support_by" class="form-control input-sm">
											</div>
										</div>
								
										<div name="sponsor_hide">
										<div class="form-group">
											<?php echo lang('ប្រភេទជំនួយ', 'ប្រភេទជំនួយ'); ?>
											<span class="red">*</span>
											<div class="controls">
												<label class="checkbox-inline">
													<input type="checkbox" value="1" <?= ($row->sponsorship_support_money==1?'':'') ?> name="sponsorship_support_money[]" class="skip"> <?= lang('ជាថវិកា '); ?>
												</label>
												<label class="radio-inline">
													<input type="checkbox" value="0" <?= ($row->sponsorship_support_money==0?'':'') ?> name="sponsorship_support_money[]" class="skip"> <?= lang('ជាសម្ភារៈផ្សេងៗ '); ?>
												</label>
											</div>
										</div>
									
										<div class="form-group">
											<?php echo lang('បើសិន (​​ជាសម្ភារៈផ្សេងៗ)', 'ជំនួយឧបត្ថម្ភ'); ?>
											<span class="red">*</span>
											<div class="controls">
												<input type="text" value="<?= set_value("sponsorship_support_other", $row->sponsorship_support_other) ?>" name="sponsorship_support_other" class="form-control input-sm">
											</div>
										</div>
									</div>
								</div>
							
							</div>						
						
						</div>
						
						
						<div class="form-group">
							<?php echo lang("សំណូមពរផ្សេងៗ","សំណូមពរផ្សេងៗ"); ?>
							<div class="controls">
								<textarea name="suggestions" class="form-control skip"><?= set_value("suggestions", $row->suggestions) ?></textarea>
							</div>							
						</div>
						
					</div>
					
					
					
					<div class="col-lg-6">
					
						<div class="panel panel-warning">
							
							<div class="panel-heading"><?php echo lang("សេវាធានារ៉ាប់រង","សេវាធានារ៉ាប់រង") ?></div>
							
							<div class="panel-body" style="padding: 5px;">
								
								<div class="col-lg-12">
									<div class="form-group">
										<div class="controls">
											<label class="radio-inline">
												<input type="radio" value="0" <?= ($row->insurance_service==0?'checked':'') ?> name="insurance_service" class="skip"> <?= lang('គ្មាន'); ?>
											</label>
											<label class="radio-inline">
												<input type="radio" value="1" <?= ($row->insurance_service==1?'checked':'') ?> name="insurance_service" class="skip"> <?= lang('មាន'); ?>
											</label>
										</div>
									</div>
									
										<div class="form-group">
											<?php echo lang('សេវាធានារ៉ាប់រង (ពីអង្គការ)', 'សេវាធានារ៉ាប់រង'); ?>
											<span class="red">*</span>
											<div class="controls">
												<input type="text"​ value="<?= set_value("insurance_service_by", $row->insurance_service_by) ?>" name="insurance_service_by" class="form-control input-sm">
											</div>
										</div>
									<div name="hide_insurance_service">	
										<div class="form-group">
											<?php echo lang('លើផ្នែក', 'លើផ្នែក'); ?> 
											<div class="controls">
												<label class="checkbox-inline">
													<input type="checkbox" name="insurance_service_on[]" <?= ($row->insurance_service_on=='health'?'checked':'') ?> value="health" class="skip"> <?= lang('សុខភាព'); ?>
												</label>
												<label class="radio-inline">
													<input type="checkbox" name="insurance_service_on[]" <?= ($row->insurance_service_on=='life'?'checked':'') ?> value="life" class="skip"> <?= lang('អាយុជីវិត'); ?>
												</label>
												<label class="radio-inline">
													<input type="checkbox" name="insurance_service_on[]" <?= ($row->insurance_service_on=='other'?'checked':'') ?> value="other" class="skip"> <?= lang('ផ្សេងទៀត'); ?>
												</label>
											</div>
										</div>
										<div class="form-group">
											<?php echo lang('បើសិន (ផ្សេងទៀត)', 'ជំនួយឧបត្ថម្ភ'); ?>
											<span class="red">*</span>
											<div class="controls">
												<input type="text" value="<?= set_value("insurance_service_other", $row->insurance_service_other) ?>" name="insurance_service_other" class="form-control input-sm">
											</div>
										</div>
									</div>
								</div>
								
							</div>
							
						</div>
						
					</div>
				
				</div>
				
			</div>
		</div>
	</div>
	
	<div id="content_3" class="tab-pane fade in">
	
		<div class="box">
		
			<div class="box-content"> 
				
				<div class="row">
				
					<div class="col-lg-6">
					
						<div class="panel panel-warning">
							
							<div class="panel-heading"><?php echo lang("ព័ត៌មានអាជ្ញាធរមូលដ្ឋាន"," ព័ត៌មានអាជ្ញាធរមូលដ្ឋាន") ?></div>
							
							<div class="panel-body" style="padding: 5px;">
								
								<div class="col-lg-12">
								
									<div class="form-group">
										<?php echo lang('ឈ្មោះមេភូមិ / នាយប៉ុស្តិ៍នគរបាលរដ្ឋបាលៈ', 'ឈ្មោះមេភូមិ / នាយប៉ុស្តិ៍នគរបាលរដ្ឋបាលៈ'); ?>
										<span class="red">*</span>
										<div class="controls">
											<input type="text" value="<?= set_value("local_authorities_name", $row->local_authorities_name) ?>" name="local_authorities_name" class="form-control input-sm" />
										</div>
									</div>
									
									<div class="form-group">
										<?php echo lang('លេខទូរស័ព្ទទំនាក់ទំនងៈ', 'លេខទូរស័ព្ទទំនាក់ទំនងៈ'); ?>
										<span class="red">*</span>
										<div class="controls">
											<input type="text" value="<?= set_value("local_authorities_contact",$row->local_authorities_contact) ?>" name="local_authorities_contact" class="form-control input-sm" />
										</div>
									</div>
									
									<div class="form-group">
										<?php echo lang('មតិរបស់មេភូមិ / នាយប៉ុស្តិ៍នគរបាលរដ្ឋបាលៈ', 'មតិរបស់មេភូមិ / នាយប៉ុស្តិ៍នគរបាលរដ្ឋបាលៈ'); ?>
										<span class="red">*</span>
										<div class="controls">
											<label class="radio-inline">
												<input type="radio" name="local_authorities_opinion" <?= ($row->local_authorities_opinion==0?'checked':'') ?> value="0" class="skip"> <?= lang('មិនធ្លាប់មានពិរុទ្ធ  '); ?>
											</label>
											<label class="radio-inline">
												<input type="radio" name="local_authorities_opinion" <?= ($row->local_authorities_opinion==1?'checked':'') ?> value="1" class="skip"> <?= lang('ធ្លាប់មានពិរុទ្ធ '); ?>
											</label>
										</div>
									</div>
									
									<div class="form-group">
										<?php echo lang('បើ​សិន (ធ្លាប់មានពិរុទ្ធ)', 'បើ​ធ្លាប់មានពិរុទ្ធ'); ?>
										<span class="red">*</span>
										<div class="controls">
											<textarea name="local_authorities_comment" class="skip form-control" ><?= set_value("local_authorities_comment", $row->local_authorities_comment) ?></textarea>
										</div>
									</div>
									
								</div>
								
							</div>
							
						</div>
						
					</div>
				
				<div class="col-sm-6">
					<div class="form-group">
						<?php echo lang('មតិសម្រាប់មន្ត្រីចុះសួរសុខទុក្ខៈ', 'មតិសម្រាប់មន្ត្រីចុះសួរសុខទុក្ខៈ'); ?>
						<span class="red">*</span>
						<div class="controls">
							<textarea name="feedback_officials" class="skip form-control"><?= $row->feedback_officials ?></textarea>
						</div>
					</div>
				</div>
								
			</div>
			
		</div>
			
	</div>	
	<?php echo form_close(); ?> 
</div>  
</div> 
<script type="text/javascript" src="<?= $assets ?>styles/helpers/components/js/bootstrap-formhelpers.js"></script>	
<link href="<?= $assets ?>styles/helpers/components/js/bootstrap-formhelpers.min.css" rel="stylesheet" />
<script type="text/javascript" src="<?= $assets ?>styles/helpers/components/js/format_date.js"></script>
<script type="text/javascript">
	$(function(){
		$("#case_no").autocomplete({
            source: function (request, response) {
                var q = request.term;
				$.ajax({
					type: 'get',
					url: '<?= site_url('applications/suggestions_caseno_social_affair'); ?>',
					dataType: "json",
					data: {
						q : q,
						type : 'home_visit'
					},
					success: function (data) {
						response(data);
					}
				});
            },
            minLength: 1,
            autoFocus: false,
            delay: 200,
            response: function (event, ui) {
                if (ui.content[0].id == 0) {
                    $(this).removeClass('ui-autocomplete-loading');
                    $(this).removeClass('ui-autocomplete-loading');
                }
            },
            select: function (event, ui) {
                event.preventDefault();
                if (ui.item.id !== 0) {                	
                	location.href = "<?= site_url("application_forms2/add_home_visit/") ?>/"+ui.item.row.id;
				}
            }
        });
        
        getAllAutoComplete(".village, .village","/getAllTags?v=village");
		
		function getAllAutoComplete(element, base_url){
			$(element).autocomplete({
				source: function (request, response) {
					var q = request.term;
					$.ajax({
						type: 'GET',
						url: '<?= site_url('asylum_seekers/'); ?>'+base_url,
						dataType: "json",
						data: {
							q : q
						},
						success: function (data) {
							response(data);
						}
					});
				},
				minLength: 1,
				autoFocus: false,
				delay: 200,
				response: function (event, ui) {
					if (ui.content[0].id == 0) {
						$(this).removeClass('ui-autocomplete-loading');
						$(this).removeClass('ui-autocomplete-loading');
					}
				},
				select: function (event, ui) {
					event.preventDefault();
					$(this).val(ui.item.label);
				}
			});
		}
        
        function pad(str) {
		  str = str.toString();
		  return str.length < 2 ? pad("0" + str, 2) : str;
		}	
		
		/*** HIDE TEXTBOX ***/
		var sponsorship_support = '<?= $row->sponsorship_support ?>';
		if(sponsorship_support==0){
			$('[name="sponsor_hide"]').hide();
			$('[name="sponsorship_support_by"]').prop('disabled', true); 
		}
		var insurance_service = '<?= $row->insurance_service ?>';
		if(insurance_service==0){
			$('[name="hide_insurance_service"]').hide();
			$('[name="insurance_service_by"]').prop('disabled', true);
		}
		/*** END ***/
	});	
	function memberChange(e){

		var member_id = $(e).val();
		var id = "<?= $this->uri->segment(3)?>";
		$.ajax({ 
            type : "GET",
            url  : "<?= site_url()?>global_con/ajax?action=homeVisitMemberChange&id="+ id +"&member_id="+member_id,
            dataType: "json",
            success:function(data){ 
           		if (data.result!= 'false'){
           			var result = data.result;
					$('input[name=firstname]').val(result.firstname_kh);
					$('input[name=lastname]').val(result.lastname_kh);
					$("#gender_"+result.gender).attr('checked', 'checked');
					$('input[name=gender]').iCheck('update');
					var date = result.dob;
					if (date!='' && date!=null && date!='NULL'){
						try {
							date = Date.parse(date);
							date = date.toString("dd/MM/yyyy");
						} catch(err) {
						}
					}
					$('input[name=dob]').val(date);
					$('input[name=occupation_kh]').val(result.occupation_kh);
					$('input[name=occupation]').val(result.occupation);
					$('input[name=education_kh]').val(result.education_kh);
					$('input[name=education]').val(result.education);
					if(result.is_contact > 0){
						$('input[name=is_contact][value=1]').attr('checked', true); 
					}else{
						$('input[name=is_contact][value=0]').attr('checked', true); 
					}
					$('input[name=contact_reason]').val(result.education);	
					if(result.country > 0){
						$(".country").val(result.country).change();
					}else{
						$(".country").val(0).change();
					}
					if(result.province > 0){
						$(".province").val(result.province).change();
					}else{
						$(".province").val(0).change();
					}
					if(result.district > 0){
						$(".district").val(result.district).change();
					}else{
						$(".district").val(0).change();
					}
					if(result.commune > 0){
						$(".commune").val(result.commune).change();
					}else{
						$(".commune").val(0).change();
					}
					$('input[name=address_kh]').val(result.address_kh);
					$('input[name=address]').val(result.address);
					if(result.family_status !=null){
						$('input[name=family_status][value='+result.family_status+']').attr('checked',true);
					}else{
						$('input[name=family_status][value=medium]').attr('checked',true);
					}
					if(result.sponsorship_support >0){
						$('input[name=sponsorship_support][value='+result.sponsorship_support+']').attr('checked',true);
						$('[name="sponsor_hide"]').show();
						$('[name="sponsorship_support_by"]').prop('disabled', false); 
						$('input[name=sponsorship_support_by]').val(result.sponsorship_support_by);
					}else{
						$('input[name=sponsorship_support][value=0]').attr('checked',true);
						$('[name="sponsor_hide"]').hide();
						$('[name="sponsorship_support_by"]').prop('disabled', true);
					} 
					$('input[name=contact_reason]').val(result.suggestions);	
					$('input[name=health_status]').val(result.health_status); 
					
					if(result.insurance_service > 0){
						$('input[name=insurance_service][value='+result.insurance_service+']').attr('checked',true);
						$('[name="hide_insurance_service"]').show();
						$('[name="insurance_service_by"]').prop('disabled', false);
					}else{
						$('input[name=insurance_service][value=0]').attr('checked',true);
						$('[name="hide_insurance_service"]').hide();
						$('[name="insurance_service_by"]').prop('disabled', true);
					}
					$('input[name=insurance_service_on][value='+result.insurance_service_on+']').attr('checked',true);
					$('input[name=insurance_service_other]').val(result.insurance_service_other);	
					$('input[name=local_authorities_name]').val(result.local_authorities_name);	
					$('input[name=local_authorities_contact]').val(result.local_authorities_contact);	
					$('input[name=local_authorities_opinion][value='+result.local_authorities_opinion+']').attr('checked',true);
					$('input[name=local_authorities_comment]').val(result.local_authorities_comment);				
					$('input[name=feedback_officials]').val(result.feedback_officials);				
					
           		}
            }
        });
	}

	$('body').on('click','[name="sponsorship_support"]',function(e){
	  if( $(this).val() == 0 ){
	  	$('[name="sponsor_hide"]').hide();
	  	$('[name="sponsorship_support_by"]').prop('disabled', true);
	  }else{
	  	$('[name="sponsor_hide"]').show();
	  	$('[name="sponsorship_support_by"]').prop('disabled', false);
	  }
	});

	$('body').on('click','[name="insurance_service"]',function(e){
	  if( $(this).val() == 0 ){
	  	$('[name="hide_insurance_service"]').hide();
	  	$('[name="insurance_service_by"]').prop('disabled', true);
	  }else{
	  	$('[name="hide_insurance_service"]').show();
	  	$('[name="insurance_service_by"]').prop('disabled', false);
	  }
	});
</script>