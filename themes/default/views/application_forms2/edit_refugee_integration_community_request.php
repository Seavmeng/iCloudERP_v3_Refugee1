
<div class="modal-dialog modal-lg">
	<div class="modal-content">
		<div class="modal-header">				
			<button type="button" class="close" data-dismiss="modal">
				<span aria-hidden="true"><i class="fa fa-2x">&times;</i></span>
				<span class="sr-only"><?=lang('close');?></span>
			</button>
			<h4 class="modal-title" id="payModalLabel"><?= lang("edit_refugee_integration_community_request") ?></h4>
		</div>
		<?php  echo form_open_multipart("application_forms2/edit_refugee_integration_community_request/".$id); ?>
		<?php 
			$countries_ = array(lang("select"));
			foreach($countries as $country){
				$countries_[$country->id] = $country->country;
			} 
			?>
			<?php 
			$provinces_ = array(lang("select"));
			foreach($provinces as $province){
				$provinces_[$province->id] = $province->name;
			} 
			?>
			<?php 
			$districts_ = array(lang("select"));
			foreach($districts as $district){
				$districts_[$district->id] = $district->name;
			} 
			?>
			<?php 
			$communes_ = array(lang("select"));
			foreach($communes as $commune){
				$communes_[$commune->id] = $commune->name;
			} 
		?>
		<div class="modal-body"> 
			<div class='col-sm-9'>
				<div class="form-group">  
					<?php echo lang('case_no', 'case_no'); ?>​ 
					<span class="red">*</span>
					<div class="control">
						<input type="text" class="form-control input-sm case_no"  value="<?= $application->case_prefix.$application->case_no ?>" disabled>
						<input type="hidden" name="application_id" value="<?= $result->id;?>">
					</div>
				</div>   
				 
				
				 
				<div class="form-group">
					<?php echo lang('occupation', 'occupation'); ?>​ 
					<span class="red">*</span>
					<div class="control">
						<input type="text" class="form-control input-sm " name="occupation" placeholder="<?=lang('occupation')?>" value="<?= $result->occupation ?>"/>
					</div>
				</div> 
				
				<div class="clearfix"></div>
				
				<div class="panel panel-warning">
					<div class="panel-heading"><?php echo lang("address","address") ?></div>
					<div class="panel-body" style="padding: 5px;">
						<div class="col-lg-12">
							<div class="form-group">
								<?php echo lang('country', 'country'); ?>
								<div class="controls">
									<?php echo form_dropdown('country', $countries_, $result->country, ' class="form-control" '); ?>												
								</div>
							</div>
						</div>
						
						<div class="col-lg-6">
							<div class="form-group">
								<?php echo lang('province', 'province'); ?>
								<div class="controls">
									<?php echo form_dropdown('province', $provinces_, $result->province, ' class="form-control" '); ?>												
								</div>
							</div>
						</div>
						
						<div class="col-lg-6">
							<div class="form-group">
								<?php echo lang('district', 'district'); ?>
								<div class="controls">
									<?php echo form_dropdown('district', $districts_, $result->district, ' class="form-control" '); ?>												
								</div>
							</div>
						</div>
						
						<div class="col-lg-6">
							<div class="form-group">
								<?php echo lang('commune', 'commune'); ?>
								<div class="controls">
									<?php echo form_dropdown('commune', $communes_, $result->commune, ' class="form-control" '); ?>												
								</div>
							</div>
						</div>
						
						<div class="col-lg-6">
							<div class="form-group">
								<?php echo lang('village', 'village'); ?>
								<span class="red">*</span>
								<div class="controls">
									<?php 
									echo form_input('village', set_value('village', $this->applications->getTagsById($result->village)),'class="form-control village" '); ?>											
								</div>
							</div>
						</div>
						
						<div class="col-lg-12">
							<div class="form-group"> 
								<?php echo lang('other', 'other'); ?>​ (KH)
								<span class="red">*</span>
								<div class="control">
									<input type="text" value="<?= set_value("address_kh", $result->address_kh) ?>" class="form-control input-sm" name="address_kh"  id="address_kh" />
								</div> 
							</div> 
						</div> 
						
						<div class="col-lg-12">
							<div class="form-group"> 
								<?php echo lang('other', 'other'); ?>​  (EN)
								<span class="red">*</span>
								<div class="control">
									<input type="text" value="<?= set_value("address", $result->address) ?>" class="form-control input-sm" name="address"  id="address" />
								</div> 
							</div> 
						</div>
						 
					</div>
				</div> 
				
				<div class="form-group"> 
					<?php echo lang('contact_number', 'contact_number'); ?>​ 
					<span class="red">*</span>
					<div class="control">
						<input type="text" class="form-control input-sm bfh-phone" data-format="+855 (ddd) ddd-dddd" name="contact_number" value="<?= $result->contact_number ?>"/>
					</div> 
				</div>	  
				<div class="form-group">
					<?php echo lang('attachment','attachment'); ?>​ 
					<span class="red">
						<i class="fa fa-file-word-o" aria-hidden="true"></i>&nbsp;
						<i class="fa fa-file-pdf-o" aria-hidden="true"></i>
					</span>	
					<input accept=".pdf,.doc" type="file" name="doc" data-browse-label="<?= lang('document'); ?>" ​ data-show-upload="false" data-show-preview="false" accept="image/*" class="file" />	
				</div> 
			</div>
			
			<div class="col-md-3">
				<?= lang("photo", "photo"); ?>
				<?php
					$photo = "no_image.png";
					if(!empty($result->photo) || !file_exists("assets/uploads/".$result->photo)){
						$photo = $result->photo;
					}
				?>
				<div class="form-group">
					  <div class="main-img-preview">
						<img class="thumbnail img-preview" id='img_preview' src="<?= site_url("assets/uploads/".$photo); ?>" title="Preview Logo">
					  </div>
					  <div class="input-group">
						<input id="fakeUploadLogo" class="form-control input-sm fake-shadow" placeholder="<?= lang("choose_file") ?>" disabled="disabled">
						<div class="input-group-btn">
						  <div class="fileUpload btn btn-danger fake-shadow">
							<span><i class="fa fa-upload"></i>&nbsp;&nbsp;<?= lang("upload") ?></span>
							<input id="logo-id" accept=".gif,.jpg,.png" data-maxfile="1024" name="photo" type="file" class="attachment_upload">
						  </div>
						</div>
					  </div>
				</div>
			</div>
			
			<div class="clearfix"></div> 
		</div>
		
		<div class="modal-footer">
			<?php echo form_submit('edit_refugee_integration_community_request', lang('submit'), 'class="btn btn-primary"'); ?>
		</div>
		<?php  echo form_close(); ?>
	</div>
</div>

<style type="text/css"> 
	ul.ui-autocomplete {
	    z-index: 1100;
	}
/* File Upload */
	.fake-shadow {
		box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
	}
	.fileUpload {
		position: relative;
		overflow: hidden;
	}
	.fileUpload #logo-id {
		position: absolute;
		top: 0;
		right: 0;
		margin: 0;
		padding: 0;
		font-size: 33px;
		cursor: pointer;
		opacity: 0;
		filter: alpha(opacity=0);
	}
	.img-preview {
		max-width: 100%;
		width:200px !important;
	}
</style>
<script type="text/javascript">
	$(document).ready(function() {
		var brand = document.getElementById('logo-id');
		brand.className = 'attachment_upload';
		brand.onchange = function() {
			document.getElementById('fakeUploadLogo').value = this.value.substring(12);
		};
		// Source: http://stackoverflow.com/a/4459419/6396981
		function readURL(input) {
			if (input.files && input.files[0]) {
				var reader = new FileReader();
				
				reader.onload = function(e) {
					$('.img-preview').attr('src', e.target.result);
				};
				reader.readAsDataURL(input.files[0]);
			}
		}
		$("#logo-id").change(function() {
			readURL(this);
		});
		
		$("#upload").click(function(){
			var filename = "<?= site_url()?>/" + $("#webcam-preview img").attr("src");
			$('.img-preview').attr('src', filename);
			return false;
		});
		
	});
</script>
<script type="text/javascript" src="<?= $assets ?>styles/helpers/components/js/bootstrap-formhelpers.js"></script>	
<link href="<?= $assets ?>styles/helpers/components/js/bootstrap-formhelpers.min.css" rel="stylesheet" />
<?= $modal_js ?>
<script type="text/javascript">
	
	$(function(){
		
		getAllAutoComplete(".village, .village","/getAllTags?v=village");
		
		function getAllAutoComplete(element, base_url){
			$(element).autocomplete({
				source: function (request, response) {
					var q = request.term;
					$.ajax({
						type: 'GET',
						url: '<?= site_url('asylum_seekers/'); ?>'+base_url,
						dataType: "json",
						data: {
							q : q
						},
						success: function (data) {
							response(data);
						}
					});
				},
				minLength: 1,
				autoFocus: false,
				delay: 200,
				response: function (event, ui) {
					if (ui.content[0].id == 0) {
						$(this).removeClass('ui-autocomplete-loading');
						$(this).removeClass('ui-autocomplete-loading');
					}
				},
				select: function (event, ui) {
					event.preventDefault();
					$(this).val(ui.item.label);
				}
			});
		}
			
	});
	
</script>