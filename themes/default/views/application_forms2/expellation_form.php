<?php 
	echo $this->output->enable_profiler(TRUE);
?>
					<div class="content-cover a4">
					
							<img src="<?= base_url("assets/bg33.jpg") ?>" class="hidden-bg" />
							
							<div class="cover-in">
							
								<div class="content-header" style="height: 200px;">
								
								</div>
								
								<div class="cover-left"></div>
								
								<div class="text-center cover-center" >
									<h3><?= lang("ប្រកាស") ?></h3>
									<h3><?= lang("ស្តីពី") ?></h3>
									<h3><?= lang("ការបណ្តេញចេញជនភៀសខ្លូន") ?></h3>
									<h3><span class='cover-line'>5</span></h3>
									<h3><?= lang("ឧបនាយករដ្ឋមន្រ្តី រដ្ឋមន្រ្តីក្រសួងមហាផ្ទៃ") ?></h3>
								</div> 
								<div class="cover-right">
									<div class="cover-photo">
										<img src="<?= base_url("assets/uploads/".$result->photo) ?>" height="120" />
									</div>
								</div>
								
								<div style="clear:both;"></div>
								  
								<div style="clear:both;"></div>
								
								<style type="text/css">
									.cover-photo{
										margin-top:30px;
										margin-left:20px;
										width:120px;
										height:120px;
									}
									.cover-center{
										float:left; 
										width:50%;
									}
									.cover-left{
										float:left; 
										width:25%; 
										height:10px;
									}
									.cover-right{
										float:right; 
										width:25%; 
									}
								</style> 
								<div class="content-body recognition-list text-left"  style=" text-align: justify;text-justify: inter-word;"> 
								  <?php echo $recognition->description ?>
								</div>
								
								<div class="text-center">
									<h3><u><?= lang("សម្រេច") ?></u></h3>
								</div>  
								<div >
								
								
									<table width="100%" class="text-left" >
										<tr>
											<td width="60" class="text-cover">
												ប្រការ<span class='khmer-number'>១ </span>៖
											</td>
											<td>
												បណ្តេញឈ្មោះៈ  &nbsp; 
												<span class='bold'> <?= $result->lastname_kh ." ".$result->firstname_kh." / ".$result->lastname ." ".$result->firstname ?></span> 
											</td>
											<td>
												ភេទៈ&nbsp; 
												 <span class='bold'> <?= lang($result->gender) ?>   </span>
											</td>
											<td>
												ថ្ងៃខែឆ្នាំកំណើតៈ
												<span class='bold'> &nbsp;<?= $this->erp->khmer_date_format($result->dob); ?> </span>
											</td>
										</tr>
										<tr>
											<td width="60"></td>
											<td colspan='2'>
												សញ្ជាតិៈ&nbsp;
												<span class='bold'> <?= $result->nationality_kh; ?>  </span>
												&nbsp;
												&nbsp;
												&nbsp;
												&nbsp;
												&nbsp;
												ជាតិពន្ធុៈ&nbsp;
												<span class='bold'> <?= $result->ethnicity_kh; ?>   </span>
												&nbsp;
												&nbsp;
												&nbsp;
												&nbsp;
												&nbsp;
												ករណីលេខៈ&nbsp;
												<span class='bold'> <?= $this->erp->toKhmer($result->case_prefix." ".$result->case_no); ?>  </span>
											</td>
											<td width="60">
												ថ្ងៃខែឆ្នាំចុះឈ្មោះៈ&nbsp;
												<span class='bold'> <?=  $this->erp->khmer_date_format($result->registered_date); ?>  </span>
											</td>
										</tr> 
										<tr>
											<td width="60"></td>
											<td colspan='4' >
												ទីកន្លែងកំណើតៈ&nbsp;<span class='bold'>  <?= $result->pob_kh; ?> </span> 
											</td>
										</tr>
									</table> 
									<table width="100%">
									<?php if(!empty($member)){?>
										<tr>
											<td width="60"></td>
											<td colspan='4'>
											និងកូនក្នុងបន្ទុកចំនួន <b> <?php echo ((count($member)<10)?$this->erp->toKhmer('0'):'').$this->erp->toKhmer(count($member));?> </b>នាក់ ៖
											</td>  
										</tr> 
									<?php }?>
									
									<?php  foreach($member as $key => $row) { ?> 
										<tr>   
											<td width="60"></td>
											<td width="20"><?= $this->erp->toKhmer($key+1)?>.</td>
											<td class="bold" width='285'><?= $row->lastname_kh.' '.$row->firstname_kh." / ".$row->lastname.' '.$row->firstname?></td>
											<td  width='85'>ភេទៈ&nbsp; <span class='bold'><?= lang($row->gender)?></span></td>
											<td>ថ្ងៃខែឆ្នាំកំណើតៈ&nbsp;<span class='bold'> <?=  $this->erp->khmer_date_format($row->dob)?></span></td>
										</tr>  
										<?php }?>  
									</table>
									<table width="100%">
										<tr>
											<td width="60"></td>
											<td class="left bold"> ចេញពីព្រះរាជាណាចក្រកម្ពុជា ។</td>
										</tr>
									</table> 
								 
									<table width="100%">
										<tr>
											<td width="60" class="text-cover">
												ប្រការ<span class='khmer-number'>២ </span>៖
											</td>
											<td>
												ជនភៀសខ្លួនដូចមានក្នុងប្រការ១ ខាងលើមានកាតព្វកិច្ចគោរព និងអនុវត្តច្បាប់របស់ព្រះរាជាណាចក្រកម្ពុជាឱ្យបានមឺុងម៉ាត់ ។
											</td>
										<tr>
									</table>
									<table width="100%">
										<tr>
											<td width="60" class="text-cover">
												ប្រការ<span class='khmer-number'>៣ </span>៖
											</td>
											<td>
												ស្នងការរាជធានី-ខេត្តនិងស្ថាប័នដែលមានការពាក់ព័ន្ធជាមួយសាមុីខ្លួន ត្រូវអនុវត្តប្រកាសនេះចាប់ពីថ្ងៃចុះហត្ថលេខាតទៅ។
											</td>
										<tr>
									</table>
								</div>
								<div class="content-footer​" style="float:right; margin-right:30px;">
									<table width="100%">
										<tr>
											<td class='text-center' style="margin-top: 3px;">
											<?= $this->erp->footer_date_khmer('រាជធានីភ្នំពេញ');?> 
											</td>
										</tr>
										<tr>
											<td>
												<center> 
													ឧបនាយករដ្ឋមន្រ្តី រដ្ឋមន្រ្តីក្រសួងមហាផ្ទៃ
												</center>
											</td>
										</tr>
									</table>
								</div>
								
							</div>
							
					</div>
			

<style type="text/css" media="all">
	@media print{
		.bblack{ background: white !important; }
		.modal-content, .box, 
		.modal-header{ border:none !important; }
		.table { border:1px solid #000 !important; }
		.hidden-bg{
			display:block !important;
		}
	}
	.hidden-bg{
		z-index:-9999;
		position:absolute;
		display:none;
	}
	.content-cover span, p {
		padding: 0;
		margin: 0;
		font-size:12px;	
	}
	.content-cover center{
		font-size:12px;
		font-family: 'Moul', sans-serif !important;
	}
	.content-cover table td{
		font-size:12px;	
		line-height:26px;
		white-space: nowrap;
	}
	.content-cover{
		width:800px;
		height:1200px;
		background:url("assets/bg33.jpg");
		background-size: 800px auto;
		background-repeat: no-repeat;
		color: blue;
		position:relative;
				
	}
	.cover-in{
		margin:0 70px;
		width:670px;
		position:absolute;
		white-space: wrap;
		padding:20px;
	}	
	ul{
		list-style: none;
		text-align:justify;
		padding: 0;
		margin: 0;
	}
	.recognition-list >ul li:before {
	  content: '-';
	  position: absolute;
	  margin-left: -10px;
	}  
	h3 {
		line-height: 5px;
	}
	.content-cover h3{ 
		font-family: 'Moul', sans-serif !important;
	}
	hr{
		padding:0px;
		margin:0px;
		border-top:none !important;
		border-bottom:dotted 1px !important;
	}
	@font-face {
		font-family: "TACTENG Font";
		src: url(themes/default/assets/fonts/TACTENG.TTF) format("truetype");
	} 
	span.cover-line { 
		font-size:30px;
		padding:0px;
		font-family: "TACTENG Font";
	}  
	.content-body, table, .content-body li{ 
		line-height:19px;
		font-size: 12px !important; 
	}
	@font-face {
			font-family: "Khmer Moul";
			src: url(themes/default/assets/fonts/KhmerMoul.TTF) format("truetype");
	}  
	span.khmer-number {
		font-family:'Khmer Moul'; 
	}
	table {
		text-align: justify;
		text-justify: inter-word;
		margin-left: -10px;
	}
</style>