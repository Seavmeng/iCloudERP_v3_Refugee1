<ul id="myTab" class="nav nav-tabs">
	<li class="c1">
		<a href="#content_1" class="tab-grey">
			 <?= lang('ផ្នែក ក.ព័ត៌មានជនភៀសខ្លួន'); ?>​ 
			<i class="fa fa-chevron-right" aria-hidden="true"></i>
		</a>
	</li>
	<li class="c2">
		<a href="#content_2" class="tab-grey">
			<?= lang('ផ្នែក ខ.ព័ត៌មានសូមធ្វើការធានាលើសមាជិកគ្រួសារ');?>
			<i class="fa fa-chevron-right" aria-hidden="true"></i>
		</a>
	</li> 
</ul>  
<div class="tab-content">

	<?php 
		$countries_ = array(lang("select"));
		foreach($countries as $country){
			$countries_[$country->id] = $country->country;
		} 
		?>
		<?php 
		$provinces_ = array(lang("select"));
		foreach($provinces as $province){
			$provinces_[$province->id] = $province->name;
		} 
		?>
		<?php 
		$districts_ = array(lang("select"));
		foreach($districts as $district){
			$districts_[$district->id] = $district->name;
		} 
		?>
		<?php 
		$communes_ = array(lang("select"));
		foreach($communes as $commune){
			$communes_[$commune->id] = $commune->name;
		} 
	?>
	
	<div id="content_1" class="tab-pane fade in">
		<div class="box">
			<div class="box-content"> 
				<?php  echo form_open_multipart("application_forms2/add_family_guarantee_request/".$id); ?>	
				
				<div class="row"> 
				
					 <div class="col-sm-6">
					   
					    <div class="col-sm-12">
							<div class="form-group">
								<?php echo lang('case_no', 'case_no'); ?>​ 
								<span class="red">*</span>
								<div class="control">
									<input type="text" value="<?= set_value('case_no');?>" name='case_no' class="form-control input-sm case_no" id="case_no" >
									<input type="hidden" name="application_id" id="application_id" value="<?= set_value('application_id');?>">
								</div>
							</div>
							
							<div class="form-group">
								<?php echo lang('ឈ្មោះ', 'ឈ្មោះ'); ?>​ 
								<span class="red">*</span>
								<div class="control" id="members">
									<select class="form-control"></select>
								</div>
							</div>
						
						</div>
						
						<div class="clearfix"></div>
						
						<div class="col-sm-6">
							<div class="form-group">
								<?php echo lang('firstname', 'firstname'); ?>
								<small class="red">* (<?= lang('just_information'); ?>)</small>
								<input class="form-control input-sm" type="text" name="firstname" value="<?= set_value("firstname") ?>" />
							</div>
						</div>
						
						<div class="col-sm-6">
							<div class="form-group">
								<?php echo lang('lastname', 'lastname'); ?>
								<small class="red">* (<?= lang('just_information'); ?>)</small>
								<input class="form-control input-sm" type="text" name="lastname" value="<?= set_value("lastname") ?>" />
							</div>
						</div>
						
						<div class="col-sm-6">
							<div class="form-group">
								<?php echo lang('ភេទ', 'ភេទ'); ?>
								<small class="red">* (<?= lang('just_information'); ?>)</small>
								<div class="controls">
									<label class="radio-inline">
										<input type="radio" value="male"  name="gender"  id="male"> <?= lang('male'); ?>
									</label>
									<label class="radio-inline">
										<input type="radio" value="female" name="gender"  id="female"> <?= lang('female'); ?>
									</label> 
								</div>
							</div>
						</div>
						
						
						<div class="col-sm-6">
							<div class="form-group">
								<?php echo lang('ថ្ងៃខែឆ្នាំកំណើត', 'ថ្ងៃខែឆ្នាំកំណើត'); ?>
								<small class="red">* (<?= lang('just_information'); ?>)</small>
								<div class="controls">
									<input type="text" value="<?= set_value('dob') ?>" name="dob" class="form-control input-sm date" />
								</div>
							</div>
						</div>
						
						<div class="col-sm-6">
							<div class="form-group">
								<?php echo lang('មុខរបរបច្ចុប្បន្ន', 'មុខរបរបច្ចុប្បន្ន'); ?>
								<span class="red">*</span>								
								<div class="controls">
									<input type="text" value="<?= set_value('occupation_kh') ?>" name="occupation_kh" class="form-control input-sm" />
								</div>
							</div>
						</div>
						
						
						<div class="col-sm-6">
							<div class="form-group">
								<?php echo lang('មុខរបរបច្ចុប្បន្ន', 'មុខរបរបច្ចុប្បន្ន'); ?>​ (EN)
								<span class="red">*</span>
								<div class="control">
									<input type="text" value="<?= set_value('occupation') ?>" class="form-control input-sm "   name="occupation" id="current_occupation" />
								</div>
							</div>
						</div>
						
						<div class="col-sm-12">
							<div class="form-group">
								<?php echo lang('លេខទូរស័ព្ទទំនាក់ទំនង', 'លេខទូរស័ព្ទទំនាក់ទំនង'); ?>​  
								<div class="control">
									<input type="text" value="<?= set_value('contact_number') ?>"  class="form-control input-sm input-medium bfh-phone" data-format="+855 (ddd) ddd-dddd" name="contact_number"  id="contact_number" />
								</div>
							</div> 
						</div> 
						
						<div class="col-sm-12">
							<div class="form-group">
								<button type="submit" class="btn btn-success ">
									<?= lang("submit"); ?> 
								</button>
							</div>
						</div>
						
					</div>  
					
					
					<div class="col-sm-6">
					
						<div class="panel panel-warning">
							
							<div class="panel-heading a"><?php echo lang("address","address") ?></div>
							<div class="panel-heading hide b"><?php echo lang("address_before","address_before") ?></div>
							<style type="text/css">.show {display: block;}.hide {display: none;}
							</style>
							
							<div class="panel-body" style="padding: 5px;">
							
								<div class="col-lg-12">
									<div class="form-group">
										<?php echo lang('country', 'country'); ?> 
										<div class="controls">
											<?php echo form_dropdown('country', $countries_, set_value('country'), ' class="form-control" '); ?>												
										</div>
									</div>
								</div>
								
								<div class="col-lg-6">
									<div class="form-group">
										<?php echo lang('province', 'province'); ?> 
										<div class="controls">
											<?php echo form_dropdown('province', $provinces_, set_value('province'), ' class="form-control" '); ?>												
										</div>
									</div>
								</div>
								
								<div class="col-lg-6">
									<div class="form-group">
										<?php echo lang('district', 'district'); ?> 
										<div class="controls">
											<?php echo form_dropdown('district', $districts_, set_value('district'), ' class="form-control" '); ?>												
										</div>
									</div>
								</div>
								
								<div class="col-lg-12">
									<div class="form-group">
										<?php echo lang('commune', 'commune'); ?>  
										<div class="controls">
											<?php echo form_dropdown('commune', $communes_, set_value('commune'), ' class="form-control" '); ?>												
										</div>
									</div>
								</div>
								
								<div class="col-lg-6">
									<div class="form-group">
										<?php echo lang('village', 'village')." (KH)"; ?> 
										<div class="controls">
											<?php 
											echo form_input('village_kh', set_value('village_kh'),'class="form-control village_kh" '); ?>											
										</div>
									</div>
								</div>
								
								<div class="col-lg-6">
									<div class="form-group">
										<?php echo lang('village', 'village')." (EN)"; ?> 
										<div class="controls">
											<?php 
											echo form_input('village', set_value('village'),'class="form-control village" '); ?>											
										</div>
									</div>
								</div>
								
								<div class="col-lg-12">
									<div class="form-group"> 
										<?php echo lang('other', 'other'); ?>​ (KH) 
										<div class="control">
											<input type="text" value="" class="form-control input-sm" name="address_kh"  id="address_kh" />
										</div> 
									</div> 
								</div> 
								
								<div class="col-lg-12">
									<div class="form-group"> 
										<?php echo lang('other', 'other'); ?>​ (EN) 
										<div class="control">
											<input type="text" value="" class="form-control input-sm" name="address"  id="address" />
										</div> 
									</div> 
								</div> 
							</div>
						</div> 
					</div>	 
				</div>
				<?php echo form_close();?>
			</div>
		</div>
	</div>
	
	<?php echo form_close(); ?> 
</div>
</div> 
<script type="text/javascript" src="<?= $assets ?>styles/helpers/components/js/bootstrap-formhelpers.js"></script>	
<link href="<?= $assets ?>styles/helpers/components/js/bootstrap-formhelpers.min.css" rel="stylesheet" />
<script type="text/javascript">
	$(function(){     
		$(".case_no").autocomplete({
			source: function (request, response) {
				var q = request.term;
					$.ajax({
						type: 'get',
						url: '<?= site_url('applications/suggestions_caseno_social_affair_add_form'); ?>',
						dataType: "json",
						data: {
							q : q,
							type : 'family_guarantee'
						},
						success: function (data) {
							response(data);
						}
					});
			},
			minLength: 1,
			autoFocus: false,
			delay: 200,
			response: function (event, ui) {
				if (ui.content[0].id == 0) {
					$(this).removeClass('ui-autocomplete-loading');
					$(this).removeClass('ui-autocomplete-loading');
				}
			},
			select: function (event, ui) {
				event.preventDefault();
				if (ui.item.id !== 0) {  
					var id = ui.item.id;
					$.ajax({
						type: 'GET',
						url: '<?= site_url('application_forms/getMemberRefugeesAll'); ?>',
						dataType: "json",
						data: { id : id },
						success: function (response) {											
							$("#members").html(response);						
							$("select").select2();
							$("#member_id").trigger("change");
						}  
					}); 
					$("input[name='dob']").val(ui.item.row.dob);  
               	 	$("input[name='application_id']").val(ui.item.row.application_id);
                	$("input[name='nationality_kh']").val(ui.item.row.nationality_kh);                	
                	$("input[name='firstname']").val(ui.item.row.firstname_kh);
                	$("input[name='lastname']").val(ui.item.row.lastname_kh);
                	
                	$(this).val(ui.item.label);	
					$("#"+ui.item.row.gender+"").iCheck("check");	 				 		
				} 
			}
		});
		 
		getAllAutoComplete(".village, .village","/getAllTags?v=village");
		
		function getAllAutoComplete(element, base_url){
			$(element).autocomplete({
				source: function (request, response) {
					var q = request.term;
					$.ajax({
						type: 'GET',
						url: '<?= site_url('asylum_seekers/'); ?>'+base_url,
						dataType: "json",
						data: {
							q : q
						},
						success: function (data) {
							response(data);	
						}
					});
				},
				minLength: 1,
				autoFocus: false,
				delay: 200,
				response: function (event, ui) {
					if (ui.content[0].id == 0) {
						$(this).removeClass('ui-autocomplete-loading');
						$(this).removeClass('ui-autocomplete-loading');
					}
				},
				select: function (event, ui) {
					event.preventDefault();
					$(this).val(ui.item.label);
					
					
				}
			});
		}
		
	}); 
	
	$(document).on('change','#member_id',function(){
		var application_id = $('#application_id').val();
		var member_id = $('select#member_id').val();
		$.ajax({
			url: '<?= site_url('application_forms2/getLastDataByApplicationFamilyGuranteeRequest'); ?>',
			type: 'GET',
			dataType: "JSON",
			contentType:'applications/json; charset=utf-8',
			data: { application_id : application_id,
					member_id : member_id
			 },
			success: function (d) {
				$('input[name="firstname"]').val(d.result.lastname_kh);
				$('input[name="lastname"]').val(d.result.firstname_kh);
				$("input[name='dob']").val(d.result.dob);
				$("#"+d.result.gender).iCheck("check");								
				$('input[name="occupation_kh"]').val(d.result.occupation_kh);
				$('input[name="occupation"]').val(d.result.occupation);
				$("select[name='country'] option[value="+d.result.country+"]").attr('selected','selected').change();
				$("select[name='province'] option[value="+d.result.province+"]").attr('selected','selected').change();
				$("select[name='district'] option[value="+d.result.district+"]").attr('selected','selected').change();
				$("select[name='commune'] option[value="+d.result.commune+"]").attr('selected','selected').change();
				$("input[name='village']").val(d.vill);
				$("input[name='village_kh']").val(d.vill_kh);
				$('input[name="address_kh"]').val(d.result.address_kh);
				$('input[name="address"]').val(d.result.address);
				$('input[name="contact_number"]').val(d.result.phone);
				$("#img_preview").attr('src','assets/uploads/'+d.result.photo).change();
			}  
		});	
	});

	function pad(str) {
	  str = str.toString();
	  return str.length < 2 ? pad("0" + str, 2) : str;
	}
	
	
</script>
<script type="text/javascript">
	$(function() {
		$('.c2').on('click', function(){
	    	$(".b").removeClass("hide");
	    	$(".a").addClass("hide");
		});
		$('.c1').on('click', function(){
	    	$(".a").removeClass("hide");
	    	$(".b").addClass("hide");
		});
	});
</script>

