<div class="modal-dialog modal-lg">
	<div class="modal-content">
		<div class="modal-header">				
			<button type="button" class="close" data-dismiss="modal">
				<span aria-hidden="true"><i class="fa fa-2x">&times;</i></span>
				<span class="sr-only"><?=lang('close');?></span>
			</button>
			<h4 class="modal-title" id="payModalLabel"><?= lang('add_member_exiting'); ?></h4>
		</div>
		
		<?php $attributes = array('id' => 'form1');?>
		
		<?php  echo form_open_multipart("application_forms2/add_home_visit_member_exiting/".$id, $attributes); ?> 
		  		
		<div class="modal-body"> 
				<div class="col-sm-12"> 
					<div class="row">
						<div>
							<p>
								សូមជ្រើសរើសសមាជិកគ្រួសារដែលមានស្រាប់មកធានា:
							</p> 
						</div>
						  <div class="form-group"> 
							<table class="table table-condensed table-bordered table-hover table-striped" width="100%">
								<thead>
									<tr>
										<td class="text-center" width='50'>#</td>
										<td><?= lang('full_name') ?></td> 
										<td><?= lang('sex') ?></td>
										<td><?= lang('relationship') ?></td>
										<td><?= lang('current_occupation') ?></td>
										<td><?= lang('education') ?></td>  
										<td><?= lang('dob') ?> </td> 								
									</tr>
								</thead>
								<tbody>
								<?php foreach($members as $key => $member){ ?>
									<tr>
										<td class="text-center">
											 <input type="checkbox" name="member_id[]" value="<?php echo $member->id;?>">											 
										</td> 
										<td><?= $member->lastname_kh.' '.$member->firstname_kh ?></td> 
										<td class="text-center"><?= lang($member->gender) ?></td> 
										<td class="text-center"><?= $member->relationship; ?></td>
										<td class="text-center"><?= $member->occupation_kh; ?></td> 
										<td class="text-center"><?= $member->education_kh; ?></td> 
										<td class="text-center"><?= $this->erp->toKhmer($this->erp->hrsd($member->dob)) ?></td>  
									</tr>	
								<?php }?>
								</tbody>
							</table> 
						</div> 
					</div>
				</div>
		</div>
			<div class="clearfix"></div>	
		 <div class="modal-footer">
			<?php echo form_submit('submit', lang('submit'), 'class="btn btn-primary save"'); ?>
		 </div>
		
		<?php  echo form_close(); ?>
			
	</div>
</div> 
<?= $modal_js ?>
