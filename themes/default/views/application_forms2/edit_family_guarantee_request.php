<ul id="myTab" class="nav nav-tabs">
	<li class="">
		<a href="#content_1" class="tab-grey">
			<?= lang('ផ្នែក ក.ព័ត៌មានជនភៀសខ្លួន'); ?> 
			<i class="fa fa-chevron-right" aria-hidden="true"></i>
		</a>
	</li>
	<li class="">
		<a href="#content_2" class="tab-grey">
			<?= lang('ផ្នែក ខ.ព័ត៌មានសូមធ្វើការធានាលើសមាជិកគ្រួសារ');?>
			<i class="fa fa-chevron-right" aria-hidden="true"></i>
		</a>
	</li>
</ul>  
<div class="tab-content">
	<?php 
		$countries_ = array(lang("select"));
		foreach($countries as $country){
			$countries_[$country->id] = $country->country;
		} 
		?>
		<?php 
		$provinces_ = array(lang("select"));
		foreach($provinces as $province){
			$provinces_[$province->id] = $province->name;
		} 
		?>
		<?php 
		$districts_ = array(lang("select"));
		foreach($districts as $district){
			$districts_[$district->id] = $district->name;
		} 
		?>
		<?php 
		$communes_ = array(lang("select"));
		foreach($communes as $commune){
			$communes_[$commune->id] = $commune->name;
		} 
	?>
	
	<div id="content_1" class="tab-pane fade in">
		<div class="box">
			<div class="box-content"> 
				<?php  echo form_open_multipart("application_forms2/edit_family_guarantee_request/".$id); ?>	
				
				<div class="row"> 
				
					 <div class="col-sm-6">
					   
					    <div class="col-sm-12">
							<div class="form-group">
								<?php echo lang('case_no', 'case_no'); ?>​ 
								<span class="red">*</span>
								<div class="control">
									<input type="text" readonly="readonly" value="<?= set_value('case_no', $application->case_prefix.$application->case_no);?>" name='case_no' class="form-control input-sm case_no" id="case_no" >
									<input type="hidden" name="application_id" value="<?= set_value('application_id', $row->application_id);?>">
								</div>
							</div>
							
							<div class="form-group">
								<?php echo lang('ឈ្មោះ', 'ឈ្មោះ'); ?>​ 
								<?php
									$members_ = array($application->firstname_kh . " " . $application->lastname_kh." ".lang("( ម្ចាស់ករណី)"));
									foreach($default_members as $member){
										$members_[$member->id] = $member->firstname_kh . " " . $member->lastname_kh." ".lang("( សមាជិកក្នុងបន្ទុក)");
									}						 
								?>
								<span class="red">*</span>
								<div class="control" id="members">
									<?php echo form_dropdown('member', $members_, $row->member_id, ' class="form-control" '); ?>
								</div>
							</div>
				
						</div>
						
						<div class="clearfix"></div>
						
						<div class="col-sm-6">
							<div class="form-group">
								<?php echo lang('firstname', 'firstname'); ?>
								<small class="red">* (<?= lang('just_information'); ?>)</small>
								<input class="form-control input-sm" type="text" name="firstname" value="<?= set_value("firstname", $application->firstname_kh) ?>" />
							</div>
						</div>
						
						<div class="col-sm-6">
							<div class="form-group">
								<?php echo lang('lastname', 'lastname'); ?>
								<small class="red">* (<?= lang('just_information'); ?>)</small>
								<input class="form-control input-sm" type="text" name="lastname" value="<?= set_value("lastname",$application->lastname_kh) ?>" />
							</div>
						</div>
						
						<div class="col-sm-6">
							<div class="form-group">
								<?php echo lang('ភេទ', 'ភេទ'); ?>
								<small class="red">* (<?= lang('just_information'); ?>)</small>
								<div class="controls">
									<label class="radio-inline">
										<input type="radio" value="male" <?= ($application->gender=='male'?'checked':'') ?> name="gender"> <?= lang('male'); ?>
									</label>
									<label class="radio-inline">
										<input type="radio" value="female" <?= ($application->gender=='female'?'checked':'') ?>  name="gender"> <?= lang('female'); ?>
									</label> 
								</div> 
							</div>
						</div>
						
						<div class="col-sm-6">
							<div class="form-group">
								<?php echo lang('ថ្ងៃខែឆ្នាំកំណើត', 'ថ្ងៃខែឆ្នាំកំណើត'); ?>
								<small class="red">* (<?= lang('just_information'); ?>)</small>
								<div class="controls">
									<input type="text" value="<?= set_value('dob', $this->erp->hrsd($application->dob)) ?>" name="dob" class="form-control input-sm date" />
								</div>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<?php echo lang('មុខរបរបច្ចុប្បន្ន', 'មុខរបរបច្ចុប្បន្ន'); ?>								
								<span class="red">*</span>
								<div class="controls">
									<input type="text" value="<?= set_value('occupation_kh', $row->occupation_kh) ?>" name="occupation_kh" class="form-control input-sm" />
								</div>
							</div>
						</div>
						
						<div class="col-sm-6">
							<div class="form-group">
								<?php echo lang('មុខរបរបច្ចុប្បន្ន', 'មុខរបរបច្ចុប្បន្ន'); ?>​ (EN)
								<span class="red">*</span>
								<div class="control">
									<input type="text" value="<?= set_value('occupation', $row->occupation) ?>" class="form-control input-sm "   name="occupation" id="occupation" />
								</div>
							</div>
						</div>
						
						
						<div class="col-sm-12">
							<div class="form-group">
								<?php echo lang('លេខទូរស័ព្ទទំនាក់ទំនង', 'លេខទូរស័ព្ទទំនាក់ទំនង'); ?>​  
								<div class="control">
									<input type="text" value="<?= set_value('contact_number', $row->contact_number) ?>"  class="form-control input-sm input-medium bfh-phone" data-format="+855 (ddd) ddd-dddd" name="contact_number"  id="contact_number" />
								</div>
							</div> 
						</div>
						
						<div class="col-sm-12 hidden" >
							<div class="form-group">
								<?php echo lang('attachment', 'attachment'); ?>​ 
								<span class="red">*</span>
								<div class="control">
									<input type="file"  class="form-control input-sm" name="attachment"  id="attachment" />
								</div>
							</div> 
						</div>
						
						<div class="col-sm-12">
							<div class="form-group">
								<button type="submit" class="btn btn-success ">
									<?= lang("submit"); ?> 
								</button>
							</div>
						</div> 
					</div>  
					
					
					<div class="col-sm-6">
					
						<div class="panel panel-warning">
							
							<div class="panel-heading"><?php echo lang("address","address") ?></div>
							
							<div class="panel-body" style="padding: 5px;">
							
								<div class="col-lg-12">
									<div class="form-group">
										<?php echo lang('country', 'country'); ?> 
										<div class="controls">
											<?php echo form_dropdown('country', $countries_, set_value('country', $row->country), ' class="form-control" '); ?>												
										</div>
									</div>
								</div>
								
								<div class="col-lg-6">
									<div class="form-group">
										<?php echo lang('province', 'province'); ?> 
										<div class="controls">
											<?php echo form_dropdown('province', $provinces_, set_value('province', $row->province), ' class="form-control" '); ?>												
										</div>
									</div>
								</div>
								
								<div class="col-lg-6">
									<div class="form-group">
										<?php echo lang('district', 'district'); ?> 
										<div class="controls">
											<?php echo form_dropdown('district', $districts_, set_value('district', $row->district), ' class="form-control" '); ?>												
										</div>
									</div>
								</div>
								
								<div class="col-lg-12">
									<div class="form-group">
										<?php echo lang('commune', 'commune'); ?> 
										<div class="controls">
											<?php echo form_dropdown('commune', $communes_, set_value('commune', $row->commune), ' class="form-control" '); ?>												
										</div>
									</div>
								</div>
								
								<div class="col-lg-6">
									<div class="form-group">
										<?php echo lang('village', 'village')." (KH)"; ?> 
										<div class="controls">
											<?php 
											echo form_input('village_kh', set_value('village_kh', $this->applications->getTagsKhmerById($row->village)),'class="form-control village_kh" '); ?>											
										</div>
									</div>
								</div>
								
								<div class="col-lg-6">
									<div class="form-group">
										<?php echo lang('village', 'village')." (EN)"; ?> 
										<div class="controls">
											<?php 
											echo form_input('village', set_value('village', $this->applications->getTagsById($row->village)),'class="form-control village" '); ?>											
										</div>
									</div>
								</div>
								
								<div class="col-lg-12">
									<div class="form-group"> 
										<?php echo lang('other', 'other'); ?>​ (KH) 
										<div class="control">
											<input type="text" value="<?= set_value("address_kh", $row->address_kh) ?>" class="form-control input-sm" name="address_kh"  id="address_kh" />
										</div> 
									</div> 
								</div> 
								
								<div class="col-lg-12">
									<div class="form-group"> 
										<?php echo lang('other', 'other'); ?>​  (EN) 
										<div class="control">
											<input type="text" value="<?= set_value("address", $row->address) ?>" class="form-control input-sm" name="address"  id="address" />
										</div> 
									</div> 
								</div> 
								
							</div>
						</div>
							
					</div>	
									
				</div>
				<?php echo form_close();?>
			</div>
		</div>
	</div>
	<div id="content_2" class="tab-pane fade in">
		<div class="box">
			<div class="box-content">
				<div class="row">    
					<div class="col-sm-12">
						<div class="form-group">
							<label><?= lang("សូមជ្រើសរើសសមាជិកគ្រួសារដើម្បីធានា","សូមជ្រើសរើសសមាជិកគ្រួសារដើម្បីធានា");?></label>
							<a  data-backdrop="static" data-keyboard="false"  data-toggle="modal" data-target="#myModal" href="<?= site_url("application_forms2/add_family_guarantee_member/".$id); ?>" class="btn btn-danger"><?= lang("add_member") ?></a>
							<a data-backdrop="static" data-keyboard="false"  data-toggle="modal" data-target="#myModal" href="<?= site_url("application_forms2/add_family_guarantee_member_exiting/".$id); ?>" class="btn btn-info"><?= lang("add_member_exiting") ?></a>
						</div>
						<div class="form-group"> 
							<table class="table table-condensed table-bordered table-hover table-striped" width="100%">
								<thead>
									<tr>
										<td class="text-center" width='100'>#</td>
										<td><?= lang('full_name') ?></td> 
										<td><?= lang('sex') ?></td>
										<td width="80px; "><?= lang('relationship') ?></td>
										<td><?= lang('current_occupation') ?></td>
										<td width="120px; "><?= lang('Education') ?></td>
										<td><?= lang('dob') ?> </td>
										<td width="120px; "><?= lang('ការសម្រេច') ?> </td>
										<td width="120px; "><?= lang('ការមកដល់') ?> </td>
									</tr>
								</thead>
								<tbody>
								<?php 
									$relationships = array();
									foreach($relationship as $relation){
										$relationships[$relation->id] = $relation->relationship_kh;
									} 						
									foreach($members as $key => $member){
																			
										if($member->is_guarantee == 1){
											$guarantee = lang("guarantee");
										}else{
											$guarantee = "";
										}
									?>
									<tr>
										<td class="text-center">
											<a href="<?= site_url("application_forms2/delete_family_guarantee_member/".$member->id) ?>" onclick="return confirm('Are you sure?');" class="btn btn-danger btn-xs">
												<i class="fa fa-trash"></i>
											</a>
											<a href="<?= site_url("application_forms2/edit_family_guarantee_member/".$member->id) ?>" class="btn btn-success btn-xs" data-backdrop="static" data-keyboard="false"  data-toggle="modal" data-target="#myModal">
												<i class="fa fa-edit"></i>
											</a>  
											<a href="javascript:void(0)" class="hidden btn btn-warning btn-xs" id="button_<?php echo $member->id?>"  onclick="saveData(<?php echo $member->id?>)" >
												<i class="fa fa-check"></i>
											</a> 
										</td> 
										<td><?= $member->lastname_kh.' '.$member->firstname_kh ?></td> 
										<td class="text-center"><?= lang($member->gender) ?></td> 
										<td class="text-center"><?= $relationships[$member->relationship]; ?></td>
										<td class="text-center"><?= $member->occupation_kh; ?></td> 
										<td class="text-center"><?= $member->education_kh; ?></td> 
										<td class="text-center"><?= $this->erp->toKhmer($this->erp->hrsd($member->dob)) ?></td>
										<td class="text-center"> 
											<input    id="status_option_<?php echo $member->id?>"​ value="<?= $member->status_option ?>" type="text" name="status_option_<?php echo $member->id?>"   onkeyup="myFunction(<?php echo $member->id?>)"     class="form-control input-sm" />
										</td> 	
										<td class="text-center">
											<input  id="come_option_<?php echo $member->id?>"  value="<?= $member->come_option ?>" type="text" name="come_option_<?php echo $member->id?>"     onkeyup="myFunction(<?php echo $member->id?>)"  class="form-control input-sm" />
										</td> 	
									</tr>	
								<?php }?>										
								</tbody>
							</table> 
						</div>
					</div>
				</div> 
			</div>
		</div>
	</div>  
	
</div>
</div>
<?php echo form_close(); ?>  
<script type="text/javascript" src="<?= $assets ?>styles/helpers/components/js/bootstrap-formhelpers.js"></script>	
<link href="<?= $assets ?>styles/helpers/components/js/bootstrap-formhelpers.min.css" rel="stylesheet" />
<script type="text/javascript">
	function myFunction(num) {  
		var x = document.getElementById("status_option_"+num);
		var y = document.getElementById("come_option_"+num);
		if(x.value != ""  || y.value != ""){
			$("#button_"+num).removeClass("hidden");
		}else{
			$("#button_"+num).addClass("hidden");
		} 
	}  
	function saveData(n) {
		var id = n; 
		var status_option = $('input[name="status_option_'+n+'"]').val();
		var come_option = $('input[name="come_option_'+n+'"]').val();  
		$.ajax({
			type: 'post',
			url: '<?= site_url('application_forms2/update_member_guarantee'); ?>'+"/"+id,
			data: { 	
				func: 'update_status',
					'<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>',
				status:status_option,
				come:come_option
			}, 
			success: function (data) { 
				 bootbox.alert("<?php echo lang('success_change_status');?>", function(){ 
							window.location.href= " <?= site_url('application_forms2/edit_family_guarantee_request/'.$id); ?>"+"#content_2";
							location.reload();
						})
			},
			error:function (){
				alert("error");
			}
		});   
	}
	$(function(){   
		  $("input[name='is_come']").on("ifUnchecked",function(){
			var id= $(this).val();
			if(id) { 
				$.ajax({
					type: 'post',
					url: '<?= site_url('applications/family_come_status'); ?>'+"/"+id+"?status=not_come", 
					data: { 
						func: 'family_come_status',
						'<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'
					}, 
					success: function (response) {
						bootbox.alert("<?php echo lang('success_change_status');?>", function(){ 
							window.location.href= " <?= site_url('application_forms2/edit_family_guarantee_request/'.$id); ?>"+"#content_2";
						})
					},
					error:function (){
						alert("error");
					}
				});   
			}
		});
		 
		$("input[name='is_come']").on("ifChecked",function(){
			var id= $(this).val();
			if(id) {
				$.ajax({
					type: 'post',
					url: '<?= site_url('applications/family_come_status'); ?>'+"/"+id, 
					data: { 
						func: 'family_come_status',
						'<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'
					}, 
					success: function (response) { 
						bootbox.alert("<?php echo lang('success_change_status');?>", function(){ 
							window.location.href= " <?= site_url('application_forms2/edit_family_guarantee_request/'.$id); ?>"+"#content_2";
						})
						 
					},
					error:function (){
						alert("error");
					}
				});  
			} 
		});
		  
		$(".case_no").autocomplete({
			source: function (request, response) {
				var q = request.term; 
					$.ajax({
						type: 'get',
						url: '<?= site_url('applications/suggestions_caseno'); ?>',
						dataType: "json",
						data: {
							q : q
						},
						success: function (data) {
							response(data);
						}
					}); 
			},
			minLength: 1,
			autoFocus: false,
			delay: 200,
			response: function (event, ui) {
				if (ui.content[0].id == 0) {
					$(this).removeClass('ui-autocomplete-loading');
					$(this).removeClass('ui-autocomplete-loading');
				}
			},
			select: function (event, ui) {
				event.preventDefault();
				if (ui.item.id !== 0) { 
					var date = new Date(ui.item.row.dob);
					var dob = (date.getDate() + 1) + '/' + pad(date.getMonth()) + '/' +  date.getFullYear();
					$("input[name='dob'").val(dob); 
               	 	$("input[name='application_id'").val(ui.item.row.application_id);
                	$("input[name='nationality_kh'").val(ui.item.row.nationality_kh);                	
                	$("input[name='firstname'").val(ui.item.row.firstname_kh);
                	$("input[name='lastname'").val(ui.item.row.lastname_kh);
				} 
			}
		}); 
		
		getAllAutoComplete(".village, .village","/getAllTags?v=village");
		function getAllAutoComplete(element, base_url){
			$(element).autocomplete({
				source: function (request, response) {
					var q = request.term;
					$.ajax({
						type: 'GET',
						url: '<?= site_url('asylum_seekers/'); ?>'+base_url,
						dataType: "json",
						data: {
							q : q
						},
						success: function (data) {
							response(data);
						}
					});
				},
				minLength: 1,
				autoFocus: false,
				delay: 200,
				response: function (event, ui) {
					if (ui.content[0].id == 0) {
						$(this).removeClass('ui-autocomplete-loading');
						$(this).removeClass('ui-autocomplete-loading');
					}
				},
				select: function (event, ui) {
					event.preventDefault();
					$(this).val(ui.item.label);
				}
			});
		}
	
	}); 
	function pad(str) {
	  str = str.toString();
	  return str.length < 2 ? pad("0" + str, 2) : str;
	}   
</script>
  
 


