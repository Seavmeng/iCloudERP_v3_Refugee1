<script>$(document).ready(function () {
        CURI = '<?= site_url('reports/balance_sheet'); ?>';
	});</script>
<style>@media print {
        .fa {
            color: #EEE;
            display: none;
        }

        .small-box {
            border: 1px solid #CCC;
        }
    }
</style>
<?php
	$start_date=date('Y-m-d',strtotime($start));
	$rep_space_end=str_replace(' ','_',$end);
	$end_date=str_replace(':','-',$rep_space_end);
?>
<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-bars"></i><?= lang('balance_sheet'); ?></h2>
        <div class="box-icon">
            <div class="form-group choose-date hidden-xs">
                <div class="controls">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                        <input type="text"
                               value="<?= ($start ? $this->erp->hrld($start) : '') . ' - ' . ($end ? $this->erp->hrld($end) : ''); ?>"
                               id="daterange" class="form-control">
                        <span class="input-group-addon"><i class="fa fa-chevron-down"></i></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="box-icon">
            <ul class="btn-tasks">
                <li class="dropdown"><a href="#" id="pdf" class="tip" title="<?= lang('download_pdf') ?>"><i class="icon fa fa-file-pdf-o"></i></a></li>
				<li class="dropdown"><a href="#" id="xls" class="tip" title="<?= lang('download_xls') ?>"><i class="icon fa fa-file-excel-o"></i></a></li>
                <li class="dropdown"><a href="#" id="image" class="tip" title="<?= lang('save_image') ?>"><i
                            class="icon fa fa-file-picture-o"></i></a></li>
						<li class="dropdown">
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#"><i
                                    class="icon fa fa-building-o tip" data-placement="left"
                                    title="<?= lang("projects") ?>"></i></a>
                            <ul class="dropdown-menu pull-right" class="tasks-menus" role="menu"
                                aria-labelledby="dLabel">
                                <li><a href="<?= site_url('reports/balance_sheet') ?>"><i
                                            class="fa fa-building-o"></i> <?= lang('projects') ?></a></li>
                                <li class="divider"></li>
                                <?php
								$b_sep = 0;
                                foreach ($billers as $biller) {
									
									$biller_sep = explode('-', $this->uri->segment(7));
									if($biller_sep[$b_sep] == $biller->id){
										echo '<li ' . ($biller_id && $biller_id == $biller->id ? 'class="active"' : '') . '>&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" name="biller_checkbox[]" class="checkbox biller_checkbox" checked value="'. $biller->id .'" >&nbsp;&nbsp;' . $biller->company . '</li>';
										echo '<li class="divider"></li>';
										$b_sep++;
									}else{
										echo '<li ' . ($biller_id && $biller_id == $biller->id ? 'class="active"' : '') . '>&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" name="biller_checkbox[]" class="checkbox biller_checkbox" value="'. $biller->id .'" >&nbsp;&nbsp;' . $biller->company . '</li>';
										echo '<li class="divider"></li>';
									}
                                }
                                ?>
								<li class="text-center"><a href="#" id="biller-filter" class="btn btn-primary"><?=lang('submit')?></a></li>
                            </ul>
                        </li>
            </ul>
        </div>
    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">
                <p class="introtext"><?= lang('list_results'); ?></p>
				<?php $num_col=2; ?>
                <div class="table-scroll">
                    <table  cellpadding="0" cellspacing="0" border="0" class="table table-bordered table-condensed">
						 <thead>
							<tr>                           
								<th style="text-align:left;"><div class="fix-text-col"><?= lang("account_name"); ?></div></th>
								<?php
								$new_billers = array();
								foreach ($billers as $b1) {
									if($this->uri->segment(7)){
										$biller_sep = explode('-', $this->uri->segment(7));
										for($i=0; $i < count($biller_sep); $i++){
											if($biller_sep[$i] == $b1->id){
												echo '<th><div class="fix-text">' . $b1->company . '</div></th>';
												$new_billers[] = array('id' => $b1->id);
											}
										}
									}else{
										$new_billers = $billers;
										echo '<th><div class="fix-text">' . $b1->company . '</div></th>';
									}
									$num_col++;
								}
								?>
								<th><div class="fix-text"><?= lang("total_amount") ?></div></th>
							</tr>
							<tr class="primary">                            
								<th style="text-align:left;" colspan="<?=$num_col?>"><?= lang("asset"); ?></th>							
							</tr>
                        </thead> 
				
                        <tbody>
						<?php
							$total_asset = 0;
							$totalBeforeAyear_asset = 0;
							
							$colbot = 0;
							if($this->uri->segment(7)){
								$col1 = 3;
								$colbot = 3;
							}else{
								$colcount = count($new_billers);
								$col1 = $colcount+2;
								$colbot = $colcount + 2;
							}
							
							$total_asset_arr = array();
							$total_lib_arr = array();
							$total_eq_arr = array();
							$sum_asset_arr = array();
							$sum_lib_arr = array();
							$sum_eq_arr = array();
							
							foreach($dataAsset->result() as $row){
								//$total_asset += round($row->amount,2);
								
								$index = 0;
								$total_per_asset = 0;
								echo '<tr>';
								for($i = 1; $i <= count($new_billers); $i++){
									$bill_id = 0;
									if($this->uri->segment(7)){
										$bill_id = $new_billers[$index]['id'];
									}else{
										$bill_id = $new_billers[$index]->id;
									}
									
									$query = $this->db->query("SELECT
									SUM(CASE WHEN erp_gl_trans.amount < 0 THEN erp_gl_trans.amount ELSE 0 END) as NegativeTotal,
									SUM(CASE WHEN erp_gl_trans.amount >= 0 THEN erp_gl_trans.amount ELSE 0 END) as PostiveTotal,
									SUM(
										COALESCE (erp_gl_trans.amount, 0)
									) AS amount
									FROM
										erp_gl_trans
									WHERE
										biller_id = '$bill_id' AND account_code = '" . $row->account_code . "'
										AND date(erp_gl_trans.tran_date) >= '$from_date' AND date(erp_gl_trans.tran_date) <= '$to_date' ;");
									$totalBeforeAyearRows = $query->row();
									$totalBeforeAyear_asset += ($totalBeforeAyearRows->NegativeTotal + $totalBeforeAyearRows->PostiveTotal);
									
									$amount_asset = '';
									$amount_asset = $totalBeforeAyearRows->amount;
									
									if($totalBeforeAyearRows->amount<0){
										$amount_asset = '( '.number_format(abs($totalBeforeAyearRows->amount),2).' )';
									}else{
										$amount_asset = number_format(abs($totalBeforeAyearRows->amount),2);
									}
									
									if(($index+1)==1){
										$total_asset_arr[] = array(
											'biller_id' => $bill_id,
											'amount' => round($totalBeforeAyearRows->amount,2)
										);
								?>
										<td><div class="fix-text-col"><?php echo $row->account_code;?> - <?php echo $row->accountname;?></div></td>
										<td><div class="fix-text text-right"><?php echo $amount_asset;?></div></td>
								<?php
									}else{
										$total_asset_arr[] = array(
											'biller_id' => $bill_id,
											'amount' => round($totalBeforeAyearRows->amount,2)
										);
										echo '<td><div class="fix-text text-right">'. $amount_asset.'</div></td>';
									}
									$total_per_asset += round($totalBeforeAyearRows->amount, 2);
									
									$total_asset += round($totalBeforeAyearRows->amount,2);
									
									$index++;
								}
								
								if($total_per_asset<0){
									$total_per_asset = '( '.number_format(abs($total_per_asset),2).' )';
								}else{
									$total_per_asset = number_format(abs($total_per_asset),2);
								}
								
								echo '<td><div class="fix-text text-right">'.$total_per_asset.'</div></td>';
								echo '</tr>';
							}
						?>
							<tr>
								<td><div class="fix-text-col"><b><?= lang("total_asset"); ?></b></div></td>
								<?php
								for($c= 0; $c < count($new_billers); $c++){
									$in_bill_id1 = 0;
									if($this->uri->segment(7)){
										$in_bill_id1 = $new_billers[$c]['id'];
									}else{
										$in_bill_id1 = $new_billers[$c]->id;
									}
									$total_asset_amt = 0;
									foreach($total_asset_arr as $new_arr){
										if($new_arr['biller_id'] == $in_bill_id1){
											$total_asset_amt += $new_arr['amount'];
										}
									}
									$sum_asset_arr[] = array(
										'id' => $in_bill_id1,
										'amount' => $total_asset_amt
									);
									if($total_asset_amt<0){
										$total_asset_amt = '( '.number_format(abs($total_asset_amt),2).' )';
									}else{
										$total_asset_amt = number_format(abs($total_asset_amt),2);
									}
									echo '<td><div class="fix-text text-right"><b>'. $total_asset_amt .'</b></div></td>';
								}
								$total_asset_display = '';
								if($total_asset<0){
									$total_asset_display = '( '.number_format(abs($total_asset),2).' )';
								}else{
									$total_asset_display = number_format(abs($total_asset),2);
								}
								
								?>
								<td><div class="fix-text text-right"><b><?php echo $total_asset_display;?></b></div></td>
							</tr>
						    <tr class="primary">
								<th style="text-align:left;" colspan="<?=$num_col?>"><?= lang("liabilities"); ?></th>
							</tr>  
							<?php
							$total_liability = 0;
							$totalBeforeAyear_liability = 0;
							foreach($dataLiability->result() as $rowlia){
								// $total_liability += round(abs($rowlia->amount),2);
								
								$index = 0;
								$total_per_lib = 0;
								echo '<tr>';
								for($i = 1; $i <= count($new_billers); $i++){
									$bill_id = 0;
									if($this->uri->segment(7)){
										$bill_id = $new_billers[$index]['id'];
									}else{
										$bill_id = $new_billers[$index]->id;
									}
								
									$query = $this->db->query("SELECT
									SUM(CASE WHEN erp_gl_trans.amount < 0 THEN erp_gl_trans.amount ELSE 0 END) as NegativeTotal,
									SUM(CASE WHEN erp_gl_trans.amount >= 0 THEN erp_gl_trans.amount ELSE 0 END) as PostiveTotal,
									SUM(
										COALESCE (erp_gl_trans.amount, 0)
									) AS amount
									FROM
										erp_gl_trans
									WHERE
										biller_id = '$bill_id' AND account_code = '" . $rowlia->account_code . "'
										AND date(erp_gl_trans.tran_date) BETWEEN '$from_date' AND '$to_date' ;");
									$totalBeforeAyearRows = $query->row();
									$totalBeforeAyear_liability += ($totalBeforeAyearRows->NegativeTotal + $totalBeforeAyearRows->PostiveTotal);
								
									$amount_lib = '';
									
									if((-1)*$totalBeforeAyearRows->amount<0){
										$amount_lib = '( '.number_format(abs($totalBeforeAyearRows->amount),2).' )';
									}else{
										$amount_lib = number_format(abs($totalBeforeAyearRows->amount),2);
									}
									
									if(($index+1)==1){
										$total_lib_arr[] = array(
											'biller_id' => $bill_id,
											'amount' => round(abs($totalBeforeAyearRows->amount),2)
										);
									?>
										<td><div class="fix-text-col"><?php echo $rowlia->account_code;?> - <?php echo $rowlia->accountname;?></div></td>
										<td><div class="fix-text text-right"><?php echo $amount_lib;?></div></td>
									<?php
									} else {
										$total_lib_arr[] = array(
											'biller_id' => $bill_id,
											'amount' => round(abs($totalBeforeAyearRows->amount),2)
										);
										echo '<td><div class="fix-text text-right">'. $amount_lib .'</div></td>';
									}
									$total_per_lib += round((-1)*$totalBeforeAyearRows->amount, 2);
									
									$total_liability += round((-1)*$totalBeforeAyearRows->amount,2);
									$index++;
								}
								$total_per_lib_display = $total_per_lib;
								
								if($total_per_lib < 0){
									$total_per_lib_display = '( ' . number_format(abs($total_per_lib),2) . ' )';
								}else{
									$total_per_lib_display = number_format(abs($total_per_lib),2);
								}
							
								echo '<td><div class="fix-text text-right">'. $total_per_lib_display.'</div></td>';
								echo '</tr>';
							}
						?>
							<tr>
							<td><div class="fix-text-col"><b><?= lang("total_liabilities"); ?></b></div></td>
							
							<?php
							for($c= 0; $c < count($new_billers); $c++){
								$in_bill_id1 = 0;
								if($this->uri->segment(7)){
									$in_bill_id1 = $new_billers[$c]['id'];
								}else{
									$in_bill_id1 = $new_billers[$c]->id;
								}
								$total_lib_amt = 0;
								foreach($total_lib_arr as $new_arr){
									if($new_arr['biller_id'] == $in_bill_id1){
										$total_lib_amt += abs($new_arr['amount']);
									}
								}
								$sum_lib_arr[] = array(
									'biller_id' => $in_bill_id1,
									'amount' => abs($total_lib_amt)
								);
								/*
								if($total_lib_amt<0){
									$total_lib_amt = '( '.number_format(abs($total_lib_amt),2).' )';
								}else{
									$total_lib_amt = number_format(abs($total_lib_amt),2);
								}
								*/
								echo '<td><div class="fix-text text-right"><b>'. number_format(abs($total_lib_amt),2) .'</b></div></td>';
							}
							
							$end_total_lib = $total_liability;
							
							/*
							$total_lib_display = '';
							if($total_liability<0){
								$total_lib_display = '( '.number_format(abs($total_liability),2).' )';
							}else{
								$total_lib_display = number_format(abs($total_liability),2);
							}
							*/
							?>
							
							<td><div class="fix-text text-right"><b><?php echo number_format(abs($total_liability),2);?></b></div></td>						
							</tr>
							<tr class="primary">                            
								<th style="text-align:left;" colspan="<?=$num_col?>"><?= lang("equities"); ?></th>                       
							</tr>
						<?php
							
							$total_income = 0;
							$total_expense = 0;
							$total_retained = 0;

							$total_income_beforeAyear = 0;
							$total_expense_beforeAyear = 0;
							$total_retained_beforeAyear = 0;
							$queryIncom = $this->db->query("SELECT sum(erp_gl_trans.amount) AS amount FROM
														erp_gl_trans
													INNER JOIN erp_gl_charts ON erp_gl_charts.accountcode = erp_gl_trans.account_code
													WHERE DATE(tran_date) = '$totalBeforeAyear' AND	erp_gl_trans.sectionid IN ('40,70') 
								AND date(erp_gl_trans.tran_date) BETWEEN '$from_date' AND '$to_date' GROUP BY erp_gl_trans.account_code;");
							$total_income_beforeAyear = $queryIncom->amount;

							$queryExpense = $this->db->query("SELECT sum(erp_gl_trans.amount) AS amount FROM
														erp_gl_trans
													INNER JOIN erp_gl_charts ON erp_gl_charts.accountcode = erp_gl_trans.account_code
													WHERE DATE(tran_date) = '$totalBeforeAyear' AND	erp_gl_trans.sectionid IN ('50,60,80,90') GROUP BY erp_gl_trans.account_code;");
							$total_expense_beforeAyear = $queryExpense->amount;

							$total_retained_beforeAyear = abs($total_income_beforeAyear)-abs($total_expense_beforeAyear);
							$retained_inc_arr = array();
							$retained_exp_arr = array();
							foreach($dataIncome->result() as $rowincome){
								$total_income += $rowincome->amount;
							}
							foreach($dataAllIncome->result() as $rowallinc){
								$retained_inc_arr[] = array(
									'biller_id' => $rowallinc->biller_id,
									'amount' => round($rowallinc->amount,2)
								);
							}
							
							foreach($dataExpense->result() as $rowexpense){
								$total_expense += $rowexpense->amount;
							}
							foreach($dataAllExpense->result() as $rowallexp){
								$retained_exp_arr[] = array(
									'biller_id' => $rowallexp->biller_id,
									'amount' => round($rowallexp->amount,2)
								);
							}
							//$this->erp->print_arrays($retained_inc_arr, $retained_exp_arr);
							$total_retained = abs($total_income) - abs($total_expense);
						?>
							<tr>
							<td><div class="fix-text-col">300200 - Retained Earnings</div></td>
							<?php
							//$this->erp->print_arrays($retained_exp_arr, $retained_inc_arr);
							$total_retained_arr = array();
							for($c= 0; $c < count($new_billers); $c++){
								$in_bill_id1 = 0;
								if($this->uri->segment(7)){
									$in_bill_id1 = $new_billers[$c]['id'];
								}else{
									$in_bill_id1 = $new_billers[$c]->id;
								}
								$total_per_retained = 0;
								
								$k = 0;
								$r_inc_per = 0;
								$r_exp_per = 0;
								
								if(count($retained_exp_arr) == 0){
									foreach($retained_inc_arr as $exp_row){
										if($exp_row['biller_id'] == $in_bill_id1){
											$r_exp_per += $exp_row['amount'];
										}
										$k++;
									}
								}
								
								foreach($retained_exp_arr as $exp_row){
									if($exp_row['biller_id'] == $in_bill_id1){
										$r_exp_per += $exp_row['amount'];
									}
									if($in_bill_id1 == $retained_inc_arr[$k]['biller_id']){
										$r_inc_per += $retained_inc_arr[$k]['amount'];
									}
									$k++;
								}
								$total_per_retained = abs($r_inc_per) - abs($r_exp_per);
								
								$total_retained_arr[] = array(
									'biller_id' => $in_bill_id1,
									'amount' => round($total_per_retained,2)
								);
								
								if($total_per_retained<0){
									$total_per_retained = '( '.number_format(abs($total_per_retained),2).' )';
								}else{
									$total_per_retained = number_format(abs($total_per_retained),2);
								}
								echo '<td class="text-right">'. $total_per_retained .'</td>';
							}
							?>
							
						<?php if($total_retained<0) { ?>						
							<td><div class="fix-text text-right"><?php echo '( ' . number_format(abs($total_retained),2) . ' )';?></div></td>
						<?php } else { ?>
							<td><div class="fix-text text-right"><?php echo number_format(abs($total_retained),2);?></div></td>
						<?php }	?>							
							
							
							</tr>
						<?php
							$total_equity = 0;
							$totalBeforeAyear_equity = 0;
							foreach($dataEquity->result() as $rowequity){
								$total_equity += round($rowequity->amount,2);
								
								$index = 0;
								$total_per_eq = 0;
								echo '<tr>';
								for($i = 1; $i <= count($new_billers); $i++){
									$bill_id = 0;
									if($this->uri->segment(7)){
										$bill_id = $new_billers[$index]['id'];
									}else{
										$bill_id = $new_billers[$index]->id;
									}
									$query = $this->db->query("SELECT
										SUM(erp_gl_trans.amount) AS amount
									FROM
										erp_gl_trans
									WHERE
										biller_id = '$bill_id' AND account_code = '" . $rowequity->account_code . "'
									AND date(erp_gl_trans.tran_date) BETWEEN '$from_date' AND '$to_date' ;");
									$totalBeforeAyearRows = $query->row();
									$totalBeforeAyear_equity += round($totalBeforeAyearRows->amount,2);
									/*
									$amount_eq = '';
									$amount_eq = $totalBeforeAyearRows->amount;
									
									if($totalBeforeAyearRows->amount<0){
										$amount_eq = '( '.number_format(abs($totalBeforeAyearRows->amount),2).' )';
									}else{
										$amount_eq = number_format(abs($totalBeforeAyearRows->amount),2);
									}
									*/
									if(($index+1)==1){
										$total_eq_arr[] = array(
											'biller_id' => $bill_id,
											'amount' => round(abs($totalBeforeAyearRows->amount),2)
										);
							?>
									<td><div class="fix-text-col"><?php echo $rowequity->account_code;?> - <?php echo $rowequity->accountname;?></div></td>
									<td><div class="fix-text text-right"><?php echo number_format(abs($totalBeforeAyearRows->amount),2);?></div></td>
							<?php
									}else{
										$total_eq_arr[] = array(
											'biller_id' => $bill_id,
											'amount' => round(abs($totalBeforeAyearRows->amount),2)
										);
										echo '<td><div class="text-right">' .number_format(abs($totalBeforeAyearRows->amount),2). '</div></td>';
									}
									$total_per_eq += round(abs($totalBeforeAyearRows->amount),2);
									$index++;
								}
								/*
								if($total_per_eq<0){
									$total_per_eq = '( '.number_format(abs($total_per_eq),2).' )';
								}else{
									$total_per_eq = number_format(abs($total_per_eq),2);
								}
								*/
								echo '<td><div class="fix-text text-right">'.number_format(abs($total_per_eq),2).'</div></td>';
								echo '</tr>';
							}
						?>							
							<tr>
							<td><div class="fix-text-col"><b><?= lang("total_equities"); ?></b></div></td>
							
							<?php
							for($c= 0; $c < count($new_billers); $c++){
								$in_bill_id1 = 0;
								if($this->uri->segment(7)){
									$in_bill_id1 = $new_billers[$c]['id'];
								}else{
									$in_bill_id1 = $new_billers[$c]->id;
								}
								$total_eq_amt = 0;
								$k = 0;
								if(count($total_eq_arr) == 0){
									foreach($total_retained_arr as $new_arr){
										if($new_arr['biller_id'] == $in_bill_id1){
											$total_eq_amt += $total_retained_arr[$k]['amount'] ;
										}
										$k++;
									}
								}
								foreach($total_eq_arr as $new_arr){
									if($new_arr['biller_id'] == $in_bill_id1){
										if($total_retained_arr[$k]['biller_id'] == $in_bill_id1){
											$total_eq_amt += abs($new_arr['amount']) + $total_retained_arr[$k]['amount'] ;
										}else{
											$total_eq_amt += ($new_arr['amount']);
										}
									}
									$k++;
								}
								$sum_eq_arr[] = array(
									'biller_id' => $in_bill_id1,
									'amount' => $total_eq_amt
								);

								if($total_eq_amt<0){
									$total_eq_amt = '( '.number_format(abs($total_eq_amt),2).' )';
								}else{
									$total_eq_amt = number_format(abs($total_eq_amt),2);
								}
								
								echo '<td><div class="fix-text text-right"><b>'. $total_eq_amt .'</b></div></td>';
							}
							$total_eq_sum = round(($total_retained+$total_equity), 2);

							$end_total_eq = $total_eq_sum;
							
							if($total_eq_sum <0){
								$total_eq_sum = '( ' . number_format(abs($total_retained + $total_equity),2) . ' )';
							}else{
								$total_eq_sum = number_format(abs($total_retained + $total_equity),2);
							}
							?>
							
							<td><div class="fix-text text-right"><b><?php echo $total_eq_sum;?></b></div></td>														

							</tr>
							
							<tr>
								<td><div class="fix-text-col"><b><?= lang("total_liabilities_equities"); ?></b></div></td>
								
								<?php
								for($c= 0; $c < count($new_billers); $c++){
									$in_bill_id1 = 0;
									if($this->uri->segment(7)){
										$in_bill_id1 = $new_billers[$c]['id'];
									}else{
										$in_bill_id1 = $new_billers[$c]->id;
									}
									$total_lib_eq = 0;
									
									$k = 0;
									foreach($sum_lib_arr as $lib_row){
										if($lib_row['biller_id'] == $in_bill_id1 && ($lib_row['biller_id'] == $sum_eq_arr[$k]['biller_id'])){
											$total_lib_eq += $lib_row['amount'] + $sum_eq_arr[$k]['amount'];
										}
										$k++;
									}
									if($total_lib_eq<0){
										$total_lib_eq = '( '.number_format(abs($total_lib_eq),2).' )';
									}else{
										$total_lib_eq = number_format(abs($total_lib_eq),2);
									}
									echo '<td><div class="fix-text text-right"><b>'. $total_lib_eq .'</b></div></td>';
								}
								?>
								
								<td><div class="fix-text text-right"><b><?php echo number_format(abs($end_total_lib) + $end_total_eq,2);?></b></div></td>
								<!--<td><strong><span class="pull-right blightOrange"><?php echo number_format($total_liability + ($total_equity-$total_retained),2);?></span></strong></td>-->
							</tr>
                        </tbody>

						<!--
                        <tfoot class="dtFilter">
                        <tr class="active">                            
                            <th colspan="<?=$col1?>"><?= lang("Total ASSET = LIABILITIES + EQUITY"); ?></th>
                            <th><span class="pull-right"><?php echo number_format(abs($total_equity+$total_liability-$total_retained)-abs($total_asset),2);?></span></th>

                        </tr>
                        </tfoot>
						-->
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="<?= $assets ?>js/html2canvas.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
		$("#biller-filter").on('click', function(event){
			event.preventDefault();
			var hasCheck = false;
			biller_ids = '';
			$.each($("input[name='biller_checkbox[]']:checked"), function(){
				hasCheck = true;
				biller_ids += $(this).val() + '-';
			});
			var billers = removeSymbolLastString(biller_ids, '-');
			if(hasCheck == true){
				var encodedName = encodeURIComponent(billers);
				var url = "<?php echo site_url('reports/balance_sheet/'.$start.'/'.$end.'/0/0') ?>" + '/' + encodeURIComponent(billers);
				window.location.href = "<?=site_url('reports/balance_sheet/'. $start .'/'.$end.'/0/0/')?>" + '/' + encodedName;
			}
			
			if(hasCheck == false){
				bootbox.alert('Please select project first!');
				return false;
			}
			return false;
		});
		
        $('#pdf').click(function (event) {
            event.preventDefault();
            window.location.href = "<?=site_url('reports/balance_sheet/'. $start .'/'.$end.'/pdf/0/'.$biller_id)?>";
            return false;
        });
		$('#xls').click(function (event) {
            event.preventDefault();
            window.location.href = "<?=site_url('reports/balance_sheet/'. $start .'/'.$end.'/0/xls/'.$biller_id)?>";
            return false;
        });
        $('#image').click(function (event) {
            event.preventDefault();
            html2canvas($('.box'), {
                onrendered: function (canvas) {
                    var img = canvas.toDataURL()
                    window.open(img);
                }
            });
            return false;
        });
    });
	
	function removeSymbolLastString(string, symbol = ','){
		var strVal = $.trim(string);
		var lastChar = strVal.slice(-1);
		if (lastChar == symbol) {
			strVal = strVal.slice(0, -1);
		}
		return strVal;
	}
	
</script>