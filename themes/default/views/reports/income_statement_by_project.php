<script>$(document).ready(function () {
        CURI = '<?= site_url('reports/income_statement_by_project'); ?>';
    });</script>
<style>@media print {
        .fa {
            color: #EEE;
            display: none;
        }

        .small-box {
            border: 1px solid #CCC;
        }
    }

</style>

<?php if ($Owner) {
    echo form_open('reports/actions_income_statement_by_project', 'id="action-form"');
} ?>
<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-bars"></i><?= lang('income_by_project'); ?></h2>
		
		<div class="box-icon">
            <div class="form-group choose-date hidden-xs">
                <div class="controls">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                        <input type="text" value="<?= ($start ? $this->erp->hrld($start) : '') . ' - ' . ($end ? $this->erp->hrld($end) : ''); ?>"
                               id="daterange" class="form-control">
                        <span class="input-group-addon"><i class="fa fa-chevron-down"></i></span>
                    </div>
                </div>
            </div>
        </div>
		
        <div class="box-icon">
            <ul class="btn-tasks">
				<li class="dropdown"><a href="#" id="xls" data-action="export_excel" class="tip" title="<?= lang('download_excel') ?>"><i
                            class="icon fa fa-file-excel-o"></i></a></li>
                <li class="dropdown"><a href="#" id="pdf" data-action="export_pdf" class="tip" title="<?= lang('download_pdf') ?>"><i
                            class="icon fa fa-file-pdf-o"></i></a></li>
                <li class="dropdown"><a href="#" id="image" class="tip" title="<?= lang('save_image') ?>"><i
                            class="icon fa fa-file-picture-o"></i></a></li>
				<li class="dropdown">
					<a data-toggle="dropdown" class="dropdown-toggle" href="#"><i
							class="icon fa fa-building-o tip" data-placement="left"
							title="<?= lang("billers") ?>"></i></a>
					<ul class="dropdown-menu pull-right" class="tasks-menus" role="menu"
						aria-labelledby="dLabel">
						<li><a href="<?= site_url('reports/income_statement_by_project') ?>"><i
									class="fa fa-building-o"></i> <?= lang('billers') ?></a></li>
						<li class="divider"></li>
						<?php
						$b_sep = 0;
						foreach ($billers as $biller) {
							$biller_sep = explode('-', $this->uri->segment(7));
							if($biller_sep[$b_sep] == $biller->id){
								echo '<li ' . ($biller_id && $biller_id == $biller->id ? 'class="active"' : '') . '>&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" name="biller_checkbox[]" class="checkbox biller_checkbox" checked value="'. $biller->id .'" >&nbsp;&nbsp;' . $biller->company . '</li>';
								echo '<li class="divider"></li>';
								$b_sep++;
							}else{
								echo '<li ' . ($biller_id && $biller_id == $biller->id ? 'class="active"' : '') . '>&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" name="biller_checkbox[]" class="checkbox biller_checkbox" value="'. $biller->id .'" >&nbsp;&nbsp;' . $biller->company . '</li>';
								echo '<li class="divider"></li>';
							}
							//echo '<li ' . ($biller_id && $biller_id == $biller->id ? 'class="active"' : '') . '><a href="' . site_url('reports/balance_sheet/'.$start.'/'.$end.'/0/0/' . $biller->id) . '"><input type="checkbox" class="checkbox biller_checkbox" value="'. $biller->id .'" >&nbsp;&nbsp;' . $biller->company . '</a></li>';
						}
						?>
						<li class="text-center"><a href="#" id="biller-filter" class="btn btn-primary"><?=lang('submit')?></a></li>
					</ul>
                </li>
            </ul>
        </div>

    </div>
	<?php if ($Owner) { ?>
    <div style="display: none;">
        <input type="hidden" name="form_action" value="" id="form_action"/>
        <?= form_submit('performAction', 'performAction', 'id="action-form-submit"') ?>
    </div>
    <?php echo form_close(); ?>
<?php } ?>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">

                <p class="introtext"><?= lang('list_results'); ?></p>
				<!--
				<div id="form">

                    <?php echo form_open("reports/income_statement_by_project"); ?>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label" for="biller"><?= lang("biller"); ?></label>
                                <?php
                                $bl[''] = lang("all");
                                foreach ($billers as $biller) {
                                    $bl[$biller->id] = $biller->company != '-' ? $biller->company : $biller->name;
                                }
                                echo form_dropdown('biller', $bl, (isset($_POST['biller']) ? $_POST['biller'] : ""), 'class="form-control" id="biller" data-placeholder="' . $this->lang->line("select") . " " . $this->lang->line("biller") . '"');
                                ?>
                            </div>
                        </div>
						
						<div class="col-sm-4">
                            <div class="form-group">
                                <?= lang("year", "year"); ?>
                                <?php echo form_input('year', (isset($_POST['year']) ? $_POST['year'] : $year), 'class="form-control number_only date-year" id="year"'); ?>
                            </div>
                        </div>
						
                    </div>
                    <div class="form-group">
                        <div class="controls"> <?php echo form_submit('submit_report', $this->lang->line("submit"), 'class="btn btn-primary"'); ?> </div>
                    </div>
                    <?php echo form_close(); ?>

                </div>
				-->
                <div class="table-scroll">
					<table class="table table-bordered">
						<thead>
							<tr>
								<th style="text-align:center;" colspan="20"><?php echo $year?></th>
							</tr>
							<tr class="primary">
								<th class="text-center"><div class="fix-text"><?= lang('Code');?></div></th>
								<th class="text-center"><div class="fix-text"><?= lang('Project');?></div></th>
								<th class="text-center"><div class="fix-text"><?= lang('Amount');?></div></th>
								<th class="text-center"><div class="fix-text"><?= lang('Period');?></div></th>
								<th class="text-center"><div class="fix-text"><?= lang('Start_Date');?></div></th>
								<th class="text-center"><div class="fix-text"><?= lang('End_Date');?></div></th>
								<th style="text-align:left;"><div class="fix-text-moth"><?= lang("Jan"); ?></div></th>
								<th style="text-align:left;"><div class="fix-text-moth"><?= lang("Feb"); ?></div></th>
								<th style="text-align:left;"><div class="fix-text-moth"><?= lang("Mar"); ?></div></th>
								<th style="text-align:left;"><div class="fix-text-moth"><?= lang("Apr"); ?></div></th>
								<th style="text-align:left;"><div class="fix-text-moth"><?= lang("May"); ?></div></th>
								<th style="text-align:left;"><div class="fix-text-moth"><?= lang("Jun"); ?></div></th>
								<th style="text-align:left;"><div class="fix-text-moth"><?= lang("Jul"); ?></div></th>
								<th style="text-align:left;"><div class="fix-text-moth"><?= lang("Aug"); ?></div></th>
								<th style="text-align:left;"><div class="fix-text-moth"><?= lang("Sep"); ?></div></th>
								<th style="text-align:left;"><div class="fix-text-moth"><?= lang("Oct"); ?></div></th>
								<th style="text-align:left;"><div class="fix-text-moth"><?= lang("Nov"); ?></div></th>
								<th style="text-align:left;"><div class="fix-text-moth"><?= lang("Dec"); ?></div></th>
								<th><div class="fix-text">Total</div></th>
								<th><div class="fix-text">Balance</div></th>
							</tr>
						</thead>
	
						<tbody>
								<?php 
								$sum_jan = 0;
								$sum_feb = 0;
								$sum_mar = 0;
								$sum_apr = 0;
								$sum_may = 0;
								$sum_jun = 0;
								$sum_jul = 0;
								$sum_aug = 0;
								$sum_sep = 0;
								$sum_oct = 0;
								$sum_nov = 0;
								$sum_dec = 0;
								$sum_total = 0;
								$sum_balance = 0;
								$sum_amount = 0;
								foreach ($monthlyIncomes->result() as $project) {
								?>
								<tr>
									<td><div class="fix-text"><?=$project->code?></div></td>
									<td><div class="fix-text"><?=$project->company?></div></td>
									<td><div class="fix-text"><?=number_format($project->total_amount)?></div></td>
									<td><div class="fix-text"><?=$project->period?></div></td>
									<td><div class="fix-text"><?=$this->erp->hrsd($project->start_date)?></div></td>
									<td style="border-right:1px solid #000;">
										<div class="fix-text"><?=$this->erp->hrsd($project->end_date)?></div>
									</td>
									<td><div class="fix-text-moth"><?=number_format(abs($project->jan), 2)?></div></td>
									<td><div class="fix-text-moth"><?=number_format(abs($project->feb), 2)?></div></td>
									<td><div class="fix-text-moth"><?=number_format(abs($project->mar), 2)?></div></td>
									<td><div class="fix-text-moth"><?=number_format(abs($project->apr), 2)?></div></td>
									<td><div class="fix-text-moth"><?=number_format(abs($project->may), 2)?></div></td>
									<td><div class="fix-text-moth"><?=number_format(abs($project->jun), 2)?></div></td>
									<td><div class="fix-text-moth"><?=number_format(abs($project->jul), 2)?></div></td>
									<td><div class="fix-text-moth"><?=number_format(abs($project->aug), 2)?></div></td>
									<td><div class="fix-text-moth"><?=number_format(abs($project->sep), 2)?></div></td>
									<td><div class="fix-text-moth"><?=number_format(abs($project->oct), 2)?></div></td>
									<td><div class="fix-text-moth"><?=number_format(abs($project->nov), 2)?></div></td>
									<td><div class="fix-text-moth"><?=number_format(abs($project->dec), 2)?></div></td>
									<td><div class="fix-text-moth"><?=number_format(abs($project->total), 2)?></div></td>
									<td><div class="fix-text-moth"><?=number_format($project->total_amount - abs($project->total), 2)?></div></td>
								</tr>
								<?php 
								$sum_amount += abs($project->total_amount);
								$sum_jan += abs($project->jan);
								$sum_feb += abs($project->feb);
								$sum_mar += abs($project->mar);
								$sum_apr += abs($project->apr);
								$sum_may += abs($project->may);
								$sum_jun += abs($project->jun);
								$sum_jul += abs($project->jul);
								$sum_aug += abs($project->aug);
								$sum_sep += abs($project->sep);
								$sum_oct += abs($project->oct);
								$sum_nov += abs($project->nov);
								$sum_dec += abs($project->dec);
								$sum_total += abs($project->total);
								$sum_balance += $project->total_amount - abs($project->total);
								} ?>
							<tr>
								
							</tr>
							<tr class="active">                            
								<th><div class="fix-text-moth"></div></th>
								<th><div class="fix-text-moth"></div></th>
								<th><div class="fix-text"><?=number_format($sum_amount,2)?></div></th>
								<th colspan="3"></th>
								<th><div class="fix-text-moth"><?=number_format($sum_jan,2)?></div></th>
								<th><div class="fix-text-moth"><?=number_format($sum_feb,2)?></div></th>
								<th><div class="fix-text-moth"><?=number_format($sum_mar,2)?></div></th>
								<th><div class="fix-text-moth"><?=number_format($sum_apr,2)?></div></th>
								<th><div class="fix-text-moth"><?=number_format($sum_may,2)?></div></th>
								<th><div class="fix-text-moth"><?=number_format($sum_jun,2)?></div></th>
								<th><div class="fix-text-moth"><?=number_format($sum_jul,2)?></div></th>
								<th><div class="fix-text-moth"><?=number_format($sum_aug,2)?></div></th>
								<th><div class="fix-text-moth"><?=number_format($sum_sep,2)?></div></th>
								<th><div class="fix-text-moth"><?=number_format($sum_oct,2)?></div></th>
								<th><div class="fix-text-moth"><?=number_format($sum_nov,2)?></div></th>
								<th><div class="fix-text-moth"><?=number_format($sum_dec,2)?></div></th>
								<th><div class="fix-text"><?=number_format($sum_total,2)?></div></th>
								<th><div class="fix-text"><?=number_format($sum_balance,2)?></div></th>
							</tr>
						</tbody>
					</table>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="<?= $assets ?>js/html2canvas.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
		
		$("#biller-filter").on('click', function(event){
			event.preventDefault();
			var hasCheck = false;
			biller_ids = '';
			$.each($("input[name='biller_checkbox[]']:checked"), function(){
				hasCheck = true;
				biller_ids += $(this).val() + '-';
			});
			var billers = removeSymbolLastString(biller_ids, '-');
			if(hasCheck == true){
				var encodedName = encodeURIComponent(billers);
				var url = "<?php echo site_url('reports/income_statement_by_project/'.$start.'/'.$end.'/0/0') ?>" + '/' + encodeURIComponent(billers);
				window.location.href = "<?=site_url('reports/income_statement_by_project/'. $start .'/'.$end.'/0/0/')?>" + '/' + encodedName;
			}
			
			if(hasCheck == false){
				bootbox.alert('Please select project first!');
				return false;
			}
			return false;
		});
		
		$(document).on('focus','.date-year', function(t) {
			$(this).datetimepicker({
				format: "yyyy",
				startView: 'decade',
				minView: 'decade',
				viewSelect: 'decade',
				autoclose: true,
			});
		});
		
        $('#pdf').click(function (event) {
            event.preventDefault();
            window.location.href = "<?=site_url('reports/income_statement_by_project/'.$start. '/' . $end . '/pdf/0/'.$biller_id_no_sep)?>";
            return false;
        });
		
		$('#xls').click(function (event) {
            event.preventDefault();
			window.location.href = "<?=site_url('reports/income_statement_by_project/'.$start. '/' . $end . '/0/xls/'.$biller_id_no_sep)?>";
			
            return false;
        });
		
        $('#image').click(function (event) {
            event.preventDefault();
            html2canvas($('.box'), {
                onrendered: function (canvas) {
                    var img = canvas.toDataURL()
                    window.open(img);
                }
            });
            return false;
        });
    });
	
	function removeSymbolLastString(string, symbol = ','){
		var strVal = $.trim(string);
		var lastChar = strVal.slice(-1);
		if (lastChar == symbol) {
			strVal = strVal.slice(0, -1);
		}
		return strVal;
	}
</script>