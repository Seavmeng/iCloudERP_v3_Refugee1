<?php
$v = "";

if ($this->input->post('sproduct')) {
    $v .= "&product=" . $this->input->post('sproduct');
}
if ($this->input->post('category')) {
    $v .= "&category=" . $this->input->post('category');
}
if ($this->input->post('start_date')) {
    $v .= "&start_date=" . $this->input->post('start_date');
}
if ($this->input->post('end_date')) {
    $v .= "&end_date=" . $this->input->post('end_date');
}
if ($this->input->post('cf1')) {
    $v .= "&cf1=" . $this->input->post('cf1');
}
if ($this->input->post('cf2')) {
    $v .= "&cf2=" . $this->input->post('cf2');
}
if ($this->input->post('cf3')) {
    $v .= "&cf3=" . $this->input->post('cf3');
}
if ($this->input->post('cf4')) {
    $v .= "&cf4=" . $this->input->post('cf4');
}
if ($this->input->post('cf5')) {
    $v .= "&cf5=" . $this->input->post('cf5');
}
if ($this->input->post('cf6')) {
    $v .= "&cf6=" . $this->input->post('cf6');
}
?>
<style type="text/css" media="screen">
    #PRData td:nth-child(6), #PRData td:nth-child(7) {
        text-align: right;
    }
    <?php if($Owner || $Admin || $this->session->userdata('show_cost')) { ?>
    #PRData td:nth-child(8) {
        text-align: right;
    }
    <?php } ?>
</style>
<script>
    /*var oTable;
    $(document).ready(function () {
        oTable = $('#PRData').dataTable({
            "aaSorting": [[2, "asc"], [3, "asc"]],
			//"bSort": false,
            "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?= lang('all') ?>"]],
            "iDisplayLength": <?= $Settings->rows_per_page ?>,
            'bProcessing': true, 'bServerSide': true,
			"bStateSave": true,
			"fnStateSave": function (oSettings, oData) {
				localStorage.setItem('DataTables_' + window.location.pathname, JSON.stringify(oData));
			},
			"fnStateLoad": function (oSettings) {
				var data = localStorage.getItem('DataTables_' + window.location.pathname);
				return JSON.parse(data);
			},
            'sAjaxSource': '<?= site_url('products/getProducts'.($warehouse_id ? '/'.$warehouse_id : '').'/?v=1'.$v) ?>',
            'fnServerData': function (sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?= $this->security->get_csrf_token_name() ?>",
                    "value": "<?= $this->security->get_csrf_hash() ?>"
                });
                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
            },
            'fnRowCallback': function (nRow, aData, iDisplayIndex) {
                var oSettings = oTable.fnSettings();
                nRow.id = aData[0];
                nRow.className = "product_link";
                //if(aData[7] > aData[9]){ nRow.className = "product_link warning"; } else { nRow.className = "product_link"; }
                return nRow;
            },
            "aoColumns": [
                {"bSortable": false, "mRender": checkbox}, {
                    "bSortable": false,
                    "mRender": img_hl
                }, null, null, null,null, <?php if($Owner || $Admin){ echo '{"mRender": currencyFormat}, {"mRender": currencyFormat},'; } else { if($GP['products-cost']){if($this->session->userdata('show_cost')){ echo '{"mRender": currencyFormat},';}}if($GP['products-price']){if($this->session->userdata('show_price')) { echo '{"mRender": currencyFormat},'; }} } ?> {"mRender": formatQuantity}, null, <?php if(!$warehouse_id || !$Settings->racks) { echo '{"bVisible": false},'; } else { echo '{"bSortable": true},'; } ?> {"mRender": formatQuantity}, {"bSortable": false}
            ],
			"aoColumnDefs": [
			  { "bSearchable": false, "aTargets": [8] }
			],
        }).fnSetFilteringDelay().dtFilter([
            {column_number: 2, filter_default_label: "[<?=lang('product_code');?>]", filter_type: "text", data: []},
            {column_number: 3, filter_default_label: "[<?=lang('product_name');?>]", filter_type: "text", data: []},
            {column_number: 4, filter_default_label: "[<?=lang('category');?>]", filter_type: "text", data: []},
			{column_number: 5, filter_default_label: "[<?=lang('sub_category');?>]", filter_type: "text", data: []},
            <?php $col = 5;
            if($Owner || $Admin) {
				if($GP['products-cost']){
					echo '{column_number : 6, filter_default_label: "['.lang('product_cost').']", filter_type: "text", data: [] },';
				}
				if($GP['products-price']){
					echo '{column_number : 7, filter_default_label: "['.lang('product_price').']", filter_type: "text", data: [] },';
				}
                $col += 2;
            } else {
				
                if($this->session->userdata('show_cost')) { $col++; echo '{column_number : '.$col.', filter_default_label: "['.lang('product_cost').']", filter_type: "text", data: [] },'; }
                if($this->session->userdata('show_price')) { $col++; echo '{column_number : '.$col.', filter_default_label: "['.lang('product_price').']", filter_type: "text, data: []" },'; }
            }
            ?>
            {column_number: <?php $col++; echo $col; ?>, filter_default_label: "[<?=lang('quantity');?>]", filter_type: "text", data: []},
            {column_number: <?php $col++; echo $col; ?>, filter_default_label: "[<?=lang('product_unit');?>]", filter_type: "text", data: []},
            <?php if($warehouse_id && $Settings->racks) { $col++; echo '{column_number : '. $col.', filter_default_label: "['.lang('rack').']", filter_type: "text", data: [] },'; } ?>
            {column_number: <?php $col++; echo $col; ?>, filter_default_label: "[<?=lang('alert_quantity');?>]", filter_type: "text", data: []},
        ], "footer");

    });*/
</script>
<?php 
if ($Owner) {
    echo form_open('reports/inventory_valuation'.($warehouse_id ? '/'.$warehouse_id : ''), 'id="action-form"');
} 
?>
<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-barcode"></i><?= lang('inventory_valuation_detail') ; ?>
        </h2>
		<!--<div class="box-icon">
            <ul class="btn-tasks">
                <li class="dropdown">
                    <a href="javascript:void(0);" class="toggle_up tip" title="<?= lang('hide_form') ?>">
                        <i class="icon fa fa-toggle-up"></i>
                    </a>
                </li>
                <li class="dropdown">
                    <a href="javascript:void(0);" class="toggle_down tip" title="<?= lang('show_form') ?>">
                        <i class="icon fa fa-toggle-down"></i>
                    </a>
                </li>
            </ul>
        </div>-->
        <div class="box-icon">
            <ul class="btn-tasks">
                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#"><i class="icon fa fa-tasks tip" data-placement="left" title="<?= lang("actions") ?>"></i></a>
                    <ul class="dropdown-menu pull-right" class="tasks-menus" role="menu" aria-labelledby="dLabel">
                        <!--<li>
							<a href="<?= site_url('products/add') ?>">
								<i class="fa fa-plus-circle"></i> <?= lang('add_product') ?>
							</a>
						</li>-->

                        
                    </ul>
                </li>
                
            </ul>
        </div>

    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">

                <p class="introtext"><?= lang('list_results'); ?></p>
                <div id="form">

                </div>

                <div class="clearfix"></div>

                <div class="table-responsive">
                    <table id="" class="table table-condensed table-bordered">
                        <thead>
                        <tr class="primary">
                            <th></th>
							<th>Type</th>
							<th>Date</th>
							<th>Name</th>
							<th>Num</th>
							<th>Class</th>
							<th>Qty</th>
							<th>Cost</th>
							<th>On Hand</th>
							<th>Avg Cost</th>
							<th>Asset Value</th>
                        </tr>
                        </thead>
                        <tbody>
							<?php foreach($warehouses as $warehouse){
							?>
							<tr>
								<td style="border-bottom:none;"><h3 class="text-right"><b><?=$warehouse->warehouse?></b></h3></td>
								<td colspan="10"></td>
							</tr>
							
							<?php 
							$categories = $this->reports_model->getCategoriesInventoryValuationByWarehouse($warehouse->warehouse_id);
							foreach($categories AS $category){ ?>
							<tr>
								<td style="border-top:none;border-bottom:none;"><h5 class="text-right"><b><?=$category->category_name?></b></h5></td>
								<td colspan="10"></td>
							</tr>
							
							
							<tr>
								<td style="border-bottom:none;border-top:none;"><h5 class="text-right"><b>Product Code</b></h5></td>
								<td colspan="10"></td>
							</tr>

							<tr>
								<td style="border-top:none;border-bottom:none;"></td>
								<td>PURCHASES</td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
							</tr>
							
							<tr>
								<td style="border-top:none;border-bottom:none;"><h5 class="text-right"><b>Total Product Code</b></h5></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td class="text-right"><b>2.00</b></td>
								<td></td>
								<td class="text-right"><b>10.00</b></td>
							</tr>
							<?php } ?>
							
							<?php } ?>
							
                        </tbody>

                        <tfoot class="dtFilter">
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $('#form').hide();
    $('.toggle_down').click(function () {
        $("#form").slideDown();
        return false;
    });
    $('.toggle_up').click(function () {
        $("#form").slideUp();
        return false;
    });
	$(document).ready(function(){
		/*
		$("#excel").click(function(e){
			e.preventDefault();
			window.location.href = "<?=site_url('products/getProductAll/0/xls/')?>";
			return false;
		});
		$('#pdf').click(function (event) {
            event.preventDefault();
            window.location.href = "<?=site_url('products/getProductAll/pdf/?v=1'.$v)?>";
            return false;
        });
		*/
		$('body').on('click', '#multi_adjust', function() {
			 if($('.checkbox').is(":checked") === false){
				alert('Please select at least one.');
				return false;
			}
			var arrItems = [];
			$('.checkbox').each(function(i){
				if($(this).is(":checked")){
					if(this.value != ""){
						arrItems[i] = $(this).val();   
					}
				}
			});
			$('#myModal').modal({remote: '<?=base_url('products/multi_adjustment');?>?data=' + arrItems + ''});
			$('#myModal').modal('show');
        });
		$('#excel').on('click', function(e){
			e.preventDefault();
			if ($('.checkbox:checked').length <= 0) {
				window.location.href = "<?=site_url('products/getProductAll/0/xls/')?>";
				return false;
			}
		});
		$('#pdf').on('click', function(e){
			e.preventDefault();
			if ($('.checkbox:checked').length <= 0) {
				window.location.href = "<?=site_url('products/getProductAll/pdf/?v=1'.$v)?>";
				return false;
			}
		});
	});
</script>
<?php if ($Owner) { ?>
    <div style="display: none;">
        <input type="hidden" name="form_action" value="" id="form_action"/>
        <?= form_submit('performAction', 'performAction', 'id="action-form-submit"') ?>
    </div>
    <?= form_close() ?>
<?php } ?>
