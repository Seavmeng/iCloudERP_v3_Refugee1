
	
<div class="modal-dialog modal-lg"> 
	<div class="modal-content">
		<div class="modal-header">				
			<button type="button" class="close" data-dismiss="modal">
				<span aria-hidden="true"><i class="fa fa-2x">&times;</i></span>
				<span class="sr-only"><?=lang('close');?></span>
			</button>
			<h4 class="modal-title" id="payModalLabel"><?= lang('add_appointment_refugee'); ?></h4>
		</div>
		<?php  echo form_open_multipart("applications/add_appointment_refugee/".$id); ?>
		<div class="modal-body"> 
			<div class="col-sm-12">
					<div class="form-group">
						<?php echo lang('family_members_travel', 'family_members_travel'); ?>​ 
						<span class="red">*</span>
						<div class="controls">
							<table width="50%" class="table table-bordered" style='white-space:nowrap;'>
								<tr>
									<th></td>
									<th><?= lang("member_name") ?></th>
									<th><?= lang("gender") ?></th>
									<th><?= lang("dob") ?></th> 
								</tr> 
								<tbody class="tbody">
								<?php if(!empty($result_members)){?>
									<?php foreach ($result_members as $member){?>
										<tr>
											<td class="text-center"> 
												<input type='checkbox' />
											</td>
											<td>
												<div class="col-sm-6">
													<input type="text" value='<?= set_value('member_name[]',$member->firstname_kh);?>' class="form-control input-sm" value='<?= $member->firstname_kh?>' name="member_name_kh[]" />
												</div>
												 
												<div class="col-sm-6">
													<input type="text" value='<?= set_value('member_name[]',$member->firstname);?>' class="form-control input-sm" name="member_name[]" />
												</div>
											</td>
											<td>
												<select class="input-sm" name="member_gender[]">
													<option value="male" <?= ($member->gender=='male'?'selected':'')?>><?= lang("male"); ?></option>
													<option value="female" <?= ($member->gender=='female'?'selected':'')?>><?= lang("female"); ?></option>
												</select>
											</td>
											<td>
												<input type="text" class="form-control input-sm date"​​ value='<?= set_value('member_date',$this->erp->hrsd($member->dob))?>'  name="member_date[]" />
											</td> 
										</tr>
									<?php }?>
								<?php }?>
								</tbody> 
							</table>  
						</div>
					</div>
				</div> 
			<div class="clearfix"></div>
		</div>
		<div class="modal-footer">
			<?php echo form_submit('submit', lang('submit'), 'class="btn btn-primary"'); ?>
		</div>
		<?php  echo form_close(); ?>
	</div>
</div>

<script type="text/javascript">
	$(function(){
		$("select").select2();
	});
</script> 