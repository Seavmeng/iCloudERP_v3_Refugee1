<?php  	
	$icon_check='<span style="font-size:17px;">&#x2611;</span>';
	$icon_uncheck='<span style="font-size:17px;">&#x2610;</span>'; 
?>
<div class='col-lg-3 director-txt'> 
	<div class='form-group'> 
		<?= lang("នាយកគ្រប់គ្រង៖", "នាយកគ្រប់គ្រង៖");?>
		<select   class="form-control text-cover director">
			<?php foreach($departments as $dep) {?>
			<option value="<?= $dep->department_en ?>"><b><?= $dep->department_kh?></b></option>
			<?php }?>
		</select>
	</div>
</div> 
<div class="clearfix"></div>  
<div class="box a4" >
	<div class="box-content">
		<div class="row text-size">			
			<?php $this->load->view($this->theme."applications/head-cover"); ?>
			<div class="col-sm-12">
				<p>លេខៈ...............</p>
			</div>			
			<div class="col-sm-12 text-center">
				<h2 class='text-cover'>លិខិតកោះហៅសម្ភាសន៍</h2>
				<h3>Appointment For Interview</h3>
			</div>
			<div class="col-sm-12 text-justify​ table-fill"> 
				<table width="30%" style="white-space:nowrap;">
					<tr>
						<td width="140">- &nbsp;<?= lang("លេខករណី") ?> / <?= lang("Case No") ?> : </td>
						<td class='text-english'>
							<b><?=  $this->erp->toKhmer($result->case_prefix." ".$result->case_no) ?>
						</td>
					</tr> 
				</table> 
				<table width="30%" style="white-space:nowrap;">
					<tr>
						<td>-  &nbsp;ជាម្ចាស់ករណី / Principle Applicant: <?php echo ($appointment->member_id == 0 ? $icon_check : $icon_uncheck);?>
						&nbsp;&nbsp;
						ឬ ជាសមាជិកក្នុងបន្ទុក / Dependent Member: <?php echo ($appointment->member_id >= 1 ? $icon_check : $icon_uncheck);?>
						</td> 
					</tr>  
				</table>  
				<table width="100%" style="white-space:nowrap;">
					<tr>
						<td colspan="2">
							- &nbsp;<?= lang("សូមអញ្ចើញលោក លោកស្រី ") ?> / <?= lang("To Invite Mr./Ms") ?> : 
							<?php
								if(!empty($member_dependant)){ 
							?>
								<b><?= $member_dependant->lastname_kh ."  ". $member_dependant->firstname_kh.' / '.$member_dependant->lastname ."  ". $member_dependant->firstname ?></b>
							<?php	
								}else{
							?>
								<b><?= $result->lastname_kh ."  ". $result->firstname_kh.' / '.$result->lastname ."  ". $result->firstname ?></b>
							<?php } ?> 
						</td>

					</tr>
			
					<tr>
						<td>- &nbsp;<?= lang("ថ្ងៃខែឆ្នាំកំណើត ") ?> / <?= lang("DOB") ?> :
					 
							<?php
								if(!empty($member_dependant)){ 
							?>
								<b class='text-english'> &nbsp; <?=  $this->erp->toKhmer($this->erp->hrsd($member_dependant->dob) )?></b> </td>						
							<?php	
								}else{
							?>
								<b class='text-english'> &nbsp; <?=  $this->erp->toKhmer($this->erp->hrsd($result->dob) )?></b> </td>						
							<?php } ?> 
					   
						<td width="130">- &nbsp;<?= lang("សញ្ជាតិ ") ?> / <?= lang("Nationality") ?> : </td>
						<td>
							 
							<?php
								if(!empty($member_dependant)){ 
							?>
								<b><?= $member_dependant->nationality_kh; ?></b>
							<?php	
								}else{
							?>
								<b><?= $result->nationality_kh; ?></b>
							<?php } ?> 
							 
							
						</td>
					</tr>
					<tr>
						<td>
							- &nbsp;<?= lang("កាលបរិច្ឆេទសម្ភាសន៍ ") ?> / <?= lang("Date of Interview") ?> :​​ 
							<b><?= $this->erp->toKhmer($this->erp->hrsd($appointment->appointment_date) )?></b>
                        </td>
						<td width="130">
							- &nbsp;<?= lang("ម៉ោង ") ?> / <?= lang("Time ") ?> : 
							<b><?=  $this->erp->toKhmer(date("H:i",strtotime($appointment->appointment_date)) )?></b>
                        </td>
					</tr>
				</table> 
				<br/>
				<?php if($appointment->member_id == 0){?>
				
				<div class='col-sm-12 text-center' >
					<p class='bold'>សមាជិកគ្រួសារ </p>
					<p><u>Family Members</u></p>
				</div>
			
				<?php if(!empty($result_members)){ ?>
					<table width='50%' style='margin:auto; ' class='table-padding'>
						<tr>
							<td><?= lang("ល.រ") ?></td>
							<td><?= lang("ឈ្មោះ") ?></td>
							<td><?= lang("ភេទ") ?></td>
							<td><?= lang("ថ្ងៃខែឆ្នាំកំណើត") ?></td>
						</tr>
						<?php foreach($result_members as $key => $member){ ?>
							<tr>
								<td><?= $this->erp->toKhmer(($key+1)) ?></td>
								<td>
									<?= $member->lastname_kh.' '.$member->firstname_kh ?>&nbsp;
								</td>
								<td>
									<?= $this->erp->genderToKhmer($member->gender) ?>&nbsp;
								</td>
								<td>
									<?= $this->erp->khmer_date_format($member->dob) ?>&nbsp;
								</td>
							</tr>
						<?php } ?>
					</table>
				<?php } ?>
				<?php } ?>
			
				<table width="100%">
					<tr>
						<td width="140" style="vertical-align: top;">
							<span>- &nbsp;<?= lang("ទីកន្លែងសម្ភាសន៍") ?> : </span>
						</td>
						<td>
							<span><?= $this->erp->toKhmer($office->location_kh )?></span>
						</td>
					</tr> 
					<tr>
						<td style="vertical-align: top; ">
							<span>- &nbsp;<?= lang("Place of Interview") ?> : </span>
						</td>
						<td style="text-justify: inter-word;" class='text-english'>
							<span><?=  $this->erp->toKhmer($office->location )?></span>
						</td>
					</tr>
					<tr>
						<td colspan='2'>
							<p>
								សូមយកមកជាមួយនូវឯកសារភស្តុតាងដែលជាប្រយោជន៏សំរាប់ការសម្ភាសន៍។ <br/>
								Please bring any supporting documents for interview.
							</p>
						</td>
					</tr>
				</table>   
			</div>
			
			<div class="clearfix"></div>
			<br/>  
			<div class='col-sm-12'> 
				<div class="div-left" style="line-height:25px; width:55%;">  
					<p class="bold">
						<i><?= lang("សំគាល់ ")?> / <?= lang("Note") ?></i>
					</p>
					<p> 
						សូមជូនដំណឹងមកយើងខ្ញុំជាមុន    ប្រសិនបើអ្នកពុំអាចមកសម្ភាសន៍
						បានតាមការកំណត់ខាងលើ។  អវត្ដមានក្នុងកិច្ចសម្ភាសន៍ ដោយគ្មាន
						មូលហេតុអាចមានផលអវិជ្ជមាន ចំពោះការស្នើសុំសិទ្ធិជ្រកកោន និង
						ឋានៈជនភៀសខ្លួនរបស់អ្នក ។  
						<br/>Please  notify us in advance if you  could  not  come for interview. The absence without any reason, your application may not be considered.
					</p> 
				</div> 
				<div class="div-right text-center" style='width:45%;'>
					<?= $this->erp->footer_date_khmer('រាជធានីភ្នំពេញ');?>  
					<div class="text-cover director_text_sub"></div>
					<div class="text-cover director_text"  >ប្រធាននាយកដ្ឋានជនភៀសខ្លួន</div>
					<div class="director_text_en">Director of Refugee Department</div> 
				</div> 
			</div>  
		</div> 
		<br/>
		<br/>
	</div>
</div>
<script type='text/javascript'>
	$(".director").on('click',function(){ 
		var txt = $(".director option:selected").text();
		var txt_en = $(".director option:selected").val();
		if (txt=="រដ្ឋលេខាធិការ") {
			$(".director_text_sub").text('ជ.រដ្ឋមន្រ្តី');
		}else{
			$(".director_text_sub").text('');
		}
		$(".director_text").text(txt); 
		$(".director_text_en").text(txt_en); 
	});
</script>

<style type="text/css">  
	@media print{
		.bblack{ background: white !important; }
		.modal-content,  
		.modal-header{ border:none !important; }
		.table { border:1px solid #EEE !important; } 
		.box { padding:5px !important ;  } 
		.director-txt{display:none !important;}
		.box-content{
			padding: 20px !important;
		}
		.box{
			    border: none !important;
		}
		
	} 
	.table {
		font-size:20px !important;
	}
	.table-fill td{
		padding:2px;		
	}
	.thumb{
		border:5px solid #FFF;
		box-shadow:0px 0.5px 0.5px #000;
	}
	hr{
		padding:0px;
		margin:0px;
		border-top:none !important;
		border-bottom:dotted 1px !important;
	} 
	.box{
		border:5px; 
		border-style: double;
		min-height:800px;		
		background: white; 
	}
</style>


