<?php 

	$v = "";
	
	if ($this->input->post('reference_no')) {
		$v .= "&reference_no=" . $this->input->post('reference_no');
	}

	if ($this->input->post('start_date')) {
		$v .= "&start_date=" . $this->input->post('start_date');
	}
	
	if ($this->input->post('end_date')) {
		$v .= "&end_date=" . $this->input->post('end_date');
	}
	
	if ($this->input->post('origin')) {
		$v .= "&origin=" . $this->input->post('origin');
	}
	
	if ($this->input->post('destination')) {
		$v .= "&destination=" . $this->input->post('destination');
	}
	
	if ($this->input->post('slbiller')) {
		$v .= "&slbiller=" . $this->input->post('slbiller');
	}

	if($this->input->post('user')){
		$v .= "&user=" . $this->input->post('user');
	}
	
	if($this->input->post('status')){
		$v .= "&status=" . $this->input->post('status');
	}
	
	$num_rows = $this->shippings->checkRegister()->num_rows();
	
?>
	<script>
		$(document).ready(function () {
			var oTable = $('#applicationTable').dataTable({
				"aaSorting": [[1, "asc"]],
				"aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?= lang('all') ?>"]],
				"iDisplayLength": <?= $Settings->rows_per_page ?>,
				'bProcessing': true, 'bServerSide': true,
				'sAjaxSource': '<?= site_url('shippings/getShipments/?v=1'.$v) ?>',
				'fnServerData': function (sSource, aoData, fnCallback) {
					aoData.push({
						"name": "<?= $this->security->get_csrf_token_name() ?>",
						"value": "<?= $this->security->get_csrf_hash() ?>"
					});
					$.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
				},
				"aoColumns": [
				{"bSortable": false, "mRender": checkbox}, 
				{"mRender" : fld, "sClass" : "center"}, 
				{"sClass" : "center"},
				{"sClass" : "center"},
				{"sClass" : "center"},
				{"sClass" : "center"},
				{"sClass" : "center"},
				{"sClass" : "center"},
				{"mRender" : currencyFormat },
				{"sClass" : "center"},
				{"sClass" : "left"},
				{"mRender" : row_status},
				{"mRender" : row_status},
				{"bSortable": false, "sClass" : "center"}]
				,'fnRowCallback': function (nRow, aData, iDisplayIndex) {
						var oSettings = oTable.fnSettings();						
						var action = $('td:eq(13)', nRow);
						
						if (aData[11] == 'void' || aData[12]=="taken" || aData[12]=="transfer"){
							action.find('.void-sm').remove();
							action.find('.req-void-sm').remove();
							action.find('.delete-sm').remove();
							action.find('.add-sm').remove();
							action.find('.edit-sm').remove();
						}
						
						if(aData[11] == 'request'){
							action.find('.req-void-sm').remove();
							action.find('.delete-sm').remove();
							action.find('.add-sm').remove();
							action.find('.edit-sm').remove();
						}
						
						if (aData[11] == 'change' || aData[11] == 'return'){
							action.find('.add-sm').remove();
							action.find('.edit-sm').remove();
							action.find('.delete-sm').remove();														
						}
						
						if(aData[12] == 'deliveries'){
							action.find('.void-sm').remove();
							action.find('.req-void-sm').remove();
							action.find('.delete-sm').remove();
						}
						
						<?php if($num_rows <= 0){ ?>
							action.find('.add-sm').remove();
						<?php } ?>
						<?php if(!$GP['shipments-add'] && !$this->Owner){ ?>
							action.find('.add-sm').remove();
						<?php } ?>
						<?php if(!$GP['shipments-edit'] && !$this->Owner){ ?>
							action.find('.edit-sm').remove();
						<?php } ?>
						<?php if(!$GP['shipments-delete'] && !$this->Owner){ ?>
							action.find('.delete-sm').remove();
						<?php } ?>
						<?php if(!$GP['shipments-void'] && !$this->Owner){ ?>
							action.find('.void-sm').remove();
						<?php } ?>
						<?php if(!$GP['shipments-requestvoid'] && !$this->Owner){ ?>
							action.find('.req-void-sm').remove();
						<?php } ?>
						<?php if(!$GP['shipments-viewitems'] && !$this->Owner){ ?>
							action.find('.view-sm').remove();
						<?php } ?>
						
						return nRow;
					},
					"fnFooterCallback": function (nRow, aaData, iStart, iEnd, aiDisplay) {
						var total = 0, price = 0, 
							total_kh = 0, price_kh = 0;
						
						for (var i = 0; i < aaData.length; i++) 
						{
							var c = aaData[aiDisplay[i]][8].split("__");
							if(c[1] == "USD"){
								total += parseFloat(aaData[aiDisplay[i]][8]);
							}else if(c[1] == "KHM"){
								total_kh += parseFloat(aaData[aiDisplay[i]][8]);
							}
						}
						
						var nCells = nRow.getElementsByTagName('th');
						nCells[8].innerHTML = "<div class='text-right'>" + formatMoney(total_kh) + "</div>";
					}
				}).fnSetFilteringDelay().dtFilter([            
					{column_number: 1, filter_default_label: "[<?= lang("date") ?>]", filter_type: "text", data: []},
					{column_number: 2, filter_default_label: "[<?= lang("reference_no") ?>]", filter_type: "text", data: []},
					{column_number: 3, filter_default_label: "[<?= lang("reference_no") ?>]", filter_type: "text", data: []},
					{column_number: 4, filter_default_label: "[<?= lang("origin") ?>]", filter_type: "text", data: []},
					{column_number: 5, filter_default_label: "[<?= lang("destination") ?>]", filter_type: "text", data: []},				
					{column_number: 6, filter_default_label: "[<?= lang("phone_sender") ?>]", filter_type: "text", data: []},
					{column_number: 7, filter_default_label: "[<?= lang("phone_receiver") ?>]", filter_type: "text", data: []},
					{column_number: 9, filter_default_label: "[<?= lang("assign_to") ?>]", filter_type: "text", data: []},
					{column_number: 10, filter_default_label: "[<?= lang("description") ?>]", filter_type: "text", data: []},
					{
						column_number: 11, select_type: 'select2',
						select_type_options: {
							placeholder: '<?=lang('status');?>',
							width: '100%',
							minimumResultsForSearch: -1,
							allowClear: true
						},
						data: [
							   {value: 'active', label: '<?=lang('active');?>'},
							   {value: 'void', label: '<?=lang('void');?>'}, 
							   {value: 'change', label: '<?=lang('change');?>'},
							   {value: 'return', label: '<?=lang('return');?>'},
							   {value: 'request', label: '<?=lang('request');?>'}]
					},
					{
						column_number: 12, select_type: 'select2',
						select_type_options: {
							placeholder: '<?=lang('status');?>',
							width: '100%',
							minimumResultsForSearch: -1,
							allowClear: true
						},
						data: [
							   {value: 'deliveries', label: '<?=lang('deliveries');?>'}, 
							   {value: 'received', label: '<?=lang('received');?>'},
							   {value: 'taken', label: '<?=lang('taken');?>'},
							   {value: 'call', label: '<?=lang('call');?>'},
							   {value: 'block', label: '<?=lang('block');?>'},
							   {value: 'transfer', label: '<?=lang('transfer');?>'},
                                                           {value: 'pending', label: '<?=lang('pending');?>'}]
					}
				], "footer");
		});
	</script>
	<?= form_open('shippings/shipments', 'id="action-form"') ?>
<div class="box no-print">
	<div class="box-header">
		<h2 class="blue"><i class="fa-fw fa fa-folder-open"></i><?= lang('shipments'); ?></h2>
		<div class="box-icon">
			<ul class="btn-tasks">
				<li class="dropdown">
					<a href="#" class="toggle_up tip" title="<?= lang('hide_form') ?>">
						<i class="icon fa fa-toggle-up"></i>
					</a>
				</li>
				<li class="dropdown">
					<a href="#" class="toggle_down tip" title="<?= lang('show_form') ?>">
						<i class="icon fa fa-toggle-down"></i>
					</a>
				</li>
			</ul>
		</div>
		<?php if($GP['shipments-export'] || $this->Owner){ ?>
		<div class="box-icon">
			<ul class="btn-tasks">
				<li class="dropdown"><a href="#" id="pdf" data-action="export_pdf" class="tip" title="<?= lang('download_pdf') ?>"><i class="icon fa fa-file-pdf-o"></i></a></li>
				<li class="dropdown"><a href="#" id="xls" data-action="export_excel" class="tip" title="<?= lang('download_xls') ?>"><i class="icon fa fa-file-excel-o"></i></a></li>
			</ul>
		</div>
		<?php } ?>
	</div>
	<div class="box-content">
		<div class="row">
			<div class="col-lg-12">
				<p class="introtext"><?= lang('list_results'); ?></p>

				<?php echo form_open("shippings/shipments", ' method="GET" id="myform"'); ?>
				
				<div class="form">				
					<div class="row">						
						<div class="col-sm-3">
							<div class="form-group">
								<label class="control-label"><?= lang("reference_no"); ?></label>
								<input type="text" name="reference_no" value="<?= $reference_no ?>" class="reference_no form-control" />
							</div>
						</div>
						
						<div class="col-sm-3">
							<div class="form-group">
								<label class="control-label"><?= lang("start_date"); ?></label>
								<input type="text" name="start_date" value="<?= $start_date ?>" class="date form-control" id="date" />
							</div>
						</div>
						
						<div class="col-sm-3">
							<div class="form-group">
								<label class="control-label"><?= lang("end_date"); ?></label>
								<input type="text" name="end_date" value="<?= $end_date ?>" class="date form-control" id="date" />
							</div>
						</div>
						
						<div class="col-sm-3">
							<div class="form-group">
								<label class="control-label"><?= lang("origin"); ?></label>
								<?php
									$locations = array("...");
									foreach($location as $loc){
										$locations[$loc->id] = ($this->Settings->language == 'khmer') ? $loc->location_kh : $loc->location;
									}
									echo form_dropdown('origin', $locations, $origin, 'class="form-control" id="origin"');
								?>
							</div>
						</div>
						
						<div class="col-sm-3">
							<div class="form-group">
								<label class="control-label"><?= lang("destination"); ?></label>
								<?php
									$locations = array("...");
									foreach($location as $loc){
										$locations[$loc->id] = ($this->Settings->language == 'khmer') ? $loc->location_kh : $loc->location;
									}
									
									echo form_dropdown('destination', $locations, $destination, 'class="form-control" id="destination"');
								?>
							</div>
						</div>
						
						<div class="col-sm-3">						
							<div class="form-group">
							<label class="control-label" ><?= lang("biller"); ?></label> 
							<?php
							$bl[""] = array("...");
							foreach ($billers as $biller) {
								$bl[$biller->id] = $biller->company != '-' ? $biller->company : $biller->name;
							}
								echo form_dropdown('biller', $bl, (isset($_POST['biller']) ? $_POST['biller'] : ($this->session->userdata('biller_id') ? $this->session->userdata('biller_id') : $Settings->default_biller)), 'id="slbiller" data-placeholder="' . lang("select") . ' ' . lang("biller") . '" required="required" class="form-control input-tip select" style="width:100%;"');
							?>
							</div>
						</div>
						
						<div class="col-sm-3">
							<div class="form-group">
								<label class="control-label" ><?= lang("user"); ?></label> 
								<?php									
									$user_ = array("...");
									foreach($users as $user){
										$user_[$user->id] = $user->username;
									}									
									echo form_dropdown('user', $user_, $user_id, 'class="form-control" id="user"');
								?>								
							</div>
						</div>
						
						<div class="col-sm-3">
							<div class="form-group">
								<label class="control-label" ><?= lang("status"); ?></label> 
								<?php									
									$status_ = array("...");
									foreach($bStatus as $s){
										$status_[$s->status] = lang($s->status);
									}									
									echo form_dropdown('status', $status_, $status, 'class="form-control"');
								?>								
							</div>
						</div>

					</div>					
					<div class="form-group">
						<div class="controls"> 
							<input type="submit" value="<?= lang('cancel') ?>" class="btn btn-danger">
							<input type="submit" value="<?= lang('search') ?>" class="btn btn-primary">
							<?php if($GP['shipments-export'] || $this->Owner){ ?>
							<input type="submit" name="export" value="<?= lang('export') ?>" class="btn btn-danger input-xs">
							<?php } ?>
						</div>
					</div>					
				</div>
				
				<?php echo form_close(); ?>

				<div class="table-responsive">
					<table id="applicationTable" class="table table-condensed table-bordered table-hover table-striped">
						<thead>
							<tr>
								<th style="min-width:30px; width: 30px; text-align: center;">
									<input class="checkbox checkth" type="checkbox" name="check"/>
								</th>
								<th style="min-width:150px;"><?= $this->lang->line("date"); ?></th>
								<th style="min-width:150px;"><?= $this->lang->line("reference_no"); ?></th>
								<th style="min-width:150px;"><?= $this->lang->line("do_reference_no"); ?></th>
								<th style="min-width:150px;"><?= $this->lang->line("origin"); ?></th>
								<th style="min-width:150px;"><?= $this->lang->line("destination");?> </th>                               
								<th style="min-width:150px;"><?= $this->lang->line("phone_sender");?> </th>
								<th style="min-width:150px;"><?= $this->lang->line("phone_receiver");?> </th>
								<th style="min-width:150px;"><?= $this->lang->line("price_shipping");?> </th>
								<th style="min-width:120px;"><?= $this->lang->line("assign_to");?></th>
								<th style="min-width:120px;"><?= $this->lang->line("description");?></th>
								<th style="min-width:120px;"><?= $this->lang->line("status");?></th>
								<th style="min-width:120px;"><?= $this->lang->line("shipping_status");?></th>
								<th style="min-width:120px;"><?= $this->lang->line("actions"); ?></th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td colspan="5" class="dataTables_empty">
									<?= lang('loading_data_from_server') ?>
								</td>
							</tr>
						</tbody>
						
						
						<tfoot class="dtFilter">
							<tr class="active">
								<th style="min-width:30px; width: 30px; text-align: center;">
									<input class="checkbox checkft" type="checkbox" name="check"/>
								</th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th style="width:100px; text-align: center;"></th>
							</tr>
						</tfoot>
						
					</table>
				</div>
			</div>
		</div>
	</div>
</div>	
<div style="display: none;">
    <input type="hidden" name="form_action" value="" id="form_action"/>
    <?= form_submit('submit', 'submit', 'id="action-form-submit"') ?>
</div>

<?= form_close() ?>

<style type="text/css">
	#applicationTable, #applicationTable1{ 
		white-space: nowrap; 
		overflow-x: scroll; 
		width:100%;
		display:block;
	}
</style>

<script language="javascript">
    $(document).ready(function () {        
        $('#delete').click(function (e) {
            e.preventDefault();
            $('#form_action').val($(this).attr('data-action'));
            $('#action-form-submit').trigger('click');
        });
        $('#pdf').click(function (event) {
            event.preventDefault();
            window.location.href = "<?=site_url('shippings/export_shipments/?&pdf=pdf'.$v)?>";
            return false; 
        });
        $('#xls').click(function (event) {
            event.preventDefault();
            window.location.href = "<?=site_url('shippings/export_shipments/?xls=xls'.$v)?>";
            return false;
        });
		
        $('.toggle_down').click(function () {
            $(".form").slideDown();
            return false;
        });
		
        $('.toggle_up').click(function () {
            $(".form").slideUp();
            return false;
        });
		
    });
</script>
<div id="shipment_html"></div>




