<ul id="myTab" class="nav nav-tabs">

	<li class=""><a href="#content_1" class="tab-grey"><?= lang("interview_refugees"); ?></a></li>
	
</ul>
	<?php
		$v="";
		if($this->input->get("id")){
			$v .="&id=".$this->input->get("id");
		} 

		if($this->input->get("type")) {
			$v .= "&type=" . $this->input->get("type"); 
		}
		
		if($this->input->post("date_from")) {
			$v .= "&date_from=" . $this->input->post("date_from");
			$date_from = $this->input->post("date_from");
		}

		if($this->input->post("date_to")) {
			$v .= "&date_to=" . $this->input->post("date_to");
			$date_to = $this->input->post("date_to");
		}
		if($this->input->post("case_no")) {
			$v .= "&case_no=" . $this->input->post("case_no");
			$case_no = $this->input->post("case_no");
		}
		
	?>
	<div class="tab-content">
		
		<div id="content_1" class="tab-pane fade in">
			
			<script>
				$(document).ready(function () {
					var oTable = $('#dataTable2').dataTable({
						"aaSorting": [[1, "asc"]],
						"aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?= lang('all') ?>"]],
						"iDisplayLength": <?= $Settings->rows_per_page ?>,
						'bProcessing': true, 'bServerSide': true,
						'sAjaxSource': "<?= site_url('applications/getInterviewRefugees/?v=1'.$v) ?>",
						'fnServerData': function (sSource, aoData, fnCallback) {
							aoData.push({
								"name": "<?= $this->security->get_csrf_token_name() ?>",
								"value": "<?= $this->security->get_csrf_hash() ?>"
							});
							$.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback });
						},
						"aoColumns": [
						{"bSortable": false, "mRender": checkbox}, 			
						{"sClass" : "center"},
						{"sClass" : "center"},
						{"sClass" : "center"},	
						{"sClass" : "center", "mRender": fld},
						{"sClass" : "center"},
						{"sClass" : "center"},
						{"sClass" : "center", "mRender" : fld}, 
						{"mRender" : row_status},
						{"bSortable": false, "sClass" : "center"}]
						,'fnRowCallback': function (nRow, aData, iDisplayIndex) {
								var oSettings = oTable.fnSettings();
								nRow.id = aData[0];
								var action = $('td:eq(9)', nRow); 
								if(aData[8] == 'approved'){
									action.find('.evalue-it').remove();
									action.find('.edit-it').remove();
									action.find('.delete-it').remove();
								}
								if(aData[8] == 'inactive'){
									action.find('.evalue-it').remove();
									action.find('.edit-it').remove();
									action.find('.delete-it').remove();
								}
								return nRow;
							},
							"fnFooterCallback": function (nRow, aaData, iStart, iEnd, aiDisplay) {
								var total = 0, total_kh = 0;
								for (var i = 0; i < aaData.length; i++) 
								{
								}
								var nCells = nRow.getElementsByTagName('th');					
							}
						}).fnSetFilteringDelay().dtFilter([            				
							{column_number: 0, filter_default_label: "[<?= lang("#") ?>]", filter_type: "text", data: []},
							{column_number: 1, filter_default_label: "[<?= lang("case_no") ?>]", filter_type: "text", data: []},
							{column_number: 2, filter_default_label: "[<?= lang("firstname") ?>]", filter_type: "text", data: []},
							{column_number: 3, filter_default_label: "[<?= lang("lastname") ?>]", filter_type: "text", data: []},
							{column_number: 4, filter_default_label: "[<?= lang("dob") ?>]", filter_type: "text", data: []},								
							{column_number: 5, filter_default_label: "[<?= lang("gender") ?>]", filter_type: "text", data: []},
							{column_number: 6, filter_default_label: "[<?= lang("nationality") ?>]", filter_type: "text", data: []},
							{column_number: 7, filter_default_label: "[<?= lang("appointment_interview") ?>]", filter_type: "text", data: []},				
						], "footer");
				});
			</script>
			
			<div class="box">
				<div class="box-header">
					<h2 class="blue">
						<i class="fa-fw fa fa-barcode"></i>
						<?= lang('interview_refugees'); ?>
					</h2> 
					<div class="box-icon hidden">
						<ul class="btn-tasks">
							<li class="dropdown">
								<a href="#" class="toggle_up tip" title="<?= lang('hide_form') ?>">
									<i class="icon fa fa-toggle-up"></i>
								</a>
							</li>
							<li class="dropdown">
								<a href="#" class="toggle_down tip" title="<?= lang('show_form') ?>">
									<i class="icon fa fa-toggle-down"></i>
								</a>
							</li>
							
							<li class="dropdown">
								<a data-toggle="dropdown" class="dropdown-toggle" href="#">
									<i class="icon fa fa-tasks tip" data-placement="left" title="<?= lang("actions") ?>"></i>
								</a>
								<ul class="dropdown-menu pull-right tasks-menus" role="menu" aria-labelledby="dLabel">
									<li>
										<a href="<?= site_url('applications/add_interview_refugee'); ?>"><i class="fa fa-plus-circle"></i> 
											<?= lang("add_interview_refugee"); ?>
										</a>
									</li>
									<li>
										<a href="#" id="excel" data-action="export_excel"><i class="fa fa-file-excel-o"></i> 
											<?= lang('export_to_excel') ?>
										</a>
									</li>
								</ul>
							</li>
					</div>
					
				</div>
			
				<div class="box-content">
					<div class="row">
						<?= form_open_multipart("applications/interview_refugees"); ?>
						
						<div class="col-sm-12">
							<p class="introtext"><?= lang('list_results'); ?></p>
							<div class="form">				
									<div class="row">
										<div class="col-md-3">
											<div class="form-group"> 		
												<?php echo lang('លេខករណី', 'លេខករណី	'); ?> :
												
												<input type="text" value="<?= $case_no ?>" class="form-control" name="case_no" />
											</div> 
										</div>
										<div class="col-sm-3">
											<div class="form-group" >
												<?php echo lang('កាលបរិច្ឆេទសម្ភាសន៍ចាប់ពី', 'កាលបរិច្ឆេទសម្ភាសន៍ចាប់ពី'); ?>
												<input name="date_from" value="<?= $date_from ?>" class="form-control input-sm date" id="register_from" type="text">
											</div>
										</div>
										<div class="col-sm-3">
											<div class="form-group" >
												<?php echo lang('date_to', 'date_to'); ?>
												<input name="date_to" value="<?= $date_to ?>" class="form-control input-sm date" id="register_to" type="text">
											</div>
										</div>
										
										<div class="col-sm-12">
											<div class="form-group">
												<input type="submit" value="<?= lang("search") ?>" name="search" class="btn btn-default btn-info" />
											</div>
										</div>
										
									</div>
								
							</div>				
							
							<div class="table-responsive">
								<table id="dataTable2" class="table table-condensed table-bordered table-hover table-striped">
									<thead>
										<tr>
											<th style="width:3%;"><?= lang("#") ?></th>
											<th><?= lang("case_no") ?></th>
											<th><?= lang("firstname") ?></th>
											<th><?= lang("lastname") ?></th>
											<th><?= lang("dob") ?></th>
											<th><?= lang("gender") ?></th>
											<th><?= lang("nationality") ?></th>
											<th><?= lang("interview_date") ?></th> 
											<th><?= lang("status") ?></th>  
											<th><?= lang("action") ?></th>
										</tr>
									</thead>
									<tbody>
										
									</tbody>					
									<tfoot>
										<tr>
											<th></th>
											<th></th>
											<th></th>
											<th></th>
											<th></th> 							
											<th></th>
											<th></th>
											<th></th>
											<th></th> 
											<th></th>
										</tr>
									</tfoot>
								</table>
							</div>
							
						</div>
						<?= form_close(); ?>
					</div>
				</div>
			</div>
		
		</div>
		
	
	</div>

<style type="text/css">
	.table {
		white-space:nowrap;
	}
</style>


	
