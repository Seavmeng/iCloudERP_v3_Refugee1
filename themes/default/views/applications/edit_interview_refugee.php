<ul id="myTab" class="nav nav-tabs">
	<li class="">
		<a href="#content_1" class="tab-grey">
			<?= lang("interview"); ?>
			<i class="fa fa-chevron-right" aria-hidden="true"></i>
		</a>
	</li>
	<li class="">
		<a href="#content_2" class="tab-grey">
			<?= lang("pre_interview"); ?>
			<i class="fa fa-chevron-right" aria-hidden="true"></i>
		</a>
	</li>
	<li class="">
		<a href="#content_3" class="tab-grey">
			<?= lang("interview_records"); ?>
			<i class="fa fa-chevron-right" aria-hidden="true"></i>
		</a>
	</li>
	<li class="">
		<a href="#content_4" class="tab-grey">
			<?= lang("post_interview"); ?>
			<i class="fa fa-chevron-right" aria-hidden="true"></i>
		</a>
	</li>
	<li class="">
		<a href="#content_5" class="tab-grey">
			<?= lang("ឯកសារភ្ជាប់"); ?>
			<i class="fa fa-chevron-right" aria-hidden="true"></i>
		</a>
	</li>
</ul>

<?= form_open_multipart("applications/edit_interview_refugee/".$id); ?>

<?php 
	$countries_ = array(lang("select"));
	foreach($countries as $country){
		$countries_[$country->id] = $country->country;
	} 
	$provinces_ = array(lang("select"));
	foreach($provinces as $province){
		$provinces_[$province->id] = $province->name;
	} 
	$districts_ = array(lang("select"));
	foreach($districts as $district){
		$districts_[$district->id] = $district->name;
	} 
	$communes_ = array(lang("select"));
	foreach($communes as $commune){
		$communes_[$commune->id] = $commune->name;
	} 
?>
					
<div class="tab-content">
	
	<div id="content_1" class="tab-pane fade in">
		<div class="box">
			
			<div class="box-header">
				<h2 class="blue">
					<i class="fa-fw fa fa-barcode"></i>
					<?= lang("interview_form") ?>
				</h2>
			</div>
			
			<div class="box-content" >
				
				<div class="col-sm-5" style="padding-left:0px;">  
				
					<div class="col-sm-12">
						<div class="form-group">
							<?php echo lang('case_no', 'case_no'); ?>​ 
							<span class="red">*</span>
							<div class="controls">
								<input type="text" readonly value="<?= set_value('case_no',$application->case_prefix.$application->case_no) ?>" name="case_no" id="case_no" class="form-control input-sm" />
							</div>
						</div>
					</div> 
					
					<div class="col-sm-6">
						<div class="form-group">
							<?php echo lang('firstname', 'firstname'); ?>
							<small class="red">*</small>
							<input class="form-control input-sm" type="text" name="firstname_kh" value="<?= set_value("firstname_kh", $application->firstname_kh) ?>" />
						</div>
					</div>
					
					<div class="col-sm-6">
						<div class="form-group">
							<?php echo lang('lastname', 'lastname'); ?>
							<small class="red">*</small>
							<input class="form-control input-sm" type="text" name="lastname_kh" value="<?= set_value("lastname_kh", $application->lastname_kh) ?>" />
						</div>
					</div>
					
					<div class="col-sm-6">
						<div class="form-group">
							<?php echo lang('ភេទ', 'ភេទ'); ?>
							<small class="red">*</small>
							<div class="controls">
								<label class="radio-inline">
									<input type="radio" <?= ($application->gender=='male'?'checked':'')  ?> value="male" name="gender"> <?= lang('male'); ?>
								</label>
								<label class="radio-inline">
									<input type="radio" <?= ($application->gender=='female'?'checked':'')  ?> value="female" name="gender"> <?= lang('female'); ?>
								</label> 
							</div>
						</div>
					</div>
					
					<div class="col-sm-6">
						<div class="form-group">
							<?php echo lang('ថ្ងៃខែឆ្នាំកំណើត', 'ថ្ងៃខែឆ្នាំកំណើត'); ?>
							<small class="red">* </small>
							<div class="controls">
								<input type="text" value="<?= set_value('dob',$this->erp->hrsd($application->dob)) ?>" name="dob" class="form-control input-sm date" />
							</div>
						</div>
					</div>
					
					<div class="col-sm-6">
						<div class="form-group">
							<?php echo lang('សញ្ជាតិ​ ', 'សញ្ជាតិ​ '); ?>​  
							<span class="red">*</span>
							<div class="controls">
								<input type="text"  value="<?= set_value('nationality_kh', $application->nationality_kh) ?>"  name="nationality_kh" class="form-control input-sm" />								
							</div>
						</div>
					</div>
					
					<div class="col-sm-6">
						<div class="form-group">
							<?php echo lang('សញ្ជាតិ​ ភាសាអង់គ្លេស', 'សញ្ជាតិ​ ​ភាសាអង់គ្លេស'); ?>​  
							<span class="red">*</span>
							<div class="controls">
								<input type="text"  value="<?= set_value('nationality', $application->nationality) ?>"  name="nationality" class="form-control input-sm" />								
							</div>
						</div>
					</div>
					
					
					<div class="col-sm-12">
						<div class="panel panel-warning">
							<div class="panel-heading"><?php echo lang("address","address") ?> / <?= lang("address") ?></div>
							<div class="panel-body" style="padding: 5px;">
								<div class="col-lg-6">
									<div class="form-group">
										<?php echo lang('country', 'country'); ?>
										<div class="controls">
											<?php echo form_dropdown('country', $countries_, $application->country, ' class="form-control" '); ?>												
										</div>
									</div>
								</div>
								
								<div class="col-lg-6">
									<div class="form-group">
										<?php echo lang('province', 'province'); ?>
										<div class="controls">
											<?php echo form_dropdown('province', $provinces_, $application->province, ' class="form-control" '); ?>												
										</div>
									</div>
								</div>
								
								<div class="col-lg-6">
									<div class="form-group">
										<?php echo lang('district', 'district'); ?>
										<div class="controls">
											<?php echo form_dropdown('district', $districts_, $application->district, ' class="form-control" '); ?>												
										</div>
									</div>
								</div>
								
								<div class="col-lg-6">
									<div class="form-group">
										<?php echo lang('commune', 'commune'); ?>
										<div class="controls">
											<?php echo form_dropdown('commune', $communes_, $application->commune, ' class="form-control" '); ?>												
										</div>
									</div>
								</div>
								
								<div class="col-sm-12">
									<div class="form-group">
										<?php echo lang('អាសយដ្ឋាន', 'អាសយដ្ឋាន'); ?> (<?php echo lang("KH") ?>)
										<span class="red">*</span>
										<div class="controls">
											<input type="text" value="<?= set_value('address', $application->address) ?>" name="address" class="form-control input-sm" />
										</div>
									</div>
								</div>
								
								<div class="col-sm-12">
									<div class="form-group">
										<?php echo lang('អាសយដ្ឋាន', 'អាសយដ្ឋាន'); ?> (<?php echo lang("EN") ?>)
										<span class="red">*</span>
										<div class="controls">
											<input type="text" value="<?= set_value('address_kh', $application->address_kh) ?>" name="address_kh" class="form-control input-sm" />
										</div>
									</div>
								</div>
								
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
				
				</div>
				
				<div class="col-sm-5" style="padding-left:0px;">
					
					<div class="col-sm-6">
						<div class="form-group">
							<?php echo lang('interview_officer', 'interview_officer'); ?>​ 
							<span class="red">*</span>
							<div class="controls">
								<?php 
									$users_ = array(lang("select"));
									foreach($users as $user){
										$users_[$user->id] = $user->last_name." ".$user->first_name;
									} 
								?>
								<div class="controls"> 								
									<?php echo form_dropdown('interview_officer', $users_, $result->interview_officer, ' class="form-control" '); ?>							
								</div>
							</div>
						</div>
					</div>
					
					<div class="col-sm-6">
						<div class="form-group">
							<?php echo lang('ភាសា​', 'ភាសា​'); ?>
							<span class="red">*</span>
							<div class="controls">
								<?php 
									$languages_ = array(lang("select"));
									foreach($languages as $language){
										$languages_[$language->id] = $language->language;
									} 
							
								?>
								<div class="controls"> 								
									<?php echo form_dropdown('language[]', $languages_, json_decode($result->language), ' class="form-control"  multiple="multiple"'); ?>							
								</div>
							</div>
						</div>
					</div>
					
					<div class="col-sm-6">
						<div class="form-group">
							<?php echo lang('នាមខ្លួន ភាសាអង់គ្លេស', 'នាមខ្លួន ភាសាអង់គ្លេស'); ?>  
							<span class="red">*</span>
							<div class="controls">
								<input type="text" value="<?= set_value('firstname', $application->firstname) ?>" name="firstname" class="form-control input-sm" />								
							</div>
						</div>
					</div>
					
					<div class="col-sm-6">
						<div class="form-group">
							<?php echo lang('នាមត្រកូល ភាសាអង់គ្លេស', 'នាមខ្លួន ភាសាអង់គ្លេស'); ?>  
							<span class="red">*</span>
							<div class="controls">
								<input type="text" value="<?= set_value('lastname', $application->lastname) ?>" name="lastname" class="form-control input-sm" />								
							</div>
						</div>
					</div>
					
					<div class="col-sm-6">
						<div class="form-group">
							<?php echo lang('សាសនា​', 'សាសនា ​'); ?>​  
							<span class="red">*</span>
							<div class="controls">
								<input type="text" value="<?= set_value('religion_kh', $application->religion_kh) ?>" name="religion_kh" class="form-control input-sm" />								
							</div>
						</div>
					</div>
					
					<div class="col-sm-6">
						<div class="form-group">
							<?php echo lang('សាសនា​ ភាសាអង់គ្លេស', 'សាសនា ​ភាសាអង់គ្លេស'); ?>​  
							<span class="red">*</span>
							<div class="controls">
								<input type="text" value="<?= set_value('religion', $application->religion) ?>" name="religion" class="form-control input-sm" />								
							</div>
						</div>
					</div>
					
					<div class="col-sm-6">
						<div class="form-group">
							<?php echo lang('ជាតិពន្ធុ', 'ជាតិពន្ធុ ​'); ?>​  
							<span class="red">*</span>
							<div class="controls">
								<input type="text" value="<?= set_value('ethnicity_kh', $application->ethnicity_kh) ?>" name="ethnicity_kh" class="form-control input-sm" />								
							</div>
						</div>
					</div>
					
					<div class="col-sm-6">
						<div class="form-group">
							<?php echo lang('ជាតិពន្ធុ ភាសាអង់គ្លេស', 'ជាតិពន្ធុ ភាសាអង់គ្លេស​'); ?>​  
							<span class="red">*</span>
							<div class="controls">
								<input type="text" value="<?= set_value('ethnicity', $application->ethnicity) ?>" name="ethnicity" class="form-control input-sm" />								
							</div>
						</div>
					</div>
					
					<div class="col-sm-12">
						<div class="form-group">
							<?php echo lang('អ្នកបកប្រែ', 'អ្នកបកប្រែ '); ?>  
							<span class="red">*</span>
							<div class="controls">
								<input type="text" value="<?= set_value('interpreter',$result->interpreter) ?>" name="interpreter" class="form-control input-sm" />								
							</div>
						</div>
					</div>
					
					<div class="col-sm-12">
						<div class="form-group">
							<?php echo lang('កន្លែងសម្ភាសន៍​ ', 'កន្លែងសម្ភាសន៍​'); ?>​  
							<span class="red">*</span>
							<?php 
								$offices_ = array(lang("select"));
								foreach($offices as $office){
									$offices_[$office->id] = $office->office_kh;
								} 
							?>
							
							<div class="controls"> 								
								<?php echo form_dropdown('interview_place', $offices_, $result->interview_place, ' class="form-control" '); ?>							
							</div>
						</div>
					</div>
					
					<div class="col-sm-12">
						<div class="form-group">
							<?php echo lang('កាលបរិច្ឆេទសម្ភាសន៍ ', 'កាលបរិច្ឆេទសម្ភាសន៍ '); ?>  
							<span class="red">*</span>
							<div class="controls"> 
								<input type="text" value="<?= set_value('interview_date', $this->erp->hrsd($result->interview_date)) ?>" name="interview_date" class="form-control input-sm date" />								
							</div>
						</div>
					</div>
					
					<div class="col-sm-12">
						<div class="form-group">
							<?php echo lang('សម្ភាសន៍បានចាប់ផ្តើម​ ', 'សម្ភាសន៍បានចាប់ផ្តើម'); ?>​  
							<span class="red">*</span>
							<div class="controls">
								<input type="text" value="<?= set_value('interview_started', $result->interview_started) ?>" name="interview_started" class="form-control input-sm timepicker" />								
							</div>
						</div>
					</div> 
					
					<div class="col-sm-12">
						<div class="form-group">
							<?php echo lang('សម្ភាសន៍បានបញ្ជប់​', 'សម្ភាសន៍បានបញ្ជប់​ '); ?>​ 
							<span class="red">*</span>
							<div class="controls ">
								<input type="text" value="<?= set_value('interview_ended', $result->interview_ended) ?>" name="interview_ended" class="form-control input-sm timepicker" disabled />								
							</div>
						</div>
					</div>
					
					<div class="col-sm-12">
						<div class="form-group"> 
							<button type="submit" class="btn btn-success" /><?= lang("submit") ?></button>
						</div>
					</div> 
					
				</div>
					
				<div class="col-sm-2" style="padding-left: 0px;">
					<div class="form-group">
					  <?php echo lang('photo', 'photo'); ?>​ 
					  <div class="main-img-preview">
						<?php
							$photo='no_image.png';
							if(isset($application->photo)){
								$photo=$application->photo;
							}
						?>
						<img class="thumbnail img-preview" id='img_preview' src="<?= site_url('assets/uploads/'.$photo.'')?>" title="Preview Logo">
					  </div>
					</div> 
				</div>
				
				<div class="clearfix"></div>
				
			</div>
		</div> 
	</div>
	
	<?= form_close(); ?>
	
	<div id="content_2" class="tab-pane fade in">
		<div class="box">
			<div class="box-header">
				<h2 class="blue">
					<i class="fa-fw fa fa-barcode"></i>
					<?= lang("pre_interview") ?>
				</h2>
			</div>
			<div class="box-content">
				<div class="row">	
					
					<div class="col-sm-12">
					
						<p class="bold" style="color:#FF5454;">****<?php echo lang("កំណត់សម្គាល់ជនភៀសខ្លួន") ?> :</p>
						<p style="color:#FF5454;"><?php echo strip_tags($application->notification); ?></p>
						<p class="bold">អ្នកបកប្រែត្រូវបានសួរថា តើគាត់ស្តាប់មន្ត្រីសម្ភាសន៍យល់ ប្ញុមិនយល់ទេ</p>
						<p>Interpreter asked whether or not he understands Interview Officer (IO)</p>
						<p class="bold">មន្ត្រីសម្ភាសន៍ណែនាំអ្នកបកប្រែ-សួរអ្នកដាក់ពាក្យសុំថា តើគាត់ស្តាប់អ្នកបកប្រែយល់ប្ញុទេ</p>
						<p>I O introduces the interpreter and asks s/he can understand the interpreter</p>
						<p class="bold">សួរអ្នកដាក់ពាក្យសុំថា តើគាត់មានអារម្មណ៍និងសុខភាពល្អឫុទេ</p>
						<p>Applicant is asked if he is feeling fit and well</p>
						<p class="bold">ការពន្យល់នៃនីតិវិធីនៃកិច្ចសម្ភាសន៍ឋានៈជនភៀសខ្លួន</p>
						<p>Explanation of RSD interview procedures</p>
						<p class="bold">ការរក្សាការសម្ងាត់</p>
						<p>Confidentiality</p>
						<p class="bold">កាតព្វកិច្ចប្រាប់ពីការពិត</p>
						<p>Obligation to tell the truth</p>
						<p class="bold">កាតព្វកិច្ចដើម្បីសហប្រតិបត្តិការ</p>
						<p>Obligation to cooperate</p>
						<p class="bold size16">តើអ្នកដាក់ពាក្យសុំមានសំណួរអ្វីទៀតទេ?</p>
						<p>Applicant has any questions?</p> 
						<a onclick="activeTab(3)"  class="btn btn-success" /><?= lang("next") ?></a>
					</div>
				</div>
			</div>
		</div>
	</div> 
	
	
	<div id="content_3" class="tab-pane fade in">
		<div class="box">
		
			<div class="box-header">
				<h2 class="blue">
					<i class="fa-fw fa fa-barcode"></i>
					<?= lang("interview_records") ?>
				</h2>
			</div> 
			<div class="box-content">
			
			<?= form_open("applications/add_interview_records/".$id); ?>
			
				<div class="row">	 
					<div class="col-md-3 column">
						<div class='form-group'>										
							<?= lang("member","member");?>
							<?php    
						 	$members_ = array(lang("select"));
							foreach($result_members as $member){
								$members_[$member->id] = $member->lastname_kh . " " . $member->firstname_kh;
							} 
							echo form_dropdown('member', $members_, 0, ' id="member" class="form-control" '); 
							 
						  /* 	$members_ = array(lang("select"));
							
							foreach($members as $member){
								$members_[$member->id] = $member->firstname_kh . " " . $member->lastname_kh;
							} 
							echo form_dropdown('member', $members_, 0, ' id="member" class="form-control" '); 
							   */
							?>
						</div>
					</div> 
					<div class="col-md-12 column">
							<table class="table table-bordered table-hover" id="tab_interview">
								<thead>
									<tr>
										<th><?= lang("#") ?></th>
										<th><?= lang("សំណួរ") ?></th>
										<th><?= lang("ចម្លើយ") ?></th>
										<th style="width: 120px;"><?= lang("កំណត់​ចំណាំ") ?></th>
										<th width='3%'></th>
									</tr>
								</thead>
								<tbody>
									<?php foreach($interview_records as $i => $row){  ?>
										<tr>											
											<td class="center" width='20'>
												<?php echo ($i+1) ?>
												<input type="hidden" name="member[]" id="member" value="<?= $row->interview_member_id?>" />
											</td>												
											<td>
												<textarea class="questions skip" id="question" name='questions[]' ><?= $row->questions?> </textarea>	
											</td>
											<td> 
												<textarea class="answers skip" id="answer" name="answers[]"><?= $row->answers?></textarea>
											</td>
											<td> 
												<textarea class="notes skip" name='notes[]' ><?= $row->notes?></textarea>
											</td>
											<td>
												<a title="<?= lang('delete');?>" href="<?= 'applications/delete_interview_records/'.$row->id ?>" class="btn btn-danger btn-xs">
												<i class="fa fa-trash"></i>
												</a>
											</td>
										</tr>
									<?php } ?> 
								</tbody>
							</table>						
					</div> 
							
					<div class="col-sm-12">
						<p><?= lang('ដើម្បីបង្កើតជួរដេកត្រូវប្រើ (  Crlt + Enter  ) ');?></p>
						<button type="submit" class="btn btn-success save-qa" disabled /><?= lang("submit") ?></button>
					</div>
				</div>
			<?= form_close();?>
			</div>
		</div>
	</div>
	
	<div id="content_4" class="tab-pane fade in">
		<div class="box">
			<div class="box-header">
				<h2 class="blue">
					<i class="fa-fw fa fa-barcode"></i>
					<?= lang("post_interview") ?>
				</h2>
			</div>
			<div class="box-content">
				<?= form_open_multipart("applications/add_clarification_applicant/".$id); ?>
				<div class="row">	 
					<div class="col-sm-12">
						<p class="bold">អ្នកដាក់ពាក្យសុំបានត្រួតពិនិត្យព័ត៌មានដែលបានកត់ត្រា ហើយបញ្ជាក់ចំពោះភាពត្រឹមត្រូវនៃការកត់ត្រា នៃកិច្ចសម្ភាសន៍។</p>
						<p>The applicant has reviewed the information recorded and attests to the accuracy of the transcription of the interview.</p>
						<p class="bold">សេចក្តីបញ្ជាក់ធ្វើដោយអ្នកដាក់ពាក្យសុំ </p>
						<p>Clarification made by Applicant: </p>
						<div class="form-group">						
							<div class="controls">	
								<textarea name="clarification_applicant" style="min-height: 400px !important;"><?= set_value("clarification_applicant",$result->clarification_applicant) ?></textarea>
							</div>
						</div>
					</div> 
					<div class="col-sm-12">
						<button type="submit" class="btn btn-success" /><?= lang("submit") ?></button>
					</div>
					<?= form_close();?>
				</div>
			</div>
		</div>
	</div>
	
	<div id="content_5" class="tab-pane fade in">
		<div class="box"> 
			<div class="box-header">
				<h2 class="blue">
					<i class="fa-fw fa fa-barcode"></i>
					<?= lang("ឯកសារភ្ជាប់") ?>
				</h2>
			</div> 
			<div class="box-content"> 
				<div class='row'>  
					<div class="col-sm-12">
						<div class="form-group">
							<a data-toggle='modal' data-target='#myModal'  href="<?= base_url().'asylum_seekers/add_identification_document/'.$counseling->id?>" class="btn btn-warning" id="add_D">
								<?= lang("add"); ?>
								<i class="fa fa-plus"></i>
							</a>
						</div>
					</div> 
					<div class="col-sm-12">
						<div class="form-group"> 
							<table class="text-center table table-condensed table-bordered table-hover table-striped" width="100%">
								<thead>
									<tr>
										<td class="text-center">#</td>
										<td><?= lang("document_type") ?></td>
										<td><?= lang("issued_place") ?></td>
										<td><?= lang("issued_date") ?></td>
										<td><?= lang("expired_date") ?></td>
										<td><?= lang("original_provided") ?></td>
									</tr>
								</thead>
								<tbody id="part_D">
								<?php foreach($identification as $iden){?>
									<tr>
										<td width='100' class='text-center'>
											<a href="<?= site_url("asylum_seekers/delete_identification_document/".$iden->id); ?>" class='btn btn-danger btn-xs remove'>
												<i class='fa fa-trash'></i>
											</a>
											<a href="<?= site_url("asylum_seekers/edit_identification_document/".$counseling->id.'/'.$iden->id); ?>" class='btn btn-success btn-xs' data-backdrop="static" data-keyboard="false"  data-toggle="modal" data-target="#myModal">
												<i class='fa fa-edit'></i>
											</a>
											<?php if(!empty($iden->attachment)){?>
											<a target='_blank' title="<?= lang('download')?>" href="<?= site_url("assets/uploads/document/identification/".$iden->attachment); ?>" class='btn btn-primary btn-xs'>
												<i class='fa fa-download'></i> 
											</a>
											<?php }?>
										</td>
										<td><?= $iden->document_type?></td> 
										<td><?= $iden->issued_place?></td> 
										<td><?= $this->erp->hrsd($iden->issued_date)?></td> 
										<td><?= $this->erp->hrsd($iden->expired_date)?></td> 
										<td><?= $iden->original_provided?></td> 
									</tr>
								<?php }?>
								</tbody>
							</table> 
						</div>
					</div>  
				</div>  
			</div>
		</div>
	</div>
	
</div>

<style type="text/css">	
	.disabled { cursor: not-allowed; }
	#img_preview{
		width:180px !important;
	} 
	.bootstrap-tagsinput{
		width:100%;
	}
	.text-break {
		color:blue;
		overflow-wrap: break-word;
		height:50px;
	}

	textarea {
		display: inline-block;
		border: solid 1px #000;
		min-height: 60px;		
		border:1px solid #EEE;
		resize:none;
		width: 100%;
		padding:5px;
	}
	.table-refugee-notification td {
		padding:5px;
	}	 
</style>
<script type="text/javascript" src="<?= $assets ?>tagsinput/src/bootstrap-tagsinput.js"></script>
<link href="<?= $assets ?>tagsinput/src/bootstrap-tagsinput.css" rel="stylesheet" />
<script type="text/javascript"> 
	$("input[name='interpreter']").tagsinput('items');	
	
	
	expanding_data();
	
	function expanding_data() {
		$(".questions, .answers, .notes").height(this.scrollHeight);
		$(".questions, .answers, .notes").on('keyup', function() {				
			$(this).height(0).height(this.scrollHeight);
		});
	}
	function activeTab(tab){
		$('.nav-tabs a[href="#content_'+tab+'"]').tab('show');
	};
	function append_data(){
		var i = $("#tab_interview tbody tr").length + 1;
		var member_id = $("#member").val();
		var html = "<tr>" ; 
				html += "<td class='center'>"+i+"<input type='hidden' value='"+member_id+"' name='member[]' /></td>";
				html += "<td><textarea class='questions' name='questions[]'></textarea></td>";
				html += "<td><textarea class='answers' name='answers[]'></textarea></td>";
				html += "<td><textarea class='notes' name='notes[]' ></textarea></td>";
				html += "<td class='center'>";
					html +="<a  class='btn btn-danger btn-xs remove'>";
					html +="<i class='fa fa-trash'></i>";
					html +="</a>";
				html +="</td>";
			html +="</tr>";
			
			$("#tab_interview").append(html);
			$(".remove").click(delete_data);
			
			expanding_data();
			if(i > 0) {
				$(".save-qa").removeAttr('disabled');
			}
	}
	
	
	 
	function delete_data(){
		var parent = $(this).parent().parent();
		parent.remove();
		return false;
	}
	
	$(function(){
		
		$('.timepicker').datetimepicker({
			format: 'hh:ii',
			startView: 0
		});
	
		$(this).keydown(function (e) { 
			  if(e.ctrlKey && e.keyCode == 13) { 
				append_data();
				return false;
			  } 
		}); 
	 
	});
	
</script> 
