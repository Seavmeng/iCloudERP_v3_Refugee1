<?php   
	$icon_check='<span style="font-size:17px;">&#x2611;</span>';
	$icon_uncheck='<span style="font-size:17px;">&#x2610;</span>';
?>
<div class="box a4"> 
	<div class="box-content"> 
		<div class="row text-size"> 
			<?php $this->load->view($this->theme."applications/head-cover_ministry"); ?> 
			<div class="col-sm-12 text-center text-cover">
				<h2 class="text-cover">ទម្រង់បែបបទវាយតម្លៃកំណត់ឋានៈជនភៀសខ្លួន</h2>
			</div>
			
			<div class="col-sm-12 text-justify​ table-fill">
			 <div style="border:1.5px solid #000; padding:10px;">
				 <table width="100%" style="white-space:nowrap;">
					<tr>
						<td width="100">
							<?= lang("លេខករណី ៖"); ?> 
							<b><?= $application->case_prefix.$application->case_no;?></b>
						</td>						
						<td width="80px">
							<?= lang(" សម្ភាសន៍ ៖ ") ?> 
							<span style="font-size:17px;">&#x2611;</span>
							<?= lang(" លើកទី") ?><?= $this->erp->toKhmer($evaluation->number_of_interview) ?>
						</td>
					</tr>
					
					<tr>
						<td width="100">
							<?= lang("ឈ្មោះមន្ដ្រីសម្ភាសន៍ ៖"); ?>
							<?php 
								foreach ($users as $user){
									echo ($interview->interview_officer == $user->id ? $user->full_name :''); 
								}
							?>
						</td>
						<td></td>
					</tr>
					<tr>
						<td width="100">
							<?= lang("កាលបរិច្ឆេទសម្ភាសន៍ ៖"); ?>
							<?=  $this->erp->toKhmer($this->erp->hrsd($interview->interview_date));?>
						</td>
						<td></td>
					</tr>
					<tr>
						<td width="100">
							<?= lang("ឈ្មោះអ្នកបកប្រែ ៖"); ?>
							<?php echo str_replace(",",",  ",$interview->interpreter);?>
						</td>
						<td></td>
					</tr>
					<tr>
						<td width="100">
							<?= lang("ឈ្មោះមន្ដ្រីវិនិច្ឆ័យ ៖"); ?>
							<?php 
								foreach ($users as $user){
									echo ($evaluation->evaluate_officer == $user->id ? $user->full_name:''); 
								}
							?>
						</td>
						<td></td>
					</tr>					
				</table>
			</div>
			
				<h3 class="text-cover">ប្រវត្ដិរូបសង្ខេបរបស់អ្នកដាក់ពាក្យសុំ</h3>
				<p>(សូមមើលទម្រង់បែបបទពាក្យសុំឋានៈជនភៀសខ្លួន ដើម្បីបំពេញប្រវត្ដិរូប)</p>
				
			  <div style="border:1.5px solid #000; padding:10px;">
				
				  <table width="100%" style="white-space:nowrap;">
						<tr>
							<td width="100">
								<?= lang("ឈ្មោះពេញ ៖"); ?>
								<?= $application->lastname_kh." ".$application->firstname_kh." / ".$application->lastname." ".$application->firstname; ?>
							</td>
							<td></td>
							<td width="80px">
								<?= lang("សញ្ជាតិ ៖") ?>
								<?= $application->nationality_kh; ?>
							</td>
						</tr>
						<tr>
							<td width="300" colspan="3">
								<?= lang("ប្រសិនបើគ្មានសញ្ជាតិ បញ្ជាក់ពីអតីតនីវេសនដ្ឋាន ៖  "); ?>
								<?php //$application->address_o_kh; ?>
							</td>
						</tr>
						<tr>
							<td width="100">
								<?= lang("ថ្ងៃខែឆ្នាំកំណើត ៖ "); ?>
								<?=  $this->erp->toKhmer($this->erp->hrsd($application->dob));?>
							</td>							
							<td width="50">
								<?= lang("ភេទៈ "); ?>
								<?=  $this->erp->genderToKhmer($application->gender);?>
							</td>							
							<td width="50">
								<?= lang("ជាតិពន្ធុ ៖   "); ?>
								<?=  lang($application->ethnicity_kh);?>
							</td>							
						</tr>
						<tr>
							<td width="100">
								<?= lang("ស្ថានភាពគ្រួសារ ៖  "); ?>
								<?=  lang($application->marital_status);?>
							</td>
							<td></td>
							<td width="50">
								<?= lang("សាសនា ៖  "); ?>
								<?=  lang($application->religion_kh);?>
							</td>
						</tr>
						<tr>
                            <td width="100">
                                <?= lang(" ចំនួនមនុស្សដែលរួមដំណើរជាមួយអ្នកដាក់ពាក្យសុំៈ "); ?>
                                <?php
                                $n = count($result_members);
                                $number_member =($n<10?'0':'').$n;
                                ?>
                                <?=  $this->erp->toKhmer($number_member);?> នាក់
                            </td>
							<td></td>
                            <td width="100">
                                <?= lang("ពាក់ព័ន្ឋករណីលេខ ៖ "); ?>
                                <?= !empty($application->related_case)?  lang($application->related_case) : lang("មិនមាន")  ;?>
                            </td>
						</tr>					
				</table>
			
			</div>
				
			<h3 class="text-center text-cover">ផ្នែកទី <span class="khmer-number">១</span> -អំណះអំណាងសង្ខេប</h3>
				
				<?php 
					$evaluations = $this->applications->getEvaluationDetail($id,1); 
					foreach($evaluations as $evaluation){
						echo '<ul class="question-lists">';
							echo "<li>";
								echo $evaluation->question;
							echo "</li>";
						echo '</ul>';
						echo "<div class='indent_line break-word'>".$evaluation->answer."</div>";
					}  
				?>
				
			
			<h3 class="text-center text-cover"><u>ផ្នែកទី  <span class="khmer-number">២</span> -ការវាយតំលៃអំពីភាពគួរជឿជាក់បាន</u></h3>	
			  
			<?php
				$title = array("description"=>'សេចក្ដីសន្និដ្ឋានចំពោះការវាយតំលៃភាពជឿជាក់បាន');	  
				$evaluations = $this->applications->getEvaluationDetail($id,2);
				$i=1;
				foreach($evaluations as $evaluation){
					$i++;
					echo '<ul class="question-lists">';
						echo "<li>";
						echo $evaluation->question;
						echo $this->applications->getCheckById($evaluation->id);
						echo "</li>";
					echo '</ul>';
					 
					echo "<div class='indent_line break-word'>".$evaluation->answer."</div>"; 
					if($i==6){
						echo "<div class='text-center text-cover'>".$title['description'].'</div>';
						
					}
				}
			?> 
				
			<h3 class="text-center text-cover"><u>ផ្នែកទី​<span class="khmer-number">៣</span>- ការវាយតំលៃសី្តពីការរាប់បញ្ចូល-អនុសញ្ញាឆ្នាំ<span class="khmer-number">១៩៥១</span>/ពិធីសារឆ្នាំ<span class="khmer-number">១៩៦៧</span></u></h3>
			 
				<?php 
					$title3 = array(
						"description1"=>'“ភាពភ័យខ្លាច” បែបអត្តទិដ្ឋិ  ',
						"description2"=>'មូលដ្ថានបែបយថាភាព',
						"description3"=>'ការធ្វើទុកបុកម្នេញ',
						"description4"=>'ហេតុផលនៃការធ្វើទុកបុកម្នេញ',
						"description5"=>'សេចក្តីសន្និដ្ឋានអំពីការរាប់បញ្ចូលនៅក្នុងអនុសញ្ញាឆ្នាំ<span class="khmer-number">១៩៥១</span>/ពិធីសារឆ្នាំ<span class="khmer-number">១៩៦៧</span>'
					);	 
					$i=1; 
					$evaluations = $this->applications->getEvaluationDetail($id,3);
					echo "<p class='text-center text-cover'>".$title3['description1'].'&nbsp;<span>(ការភ័យខ្លាចរបស់បុគ្គល) </span></p>';
					
					foreach($evaluations as $evaluation){   
						echo '<ul class="question-lists">';
							echo "<li>";
							echo $evaluation->question;
							echo $this->applications->getCheckById($evaluation->id);
							echo "</li>";
						echo '</ul>';
						echo ($i != 8)?"<div class='indent_line break-word'>".$evaluation->answer."</div>":"";
						
						if($i==1){ 
							echo "<div class='text-center text-cover'>".$title3['description2'].'</div>';
						}
						if($i==2){ 
							$ans = $evaluation->is_yes;
							if($ans == '0'){
								echo "<div class='hidden'>";
							} 
						} 
						if($i==2){  
							echo "<div class='text-center text-cover'><u>".$title3['description3'].'</u></div>';
						} 
						if($i==7){  
								echo "<div class='text-center text-cover'><u>".$title3['description4'].'</u></div>'; 
						}
						if($i==8){   
							/// list answer point 3.8s 
							$rows = json_decode( $evaluation->answer, true ); 
								for($j=0;$j<count($rows);$j++) {  
									if($rows[$j]["multiple_choice"]['option']=='a'){
										$a ='checked'; 
										$valueA = $rows[$j]["multiple_choice"]['value']; 
									}  
									if($rows[$j]["multiple_choice"]['option']=='b'){
										$b ='checked'; 
										$valueB = $rows[$j]["multiple_choice"]['value']; 
									} 
									if($rows[$j]["multiple_choice"]['option']=='c'){
										$c = 'checked'; 
										$valueC = $rows[$j]["multiple_choice"]['value']; 
									} 
									if($rows[$j]["multiple_choice"]['option']=='d'){
										$d ='checked'; 
										$valueD = $rows[$j]["multiple_choice"]['value']; 
									} 
									if($rows[$j]["multiple_choice"]['option']=='e'){
										$e ='checked'; 
										$valueE = $rows[$j]["multiple_choice"]['value']; 
									}  
								}
							$list = "<div class='indent_line break-word'>";
								$list .= ($a=='checked'?$icon_check:$icon_uncheck)."<span class='bold'>".lang('ពូជសាសន៍  ')."</span>".lang('សូមបញ្ជាក់៖​ ');
								$list .= $valueA; 
							$list .= "</div>";
							
							$list .= "<div class='indent_line break-word'>";
								$list .= ($b=='checked'?$icon_check:$icon_uncheck)."<span class='bold'>".lang('សាសនា  ')."</span>".lang('សូមបញ្ជាក់៖ ');
								$list .= $valueB; 
							$list .= "</div>";
							$list .= "<div class='indent_line break-word'>";
								$list .= ($c=='checked'?$icon_check:$icon_uncheck)."<span class='bold'>".lang('សញ្ជាតិ  ')."</span>".lang('សូមបញ្ជាក់៖ ');
								$list .=$valueC; 
							$list .= "</div>";
							$list .= "<div class='indent_line break-word'>";
								$list .= ($d=='checked'?$icon_check:$icon_uncheck)."<span class='bold'>".lang('សមាជិកភាពនៃក្រុមសង្គមជាក់លាក់ណាមួយ  ')."</span>".lang('  សូមបញ្ជាក់៖ ');
								$list .= $valueD; 
							$list .= "</div>";
							$list .= "<div class='indent_line break-word'>";
								$list .= ($e=='checked'?$icon_check:$icon_uncheck)."<span class='bold'>".lang('ទស្សនៈនយោបាយ  ')."</span>".lang('សូមបញ្ជាក់៖ ');
								$list .= $valueE; 
							$list .= "</div>"; 
							$list .= "<div class='indent_line break-word'>";
								$list.=$rows['0']["answer"];
							$list .= "</div>"; 
							echo $list;
							
							
						}  
					  	if($i==9){ 
							if($ans == '0'){
								echo "</div>";
							}
							echo "<div class='text-center text-cover'><u>".$title3['description5'].'</u></div>'; 							
						}  
						$i++;
					} 
				?> 
				
			<h3 class="text-center text-cover"><u>ផ្នែកទី<span class="khmer-number">៤</span>-ការវាយតំលៃការរាប់បញ្ចូល-តម្រូវការកិច្ចការពារអន្ដរជាតិដទៃទៀត</u></h3>
			<p class="text-center ">(បំពេញតែក្នុងករណីដែលអ្នកដាក់ពាក្យសុំមិនបានជួបទៅនិងលក្ខណ្ឌការរាប់បញ្ចូលនៅក្នុងផ្នែកទី៣)</p>
			 
				<?php 
					$evaluations = $this->applications->getEvaluationDetail($id,4); 
					foreach($evaluations as $evaluation){
						echo '<ul class="question-lists">';
							echo "<li>";
							echo $evaluation->question;
							echo $this->applications->getCheckById($evaluation->id);
							echo "</li>";
						echo '</ul>';
						echo "<div class='indent_line break-word'>".$evaluation->answer."</div>";
					}
				?> 
			<h3 class="text-center text-cover"><u>ផ្នែក <span class="khmer-number">៥</span> -  ការវាយតម្លៃការមិនរាប់បញ្ចូល</u></h3>
			<p class="text-center">(ផ្នែកនេះត្រូវតែបំពេញប្រសិនបើអ្នកដាក់ពាក្យសុំបំពេញលក្ខណៈវិនិច្ឆ័យការរាប់បញ្ចូលនៅក្នុងផ្នែក ៣ ឬផ្នែក៤)</p>
			 
			<?php 
				
				$title5=array(
							"description"=>'សេចក្តីសន្និដ្ឋានអំពីការវាយតម្លៃមិនរាប់បញ្ចូល',
							"description1"=>'(ប្រសិនបើចម្លើយចំពោះសំណួរទាំងបីក្នុង  ៥.២, ៥.៣ និង ៥.៤ គឺ ទេ សូមបន្តទៅ ៥.៨។ ប្រសិនបើចម្លើយ ចំពោះសំណួរមួយ ឬច្រើនមានចម្លើយ បាទ/ចាស៎ សូមបន្តសំណួរបន្ទាប់ )',
						);
				$evaluations = $this->applications->getEvaluationDetail($id,5);
				$i=1;
				foreach($evaluations as $evaluation){
					$checkAns= $this->applications->getCheckById($evaluation->id); 
					if($i==5) {
						echo "<div class='text-center'>".$title5['description1'].'</div>'; 
					}
					if($i==8){
						echo "<div class='text-center text-cover'><i>".$title5['description'].'</i></div>';
							 $rows = json_decode( $evaluation->answer, true ); 
								for($j=0;$j<count($rows);$j++) {  
									if($rows[$j]["multiple_choice"]['option']=='a'){
										$a ='checked'; 
										$valueA = $rows[$j]["multiple_choice"]['value'];
									}  
									if($rows[$j]["multiple_choice"]['option']=='b'){
										$b ='checked'; 
										$valueB = $rows[$j]["multiple_choice"]['value'];
									} 
									if($rows[$j]["multiple_choice"]['option']=='c'){
										$c = 'checked';  
										$valueC = $rows[$j]["multiple_choice"]['value'];
									}
								}
					}  
					echo '<ul class="question-lists">';
						echo "<li>";
						echo $evaluation->question;
						echo $checkAns;
						echo "</li>";
					echo '</ul>';
					echo ($i!=8)?"<div class='indent_line break-word'>".$evaluation->answer."</div>":''; 
					
					$mcq_answer =$evaluation->other_answer ;  
					$i++;
				} 
			?>   
				<div class='indent_line break-word'><?php echo (($a=='checked')? $icon_check : $icon_uncheck);?>&nbsp;<?= lang('1 F(A)  ឧក្រិដ្ឋកម្មប្រឆាំងសន្តិភាព ឧក្រិដ្ឋកម្មសង្គ្រាម ឬឧក្រិដ្ឋកម្មប្រឆាំងមនុស្សជាតិ');?></div>
				<div class='indent_line break-word'><?= lang('សូមបញ្ជាក់​ ៖');?><?= $valueA?></div>
				<div class='indent_line break-word'><?php echo (($b=='checked')? $icon_check : $icon_uncheck);?>&nbsp;<?= lang('1 F(B)	ឧក្រិដ្ឋកម្មធ្ងន់ធ្ងរមិនមែននយោបាយមុនពេលចូលក្នុងប្រទេសភៀសខ្លួន ');?></div>
				<div class='indent_line break-word'><?= lang('សូមបញ្ជាក់​ ៖');?><?= $valueB?></div>
				<div class='indent_line break-word'><?php echo (($c=='checked')? $icon_check : $icon_uncheck);?>&nbsp;<?= lang('1 F(c)	សកម្មភាពដែលផ្ទុយនឹងគោលបំណង និងគោលការណ៍របស់អង្គការសហប្រជាជាតិ។');?></div>
				<div class='indent_line break-word'><?= lang('សូមបញ្ជាក់​ ៖');?><?= $valueC?></div>
			 
			<?php  
				echo "<div class='indent_line break-word'>";
				echo $rows['0']["answer"];
				echo "</div>";
			?>
			<h3 class="text-center text-cover"><u>ផ្នែក <span class="khmer-number">៦</span> អនុសាសន៍</u></h3>
			<ul class="question-lists">
			<?php 
				$evaluations = $this->applications->getEvaluationDetail($id,6);
				foreach($evaluations as $evaluation){
					echo "<table width='100%' border='1'><tr><td>"; 
					echo "<li>";
					echo $evaluation->question; 
					echo "</li>";
					echo "<div class='indent_line break-word'>".$evaluation->answer."</div>";
					echo "</tr></td></table>";
				}
			?>
			</ul>
			
			<br/>
			<br/>
			<br/>
			<br/>
			
  			<style type="text/css">
				.question-lists li{
					list-style:none;
					line-height:32px;
				}
			</style>	    			
	 		  	 
			</div>
		</div>
	</div> 
</div>



<style type="text/css">
	div.indent_line {
		color:blue; 
		width:95%;
		margin:0 auto; 
	}
	ul.question-lists li {
		margin-top:5px;
	}
	
	@font-face {
		font-family: "Khmer Moul";
		src: url(themes/default/assets/fonts/KhmerMoul.TTF) format("truetype");
	}  
	span.khmer-number {
		font-family:'Khmer Moul';
		font-size:18px;
	}
	@font-face {
		font-family: "TACTENG Font";
		src: url(themes/default/assets/fonts/TACTENG.TTF) format("truetype");
	}
	span.underline { 
		font-size:40px;
		font-family: "TACTENG Font", Verdana, Tahoma;
	}
	@media print{
		.bblack{ background: white !important; }
		.modal-content, .box, 
		.modal-header{ border:none !important; }
		.box-content{
			padding: 20px !important;
		}
	}
 
	hr{
		padding:0px;
		margin:0px;
		border-top:none !important;
		border-bottom:dotted 1px !important;
	} 
	.nopadding {
	   padding: 0 !important;
	   margin: 0 !important;
	} 
	.table-fill td{
		padding:5px;
	} 
	.table-none td{
		border: 1px solid #000;
		padding:15px;
	}  
	.box{ 
		line-height:28px; 
	} 
</style>