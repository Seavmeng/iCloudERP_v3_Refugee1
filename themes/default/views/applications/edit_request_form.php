 
<ul id="myTab" class="nav nav-tabs">
	<li class="">
		<a href="#content_1" class="tab-grey"><?= lang("preliminary_stay"); ?></a>
	</li>
	<li class="">
		<a href="#content_2" class="tab-grey"><?= lang("withdrawal_asylum"); ?></a>
	</li>
	<li class="">
		<a href="#content_3" class="tab-grey"><?= lang("written_report"); ?></a>
	</li>
	<li class="">
		<a href="#content_4" class="tab-grey"><?= lang("home_visit"); ?></a>
	</li>
	
</ul>

<div class="tab-content">

	<div id="content_1" class="tab-pane fade in">
		<div class="box">
			<div class="box-header">
			
				<h2 class="blue">
					<i class="fa-fw fa fa-barcode"></i>
					<?= lang("preliminary_stay") ?>
				</h2>
			</div>   
			<div class="box-content">
				<div class="row"> 
					<div id="form1">
						<?php  echo form_open_multipart("applications/edit_preliminary_stay/".$result->id."/".$preliminary_stay->id); ?>
						<div class="col-sm-6"> 
							<div class="col-sm-6">
								<div class="form-group">
									<?php echo lang('case_no', 'case_no'); ?>​ 
									<span class="red">*</span>
									<div class="control">
										<input type="text" readonly value="<?= $result->case_no ?>"  class="form-control input-sm case_no" id="case_no" placeholder="0000017">
										<input type="hidden" name="application_id" value="<?= $result->id;?>">
									</div>
								</div>
							</div>
							
							<div class="col-sm-6">
								<div class="form-group">
									<?php echo lang('date_issued', 'date_issued'); ?>​ 
									<span class="red">*</span>
									<div class="control">
										<input type="text" value="<?= set_value('date_issued',$this->erp->hrsd($preliminary_stay->date_issued)) ?>" class="form-control input-sm date" name="date_issued" />
									</div>
								</div>
							</div> 
							<div class="col-sm-6">
								<div class="form-group">
									<?php echo lang('valid_until', 'valid_until'); ?>​ 
									<span class="red">*</span>
									<div class="control">
										<input type="text" value="<?= set_value('valid_until',$this->erp->hrsd($preliminary_stay->valid_until)) ?>" class="form-control input-sm date" name="valid_until" />
									</div>
								</div>
							</div>
							
							<div class="col-sm-6">
								<div class="form-group">
									<?php echo lang('contact_number', 'contact_number'); ?>​ 
									<span class="red">*</span>
									<div class="control">
										<input type="text"  value="<?= set_value('contact_number',$preliminary_stay->contact_number) ?>"  class="form-control input-sm bfh-phone" data-format="+855 (ddd) ddd-dddd" name="contact_number" />
									</div>
								</div>
							</div>
							
							<div class="col-sm-12">
								<div class="form-group">
									<div class="control">
										<button type="submit" name="form1" class="btn btn-default btn-success"><?= lang("submit") ?></button>
									</div>
								</div>
							</div> 
						</div>
						
						
						<?= form_close(); ?>
					</div>
				
				</div>
			</div>
		</div>
	</div>
	
	<!---********************--->
	
	<div id="content_2" class="tab-pane fade in">
		<div class="box">
			<div class="box-header">
				<h2 class="blue">
					<i class="fa-fw fa fa-barcode"></i>
					<?= lang("withdrawal_asylum") ?>
				</h2>
			</div>
			
			<div class="box-content">
				<div class="row">
					
					<?php  
						$id = $result->id;
						$withdrawal_id = $withdrawal->id; 
					?>
					
					<div id="form2">
						<?php  echo form_open_multipart("applications/edit_withdrawal_asylum/".$id."/".$withdrawal_id); ?>
						<div class="col-sm-6">
						
							<div class="col-sm-6">
								<div class="form-group">
									<?php echo lang('case_no', 'case_no'); ?>​ 
									<span class="red">*</span>
									<div class="control">
										<input type="text" readonly value="<?= $result->case_no ?>"  class="form-control input-sm case_no" id="case_no" placeholder="0000017">
										<input type="hidden" name="application_id">
										<input type="hidden" value='2' name="content_id">
									</div>
								</div>
							</div>
							
							<div class="col-sm-6">
								<div class="form-group">
									<?php echo lang('date_withdrawal', 'date_withdrawal'); ?>​ 
									<span class="red">*</span>
									<div class="control">
										<input type="text" value="<?= set_value('date_withdrawal',date_format(date_create($withdrawal->date_withdrawal),"d/m/Y")) ?>" class="form-control input-sm date" name="date_withdrawal" />
										
									</div>
								</div>
							</div>

							<div class="col-sm-12">
								<div class="form-group">
									<?php echo lang('withdrawal_reason', 'withdrawal_reason'); ?>​ 
									<span class="red">*</span>
									<div class="control">
										<textarea name="withdrawal_reason" >
											<?= $withdrawal->withdrawal_reason;?>
										</textarea>
									</div>
								</div>
								<div class="form-group">
									<div class="control">
										<button type="submit" name="form2" class="btn btn-default btn-success"><?= lang("submit") ?></button>
									</div>
								</div>
							</div> 
						</div>
						
						<?php echo form_close(); ?>
					</div>
					
				</div>
			</div>
		</div>
	</div>
	
	<!---********************--->
	
	<div id="content_3" class="tab-pane fade in">
		<div class="box">
			<div class="box-header">
				<h2 class="blue">
					<i class="fa-fw fa fa-barcode"></i>
					<?= lang("written_report") ?>
				</h2>
			</div>
			
			<div class="box-content"> 
				<div class="row"> 
					<div id="form3">
						<?php  echo form_open_multipart("applications/request_form/".$id); ?>
						<div class="col-sm-6">
							<div class="col-sm-6">
								<div class="form-group">
									<?php echo lang('written_date', 'written_date'); ?>​ 
									<span class="red">*</span>
									<div class="control">
										<input type="text" value="<?= date("d/M/Y") ?>" class="form-control input-sm date case_no" name="text" />
									</div>
								</div>
							</div>
							
							<div class="col-sm-6">
								<div class="form-group">
									<?php echo lang('sample_writing_report', 'sample_writing_report'); ?>​ 
									<div class="control">
										<a href="<?= base_url("assets/downloads/F16_Written_Report.doc")?>" class="btn btn-default input-sm " />
											<?= lang("download"); ?>
											<i class="fa fa-file-word-o" aria-hidden="true"></i>
										</a>
									</div>
								</div>
							</div>
							
							<div class="col-sm-6">
								<div class="form-group">
									<?php echo lang('attachment_writing_report','attachment_writing_report'); ?>​ 
									<span class="red">*</span>
									<input type="file" class="form-control" />
								</div>
							</div>
							
							<div class="col-sm-12">
								<div class="form-group">
									<div class="control">
										<button type="submit" name="form3" class="btn btn-default btn-success"><?= lang("submit") ?></button>
									</div>
								</div>
							</div>
						</div>
						<?= form_close(); ?>
					</div>
					
				</div>
			</div>
		</div>
	</div>
	
	
	<!---********************--->
	
	<div id="content_4" class="tab-pane fade in">
		<div class="box">
			<div class="box-header">
				<h2 class="blue">
					<i class="fa-fw fa fa-barcode"></i>
					<?= lang("home_visit") ?>
				</h2>
			</div> 
			<div class="box-content">
				<div class="row"> 
					<div id="form4">   
					<?php
						$home_visit_id = $home_visit->id;
						$id = $home_visit->application_id;
					?>
					<?php  echo form_open_multipart("applications/edit_home_visit/".$id."/".$home_visit_id); ?>
						<div class="col-sm-6">
							<h3>
								<?php echo lang('part_a_personal', 'ផ្នែក ក. ព័តិមានបុគ្គល'); ?>​ 										
							</h3>
							
							<fieldset class="scheduler-border">  
									<input type="hidden" name="application_id" value="<?= $home_visit->application_id;?>">
									<input type="hidden" name="home_visit_id" value="<?= $home_visit->id;?>">
								<div class="col-sm-12" style="padding-left: 0px; padding-right: 0px;"> 
									<div class="col-sm-6" style="padding-left: 0px;">
										<div class="form-group">
											<?php echo lang('name', 'ឈ្មោះ'); ?>​
											<span class="red">*</span>
											<div class="control">
												<input type="text" value="<?= $home_visit->name;?>" class="form-control input-sm" name="username" />
											</div>
										</div>
									</div>
									
									<div class="col-sm-6" style="padding-right: 0px;">									
										<div class="form-group">
											<?php echo lang('gender', 'ភេទ'); ?>​ 
											<span class="red">*</span>
											<div class="controls"> 
												<label class="radio-inline">
													<input type="radio" value="male" <?php echo(($result->gender=='male')?'checked':'');?>  id="male" name="gender"><?= lang("male") ?>
												</label>
												<label class="radio-inline">
													<input type="radio" value="female" id="female" <?php echo(($result->gender=='female')?'checked':'');?> name="gender"><?= lang("female") ?>
												</label> 
											</div>
										</div>
									</div> 
								</div> 
								
								<div class="col-sm-12" style="padding-left: 0px; padding-right: 0px;">
								
									<div class="col-sm-6" style="padding-left: 0px;">
										<div class="form-group">
											<?php echo lang('ethnicity', 'ជនជាតិ'); ?>​
											<span class="red">*</span>
											<div class="control">
												<input type="text" value="<?= $home_visit->ethnicity;?>" class="form-control input-sm" name="ethnicity" />
											</div>
										</div>
									</div>
									
									<div class="col-sm-6" style="padding-right: 0px;">									
										<div class="form-group">
											<?php echo lang('dob', 'ឆ្នាំកំណើត'); ?>​ 
											<span class="red">*</span>
											<div class="control">
												<input type="text"  value="<?= set_value('dob',date_format(date_create( $home_visit->dob),"d/m/Y")) ?>" class="form-control input-sm date" name="dob" />
											</div>
										</div>
									</div>
									
								</div>
								
								<div class="col-sm-12" style="padding-left: 0px; padding-right: 0px;">
								
									<div class="col-sm-6" style="padding-left: 0px;">
										<div class="form-group">
											<?php echo lang('job', 'មុខរបរៈ'); ?>​
											<span class="red">*</span>
											<div class="control">
												<input value="<?= set_value('job', $home_visit->job) ?>"  type="text" class="form-control input-sm" name="job" />
											</div>
										</div>
									</div>
									
									<div class="col-sm-6" style="padding-right: 0px;">									
										<div class="form-group">
											<?php echo lang('education', 'ការសិក្សាអប់រំៈ'); ?>​ 
											<span class="red">*</span>
											<div class="control">
												<input type="text" value="<?= set_value('education', $home_visit->education) ?>"  class="form-control input-sm" name="education" />
											</div>
										</div>
									</div>
									
								</div>
							
							</fieldset>
							
							<h3>
								<?php echo lang('address', 'អាសយដ្ឋាន'); ?>​ 										
							</h3> 
						<fieldset class="scheduler-border">
								<div class="col-sm-12" style="padding-left: 0px; padding-right: 0px;">
								
									<div class="col-sm-6" style="padding-left: 0px;">
										<div class="form-group">
											<?php echo lang('home_number', 'ផ្ទះ'); ?>​ 
											<span class="red">*</span>
											<div class="control">
												<input type="text" value="<?= set_value('home_number', $home_visit->home_number) ?>" class="form-control input-sm" name="home_number" />
											</div>
										</div>
									</div>
									
									<div class="col-sm-6" style="padding-right: 0px;">									
										<div class="form-group">
											<?php echo lang('street_number', 'ផ្លូវ'); ?>​ 
											<span class="red">*</span>
											<div class="control">
												<input  type="text"  value="<?= set_value('street_number', $home_visit->street) ?>" class="form-control input-sm" name="street_number" />
											</div>
										</div>
									</div>
									
								</div>
								
								<div class="col-sm-12" style="padding-left: 0px; padding-right: 0px;">
								
									<div class="col-sm-6" style="padding-left: 0px;">
										<div class="form-group">
											<?php echo lang('group', 'ក្រុម'); ?>​ 
											<span class="red">*</span>
											<div class="control">
												<input type="text"  value="<?= set_value('group', $home_visit->group) ?>"  class="form-control input-sm" name="group" />
											</div>
										</div>
									</div>
									
									<div class="col-sm-6" style="padding-right: 0px;">									
										<div class="form-group" style="padding-right: 0px;">
											<?php echo lang('village', 'ភូមិ'); ?>​ 
											<span class="red">*</span>
											<div class="control">
												<input type="text" value="<?= set_value('village', $home_visit->village) ?>" class="form-control input-sm" name="village" />
											</div>
										</div>
									</div> 
								</div> 
								<div class="col-sm-12" style="padding-left: 0px; padding-right: 0px;"> 
									<div class="col-sm-6" style="padding-left: 0px;">
										<div class="form-group">
											<?php echo lang('commune', 'ឃុំ / សង្កាត់'); ?>​ 
											<span class="red">*</span>
											<div class="control">
												<input type="text" value="<?= set_value('commune', $home_visit->commune) ?>" class="form-control input-sm" name="commune" />
											</div>
										</div>
									</div>
									
									<div class="col-sm-6" style="padding-right: 0px;">									
										<div class="form-group">
											<?php echo lang('district', 'ស្រុក / ខ័ណ្ឌ'); ?>​ 
											<span class="red">*</span>
											<div class="control">
												<input type="text" value="<?= set_value('district', $home_visit->district) ?>" class="form-control input-sm" name="district" />
											</div>
										</div>
									</div>
									
								</div>
								
								<div class="form-group">
									<?php echo lang('city', 'ខេត្ត/ក្រុង'); ?>​ 
									<span class="red">*</span>
									<div class="control">
										<input type="text"  value="<?= set_value('city', $home_visit->city) ?>" class="form-control input-sm" name="city" />
									</div>
								</div>
								
								<div class="form-group">
									<?php echo lang('phone', 'លេខទំនាក់ទំនងៈ'); ?>​ 
									<span class="red">*</span>
									<div class="control">
										<input type="text" value="<?= set_value('phone', $home_visit->phone) ?>" class="form-control input-sm" name="phone" />
									</div>
								</div> 
							</div> 
						</fieldset>
						
						<div class="col-sm-6">	
							<h3>
								<?php echo lang('​part_b_live_status', 'ផ្នែក ខ. ស្ថានភាពរស់នៅ'); ?>​ 										
							</h3>
							<?php
								//var_dump($home_visit);
							?>
							<fieldset class="scheduler-border"> 
								<div class="form-group">
									<?php echo lang('family_status', 'ស្ថានភាពគ្រួសារៈ'); ?>​ 
									<span class="red">*</span>
									<div class="control"> 
										<div class="control"> 
										<div class="controls">  
											<label class="radio-inline">
												<input type="radio" value="poor"  id="poor" <?php echo(($home_visit->family_status=='poor')?'checked':'');?> name="family_status"><?= lang("poor") ?>
											</label>
											<label class="radio-inline">
												<input type="radio" value="medium" id="medium" <?php echo(($home_visit->family_status=='medium')?'checked':'');?> name="family_status"><?= lang("medium") ?>
											</label> 
											<label class="radio-inline">
												<input type="radio" value="rich" id="rich" <?php echo(($home_visit->family_status=='rich')?'checked':'');?> name="family_status"><?= lang("rich") ?>​
											</label> 
										</div>
									</div>
									</div>
								</div>
								
								<div class="form-group">
									<?php echo lang('health_status', 'ស្ថានភាពសុខភាពៈ'); ?>​ 
									<span class="red">*</span>
									<div class="control">
										<input type="text" value="<?= set_value('health_status', $home_visit->health_status) ?>" class="form-control input-sm" name="health_status" />
									</div>
								</div>
								
								<div class="form-group">
									<?php echo lang('supporter', 'ជំនួយឧបត្ថម្ភៈ'); ?>​ 
									<span class="red">*</span>
									<div class="control">
										<input type="text" value="<?= set_value('supporter', $home_visit->supporter) ?>" class="form-control input-sm" name="supporter" />
									</div>
								</div>
							
							</fieldset>
							
							
							<h3>
								<?php echo lang('​part_c_information_of_local_authorities', 'ផ្នែក គ.  ព័តិមានអាជ្ញាធរមូលដ្ឋាន'); ?>​ 										
							</h3>
							
							<fieldset class="scheduler-border">
							
								<div class="form-group">
									<?php echo lang('village_or_post_mg_name', 'ឈ្មោះមេភូមិ/មេប៉ុស្តិ៍ៈ'); ?>​ 
									<span class="red">*</span>
									<div class="control">
										<input type="text" value="<?= set_value('village_manager', $home_visit->village_manager_name) ?>" class="form-control input-sm" name="village_manager" />
									</div>
								</div>
								
								<div class="form-group">
									<?php echo lang('phone', 'លេខទំនាក់ទំនងៈ'); ?>​ 
									<span class="red">*</span>
									<div class="control">
										<input type="text" value="<?= set_value('village_manager_contact', $home_visit->village_manager_contact) ?>" class="form-control input-sm" name="village_manager_contact" />
									</div>
								</div>
								
								<div class="form-group">
									<?php echo lang('village_or_post_mg_speech', 'មតិរបស់មេភូមិ/មេប៉ុស្តិ៍ៈ'); ?>​ 
									<span class="red">*</span>
									<div class="control">
										<input type="text" value="<?= set_value('village_manager_speech', $home_visit->village_manager_speech) ?>" class="form-control input-sm" name="village_manager_speech" />
									</div>
								</div>
								
								<div class="form-group">
									<?php echo lang('officer_only', 'សំរាប់មន្ត្រីៈ '); ?>​ 
									<span class="red">*</span>
									<div class="control">
										<textarea   name="ministry_text" >
											<?= set_value('ministry_text', $home_visit->ministry_text) ?>
										</textarea>
									</div>
								</div> 
							</fieldset> 
						</div> 
						<div class="col-sm-12">
							<div class="form-group">
								<div class="control">
									<button type="submit" name="form4" class="btn btn-default btn-success"><?= lang("submit") ?></button>
								</div>
							</div>
						</div>  
					<?php echo form_close();?>
					</div> 
				</div> 
			</div>
		</div>
	</div>
</div>

<script type="text/javascript" src="<?= $assets ?>styles/helpers/components/js/bootstrap-formhelpers.js"></script>	
<link href="<?= $assets ?>styles/helpers/components/js/bootstrap-formhelpers.min.css" rel="stylesheet" />
<script type="text/javascript">
	$(function(){
		
		$(".case_no").autocomplete({
			source: function (request, response) {
				var q = request.term;
				if($.isNumeric(q)){
					$.ajax({
						type: 'get',
						url: '<?= site_url('applications/suggestions_caseno'); ?>',
						dataType: "json",
						data: {
							q : q
						},
						success: function (data) {
							response(data);
						}
					});
				}
			},
			minLength: 1,
			autoFocus: false,
			delay: 200,
			response: function (event, ui) {
				if (ui.content[0].id == 0) {
					$(this).removeClass('ui-autocomplete-loading');
					$(this).removeClass('ui-autocomplete-loading');
				}
			},
			select: function (event, ui) {
				event.preventDefault();
				if (ui.item.id !== 0) {
					$("input[name='application_id'").val(ui.item.row.application_id);
				} 
			}
		});
		
	});
</script>


