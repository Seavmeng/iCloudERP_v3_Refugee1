<div class='col-lg-3 director-txt'> 
	<div class='form-group'> 
		<?= lang("នាយកគ្រប់គ្រង៖", "នាយកគ្រប់គ្រង៖");?>
		<select   class="form-control text-cover director">
			<?php foreach($departments as $dep) {?>
			<option value="<?= $dep->department_en ?>"><b><?= $dep->department_kh?></b></option>
			<?php }?>
		</select>
	</div>
</div> 
<div class="clearfix"></div>  
<div class="box a4" >
		<div class="box-content">
			<div class="wrap-content text-size">			 
				<?php $this->load->view($this->theme."applications/head-cover_1"); ?>  
					<div class="col-sm-12 text-center text-cover">
					​​​​​​​​​    <br>
						<p>លិខិត​ជូន​ដំណឹង​ស្ដី​ពី​ការ​ពន្យា​ពេល​ធ្វើ​សេច​ក្ដី​សម្រេច</p>
						<span class='text-english'>Extension Notification on Refugee Status Determination </span>
					</div>  
					<table style="white-space:nowrap;">
						<tr> 
							<td colspan="2">ករណីលេខ / Case No:&nbsp;
								<?= $result->case_prefix." ".$result->case_no ?>
							</td>
						</tr> 
						<tr> 
							<td colspan="2"><span style="margin-left:0em;"></span>ឈ្មោះ / Name of Applications: &nbsp;
								<?= $result->lastname_kh ."  ".$result->firstname_kh." / ".$result->lastname ."  ".$result->firstname ?>
							</td>
						</tr> 
						<tr> 
							<td width="200"><p>ភេទ / Sex: <?= lang($result->gender)?></p></td>
							<td width="200"><p>សញ្ជាតិ / Nationality:&nbsp; <?= $result->nationality_kh ?></p></td>
							<td width="200"><p>ជាតិពន្ធុ / Ethnicity:&nbsp; <?= $result->ethnicity_kh?></p></td>
						</tr> 
						<tr>
							<td colspan="2"><p>ឆ្នាំកំណើត / Date of Birth: &nbsp; <?= $this->erp->khmer_date_format($result->dob)?></td>
						</tr>
					</table>  
					<table>
						<tr>
							<td>
								<p>
									<span style="margin-left:3em;"></span>
										នាយក​ដ្ឋាន​ជន​ភៀស​​ខ្លួន​សូម​ជំរាប​ជូន​ដំណឹង​ថា ការ​សម្រេច​ចិត្ត​ចំពោះ​​ករណី​របស់​លោក​/លោកស្រី មិន​ទាន់​រួច​រាល់​ជាស្ថាព​រនៅ​ទ្បើយ។ 
										នាយក​ដ្ឋាន​ជន​ភៀស​ខ្លួន នឹង​ជូន​ដំណឹង​អំពី​លទ្ធផល​នៃ​កិច្ច​សម្ភាសន៍ និង ការ​សម្រេច​ជា​ចុង​ក្រោយ មិន​អោយ​ហួស​ពី​រយះ​ពេល ៤៥ (សែសិបប្រាំ) ថ្ងៃទ្បើយ។ 
								</p>
								<p>
									<span style="margin-left:3em;"></span> 
										សូម​ថ្លែង​អំណរ​គុណ​ចំពោះ​កិច្ច​សហការ និង​ការ​យោគ​យល់​របស់​លោក លោកស្រី​លើ​បញ្ហា​នេះ។ 
								</p>
								<p>
									<span style="margin-left:3em;"></span>
										The Department of Refugee would like to inform you that the final decision on your case has not been finalized. The Department will officially inform you the result of interview and decision within 45 days.<br/>
​									<span style="margin-left:3em;"></span>
										We appreciate for your kind cooperation and understanding on this matter.
								</p> 
							</td>
						</tr>
					</table>  
				<div class="col-sm-6 text-justify">
					
				</div>
				
				<div class="col-sm-6">
					<div class="content-footer​" style="float:right;margin-right:30px;white-space:nowrap;">
						<table width="100%"  class="text-center" >
							<tr>
								<td>
									<?= $this->erp->footer_date_khmer('រាជធានីភ្នំពេញ');?>  
									<div class="text-cover director_text_sub"></div>
									<div class="text-cover director_text"  >ប្រធាននាយកដ្ឋានជនភៀសខ្លួន</div>
									<div class="director_text_en">Director of Refugee Department</div> 
								</td> 
							</tr>							
						</table>
					</div>
				</div>
			</div>
		</div>
</div>

<script>
	$(".director").on('click',function(){ 
		var txt = $(".director option:selected").text();
		var txt_en = $(".director option:selected").val();
		if (txt=="រដ្ឋលេខាធិការ") {
			$(".director_text_sub").text('ជ.រដ្ឋមន្រ្តី');
		}else{
			$(".director_text_sub").text('');
		}
		$(".director_text").text(txt); 
		$(".director_text_en").text(txt_en); 
	});
</script>
<style type="text/css">
	@media print{
		.bblack{ background: white !important; }
		.modal-content, .box, 
		.modal-header{ border:none !important; }
		.table { border:1px solid #EEE !important; }
		.director-txt {display:none; !important;}
		.box-content{
			padding: 20px !important;
		}
	}
	.table-fill td{
		padding:5px;
	}
	.thumb{
		border:5px solid #FFF;
		box-shadow:0px 0.5px 0.5px #000;
	}
	h2, center{
		font-family: 'Moul', sans-serif !important;
	}
	hr{
		padding:0px;
		margin:0px;
		border-top:none !important;
		border-bottom:dotted 1px !important;
	}
	.box{  
		line-height:28px; 
	} 
</style>