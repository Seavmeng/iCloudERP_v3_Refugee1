<div class="modal-dialog modal-md">
	<div class="modal-content">
		<div class="modal-header">				
			<button type="button" class="close" data-dismiss="modal">
				<span aria-hidden="true"><i class="fa fa-2x">&times;</i></span>
				<span class="sr-only"><?=lang('close');?></span>
			</button>
			<h4 class="modal-title" id="payModalLabel"><?= lang('edit_negative'); ?></h4>
		</div>
		
		<?php  echo form_open("applications/edit_negative/".$id); ?> 
		<div class="modal-body"> 
			<div class="col-sm-12">
				
				<div class="form-group">
					<?php echo lang('ថ្ងៃចុះហត្ថលេខា', 'ថ្ងៃចុះហត្ថលេខា'); ?>
					<span class="red">*</span>​
					<div class="controls">
						<input type="text" value="<?= ($negative->negative_date?$this->erp->hrsd($negative->negative_date):date("d/m/Y")) ?>" name="negative_date" class="form-control date input-sm" />							
					</div>
				</div>
				
				<div class="form-group">
					<?php echo lang('ថ្ងៃជូនដំណឹង', 'ថ្ងៃជូនដំណឹង'); ?>​
					<span class="red">*</span>
					<div class="controls">
						<input type="text" value="<?= ($negative->provided_date?$this->erp->hrsd($negative->provided_date):date("d/m/Y")) ?>" name="provided_date" class="form-control date input-sm" />							
					</div>
				</div>
				 
				<div class="form-group">
					<?php echo lang('លេខជូនដំណឹង', 'លេខជូនដំណឹង'); ?>​
					<span class="red">*</span>
					<div class="controls">
						<input type="text" value="" name="negative_no" class="form-control input-sm" />							
					</div>
				</div>
				
			</div> 
			<div class="clearfix"></div>
		</div>
		<div class="modal-footer">
		   <?php echo form_submit('submit', lang('submit'), 'class="btn btn-primary"'); ?>
		</div>
		<?php  echo form_close(); ?>
	</div>
</div> 
<?= $modal_js ?> 
<style type="text/css"> 
	ul.ui-autocomplete {
		z-index: 1100 !important;
	}
</style>
<script type="text/javascript">
	$(function(){
		
		$(".case_no").autocomplete({
			source: function (request, response) {
				var q = request.term;
					$.ajax({
						type: 'get',
						url: '<?= site_url('applications/suggestions_caseno'); ?>',
						dataType: "json",
						data: {
							q : q
						},
						success: function (data) {
							response(data);
						}
					});
			},
			minLength: 1,
			autoFocus: false,
			delay: 200,
			response: function (event, ui) {
				if (ui.content[0].id == 0) {
					$(this).removeClass('ui-autocomplete-loading');
					$(this).removeClass('ui-autocomplete-loading');
				}
			},
			select: function (event, ui) {
				event.preventDefault();
				if (ui.item.id !== 0) { 					
					var id = ui.item.id;										
					
					$("input[name='application_id'").val(ui.item.row.id);
					$(this).val(ui.item.label);
				} 
			}
		});  
		
	});
</script>
