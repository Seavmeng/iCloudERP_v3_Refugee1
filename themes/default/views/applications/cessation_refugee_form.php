<?php 
	echo $this->output->enable_profiler(TRUE);
?>
					<div class="content-cover a4">
					
							<img src="<?= base_url("assets/bg33.jpg") ?>" class="hidden-bg" />
							
							<div class="cover-in">
							
								<div class="content-header" style="height: 200px;">
								
								</div>
								
								<div class="cover-left"></div>
								
								<div class="text-center cover-center" >
									<h3><?= lang("ប្រកាស") ?></h3>
									<h3><?= lang("ស្តីពី") ?></h3>
									<h3><?= lang("ការបញ្ឈប់ឋានៈជនភៀសខ្លួន") ?></h3>
									<h3><span class='cover-line'>5</span></h3>
									<h3><?= lang("ឧបនាយករដ្ឋមន្រ្តី រដ្ឋមន្រ្តីក្រសួងមហាផ្ទៃ") ?></h3>
								</div>
								
								<div class="cover-right">
									<div class="cover-photo">
										<img src="<?= base_url("assets/uploads/".$result->photo) ?>" height="120" />
									</div>
								</div>
								
								<div style="clear:both;"></div>
								  
								<div style="clear:both;"></div>
								
								<style type="text/css">
									.cover-photo{
										margin-top:30px;
										margin-left:20px;
										width:120px;
										height:120px;
									}
									.cover-center{
										float:left; 
										width:50%;
									}
									.cover-left{
										float:left; 
										width:25%; 
										height:10px;
									}
									.cover-right{
										float:right; 
										width:25%; 
									}
								</style> 
								<div class="content-body text-left"  style=" text-align: justify;text-justify: inter-word;"> 
									 <?php echo $recognition->description ?>
								</div>
								
								<div class="text-center">
									<h3><u><?= lang("សម្រេច") ?></u></h3>
								</div>  
								<div >
									<table width="100%" class="text-left" >
										<tr>
											<td width="70" class="title-header-form">
												ប្រការ<span class='khmer-number'>១ </span>៖
											</td>
											<td width="70">
												បញ្ឈប់ឈ្មោះៈ&nbsp;
											</td>
											<td class="bold ">
												&nbsp;<?= $result->lastname_kh ." ".$result->firstname_kh." / ".$result->lastname ." ".$result->firstname ?>&nbsp;
											</td>
											<td width="10">
												ភេទៈ&nbsp;
											</td>
											<td class="bold "  width='100' >
												&nbsp;<?= lang($result->gender) ?> &nbsp;
											</td>
											<td width="70">
												ថ្ងៃខែឆ្នាំកំណើតៈ&nbsp;
											</td>
											<td class="bold ">
												&nbsp;<?= $this->erp->khmer_date_format($result->dob); ?> &nbsp;
											</td>
										</tr>
									</table>
									<table width="100%">
										<tr>
											<td width="70"></td>
											<td width="40">
												សញ្ជាតិៈ&nbsp;
											</td>
											<td class="bold ">
												<?= $result->nationality_kh; ?> &nbsp;
											</td>
											<td width="40">
												ជាតិពន្ធុៈ&nbsp;
											</td>
											<td class="bold "  width='100'>
												<?= $result->ethnicity_kh; ?> &nbsp;
											</td>
											<td width="10">
												ករណីលេខៈ&nbsp;
											</td>
											<td class="bold "  width='100'>
												<?= $this->erp->toKhmer($result->case_prefix." ".$result->case_no); ?> &nbsp;
											</td>
											<td width="70">
												ថ្ងៃខែឆ្នាំចុះឈ្មោះៈ&nbsp;
											</td>
											<td class="bold ">
												<?=  $this->erp->khmer_date_format($result->registered_date); ?> &nbsp;
											</td>
										</tr>
									</table>
									<table width="100%">
										<tr>
											<td width="70"></td>
											<td width="40">
												ទីកន្លែងកំណើតៈ&nbsp;
											</td>
											<td class="bold left">
												&nbsp;&nbsp;&nbsp;&nbsp;<?= $result->pob_kh; ?> &nbsp;
											</td>
										</tr>
									</table> 
									<?php if(!empty($member)){?>
									<table width="100%">
										<tr>
											<td width="70"></td>
											<td width="40">
											និងកូនក្នុងបន្ទុកចំនួន <?php echo $this->erp->toKhmer(count($member));?> នាក់ ៖
											</td> 
											<td class="bold left">
												 
											</td>
										</tr>
									</table> 
									<?php }?>
									
									<?php  foreach($member as $key => $row) { ?>
									<table width="100%"> 
										<tr>   
											<td width='70'></td>
											<td width="20"><?= $this->erp->toKhmer($key+1)?>.</td>
											<td class="bold"><?= $row->lastname_kh.' '.$row->firstname_kh." / ".$row->lastname.' '.$row->firstname?></td>
											<td width='14'>ភេទៈ&nbsp;</td>
											<td class="bold"><?= lang($row->gender)?></td>
											<td width='100'>ថ្ងៃខែឆ្នាំកំណើតៈ&nbsp;</td>
											<td class="bold"><?=  $this->erp->khmer_date_format($row->dob)?></td>
										</tr> 
									</table>  
										<?php }?>  
									<table width="100%">
										<tr>
											<td width="70"></td>
											<td class="left bold">ពីឋានៈជនភៀសខ្លួន ។</td>
										</tr>
									</table>
									<table width="100%">
										<tr>
											<td width="70" class="title-header-form">
												ប្រការ<span class='khmer-number'>២ </span>៖
											</td>
											<td>
												ជនភៀសខ្លួនដូចមានក្នុងប្រការ១ ខាងលើមានកាតព្វកិច្ច និងអនុវត្តច្បាប់របស់ព្រះរាជាណាចក្រកម្ពុជាឱ្យបានមឺុងម៉ាត់ ។
											</td>
										<tr>
									</table>
									<table width="100%">
										<tr>
											<td width="70" class="title-header-form">
												ប្រការ<span class='khmer-number'>៣ </span>៖
											</td>
											<td>
												ស្នងការរាជធានី-ខេត្តនិងស្ថាប័នដែលមានការពាក់ព័ន្ធជាមួយសាមុីខ្លួន ត្រូវអនុវត្តប្រកាសនេះចាប់ពីថ្ងៃចុះហត្ថលេខាតទៅ។
											</td>
										<tr>
									</table>
								</div>
								
								<div class="content-footer​" style="float:right; margin-right:30px;">
									<table width="100%">
										<tr>
											<td class='text-center' style="margin-top: 3px;">
											<?= $this->erp->footer_date_khmer('រាជធានីភ្នំពេញ');?> 
											</td>
										</tr>
										<tr>
											<td>
												<center> 
													ឧបនាយករដ្ឋមន្រ្តី រដ្ឋមន្រ្តីក្រសួងមហាផ្ទៃ
												</center>
											</td>
										</tr>
									</table>
								</div>
								
							</div>
							
					</div>
			

<style type="text/css" media="all">
	@media print{
		.bblack{ background: white !important; }
		.modal-content, .box, 
		.modal-header{ border:none !important; }
		.table { border:1px solid #000 !important; }
		.hidden-bg{
			display:block !important;
		}
	}
	 .hidden-bg{
		z-index:-9999;
		position:absolute;
		display:none;
	}
	.content-cover span, p {
		padding: 0;
		margin: 0;
		font-size:12px;	
	}
	.content-cover center{
		font-size:12px;
		font-family: 'Moul', sans-serif !important;
	}
	.content-cover table td{
		font-size:12px;	
		line-height:26px;
		white-space: nowrap;
	}
	.content-cover{
		width:800px;
		height:1200px;
		background:url("assets/bg33.jpg");
		background-size: 800px auto;
		background-repeat: no-repeat;
		color: blue;
		position:relative;
				
	}
	.cover-in{
		margin:0 70px;
		width:670px;
		position:absolute;
		white-space: wrap;
		padding:20px;
	}	
	ul{
		list-style: none;
		text-align:justify;
		padding: 0;
		margin: 0;
	}
	ul li:before {
	  content: '-';
	  position: absolute;
	  margin-left: -10px;
	} 
	.content-list li {
		line-height:26px;
		font-size:12px;
		list-style:square;
	}  
	h3 {
		line-height: 5px;
	}
	.content-cover h3{ 
		font-family: 'Moul', sans-serif !important;
	}
	hr{
		padding:0px;
		margin:0px;
		border-top:none !important;
		border-bottom:dotted 1px !important;
	}
	@font-face {
		font-family: "TACTENG Font";
		src: url(themes/default/assets/fonts/TACTENG.TTF) format("truetype");
	} 
	span.cover-line { 
		font-size:30px;
		padding:0px;
		font-family: "TACTENG Font";
	} 
	.content-body, table, .content-body li{ 
		line-height:19px;
		font-size: 12px !important;
	}
	@font-face {
			font-family: "Khmer Moul";
			src: url(themes/default/assets/fonts/KhmerMoul.TTF) format("truetype");
	}  
	span.khmer-number {
		font-family:'Khmer Moul'; 
	}
	table {
		text-align: justify;
		text-justify: inter-word;
		margin-left: -10px;
	}
</style>