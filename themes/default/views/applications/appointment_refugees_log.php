
<?php 
	$v = "";
	if($this->input->get("id")){
		$v .="&id=".$this->input->get("id");
	}
	
	if($this->input->post("date_from")) {
		$v .= "&date_from=" . $this->input->post("date_from");
		$date_from = $this->input->post("date_from");
	}

	if($this->input->post("date_to")) {
		$v .= "&date_to=" . $this->input->post("date_to");
		$date_to = $this->input->post("date_to");
	}
	
?> 
<ul id="myTab" class="nav nav-tabs">
	<li class=""><a href="#content_1" class="tab-grey"><?= lang("អំណានចាស់"); ?></a></li>
</ul>
	<div class="tab-content">
		<div id="content_1" class="tab-pane fade in">
			<script>
				$(document).ready(function () {
					var oTable = $('#dataTable1').dataTable({
						"aaSorting": [[1, "asc"]],
						"aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?= lang('all') ?>"]],
						"iDisplayLength": <?= $Settings->rows_per_page ?>,
						'bProcessing': true, 'bServerSide': true,
						'sAjaxSource': "<?= site_url('applications/getAppointmentRefugeesLog/?v=1'.$v) ?>",
						'fnServerData': function (sSource, aoData, fnCallback) {
							aoData.push({
								"name": "<?= $this->security->get_csrf_token_name() ?>",
								"value": "<?= $this->security->get_csrf_hash() ?>"
							});
							$.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
						},
						"aoColumns": [
						{"bSortable": false, "mRender": checkbox}, 									
						{"sClass" : "center"},
						{"sClass" : "center"},
						{"sClass" : "center"},	
						{"sClass" : "center", "mRender" : fld},
						{"sClass" : "center"},
						{"sClass" : "center"},
						{"sClass" : "center", "mRender" : fld}, 
						{"sClass" : "center", "mRender" : fld}, 						
						{"mRender" : row_status},
						{"bSortable": false, "sClass" : "center"}]
						,'fnRowCallback': function (nRow, aData, iDisplayIndex) {
								var oSettings = oTable.fnSettings();
								var action = $('td:eq(15)', nRow);	
								return nRow;
							},
							"fnFooterCallback": function (nRow, aaData, iStart, iEnd, aiDisplay) {
								var total = 0, total_kh = 0;
								for (var i = 0; i < aaData.length; i++) 
								{
								}
								var nCells = nRow.getElementsByTagName('th');					
							}
						}).fnSetFilteringDelay().dtFilter([            				
							{column_number: 0, filter_default_label: "[<?= lang("#") ?>]", filter_type: "text", data: []},							
							{column_number: 1, filter_default_label: "[<?= lang("case_no") ?>]", filter_type: "text", data: []},
							{column_number: 2, filter_default_label: "[<?= lang("firstname") ?>]", filter_type: "text", data: []},
							{column_number: 3, filter_default_label: "[<?= lang("lastname") ?>]", filter_type: "text", data: []},
							{column_number: 4, filter_default_label: "[<?= lang("dob") ?>]", filter_type: "text", data: []},								
							{column_number: 5, filter_default_label: "[<?= lang("gender") ?>]", filter_type: "text", data: []},
							{column_number: 6, filter_default_label: "[<?= lang("nationality") ?>]", filter_type: "text", data: []},
							{column_number: 7, filter_default_label: "[<?= lang("appointment_date") ?>]", filter_type: "text", data: []},				
						], "footer");  
				});
			</script>
			
			
			<div class="box">
				<div class="box-header"> 
					<h2 class="blue">
						<i class="fa-fw fa fa-barcode"></i>
						<?= lang('អំណានចាស់'); ?>
					</h2>
					<div class="box-icon">
						<ul class="btn-tasks">
							<li class="dropdown">
								<a data-toggle="dropdown" class="dropdown-toggle" href="#">
									<i class="icon fa fa-tasks tip" data-placement="left" title="<?= lang("actions") ?>"></i>
								</a>
								<ul class="dropdown-menu pull-right tasks-menus" role="menu" aria-labelledby="dLabel">
									<li>
										<a href="<?= site_url('applications/appointment_refugees'); ?>" ><i class="fa fa-newspaper-o"></i><?php echo lang("appointment_refugees");?></a>
									</li>
								</ul>
							</li>
						</ul>
					</div> 
				</div>
			
				<div class="box-content">
					<div class="row">  
						<div class="col-sm-12">  
							<p class="introtext"><?= lang('list_results'); ?></p>
							<div class="form">				
								<div class="row">
									<?= form_open_multipart("applications/appointment_refugees_log") ?>
									<div class="col-sm-3">
										<div class="form-group">
											<?php echo lang('កាលបរិច្ឆេទសម្ភាសន៍ចាប់ពី', 'កាលបរិច្ឆេទសម្ភាសន៍ចាប់ពី'); ?>
											<input name="date_from" value="<?= $date_from ?>" class="form-control input-sm date" id="date" type="text">
										</div>
									</div>
									<div class="col-sm-3">
										<div class="form-group">
											<?php echo lang('date_to', 'Date To'); ?>
											<input name="date_to" value="<?= $date_to ?>" class="form-control input-sm date" id="date" type="text">
										</div>
									</div>
									<div class="col-sm-12">
										<div class="form-group">
											<input type="submit" value="<?= lang("search") ?>" class="btn btn-default btn-info" />
										</div>
									</div>
									<?= form_close() ?>
								</div>
							</div>	
							
							<div class="table-responsive">
								<table id="dataTable1" class="table table-condensed table-bordered table-hover table-striped">
									<thead>
										<tr>
											<th style="width:3%;"><?= lang("#") ?></th>											
											<th><?= lang("case_no") ?></th>
											<th><?= lang("firstname") ?></th>
											<th><?= lang("lastname") ?></th>
											<th><?= lang("dob") ?></th>
											<th><?= lang("gender") ?></th>
											<th><?= lang("nationality") ?></th> 
											<th><?= lang("interview_date") ?></th>
											<th><?= lang("modified_date") ?></th>
											<th><?= lang("status") ?></th>
											<th><?= lang("action") ?></th>
										</tr>
									</thead>
									<tbody>
									</tbody>					
									<tfoot>
										<tr> 
											<th></th>											
											<th></th>
											<th></th>
											<th></th>
											<th></th>											
											<th></th>
											<th></th>
											<th></th>
											<th></th>
											<th></th>
											<th></th>
										</tr>
									</tfoot>
								</table>
							</div> 
						</div> 
					</div>
				</div>
			</div> 
		</div>   
	</div>
<style type="text/css">
	.table {
		white-space:nowrap;
	}
</style>
