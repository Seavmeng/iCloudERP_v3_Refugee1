<div class="modal-dialog modal-md">
	<div class="modal-content">
		<div class="modal-header">				
			<button type="button" class="close" data-dismiss="modal">
				<span aria-hidden="true"><i class="fa fa-2x">&times;</i></span>
				<span class="sr-only"><?=lang('close');?></span>
			</button>
			<h4 class="modal-title" id="payModalLabel"><?= lang('attachment'); ?></h4>
		</div>
		
		<?php  echo form_open_multipart("applications/attach_appointment_refugee/".$id); ?> 
		<div class="modal-body"> 
			<div class="col-sm-12">  
				<div class="form-group">
					<?php echo lang('attachment','attachment'); ?>​
					<span class="red">
						<i class="fa fa-file-word-o" aria-hidden="true"></i>&nbsp;
						<i class="fa fa-file-pdf-o" aria-hidden="true"></i>
					</span>	 
					<input id="attachment" accept=".pdf,.doc," type="file" name="attachment" data-browse-label="<?= lang('document'); ?>" ​ data-show-upload="false" data-show-preview="false" class="file" />	
				</div>  
			</div>
			<div class="col-sm-12">  
				<div class="form-group">
					<?php echo lang('name','name'); ?>​  
					<span class="red"> * </span>
					<input class="form-control" type="text" name="file_rename"/>	
				</div>  
			</div>
			<div class="col-sm-12"> 
				<?php $attachment = json_decode($result->attachment); ?>
				<?php if(!empty($attachment)) { ?>
				<label class="label-control"><?= lang("existing_document");?></label>
				<table class="table table-condensed table-bordered table-hover table-striped"> 
					<thead>
						<tr>
							<th width="50" class="text-center">#</th>
							<th><?= lang("file_name") ?></th>
							<th width="120"><?= lang("date_created") ?></th>
						</tr>
					</thead>
					<tbody>
						<?php 
							foreach($attachment as $key => $att) {
						?>
						<tr>
							<td> 
								<a href="<?= site_url("applications/delete_attach_appointment_refugee/".$id."/".$att->file_name ); ?>" onclick="return confirm('<?php echo lang("r_u_sure");?>')" class='btn btn-danger btn-xs remove'>
									<i class='fa fa-trash'></i>
								</a>
								<a target="_blank" href="<?= site_url("assets/uploads/document/appointment_refugee/".$att->file_name); ?>" class='btn btn-success btn-xs' >
									<i class='fa fa-download'></i>
								</a> 
							</td>
							<td><?= $att->file_rename ?></td>
							<td class="text-center"><?= $this->erp->hrld( $att->date_created )?></td>
						</tr>
						<?php }	?>
					</tbody>
				<?php }?> 
				</table>
			</div> 
			<div class="clearfix"></div>
		</div>
		<div class="modal-footer">
		   <?php echo form_submit('submit', lang('submit'), 'class="btn btn-primary save" disabled'); ?>
		</div>
		<?php  echo form_close(); ?>
	</div>
</div> 
<script type="text/javascript" src="<?= $assets ?>styles/helpers/components/js/bootstrap-formhelpers.js"></script>	
<link href="<?= $assets ?>styles/helpers/components/js/bootstrap-formhelpers.min.css" rel="stylesheet" />
<?= $modal_js ?>  
<script type="text/javascript">  
	
	$(function(){		
		$(".save").on("click",function(){
			var name = $("input[name='file_rename']").val() ;
			if(name==""){
				bootbox.alert('<?= lang("please_select_all");?>');
				return false; 
			}  
		});
		
		$("#attachment").on("change",function(){
			if($(this).val() != null){
				$("input[name='submit']").removeAttr("disabled");	
			}
			return false;
		}); 
	});  
	 
</script>
