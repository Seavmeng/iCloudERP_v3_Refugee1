​
<ul id="myTab" class="nav nav-tabs">
	<li class="">
		<a href="#content_1" class="tab-grey">
			<?= lang("interview"); ?>
			<i class="fa fa-chevron-right" aria-hidden="true"></i>
		</a>
	</li>
	<li class="disabled">
		<a href="#content_2" class="tab-grey">
			<?= lang("pre_interview"); ?>
			<i class="fa fa-chevron-right" aria-hidden="true"></i>
		</a>
	</li>
	<li class="disabled">
		<a href="#content_3" class="tab-grey">
			<?= lang("interview_records"); ?>
			<i class="fa fa-chevron-right" aria-hidden="true"></i>
		</a>
	</li>
	<li class="disabled">
		<a href="#content_4" class="tab-grey">
			<?= lang("post_interview"); ?>
			<i class="fa fa-chevron-right" aria-hidden="true"></i>
		</a>
	</li>
	<li class="disabled">
		<a href="#content_5" class="tab-grey">
			<?= lang("ឯកសារភ្ជាប់"); ?>
			<i class="fa fa-chevron-right" aria-hidden="true"></i>
		</a>
	</li>
</ul> 
<?= form_open_multipart("applications/add_interview_refugee/".$id."?q=".$_GET['q']); ?>

<?php 
	$countries_ = array(lang("select"));
	foreach($countries as $country){
		$countries_[$country->id] = $country->country;
	} 
	$provinces_ = array(lang("select"));
	foreach($provinces as $province){
		$provinces_[$province->id] = $province->name;
	} 
	$districts_ = array(lang("select"));
	foreach($districts as $district){
		$districts_[$district->id] = $district->name;
	} 
	$communes_ = array(lang("select"));
	foreach($communes as $commune){
		$communes_[$commune->id] = $commune->name;
	} 
?>
<div class="tab-content">​
	<div id="content_1" class="tab-pane fade in">
		<div class="box"> 
			<div class="box-header">
				<h2 class="blue">
					<i class="fa-fw fa fa-barcode"></i>
					<?= lang("interview_form") ?>
				</h2>
			</div>
			
			<div class="box-content" >
				
				<div class="col-sm-5" style="padding-left:0px; padding-right:0px;">  
				
					<div class="col-sm-12">
						<div class="form-group">
							<?php echo lang('case_no', 'case_no'); ?>​ 
							<span class="red">*</span>
							<div class="controls">
								<input type="text" value="<?= set_value('case_no', $application->case_prefix.$application->case_no) ?>" name="case_no" id="case_no" class="form-control input-sm" />								
								<input type="hidden" name="is_appeal"  value="<?php echo $_GET['q'];?>"/>
							</div>
						</div>
					</div> 
					
					<div class="col-sm-6">
						<div class="form-group">
							<?php echo lang('firstname', 'firstname'); ?>
							<small class="red">*</small>
							<input class="form-control input-sm" type="text" name="firstname_kh" value="<?= set_value("firstname", $application->firstname_kh) ?>" />
						</div>
					</div>
					
					<div class="col-sm-6">
						<div class="form-group">
							<?php echo lang('lastname', 'lastname'); ?>
							<small class="red">*</small>
							<input class="form-control input-sm" type="text" name="lastname_kh" value="<?= set_value("lastname", $application->lastname_kh) ?>" />
						</div>
					</div>
					
					<div class="col-sm-6">
						<div class="form-group">
							<?php echo lang('ភេទ', 'ភេទ'); ?>
							<small class="red">*</small>
							<div class="controls">
								<label class="radio-inline">
									<input type="radio" <?= ($application->gender=='male'?'checked':'')  ?> value="male" name="gender"> <?= lang('male'); ?>
								</label>
								<label class="radio-inline">
									<input type="radio" <?= ($application->gender=='female'?'checked':'')  ?> value="female" name="gender"> <?= lang('female'); ?>
								</label> 
							</div>
						</div>
					</div>
					
					<div class="col-sm-6">
						<div class="form-group">
							<?php echo lang('ថ្ងៃខែឆ្នាំកំណើត', 'ថ្ងៃខែឆ្នាំកំណើត'); ?>
							<small class="red">*</small>
							<div class="controls">
								<input type="text" value="<?= set_value('dob', $this->erp->hrsd($application->dob)) ?>" name="dob" class="form-control input-sm date" />
							</div>
						</div>
					</div>
					
					<div class="col-sm-6">
						<div class="form-group">
							<?php echo lang('សញ្ជាតិ​ ', 'សញ្ជាតិ​ '); ?>​  
							<span class="red">*</span>
							<div class="controls">
								<input type="text"  value="<?= set_value('nationality_kh', $application->nationality_kh) ?>"  name="nationality_kh" class="form-control input-sm" />								
							</div>
						</div>
					</div>
					
					<div class="col-sm-6">
						<div class="form-group">
							<?php echo lang('សញ្ជាតិ​ ភាសាអង់គ្លេស', 'សញ្ជាតិ​ ​ភាសាអង់គ្លេស'); ?>​  
							<span class="red">*</span>
							<div class="controls">
								<input type="text"  value="<?= set_value('nationality', $application->nationality) ?>"  name="nationality" class="form-control input-sm" />								
							</div>
						</div>
					</div>
								

					<div class="col-sm-12">
						<div class="panel panel-warning">
							<div class="panel-heading"><?php echo lang("address","address") ?> / <?= lang("address") ?></div>
							<div class="panel-body" style="padding: 5px;">
								<div class="col-lg-6">
									<div class="form-group">
										<?php echo lang('country', 'country'); ?>
										<div class="controls">
											<?php echo form_dropdown('country', $countries_, $application->country, ' class="form-control" '); ?>												
										</div>
									</div>
								</div>
								
								<div class="col-lg-6">
									<div class="form-group">
										<?php echo lang('province', 'province'); ?>
										<div class="controls">
											<?php echo form_dropdown('province', $provinces_, $application->province, ' class="form-control" '); ?>												
										</div>
									</div>
								</div>
								
								<div class="col-lg-6">
									<div class="form-group">
										<?php echo lang('district', 'district'); ?>
										<div class="controls">
											<?php echo form_dropdown('district', $districts_, $application->district, ' class="form-control" '); ?>												
										</div>
									</div>
								</div>
								
								<div class="col-lg-6">
									<div class="form-group">
										<?php echo lang('commune', 'commune'); ?>
										<div class="controls">
											<?php echo form_dropdown('commune', $communes_, $application->commune, ' class="form-control" '); ?>												
										</div>
									</div>
								</div>

								<div class="col-sm-12 text-nowrap">
									<div class="form-group">
										<?php echo lang('អាសយដ្ឋាន', 'អាសយដ្ឋាន'); ?> (<?php echo lang("KH") ?>)
										<span class="red">*</span>
										<div class="controls">
											<input type="text" value="<?= set_value('address_kh', $application->address_kh) ?>" name="address_kh" class="form-control input-sm" />
										</div>
									</div>
								</div>
								<div class="col-sm-12">
									<div class="form-group">
										<?php echo lang('អាសយដ្ឋាន', 'អាសយដ្ឋាន'); ?> (<?php echo lang("EN") ?>)
										<span class="red">*</span>
										<div class="controls">
											<input type="text" value="<?= set_value('address', $application->address) ?>" name="address" class="form-control input-sm" />
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
				<div class="col-sm-5" style="padding-left:0px;"> 
					<div class="col-sm-6">
						<div class="form-group">
							<?php echo lang('interview_officer', 'interview_officer'); ?>​ 
							<span class="red">*</span>
							<div class="controls">
								<?php 
									$users_ = array(lang("select"));
									foreach($users as $user){
										if($user->group_id == 7){ 
											$users_[$user->id] = $user->last_name." ".$user->first_name;
										}
									} 
								?>
								<div class="controls"> 								
									<?php echo form_dropdown('interview_officer', $users_, $appointment->interview_by, ' class="form-control" '); ?>							
								</div>
							</div>
						</div>
					</div>
					
					<div class="col-sm-6">
						<div class="form-group">
							<?php echo lang('ភាសា​', 'ភាសា​'); ?>
							<span class="red">*</span>
							<div class="controls">
								<?php 
									$languages_ = array(lang("select"));
									foreach($languages as $language){
										$languages_[$language->id] = $language->native_name;
									} 
								?>
								<div class="controls"> 								
									<?php echo form_dropdown('language[]', $languages_, $result->language, ' class="form-control"  multiple="multiple"'); ?>							
								</div>
							</div>
						</div>
					</div>
					
					
					<div class="col-sm-6">
						<div class="form-group">
							<?php echo lang('នាមខ្លួន ភាសាអង់គ្លេស', 'នាមខ្លួន ភាសាអង់គ្លេស'); ?>  
							<span class="red">*</span>
							<div class="controls">
								<input type="text" value="<?= set_value('firstname', $application->firstname) ?>" name="firstname" class="form-control input-sm" />								
							</div>
						</div>
					</div>
					
					<div class="col-sm-6">
						<div class="form-group">
							<?php echo lang('នាមត្រកូល ភាសាអង់គ្លេស', 'នាមខ្លួន ភាសាអង់គ្លេស'); ?>  
							<span class="red">*</span>
							<div class="controls">
								<input type="text" value="<?= set_value('lastname', $application->lastname) ?>" name="lastname" class="form-control input-sm" />								
							</div>
						</div>
					</div>					
					
					<div class="col-sm-6">
						<div class="form-group">
							<?php echo lang('សាសនា​', 'សាសនា ​'); ?>​  
							<span class="red">*</span>
							<div class="controls">
								<input type="text" value="<?= set_value('religion_kh', $application->religion_kh) ?>" name="religion_kh" class="form-control input-sm" />								
							</div>
						</div>
					</div>
					
					<div class="col-sm-6">
						<div class="form-group">
							<?php echo lang('សាសនា​ ភាសាអង់គ្លេស', 'សាសនា ​ភាសាអង់គ្លេស'); ?>​  
							<span class="red">*</span>
							<div class="controls">
								<input type="text" value="<?= set_value('religion', $application->religion) ?>" name="religion" class="form-control input-sm" />								
							</div>
						</div>
					</div>
					
					<div class="col-sm-6">
						<div class="form-group">
							<?php echo lang('ជាតិពន្ធុ', 'ជាតិពន្ធុ ​'); ?>​  
							<span class="red">*</span>
							<div class="controls">
								<input type="text" value="<?= set_value('ethnicity_kh', $application->ethnicity_kh) ?>" name="ethnicity_kh" class="form-control input-sm" />								
							</div>
						</div>
					</div>
					
					<div class="col-sm-6">
						<div class="form-group">
							<?php echo lang('ជាតិពន្ធុ ភាសាអង់គ្លេស', 'ជាតិពន្ធុ ភាសាអង់គ្លេស​'); ?>​  
							<span class="red">*</span>
							<div class="controls">
								<input type="text" value="<?= set_value('ethnicity', $application->ethnicity) ?>" name="ethnicity" class="form-control input-sm" />								
							</div>
						</div>
					</div>
					
					<div class="col-sm-12">
						<div class="form-group">
							<?php echo lang('កន្លែងសម្ភាសន៍​ ', 'កន្លែងសម្ភាសន៍​'); ?>​  
							<span class="red">*</span>
							<?php 
								$offices_ = array(lang("select"));
								foreach($offices as $office){
									$offices_[$office->id] = $office->office_kh;
								} 
							?>
							<div class="controls"> 								
								<?php echo form_dropdown('interview_place', $offices_, $appointment->appointment_place_id, ' class="form-control" '); ?>							
							</div>
						</div>
					</div>
					
					<div class="col-sm-12">
						<div class="form-group">
							<?php echo lang('អ្នកបកប្រែ', 'អ្នកបកប្រែ '); ?>  
							<span class="red">*</span>
							<div class="controls">
								<input type="text" value="<?= set_value('interpreter') ?>" name="interpreter" class="form-control input-sm" />								
							</div>
						</div>
					</div>
					
					<div class="col-sm-12">
						<div class="form-group">
							<?php echo lang('កាលបរិច្ឆេទសម្ភាសន៍ ', 'កាលបរិច្ឆេទសម្ភាសន៍ '); ?>  
							<span class="red">*</span>
							<div class="controls"> 
								<input type="text" value="<?= set_value('interview_date', date('d/m/Y')) ?>" name="interview_date" class="form-control input-sm date" />								
							</div>
						</div>
					</div>
					
					<div class="col-sm-6">
						<div class="form-group">
							<?php echo lang('សម្ភាសន៍បានចាប់ផ្តើម​ ', 'សម្ភាសន៍បានចាប់ផ្តើម'); ?>​  
							<span class="red">*</span>
							<div class="controls">
								<input type="text" value="<?= set_value('interview_started') ?>" name="interview_started" class="form-control input-sm timepicker" />								
							</div>
						</div>
					</div> 
					
					<div class="col-sm-6">
						<div class="form-group">
							<?php echo lang('សម្ភាសន៍បានបញ្ជប់​', 'សម្ភាសន៍បានបញ្ជប់​ '); ?>​ 
							<span class="red">*</span>
							<div class="controls ">
								<input type="text" value="<?= set_value('interview_ended') ?>" name="interview_ended" class="form-control input-sm timepicker" disabled />								
							</div>
						</div>
					</div>
					
					<div class="col-sm-12">
						<div class="form-group"> 
							<button type="submit" class="btn btn-success" /><?= lang("submit") ?></button>
						</div>
					</div> 
					
				</div>
					
				<div class="col-sm-2" style="padding-left: 0px; padding-right:0px;">
					<div class="form-group">
					  <?php echo lang('photo', 'photo'); ?>​ 
					  <div class="main-img-preview">
						<?php
							$photo='no_image.png';
							if(isset($application->photo)){
								$photo=$application->photo;
							}
						?>
						<img class="thumbnail img-preview" id='img_preview' src="<?= site_url('assets/uploads/'.$photo.'')?>" title="Preview Logo">
					  </div>
					</div> 
				</div>
				
				<div class="clearfix"></div>
				
			</div>
		</div> 
	</div>
	
	
</div>

<?= form_close(); ?>
 
<style type="text/css">		
	#img_preview{
		width:180px !important;
	} 
	.bootstrap-tagsinput{
		width:100%;
	}	 
</style>

<script type="text/javascript" src="<?= $assets ?>tagsinput/src/bootstrap-tagsinput.js"></script>
<link href="<?= $assets ?>tagsinput/src/bootstrap-tagsinput.css" rel="stylesheet" />

<script type="text/javascript">
	$(function(){ 
		$("input[name='interpreter']").tagsinput('items');		
		
		$('.timepicker').datetimepicker({
			format: 'hh:ii',
			startView: 0
		});	
		
		$("#case_no").autocomplete({
			source: function (request, response) {
				var q = request.term;
				$.ajax({
					type: 'GET',
					url: '<?= site_url('applications/suggestions_appointments'); ?>',
					dataType: "json",
					data: {
						q : q
					},
					success: function (data) {
						response(data);
					}
				});
			},
			minLength: 1,
			autoFocus: false,
			delay: 200,
			response: function (event, ui) {
				if (ui.content[0].id == 0) {
					$(this).removeClass('ui-autocomplete-loading');
					$(this).removeClass('ui-autocomplete-loading');
				}
			},
			select: function (event, ui) {
				event.preventDefault();
				if (ui.item.id !== 0) { 
					location.href = "<?= site_url("applications/add_interview_refugee/") ?>/"+ui.item.row.appointment_id;
				} 
			}
		});
		
		function pad(str) {
		  str = str.toString();
		  return str.length < 2 ? pad("0" + str, 2) : str;
		}		
		
	});
</script> 
