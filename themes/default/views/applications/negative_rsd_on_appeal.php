	<div class="content-cover a4">
		<div class="cover-in">
		
			<div class="content-header" style="height: 200px;"> 
			
			</div>
			
			<div class="text-center">
				<h2><?= lang("សេចក្ដីជូនដំណឹង") ?></h2>
				<h2><?= lang("ស្តីពី") ?></h2>
				<h2><?= lang("ការសម្រេចជាអវិជ្ជមាននៃការកំណត់ឋានៈជនភៀសខ្លួន") ?></h2>
				<h2><?= lang("លើបណ្តឹងជំទាស់") ?></h2>
				<h2><span class='text-english' ><?= lang("The Negative RSD on Appeal") ?></span></h2>
			</div>
			<br>
			<div class="text-content" >
				<p>លេខករណី / Case Number : <span class='text-english'><b><?= $application->case_prefix.' '.$application->case_no?></b></span></p>
				
				<table width="100%"  style="white-space:nowrap;">
					<tr>
						<td width="30"><p>ឈ្មោះអ្នកដាក់ពាក្យសុំ / Name of Applicant:&nbsp;&nbsp;</p></td>
						<td><p><b><?= $application->lastname_kh." ".$application->firstname_kh." / ".$application->lastname." ".$application->firstname?></b></p></td>
					</tr>
				</table>  
				<table width="100%" style="white-space:nowrap;"> 
					<tr> 
						<td width="50"><p>ភេទ / Sex:</p></td>
						<td width="100"><p><b><?= lang($application->gender)?></b></p></td>
						<td width="100"><p>សញ្ជាតិ / Nationality:&nbsp;</p></td>
						<td><p><b><?= $application->nationality_kh ?></b></p></td>
						<td width="100"><p>ជាតិពន្ធុ / Ethnicity:&nbsp;</p></td>
						<td><p><b><?= $application->ethnicity_kh?></b></p></td>
					</tr>
				</table>
				<table width="100%" style="white-space:nowrap;">
					<tr>
						<td width='40'><p>ឆ្នំាកំណើត / Date of Birth: &nbsp;</p></td>
						<td><p><b><?= $this->erp->khmer_date_format($application->dob)?></p></td>
					</tr>
				</table> 
				<table width="100%">
					<tr width="40"> 
						<td><p>សមាជិកក្នុងបន្ទុក / Family Composition: <b><?= $this->erp->toKhmer(($member<10?'0':'').$member)?>នាក់</b></p></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr> 
				</table>  
			</div> 
			<div class="">   
				<p>
				&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;
				ក្រ​សួង​មហា​ផ្ទៃ  នៃ​រាជរ​ដ្ឋាភិបាល​កម្ពុជា  យោង​បណ្តឹង​ជំទាស់​អោយ​ពិនិត្យ​ឡើង​វិញ​នូវ​ករណីរ​បស់​លោក អ្នក 
				យើង មានការសោកស្តាយនឹងជម្រាបថា  បន្ទាប់ពីបានពិនិត្យយ៉ាងហ្មត់ចត់នូវបណ្តឹងជំទាស់  រួចមកឃើញថា 
				លោអ្នក មិន​មាន​មូល​ហេតុថ្មី​ដែល​មាន​បន្ទុក  ដើម្បី​គាំទ្រ​នូវ​អំណះ​អំណាង​ប្តឹងទាស់  របស់​លោក​អ្នក​ដែល
				អាចផ្តល់  នូវលទ្ធផល ក្នុងការផ្លាស់ប្តូរសេចក្តីសម្រេចលើកមុនឡើយ ។ 
				ដូច្នេះ  សេចក្តី​សម្រេចជា​អវិជ្ជមាន​លើក​ទី១ ត្រូវ​បាន​រក្សា​ទុក​ដដែល។
				</p>
			</div> 
			<br/>
			<div class="text-justify">  
				<p>
				&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;
				The Ministry of Interior of the Royal Government of
				Cambodia refers to your request for reviewing your case.
				We regret to inform you that after having thoroughly examined
				your appeal, there are no substantial new elements to support 
				your claim that would application in the change of the previous decision. 
				Therefore, the first instance decision of rejection is hereby upheld.
				</p>
			</div> 
			<br/>
			<div class="content-footer​" style="float:right;">
				<table width="100%">
					<tr>
						<td class='text-center'>
							<?= $this->erp->footer_date_khmer('រាជធានីភ្នំពេញ');?> 
						</td>
					</tr>
					<tr>
						<td>
							<center> 
								ឧបនាយករដ្ឋមន្រ្តី រដ្ឋមន្រ្តីក្រសួងមហាផ្ទៃ
							</center>
						</td>
					</tr>
				</table>
			</div> 
		</div>
	</div> 
<style type="text/css">
	@media print{
		.bblack{ background: white !important; }
		.modal-content, .box, 
		.modal-header{ border:none !important; }
		.table { border:1px solid #000 !important; } 
	}
	.content-cover span, p {
		padding: 0;
		margin: 0;
		line-height:24px;
		font-size:14px;	
	}
	.content-cover center{
		font-size:14px;
		font-family: 'Moul', sans-serif !important;
	}
	.content-cover table td{
		font-size:11px;	
		line-height:26px;
		white-space: nowrap;
	}
	.content-cover{
		width:800px;
		min-height:1700px;
		background:url("assets/bg9.jpg");
		background-size: 800px auto;
		background-repeat: no-repeat; 
	}
	.cover-in{
		margin:0 50px;
		width:700px;
		position:absolute;
		white-space: wrap; 
	}	
	ul.content-list {
		list-style: none;
		padding: 0;
		margin: 0;
	}
	.content-list li {
		line-height:26px;
		font-size:11px;	
	}
	.content-list li:before {
		content: "•"; 
	}
	.content-cover h2{
		font-family: 'Moul', sans-serif !important;
	}
	hr{
		padding:0px;
		margin:0px;
		border-top:none !important;
		border-bottom:dotted 1px !important;
	} 
	.text-content table td,.text-content > p{ 
		padding-bottom:5px;
		
	} 
	.content-cover{  
		line-height:28px; 
	}  
	p {
		line-height:25px;
	}
</style>