 
<div class="box a4" >
	<div class="box-content">
		<div class="row text-size">			
			
			<?php $this->load->view($this->theme."applications/head-cover"); ?>
			
			<div class="col-sm-12 text-center">
				<h2 class="text-cover">បែបបទសម្ភាសន៍</h2>
				<h3>INTERVIEW FORM</h3>
			</div>  
			
			<style type="text/css">
				.table-data{
					white-space:nowrap;
				}
				.table-data td{
					padding:5px;
					border:1px solid #000;
				}
			</style>
			
			<?php
				$users1 = array();
				foreach($users as $user){
					$users1[$user->id] = $user->full_name;
				}
			?>
			
			 <div class="col-sm-12" style="margin-top:20px; margin-bottom: 10px;">
				<table class="table-data" width="100%">
					<tr>
						<td width="50%">
							លេខករណីៈ  <span ><?=$this->erp->toKhmer($result->case_prefix.$result->case_no) ?> </span><br/>
							Case Number:
						</td>
						<td colspan="2">
							មន្រ្តីសម្ភាសន៍ៈ  <?= strtoupper($users1[$interview->interview_officer]) ?><br/>
							Interview Officer:
						</td>
					</tr>
					
					<tr>
						<td>
							ឈ្មោះអ្នកដាក់ពាក្យសុំៈ  <?= $result->lastname_kh . " " . $result->firstname_kh ?><br/>
							Applicant’s Name: <?= $result->lastname . " " . $result->firstname ?>
						</td>
						<td colspan="2">
							អ្នកបកប្រែៈ <?= str_replace(",",",  ",$interview->interpreter);?><br/>
							Interpreter:
						</td>
					</tr>				
					
					<tr>
						<td>
							សញ្ជាតិៈ  <?= $result->nationality_kh ?><br/>
							Nationality: <?= $result->nationality ?>
						</td>
						<td colspan="2">
							<?php 
								$languages_ = array();
								foreach($languages as $language){
									$languages_[$language->id] = $language->native_name;
								}		
								
								$string_language = "";
								$langs = json_decode($interview->language);
								foreach($langs as $lang){
									$string_language .= "  ".$languages_[$lang]." - ";
								}	
							?>
							ភាសាៈ  <?= $string_language; ?> <br/>
							Language:
						</td>
					</tr>				
					
					<tr>
						<td>
							ជាតិពន្ធុៈ <?= $result->ethnicity_kh ?><br/>
							Ethnicity: <?= $result->ethnicity ?>
						</td>
						<td colspan="2">
							សាសនាៈ <?= $result->religion_kh  ?><br/>
							Religion: <?= $result->religion  ?>
						</td>
					</tr>
					
					<tr>
						<td>
							កន្លែង និងថ្ងៃខែឆ្នាំកំណើតៈ <?=$result->pob_kh; ?><br/>
							POB & DOB:  <?=$this->erp->khmer_date_format($result->dob) ?>
						</td>
						<td colspan="2">
							ទីកន្លែងស្នាក់នៅចុងក្រោយៈ    
							<?= $result->address_o_kh ?>, 
							<?= $commune_o->native_name; ?>, 
							<?= $district_o->native_name; ?>, 
							<?= $province_o->native_name; ?>
							<br/>
							Last place of residence:
						</td>
					</tr>
					
					<tr>
						<td> 
							<?php $place = $this->applications->getOfficeById($interview->interview_place) ;?>
							កន្លែង និងកាលបរិច្ឆេទៈ  <?= $place->office ?><br/>
							Interview place & Date: <?=$this->erp->khmer_date_format($interview->interview_date) ?> 
						</td>
						<td>
						
							<?php  
							$start = $interview->interview_started; 
							$end = $interview->interview_ended;
							?>
							សម្ភាសន៍ចាប់ផ្តើមៈ <span ><?=$this->erp->toKhmer(substr($start,0,5)) ?></span><br/>
							Interview started:
						</td>
						<td>
							សម្ភាសន៍បញ្ចប់ៈ <span > <?=  $this->erp->toKhmer(substr($end,0,5)) ?></span><br/>
							Interview ended:
						</td>
					</tr>
					
				</table>
			</div> 
			<div class='col-sm-12 text-center' > 
				<p class='bold'>សមាជិកគ្រួសារ </p>
				<p><u>Family Members</u></p>
			</div> 
			
			<?php if(!empty($result_members)){ ?>
			<table width='50%' style='margin:auto; ' class='table-padding'>
				<tr>
					<td><?= lang("ល.រ") ?></td>
					<td><?= lang("ឈ្មោះ") ?></td>
					<td><?= lang("ភេទ") ?></td>
					<td><?= lang("ថ្ងៃខែឆ្នាំកំណើត") ?></td>
				</tr>
				<?php foreach($result_members as $key => $member){ ?>
					<tr>
						<td><?= $this->erp->toKhmer(($key+1)) ?></td>
						<td>
							<?= $member->lastname_kh.' '.$member->firstname_kh ?>&nbsp;
						</td>
						<td>
							<?= $this->erp->genderToKhmer($member->gender) ?>&nbsp;
						</td>
						<td>
							<?= $this->erp->khmer_date_format($member->dob) ?>&nbsp;
						</td>
					</tr>
				<?php } ?>
			</table>
			<?php } ?>
			
			<div class="col-sm-12 text-center">
				<p class="bold">មុនសម្ភាសន៍ </p>
				<p><u>Pre Interview</u></p>
			</div>
			
			<div class="col-sm-12">
				<table class="table-text" style="margin-top:10px;">
					<tr> 
						<td>- &nbsp;អ្នកបកប្រែត្រូវបានសួរថា តើគាត់ស្តាប់មន្ត្រីសម្ភាសន៍យល់ ឬមិនយល់ទេ</td>
					</tr>
					<tr>
						<td>&nbsp;&nbsp;&nbsp;Interpreter asked whether or not he understands Interview Officer (IO)</td>
					</tr>
					<tr>
						<td>- &nbsp;មន្ត្រីសម្ភាសន៍ណែនាំអ្នកបកប្រែ-សួរអ្នកដាក់ពាក្យសុំថា តើគាត់ស្តាប់អ្នកបកប្រែយល់ឬទេ</td>
					</tr>
					<tr>
						<td>&nbsp;&nbsp;&nbsp;I O introduces the interpreter and asks s/he can understand the interpreter</td>
					</tr>
					<tr>
						<td>- &nbsp;សួរអ្នកដាក់ពាក្យសុំថា តើគាត់មានអារម្មណ៍និងសុខភាពល្អឬទេ</td>
					</tr>
					<tr>
						<td>&nbsp;&nbsp;&nbsp;Applicant is asked if he is feeling fit and well</td>
					</tr>
					<tr>
						<td>- &nbsp;ការពន្យល់នៃនីតិវិធីនៃកិច្ចសម្ភាសន៍ឋានៈជនភៀសខ្លួន </td>
					</tr>
					<tr>
						<td>&nbsp;&nbsp;&nbsp;Explanation of RSD interview procedures</td>
					</tr>
					<tr>
						<td>- &nbsp;ការរក្សាការសម្ងាត់</td>
					</tr>
					<tr>
						<td>&nbsp;&nbsp;&nbsp;Confidentiality</td>
					</tr>
					<tr>
						<td>- &nbsp;កាតព្វកិច្ចប្រាប់ពីការពិត</td>
					</tr>
					<tr>
						<td>&nbsp;&nbsp;&nbsp;Obligation to tell the truth</td>
					</tr>
					<tr>
						<td>- &nbsp;កាតព្វកិច្ចដើម្បីសហប្រតិបត្តិការ</td>
					</tr>
					<tr>
						<td>&nbsp;&nbsp;&nbsp;Obligation to cooperate</td>
					</tr>
					<tr>
						<td>- &nbsp;តើអ្នកដាក់ពាក្យសុំមានសំណួរអ្វីទៀតទេ?</td>
					</tr>
					<tr>
						<td>&nbsp;&nbsp;&nbsp;Applicant has any questions?</td>
					</tr>
				</table>
			</div>
			
			<div class="col-sm-12 text-center">
				<p class="bold">កំណត់ហេតុសម្ភាសន៍</p>
				<p><u>Interview Records</u></p>   
			</div> 
			
			<div class="col-sm-12">
				<table width='100%' class='table-padding tbl-record' style="text-justify:inner-word;" border='1'>

				<?php	  
				if($appointment->member_id > 0 ){ 
					$interviewRecords = $this->applications->getInterviewRecords($interview->id, $appointment->member_id);		 
					$row = $this->applications->getMemberId($appointment->member_id);
					?>
						<tr> 
							<td colspan="2">
								<?=  $row->lastname_kh." ".$row->firstname_kh; ?>
							</td>
						</tr>
						<?php foreach($interviewRecords as $key => $record) {?>
							<tr> 
								<td width="30" class="text-center text-english" ><?= ($key+1); ?></td>
								<td>
									<b><?= trim($record->questions); ?></b>
									<br/>
									<?= $record->answers?>
								</td> 
							</tr>
						<?php } ?>

				<?php } else{ ?> 
					<tr> 
						<td colspan="2">
							<?=  $result->lastname_kh." ".$result->firstname_kh; ?>
						</td>						
					</tr> 
					<?php 
						$interviewRecords = $this->applications->getInterviewRecords($interview->id, 0);
						foreach($interviewRecords as $key => $interview_record) {?>								
							<tr> 
								<td width="30" class="text-center text-english" ><?=  ($key+1); ?></td>
								<td>
									<b><?= trim($interview_record->questions); ?></b>
									<br/>
									<?= $interview_record->answers?>
								</td> 
							</tr>								
					<?php } ?>  
				<?php  
					$appoinments = json_decode($appointment->appointment_member); 					
					if($appoinments){
						foreach($appoinments as $appoinment){							
							$interviewRecords = $this->applications->getInterviewRecords($interview->id, $appoinment);
							$row = $this->applications->getMemberId($appoinment);												
						?>
							<tr> 
								<td colspan="2">
									<?=  $row->lastname_kh." ".$row->firstname_kh; ?>
								</td>
							</tr>
							
							<?php foreach($interviewRecords as $key => $interview_record) { ?>
								<tr> 
									<td width="30" class="text-center text-english" ><?=  ($key+1); ?></td>
									<td>
										<b><?= trim($interview_record->questions); ?></b>
										<br/>
										<?= $interview_record->answers?>
									</td> 
								</tr>
								
							<?php } ?>
						
						<?php } ?>
						
					<?php } ?>
										
				<?php }  ?>
				
				</table>
			</div>
			
			<div class="col-sm-12 text-center" style="margin-top:20px">
				<p class="bold">ក្រោយសម្ភាសន៍</p>
				<p><u>Post Interview</u></p>
			</div> 
			
			<div class="col-sm-12 text-justify​" style="margin-top:20px;">
				<p>អ្នកដាក់ពាក្យសុំបានត្រួតពិនិត្យព័ត៌មានដែលបានកត់ត្រា ហើយបញ្ជាក់ចំពោះភាពត្រឹមត្រូវនៃការកត់ត្រា នៃកិច្ចសម្ភាសន៍។</p>
				<p>The applicant has reviewed the information recorded and attests to the accuracy of the transcription of the interview.</p>
			</div>
			
			<div class="col-sm-12 text-left">
				<p>សេចក្តីបញ្ជាក់ធ្វើដោយអ្នកដាក់ពាក្យសុំ </p> 
				Clarification made by applicant:<br/> 
				<p><?php echo $interview->clarification_applicant;?></p>
 			</div>
			
			<div class="col-sm-12">
				<div class="div-left">
				<?php  
					$str = $interview->interpreter;
					$arr = explode(",",$str); 
					for($i=0; $i < count($arr); $i++){  
					?>   
						<p>
							ហត្ថលេខាអ្នកបកប្រែ<br/>
							Interpreter's Signature.................
					  </p> 
				<?php }	?>
				</div>
				<div class="div-right text-justify​" style="white-space:nowrap;"> 
					<p>ហត្ថលេខាអ្នកដាក់ពាក្យសុំ <br/>
					 Applicant's Finger Print and Signature ..............
					 </p>
					<?php for($i=0; $i < count($result_members); $i++){?> 
						<p>
							ហត្ថលេខាសមាជិកគ្រួសារ <br/>
							Family's Signature .............. 
						 </p>
					<?php }?>
				</div>  
			</div>
		​
			<div class="col-sm-12 text-justify" >  
				<table>
					<tr>
						<td><p>ហត្ថលេខាអ្នកសម្ភាសន៍ / Interviewers's Signature</p></td>
					</tr> 
					<tr>  
						<td><br/>&nbsp;<hr/></td>​
					</tr>​
				</table>​
			</div>
		
	</div>
</div>

<style type="text/css">
	@media print{
		.bblack{ background: white !important; }
		.modal-content, .box, 
		.modal-header{ border:none !important; }
		.table { border:1px solid #EEE !important; }
		.db-width{width:60% !important}
		.place-width{width:65% !important}
		.text-width{width:25% !important}
		.interpreter-text{width:37% !important}
		.applicant-text{width:35% !important}
		.box-content{
			padding: 20px !important;
		}
	}
	.table-padding td{
		 padding:2px;
	}
	hr{
		padding:0px;
		margin:0px;
		border-top:none !important;
		border-bottom:dotted 1px !important;
	}
	.border-bottom{
		border-bottom:1px solid #000;
	}
	.nopadding {
	   padding: 0 !important;
	   margin: 0 !important; 
	}
	.table-none td{
		border: 1px solid #000;
		padding:12px;
	}
	@font-face {
		font-family: "Khmer OS Siemreap";
		src: url(themes/default/assets/fonts/KhmerOSSiemreap.TTF) format("truetype");
	}
	.box{  
		background: white;
		line-height:28px; 
	} 
	.text-english {
		font-family: "Times New Roman", sans-serif;
	}
</style>