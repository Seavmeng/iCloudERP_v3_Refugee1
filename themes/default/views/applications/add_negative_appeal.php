<div class="modal-dialog modal-md">
	<div class="modal-content">
		<div class="modal-header">				
			<button type="button" class="close" data-dismiss="modal">
				<span aria-hidden="true"><i class="fa fa-2x">&times;</i></span>
				<span class="sr-only"><?=lang('close');?></span>
			</button>
			<h4 class="modal-title" id="payModalLabel"><?= lang('add_negative'); ?></h4>
		</div>
		
		<?php  echo form_open_multipart("applications/add_negative_appeal/"); ?>
		
		<div class="modal-body"> 
			<div class="col-sm-12">				
				
				<div class="form-group">
					<?php echo lang('case_no', 'case_no'); ?>​ 
					<span class="red">*</span>
					<div class="control">
						<input type="text" class="form-control input-sm case_no" id="case_no" />
						<input type="hidden" name="application_id"  /> 
					</div> 
				</div>
				
				<div class="form-group">
					<?php echo lang('ថ្ងៃបណ្តឹងឧទ្ធរណ៏', 'ថ្ងៃបណ្តឹងឧទ្ធរណ៏'); ?>
					<span class="red">*</span>​
					<div class="controls">
						<input type="text" name="appeal_date" class="form-control date input-sm" />							
					</div>
				</div>
				
			</div> 
			<div class="clearfix"></div>
		</div>
		<div class="modal-footer">
		   <?php echo form_submit('submit', lang('submit'), 'class="btn btn-primary save-data"'); ?>
		</div>
		<?php  echo form_close(); ?>		
	</div>
</div> 
<?= $modal_js ?> 
<style type="text/css"> 
	ul.ui-autocomplete {
		z-index: 1100 !important;
	}
</style>
<script type="text/javascript">
	$(".save-data").on('click',function(event){	 
		var application_id = $('input[name="application_id"]').val();
		var appeal_date = $('input[name="appeal_date"]').val();
		if(application_id == '' || appeal_date == ''){ 
			bootbox.alert('<?= lang("please_select_all");?>');
			return false;
		} 
	});
	$(function(){
		$(".case_no").autocomplete({
			source: function (request, response) {
				var q = request.term;
					$.ajax({
						type: 'get',
						url: '<?= site_url('applications/suggestions_caseno_negative_appeal'); ?>',
						dataType: "json",
						data: {
							q : q
						},
						success: function (data) {
							response(data);
						}
					});
			},
			minLength: 1,
			autoFocus: false,
			delay: 200,
			response: function (event, ui) {
				if (ui.content[0].id == 0) {
					$(this).removeClass('ui-autocomplete-loading');
					$(this).removeClass('ui-autocomplete-loading');
				}
			},
			select: function (event, ui) {
				event.preventDefault();
				if (ui.item.id !== 0) { 					
					var id = ui.item.id;										
					
					$("input[name='application_id'").val(ui.item.row.id);
					$(this).val(ui.item.label);
				} 
			}
		});   
	});
</script>
