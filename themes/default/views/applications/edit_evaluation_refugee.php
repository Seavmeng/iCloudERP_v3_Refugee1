<ul id="myTab" class="nav nav-tabs">
	<li class="">
		<a href="#content_1" class="tab-grey">
			<?= lang("ប្រវត្ដិរូបសង្ខេបរបស់អ្នកដាក់ពាក្យសុំ"); ?>
			<i class="fa fa-chevron-right" aria-hidden="true"></i>
		</a>
	</li>
	<li class="">
		<a href="#content_2" class="tab-grey">
			<?= lang("ផ្នែកទី១"); ?> / <?= lang("Part 1"); ?>
			<i class="fa fa-chevron-right" aria-hidden="true"></i>
		</a>
	</li>
	<li class="">
		<a href="#content_3" class="tab-grey">
			<?= lang("ផ្នែកទី២"); ?> / <?= lang("Part 2"); ?>
			<i class="fa fa-chevron-right" aria-hidden="true"></i>
		</a>
	</li>
	<li class="">
		<a href="#content_4" class="tab-grey">
			<?= lang("ផ្នែកទី៣"); ?> / <?= lang("Part 3"); ?>
			<i class="fa fa-chevron-right" aria-hidden="true"></i>
		</a>
	</li>
	
	<li class="">
		<a href="#content_5" class="tab-grey">
			<?= lang("ផ្នែកទី៤"); ?> / <?= lang("Part 4"); ?>
			<i class="fa fa-chevron-right" aria-hidden="true"></i>
		</a>
	</li>
	
	<li class="">
		<a href="#content_6" class="tab-grey">
			<?= lang("ផ្នែក ៥"); ?> / <?= lang("Part 5"); ?>
			<i class="fa fa-chevron-right" aria-hidden="true"></i>
		</a>
	</li>
	
	<li class="">
		<a href="#content_7" class="tab-grey">
			<?= lang("ផ្នែក ៦ អនុសាសន៍"); ?> / <?= lang("Part 6"); ?>
			<i class="fa fa-chevron-right" aria-hidden="true"></i>
		</a>
	</li>
	<li class="">
		<a href="#content_8" class="tab-grey">
			<?= lang("កំណត់ត្រាសម្ភាសន៍") ?>
			<i class="fa fa-chevron-right" aria-hidden="true"></i>
		</a>
	</li>
	
</ul>

<?= form_open_multipart("applications/edit_evaluation_refugee/".$id); ?>
<div class="tab-content">

	<div id="content_1" class="tab-pane fade in">
		<div class="box">
			<div class="box-header">
				<h2 class="blue">
					<i class="fa-fw fa fa-barcode"></i>
					<?= lang("ប្រវត្ដិរូបសង្ខេបរបស់អ្នកដាក់ពាក្យសុំ") ?>
				</h2>
			</div>
			
			<?php 
				$countries_ = array(lang("select"));
				foreach($countries as $country){
					$countries_[$country->id] = $country->country;
				} 
				$provinces_ = array(lang("select"));
				foreach($provinces as $province){
					$provinces_[$province->id] = $province->name;
				} 
				$districts_ = array(lang("select"));
				foreach($districts as $district){
					$districts_[$district->id] = $district->name;
				} 
				$communes_ = array(lang("select"));
				foreach($communes as $commune){
					$communes_[$commune->id] = $commune->name;
				} 
			?>

			<div class="box-content">
			
				<div class="row">
				
					<div class="col-sm-5" style="padding-right:0px;">
						
						<div class="col-sm-12">
							<div class="form-group">
								<?php echo lang('case_no', 'case_no'); ?>​ 
								<span class="red">*</span>
								<div class="controls">
									<input type="text" value="<?= set_value('case_no', $application->case_prefix.$application->case_no) ?>" name="case_no" id="case_no" class="form-control input-sm" />								
								</div>
							</div>
						</div> 
						
						<div class="col-sm-6">
							<div class="form-group">
								<?php echo lang('firstname', 'firstname'); ?>
								<small class="red">* (<?= lang('just_information'); ?>)</small>
								<input class="form-control input-sm" type="text" name="firstname" value="<?= set_value("firstname", $application->firstname_kh) ?>" />
							</div>
						</div>
						
						<div class="col-sm-6">
							<div class="form-group">
								<?php echo lang('lastname', 'lastname'); ?>
								<small class="red">* (<?= lang('just_information'); ?>)</small>
								<input class="form-control input-sm" type="text" name="lastname" value="<?= set_value("lastname", $application->lastname_kh) ?>" />
							</div>
						</div>
						
						<div class="col-sm-6">
							<div class="form-group">
								<?php echo lang('ភេទ', 'ភេទ'); ?>
								<small class="red">* (<?= lang('just_information'); ?>)</small>
								<div class="controls">
									<label class="radio-inline">
										<input type="radio" value="male" <?=($application->gender=="male"?"checked":""); ?> name="gender"> <?= lang('male'); ?>
									</label>
									<label class="radio-inline">
										<input type="radio" value="female" <?=($application->gender=="female"?"checked":""); ?> name="gender"> <?= lang('female'); ?>
									</label> 
								</div>
							</div>
						</div>
						
						<div class="col-sm-6">
							<div class="form-group">
								<?php echo lang('ថ្ងៃខែឆ្នាំកំណើត', 'ថ្ងៃខែឆ្នាំកំណើត'); ?>
								<small class="red">* (<?= lang('just_information'); ?>)</small>
								<div class="controls">
									<input type="text" value="<?= set_value('dob', $this->erp->hrsd($application->dob)) ?>" name="dob" class="form-control input-sm date" />
								</div>
							</div>
						</div>
						
						<div class="col-sm-6">		
							<div class="form-group">
								<?php echo lang('សញ្ជាតិ  ៖   ', 'សញ្ជាតិ  ៖   '); ?>​ 
								<span class="red">*</span>
								<div class="controls">
									<input type="text" value="<?= set_value('nationality', $application->nationality_kh) ?>" name="nationality" class="form-control input-sm" />								
								</div>
							</div>
						</div>
						
						<div class="col-sm-6">
							<div class="form-group">
								<?php echo lang('សាសនា    ៖   ', 'សាសនា   ៖   '); ?>​ 
								<span class="red">*</span>
								<div class="controls">
									<input type="text" value="<?= set_value('religion',$application->religion_kh) ?>" name="religion" class="form-control input-sm" />								
								</div>
							</div>
						</div>
						
						<div class="col-sm-12">
							<div class="panel panel-warning">
								<div class="panel-heading"><?php echo lang("address","address") ?> / <?= lang("address") ?></div>
								<div class="panel-body" style="padding: 5px;">
									<div class="col-lg-6">
										<div class="form-group">
											<?php echo lang('country', 'country'); ?>
											<div class="controls">
												<?php echo form_dropdown('country', $countries_, $application->country, ' class="form-control" '); ?>												
											</div>
										</div>
									</div>
									
									<div class="col-lg-6">
										<div class="form-group">
											<?php echo lang('province', 'province'); ?>
											<div class="controls">
												<?php echo form_dropdown('province', $provinces_, $application->province, ' class="form-control" '); ?>												
											</div>
										</div>
									</div>
									
									<div class="col-lg-6">
										<div class="form-group">
											<?php echo lang('district', 'district'); ?>
											<div class="controls">
												<?php echo form_dropdown('district', $districts_, $application->district, ' class="form-control" '); ?>												
											</div>
										</div>
									</div>
									
									<div class="col-lg-6">
										<div class="form-group">
											<?php echo lang('commune', 'commune'); ?>
											<div class="controls">
												<?php echo form_dropdown('commune', $communes_, $application->commune, ' class="form-control" '); ?>												
											</div>
										</div>
									</div>

									<div class="col-sm-12 text-nowrap">
										<div class="form-group">
											<?php echo lang('អាសយដ្ឋាន', 'អាសយដ្ឋាន'); ?> (<?php echo lang("KH") ?>)
											<span class="red">*</span>
											<div class="controls">
												<input type="text" value="<?= set_value('address_kh', $application->address_kh) ?>" name="address_kh" class="form-control input-sm" />
											</div>
										</div>
									</div>
									<div class="col-sm-12">
										<div class="form-group">
											<?php echo lang('អាសយដ្ឋាន', 'អាសយដ្ឋាន'); ?> (<?php echo lang("EN") ?>)
											<span class="red">*</span>
											<div class="controls">
												<input type="text" value="<?= set_value('address', $application->address) ?>" name="address" class="form-control input-sm" />
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="clearfix"></div>
						</div>
					
					</div>
					
					<div class="col-sm-5" style="padding-left:0px;"> 
						
						<div class="col-sm-12">
							<div class="form-group">
								<?php echo lang('interview_officer', 'interview_officer'); ?>​ 
								<span class="red">*</span>
								<div class="controls">
									<?php 
										$users_ = array(lang("select"));
										foreach($users as $user){
											$users_[$user->id] = ucfirst($user->last_name)." ".ucfirst($user->first_name);
										} 
									?>
									<div class="controls"> 								
										<?php echo form_dropdown('interview_officer', $users_, $interview->interview_officer, ' class="form-control" '); ?>							
									</div>
								</div>
							</div>
						</div>
						
						<div class="col-sm-12">
							<div class="form-group">
								<?php echo lang('ឈ្មោះមន្ដ្រីវិនិច្ឆ័យ   ', 'ឈ្មោះមន្ដ្រីវិនិច្ឆ័យ  '); ?>​ 
								<span class="red">*</span>
								<div class="controls">
									<div class="controls"> 								
										<?php echo form_dropdown('evaluate_officer', $users_, $evaluation->evaluate_officer, ' class="form-control" '); ?>							
									</div>
								</div>
							</div>
						</div>
						
						<div class="col-sm-12">
							<div class="form-group">
								<?php echo lang('សម្ភាសន៍  ', 'សម្ភាសន៍  '); ?> (<?php echo lang("លើកទី") ?>)
								<div class="controls">
									<input type="number" name="number_of_application" value="<?= set_value('number_of_application',1) ?>" class="form-control input-sm" />
								</div>
							</div>
						</div>
						
						<div class="col-sm-12">
							<div class="form-group">
								<?php echo lang('ពាក់ព័ន្ឋករណីលេខ     ៖   ', 'ពាក់ព័ន្ឋករណីលេខ    ៖   '); ?>​ 
								<span class="red">*</span>
								<div class="controls">
									<input type="text" readonly value="<?= set_value('last_case_no', $application->related_case) ?>" name="last_case_no" class="form-control input-sm" />								
								</div>
							</div>
						</div>
						
						<div class="col-sm-12">
							<div class="form-group">
								<?php echo lang('ភាសា​', 'ភាសា​'); ?>
								<span class="red">*</span>
								<div class="controls">
									<?php 
										$languages_ = array(lang("select"));
										foreach($languages as $language){
											$languages_[$language->id] = $language->language;
										} 
									?>
									<div class="controls"> 								
										<?php echo form_dropdown('language[]', $languages_, json_decode($interview->language), ' class="form-control"  multiple="multiple"'); ?>							
									</div>
								</div>
							</div>
						</div>
						
						<div class="col-sm-12">
							<div class="form-group">
								<?php echo lang('អ្នកបកប្រែ', 'អ្នកបកប្រែ '); ?>  
								<span class="red">*</span>
								<div class="controls">
									<input type="text" value="<?= set_value('interpreter', $interview->interpreter) ?>" name="interpreter" class="form-control input-sm" />								
								</div>
							</div>
						</div>
						
						<div class="col-sm-12">
							<div class="form-group">
								<?php echo lang('កាលបរិច្ឆេទសម្ភាសន៍ ', 'កាលបរិច្ឆេទសម្ភាសន៍ '); ?>  
								<span class="red">*</span>
								<div class="controls"> 
									<input type="text" value="<?= set_value('interview_date', $this->erp->hrsd($interview->interview_date)) ?>" name="interview_date" class="form-control input-sm date" />								
								</div>
							</div>
						</div>
						
						
						<div class="col-sm-12" >
							<div class="form-group">
								<?php echo lang('ចំនួនមនុស្សដែលរួមដំណើរជាមួយអ្នកដាក់ពាក្យសុំ     ៖   ', 'ចំនួនមនុស្សដែលរួមដំណើរជាមួយអ្នកដាក់ពាក្យសុំ    ៖   '); ?>​ 
								<span class="red">*</span>
								<div class="controls">
									<input type="text" readonly value="<?= set_value('member_with') ?>" name="member_with" class="form-control input-sm" />								
								</div>
							</div>
						</div>
						
						<div class="col-sm-12">
							<button class="btn btn-success btn-submit save" ><?= lang("submit") ?></button>
						</div>
					
					</div>
					
					<div class="col-sm-2" style="padding-left: 0px;">
						<div class="form-group">
						  <?php echo lang('photo', 'photo'); ?>​ 
						  <div class="main-img-preview">
							<?php
								$photo='no_image.png';
								if(isset($application->photo)){
									$photo=$application->photo;
								}
							?>
							<img class="thumbnail img-preview" id='img_preview' src="<?= site_url('assets/uploads/'.$photo.'')?>" title="Preview Logo">
						  </div>
						</div>
					</div>
				
					<div class="clearfix"></div>
				
				</div>
				
			</div>
			
		</div>
	</div>
	
	<div id="content_2" class="tab-pane fade in">
		<div class="box">
			<div class="box-header">
				<h2 class="blue">
					<i class="fa-fw fa fa-barcode"></i>
					<?= lang("ផ្នែកទី១-អំណះអំណាងសង្ខេប") ?>
				</h2>
			</div>
			
			
			<div class="box-content">
				<div class="form-group">
					<div class="controls">
						<p><?= $this->applications->getQuestionKh(1); ?></p>
						<input type="hidden" value="<?= $this->applications->getQuestionKh(1) ?>" name="q1" />		
						<input type="hidden" value="1" name="g1" />							
					</div>
				</div>
				
				<div class="form-group">
					<div class="controls">
						<textarea name="e1"> 
							<?php $a = 1;/// initial no. answer?> 
							<?= set_value("e1",$evaluation_detail[$a-1]->answer);?>
						</textarea>
					</div>
				</div>
			</div>
			
		</div>
	</div>
	
	<div id="content_3" class="tab-pane fade in">
		<div class="box">
			<div class="box-header">
				<h2 class="blue">
					<i class="fa-fw fa fa-barcode"></i>
					<?= lang("ផ្នែកទី២-ការវាយតំលៃអំពីភាពគួរជឿជាក់បាន") ?>
				</h2>
			</div>
			
			<div class="box-content">
				
				<div class="form-group">
					<div class="controls">
						<p><?= $this->applications->getQuestionKh(2); ?></p>
						<input type="hidden" value="<?= $this->applications->getQuestionKh(2) ?>" name="q2" />			
						<input type="hidden" value="2" name="g2" />							
					</div>
				</div>
				
				<div class="form-group">
					<div class="controls">
						<?php $a = 2;/// initial no. answer?> 
						<label class="radio-inline">
							<input type="radio" value="1" 	<?= ($evaluation_detail[$a-1]->is_yes==1?'checked':'');?> name="<?php echo 'a'.$a?>"><?= lang("បាទ") ?>
						</label>
						<label class="radio-inline">
							<input type="radio" value="0"  <?= ($evaluation_detail[$a-1]->is_yes==0?'checked':'');?> name="<?php echo 'a'.$a?>"><?= lang("ទេ") ?>
						</label> 
					</div>
				</div> 
				
				<div class="form-group">
					<div class="controls"> 
						<p><?= lang("សូមរៀបរាប់ចំណុចសំខាន់ៗនៃអំណះអំណាងនៅខាងក្រោម៖"); ?></p>
						<textarea name="e2">
							<?= set_value("e2",$evaluation_detail[$a-1]->answer);?>
						</textarea>
					</div>
				</div>
				
				<div class="form-group">
					<div class="controls"> 
						<p><?= $this->applications->getQuestionKh(3); ?></p>
						<input type="hidden" value="<?= $this->applications->getQuestionKh(3) ?>" name="q3" />			
						<input type="hidden" value="2" name="g3" />						
					</div>
				</div>
				
				<div class="form-group">
					<div class="controls"> 
						<div class="controls"> 
							<?php $a = 3;?>
							<label class="radio-inline">
								<input type="radio" value="1" 	<?= ($evaluation_detail[$a-1]->is_yes==1?'checked':'');?> name="<?php echo 'a'.$a?>"><?= lang("បាទ") ?>
							</label>
							<label class="radio-inline">
								<input type="radio" value="0"  <?= ($evaluation_detail[$a-1]->is_yes==0?'checked':'');?> name="<?php echo 'a'.$a?>"><?= lang("ទេ") ?>
							</label> 
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="controls"> 
						<p><?= lang("សូមរៀបរាប់ចំណុចសំខាន់ៗនៃអំណះអំណាងនៅខាងក្រោម៖"); ?></p>
						<textarea name="e3">
							<?= set_value("e3",$evaluation_detail[$a-1]->answer);?>
						</textarea>
					</div>
				</div>
				<div class="form-group">
					<div class="controls">
						<p><?= $this->applications->getQuestionKh(4); ?></p>
						<input type="hidden" value="<?= $this->applications->getQuestionKh(4) ?>" name="q4" />			
						<input type="hidden" value="2" name="g4" />						
					</div>
				</div>
				
				<div class="form-group">
					<div class="controls">
						<?php $a = 4;?>
							<label class="radio-inline">
								<input type="radio" value="1" 	<?= ($evaluation_detail[$a-1]->is_yes==1?'checked':'');?> name="<?php echo 'a'.$a;?>"><?= lang("បាទ") ?>
							</label>
							<label class="radio-inline">
								<input type="radio" value="0"  <?= ($evaluation_detail[$a-1]->is_yes==0?'checked':'');?> name="<?php echo 'a'.$a;?>"><?= lang("ទេ") ?>
							</label> 
					</div>
				</div>
				
				<div class="form-group">
					<div class="controls">
						<p><?= lang("សូមពន្យល់ដោយយោងទៅលើព័ត៌មានពីប្រទេសដើមជាក់លាក់៖"); ?></p>
						<textarea name="e4">
							<?= set_value("e4",$evaluation_detail[$a-1]->answer);?>
						</textarea>
					</div>
				</div>
				
				<div class="form-group">
					<div class="controls">
						<p><?= $this->applications->getQuestionKh(5); ?></p>
						<input type="hidden" value="<?= $this->applications->getQuestionKh(5) ?>" name="q5" />	
						<input type="hidden" value="2" name="g5" />	
						<?php $a = 5;?>						
					</div>
				</div>
				
				<div class="form-group">
					<div class="controls"> 
							<label class="radio-inline">
								<input type="radio" value="1" 	<?= ($evaluation_detail[$a-1]->is_yes==1?'checked':'');?> name="<?php echo 'a'.$a;?>"><?= lang("បាទ") ?>
							</label>
							<label class="radio-inline">
								<input type="radio" value="0"  <?= ($evaluation_detail[$a-1]->is_yes==0?'checked':'');?> name="<?php echo 'a'.$a;?>"><?= lang("ទេ") ?>
							</label> 
					</div>
				</div>
				<div class="form-group">
					<div class="controls"> 
						<p><?= lang("សូមរៀបរាប់ចំណុចសំខាន់ៗនៃអំណះអំណាងនៅខាងក្រោម៖"); ?></p>
						<textarea name="e5">
							<?= set_value("e5",$evaluation_detail[$a-1]->answer);?>
						</textarea>
					</div>
				</div>
				<div class="form-group">
					<div class="controls">
						<p><?= $this->applications->getQuestionKh(6); ?></p>
						<input type="hidden" value="<?= $this->applications->getQuestionKh(6) ?>" name="q6" />	
						<input type="hidden" value="2" name="g6" />								
					</div>
				</div>
				
				<div class="form-group">
					<?php $a=6;?>
					<div class="controls"> 
							<label class="radio-inline">
								<input type="radio" value="1" 	<?= ($evaluation_detail[$a-1]->is_yes==1?'checked':'');?> name="<?php echo 'a'.$a;?>"><?= lang("បាទ") ?>
							</label>
							<label class="radio-inline">
								<input type="radio" value="0"  <?= ($evaluation_detail[$a-1]->is_yes==0?'checked':'');?> name="<?php echo 'a'.$a;?>"><?= lang("ទេ") ?>
							</label> 
					</div>
				</div>
				
				<div class="form-group">
					<div class="controls">  
						<textarea name="e6">
							<?= set_value("e6",$evaluation_detail[$a-1]->answer);?>
						</textarea>
					</div>
				</div>
				<div class="form-group">
					<div class="controls">
						<?php $a=7;?>
						<p><?= $this->applications->getQuestionKh(7); ?></p>
						<input type="hidden" value="<?= $this->applications->getQuestionKh(7) ?>" name="q7" />	
						<input type="hidden" value="2" name="g7" />								
					</div>
				</div>
				
				<div class="form-group">
					<div class="controls">  
							<label class="radio-inline">
								<input type="radio" value="1" 	<?= ($evaluation_detail[$a-1]->is_yes==1?'checked':'');?> name="<?php echo 'a'.$a;?>"><?= lang("បាទ") ?>
							</label>
							<label class="radio-inline">
								<input type="radio" value="0"  <?= ($evaluation_detail[$a-1]->is_yes==0?'checked':'');?> name="<?php echo 'a'.$a;?>"><?= lang("ទេ") ?>
							</label> 
					</div>
				</div>
				
				<div class="form-group">
					<div class="controls">
						<p><?= lang("សូមបញ្ជាក់ថាតើសមាសធាតុសំខាន់ណាខ្លះដែលត្រូវបានរកឃើញថាគួរអោយជឿជាក់/មិនគួរអោយជឿជាក់ បាន។"); ?></p>
						<textarea name="e7">
							<?= set_value("e7",$evaluation_detail[$a-1]->answer);?>
						</textarea>
					</div>
				</div>
				
			</div>
			
		</div>
	</div>
	
	<div id="content_4" class="tab-pane fade in">
		<div class="box">
			<div class="box-header">
				<h2 class="blue">
					<i class="fa-fw fa fa-barcode"></i>
					<?= lang("ផ្នែកទី៣- ការវាយតំលៃសី្តពីការរាប់បញ្ចូល-អនុសញ្ញាឆ្នាំ១៩៥១/ពិធីសារឆ្នាំ១៩៦៧") ?>
				</h2>
			</div>
			
			<div class="box-content">
				
				<h2><?= lang("“ភាពភ័យខ្លាច” បែបអត្តទិដ្ឋិ   (ការភ័យខ្លាចរបស់បុគ្គល) "); ?></h2>
				
				<div class="form-group">
					<div class="controls">
						<?php $a = 8;/// initial no. answer?> 
						<p><?= $this->applications->getQuestionKh(8); ?></p>
						<input type="hidden" value="<?= $this->applications->getQuestionKh(8) ?>" name="q8" />	
						<input type="hidden" value="3" name="g8" />								
					</div>
				</div>
				
				<div class="form-group">
					<div class="controls">
						<label class="radio-inline">
							<input type="radio" value="1" 	<?= ($evaluation_detail[$a-1]->is_yes==1?'checked':'');?> name="<?php echo 'a'.$a;?>"><?= lang("បាទ") ?>
						</label>
						<label class="radio-inline">
							<input type="radio" value="0"  <?= ($evaluation_detail[$a-1]->is_yes==0?'checked':'');?> name="<?php echo 'a'.$a;?>"><?= lang("ទេ") ?>
						</label> 
					</div>
				</div>
				
				<div class="form-group">
					<div class="controls">  
						<textarea name="e8">
							<?= set_value("e8",$evaluation_detail[$a-1]->answer);?>
						</textarea>
					</div>
				</div>
				
				<div class="form-group">
					<div class="controls">
						<?php $a = 9;/// initial no. answer?> 
						<p><?= $this->applications->getQuestionKh(9); ?></p>
						<input type="hidden" value="<?= $this->applications->getQuestionKh(9) ?>" name="q9" />	
						<input type="hidden" value="3" name="g9" />								
					</div>
				</div>
				
				<div class="form-group">
					<div class="controls">
						<label class="radio-inline">
							<input type="radio" value="1"  	<?= ($evaluation_detail[$a-1]->is_yes==1?'checked':'');?> name="<?php echo 'a'.$a;?>"><?= lang("បាទ") ?>
						</label>
						<label class="radio-inline">
							<input type="radio" value="0"   <?= ($evaluation_detail[$a-1]->is_yes==0?'checked':'');?> name="<?php echo 'a'.$a;?>"><?= lang("ទេ") ?>
						</label>
					</div>
				</div>
				 
				<div class="form-group">
					<div class="controls">
						<p>សូមពន្យល់</p>
						<textarea name="e9">
							<?= set_value("e9",$evaluation_detail[$a-1]->answer);?>
						</textarea> 
					</div>
				</div>
				<div class="div1">
					<p>(ប្រសិនបើចម្លើយសំណួរ III-2 “មូលដ្ឋានដែលមានមូលដ្ឋានច្បាស់លាស់” គឺ ទេ អ្នកអាចបន្តទៅសំណួរ III-10។)</p>
					<h2>ការធ្វើទុកបុកម្នេញ</h2>
					<div class="form-group">
						<div class="controls">
							<?php $a = 10;/// initial no. answer?> 
							<p><?= $this->applications->getQuestionKh(10); ?></p>
							<input type="hidden" value="<?= $this->applications->getQuestionKh(10) ?>" name="q10" />	
							<input type="hidden" value="3" name="g10" />								
						</div>
					</div>
					
					<div class="form-group">
						<div class="controls">
							<label class="radio-inline">
								<input type="radio" value="1" 	<?= ($evaluation_detail[$a-1]->is_yes==1?'checked':'');?> name="<?php echo 'a'.$a;?>"><?= lang("បាទ") ?>
							</label>
							<label class="radio-inline">
								<input type="radio" value="0"  <?= ($evaluation_detail[$a-1]->is_yes==0?'checked':'');?> name="<?php echo 'a'.$a;?>"><?= lang("ទេ") ?>
							</label>
						</div>
					</div>
					
					<div class="form-group">
						<div class="controls">
							<p>សូមពន្យល់</p>
							<textarea name="e10">
								<?= set_value("e10",$evaluation_detail[$a-1]->answer);?>
							</textarea> 
						</div>
					</div>
					
					<div class="form-group">
						<div class="controls">
							<?php $a = 11;/// initial no. answer?> 
							<p><?= $this->applications->getQuestionKh(11); ?></p>
							<input type="hidden" value="<?= $this->applications->getQuestionKh(11) ?>" name="q11" />
							<input type="hidden" value="3" name="g11" />									
						</div>
					</div>
					
					<div class="form-group">
						<div class="controls">
							<label class="radio-inline">
								<input type="radio" value="1" 	<?= ($evaluation_detail[$a-1]->is_yes==1?'checked':'');?> name="<?php echo 'a'.$a;?>"><?= lang("បាទ") ?>
							</label>
							<label class="radio-inline">
								<input type="radio" value="0"  <?= ($evaluation_detail[$a-1]->is_yes==0?'checked':'');?> name="<?php echo 'a'.$a;?>"><?= lang("ទេ") ?>
							</label>
						</div>
					</div>
					
					<div class="form-group">
						<div class="controls">
							<p>សូមពន្យល់</p>
							<textarea name="e11">
								<?= set_value("e11",$evaluation_detail[$a-1]->answer);?>
							</textarea> 
						</div>
					</div>
					
					<div class="form-group">
						<div class="controls">
							<?php $a = 12;/// initial no. answer?> 
							<p><?= $this->applications->getQuestionKh(12); ?></p>
							<input type="hidden" value="<?= $this->applications->getQuestionKh(12) ?>" name="q12" />	
							<input type="hidden" value="3" name="g12" />								
						</div>
					</div>
					
					<div class="form-group">
						<div class="controls">
							<label class="radio-inline">
								<input type="radio" value="1" 	<?= ($evaluation_detail[$a-1]->is_yes==1?'checked':'');?> name="<?php echo 'a'.$a;?>"><?= lang("បាទ") ?>
							</label>
							<label class="radio-inline">
								<input type="radio" value="0"  <?= ($evaluation_detail[$a-1]->is_yes==0?'checked':'');?> name="<?php echo 'a'.$a;?>"><?= lang("ទេ") ?>
							</label>
						</div>
					</div>
					
					<div class="form-group">
						<div class="controls">  
							<textarea name="e12">
								<?= set_value("e12",$evaluation_detail[$a-1]->answer);?>
							</textarea>
						</div>
					</div>
					<div class="form-group">
						<div class="controls">
							<?php $a = 13;/// initial no. answer?> 
							<p><?= $this->applications->getQuestionKh(13); ?></p>
							<input type="hidden" value="<?= $this->applications->getQuestionKh(13) ?>" name="q13" />			
							<input type="hidden" value="3" name="g13" />						
						</div>
					</div>
					
					<div class="form-group">
						<div class="controls">
							<label class="radio-inline">
								<input type="radio" value="1" 	<?= ($evaluation_detail[$a-1]->is_yes==1?'checked':'');?> name="<?php echo 'a'.$a;?>"><?= lang("បាទ") ?>
							</label>
							<label class="radio-inline">
								<input type="radio" value="0"  <?= ($evaluation_detail[$a-1]->is_yes==0?'checked':'');?> name="<?php echo 'a'.$a;?>"><?= lang("ទេ") ?>
							</label>
						</div>
					</div>
					
					<div class="form-group">
						<div class="controls">
							<p>សូមពន្យល់</p>
							<textarea name="e13">
								<?= set_value("e13",$evaluation_detail[$a-1]->answer);?>
							</textarea> 
						</div>
					</div>
					
					
					<div class="form-group">
						<div class="controls">
							<?php $a = 14;/// initial no. answer?> 
							<p><?= $this->applications->getQuestionKh(14); ?></p>
							<input type="hidden" value="<?= $this->applications->getQuestionKh(14) ?>" name="q14" />		
							<input type="hidden" value="3" name="g14" />							
						</div>
					</div>
					
					<div class="form-group">
						<div class="controls">
							<label class="radio-inline">
								<input type="radio" value="1" 	<?= ($evaluation_detail[$a-1]->is_yes==1?'checked':'');?> name="<?php echo 'a'.$a;?>"><?= lang("បាទ") ?>
							</label>
							<label class="radio-inline">
								<input type="radio" value="0"  <?= ($evaluation_detail[$a-1]->is_yes==0?'checked':'');?> name="<?php echo 'a'.$a;?>"><?= lang("ទេ") ?>
							</label>
						</div>
					</div>
					
					<div class="form-group">
						<div class="controls">
							<p>សូមពន្យល់</p>
							<textarea name="e14">
								<?= set_value("e14",$evaluation_detail[$a-1]->answer);?>
							</textarea> 
						</div>
					</div>
					
					
					<div class="form-group">
						<div class="controls">
							<?php $a = 15;/// initial no. answer?> 
							<p><?= $this->applications->getQuestionKh(15); ?></p>
							<input type="hidden" value="<?= $this->applications->getQuestionKh(15) ?>" name="q15" />		
							<input type="hidden" value="3" name="g15" />							
						</div>
					</div>
					
					<div class="form-group">
						<div class="controls">
							<label class="radio-inline">
								<input type="radio" value="1" 	<?= ($evaluation_detail[$a-1]->is_yes==1?'checked':'');?> name="<?php echo 'a'.$a;?>"><?= lang("បាទ") ?>
							</label>
							<label class="radio-inline">
								<input type="radio" value="0"  <?= ($evaluation_detail[$a-1]->is_yes==0?'checked':'');?> name="<?php echo 'a'.$a;?>"><?= lang("ទេ") ?>
							</label>
						</div>
					</div>
					
					<div class="form-group">
					<?php
							$rows = json_decode( $evaluation_detail[$a-1]->answer, true ); 
							for($i=0;$i<count($rows);$i++) {  
								if($rows[$i]["multiple_choice"]['option']=='a'){
									$a ='checked'; 
									$valueA = $rows[$i]["multiple_choice"]['value']; 
								}  
								if($rows[$i]["multiple_choice"]['option']=='b'){
									$b ='checked'; 
									$valueB = $rows[$i]["multiple_choice"]['value']; 
								} 
								if($rows[$i]["multiple_choice"]['option']=='c'){
									$c = 'checked'; 
									$valueC = $rows[$i]["multiple_choice"]['value']; 
								} 
								if($rows[$i]["multiple_choice"]['option']=='d'){
									$d ='checked'; 
									$valueD = $rows[$i]["multiple_choice"]['value']; 
								} 
								if($rows[$i]["multiple_choice"]['option']=='e'){
									$e ='checked'; 
									$valueE = $rows[$i]["multiple_choice"]['value']; 
								}  
							}
							 
					?> 
						<table width="100%">
							<tr>
								<td width="100">
									<label class="checkbox">
										<input type="checkbox" <?= $a ?>   value="a" name="c15[]"><?= lang("ពូជសាសន៍") ?> 
									</label>
								</td> 
								<td width="80">សូមបញ្ជាក់៖</td>
								<td><input class="form-control" value="<?php echo $valueA;?>" type="text" name='d15[]'/></td>
							</tr>
						</table>
						<table width="100%">
							<tr>
								<td width="100">
									<label class="checkbox">
										<input type="checkbox"  <?= $b ?> value="b" name="c15[]"><?= lang("សាសនា") ?>
									</label> 
								</td> 
								<td width="80">សូមបញ្ជាក់៖</td>
								<td><input class="form-control"  value="<?php echo $valueB;?>"  type="text" name='d15[]'/></td>
							</tr>
						</table>
						<table width="100%">
							<tr>
								<td width="100"> 
									<label class="checkbox">
										<input type="checkbox" <?= $c ?> value="c" name="c15[]"><?= lang("សញ្ជាតិ") ?>
									</label> 
								</td> 
								<td width="80">សូមបញ្ជាក់៖</td>
								<td><input class="form-control"  value="<?php echo $valueC;?>"  type="text" name='d15[]'/></td>
							</tr>
						</table>
						<table width="100%">
							<tr>
								<td width="260">
									<label class="checkbox">
										<input type="checkbox" <?= $d ?> value="d" name="c15[]"><?= lang("សមាជិកភាពនៃក្រុមសង្គមជាក់លាក់ណាមួយ") ?>
									</label> 
								</td> 
								<td width="80">សូមបញ្ជាក់៖</td>
								<td><input class="form-control"  value="<?php echo $valueD;?>"  type="text" name='d15[]'/></td>
							</tr>
						</table>
						<table width="100%">
							<tr>
								<td width="160">
									<label class="checkbox">
										<input type="checkbox" <?= $e ?> value="e"  name="c15[]"><?= lang("ទស្សនៈនយោបាយ") ?>
									</label> 
								</td> 
								<td width="80">សូមបញ្ជាក់៖</td>
								<td><input class="form-control"  value="<?php echo $valueE;?>"  type="text" name='d15[]'/></td>
							</tr>
						</table> 
					</div>
					
					<div class="form-group">
						<div class="controls">
							<p>សូមពន្យល់</p>
							<textarea name="e15">
								 <?= $rows['0']["answer"]?>
							</textarea> 
						</div>
					</div>
					
					
					<h2>ហេតុផលនៃការធ្វើទុក្ខបុកម្នេញ</h2>
					
					<div class="form-group">
						<div class="controls">
							<?php $a = 16;/// initial no. answer?> 
							<p><?= $this->applications->getQuestionKh(16); ?></p>
							<input type="hidden" value="<?= $this->applications->getQuestionKh(16) ?>" name="q16" />		
							<input type="hidden" value="3" name="g16" />							
						</div>
					</div>
					
					<div class="form-group">
						<div class="controls">
							<label class="radio-inline">
								<input type="radio" value="1" 	<?= ($evaluation_detail[$a-1]->is_yes==1?'checked':'');?> name="<?php echo 'a'.$a;?>"><?= lang("បាទ") ?>
							</label>
							<label class="radio-inline">
								<input type="radio" value="0"  <?= ($evaluation_detail[$a-1]->is_yes==0?'checked':'');?> name="<?php echo 'a'.$a;?>"><?= lang("ទេ") ?>
							</label>
						</div>
					</div> 
					<div class="form-group">
						<div class="controls">
							<p>សូមបញ្ជាក់</p>
							<textarea name="e16">
								<?= set_value("e16",$evaluation_detail[$a-1]->answer);?>
							</textarea> 
						</div>
					</div>
					
				</div>
				<h2>សេចក្តីសន្និដ្ឋានអំពីការរាប់បញ្ចូលនៅក្នុងអនុសញ្ញាឆ្នាំ១៩៥១/ពិធីសារឆ្នាំ១៩៦៧</h2>
				
				<div class="form-group">
					<div class="controls">
						<?php $a = 17;/// initial no. answer?> 
						<p><?= $this->applications->getQuestionKh(17); ?></p>
						<input type="hidden" value="<?= $this->applications->getQuestionKh(17) ?>" name="q17" />
						<input type="hidden" value="3" name="g17" />									
					</div>
				</div>
				
				<div class="form-group">
					<div class="controls">
						<label class="radio-inline">
							<input type="radio" value="1" 	<?= ($evaluation_detail[$a-1]->is_yes==1?'checked':'');?> name="<?php echo 'a'.$a;?>"><?= lang("បាទ") ?>
						</label>
						<label class="radio-inline">
							<input type="radio" value="0"  <?= ($evaluation_detail[$a-1]->is_yes==0?'checked':'');?> name="<?php echo 'a'.$a;?>"><?= lang("ទេ") ?>
						</label> 
					</div>
				</div>
				
				<div class="form-group">
					<div class="controls">  
						<textarea name="e17">
							<?= set_value("e17",$evaluation_detail[$a-1]->answer);?>
						</textarea>
					</div>
				</div>
			</div>
			
		</div>
	</div>
	
	<div id="content_5" class="tab-pane fade in">
		<div class="box">
			<div class="box-header">
				<h2 class="blue">
					<i class="fa-fw fa fa-barcode"></i>
					<?= lang("ផ្នែកទី៤-ការវាយតំលៃការរាប់បញ្ចូល-តម្រូវការកិច្ចការពារអន្ដរជាតិដទៃទៀត") ?>
				</h2>
			</div>
			
			<div class="box-content">
				
				<div class="form-group">
					<div class="controls">
						<?php $a = 18;/// initial no. answer?> 
						<p><?= $this->applications->getQuestionKh(18); ?></p>
						<input type="hidden" value="<?= $this->applications->getQuestionKh(18) ?>" name="q18" />	
						<input type="hidden" value="4" name="g18" />								
					</div>
				</div>
				
				<div class="form-group">
					<div class="controls">
						<label class="radio-inline">
							<input type="radio" value="1" 	<?= ($evaluation_detail[$a-1]->is_yes==1?'checked':'');?> name="<?php echo 'a'.$a;?>"><?= lang("បាទ") ?>
						</label>
						<label class="radio-inline">
							<input type="radio" value="0"  <?= ($evaluation_detail[$a-1]->is_yes==0?'checked':'');?> name="<?php echo 'a'.$a;?>"><?= lang("ទេ") ?>
						</label>
					</div>
				</div> 
				<div class="form-group">
					<div class="controls">
						<p>ប្រសិនបើមាន សូមពន្យល់</p>
						<textarea name="e18">
							<?= set_value("e18",$evaluation_detail[$a-1]->answer);?>
						</textarea> 
					</div>
				</div> 
				<div class="form-group">
					<div class="controls">
						<h2>ប្រសិនបើអ្នកដាក់ពាក្យសុំមិនបំពេញលក្ខណៈវិនិច្ឆ័យរាប់បញ្ចូលនៅក្នុងផ្នែក ៣ ឬ ៤ត្រូវបន្តទៅផ្នែក ៦</h2>
					</div>
				</div>
				
			</div>
		</div>
	</div>
	
	<div id="content_6" class="tab-pane fade in">
		<div class="box">
			<div class="box-header">
				<h2 class="blue">
					<i class="fa-fw fa fa-barcode"></i>
					<?= lang("ផ្នែក ៥ -  ការវាយតម្លៃការមិនរាប់បញ្ចូល") ?>
				</h2>
			</div>
			
			<div class="box-content">
				<div class="controls">
					<h2>(ផ្នែកនេះត្រូវតែបំ ពេញប្រសិនបើអ្នកដាក់ពាក្យសុំបំពេញលក្ខណៈវិនិច្ឆ័យការរាប់បញ្ចូលនៅក្នុងផ្នែក ៣ ឬផ្នែក៤)</h2>
				</div>
				
				
				<div class="form-group">
					<div class="controls">
						<?php $a = 19;/// initial no. answer?> 
						<p><?= $this->applications->getQuestionKh(19); ?></p>
						<input type="hidden" value="<?= $this->applications->getQuestionKh(19) ?>" name="q19" />	
						<input type="hidden" value="5" name="g19" />														
					</div>
				</div>
				
				<div class="form-group">
					<div class="controls">
						<label class="radio-inline">
							<input type="radio" value="1" 	<?= ($evaluation_detail[$a-1]->is_yes==1?'checked':'');?> name="<?php echo 'a'.$a;?>"><?= lang("បាទ") ?>
						</label>
						<label class="radio-inline">
							<input type="radio" value="0"  <?= ($evaluation_detail[$a-1]->is_yes==0?'checked':'');?> name="<?php echo 'a'.$a;?>"><?= lang("ទេ") ?>
						</label>
					</div>
				</div>
				
				<div class="form-group">
					<div class="controls">  
						<textarea name="e19">
							<?= set_value("e19",$evaluation_detail[$a-1]->answer);?>
						</textarea>
					</div>
				</div>
				<div class="form-group">
					<div class="controls">
						<p>ប្រសិនបើមិនមាន សូមបន្តទៅផ្នែក ៦ , ប្រសិនបើមាន សូមបន្ត</p>
					</div>
				</div>
				
				<div class="form-group">
					<div class="controls">
						<?php $a = 20;/// initial no. answer?> 
						<p><?= $this->applications->getQuestionKh(20); ?></p>
						<input type="hidden" value="<?= $this->applications->getQuestionKh(20) ?>" name="q20" />	
						<input type="hidden" value="5" name="g20" />														
					</div>
				</div>
				
				<div class="form-group">
					<div class="controls">
						<label class="radio-inline">
							<input type="radio" value="1" 	<?= ($evaluation_detail[$a-1]->is_yes==1?'checked':'');?> name="<?php echo 'a'.$a;?>"><?= lang("បាទ") ?>
						</label>
						<label class="radio-inline">
							<input type="radio" value="0"  <?= ($evaluation_detail[$a-1]->is_yes==0?'checked':'');?> name="<?php echo 'a'.$a;?>"><?= lang("ទេ") ?>
						</label>
					</div>
				</div>
				
				<div class="form-group">
					<div class="controls">  
						<textarea name="e20">
							<?= set_value("e20",$evaluation_detail[$a-1]->answer);?>
						</textarea>
					</div>
				</div>
				<div class="form-group">
					<div class="controls">
						<?php $a = 21;/// initial no. answer?> 
						<p><?= $this->applications->getQuestionKh(21); ?></p>
						<input type="hidden" value="<?= $this->applications->getQuestionKh(21) ?>" name="q21" />	
						<input type="hidden" value="5" name="g21" />														
					</div>
				</div>
				
				<div class="form-group">
					<div class="controls">
						<label class="radio-inline">
							<input type="radio" value="1" 	<?= ($evaluation_detail[$a-1]->is_yes==1?'checked':'');?> name="<?php echo 'a'.$a;?>"><?= lang("បាទ") ?>
						</label>
						<label class="radio-inline">
							<input type="radio" value="0"  <?= ($evaluation_detail[$a-1]->is_yes==0?'checked':'');?> name="<?php echo 'a'.$a;?>"><?= lang("ទេ") ?>
						</label>
					</div>
				</div>
				
				<div class="form-group">
					<div class="controls">  
						<textarea name="e21">
							<?= set_value("e21",$evaluation_detail[$a-1]->answer);?>
						</textarea>
					</div>
				</div>
				<div class="form-group">
					<div class="controls">
						<?php $a = 22;/// initial no. answer?> 
						<p><?= $this->applications->getQuestionKh(22); ?></p>
						<input type="hidden" value="<?= $this->applications->getQuestionKh(22) ?>" name="q22" />
						<input type="hidden" value="5" name="g22" />															
					</div>
				</div>
				
				<div class="form-group">
					<div class="controls">
						<label class="radio-inline">
							<input type="radio" value="1" 	<?= ($evaluation_detail[$a-1]->is_yes==1?'checked':'');?> name="<?php echo 'a'.$a;?>"><?= lang("បាទ") ?>
						</label>
						<label class="radio-inline">
							<input type="radio" value="0"  <?= ($evaluation_detail[$a-1]->is_yes==0?'checked':'');?> name="<?php echo 'a'.$a;?>"><?= lang("ទេ") ?>
						</label>
					</div>
				</div>
				
				<div class="form-group">
					<div class="controls">  
						<textarea name="e22">
							<?= set_value("e22",$evaluation_detail[$a-1]->answer);?>
						</textarea>
					</div>
				</div>
				<div class="form-group">
					<div class="controls">
						<p>(ប្រសិនបើចម្លើយចំពោះសំណួរទាំងបីក្នុង  ៥.២, ៥.៣ និង ៥.៤ គឺ ទេ សូមបន្តទៅ ៥.៨។  ប្រសិនបើចម្លើយ ចំពោះសំណួរមួយ ឬច្រើនមានចម្លើយ បាទ/ចាស៎ សូមបន្តសំណួរបន្ទាប់) </p>
					</div>
				</div>
				
				<div class="form-group">
					<div class="controls">
						<?php $a = 23;/// initial no. answer?> 
						<p><?= $this->applications->getQuestionKh(23); ?></p>
						<input type="hidden" value="<?= $this->applications->getQuestionKh(23) ?>" name="q23" />		
						<input type="hidden" value="5" name="g23" />													
					</div>
				</div>
				
				<div class="form-group">
					<div class="controls">
						<label class="radio-inline">
							<input type="radio" value="1" 	<?= ($evaluation_detail[$a-1]->is_yes==1?'checked':'');?> name="<?php echo 'a'.$a;?>"><?= lang("បាទ") ?>
						</label>
						<label class="radio-inline">
							<input type="radio" value="0"  <?= ($evaluation_detail[$a-1]->is_yes==0?'checked':'');?> name="<?php echo 'a'.$a;?>"><?= lang("ទេ") ?>
						</label>
					</div>
				</div>
				
				<div class="form-group">
					<div class="controls">
						<p>ប្រសិនបើមាន សូមពន្យល់</p>
						<textarea name="e23">
							<?= set_value("e23",$evaluation_detail[$a-1]->answer);?>
						</textarea> 
					</div>
				</div>
				
				<div class="form-group">
					<div class="controls">
						<?php $a = 24;/// initial no. answer?> 
						<p><?= $this->applications->getQuestionKh(24); ?></p>
						<input type="hidden" value="<?= $this->applications->getQuestionKh(24) ?>" name="q24" />
						<input type="hidden" value="5" name="g24" />							
					</div>
				</div>
				
				<div class="form-group">
					<div class="controls">
						<label class="radio-inline">
							<input type="radio" value="1" 	<?= ($evaluation_detail[$a-1]->is_yes==1?'checked':'');?> name="<?php echo 'a'.$a;?>"><?= lang("បាទ") ?>
						</label>
						<label class="radio-inline">
							<input type="radio" value="0"  <?= ($evaluation_detail[$a-1]->is_yes==0?'checked':'');?> name="<?php echo 'a'.$a;?>"><?= lang("ទេ") ?>
						</label>
					</div>
				</div>
				
				<div class="form-group">
					<div class="controls">
						<p>ប្រសិនបើមាន សូមពន្យល់</p>
						<textarea name="e24">
							<?= set_value("e24",$evaluation_detail[$a-1]->answer);?>
						</textarea> 
					</div>
				</div>
				
				<div class="form-group">
					<div class="controls">
						<?php $a = 25;/// initial no. answer?> 
						<p><?= $this->applications->getQuestionKh(25); ?></p>
						<input type="hidden" value="<?= $this->applications->getQuestionKh(25) ?>" name="q25" />
						<input type="hidden" value="5" name="g25" />														
					</div>
				</div>
				
				<div class="form-group">
					<div class="controls">
						<label class="radio-inline">
							<input type="radio" value="1" 	<?= ($evaluation_detail[$a-1]->is_yes==1?'checked':'');?> name="<?php echo 'a'.$a;?>"><?= lang("បាទ") ?>
						</label>
						<label class="radio-inline">
							<input type="radio" value="0"  <?= ($evaluation_detail[$a-1]->is_yes==0?'checked':'');?> name="<?php echo 'a'.$a;?>"><?= lang("ទេ") ?>
						</label>
					</div>
				</div>
				
				<div class="form-group">
					<div class="controls">
						<p>ប្រសិនបើមាន សូមពន្យល់</p>
						<textarea name="e25">
							<?= set_value("e25",$evaluation_detail[$a-1]->answer);?>
						</textarea> 
					</div>
				</div>
				
				<div class="form-group">
					<div class="controls">
						<?php $a = 26;/// initial no. answer?>
						<p><?= $this->applications->getQuestionKh(26); ?></p>
						<input type="hidden" value="<?= $this->applications->getQuestionKh(26) ?>" name="q26" />
						<input type="hidden" value="5" name="g26" />															
					</div>
				</div>
				
				<div class="form-group">
					<div class="controls">
						<label class="radio-inline">
							<input type="radio" value="1" 	<?= ($evaluation_detail[$a-1]->is_yes==1?'checked':'');?> name="<?php echo 'a'.$a;?>"><?= lang("បាទ") ?>
						</label>
						<label class="radio-inline">
							<input type="radio" value="0"  <?= ($evaluation_detail[$a-1]->is_yes==0?'checked':'');?> name="<?php echo 'a'.$a;?>"><?= lang("ទេ") ?>
						</label>
					</div>
				</div>
				
				
				<p>សូមគូសចម្លើយមូលដ្ឋានពាក់ព័ន្ធ៖ </p>
				<?php
						$rows1 = json_decode( $evaluation_detail[$a-1]->answer, true ); 
						for($i=0;$i<count($rows1);$i++) {   
							if($rows1[$i]["multiple_choice"]['option']=='a'){
								$a1 ='checked'; 
								$valueA1 =  $rows1[$i]["multiple_choice"]['value'];
							}  
							if($rows1[$i]["multiple_choice"]['option']=='b'){
								$b1 ='checked';  
								$valueB1 =  $rows1[$i]["multiple_choice"]['value'];
							} 
							if($rows1[$i]["multiple_choice"]['option']=='c'){
								$c1 = 'checked';  
								$valueC1 =  $rows1[$i]["multiple_choice"]['value'];
							}     
						}
						 
				?>  
				<div class="form-group">
					<div class="controls">
							<label class="checkbox">
							<input type="checkbox" <?= $a1?> value="a" name="c26[]">
							<?= lang("ឧក្រិដ្ឋកម្មប្រឆាំងសន្តិភាព ឧក្រិដ្ឋកម្មសង្គ្រាម ឬឧក្រិដ្ឋកម្មប្រឆាំងមនុស្សជាតិ") ?>
							</label>
							<div style='margin-left:50px;'> 
								<p>សូមបញ្ជាក់​ ៖</p>
								<input type='text' value='<?= $valueA1?>' class='form-control' name="d26[]"/>
							</div>
							<label class="checkbox">
								<input type="checkbox" <?= $b1?> value="b" name="c26[]">
								<?= lang("ឧក្រិដ្ឋកម្មធ្ងន់ធ្ងរមិនមែននយោបាយមុនពេលចូលក្នុងប្រទេសភៀសខ្លួន") ?>
							</label> 
							<div style='margin-left:50px;'> 
								<p>សូមបញ្ជាក់​ ៖</p>
								<input type='text' value='<?= $valueB1?>' class='form-control' name="d26[]"/>
							</div>
							<label class="checkbox">
								<input type="checkbox" <?= $c1?> value="c" name="c26[]">
								<?= lang("សកម្មភាពដែលផ្ទុយនឹងគោលបំណង និងគោលការណ៍របស់អង្គការសហប្រជាជាតិ។") ?>
							</label>
							<div style='margin-left:50px;'> 
								<p>សូមបញ្ជាក់​ ៖</p>
								<input type='text' value='<?= $valueC1?>' class='form-control' name="d26[]"/>
							</div> 
					</div>
				</div>
				<div class="form-group">
					<div class="controls">  
						<textarea name="e26">
							 <?= $rows1['0']["answer"]?>
						</textarea>
					</div>
				</div>
			</div>
			
		</div>
	</div>
	
	<div id="content_7" class="tab-pane fade in">
		<div class="box">
			<div class="box-header">
				<h2 class="blue">
					<i class="fa-fw fa fa-barcode"></i>
					<?= lang("ផ្នែក ៦ អនុសាសន៍") ?>
				</h2>
			</div>
			
			<div class="box-content">
				<div class="row">
					<div class="col-sm-12">
					
						<div class="form-group">
							<div class="controls">
								<p><?= $this->applications->getQuestionKh(27); ?></p>
								<input type="hidden" value="<?= $this->applications->getQuestionKh(27) ?>" name="q27" />
								<input type="hidden" value="6" name="g27" />
							</div>
						</div> 
						<div class="form-group">
							<div class="controls">
							<?php $a = 27;/// initial no. answer?>
								<label class="radio-inline">
									<input type="radio" value="1" <?= ($evaluation_detail[$a-1]->is_yes==1?'checked':'');?>  name="<?php echo 'a'.$a;?>"><?= lang("បាទ") ?>
								</label>
								<label class="radio-inline">
									<input type="radio" value="0" <?= ($evaluation_detail[$a-1]->is_yes==0?'checked':'');?> name="<?php echo 'a'.$a;?>"><?= lang("ទេ") ?>
								</label> 
							</div>
						</div>  
						<div class="form-group">
							<div class="controls"> 
								<textarea name="e27" id='e27'>
									<?= set_value("e27",$evaluation_detail[$a-1]->answer);?>
								</textarea>
							</div>
						</div>
					
						<button class="btn btn-success" /><?= lang("submit") ?></button>
					</div>
				</div>
			</div>
			
		</div>
	</div>
	<div id="content_8" class="tab-pane fade in">
		<div class="box">
		
			<div class="box-header">
				<h2 class="blue">
					<i class="fa-fw fa fa-barcode"></i>
					<?= lang("interview_records") ?>
				</h2>
			</div> 
			<div class="box-content">
			
			<?= form_open("applications/add_interview_records/".$id); ?>
			
				<div class="row">	 
					<div class="col-md-3 column">
						<div class='form-group'>										
							<?= lang("member","member");?>
							<?php    
						 	$members_ = array(lang("select"));
							foreach($result_members as $member){
								$members_[$member->id] = $member->lastname_kh . " " . $member->firstname_kh;
							} 
							echo form_dropdown('member', $members_, 0, ' id="member" class="form-control" '); 

							?>
						</div>
					</div> 
					<div class="col-md-12 column">
							<table class="table table-bordered table-hover" id="tab_interview">
								<thead>
									<tr>
										<th><?= lang("#") ?></th>
										<th><?= lang("សំណួរ") ?></th>
										<th><?= lang("ចម្លើយ") ?></th>
										<th style="width: 120px;"><?= lang("កំណត់​ចំណាំ") ?></th>
										<th width='3%'></th>
									</tr>
								</thead>
								<tbody>
									<?php foreach($interview_records as $i => $row){  ?>
										<tr>											
											<td class="center" width='20'>
												<?php echo ($i+1) ?>
												<input type="hidden" name="member[]" id="member" value="<?= $row->interview_member_id?>" />
											</td>												
											<td>
												<textarea class="questions skip" id="question" name='questions[]' ><?= $row->questions?> </textarea>	
											</td>
											<td> 
												<textarea class="answers skip" id="answer" name="answers[]"><?= $row->answers?></textarea>
											</td>
											<td> 
												<textarea class="notes skip" name='notes[]' ><?= $row->notes?></textarea>
											</td>
											<td>
												<a title="<?= lang('delete');?>" href="<?= 'applications/delete_interview_records_evaluation/'.$row->id ?>" class="btn btn-danger btn-xs">
												<i class="fa fa-trash"></i>
												</a>
											</td>
										</tr>
									<?php } ?> 
								</tbody>
							</table>						
					</div> 
							
					<div class="col-sm-12">
						<p><?= lang('ដើម្បីបង្កើតជួរដេកត្រូវប្រើ (  Crlt + Enter  ) ');?></p>
						<button type="submit" class="btn btn-success save-qa" disabled /><?= lang("submit") ?></button>
					</div>
				</div>
			<?= form_close();?>
			</div>
		</div>
	</div>
</div>
<?= form_close(); ?>

<script type="text/javascript" src="<?= $assets ?>tagsinput/src/bootstrap-tagsinput.js"></script>
<link href="<?= $assets ?>tagsinput/src/bootstrap-tagsinput.css" rel="stylesheet" />

<style type="text/css">	
	#img_preview{
		width:180px !important;
	} 
	.bootstrap-tagsinput{
		width:100%;
	}
	textarea {
		display: inline-block;
		border: solid 1px #000;
		min-height: 60px;		
		border:1px solid #EEE;
		resize:none;
		width: 100%;
		padding:5px;
	}
</style>

<script type="text/javascript">
	expanding_data();
	
		function expanding_data() {
			$(".questions, .answers, .notes").height(this.scrollHeight);
			$(".questions, .answers, .notes").on('keyup', function() {				
				$(this).height(0).height(this.scrollHeight);
			});
		}
	$(function(){ 
		$("input[name='interpreter']").tagsinput('items');
		
		$("input[name='a27']").on("ifChecked",function(){
			var ans = $(this).val();
			var ans_no = "អ្នកដាក់ពាក្យសុំមិនបានជួបនូវលក្ខ័ណ្ឌការការពារជនភៀសខ្លួនជាលក្ខណៈអន្ដរជាតិក្នុងមាត្រា១ក.";
			ans_no += "របស់អនុសញ្ញា ឆ្នាំ១៩៥១ និងពិធីសារឆ្នាំ១៩៦៧ ស្ដីពីឋានៈជនភៀសខ្លួន និងអនុក្រឹត្យលេខ២២៤អនក្រ.បក ស្តីពីនីតិវិធីក្នុង";
			ans_no += "ការពិនិត្យទទួលស្គាល់ផ្តល់ឋានៈជនភៀសខ្លួន ឬសិទិ្ធជ្រកកោនដល់ជនបរទេសនៅក្នុងព្រះរាជាណាចក្រកម្ពុជាទេ ។"; 
			ans_no += "ដូច្នេះមន្ត្រីវិនិច្ឆ័យកំណត់ឋានៈសូមគោរពអនុញ្ញាតផ្តល់យោបល់ថា បុគ្គលអ្នកដាក់ពាក្យសុំនេះមិនគួរផ្ដល់ឋានៈជា ជនភៀសខ្លួន ។";
			 
			var ans_yes = "អ្នកដាក់ពាក្យសុំបានជួបនូវលក្ខ័ណ្ឌការការពារជនភៀសខ្លួនជាលក្ខណៈអន្ដរជាតិក្នុងមាត្រា១ក.";
			ans_yes += "របស់អនុសញ្ញា ឆ្នាំ១៩៥១ និងពិធីសារឆ្នាំ១៩៦៧ ស្ដីពីឋានៈជនភៀសខ្លួន និងអនុក្រឹត្យលេខ២២៤អនក្រ.បក ស្តីពីនីតិវិធីក្នុង";
			ans_yes +="ការពិនិត្យទទួលស្គាល់ផ្តល់ឋានៈជនភៀសខ្លួន ឬសិទិ្ធជ្រកកោនដល់ជនបរទេសនៅក្នុងព្រះរាជាណាចក្រកម្ពុជាទេ ។"; 
			ans_yes += "ដូច្នេះមន្ត្រីវិនិច្ឆ័យកំណត់ឋានៈសូមគោរពអនុញ្ញាតផ្តល់យោបល់ថា បុគ្គលអ្នកដាក់ពាក្យសុំនេះគួរផ្ដល់ឋានៈជា ជនភៀសខ្លួន ។";
			 
			if(ans==0) {
				$("#e27").redactor('set', ans_no); 
			}else {
				$("#e27").redactor('set', ans_yes);
			}
		}); 
		
		$(".checked_ar1").on("ifChecked",function(){
			var hidden_ar2 = $(this).parent().parent().parent().find(".hidden_ar1");
			var hidden_val = $(this).attr("value");
			hidden_ar2.val(hidden_val);
		});
		
		$(".checked_ar2").on("ifChecked",function(){
			var hidden_ar2 = $(this).parent().parent().parent().find(".hidden_ar2");
			var hidden_val = $(this).attr("value");
			hidden_ar2.val(hidden_val);
		});
		
		$("#case_no").autocomplete({
			source: function (request, response) {
				var q = request.term;
				if($.isNumeric(q)){
					$.ajax({
						type: 'get',
						url: '<?= site_url('applications/suggestions_interview_caseno'); ?>',
						dataType: "json",
						data: {
							q : q
						},
						success: function (data) {
							response(data);
						}
					});
				}
			},
			minLength: 1,
			autoFocus: false,
			delay: 200,
			response: function (event, ui) {
				if (ui.content[0].id == 0) {
					$(this).removeClass('ui-autocomplete-loading');
					$(this).removeClass('ui-autocomplete-loading');
				}
			},
			select: function (event, ui) {
				event.preventDefault();
				if (ui.item.id !== 0) {
					location.href = "<?= site_url("applications/add_evaluation_refugee/") ?>/"+ui.item.row.interview_id+"/"+ui.item.row.id;
				}
			}
			
		});
		function getInterviewOfficer(t){  
				 $("#interviewer option[value='"+t+"']").attr("selected",true).change();
		}
		function getEvaluationOfficer(t){  
				$("#evaluation_officer option[value='"+t+"']").attr("selected",true).change();
		}
		function pad(str) {
		  str = str.toString();
		  return str.length < 2 ? pad("0" + str, 2) : str;
		}
		
		$(function(){
			function append_data(){
				var i = $("#tab_interview tbody tr").length + 1;
				var member_id = $("#member").val();
				var html = "<tr>" ; 
						html += "<td class='center'>"+i+"<input type='hidden' value='"+member_id+"' name='member[]' /></td>";
						html += "<td><textarea class='questions' name='questions[]'></textarea></td>";
						html += "<td><textarea class='answers' name='answers[]'></textarea></td>";
						html += "<td><textarea class='notes' name='notes[]' ></textarea></td>";
						html += "<td class='center'>";
							html +="<a  class='btn btn-danger btn-xs remove'>";
							html +="<i class='fa fa-trash'></i>";
							html +="</a>";
						html +="</td>";
					html +="</tr>";
					
					$("#tab_interview").append(html);
					$(".remove").click(delete_data);
					
					expanding_data();
					if(i > 0) {
						$(".save-qa").removeAttr('disabled');
					}
			}
			
			
			 
			function delete_data(){
				var parent = $(this).parent().parent();
				parent.remove();
				return false;
			}
			
			$(function(){
				
				$('.timepicker').datetimepicker({
					format: 'hh:ii',
					startView: 0
				});
			
				$(this).keydown(function (e) { 
					  if(e.ctrlKey && e.keyCode == 13) { 
						append_data();
						return false;
					  } 
				}); 
			 
			});
		});	
	});
</script>

