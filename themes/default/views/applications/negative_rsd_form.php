<div class='col-lg-3 director-txt'> 
		<div class='form-group'> 
			<?= lang("នាយកគ្រប់គ្រង៖", "នាយកគ្រប់គ្រង៖");?>
			<select   class="form-control text-cover director">
				<?php foreach($departments as $dep) {?>
				<option value="<?= $dep->department_en ?>"><b><?= $dep->department_kh?></b></option>
				<?php }?>
			</select>
		</div> 
</div>
<div class='col-lg-3 text-center ministry-no' >
	<?= lang("លេខប្រកាសក្រសួងមហាផ្ទៃ៖")?>&nbsp;&nbsp;&nbsp;<span class="bold"><?=$evaluation->negative_no ?></span>
</div>
<div class="clearfix"></div>
<div class="content-cover a4 text-size">
	<img src="<?= base_url("assets/bg9.jpg") ?>" class="hidden-bg"  >
	<div class="cover-in"> 
		<div class="content-header" style="height: 200px;"></div> 
			<?php if($evaluation->resubmit == 0){ ?>
						<div class="text-center">
							<h2><?= lang("សេចក្ដីជូនដំណឹង") ?></h2>
							<h2><?= lang("ស្តីពី") ?></h2>
							<h2><?= lang("ការសម្រេចជាអវិជ្ជមាននៃការកំណត់ឋានៈជនភៀសខ្លួន") ?></h2>
							<h2> <span class='text-english'><?= lang(" Negative Notification on Refugee Status Determination") ?></span></h2> 
						</div>  
			<?php } else{ ?> 
							<div class="text-center">
								<h2><?= lang("សេចក្ដីជូនដំណឹង") ?></h2>
								<h2><?= lang("ស្តីពី") ?></h2>
								<h2><?= lang("ការសម្រេចជាអវិជ្ជមាននៃការកំណត់ឋានៈជនភៀសខ្លួន<br/><br/>លើបណ្តឹងជំទាស់") ?></h2>
								<h2> <span class='text-english'><?= lang(" Appeal Negative Notification on Refugee Status Determination ") ?></span></h2> 
							</div> 
			<?php } ?>
		<br/>
		<div class="text-content" >
			<p>លេខករណី / Case Number : <span class='text-english'><b><?= $application->case_prefix.' '.$application->case_no?></b></span></p>
			<table width="100%"  style="white-space:nowrap;">
				<tr>
					<td width="30"><p>ឈ្មោះអ្នកដាក់ពាក្យសុំ / Name of Applicant:&nbsp;&nbsp;</p></td>
					<td><p><b><?= $application->lastname_kh." ".$application->firstname_kh." / ".$application->lastname." ".$application->firstname?></b></p></td>
				</tr>
			</table>  
			<table width="100%" style="white-space:nowrap;"> 
				<tr> 
					<td width="50"><p>ភេទ / Sex:</p></td>
					<td width="100"><p><b><?= lang($application->gender)?></b></p></td>
					<td width="100"><p>សញ្ជាតិ / Nationality:&nbsp;</p></td>
					<td><p><b><?= $application->nationality_kh ?></b></p></td>
					<td width="100"><p>ជាតិពន្ធុ / Ethnicity:&nbsp;</p></td>
					<td><p><b><?= $application->ethnicity_kh?></b></p></td>
				</tr>
			</table>
			<table width="100%" style="white-space:nowrap;">
				<tr>
					<td width='40'><p>ឆ្នាំកំណើត / Date of Birth: &nbsp;</p></td>
					<td><p><b><?= $this->erp->khmer_date_format($application->dob)?></p></td>
				</tr>
			</table> 
			<table width="100%">
				<tr width="40"> 
					<?php
						$n = count($result_members); 
						$number_member =($n<10?'0':'').$n;
					?> 
					<td><p>សមាជិកក្នុងបន្ទុក / Family Composition: <b><?= $this->erp->toKhmer($number_member)?>នាក់</b></p></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr> 
			</table>  
		</div> 
		
		<?php if($evaluation->resubmit == 0){ ?>
		
		<div class="text-left" style="text-align: justify;text-justify: inter-word;">
			<p>
			<span style="margin-left:3em;"></span> 
			យើង​មាន​ការ​សោក​ស្ដាយ​នឹង​ជម្រាប​អ្នក​ថា  បន្ទាប់ពី​ធ្វើ​ការ​វិភាគ​យ៉ាង​ហ្មត់​ចត់ អំពី​អំណះ​អំណាង​របស់​អ្នក  និង ពិចារណា​យ៉ាង​យក​ចិត្ដ​ទុក​ដាក់ នូវ​រាល់​ព័ត៌មាន 
			ដែល​អាច​រក​បាន​ ក្រសួង​មហា​ផ្ទៃ​ នៃ​រាជ​រដ្ឋា​ភិបាល​កម្ពុជា បាន​សម្រេច​ថា អ្នក​មិន​មាន​លក្ខខណ្ឌ​គ្រប់គ្រាន់​ដើម្បី​ទទួល​បាន​នូវ​ការ​ការ​ពារ​ជន​ភៀស​ខ្លួន​ជា​អន្ដរ​ជាតិ   ក្រោម​ការ​សម្រេច​របស់​រាជ​រដ្ឋា ភិបាល​កម្ពុជា​ឡើយ។</p>  
			<p> 
			<span style="margin-left:3em;"></span> 
			We regret to inform you that after a thorough assessment of your refugee claim, and careful consideration of all available information, the Ministry of Interior of the Royal Government of Cambodia has determined you are not eligible for international refugee protection under the Royal Government of Cambodia mandate. 
			</p>
			
			&nbsp;
			
			<p> 
			<span style="margin-left:3em;"></span> 
			ដើម្បី​ឱ្យ​គ្រប់​លក្ខខណ្ឌ​សម្រាប់ ទទួល​បាន​ការ​ការ​ពារ​ជា​អន្ដរ​ជាតិ  ក្នុង​ឋានះ​ជា​ជន​ភៀស​ខ្លួន​,
			អ្នក​ត្រូវ​បង្ហាញ​ថា អ្នក​មាន​ការ​ភ័យខ្លាច​ប្រកប​ដោយមូល​ដ្ឋាន​ច្បាស់​លាស់​​ នៃ​ការ​ធ្វើ​ទុក្ខ​បុក​ម្នេញ​ធ្ងន់​ធ្ងរ​ដោយ​សារ​មូល​ហេតុ    
			ពូជ​សាសន៍ របស់​អ្នក  ឫុ​សាសនា​របស់​អ្នក  ឬ​សញ្ជាតិ​របស់​អ្នក  ឬុ សមាជិក​ភាព​របស់​អ្នក​ក្នុង​ក្រុម​សង្គម ដោយ​ឡែក​ណា​មួយ  
			ឬុ​មតិ នយោបាយ​របស់​អ្នក។ ក្នុង​ការ​យល់​ឃើញ​របស់​យើង អ្នក​មិន​បាន​ជួប​ប្រទះ​នូវ​លក្ខណ្ឌ​ទាំង​នេះ​ដោយ​សារៈ
			</p> 
				
			<p> 
			<span style="margin-left:3em;"></span> 
				To qualify for international protection as a refugee,
				you must demonstrate that you have a well-founded fear 
				of being persecuted on account of your race, or your 
				religion, or your nationality, or your membership of
				a particular social group, or your political opinion.
				In our view you do not meet these criteria because: 
			</p>
			&nbsp;				
			<p>
				<span style="font-size:25px;" class="checkBox1">&#x2610;</span>   
				<span style="font-size:25px; display:none;" class="checkBox11" >&#x2611;</span> 
				ទុក្ខ​ទោស​ដែល​អ្នក​ទទូល​រង​នូវ​ការ​ឈឺ​ចាប់ ឬ មាន​ការ​ភ័យ​ខ្លាច​នោះ​មិន​បាន​ពាក់ព័ន្ធ​ទៅ និង​លក្ខខណ្ឌ​ណា មួយ​នៃ​លក្ខខណ្ឌ​ទាំង​ប្រាំ​ដែល​បាន​ចែង​ខាង​លើ​នោះ​ទេ។ <br/>
				The harm you suffered or fear you have is not related to any of the five Convention grounds listed above.
			</p>
			
			&nbsp;
			
			<p>
				<span style="font-size:25px;" class="checkBox2">&#x2610;</span>   
				<span style="font-size:25px; display:none;" class="checkBox22" >&#x2611;</span>
				ព្រឹត្ដិការណ៍​ដែល​អ្នក​រៀប​រាប់​ប្រា​​ប់យើ​​ងក្នុងពេ​លសំភាស​ន៍មិនបានបង្ហា​ញថា​អ្នក​បាន​ប្រឈ​មមុខ ឬុ នឹង ប្រឈម​មុខ​នឺង​ប្រព្រឹត្ដ​កម្ម​ដែល​ធ្ងន់​ធ្ងរ​ដែល​ត្រូវ​រាប់​ថា​ជា​ការ​ធ្វើ​ទុក្ខ​បុក​ម្នេញ​ដ៏​ធ្ងន់​ធ្ងរ​នោះ​ឡើយ៕<br/>
				The events you described to us during your interview do not 
				demonstrate that you have faced or will face treatment as 
				severe as to amount to persecution.
			</p>
			
			&nbsp;
												
			<p>
				<span style="font-size:25px;" class="checkBox3">&#x2610;</span>   
				<span style="font-size:25px; display:none;" class="checkBox33" >&#x2611;</span> 
				ចម្លើយ​របស់​អ្នក​មិន​ត្រូវ​បាន​ជឿ​ទុក​ចិត្ដ​ដោយ​ហេតុ​ផល​ខាង​ក្រោមៈ​ <br/>
				Your testimony was not credible for the following reasons:
			</p>
			
			&nbsp;
			
			<p style="margin-left:40px;">
				<span style="font-size:25px;" class="checkBox4">&#x2610;</span>   
				<span style="font-size:25px; display:none;" class="checkBox44" >&#x2611;</span> 
				ដោយ​ភាព​មិន​ស៊ីសង្វាក់​ជា​មួយ និង​សេចក្ដី​ថ្លែង​ការណ៍​របស់​អ្នក ឬ​សេចក្ដី​ថ្លែង​ការណ៍ ដែល​ផ្ដល់​ដោយ បុគ្គល​ដែល​ជា​អ្នក​មាន​អំណះ​អំណាង​ពាក់​ព័ន្ធ។<br/>
				Due to material inconsistencies with your own statements or those provided by persons with related claims;
			</p>
			
			&nbsp;
			 
			<p style="margin-left:40px;">
			
				<span style="font-size:25px;" class="checkBox5">&#x2610;</span>   
				<span style="font-size:25px; display:none;" class="checkBox55" >&#x2611;</span>   
				ដោយ​ភាព​មិន​ស៊ី​សង្វាក់​ជា​មួយ​និង​ពត៍​មាន​នៃ​ប្រទេស​ដើម​កំ​ណើត​ដែល​មន្ដ្រី​កម្ពុជា​ទទួល​បាន​។<br/>
				Due to material inconsistencies with country of origin information available to the Cambodian Official .
			</p>
			
			&nbsp;
			
			<p> 
				<span style="margin-left:3em;"></span>
			សូម​បញ្ជាក់​ថា​នេះ​គ្រាន់​តែ​ជា​ការ​សម្រេច​លើក​ទីមួយ​ទេ។ អ្នក​អាច​ដាក់​បណ្ដឹង​ត​វ៉ា​ទៅ​និង​សេចក្ដី​សម្រេច​នេះ​ រួច 
			ប្រ​គល់​ទៅ​អោយ​នាយ​ក​ដ្ឋាន​ជនភៀស​​ខ្លួន​នៃ​ក្រ​សួង​មហា​ផ្ទៃ​នៃ​រាជ​រដ្ឋាភិបាល​កម្ពុ​ជាវិញ ក្នុង​រយៈ​ពេល៣០ថ្ងៃ។
			បណ្ដឹង​ត​​វ៉ា​​នឹង​​ត្រូវ​បាន​​យក​​មក​​ពិ​​ចា​រណា ​លុះ​ត្រា​​តែ​​អ្នក​​អាច​​ផ្ដល់​​នូវ​​ភស្ដុ​តាង​​សម​​ហេតុ​ផល ​
			និង​សេច​ក្ដី​ពន្យល់​ជា លាយ​ល័ក្ខ​អក្សរ ដើម្បី​បង្ហាញ​ថា​អ្នក​កំ​ពុង​ផ្សង​គ្រោះ​ថ្នាក់ ​នៃ ការ​ធ្វើ​ទុក្ខ​បុក​ម្នេញ  
			នៅ​ក្នុង​ប្រទេស​កំណើត របស់​អ្នក​សម្រាប់​ហេតុ​ផល​ដូច ដែល​បាន​ចែង​ក្នុង​កឋា​ខ័ណ្ឌ​ទី២ ខាង​លើ។
			</p>
			  
			 
			<p class='text-english'> 
			<span style="margin-left:3em;"></span>
			Please note that this is the first instance decision.
			You can file an appeal to this decision and return it
			to the Department of Refugee of the Ministry of Interior
			of the Royal Government of Cambodia within 30 days. The appeal
			will be taken for consideration only if you can provide reasonable 
			proof and explanation in writing to indicate that you are in risk of 
			being persecuted in your country of origin for the reasons as mentioned
			in paragraph 2.
			</p>
		</div> 
		
		<?php } else { ?>
		
		<div class="">   
			<p>
			&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;
			ក្រ​​សួង​​មហា​​ផ្ទៃ  នៃ​​រាជ​​រដ្ឋា​ភិបាល​​កម្ពុជា  យោង​​បណ្តឹង​​ជំទាស់​​អោយ​ពិ​និត្យ​​ឡើង​​វិញ​​នូវ​​ករណី​​របស់​​លោក​​អ្នក​ 
			យើង​​មាន​​ការ​​សោក​ស្តាយ​នឹង​ជម្រាប​ថា  បន្ទាប់​ពី​បាន​ពិ​និត្យ​យ៉ាង​ហ្មត់​ចត់នូវ​បណ្តឹង​ជំទាស់​រួច​មក​ឃើញ​ថា 
			លោក​អ្នក​មិន​មាន​មូល​ហេតុ​ថ្មី​ដែល​មាន​បន្ទុក​ដើម្បី​គាំ​ទ្រ​នូវ​​អំណះ​អំណាង​ប្តឹង​ទាស់​របស់​លោកអ្នក​ដែល​អាចផ្តល់​នូវ​លទ្ធ​ផល ក្នុង​ការ​ផ្លាស់​ប្តូរ​សេច​ក្តី​សម្រេច​លើក​មុន​ឡើយ ។ ដូច្នេះសេចក្តីសម្រេចជាអវិជ្ជមានលើកទី១ ត្រូវបានរក្សាទុកដដែល។
			</p>
		</div> 
		<br/>
		<div class="text-justify">  
			<p>
			&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;
			The Ministry of Interior of the Royal Government of
			Cambodia refers to your request for reviewing your case.
			We regret to inform you that after having thoroughly examined
			your appeal, there are no substantial new elements to support 
			your claim that would application in the change of the previous decision. 
			Therefore, the first instance decision of rejection is hereby upheld.
			</p>
		</div> 
		
		<?php } ?>
		
		<div class="content-footer​" style="float:right;">
			<table width="100%">
				<tr>
					<td class='text-center'>
						<?= $this->erp->footer_date_khmer('រាជធានីភ្នំពេញ');?> 
					</td>
				</tr>
				<tr>
					<td>
						<center> 
							<h3 class="text-cover director_text_sub"></h3>
							<h2 class="director_text">ឧបនាយករដ្ឋមន្រ្តី រដ្ឋមន្រ្តីក្រសួងមហាផ្ទៃ</h2>
						</center>
					</td>
				</tr>
			</table>
		</div> 
	</div> 
</div> 
<script type="text/javascript">
	$(".director").on('click',function(){ 
		var txt = $(".director option:selected").text();
		var txt_en = $(".director option:selected").val();
		if (txt=="រដ្ឋលេខាធិការ") {
			$(".director_text_sub").text('ជ.រដ្ឋមន្រ្តី');
		}else{
			$(".director_text_sub").text('');
		}
		$(".director_text").text(txt);  
	});
	 cursorPointer(1);
	 cursorPointer(2);
	 cursorPointer(3);
	 cursorPointer(4);
	 cursorPointer(5);
	$(".checkBox1").click(function(){ 
		myChecked(11,1);
	});
	$(".checkBox11").click(function(){
		 myUnchecked(11,1);
	});
	
	$(".checkBox2").click(function(){ 
		myChecked(22,2);
	});
	$(".checkBox22").click(function(){
		 myUnchecked(22,2);
	});
	$(".checkBox3").click(function(){ 
		myChecked(33,3);
	});
	$(".checkBox33").click(function(){
		 myUnchecked(33,3);
	});
	
	$(".checkBox4").click(function(){ 
		myChecked(44,4);
	});
	$(".checkBox44").click(function(){
		 myUnchecked(44,4);
	});
	$(".checkBox5").click(function(){ 
		myChecked(55,5);
	});
	$(".checkBox55").click(function(){
		 myUnchecked(55,5);
	});
	function myChecked(boxCheck,boxUncheck){
		 $(".checkBox"+boxCheck).show();
		 $(".checkBox"+boxCheck).css("cursor","pointer");
		 $(".checkBox"+boxUncheck).hide();
	}
	function myUnchecked(boxCheck,boxUncheck) {
		 $(".checkBox"+boxUncheck).show();
		 $(".checkBox"+boxUncheck).css("cursor","pointer");
		 $(".checkBox"+boxCheck).hide();
	}
	function cursorPointer(x){
		$(".checkBox"+x).css("cursor","pointer");
	}
</script>
<style type="text/css">
	@media print{
		input[type=checkbox] {
			display: block;
		}
		.bblack{ background: white !important; }
		.modal-content, .box, 
		.modal-header{ border:none !important; }
		.table { border:1px solid #000 !important; }
		.ministry-no,.director-txt{display:none !important;}
		.hidden-bg{
			display:block !important;
		}
	}

	.hidden-bg{
		z-index:-9999;
		position:absolute;
		display:none;
	}

	.content-cover span, p {
		padding: 0;
		margin: 0;
		line-height:24px;
		font-size:15 px;	
		text-align: justity;
	}
	.content-cover center{
		font-size:14px;
		font-family: 'Moul', sans-serif !important;
	}
	.content-cover table td{
		font-size:14px;	
		line-height:26px;
		white-space: nowrap;
	}
	.content-cover{
		width:800px;
		min-height:2000px;
		background:url("assets/bg9.jpg");
		background-size: 800px auto;
		background-repeat: no-repeat;
		background-color:white;
	}
	.cover-in{
		margin:0 50px;
		width:700px;
		position:absolute;
		white-space: wrap; 
	}	
	ul.content-list {
		list-style: none;
		padding: 0;
		margin: 0;
	}
	.content-list li {
		line-height:26px;
		font-size:14px;	
	}
	.content-list li:before {
		content: "•"; 
	}
	.content-cover h2{
		font-family: 'Moul', sans-serif !important;
	}
	hr{
		padding:0px;
		margin:0px;
		border-top:none !important;
		border-bottom:dotted 1px !important;
	} 
	.text-content table td,.text-content > p{ 
		padding-bottom:5px;
		
	} 
	.content-cover
	{  
		line-height:28px; 
	}  
	p {
		line-height:25px;
	}
</style>