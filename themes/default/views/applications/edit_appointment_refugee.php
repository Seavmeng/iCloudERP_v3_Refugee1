<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">				
			<button type="button" class="close" data-dismiss="modal">
				<span aria-hidden="true"><i class="fa fa-2x">&times;</i></span>
				<span class="sr-only"><?=lang('close');?></span>
			</button>
			<h4 class="modal-title" id="payModalLabel"><?= lang('edit_appointment_refugee'); ?></h4>
		</div>
		<?php $attributes = array('id' => 'form1');?>
		<?php  echo form_open_multipart("applications/edit_appointment_refugee/".$id, $attributes); ?> 
		<div class="modal-body">
							
			<div class="col-sm-12"> 
				<div class="clearfix"></div>   
				<div class="row">  
					<div class="col-sm-6">
						<div class="form-group">
							<?php echo lang('appointment_date', 'appointment_date'); ?>​
							<span class="red">*</span>
							<div class="controls">
								<input type="text" value="<?=  $this->erp->hrsd($result->appointment_date) ?>" name="appointment_date" class="form-control datetime input-sm" />							
							</div>
						</div>
					</div> 
					<div class="col-sm-6">
						<div class="form-group">
							<?php echo lang('appointment_place', 'appointment_place'); ?>​ 
							<span class="red">*</span>
							<div class="controls"> 
								<select class="form-control" name="appointment_place" required>
								<option value='0' ><?= lang('please_select')?></option>
									<?php foreach($offices as $office){ ?>
										<option value="<?= $office->id ?>" <?php echo ($office->id == $result->appointment_place_id)?"selected":""; ?>><?= $office->office ?></option>
									<?php } ?>
								</select>
							</div>
						</div>
					</div> 
					<div class="col-sm-6">
						<div class="form-group">
							<?php echo lang('interview_officer', 'interview_officer'); ?>​ 
							<span class="red">*</span>
							<div class="controls">
								<select class="form-control" name="interview_by">
									<option value='0' ><?= lang('please_select')?></option>
									<?php foreach($users as $user){ ?>
										<option value="<?= $user->id ?>" <?php echo ($result->interview_by==$user->id?"selected":"")?>><?= ucfirst($user->last_name)." ".ucfirst($user->first_name) ?></option>
									<?php } ?>
								</select>
							</div>
						</div>
					</div>
					
					<div class="col-sm-6">
						<div class="form-group">
							<?php echo lang('evaluate_officer', 'evaluate_officer'); ?>​ 
							<span class="red">*</span>
							<div class="controls">
								<select class="form-control" name="evaluation_by" id='evaluate_officer'>
									<option value='0' ><?= lang('please_select')?></option>
									<?php foreach($users as $user){ ?>
										<option value="<?= $user->id ?>" <?php echo ($result->evaluation_by==$user->id?"selected":"")?>><?= ucfirst($user->last_name)." ".ucfirst($user->first_name) ?></option>
									<?php } ?>
								</select>
							</div>
						</div>
					</div> 
				</div> 
			</div>​
			<div class="clearfix"></div>
			
		</div>
		 
		<div class="modal-footer">
		   <?php echo form_submit('submit', lang('submit'), 'class="btn btn-primary save"'); ?>
		</div>
		
		<?php  echo form_close(); ?>
		
	</div>
</div>

<?= $modal_js ?>
<style type="text/css"> 
	ul.ui-autocomplete {
		z-index: 1100 !important;
	}
</style>
<script type="text/javascript">
	$(function(){
		
		 
		 
		 
		 $(".case_no").autocomplete({
			source: function (request, response) {
				var q = request.term;
					$.ajax({
						type: 'get',
						url: '<?= site_url('applications/suggestions_caseno'); ?>',
						dataType: "json",
						data: {
							q : q
						},
						success: function (data) {
							response(data);
						}
					});
			},
			minLength: 1,
			autoFocus: false,
			delay: 200,
			response: function (event, ui) {
				if (ui.content[0].id == 0) {
					$(this).removeClass('ui-autocomplete-loading');
					$(this).removeClass('ui-autocomplete-loading');
				}
			},
			select: function (event, ui) {
				event.preventDefault();
				if (ui.item.id !== 0) { 					
					var id = ui.item.id;					
					
					/*
					*
					* SELECT MULTIPLE MEMBERS
					*/
					
					$.ajax({
						type: 'GET',
						url: '<?= site_url('applications/getFamilyRecognitionRefugees'); ?>',
						dataType: "json",
						data: { id : id },
						success: function (response) {											
							$(".div-member").html(response);						
							$('input[type="checkbox"],[type="radio"]').not('.skip').iCheck({
								checkboxClass: 'icheckbox_square-blue',
								radioClass: 'iradio_square-blue',
								increaseArea: '20%' // optional
							});
						}  
					}); 
					
					/*
					*
					* SELECT MEMBER 
					*/
					
					$.ajax({
						type: 'GET',
						url: '<?= site_url('application_forms/getMemberRefugees'); ?>',
						dataType: "json",
						data: { id : id },
						success: function (response) {											
							$("#members").html(response);
							$("select").select2();
						}  
					});
					
					$("input[name='application_id'").val(ui.item.row.id);
					$(this).val(ui.item.label);
				} 
			}
		});  
		 
		
	});
</script>
