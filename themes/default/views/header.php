<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <base href="<?= site_url() ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $page_title ?>
        <?= $Settings->site_name ?>
    </title>
    <link rel="shortcut icon" href="<?= $assets ?>images/icon.png" />
    <link href="<?= $assets ?>styles/theme.css" rel="stylesheet" />
    <link href="<?= $assets ?>styles/style.css" rel="stylesheet" />
    <script type="text/javascript" src="<?= $assets ?>js/jquery-2.0.3.min.js"></script>
    <script type="text/javascript" src="<?= $assets ?>js/jquery-migrate-1.2.1.min.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Battambang" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Moul|Suwannaphum" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="<?= $assets ?>js/jquery.js"></script>
    <![endif]-->
    <noscript>
        <style type="text/css">
            #loading {
                display: none;
            }
			.text{
				background-color:yellow;
			}
        </style>
    </noscript>
    <?php if ($Settings->rtl) { ?>
        <link href="<?= $assets ?>styles/helpers/bootstrap-rtl.min.css" rel="stylesheet" />
        <link href="<?= $assets ?>styles/style-rtl.css" rel="stylesheet" />
        <script type="text/javascript">
            $(document).ready(function () {
                $('.pull-right, .pull-left').addClass('flip');
            });
        </script>
        <?php } ?>
		<script type="text/javascript">
			$(window).load(function () {
				$("#loading").fadeOut("slow");
			});
		</script>
</head> 
<body> 
    <noscript>
        <div class="global-site-notice noscript">
            <div class="notice-inner">
                <p><strong>JavaScript seems to be disabled in your browser.</strong>
                    <br>You must have JavaScript enabled in your browser to utilize the functionality of this website.</p>
            </div>
        </div>
    </noscript>
    <div id="loading"></div>
    <div id="app_wrapper">
        <header id="header" class="navbar">
            <div class="container">
                <a class="navbar-brand" href="<?= site_url() ?>">
                    <span class="logo"><?= $Settings->site_name ?></span><br/>
                    <small style="font-size:13px;"><?= lang("General Department of Immigration") ?></small></span>
​​​​​​​                 </a>
                <div class="btn-group visible-xs pull-right btn-visible-sm">
                    <button class="navbar-toggle btn" type="button" data-toggle="collapse" data-target="#sidebar_menu">
                        <span class="fa fa-bars"></span>
                    </button>
                    <a href="<?= site_url('users/profile/' . $this->session->userdata('user_id')); ?>" class="btn">
                        <span class="fa fa-user"></span>
                    </a>
                    <a href="<?= site_url('logout'); ?>" class="btn">
                        <span class="fa fa-sign-out"></span>
                    </a>
                </div>
                <div class="header-nav">
                    <ul class="nav navbar-nav pull-right">
                        <li class="dropdown">
                            <a class="btn account dropdown-toggle" data-toggle="dropdown" href="#">
                            <img alt="" src="<?= $this->session->userdata('avatar') ? site_url() . 'assets/uploads/avatars/thumbs/' . $this->session->userdata('avatar') : $assets . 'images/' . $this->session->userdata('gender') . '.png'; ?>" class="mini_avatar img-rounded">                        
							<br>
                            <div class="user">
                                <p><?= strtoupper($this->session->userdata('username')); ?></p>
                            </div>
                        </a>
                            <ul class="dropdown-menu pull-right">
                                <li>
                                    <a href="<?= site_url('users/profile/' . $this->session->userdata('user_id')); ?>">
                                        <i class="fa fa-user"></i>
                                        <?= lang('profile'); ?>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?= site_url('users/profile/' . $this->session->userdata('user_id') . '/#cpassword'); ?>"><i class="fa fa-key"></i> <?= lang('change_password'); ?>
                                    </a>
                                </li>
                                <li class="divider"></li>
                                <li>
                                    <a href="<?= site_url('logout'); ?>">
                                        <i class="fa fa-sign-out"></i>
                                        <?= lang('logout'); ?>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                    <ul class="nav navbar-nav pull-right">
                        <li class="dropdown hidden-xs hidden"><a class="btn tip" title="<?= lang('dashboard') ?>" data-placement="left" href="<?= site_url('welcome') ?>"><i class="fa fa-dashboard"></i><p><?= lang('dashboard') ?></p></a></li>
                       
                                <li class="dropdown hidden-xs hidden">
                                    <a class="btn tip" title="<?= lang('calculator') ?>" data-placement="left" href="#" data-toggle="dropdown">
                                        <i class="fa fa-calculator"></i><p><?= lang('calculator') ?></p>
                                    </a>
                                    <ul class="dropdown-menu pull-right calc">
                                        <li class="dropdown-content">
                                            <span id="inlineCalc"></span>
                                        </li>
                                    </ul>
                                </li> 
							<?php if ($Owner) { ?>
								<li class="dropdown hidden-sm">
									<a class="btn tip" title="<?= lang('settings') ?>" data-placement="left" href="<?= site_url('system_settings') ?>">
										<i class="fa fa-cogs"></i><p><?= lang('settings') ?></p>
									</a>
								</li>
                            <?php } ?>
                                <?php if ($info || !$Owner) { ?>
								<li class="dropdown hidden-sm">
									<a class="btn blightOrange tip" title="<?= lang('alerts') ?>" data-placement="left" data-toggle="dropdown" href="#">
										<i class="fa fa-exclamation-triangle"></i><p><?= lang('alerts') ?> </p> 
									</a> 
									<ul class="dropdown-menu pull-right">
										<?php if($GP['recognition_refugees-index']==1 ||
											 $GP['permanent_resident-index']==1 ||
											 $GP['travel_document_refugee-index'] == 1 ||
											 $GP['refugee_card-index'] == 1 ||
											 $GP['cessation_refugee_declaration-index'] == 1 ||
											 $GP['withdrawal_refugee-index'] ){  ?>
											<?php if($refugee_recognized->notifications > 0){ ?>
												<li>
													<a href="<?= site_url('application_notifications/refugee_recognized?type=notification') ?>" class="">
														<span class="label label-danger pull-right" style="margin-top:3px;"><?= $refugee_recognized->notifications; ?></span>
														<span style="padding-right: 35px;"><?= lang('refugee_recognized') ?></span>
													</a>
												</li>
											<?php } ?>
										<?php } ?> 
										<?php if($GP['appointment_refugees-index']==1 ||
											 $GP['interview_refugees-index']==1 ||
											 $GP['evaluation_refugees-index'] == 1 ||
											 $GP['negative_refugee-index'] == 1 ||
											 $GP['negative_refugee_appeal-index'] == 1){  ?>
											<?php if($asylum_seeker_register->notifications > 0){ ?>
												<li>
													<a href="<?= site_url('application_notifications/asylum_seeker_register?type=notification') ?>" class="">
														<span class="label label-danger pull-right" style="margin-top:3px;"><?= $asylum_seeker_register->notifications; ?></span>
														<span style="padding-right: 35px;"><?= lang('asylum_seeker_register') ?></span>
													</a>
												</li>
											<?php } ?> 
											<?php if($asylum_seeker_withdrawal_list->notifications > 0){ ?>
												<li>
													<a href="<?= site_url('asylum_seekers/withdrawal_asylum') ?>" class="">
														<span class="label label-danger pull-right" style="margin-top:3px;"><?= $asylum_seeker_withdrawal_list->notifications; ?></span>
														<span style="padding-right: 35px;"><?= lang('asylum_seeker_withdrawal_list') ?></span>
													</a>
												</li>
											<?php } ?>
											<?php if($interview_refugee->notifications > 0){ ?>
												<li>
													<a href="<?= site_url('application_notifications/interview_refugees_pending?type=notification') ?>" class="">
														<span class="label label-danger pull-right" style="margin-top:3px;"><?= $interview_refugee->notifications; ?></span>
														<span style="padding-right: 35px;"><?= lang('interview_refugees_pending') ?></span>
													</a>
												</li>
											<?php } ?>
													<?php if($appointment_notification->notifications > 0){ ?>
												<li>
													<a href="<?= site_url('applications/asylum_seeker_withdrawal_list?id='.$appointment_notification->id) ?>" class="">
														<span class="label label-danger pull-right" style="margin-top:3px;"><?= $appointment_notification->notifications ?></span>
														<span style="padding-right: 35px;"><?= lang('asylum_seeker_withdrawal_list') ?></span>
													</a>
												</li>
											<?php } ?>
											<?php if($negative_refugee->notifications > 0){ ?>
												<li>
													<a href="<?= site_url('application_notifications/negative_refugee_pending?type=notification') ?>" class="">
														<span class="label label-danger pull-right" style="margin-top:3px;"><?= $negative_refugee->notifications; ?></span>
														<span style="padding-right: 35px;"><?= lang('negative_refugee_pending') ?></span>
													</a>
												</li>
											<?php } ?> 
											
											
											<?php if($first_negative->notifications > 0){ ?>
											<li>
												<a href="<?= site_url('applications/negative_refugee?id='.$first_negative->id) ?>" class="">
													<span class="label label-danger pull-right" style="margin-top:3px;"><?= $first_negative->notifications ?></span>
													<span style="padding-right: 35px;"><?= lang('first_negative') ?></span>
												</a>
											</li>
											<?php } ?>
											
											<?php if($first_leave_negative->notifications > 0){ ?>
											<li>
												<a href="<?= site_url('applications/negative_refugee?id='.$first_leave_negative->id) ?>" class="">
													<span class="label label-danger pull-right" style="margin-top:3px;"><?= $first_leave_negative->notifications ?></span>
													<span style="padding-right: 35px;"><?= lang('first_leave_negative') ?></span>
												</a>
											</li>
											<?php } ?>
											
											<?php if($second_leave_negative->notifications > 0){ ?>
												<li>
													<a href="<?= site_url('applications/negative_refugee?id='.$second_leave_negative->id) ?>" class="">
														<span class="label label-danger pull-right" style="margin-top:3px;"><?= $second_leave_negative->notifications; ?></span>
														<span style="padding-right: 35px;"><?= lang('second_leave_negative') ?></span>
													</a>
												</li>
											<?php } ?>  
											<?php if($refugee_rejected->notifications > 0){ ?>
												<li>
													<a href="<?= site_url('applications/evaluation_refugee?type=refugee_rejected') ?>" class="">
														<span class="label label-danger pull-right" style="margin-top:3px;"><?= $refugee_rejected->notifications; ?></span>
														<span style="padding-right: 35px;"><?= lang('refugee_rejected') ?></span>
													</a>
												</li>
											<?php } ?>  
										<?php } ?>  
									</ul>
								</li> 
                             <?php  } ?>
                                        <?php if ($events) { ?>
                                            <li class="dropdown hidden-xs">
                                                <a class="btn tip" title="<?= lang('calendar') ?>" data-placement="left" href="#" data-toggle="dropdown">
                                                    <i class="fa fa-calendar"></i><p><?= lang('Calendar') ?></p>
                                                    <span class="number blightOrange black"><?= sizeof($events) ?>Calander Me</span>
                                                </a>
                                                <ul class="dropdown-menu pull-right content-scroll">
                                                    <li class="dropdown-header">
                                                        <i class="fa fa-calendar"></i><p><?= lang('calendar') ?></p>
                                                        <?= lang('upcoming_events'); ?>
                                                    </li>
                                                    <li class="dropdown-content">
                                                        <div class="top-menu-scroll">
                                                            <ol class="oe">
                                                                <?php foreach ($events as $event) {
                                                                            echo '<li><strong>' . date($dateFormats['php_sdate'], strtotime($event->date)) . ':</strong><br>' . $this->erp->decode_html($event->data) . '</li>';
                                                                        } ?>
                                                            </ol>
                                                        </div>
                                                    </li>
                                                    <li class="dropdown-footer">
                                                        <a href="<?= site_url('calendar') ?>" class="btn-block link">
                                                            <i class="fa fa-calendar"></i><p><?= lang('calendar') ?></p>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <?php } ?>
                                            <li class="dropdown hidden-sm">
                                                <a class="btn tip" title="<?= lang('styles') ?>" data-placement="left" data-toggle="dropdown" href="#">
                                                    <i class="fa fa-css3"></i><p><?= lang('styles') ?></p>
                                                </a>
                                                <ul class="dropdown-menu pull-right">
                                                    <li class="bwhite noPadding">
                                                        <a href="#" id="fixed" class="">
                                                            <i class="fa fa-angle-double-left"></i>
                                                            <span id="fixedText">Fixed</span>
                                                        </a>
                                                        <a href="#" id="cssLight" class="grey">
                                                            <i class="fa fa-stop"></i> Grey
                                                        </a>
                                                        <a href="#" id="cssBlue" class="blue">
                                                            <i class="fa fa-stop"></i> Blue
                                                        </a>
                                                        <a href="#" id="cssBlack" class="black">
                                                            <i class="fa fa-stop"></i> Black
                                                        </a>
                                                        <a href="#" id="cssPurpie" class="purple">
                                                            <i class="fa fa-stop"></i> Purple
                                                        </a>
                                                        <a href="#" id="cssGreen" class="green">
                                                            <i class="fa fa-stop"></i> Green
                                                        </a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li class="dropdown hidden-xs">
                                                <a class="btn tip" title="<?= lang('language') ?>" data-placement="left" data-toggle="dropdown" href="#">
                                                    <img src="<?= base_url('assets/images/' . $Settings->language . '.png'); ?>" alt=""><p><?= lang('language') ?></p>
                                                </a>
                                                <ul class="dropdown-menu pull-right">
                                                    <?php $scanned_lang_dir = array_map(function ($path) {
                                                            return basename($path);
                                                        }, glob(APPPATH . 'language/*', GLOB_ONLYDIR));
                                                        foreach ($scanned_lang_dir as $entry) { ?>
                                                        <li>
                                                            <a href="<?= site_url('welcome/language/' . $entry); ?>">
                                                                <img src="<?= base_url(); ?>assets/images/<?= $entry; ?>.png" class="language-img"> 
                                                                &nbsp;&nbsp;<?= ucwords($entry); ?>
                                                            </a>
                                                        </li>
                                                        <?php } ?>
                                                </ul>
                                            </li>
						<?php if ($Owner && $Settings->update) { ?>
							<li class="dropdown hidden-sm">
								<a class="btn blightOrange tip" title="<?= lang('update_available') ?>" data-placement="bottom" data-container="body" href="<?= site_url('system_settings/updates') ?>">
									<i class="fa fa-download"></i>
								</a>
							</li>
						<?php } ?>
						<?php if (($Owner || $Admin)) { ?>
							<li class="dropdown hidden-sm">
								<a class="btn blightOrange tip" title="<?= lang('alerts') ?>" data-placement="left" data-toggle="dropdown" href="#">
									<i class="fa fa-exclamation-triangle"></i><p><?= lang('alerts') ?> </p>
								</a>
								
								<ul class="dropdown-menu pull-right">
									<?php
									
									if($request_card_notification->notifications > 0){ ?>
										<li>
											<a href="<?= site_url('application_forms/refugee_card?v=expired') ?>" class="">											
												<span class="label label-danger pull-right" style="margin-top:3px;"><?= $request_card_notification->notifications ?></span>
												<span style="padding-right: 35px;"><?= lang('request_card_notification') ?></span>
											</a>
										</li>
									<?php } ?>
									
									<?php if($resident_card_notification->notifications > 0){ ?>
										<li>
											<a href="<?= site_url('application_forms/permanent_resident_card_refugee?v=expired') ?>" class="">
												<span class="label label-danger pull-right" style="margin-top:3px;"><?= $resident_card_notification->notifications ?></span>
												<span style="padding-right: 35px;"><?= lang('resident_card_notification') ?></span>
											</a>
										</li>
									<?php } ?>
									
									<?php if($appointment_notification->notifications > 0){ ?>
										<li class="hidden">
											<a href="<?= site_url('applications/appointment_refugees?id='.$appointment_notification->id) ?>" class="">
												<span class="label label-danger pull-right" style="margin-top:3px;"><?= $appointment_notification->notifications ?></span>
												<span style="padding-right: 35px;"><?= lang('appointment_refugees_notification') ?></span>
											</a>
										</li>
									<?php } ?>
									
									<?php if($appointment_notification->notifications > 0){ ?>
										<li class="hidden">
											<a href="<?= site_url('applications/asylum_seeker_withdrawal_list?id='.$appointment_notification->id) ?>" class="">
												<span class="label label-danger pull-right" style="margin-top:3px;"><?= $appointment_notification->notifications ?></span>
												<span style="padding-right: 35px;"><?= lang('asylum_seeker_withdrawal_list') ?></span>
											</a>
										</li>
									<?php } ?>
									
									<?php if($first_negative->notifications > 0){ ?>
									<li>
										<a href="<?= site_url('applications/negative_refugee?id='.$first_negative->id) ?>" class="">
											<span class="label label-danger pull-right" style="margin-top:3px;"><?= $first_negative->notifications ?></span>
											<span style="padding-right: 35px;"><?= lang('first_negative') ?></span>
										</a>
									</li>
									<?php } ?>
									
									<?php if($first_leave_negative->notifications > 0){ ?>
									<li>
										<a href="<?= site_url('applications/negative_refugee?id='.$first_leave_negative->id) ?>" class="">
											<span class="label label-danger pull-right" style="margin-top:3px;"><?= $first_leave_negative->notifications ?></span>
											<span style="padding-right: 35px;"><?= lang('first_leave_negative') ?></span>
										</a>
									</li>
									<?php } ?>
									
									<?php if($second_leave_negative->notifications > 0){ ?>
										<li>
											<a href="<?= site_url('applications/negative_refugee?id='.$second_leave_negative->id) ?>" class="">
												<span class="label label-danger pull-right" style="margin-top:3px;"><?= $second_leave_negative->notifications; ?></span>
												<span style="padding-right: 35px;"><?= lang('second_leave_negative') ?></span>
											</a>
										</li>
									<?php } ?>
									
									<?php if($asylum_seeker_register->notifications > 0){ ?>
										<li>
											<a href="<?= site_url('asylum_seekers/rsd_application?type=notification') ?>" class="">
												<span class="label label-danger pull-right" style="margin-top:3px;"><?= $asylum_seeker_register->notifications; ?></span>
												<span style="padding-right: 35px;"><?= lang('asylum_seeker_register') ?></span>
											</a>
										</li>
									<?php } ?> 
									<?php if($refugee_recognized->notifications > 0){ ?>
										<li>
											<a href="<?= site_url('application_notifications/refugee_recognized?type=notification') ?>" class="">
												<span class="label label-danger pull-right" style="margin-top:3px;"><?= $refugee_recognized->notifications; ?></span>
												<span style="padding-right: 35px;"><?= lang('refugee_recognized') ?></span>
											</a>
										</li>
									<?php } ?> 
									<?php if($interview_appointment->notifications > 0){ ?>
										<li>
											<a href="<?= site_url('applications/appointment_refugees') ?>" class="">
												<span class="label label-danger pull-right" style="margin-top:3px;"><?= $interview_appointment->notifications; ?></span>
												<span style="padding-right: 35px;"><?= lang('appointment_refugees') ?></span>
											</a>
										</li>
									<?php } ?>
									
									<?php if($interview_refugee->notifications > 0){ ?>
										<li>
											<a href="<?= site_url('applications/interview_refugees?type=notification') ?>" class="">
												<span class="label label-danger pull-right" style="margin-top:3px;"><?= $interview_refugee->notifications; ?></span>
												<span style="padding-right: 35px;"><?= lang('interview_refugees') ?></span>
											</a>
										</li>
									<?php } ?>
									
									<?php if($netive_refugee_appeal->notifications > 0){ ?>
										<li>
											<a href="<?= site_url('applications/negative_refugee_appeal?type=notification') ?>" class="">
												<span class="label label-danger pull-right" style="margin-top:3px;"><?= $netive_refugee_appeal->notifications; ?></span>
												<span style="padding-right: 35px;"><?= lang('negative_refugee') ?></span>
											</a>
										</li>
									<?php } ?> 
									<?php if($refugee_rejected->notifications > 0){ ?>
										<li>
											<a href="<?= site_url('applications/evaluation_refugee?type=refugee_rejected') ?>" class="">
												<span class="label label-danger pull-right" style="margin-top:3px;"><?= $refugee_rejected->notifications; ?></span>
												<span style="padding-right: 35px;"><?= lang('refugee_rejected') ?></span>
											</a>
										</li>
									<?php } ?>  
									
									<?php if($asylum_seeker_withdrawal_list->notifications > 0){ ?>
										<li>
											<a href="<?= site_url('asylum_seekers/withdrawal_asylum') ?>" class="">
												<span class="label label-danger pull-right" style="margin-top:3px;"><?= $asylum_seeker_withdrawal_list->notifications; ?></span>
												<span style="padding-right: 35px;"><?= lang('asylum_seeker_withdrawal_list') ?></span>
											</a>
										</li>
									<?php } ?>  
								</ul>
							</li>
						<?php } ?>              
                    </ul>
                </div>
            </div>
        </header> 
        <div class="container" id="container">
            <div class="row" id="main-con">
                <div id="sidebar-left" class="col-lg-2 col-md-2">
                    <div class="sidebar-nav nav-collapse collapse navbar-collapse" id="sidebar_menu">
                        <ul class="nav main-menu">
                            <li class="mm_welcome">
                                <a href="<?= site_url() ?>">
                                    <i class="fa fa-dashboard"></i>
                                    <span class="text"> <?= lang('dashboard'); ?></span>
                                </a>
                            </li>
							<?php
							if ($Owner) { ?>
							<li class="mm_application_forms">
								<a class="dropmenu" href="#">
										<i class="fa fa-users"></i>
										<span class="text"> <?= lang('administrative_manage'); ?> </span>
										<span class="chevron closed"></span>
								</a>
								<ul>
									<li id="application_forms_recognition_refugee">
											<a class="submenu" href="<?= site_url('application_forms/recognition_refugee'); ?>">
												<i class="fa fa-list-alt"></i><span class="text"> <?= lang('recognition_refugee'); ?></span>
											</a>
									</li>

                                    <li id="application_forms_refugee_card">
                                            <a class="submenu" href="<?= site_url('application_forms/refugee_card'); ?>">
                                                <i class="fa fa-list-alt"></i><span class="text"> <?= lang('refugee_card'); ?></span>
                                            </a>
                                    </li>
                                    
									<li id="application_forms_permanent_resident_card_refugee">	
											<a class="submenu" href="<?= site_url('application_forms/permanent_resident_card_refugee'); ?>">
												<i class="fa fa-list-alt"></i><span class="text"> <?= lang('permanent_resident'); ?></span>
											</a>
									</li>  
									
									<li id="application_forms_travel_document_refugee">	
											<a class="submenu" href="<?= site_url('application_forms/travel_document_refugee'); ?>">
												<i class="fa fa-list-alt"></i><span class="text"> <?= lang('travel_document_refugee'); ?></span>
											</a>
									</li>
									  
									<li id="application_forms_cessation_elimination_refugee">	
										<a class="submenu" href="<?= site_url('application_forms/cessation_elimination_refugee'); ?>">
											<i class="fa fa-list-alt"></i><span class="text"> <?= lang('cessation_elimination_refugee'); ?></span>
										</a> 
									</li>
									<li id="application_forms_withdrawal_refugee">	
										<a class="submenu" href="<?= site_url('application_forms/withdrawal_refugee'); ?>">
											<i class="fa fa-list-alt"></i><span class="text"> <?= lang('withdrawal_refugee'); ?></span>
										</a>
									</li>  									
								</ul>
                            </li>
                            
								<li class="mm_asylum_seekers">
                                    <a class="dropmenu" href="#">
                                            <i class="fa fa-users"></i>
                                            <span class="text"> <?= lang('Receiving_applications_registrations'); ?> </span>
                                            <span class="chevron closed"></span>
                                    </a>
                                    <ul>     
                                        <li id="asylum_seekers_counseling">
                                                <a class="submenu" href="<?= site_url('asylum_seekers/counseling'); ?>">
                                                    <i class="fa fa-list"></i><span class="text"> <?= lang('counseling'); ?></span>
                                                </a>
                                        </li>
                                        <li id="asylum_seekers_rsd_application">
                                                <a class="submenu" href="<?= site_url('asylum_seekers/rsd_application'); ?>">
                                                    <i class="fa fa-list"></i><span class="text"> <?= lang('rsd_application'); ?></span>
                                                </a>
                                        </li>
                                        <li id="asylum_seekers_withdrawal_asylum">
                                                <a class="submenu" href="<?= site_url('asylum_seekers/withdrawal_asylum'); ?>">
                                                    <i class="fa fa-list"></i><span class="text"> <?= lang('withdrawal_asylum'); ?></span>
                                                </a>
                                        </li>  
                                        <li id="asylum_seekers_preliminary_stay">
                                                <a class="submenu" href="<?= site_url('asylum_seekers/preliminary_stay'); ?>">
                                                    <i class="fa fa-list"></i><span class="text"> <?= lang('preliminary_stay'); ?></span>
                                                </a>
                                        </li>  
                                    </ul>
                                </li> 		
							<li class="mm_applications">
                                        <a class="dropmenu" href="#">
                                            <i class="fa fa-users"></i>
                                            <span class="text"> <?= lang('research_and_determination_office'); ?> </span>
                                            <span class="chevron closed"></span>
                                        </a>
                                    <ul>   
                                            <li id="applications_appointment_refugees">
                                                <a class="submenu" href="<?= site_url('applications/appointment_refugees'); ?>">
                                                    <i class="fa fa-list-alt"></i><span class="text"> <?= lang('appointment_refugees'); ?></span>
                                                </a>
                                            </li>
                                            <li id="applications_interview_refugees">
                                                <a class="submenu" href="<?= site_url('applications/interview_refugees'); ?>">
                                                    <i class="fa fa-list-alt"></i><span class="text"> <?= lang('interview_refugees'); ?></span>
                                                </a>
                                            </li>	                         
                                            <li id="applications_evaluation_refugee">
                                                <a class="submenu" href="<?= site_url('applications/evaluation_refugee'); ?>">
                                                    <i class="fa fa-list"></i><span class="text"> <?= lang('evaluation_refugee'); ?></span>
                                                </a>
                                            </li>
                                            <li id="applications_negative_refugee">
                                                <a class="submenu" href="<?= site_url('applications/negative_refugee'); ?>">
                                                    <i class="fa fa-list-alt"></i><span class="text"> <?= lang('negative_refugee'); ?></span>
                                                </a>
                                            </li>
											<li id="applications_negative_refugee_appeal">
                                                <a class="submenu" href="<?= site_url('applications/negative_refugee_appeal'); ?>">
                                                    <i class="fa fa-list-alt"></i><span class="text"> <?= lang('negative_refugee_appeal'); ?></span>
                                                </a>
                                            </li>
											<li id="applications_interview_appointment_appeal">
                                                <a class="submenu" href="<?= site_url('applications/interview_appointment_appeal'); ?>">
                                                    <i class="fa fa-list-alt"></i><span class="text"> <?= lang('interview_appointment_appeal'); ?></span>
                                                </a>
                                            </li>
											<li id="applications_interview_appeal">
                                                <a class="submenu" href="<?= site_url('applications/interview_appeal'); ?>">
                                                    <i class="fa fa-list-alt"></i><span class="text"> <?= lang('interview_appeal'); ?></span>
                                                </a>
                                            </li>	
											<li id="applications_interview_evaluate_appeal">
                                                <a class="submenu" href="<?= site_url('applications/interview_evaluate_appeal'); ?>">
                                                    <i class="fa fa-list-alt"></i><span class="text"> <?= lang('interview_evaluate_appeal'); ?></span>
                                                </a>
                                            </li>	
                         
                                            <li id="applications_negative_evaluate_refugee_appeal">
                                                <a class="submenu" href="<?= site_url('applications/negative_evaluate_refugee_appeal'); ?>">
                                                    <i class="fa fa-list"></i><span class="text"> <?= lang('negative_evaluate_refugee_appeal'); ?></span>
                                                </a>
                                            </li>											
											
                                    </ul>
                                </li> 
								
								<li class="mm_application_forms2">
									<a class="dropmenu" href="#">
										<i class="fa fa-users"></i>
										<span class="text"> <?= lang('management_social'); ?> </span>
										<span class="chevron closed"></span>
									</a>
									<ul>  
										<li id="application_forms2_list_of_asylum_seekers">	
											<a class="submenu" href="<?= site_url('application_forms2/list_of_asylum_seekers?k=k4'); ?>">
												<i class="fa fa-list"></i><span class="text"> <?= lang('list_of_asylum_seekers'); ?></span>
											</a>
										</li> 
										<li id="application_forms2_list_of_refugees">	
											<a class="submenu" href="<?= site_url('application_forms2/list_of_refugees'); ?>">
												<i class="fa fa-list"></i><span class="text"> <?= lang('list_of_refugee'); ?></span>
											</a>
										</li> 
										<li id="application_forms2_family_guarantee_request">
											<a class="submenu" href="<?= site_url('application_forms2/family_guarantee_request'); ?>">
												<i class="fa fa-list-alt"></i><span class="text"> <?= lang('family_guarantee_request'); ?></span>
											</a>
										</li>
										
										<li id="application_forms2_home_visit">	
											<a class="submenu" href="<?= site_url('application_forms2/home_visit'); ?>">
												<i class="fa fa-list"></i><span class="text"> <?= lang('home_visit'); ?></span>
											</a>
										</li> 
										<li id="application_forms2_refugee_integration_community_request" class="hidden">
											<a class="submenu" href="<?= site_url('application_forms2/refugee_integration_community_request'); ?>">
												<i class="fa fa-list-alt"></i><span class="text"> <?= lang('ពាក្យសុំធ្វើសមាហរណកម្ម'); ?></span>
											</a>
										</li>
										<li id="application_forms2_refugee_house_address_change_request" class="hidden">
											<a class="submenu" href="<?= site_url('application_forms2/refugee_house_address_change_request'); ?>">
												<i class="fa fa-list-alt"></i><span class="text"> <?= lang('ពាក្យសុំផ្លាស់ប្តូរទីលំនៅ'); ?></span>
											</a>
										</li>
										<li id="application_forms2_expellation_refugee" class="hidden">	
											<a class="submenu" href="<?= site_url('application_forms2/expellation_refugee'); ?>">
												<i class="fa fa-list"></i><span class="text"> <?= lang(' ការបណ្តេញចេញ'); ?></span>
											</a>
										</li>  
									</ul>
								</li> 
								<li class="mm_application_reports">
                                    <a class="dropmenu" href="#">
                                        <i class="fa fa-users"></i>
                                        <span class="text"> <?= lang('reporting_management'); ?> </span>
                                        <span class="chevron closed"></span>
                                    </a>			
                                        <ul> 
											<li id="application_reports_general_report">
                                                <a class="submenu" href="<?= site_url('application_reports/general_report'); ?>">
                                                    <i class="fa fa-list"></i><span class="text"> <?= lang('general_report'); ?></span>
                                                </a>
                                            </li>
											<li id="application_reports_administration_control_report">
												<a class="submenu" href="<?= site_url('application_reports/administration_control_report'); ?>">
													<i class="fa fa-list"></i><span class="text"> <?= lang('administrative_manage'); ?></span>
												</a>
											</li>
                                            <li id="application_reports_asylum_seekers_report">
                                                <a class="submenu" href="<?= site_url('application_reports/asylum_seekers_report'); ?>">
                                                    <i class="fa fa-list"></i><span class="text"> <?= lang('asylum_seekers_report'); ?></span>
                                                </a>
                                            </li>
                                            <li id="application_reports_research_sections">
                                                <a class="submenu" href="<?= site_url('application_reports/research_sections'); ?>">
                                                    <i class="fa fa-list"></i><span class="text"> <?= lang('research_and_determination_office'); ?></span>
                                                </a>
                                            </li>
                                            <li id="application_reports_social_affairs">
                                                <a class="submenu" href="<?= site_url('application_reports/social_affairs'); ?>">
                                                    <i class="fa fa-list"></i><span class="text"> <?= lang('management_social'); ?></span>
                                                </a>
                                            </li>
                                       </ul>
								</li>
								
								<li class="mm_application_setup">
                                    <a class="dropmenu" href="#">
                                        <i class="fa fa-users"></i>
                                        <span class="text"> <?= lang('manage_application_type'); ?> </span>
                                        <span class="chevron closed"></span>
                                    </a>
                                    <ul>     
                                        <li id="application_setup_questions">
                                            <a class="submenu" href="<?= site_url('application_setup/questions'); ?>">
                                                <i class="fa fa-list"></i><span class="text"> <?= lang('questions'); ?></span>
                                            </a>
                                        </li>
                                        <li id="application_setup_offices">
                                            <a class="submenu" href="<?= site_url('application_setup/offices'); ?>">
                                                <i class="fa fa-list"></i><span class="text"> <?= lang('office'); ?></span>
                                            </a>
                                        </li>
										<li id="application_setup_address">
                                            <a class="submenu" href="<?= site_url('application_setup/address'); ?>">
                                                <i class="fa fa-list"></i><span class="text"> <?= lang('address_menu'); ?></span>
                                            </a>
                                        </li>
                                        <li id="application_setup_recognition_templete">
                                            <a class="submenu" href="<?= site_url('application_setup/recognition_templete'); ?>">
                                                <i class="fa fa-list"></i><span class="text"> <?= lang('recognition_templete'); ?></span>
                                            </a>
                                        </li>
                                        <li id="application_setup_case_prefix">
                                            <a class="submenu" href="<?= site_url('application_setup/case_prefix'); ?>">
                                                <i class="fa fa-list"></i><span class="text"> <?= lang('case_prefix'); ?></span>
                                            </a>
                                        </li>
                                        <li id="application_setup_family_relationship">
                                            <a class="submenu" href="<?= site_url('application_setup/family_relationship'); ?>">
                                                <i class="fa fa-list"></i><span class="text"> <?= lang('family_relationship'); ?></span>
                                            </a>
                                        </li>
                                        <li id="application_setup_department_refugee">
                                            <a class="submenu" href="<?= site_url('application_setup/department_refugee'); ?>">
                                                <i class="fa fa-list"></i><span class="text"> <?= lang('department_refugee'); ?></span>
                                            </a>
                                        </li>
                                        <li id="application_setup_reasons">
                                            <a class="submenu" href="<?= site_url('application_setup/reasons'); ?>">
                                                <i class="fa fa-list"></i><span class="text"> <?= lang('reasons'); ?></span>
                                            </a>
                                        </li>
                                        <li id="application_setup_categories">
                                            <a class="submenu" href="<?= site_url('application_setup/categories'); ?>">
                                                <i class="fa fa-list"></i><span class="text"> <?= lang('categories'); ?></span>
                                            </a>
                                        </li>
                                        <li id="application_setup_audit_logs">
                                            <a class="submenu" href="<?= site_url('application_setup/audit_logs'); ?>">
                                                <i class="fa fa-list"></i><span class="text"> <?= lang('audit_logs'); ?></span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
								<li class="mm_application_setup">
								   <a class="dropmenu" href="#">
								   <i class="fa fa-users"></i>
								   <span class="text"> <?= lang('manage_address'); ?> </span>
								   <span class="chevron closed"></span>
								   </a>
								   <ul>
									  <li id="application_setup_countries">
										 <a class="submenu" href="<?= site_url('application_setup/countries'); ?>">
										 <i class="fa fa-list"></i><span class="text"> <?= lang('countries'); ?></span>
										 </a>
									  </li>
									  <li id="application_setup_states">
										 <a class="submenu" href="<?= site_url('application_setup/states'); ?>">
										 <i class="fa fa-list"></i><span class="text"> <?= lang('states'); ?></span>
										 </a>
									  </li>
									  <li id="application_setup_provinces">
										 <a class="submenu" href="<?= site_url('application_setup/provinces'); ?>">
										 <i class="fa fa-list"></i><span class="text"> <?= lang('Provinces'); ?></span>
										 </a>
									  </li>
									  <li id="application_setup_districts">
										 <a class="submenu" href="<?= site_url('application_setup/districts'); ?>">
										 <i class="fa fa-list"></i><span class="text"> <?= lang('Districts'); ?></span>
										 </a>
									  </li>
									  <li id="application_setup_communces">
										 <a class="submenu" href="<?= site_url('application_setup/communces'); ?>">
										 <i class="fa fa-list"></i><span class="text"> <?= lang('Communces'); ?></span>
										 </a>
									  </li>
									  <li id="application_setup_tags">
										 <a class="submenu" href="<?= site_url('application_setup/tags'); ?>">
										 <i class="fa fa-list"></i><span class="text"> <?= lang('tags'); ?></span>
										 </a>
									  </li>
								   </ul>
								</li>						
                                <li class="mm_auth mm_customers mm_suppliers mm_billers">
                                    <a class="dropmenu" href="#">
                                        <i class="fa fa-users"></i>
                                        <span class="text"> <?= lang('manage_people'); ?> </span>
                                        <span class="chevron closed"></span>
                                    </a>
                                    <ul>
                                            <li id="auth_users">
                                                <a class="submenu" href="<?= site_url('users'); ?>">
                                                    <i class="fa fa-users"></i><span class="text"> <?= lang('list_users'); ?></span>
                                                </a>
                                            </li>
                                            <li id="auth_create_user">
                                                <a class="submenu" href="<?= site_url('users/create_user'); ?>">
                                                    <i class="fa fa-user-plus"></i><span class="text"> <?= lang('new_user'); ?></span>
                                                </a>
                                            </li>
                                            <li id="billers_index">
                                                <a class="submenu" href="<?= site_url('billers'); ?>">
                                                    <i class="fa fa-users"></i><span class="text"> <?= lang('list_billers'); ?></span>
                                                </a>
                                            </li>
                                            <li id="billers_index">
                                                <a class="submenu" href="<?= site_url('billers/add'); ?>" data-toggle="modal" data-target="#myModal">
                                                    <i class="fa fa-plus-circle"></i><span class="text"> <?= lang('add_biller'); ?></span>
                                                </a>
                                            </li>
                                    </ul>
                                </li>
								                                
								<?php if ($Owner) { ?>
									<li class="mm_system_settings <?= strtolower($this->router->fetch_method()) != 'settings' ? '' : 'mm_pos' ?>">
										<a class="dropmenu" href="#">
											<i class="fa fa-cog"></i><span class="text"> <?= lang('settings'); ?> </span>
											<span class="chevron closed"></span>
										</a>
										<ul>
											<li id="system_settings_index">
												<a href="<?= site_url('system_settings') ?>">
													<i class="fa fa-cog"></i><span class="text"> <?= lang('system_settings'); ?></span>
												</a>
											</li>
											<li id="system_settings_warehouses">
												<a href="<?= site_url('system_settings/warehouses') ?>">
													<i class="fa fa-upload"></i><span class="text"> <?= lang('warehouses'); ?></span>
												</a>
											</li>
											<li id="system_settings_change_logo">
												<a href="<?= site_url('system_settings/change_logo') ?>" data-toggle="modal" data-target="#myModal">
													<i class="fa fa-upload"></i><span class="text"> <?= lang('change_logo'); ?></span>
												</a>
											</li>
											<li id="system_settings_user_groups">
												<a href="<?= site_url('system_settings/user_groups') ?>">
													<i class="fa fa-key"></i><span class="text"> <?= lang('group_permissions'); ?></span>
												</a>
											</li>
										</ul>
									</li>
                                <?php } ?>
									
								<?php } else { ?>
								
								<?php if($GP['recognition_refugees-index']==1 ||
										 $GP['permanent_resident-index']==1 ||
										 $GP['travel_document_refugee-index'] == 1 ||
										 $GP['refugee_card-index'] == 1 ||
										 $GP['cessation_refugee_declaration-index'] == 1 ||
										 $GP['withdrawal_refugee-index']==1 ){  ?>
								<li class="mm_application_forms">
									<a class="dropmenu" href="#">
										<i class="fa fa-barcode"></i><span class="text"> <?= lang('administrative_manage'); ?></span> <span class="chevron closed"></span>
									</a>
										<ul>
											<?php if($GP['recognition_refugees-index']==1){ ?>
												<li id="application_forms_recognition_refugee">
													<a class="submenu" href="<?= site_url('application_forms/recognition_refugee'); ?>">
														<i class="fa fa-list-alt"></i><span class="text"> <?= lang('recognition_refugee'); ?></span>
													</a>														
												</li>												
											<?php } ?>		
											
											<?php if($GP['permanent_resident-index']==1){?>
												<li id="application_forms_permanent_resident_card_refugee">	
													<a class="submenu" href="<?= site_url('application_forms/permanent_resident_card_refugee'); ?>">
														<i class="fa fa-list-alt"></i><span class="text"> <?= lang('permanent_resident'); ?></span>
													</a>
												</li> 
											<?php } ?>	
											<?php if($GP['refugee_card-index'] == 1){?>											
												<li id="application_forms_refugee_card">
													<a class="submenu" href="<?= site_url('application_forms/refugee_card'); ?>">
														<i class="fa fa-list-alt"></i><span class="text"> <?= lang('refugee_card'); ?></span>
													</a>
												</li> 
											<?php }?>
											<?php if($GP['travel_document_refugee-index'] == 1){?>
												<li id="application_forms_travel_document_refugee">	
													<a class="submenu" href="<?= site_url('application_forms/travel_document_refugee'); ?>">
														<i class="fa fa-list-alt"></i><span class="text"> <?= lang('travel_document_refugee'); ?></span>
													</a>
												</li>
											<?php }?>
											
											
											
											<?php if($GP['cessation_refugee_declaration-index'] == 1){?>
												<li id="application_forms_cessation_elimination_refugee">	
													<a class="submenu" href="<?= site_url('application_forms/cessation_elimination_refugee'); ?>">
														<i class="fa fa-list-alt"></i><span class="text"> <?= lang('cessation_elimination_refugee'); ?></span>
													</a> 
												</li>
											<?php }?>
											
											<?php if($GP['withdrawal_refugee-index']==1){?>
												<li id="application_forms_withdrawal_refugee">	
													<a class="submenu" href="<?= site_url('application_forms/withdrawal_refugee'); ?>">
														<i class="fa fa-list-alt"></i><span class="text"> <?= lang('withdrawal_refugee'); ?></span>
													</a>
												</li>  									
											<?php }?>
										</ul>
								</li>
								
								<?php } ?>
								
								<?php if($GP['counseling-index']==1 ||
										 $GP['rsd_applications-index']==1 ||
										 $GP['withdrawal_of_asylum-index'] == 1 ||
										 $GP['preliminary_stay-index'] == 1){  ?>
										 
								<li class="mm_asylum_seekers">
									<a class="dropmenu" href="#">
										<i class="fa fa-barcode"></i><span class="text"> <?= lang('Receiving_applications_registrations'); ?></span> <span class="chevron closed"></span>
									</a>
										<ul>
											<?php if($GP['counseling-index'] == 1){?>
												<li id="asylum_seekers_counseling">
													<a class="submenu" href="<?= site_url('asylum_seekers/counseling'); ?>">
														<i class="fa fa-list"></i><span class="text"> <?= lang('counseling'); ?></span>
													</a>
												</li>
											<?php }?>
											
											<?php if($GP['rsd_applications-index'] == 1){?>
												<li id="asylum_seekers_rsd_application">
													<a class="submenu" href="<?= site_url('asylum_seekers/rsd_application'); ?>">
														<i class="fa fa-list"></i><span class="text"> <?= lang('rsd_application'); ?></span>
													</a>
												</li>
											<?php }?>
											
											<?php if($GP['withdrawal_of_asylum-index'] == 1){ ?>
												<li id="asylum_seekers_withdrawal_asylum">
													<a class="submenu" href="<?= site_url('asylum_seekers/withdrawal_asylum'); ?>">
														<i class="fa fa-list"></i><span class="text"> <?= lang('withdrawal_asylum'); ?></span>
													</a>
												</li>
											<?php }?>
											
											<?php if($GP['preliminary_stay-index']==1){ ?>
												<li id="asylum_seekers_preliminary_stay">
													<a class="submenu" href="<?= site_url('asylum_seekers/preliminary_stay'); ?>">
														<i class="fa fa-list"></i><span class="text"> <?= lang('preliminary_stay'); ?></span>
													</a>
												</li>
											<?php } ?>
										</ul>
								</li>
								
								<?php } ?>
								
								<?php if($GP['appointment_refugees-index']==1 ||
										 $GP['interview_refugees-index']==1 ||
										 $GP['evaluation_refugees-index'] == 1 ||
										 $GP['negative_refugee-index'] == 1 ||
										 $GP['negative_refugee_appeal-index'] == 1){  ?>
										 
								<li class="mm_applications">
									<a class="dropmenu" href="#">
										<i class="fa fa-barcode"></i><span class="text"> <?= lang('research_and_determination_office'); ?></span> <span class="chevron closed"></span>
									</a>
										<ul>
											<?php if($GP['appointment_refugees-index']==1){ ?>
											<li id="applications_appointment_refugees">
                                                <a class="submenu" href="<?= site_url('applications/appointment_refugees'); ?>">
                                                    <i class="fa fa-list-alt"></i><span class="text"> <?= lang('appointment_refugees'); ?></span>
                                                </a>
                                            </li>
										<?php }?>		

										<?php if($GP['interview_refugees-index'] == 1){?>
											<li id="applications_interview_refugees">
                                                <a class="submenu" href="<?= site_url('applications/interview_refugees'); ?>">
                                                    <i class="fa fa-list-alt"></i><span class="text"> <?= lang('interview_refugees'); ?></span>
                                                </a>
                                            </li>
										<?php }?>
										
										<?php if($GP['evaluation_refugees-index'] == 1){?>
											<li id="applications_evaluation_refugee">
                                                <a class="submenu" href="<?= site_url('applications/evaluation_refugee'); ?>">
                                                    <i class="fa fa-list"></i><span class="text"> <?= lang('evaluation_refugee'); ?></span>
                                                </a>
                                            </li>
										<?php }?>

										<?php if($GP['negative_refugee-index'] == 1){?>
											<li id="applications_negative_refugee">
                                                <a class="submenu" href="<?= site_url('applications/negative_refugee'); ?>">
                                                    <i class="fa fa-list-alt"></i><span class="text"> <?= lang('negative_refugee'); ?></span>
                                                </a>
                                            </li>
										<?php }?>
										
										<?php if($GP['negative_refugee_appeal-index'] == 1){?>
											<li id="applications_negative_refugee_appeal">
                                                <a class="submenu" href="<?= site_url('applications/negative_refugee_appeal'); ?>">
                                                    <i class="fa fa-list-alt"></i><span class="text"> <?= lang('negative_refugee_appeal'); ?></span>
                                                </a>
                                            </li> 
										<?php }?>
										<?php if($GP['interview_appointment_appeal-index'] == 1){?>
											<li id="applications_interview_appointment_appeal">
                                                <a class="submenu" href="<?= site_url('applications/interview_appointment_appeal'); ?>">
                                                    <i class="fa fa-list-alt"></i><span class="text"> <?= lang('interview_appointment_appeal'); ?></span>
                                                </a>
                                         <?php }?> 
										 <?php if($GP['interview_appeal-index'] == 1){?>
											<li id="applications_interview_appeal">
                                                <a class="submenu" href="<?= site_url('applications/interview_appeal'); ?>">
                                                    <i class="fa fa-list-alt"></i><span class="text"> <?= lang('interview_appeal'); ?></span>
                                                </a>
                                            </li>
										<?php }?> 
										<?php if($GP['interview_evaluate_appeal-index'] == 1){?>										
											<li id="applications_interview_evaluate_appeal">
                                                <a class="submenu" href="<?= site_url('applications/interview_evaluate_appeal'); ?>">
                                                    <i class="fa fa-list-alt"></i><span class="text"> <?= lang('interview_evaluate_appeal'); ?></span>
                                                </a>
                                            </li>
										<?php }?> 
										<?php if($GP['negative_evaluate_refugee_appeal-index'] == 1){?>
                                            <li id="applications_negative_evaluate_refugee_appeal">
                                                <a class="submenu" href="<?= site_url('applications/negative_evaluate_refugee_appeal'); ?>">
                                                    <i class="fa fa-list"></i><span class="text"> <?= lang('negative_evaluate_refugee_appeal'); ?></span>
                                                </a>
                                            </li>	
										<?php }?>   	
										</ul>
								</li> 
								<?php }?>
								
								<?php if($GP['home_visits-index']==1 ||
										 $GP['family_guarantee_request-index']==1){  ?>
										 
								<li class="mm_application_forms2">
									<a class="dropmenu" href="#">
										<i class="fa fa-barcode"></i><span class="text"> <?= lang('management_social'); ?></span> <span class="chevron closed"></span>
									</a>
									<ul>
									<?php if($GP['home_visits-index'] == 1){?> 
										
										<li id="application_forms2_list_of_asylum_seekers">	
											<a class="submenu" href="<?= site_url('application_forms2/list_of_asylum_seekers?k=k4'); ?>">
												<i class="fa fa-list"></i><span class="text"> <?= lang('list_of_asylum_seekers'); ?></span>
											</a>
										</li> 
										<li id="application_forms2_list_of_refugees">	
											<a class="submenu" href="<?= site_url('application_forms2/list_of_refugees'); ?>">
												<i class="fa fa-list"></i><span class="text"> <?= lang('list_of_refugee'); ?></span>
											</a>
										</li> 
										<li id="application_forms2_home_visit">	
											<a class="submenu" href="<?= site_url('application_forms2/home_visit'); ?>">
												<i class="fa fa-list"></i><span class="text"> <?= lang('home_visit'); ?></span>
											</a>
										</li>
									<?php }?>
									<?php if($GP['family_guarantee_request-index'] == 1){?>
										<li id="application_forms2_family_guarantee_request">
											<a class="submenu" href="<?= site_url('application_forms2/family_guarantee_request'); ?>">
												<i class="fa fa-list-alt"></i><span class="text"> <?= lang('family_guarantee_request'); ?></span>
											</a>
										</li> 
									<?php }?>
									</ul>
								</li> 
								<?php }?> 
								<?php if(($Owner || $Admin) || $this->session->userdata("allow_permission")){ ?> 
									<li class="mm_application_reports">
										<a class="dropmenu" href="#">
											<i class="fa fa-users"></i>
											<span class="text"> <?= lang('reporting_management'); ?> </span>
											<span class="chevron closed"></span>
										</a>			
											<ul> 
												<li id="application_reports_general_report">
													<a class="submenu" href="<?= site_url('application_reports/general_report'); ?>">
														<i class="fa fa-list"></i><span class="text"> <?= lang('general_report'); ?></span>
													</a>
												</li>
												
												<?php if($GP['administration_control_report-index'] == 1){ ?> 
												<li id="application_reports_administration_control_report">
													<a class="submenu" href="<?= site_url('application_reports/administration_control_report'); ?>">
														<i class="fa fa-list"></i><span class="text"> <?= lang('administrative_manage'); ?></span>
													</a>
												</li>
												<?php }  ?>
												<?php if($GP['asylum_seekers_report-index'] == 1){ ?> 
												<li id="application_reports_asylum_seekers_report">
													<a class="submenu" href="<?= site_url('application_reports/asylum_seekers_report'); ?>">
														<i class="fa fa-list"></i><span class="text"> <?= lang('asylum_seekers_report'); ?></span>
													</a>
												</li> 
												<?php }  ?>
												<?php if($GP['research_sections-index'] == 1){ ?> 
												<li id="application_reports_research_sections">
													<a class="submenu" href="<?= site_url('application_reports/research_sections'); ?>">
														<i class="fa fa-list"></i><span class="text"> <?= lang('research_and_determination_office'); ?></span>
													</a>
												</li>
												<?php }  ?>
												<?php if($GP['social_affairs-index'] == 1){ ?> 
												<li id="application_reports_social_affairs">
													<a class="submenu" href="<?= site_url('application_reports/social_affairs'); ?>">
														<i class="fa fa-list"></i><span class="text"> <?= lang('management_social'); ?></span>
													</a>
												</li>
												<?php }  ?>
										   </ul>
									</li>
									
									<li class="mm_system_settings <?= strtolower($this->router->fetch_method()) != 'settings' ? '' : 'mm_pos' ?>">
										<a class="dropmenu" href="#">
											<i class="fa fa-cog"></i><span class="text"> <?= lang('settings'); ?> </span>
											<span class="chevron closed"></span>
										</a>
										<ul>
											
											<li id="system_settings_user_groups">
												<a href="<?= site_url('system_settings/user_groups') ?>">
													<i class="fa fa-key"></i><span class="text"> <?= lang('group_permissions'); ?></span>
												</a>
											</li>										
										</ul>
									</li>
								<?php } ?>
								 <li class="mm_application_setup">
								   <a class="dropmenu" href="#">
								   <i class="fa fa-users"></i>
								   <span class="text"> <?= lang('manage_address'); ?> </span>
								   <span class="chevron closed"></span>
								   </a>
								   <ul>
									  <li id="application_setup_countries">
										 <a class="submenu" href="<?= site_url('application_setup/countries'); ?>">
										 <i class="fa fa-list"></i><span class="text"> <?= lang('countries'); ?></span>
										 </a>
									  </li>
									  <li id="application_setup_states">
										 <a class="submenu" href="<?= site_url('application_setup/states'); ?>">
										 <i class="fa fa-list"></i><span class="text"> <?= lang('states'); ?></span>
										 </a>
									  </li>
									  <li id="application_setup_provinces">
										 <a class="submenu" href="<?= site_url('application_setup/provinces'); ?>">
										 <i class="fa fa-list"></i><span class="text"> <?= lang('Provinces'); ?></span>
										 </a>
									  </li>
									  <li id="application_setup_districts">
										 <a class="submenu" href="<?= site_url('application_setup/districts'); ?>">
										 <i class="fa fa-list"></i><span class="text"> <?= lang('Districts'); ?></span>
										 </a>
									  </li>
									  <li id="application_setup_communces">
										 <a class="submenu" href="<?= site_url('application_setup/communces'); ?>">
										 <i class="fa fa-list"></i><span class="text"> <?= lang('Communces'); ?></span>
										 </a>
									  </li>
									  <li id="application_setup_tags">
										 <a class="submenu" href="<?= site_url('application_setup/tags'); ?>">
										 <i class="fa fa-list"></i><span class="text"> <?= lang('tags'); ?></span>
										 </a>
									  </li>
								   </ul>
								</li>
								 
                             </ul>							 
                          </li>
						<?php } ?>  
                        </ul>
						
                    </div>
                    <a href="#" id="main-menu-act" class="full visible-md visible-lg">
                        <i class="fa fa-angle-double-left"></i>
                    </a>
                </div>
                <div id="content" class="col-lg-10 col-md-10">
                    <div class="row">
                        <div class="col-sm-12 col-md-12">
                            <ul class="breadcrumb">
                                <?php
                            foreach ($bc as $b) {
                                if ($b['link'] === '#') {
                                    echo '<li class="active">' . $b['page'] . '</li>';
                                } else {
                                    echo '<li><a href="' . $b['link'] . '">' . $b['page'] . '</a></li>';
                                }
                            }
                            ?>
                                    <li class="right_log hidden">
                                        <?= lang('your_ip') . ' ' . $ip_address . " <span class='hidden-sm'>( " . lang('last_login_at') . ": " . date($dateFormats['php_ldate'], $this->session->userdata('old_last_login')) . " " . ($this->session->userdata('last_ip') != $ip_address ? lang('ip:') . ' ' . $this->session->userdata('last_ip') : '') . " )</span>" ?>
                                    </li>
                            </ul>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <?php if ($message) { ?>
                                <div class="alert alert-success">
                                    <button data-dismiss="alert" class="close" type="button">×</button>
                                    <?= $message; ?>
                                </div>
                                <?php } ?>
                                    <?php if ($error) { ?>
                                        <div class="alert alert-danger">
                                            <button data-dismiss="alert" class="close" type="button">×</button>
                                            <?= $error; ?>
                                        </div>
                                        <?php } ?>
                                            <?php if ($warning) { ?>
                                                <div class="alert alert-warning">
                                                    <button data-dismiss="alert" class="close" type="button">×</button>
                                                    <?= $warning; ?>
                                                </div>
                                            <?php } ?>					
                        <?php
                        if ($info) {
                            foreach ($info as $n) {
                                if (!$this->session->userdata('hidden' . $n->id)) {?>
									<div class="alert alert-info">
										<a href="#" id="<?= $n->id ?>" class="close hideComment external" data-dismiss="alert">&times;</a>
										<?= $n->comment; ?>
									</div>
                                <?php }
                            }
                        } ?>
                        <div id="alerts"></div>