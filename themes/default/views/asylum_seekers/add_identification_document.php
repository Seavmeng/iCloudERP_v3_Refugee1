<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">				
			<button type="button" class="close" data-dismiss="modal">
				<span aria-hidden="true"><i class="fa fa-2x">&times;</i></span>
				<span class="sr-only"><?=lang('close');?></span>
			</button>
			<h4 class="modal-title" id="payModalLabel"><?= lang('add_identification_document'); ?>
				
			</h4>
		</div>
		
		<?php $attributes = array('id' => 'form1');?>
		
		<?php  echo form_open_multipart("asylum_seekers/add_identification_document/".$id, $attributes); ?> 
		
		<div class="modal-body"> 
			<div class="col-sm-12" >
				<div class="form-group">
					<?php echo lang('ប្រភេទឯកសារ ៖', 'ប្រភេទឯកសារ ៖'); ?>​
					<span class="red">*</span>
					<div class="controls">					
						<input type="text" class="form-control input-sm" name="document_type"  id="document_type" />
					</div>
				</div> 
				<div class="form-group">
					<?php echo lang('កន្លែងផ្តល់​ ( Issued Place)៖', '  កន្លែងផ្តល់​ ( Issued Place)៖'); ?>​
					<span class="red">*</span>
					<div class="controls">
						<input type="text" class="form-control input-sm"  name="issued_place"   id="issued_place" />
					</div>
				</div>
				
				<div class="form-group">
					<?php echo lang('ផ្តល់នៅថ្ងៃទី (Issued Date)៖', 'ផ្តល់នៅថ្ងៃទី (Issued Date) ៖'); ?>​
					<span class="red">*</span>
					<div class="controls">
						<input type="text" class="form-control input-sm date"  name="issued_date"  id="issued_date" />
					</div>
				</div> 
				<div class="form-group">
					<?php echo lang('ផុតកំណត់នៅថ្ងៃទី (Expired Date) ៖', 'ផុតកំណត់នៅថ្ងៃទី (Expired Date)'); ?>​
					<span class="red">*</span>
					<div class="controls">
						<input type="text" class="form-control input-sm date "  name="expired_date"  id="expired_date" />
					</div>
				</div>  
				<div class="form-group">
					<?php echo lang(' ឯកសារ ៖', 'ឯកសារ ៖'); ?>​
					<span class="red">*</span>
					<div class="controls">
						<input type="file" class="input-sm "  name="attachment"  id="attachment" />
					</div>
				</div> 
			</div>​
			
			<div class="clearfix"></div>
			
		</div>
		
		 <div class="modal-footer">
			<?php echo form_submit('submit', lang('submit'), 'class="btn btn-primary save-data"'); ?>
		 </div>
		
		<?php  echo form_close(); ?>
	</div>
</div>

<script type="text/javascript">
	$(function(){
		$("select").select2();
	}); 
	$(".save-data").on('click',function(){
		var document_type = $('#document_type').val();
		var issued_place = $('#issued_place').val();
		var issued_date = $('#issued_date').val();
		var expired_date = $('#expired_date').val();
		var original_provided = $('#original_provided').val();
		if(document_type=='' || issued_place=='' || issued_date=='' || expired_date == '' || original_provided == ''){ 
			bootbox.alert('<?= lang("please_select_all");?>');
			return false;
		}  
	});

	
</script>
