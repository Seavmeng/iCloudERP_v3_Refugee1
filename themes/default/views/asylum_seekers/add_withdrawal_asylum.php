
<div class="modal-dialog modal-lg">
	<div class="modal-content">
		<div class="modal-header">				
			<button type="button" class="close" data-dismiss="modal">
				<span aria-hidden="true"><i class="fa fa-2x">&times;</i></span>
				<span class="sr-only"><?=lang('close');?></span>
			</button>
			<h4 class="modal-title" id="payModalLabel"><?= lang("add_withdrawal_asylum") ?></h4>
		</div>
		<?php  echo form_open_multipart("asylum_seekers/add_withdrawal_asylum/".$result->id); ?>
		
		<div class="modal-body ">  
			 <div class="col-sm-9">
				<div class="form-group">
					<?php echo lang('case_no', 'case_no'); ?>​ 
					<span class="red">*</span>
					<div class="control">
						<input type="text" class="form-control input-sm case_no" id="case_no" >
						<input type="hidden" name="application_id" id="application_id" value=""> 
					</div>
				</div>
				
				<div class="form-group">
					<?php echo lang('reasons', 'reasons'); ?>​ 
					<span class="red">*</span> 
					<textarea class="form-control redactor_box" name="reasons" >
						<?= $withdrawal->reasons;?>
					</textarea> 
				</div>
			</div>
			<div class="col-md-3">
				<?= lang("photo", "photo"); ?>
				<?php
					$photo = "no_image.png";
					if(!empty($result->photo) || !file_exists("assets/uploads/".$result->photo)){
						$photo = $result->photo;
					}
				?>
				<div class="form-group">
				  <div class="main-img-preview">
					<img class="thumbnail img-preview" id='img_preview' src="<?= site_url("assets/uploads/".$photo); ?>" title="Preview Logo">
				  </div>
				  <div class="input-group">
					<input id="fakeUploadLogo" class="form-control input-sm fake-shadow" placeholder="<?= lang("choose_file") ?>" disabled="disabled">
					<div class="input-group-btn">
					  <div class="fileUpload btn btn-danger fake-shadow">
						<span><i class="fa fa-upload"></i>&nbsp;&nbsp;<?= lang("upload") ?></span>
						<input id="logo-id" accept=".gif,.jpg,.png" data-maxfile="1024" name="photo" type="file" class="attachment_upload">
					  </div>
					</div>
				  </div>
				</div>
				<input type="hidden" name="photoHidden" />
			</div>
			<div class="clearfix"></div> 
		</div>
		
		 <div class="modal-footer">
			<?php echo form_submit('form2', lang('submit'), 'class="btn btn-primary save"'); ?>
		</div>
		<?php  echo form_close(); ?>
	</div>
</div>
<script type="text/javascript" src="<?= $assets ?>styles/helpers/components/js/bootstrap-formhelpers.js"></script>	
<link href="<?= $assets ?>styles/helpers/components/js/bootstrap-formhelpers.min.css" rel="stylesheet" />
<style type="text/css"> 
	ul.ui-autocomplete {
	    z-index: 1100;
	}
	/* File Upload */
	.fake-shadow {
		box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
	}
	.fileUpload {
		position: relative;
		overflow: hidden;
	}
	.fileUpload #logo-id {
		position: absolute;
		top: 0;
		right: 0;
		margin: 0;
		padding: 0;
		font-size: 33px;
		cursor: pointer;
		opacity: 0;
		filter: alpha(opacity=0);
	}
	.img-preview {
		max-width: 100%;
		width:200px !important;
	}
</style>
<script type="text/javascript">
	$(document).ready(function() {
		var brand = document.getElementById('logo-id');
		brand.className = 'attachment_upload';
		brand.onchange = function() {
			document.getElementById('fakeUploadLogo').value = this.value.substring(12);
		};
		
		function readURL(input) {
			if (input.files && input.files[0]) {
				var reader = new FileReader();
				
				reader.onload = function(e) {
					$('.img-preview').attr('src', e.target.result);
				};
				reader.readAsDataURL(input.files[0]);
			}
		}
		$("#logo-id").change(function() {
			readURL(this);
		});
		
		$("#upload").click(function(){
			var filename = "<?= site_url()?>/" + $("#webcam-preview img").attr("src");
			$('.img-preview').attr('src', filename);
			return false;
		});
		
	});
</script>
<script type="text/javascript">
	$(".save").on("click",function(){ 
		var application_id = $("#application_id").val();
		var case_no = $("#case_no").val(); 
		if(application_id == "" || case_no == ""){
			bootbox.alert("<?= lang('please_select_all');?>");
			return false;
		} 
	});
	$(function(){ 
		$(".case_no").autocomplete({
			source: function (request, response) {
				var q = request.term;
				$.ajax({
					type: 'get',
					url: '<?= site_url('applications/suggestions_caseno_withdrawal'); ?>',
					dataType: "json",
					data: {
						q : q
					},
					success: function (data) {
						response(data);
					}
				});
			},
			minLength: 1,
			autoFocus: false,
			delay: 200,
			response: function (event, ui) {
				if (ui.content[0].id == 0) {
					$(this).removeClass('ui-autocomplete-loading');
					$(this).removeClass('ui-autocomplete-loading');
				}
			},
			select: function (event, ui) {
				event.preventDefault();
				if (ui.item.id !== 0) {  
					$("input[name='application_id'").val(ui.item.row.application_id);
					$("input[name='photoHidden']").val(ui.item.row.photo);			
					$("#img_preview").attr('src','assets/uploads/'+ui.item.row.photo).change();
					 $(this).val(ui.item.label);
					 
				} 
			}
		});
		
	}); 
	function pad(str) {
	  str = str.toString();
	  return str.length < 2 ? pad("0" + str, 2) : str;
	}
</script>
<?= $modal_js ?>