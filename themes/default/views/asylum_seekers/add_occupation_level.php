<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">				
			<button type="button" class="close" data-dismiss="modal">
				<span aria-hidden="true"><i class="fa fa-2x">&times;</i></span>
				<span class="sr-only"><?=lang('close');?></span>
			</button>
			<h4 class="modal-title" id="payModalLabel"><?= lang('add_occupation_level'); ?>
				
			</h4>
		</div>
		
		<?php $attributes = array('id' => 'form1');?>
		
		<?php  echo form_open_multipart("asylum_seekers/add_occupation_level/".$id, $attributes); ?> 
		
		<div class="modal-body"> 
			<div class="col-sm-12" >
				<div class="form-group">
					<?php echo lang('ឈ្មោះក្រុមហ៊ុន / ស្ថាប័ន ៖', 'ឈ្មោះក្រុមហ៊ុន / ស្ថាប័ន ៖'); ?>​
					<span class="red">*</span>
					<div class="controls">					
						<input type="text" class="form-control input-sm" name="name_employer"  id="name_employer" />
					</div>
				</div> 
				<div class="form-group">
					<?php echo lang('ទីកន្លែង / ទីក្រុង៖', 'ទីកន្លែង / ទីក្រុង៖'); ?>​
					<span class="red">*</span>
					<div class="controls">
						<input type="text" class="form-control input-sm"  name="country"   id="country" />
					</div>
				</div>
				
				<div class="form-group">
					<?php echo lang('ចាប់ពី៖', 'ចាប់ពី៖'); ?>​
					<span class="red">*</span>
					<div class="controls">
						<input type="text" class="form-control input-sm date"  name="from"  id="from" />
					</div>
				</div> 
				<div class="form-group">
					<?php echo lang('ដល់៖', 'ដល់'); ?>​
					<span class="red">*</span>
					<div class="controls">
						<input type="text" class="form-control input-sm date "  name="to"  id="to" />
					</div>
				</div> 
				<div class="form-group">
					<?php echo lang('មុខងារ ៖', 'មុខងារ ៖'); ?>​
					<span class="red">*</span>
					<div class="controls">
						<input type="text" class="form-control input-sm "  name="job_title"  id="job_title" />
					</div>
				</div> 
			</div>​
			
			<div class="clearfix"></div>
			
		</div>
		
		 <div class="modal-footer">
			<?php echo form_submit('submit', lang('submit'), 'class="btn btn-primary save-data"'); ?>
		 </div>
		
		<?php  echo form_close(); ?>
	</div>
</div>

<script type="text/javascript">
	$(function(){
		$("select").select2();
	}); 
	$(".save-data").on('click',function(){
		var name_institution = $('#name_institution').val();
		var country = $('#country').val();
		var froms = $('#from').val();
		var to = $('#to').val();
		var qualification = $('#qualification').val();
		if(name_institution=='' || country=='' || froms=='' || to == '' || qualification == ''){ 
			bootbox.alert('<?= lang("please_select_all");?>');
			return false;
		}  
	}); 
</script>
