<?php  
	$icon_check='<span style="font-size:17px;">&#x2611;</span>';
	$icon_uncheck='<span style="font-size:17px;">&#x2610;</span>'; 
?> 
<div class="box a4" >
	<div class="box-content">
		<div class="row">			
			<?php $this->load->view($this->theme."applications/head-cover"); ?>
			
			<div class="col-sm-12 text-center">
				<h2 class="text-cover" style="margin-top:0px;"> ទម្រង់ដកពាក្យសុំសិទ្ធិជ្រកកោន</h2>
				<h3>WITHDRAWAL OF ASYLUM APPLICATION FORM </h3>
			</div>
			
			<div class="col-sm-12">  
				<div class="wrap-content">
					<table width="100%" style="white-space:nowrap;">						
						<tr>
							<td width="20px">-</td>
							<td>លេខករណី  /  Case Number :  <?= $this->erp->toKhmer($result->case_prefix.$result->case_no); ?></td>
							<td></td>
							<td rowspan="6" width="125" height="125" class='text-right cover-photo' style="vertical-align:top;" >
									<img src="<?= base_url("assets/uploads/".$withdrawal->photo) ?>" class="thumb"  >	 
							</td>
						</tr>
						<tr>
							<td>-</td>
							<td>នាមត្រកូល / Surname</td>
							<td>- &nbsp;&nbsp; នាមខ្លួន / Given Name</td>
						</tr>
						<tr>
							<td></td>
							<td>
								<?= $result->firstname_kh; ?> <?php if($result->firstname != "") echo " / " . $result->firstname; ?>
							</td>
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;
								<?= $result->lastname_kh; ?> <?php if($result->lastname != "") echo " / " . $result->lastname; ?>
							</td>
						</tr>
						<tr>							
							<td>-</td>
							<td> 
								ភេទ  :  
								&nbsp;&nbsp;&nbsp;
								<?php echo ($result->gender == 'male' ? $icon_check : $icon_uncheck);?>&nbsp;&nbsp;ប្រុស 
								&nbsp;&nbsp;&nbsp;
								<?php echo ($result->gender == 'female' ? $icon_check : $icon_uncheck);?>&nbsp;&nbsp;ស្រី 
							</td>
							<td>
								- &nbsp;&nbsp;  ថ្ងៃខែឆ្នាំកំណើត 
							</td>
						</tr>
						<tr>
							<td>-</td>
							<td>
								Sex :
								&nbsp;&nbsp;&nbsp; 
								<span style=''>Male</span>
								&nbsp;&nbsp;&nbsp;
								<span style=''>Female</span>
							</td>
							<td>
								- &nbsp;&nbsp; Date of Birth : <?= $this->erp->toKhmer($this->erp->hrsd($result->dob)) ?>
							</td>
						</tr>
						<tr>
							<td>-</td>
							<td colspan="2">ទីកន្លែងកំណើត / Place of Birth : <?= $result->pob_kh; ?></td>
						</tr>
						<tr>
							<td>-</td>
							<td>សញ្ជាតិ</td>
							<td>- &nbsp;&nbsp; ថ្ងៃខែឆ្នាំចុះឈ្មោះ</td>
						</tr>
						<tr> 
							<td>-</td>
							<td>Nationality : <?= $result->nationality_kh ?></td>
							<td>- &nbsp;&nbsp; Date of Registration : <?= $this->erp->toKhmer($this->erp->hrsd($result->registered_date)) ?></td>
						</tr>
					</table> 
					
					<?php if(!empty($members)) { ?>
					
						<table width="100%">
							<tr class="text-center">
								<td>សមាជិកគ្រួសាររួមដំណើរជាមួយ / Family members travel with.</td>
							</tr>
						</table>
						
					<?php } ?>
					
					<?php  if(!empty($members)) { ?>					
						<table width="80%" style='margin:0 auto;'>					
							<tr>
								<td width='10'>ល.រ</td>
								<td>ឈ្មោះ  / Name</td>
								<td>ភេទ  / Gender</td>
								<td>ថ្ងៃខែឆ្នាំកំណើត / Date of Birth</td>
							</tr>
							<?php foreach($members as $i => $member){ ?>
								<tr>
									<td width='10'><?= $this->erp->toKhmer($i+1) ?>.</td>
									<td><?= $member->lastname_kh ." ".$member->firstname_kh ?></td>
									<td><?= $this->erp->genderToKhmer($member->gender) ?></td>
									<td><?= $this->erp->toKhmer($this->erp->hrsd($member->dob)); ?></td>
								</tr>
							<?php }	?>
						</table>					
					<?php }	?>
					<br/>
					
					<table width="100%">
						<tr>
							<td width="20">-</td>
							<td>
								អាសយដ្ឋាននៅកម្ពុជា /   Address in Cambodia :  
								<?= $result->address_kh; ?>
								<?= $this->asylum_seekers->getTagsById($result->village); ?>
								<?= $this->asylum_seekers->getAllAddressKH($result->commune,"commune"); ?>  
								<?= $this->asylum_seekers->getAllAddressKH($result->district,"district"); ?> 
								<?= $this->asylum_seekers->getAllAddressKH($result->province,"province"); ?>
								<?= $this->asylum_seekers->getAllAddressKH($result->country,"country"); ?>
							</td> 
						</tr>
						<tr>
							<td width="20">-</td>
							<td>
								លេខទូរស័ព្ទទំនាក់ទំនង / Contact Number : <?= $this->erp->toKhmer($result->phone); ?>
							</td> 
						</tr>
						<tr>
							<td colspan="2" class="space-size">
								ខ្ញុំ​បាទ​/​នាងខ្ញុំ​សូមស្នើសុំនាយកដ្ឋានជនភៀសខ្លួន នៃ​អគ្គនាយកដ្ឋាន​អន្តោប្រវេសន៍ ដើម្បីដកពាក្យនិង​បពា្ឈប់​កិច្ចដំណើរ​ការសុំ​សិទ្ធិ​ជ្រកកោន​​​ដោយ​ហេតុ​ផលដូច​ខាង​ក្រោម /   
								I would like to request to the Refugee Department of General Department of Immigration for withdrawing my asylum application and to cancel the 
								 formality process according to the following reason:
							</td>
						</tr>
						<tr> 
							<td  colspan="2" class="border-dotted">
								<?= $withdrawal->reasons; ?>
							</td>
						</tr>
					</table>  
					 
					<div class='col-sm-12'>
						<div class='col-sm-6 tbl-left' >
							<div style="border:2px solid #999;">
								<table width='100%' class='text-center'>
									<tr>
										<td>
											<h4>សម្រាប់មន្រ្តី /  Officer Use Only</h4>
											<i>
											<?= $this->erp->footer_date_khmer("រាជធានីភ្នំពេញ");?>
											</i>
										</td>
									</tr>
								</table>
								<br/> 
								<br/>
								<br/>
								<br/>
							</div>
						</div>
						<div class='col-sm-6 tbl-right'>
							<table width='100%' class="text-center"  >
								<tr >
									<td >
										<?= $this->erp->footer_date_khmer("រាជធានីភ្នំពេញ");?>
										<h4>ស្នាមមេដៃអ្នកដកពាក្យ </h4>
										Applicant’s thumb print
									</td>
								</tr>
							</table>
						</div>
					</div> 
				​ 
				<div class='clearfix'></div> 
				<br/><br/> 
			</div> 
			</div>  
		</div>
	</div>
</div>

<style type="text/css"> 
	@media print{
		.bblack{ background: white !important; }
		.modal-content, .box, 
		.modal-header{ border:none !important; }
		.box-border{border:1.5px solid !important}
		.box-content{
			padding: 20px !important;
		}
	} 
	  
	.wrap-content table td{
		padding:5px;
	}
	.wrap-content{
		height:auto;		
		font-size:14px;
	}
	.checkbox-gender, 
	.checkbox-family {
		float:left;
		margin-left: 20px;
	}
	#image-cover{
		width:125px;
		height:125px;
	}
	.box-office{
		width:300px;
		border:1px solid #000;
		margin:5px;
		padding:5px;
	}
	hr{
		padding:0px;
		margin:0px;
		border-top:none !important;
		border-bottom:dotted 1px !important;
	}
	.border-bottom{
		border-bottom:1px solid #000;
	}
	.table-fill{
		margin:0 auto;
	}
	.table-fill td{
		padding:5px; 
	}
</style>