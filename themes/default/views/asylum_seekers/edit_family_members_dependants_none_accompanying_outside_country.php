<div class="modal-dialog  modal-lg">
	<div class="modal-content">
		<div class="modal-header">				
			<button type="button" class="close" data-dismiss="modal">
				<span aria-hidden="true"><i class="fa fa-2x">&times;</i></span>
				<span class="sr-only"><?=lang('close');?></span>
			</button>
			<h4 class="modal-title" id="payModalLabel"><?= lang('edit_family_members_dependants_none_accompanying_outside_country'); ?></h4>
		</div>
		
		<?php $attributes = array('id' => 'form1');?>
                  <?php 
                    $countries_ = array(lang("select"));
                    foreach($countries as $country){
                        $countries_[$country->id] = $country->country;
                    } 
                    ?>
                    <?php 
                    $provinces_ = array(lang("select"));
                    foreach($provinces as $province){
                        $provinces_[$province->id] = $province->name;
                    } 
                    ?>
                    <?php 
                    $districts_ = array(lang("select"));
                    foreach($districts as $district){
                        $districts_[$district->id] = $district->name;
                    } 
                    ?>
                    
                    <?php 
                    $states_ = array(lang("select"));
                    foreach($states as $state){
                        $states_[$state->id] = $state->name;
                    } 
                ?>
		<?php 
                $communes_ = array(lang("select"));
                foreach($communes as $commune){
                    $communes_[$commune->id] = $commune->name;
                } 
                ?>
		<?php  echo form_open_multipart("asylum_seekers/edit_family_members_dependants_none_accompanying_outside_country/".$counseling_id.'/'.$member_id, $attributes); ?> 
		
		<div class="modal-body"> 
			<div class="col-sm-6" >
				<div class="form-group">
					<?php echo lang('firstname', 'firstname'); ?>​
					<span class="red">*</span>
					<div class="controls">					
						<input type="text" value='<?= $result->firstname_kh?>' class="form-control input-sm" name="firstname_kh"  id="firstname_kh" />
					</div>
				</div>
			</div>
			
			<div class="col-sm-6" >
				<div class="form-group">
				<?php echo lang('firstname', 'firstname'); ?>​ (EN)
					<span class="red">*</span>
					<div class="controls">					
						<input type="text"  value='<?= $result->firstname?>' class="form-control input-sm" name="firstname"  id="firstname" />
					</div>
				</div>
			</div>
			
			<div class="col-sm-6" >
				<div class="form-group">
					<?php echo lang('lastname', 'lastname'); ?>​
					<span class="red">*</span>
					<div class="controls">					
						<input type="text"  value='<?= $result->lastname_kh?>' class="form-control input-sm" name="lastname_kh"  id="lastname_kh" />
					</div>
				</div>
			</div>
			
			<div class="col-sm-6" >
				<div class="form-group">
				<?php echo lang('lastname', 'lastname'); ?>​ (EN)
					<span class="red">*</span>
					<div class="controls">					
						<input type="text"  value='<?= $result->lastname?>' class="form-control input-sm" name="lastname"  id="lastname" />
					</div>
				</div>
			</div>
			<div class="col-sm-6">		
				<div class="form-group">
					<?php echo lang('ទីកន្លែងកំណើត', 'pob'); ?>​
					<span class="red">*</span>
					<div class="controls">
						<input type="text" value='<?= $result->pob_kh?>' class="form-control input-sm "   name="pob_kh" />
					</div>
				</div>
			</div>
			<div class="col-sm-6">		
				<div class="form-group">
					<?php echo lang('ទីកន្លែងកំណើត', 'pob'); ?>​ (EN)
					<span class="red">*</span>
					<div class="controls">
						<input type="text" value='<?= $result->pob?>' class="form-control input-sm " name="pob" />
					</div>
				</div>
			</div>		
			<div class="col-sm-6">
				<div class="form-group">
					<?php echo lang('gender', 'gender'); ?>​
					<span class="red">*</span>
					<div class="controls">
					<select class="form-control selectskip" name="gender">
							<option <?= ($result->gender=='male'?'selected':'')?> value="male" ><?= lang("male") ?></option>
							<option <?= ($result->gender=='female'?'selected':'')?> value="female" ><?= lang("female") ?></option>
						</select>					
					</div>
				</div>
			</div>
			<div class="col-sm-6">	
				<div class="form-group">
					<?php echo lang('dob', 'dob'); ?>​
					<span class="red">*</span>
					<div class="controls">
						<input type="text" value='<?= $this->erp->hrsd($result->dob)?>' class="form-control input-sm date"  name="dob" />
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
			<div class="col-sm-6">	
				<div class="form-group">
					<?php echo lang('nationality', 'nationality'); ?>​
					<span class="red">*</span>
					<div class="controls">
						<input type="text"  value='<?= $result->nationality_kh?>' class="form-control input-sm "  name="nationality_kh" id="nationality_kh" />
					</div>
				</div> 
			</div> 
                        <div class="col-sm-6">	
				<div class="form-group">
					<?php echo lang('nationality', 'nationality'); ?>​ (EN)
					<span class="red">*</span>
					<div class="controls">
						<input type="text"  value='<?= $result->nationality?>' class="form-control input-sm "  name="nationality" id="nationality" />
					</div>
				</div> 
			</div> 
			<div class="col-sm-6">	
				<div class="form-group">
					<?php echo lang('relationship', 'relationship'); ?>​
					<span class="red">*</span>
					<div class="controls">
					<div class="controls">
						<select class="form-control" name="relationship">
							​<?php foreach($relationship as $row){?>
								<option value="<?= $row->id ?>" <?= ($result->relationship==$row->id?'selected':'')?>><?= $row->relationship_kh ?></option>  
							<?php }?> 
						</select>
					</div>
					</div>
				</div> 
			</div> 
			<div class="col-sm-6">	
				<div class="form-group">
					<?php echo lang('ethnicity', 'ethnicity'); ?>​
					<span class="red">*</span>
					<div class="controls">
						<input type="text"  value='<?= $result->ethnicity?>' class="form-control input-sm "  name="ethnicity"   id="ethnicity" />
					</div>
				</div>
			</div>
			<div class="col-sm-12">
				<div class="form-group">
					<?php echo lang('ឋានៈនៅទីនោះ', 'ឋានៈនៅទីនោះ'); ?>​
					<span class="red">*</span>
					<div class="controls">
						<input type="text"  value='<?= $result->status?>' class="form-control input-sm "  name="status"   id="status" />
					</div>
				</div>
			</div>
                        <div class="col-sm-12">
                                        <div class="panel panel-warning">
                                            <div class="panel-heading"><?php echo lang("អាសយដ្ឋានបច្ចុប្បន្ន","អាសយដ្ឋានបច្ចុប្បន្ន") ?> / <?= lang("Current address") ?></div>
                                            <div class="panel-body" style="padding: 5px;">
                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <?php echo lang('country', 'country'); ?>
                                                        <div class="controls">
                                                            <?php echo form_dropdown('country', $countries_, $result->country, ' class="form-control" '); ?>												
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <?php echo lang('province', 'province'); ?>
                                                        <div class="controls">
                                                            <?php echo form_dropdown('province', $provinces_, $result->province, ' class="form-control" '); ?>												
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <?php echo lang('district', 'district'); ?>
                                                        <div class="controls">
                                                            <?php echo form_dropdown('district', $districts_, $result->district, ' class="form-control" '); ?>												
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <?php echo lang('commune', 'commune'); ?>
                                                        <div class="controls">
                                                            <?php echo form_dropdown('commune', $communes_, $result->commune, ' class="form-control" '); ?>												
                                                        </div>
                                                    </div>
                                                </div>
                                               <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <?php echo lang('village', 'village'); ?>
                                                        <div class="controls">
                                                            <?php 
                                                            echo form_input('village', $this->asylum_seekers->getTagsById($result->village),'class="form-control village" '); ?>											
                                                        </div>
                                                    </div>
                                                </div>   
                                                <div class="col-sm-6 text-nowrap">
                                                        <div class="form-group">
                                                            <?php echo lang('other', 'other'); ?> (<?php echo lang("KH") ?>)
                                                            <span class="red">*</span>
                                                            <div class="controls">
                                                                <input type="text" value="<?= set_value('address_kh',$result->address_kh) ?>" name="address_kh" class="form-control input-sm" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <?php echo lang('other', 'other'); ?> (<?php echo lang("EN") ?>)
                                                            <div class="controls">
                                                                <input type="text" value="<?= set_value('address',$result->address) ?>" name="address" class="form-control input-sm" />
                                                            </div>
                                                        </div>
                                                    </div>                                        
                                         </div>
                                </div>
                        </div>
			<div class="col-sm-12">
				<div class="form-group">
					<?php echo lang('photo','photo');?>
					<span class="red">*</span>
					<input type="file" name="member_photo" />
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
		 <div class="modal-footer">
			<?php echo form_submit('submit', lang('submit'), 'class="btn btn-primary save-data"'); ?>
		 </div>
		
		<?php  echo form_close(); ?>
	</div>
</div>
<style type="text/css">
	ul.ui-autocomplete {
		z-index: 1100;
	}
</style>
<script type="text/javascript">
	$(function(){
		$("select").select2();
		getAllAutoComplete(".village","/getAllTags?v=village");
		function getAllAutoComplete(element, base_url){
			$(element).autocomplete({
				source: function (request, response) {
					var q = request.term;
					$.ajax({
						type: 'GET',
						url: '<?= site_url('asylum_seekers/'); ?>'+base_url,
						dataType: "json",
						data: {
							q : q
						},
						success: function (data) {
							response(data);
						}
					});
				},
				minLength: 1,
				autoFocus: false,
				delay: 200,
				response: function (event, ui) {
					if (ui.content[0].id == 0) {
						$(this).removeClass('ui-autocomplete-loading');
						$(this).removeClass('ui-autocomplete-loading');
					}
				},
				select: function (event, ui) {
					event.preventDefault();
					$(this).val(ui.item.label);
				}
			});
		}
		
	});    
	
</script>

