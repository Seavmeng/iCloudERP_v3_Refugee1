
		<ul id="myTab" class="nav nav-tabs">
			<li class="">
				<a href="#content_1" class="tab-grey">
					<?= lang("counseling_application_form"); ?> <i class="fa fa-chevron-right" aria-hidden="true"></i>
				</a>
			</li>
			<li class="">
				<a href="#content_2" class="tab-grey">
					<?= lang("written_claim_for_counseling"); ?> <i class="fa fa-chevron-right" aria-hidden="true"></i>
				</a>
			</li>
		</ul>

		<?= form_open_multipart("asylum_seekers/edit_counseling_application/".$result->id); ?>

		<div class="tab-content">	

			<div id="content_1" class="tab-pane fade in">
			
			<div class="box">
				<div class="box-content">
					<div class="row">		 
						<div class="col-sm-5" style="padding-right:0px;">
							
							<div class="col-sm-6">
								<div class="form-group">
									<?php echo lang('នាមខ្លួន​ / ​Given Name', 'នាមខ្លួន​ / ​Given Name'); ?>
									<span class="red">*</span>
									<div class="controls">
										<input type="text" value="<?= set_value('firstname_kh', $result->firstname_kh) ?>" name="firstname_kh" class="form-control input-sm" />								
									</div>
								</div>
							</div>
							
							<div class="col-sm-6">
								<div class="form-group">
									<?php echo lang('First Name','&nbsp;', '&nbsp;'); ?>​ 
									<span class="red">*</span>
									<div class="controls">
										<input type="text" value="<?= set_value('firstname', $result->firstname) ?>" name="firstname" class="form-control input-sm" />								
									</div>
								</div>
							</div>
								
							<div class="col-sm-6">
								<div class="form-group">
									<?php echo lang('នាមត្រកូល / ​ Surname', 'នាមត្រកូល / ​ Surname'); ?>
									<span class="red">*</span>
									<div class="controls">
										<input type="text" value="<?= set_value('lastname_kh', $result->lastname_kh) ?>" name="lastname_kh" class="form-control input-sm" />
									</div>
								</div>
							</div>
							
							<div class="col-sm-6">
								<div class="form-group">
									<?php echo lang('Last Name','&nbsp;', '&nbsp;'); ?>
									<span class="red">*</span>
									<div class="controls">
										<input type="text" value="<?= set_value('lastname', $result->lastname) ?>" name="lastname" class="form-control input-sm" />
									</div>
								</div>
							</div>
							
							<div class="col-sm-6">
								<div class="form-group">
									<?php echo lang('ឈ្មោះហៅក្រៅ / Nickname', 'ឈ្មោះហៅក្រៅ / Nickname'); ?>
									<span class="red">*</span>
									<div class="controls">
										<input type="text" name="nickname_kh" class="form-control input-sm" value="<?= set_value('nickname_kh', $result->nickname_kh) ?>" / >
									</div>
								</div>
							</div>
								
							<div class="col-sm-6">
								<div class="form-group">
									<label>&nbsp;</label>
									<span class="red">*</span>
									<div class="controls">
										<input type="text" name="nickname" class="form-control input-sm" value="<?= set_value('nickname', $result->nickname) ?>" / >
									</div>
								</div>
							</div>
							
							<div class="col-sm-6">
								<div class="form-group">
									<?php echo lang('ថ្ងៃខែឆ្នាំកំណើត / Date of Birth', 'ថ្ងៃខែឆ្នាំកំណើត / Date of Birth'); ?>
									<span class="red">*</span>
									<div class="controls">
										<input type="text" value="<?= set_value('dob',$this->erp->hrsd($result->dob)) ?>" name="dob" class="form-control input-sm date" />
									</div>
								</div>
							</div>
							
							
							<div class="col-sm-6">
								<div class="form-group">
									<?php echo lang('ភេទ /​ Sex', 'ភេទ /​ Sex'); ?>
									<span class="red">*</span>
									<div class="controls">
										<label class="radio-inline">
											<input type="radio" value="male" <?= ($result->gender=="male"?"checked":""); ?> name="gender"><?= lang('male'); ?>
										</label>
										
										<label class="radio-inline">
											<input type="radio" value="female" <?= ($result->gender=="female"?"checked":""); ?> name="gender"><?= lang('female'); ?>
										</label>
									</div>
								</div>
							</div>
							
							<div class="clearfix"></div>
							
							<div class="col-sm-6">
								<div class="form-group">
									<?php echo lang('សញ្ជាតិ / Nationality', 'សញ្ជាតិ / Nationality'); ?>
									<span class="red">*</span>
									<div class="controls">
										<input type="text" value="<?= set_value('nationality_kh', $result->nationality_kh) ?>" name="nationality_kh" class="form-control input-sm" />
									</div>
								</div>
							</div>
							
							<div class="col-sm-6">
								<div class="form-group">
									<?php echo lang('&nbsp;', '&nbsp;'); ?>
									<div class="controls">
										<input type="text" value="<?= set_value('nationality', $result->nationality) ?>" name="nationality" class="form-control input-sm" />
									</div>
								</div>
							</div>
							
							
							<div class="col-sm-6">
								<div class="form-group">
									<?php echo lang('សាសនា / Religion', 'សាសនា / Religion'); ?>
									<span class="red">*</span>
									<div class="controls">
										<input type="text" value="<?= set_value('religion_kh', $result->religion_kh) ?>" name="religion_kh" class="form-control input-sm" />
									</div>
								</div>
							</div>
							
							<div class="col-sm-6">
								<div class="form-group">
									<?php echo lang('&nbsp;', '&nbsp;'); ?>
									<div class="controls">
										<input type="text" value="<?= set_value('religion', $result->religion) ?>" name="religion" class="form-control input-sm" />
									</div>
								</div>
							</div>
							
							<div class="col-sm-6">
								<div class="form-group">
									<?php echo lang('ជាតិពន្ធុ / Ethnicity', 'ជាតិពន្ធុ / Ethnicity'); ?>
									<span class="red">*</span>
									<div class="controls">
										<input type="text" value="<?= set_value('ethnicity_kh', $result->ethnicity_kh) ?>" name="ethnicity_kh" class="form-control input-sm" />
									</div>
								</div>
							</div>
							
							<div class="col-sm-6">
								<div class="form-group">
									<?php echo lang('&nbsp;', '&nbsp;'); ?>
									<div class="controls">
										<input type="text" value="<?= set_value('ethnicity', $result->ethnicity) ?>" name="ethnicity" class="form-control input-sm" />
									</div>
								</div>
							</div>
							
							<div class="col-sm-6">
								<div class="form-group"> 
									<?php echo lang("ទីកន្លែងកំណើត / Place of Birth","ទីកន្លែងកំណើត / Place of Birth") ?>
									<span class="red">*</span>
									<div class="controls">
										<input type="text" value="<?= set_value('pob_kh', $result->pob_kh) ?>" name="pob_kh" class="form-control input-sm" />
									</div>
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group">
									<?php echo lang('&nbsp;', '&nbsp;'); ?>
									<div class="controls">
										<input type="text" value="<?= set_value('pob', $result->pob) ?>" name="pob" class="form-control input-sm" />
									</div>
								</div>
							</div>
							
							<?php 
							$countries_ = array(lang("select"));
							foreach($countries as $country){
								$countries_[$country->id] = $country->country;
							} 
							?>
							<?php 
							$provinces_ = array(lang("select"));
							foreach($provinces as $province){
								$provinces_[$province->id] = $province->name;
							} 
							?>
							<?php 
							$districts_ = array(lang("select"));
							foreach($districts as $district){
								$districts_[$district->id] = $district->name;
							} 
							?>
							
							<?php 
							$states_ = array(lang("select"));
							foreach($states as $state){
								$states_[$state->id] = $state->name;
							} 
							?>
							
							<?php 
							$communes_ = array(lang("select"));
							foreach($communes as $commune){
								$communes_[$commune->id] = $commune->name;
							} 
							
							?>
							
							<div class="col-sm-12">
								<div class="panel panel-warning">
									<div class="panel-heading"><?php echo lang("អាសយដ្ឋានបច្ចុប្បន្ន","អាសយដ្ឋានបច្ចុប្បន្ន") ?> / <?= lang("Current address") ?></div>
									<div class="panel-body" style="padding: 5px;">
										
										
										<div class="col-lg-12">
											<div class="form-group">
												<?php echo lang('country', 'country'); ?>
												<div class="controls">
													<?php echo form_dropdown('country', $countries_, $result->country, ' class="form-control" '); ?>												
												</div>
											</div>
										</div>
										
										<div class="col-lg-6">
											<div class="form-group">
												<?php echo lang('province', 'province'); ?>
												<div class="controls">
													<?php echo form_dropdown('province', $provinces_, $result->province, ' class="form-control" '); ?>												
												</div>
											</div>
										</div>
										
										<div class="col-lg-6">
											<div class="form-group">
												<?php echo lang('district', 'district'); ?>
												<div class="controls">
													<?php echo form_dropdown('district', $districts_, $result->district, ' class="form-control" '); ?>												
												</div>
											</div>
										</div>
										
										<div class="col-lg-6">
											<div class="form-group">
												<?php echo lang('commune', 'commune'); ?>
												<div class="controls">
													<?php echo form_dropdown('commune', $communes_, $result->commune, ' class="form-control" '); ?>												
												</div>
											</div>
										</div>
										
										<div class="col-lg-6">
											<div class="form-group">
												<?php echo lang('village', 'village'); ?>
												<div class="controls">
													<?php 
													echo form_input('village', $this->asylum_seekers->getTagsById($result->village),'class="form-control village" '); ?>											
												</div>
											</div>
										</div>
										
										<div class="col-sm-12 text-nowrap">
											<div class="form-group">
												<?php echo lang('other', 'other'); ?> (<?php echo lang("KH") ?>)
												<span class="red">*</span>
												<div class="controls">
													<input type="text" value="<?= set_value('address_kh',$result->address_kh) ?>" name="address_kh" class="form-control input-sm" />
												</div>
											</div>
										</div>
										
										<div class="col-sm-12">
											<div class="form-group">
												<?php echo lang('other', 'other'); ?> (<?php echo lang("EN") ?>)
												<div class="controls">
													<input type="text" value="<?= set_value('address',$result->address) ?>" name="address" class="form-control input-sm" />
												</div>
											</div>
										</div>
							
									</div>
								</div>
							</div>
							
						</div>
						
						<div class="col-sm-5" style="padding-right:0px;">
								<div class="col-sm-6" style="white-space:nowrap;">
									<div class="form-group">
										<?php echo lang('ឯកសារធ្វើដំណើរ / Travel documents', 'ឯកសារធ្វើដំណើរ / Travel documents'); ?>
										<span class="red">*</span>
										<div class="controls">
											<input type="text" value="<?= set_value('travel_documents_kh',$result->travel_documents_kh) ?>" name="travel_documents_kh" class="form-control input-sm" />
										</div>
									</div>
								</div>
								
								<div class="col-sm-6">
									<div class="form-group">
										<?php echo lang('&nbsp;', '&nbsp;'); ?>
										<div class="controls">
											<input type="text" value="<?= set_value('travel_documents',$result->travel_documents) ?>" name="travel_documents" class="form-control input-sm" />
										</div>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<?php echo lang('កន្លែងផ្តល់ / Issued place', 'កន្លែងផ្តល់ / Issued place'); ?>
										<span class="red">*</span>
										<div class="controls">
											<input type="text" value="<?= set_value('place_of_issued_kh',$result->place_of_issued_kh) ?>" name="place_of_issued_kh" class="form-control input-sm" />
										</div>
									</div>
								</div>
								
								<div class="col-sm-6">
									<div class="form-group">
										<?php echo lang('&nbsp;', '&nbsp;'); ?>
										<div class="controls">
											<input type="text" value="<?= set_value('place_of_issued',$result->place_of_issued) ?>" name="place_of_issued" class="form-control input-sm" />
										</div>
									</div>
								</div>
								
								<div class="col-sm-6">
									<div class="form-group">
										<?php echo lang('ផ្តល់នៅថ្ងៃទី / Issued date', 'ផ្តល់នៅថ្ងៃទី / Issued date'); ?>
										<span class="red">*</span>
										<div class="controls">
											<input type="text" value="<?= set_value('issued_date',$this->erp->hrsd($result->issued_date)) ?>" name="issued_date" class="form-control input-sm date" />
										</div>
									</div>
								</div>
								
								<div class="col-sm-6">
									<div class="form-group">
										<?php echo lang('ផុតកំណត់នៅថ្ងៃទី / Expired date', 'ផុតកំណត់នៅថ្ងៃទី / Expired date'); ?>
										<span class="red">*</span>
										<div class="controls">
											<input type="text" value="<?= set_value('expired_date',$this->erp->hrsd($result->expired_date)) ?>" name="expired_date" class="form-control input-sm date" />
										</div>
									</div>
								</div>
								
								
								<div class="col-sm-12">
									<div class="form-group">
										<?php echo lang('មធ្យោបាយធ្វើដំណើរ / Means of transportation', 'មធ្យោបាយធ្វើដំណើរ / Means of transportation'); ?>
										<span class="red">*</span>
										<div class="controls">
										
											<label class="radio-inline">
												<input type="radio" value="sea" <?= ($result->means_of_transportations=="sea"?"checked":""); ?> name="means_of_transportations"><?= lang("sea"); ?>
											</label>
											<label class="radio-inline">
												<input type="radio" value="land" <?= ($result->means_of_transportations=="land"?"checked":""); ?> name="means_of_transportations"><?= lang("land"); ?>
											</label>
											
											<label class="radio-inline">
												<input type="radio" value="air" <?= ($result->means_of_transportations=="air"?"checked":""); ?> name="means_of_transportations"><?= lang("air"); ?>
											</label>
											
											<label class="radio-inline">
												<input type="radio" value="other" <?= ($result->means_of_transportations=="other"?"checked":""); ?> name="means_of_transportations"><?= lang("other"); ?>
											</label>
											
										</div>
									</div>
								</div>
								
								
								<div class="col-sm-6 text-nowrap">
									<div class="form-group">
										<?php echo lang('ថ្ងៃមកដល់លើកទីមួយ / First arrival date', 'ថ្ងៃមកដល់លើកទីមួយ / First arrival date'); ?>
										<span class="red">*</span>
										<div class="controls">
											<input type="text" value="<?= set_value('first_arrived_date',$this->erp->hrsd($result->first_arrived_date)) ?>" name="first_arrived_date" class="form-control input-sm date" />
										</div>
									</div>
								</div>
								
								<div class="col-sm-6 text-nowrap">
									<div class="form-group">
										<?php echo lang('ថ្ងៃចូលចុងក្រោយ / Last date of entry', 'ថ្ងៃចូលចុងក្រោយ / Last date of entry'); ?>
										<span class="red">*</span>
										<div class="controls">
											<input type="text" value="<?= set_value('last_entry_date',$this->erp->hrsd($result->last_entry_date)) ?>" name="last_entry_date" class="form-control input-sm date" />
										</div>
									</div>
								</div>
								
								<div class="col-sm-6">
									<div class="form-group">							
										<?php echo lang('ចូលតាមច្រក / Port of entry', 'ចូលតាមច្រក / Port of entry'); ?>
										<span class="red">*</span>
										<div class="controls">
											<input type="text"​ value="<?= set_value('port_of_entry_kh',$result->port_of_entry_kh) ?>" name="port_of_entry_kh" class="form-control input-sm" />
										</div>
									</div>
								</div>
								
								<div class="col-sm-6">
									<div class="form-group">							
										<?php echo lang('&nbsp;', '&nbsp;'); ?>
										<div class="controls">
											<input type="text"​ value="<?= set_value('port_of_entry',$result->port_of_entry) ?>" name="port_of_entry" class="form-control input-sm" />
										</div>
									</div>
								</div>
								
								<div class="col-sm-6" style="white-space:nowrap;">
									<div class="form-group">
										<?php echo lang('គោលបំណង / Purpose', 'គោលបំណង / Purpose'); ?>
										<span class="red">*</span>
										<div class="controls">
											<input type="text" value="<?= set_value('purpose_of_entry_kh',$result->purpose_of_entry_kh) ?>" name="purpose_of_entry_kh" class="form-control input-sm" />
										</div>
									</div>
								</div>
								
								<div class="col-sm-6">
									<div class="form-group">
										<?php echo lang('&nbsp;', '&nbsp;'); ?>
										<div class="controls">
											<input type="text" value="<?= set_value('purpose_of_entry',$result->purpose_of_entry) ?>" name="purpose_of_entry" class="form-control input-sm" />
										</div>
									</div>
								</div>
								
								<div class="col-sm-6">
									<div class="form-group">
										<?= lang("ទូរស័ព្ទទំនាក់ទំនង / Phone number", "ទូរស័ព្ទទំនាក់ទំនង / Phone number"); ?>
										<span class="red">*</span>
										<div class="controls">
											<input type="text" value="<?= set_value('phone',$result->phone) ?>" name="phone" class="form-control input-sm input-medium bfh-phone" data-format="+855 (ddd) ddd-dddd" />
										</div>
									</div>
								</div>
								
								<div class="col-sm-12">
									<div class="panel panel-warning">
										<div class="panel-heading"><?php echo lang("អាសយដ្ឋាននៅប្រទេសដើម / Address of original country","អាសយដ្ឋាននៅប្រទេសដើម / Address of original country") ?></div>
										<div class="panel-body" style="padding: 5px;">
											
											<div class="col-lg-6">
												<div class="form-group">
													<?php echo lang('country', 'country'); ?>
													<div class="controls">
														<?php echo form_dropdown('country_o', $countries_, $result->country_o, ' class="form-control" '); ?>												
													</div>
												</div>
											</div>
											
											<div class="col-lg-6">
												<div class="form-group">
													<?php echo lang('state', 'state'); ?>
													<div class="controls">
														<?php echo form_dropdown('state_o', $states_,$result->state_o, ' class="form-control" '); ?>												
													</div>
												</div>
											</div>
											
											<div class="col-lg-6">
												<div class="form-group">
													<?php echo lang('province', 'province'); ?>
													<div class="controls">
														<?php echo form_dropdown('province_o', $provinces_, $result->province_o, ' class="form-control" '); ?>												
													</div>
												</div>
											</div>
											
											<div class="col-lg-6">
												<div class="form-group">
													<?php echo lang('district', 'district'); ?>
													<div class="controls">
														<?php 
														// echo form_input('district_o', $this->asylum_seekers->getTagsById($result->district_o), 'class="form-control district_o" ');
														echo form_dropdown('district_o', $districts_, $result->district_o, ' class="form-control" '); ?>												
													</div>
												</div>
											</div>
											
											<div class="col-lg-6">
												<div class="form-group">
													<?php echo lang('commune', 'commune'); ?>
													<div class="controls">
														<?php 
														// echo form_input('commune_o', $this->asylum_seekers->getTagsById($result->commune_o),'class="form-control commune_o" ');
														echo form_dropdown('commune_o', $communes_, $result->commune_o, ' class="form-control" '); ?>												
													</div>
												</div>
											</div>

											<div class="col-lg-6">
												<div class="form-group">
													<?php echo lang('village', 'village'); ?>
													<div class="controls">
														<?php 
														echo form_input('village_o', $this->asylum_seekers->getTagsById($result->village_o),'class="form-control village_o" ');
														//echo form_dropdown('commune_o', $communes_, 0, ' class="form-control" '); ?>												
													</div>
												</div>
											</div>
											
											<div class="col-lg-12">
												<div class="form-group">
													<?php echo lang('other', 'other'); ?> (<?php echo lang("KH") ?>)
													<div class="controls">
														<input type="text" value="<?= set_value('address_original_kh', $result->original_address_kh) ?>" name="address_original_kh" class="form-control input-sm" />
													</div>
												</div>
											</div>
											
											<div class="col-lg-12">
												<div class="form-group">
													<?php echo lang('other', 'other'); ?> (<?php echo lang("EN") ?>)
													<div class="controls">
														<input type="text" value="<?= set_value('address_original',$result->original_address) ?>" name="address_original" class="form-control input-sm" />
													</div>
												</div>
											</div>
										</div>
									</div>
								</div> 
						</div> 
						<div class="col-sm-2">
						<div class="col-md-12" style="padding-left:0px; padding-right:0px;">
							<div class="form-group">
								<?php echo lang('counseling_no', 'counseling_no'); ?>
								<span class="red">*</span>
								<div class="controls">
									<input type="text" readonly="readonly" name="counseling_no" class="form-control input-sm" value="<?= $result->counseling_no ?>" / >
								</div>
							</div>
							<?= lang("photo", "photo"); ?>
							<?php
								$photo = "no_image.png";
								if(!empty($result->photo) || !file_exists("assets/uploads/".$result->photo)){
									$photo = $result->photo;
								}
							?>
							<div class="form-group">
							  <div class="main-img-preview">
								<img class="thumbnail img-preview" src="<?= site_url("assets/uploads/".$photo); ?>" title="Preview Logo">
							  </div>
							  <div class="input-group">
								<input id="fakeUploadLogo" class="form-control input-sm fake-shadow" placeholder="<?= lang("choose_file") ?>" disabled="disabled">
								<div class="input-group-btn">
								  <div class="fileUpload btn btn-danger fake-shadow">
									<span><i class="fa fa-upload"></i>&nbsp;&nbsp;<?= lang("upload") ?></span>
									<input id="logo-id" accept=".gif,.jpg,.png" data-maxfile="1024" name="userfile" type="file" class="attachment_upload">
								  </div>
								</div>
							  </div>
							</div>
						</div>
						<div class="form-group">
							<div class="controls">
								<a href="#" style="width:100%" class="btn btn-default" id="take-photo"><?= lang("take") ?></a>
							</div>
						</div>
					</div>
					
						<div class="col-sm-10">
						
							<div class="col-sm-12" style="padding-right:0px;">
							
								<div class="form-group">
									<?php echo lang('family_members_travel', 'family_members_travel'); ?> 
								</div>
								
								<table width="50%" class="table table-condensed table-bordered table-hover table-striped">
									<thead>
										<tr>
											<th class="text-center">
												<a class="btn btn-danger btn-xs" href="<?php echo site_url('asylum_seekers/add_member/'.$result->id) ?>" data-backdrop="static" data-keyboard="false"  data-toggle="modal" data-target="#myModal">
													<i class="fa fa-plus"></i>
												</a>
											</th>
											<th><?= lang("member") ?></th>
											<th><?= lang("gender") ?></th>
											<th><?= lang("dob") ?></th>
											<th><?= lang("occupation") ?></th>
											<th><?= lang("education") ?></th>
											<th><?= lang("relationship") ?></th>
										</tr>
									</thead>
									
									<tbody class="tbody">
										<?php foreach($result_members as $member){ ?>
											<tr>
												<td class="text-center">
													<a href="<?= site_url("asylum_seekers/delete_member/".$member->id); ?>" onclick="return confirm('<?php echo lang("r_u_sure");?>')" class='btn btn-danger btn-xs remove'>
														<i class='fa fa-trash'></i>
													</a>
													<a href="<?= site_url("asylum_seekers/edit_member/".$member->id."/".$result->id); ?>" class='btn btn-success btn-xs' data-backdrop="static" data-keyboard="false"  data-toggle="modal" data-target="#myModal">
														<i class='fa fa-edit'></i>
													</a>
												</td>
												<td class="text-center">
													<?= $member->lastname_kh.' '.$member->firstname_kh ?> / 
													<?= $member->lastname.' '.$member->firstname ?>
												</td>
												<td class="text-center"><?= lang($member->gender) ?></td>
												<td class="text-center"><?= $this->erp->toKhmer($this->erp->hrsd($member->dob)) ?></td>
												<td class="text-center"><?= $member->occupation_kh ?></td>
												<td class="text-center"><?= $member->education_kh ?></td>
												<td class="text-center"><?= lang($member->relationship) ?></td>
											</tr>
										<?php } ?>
									</tbody>
									
								</table>
							</div>
							
							<div class="col-sm-12">
								<div class="form-group">
									<button class="btn btn-danger right"><?= lang("reset"); ?></button>
									<button class="btn btn-success"><?= lang("submit"); ?></button>
								</div>
							</div>
							
						</div>

					</div>
					
				</div>
				
			</div>
			
		</div>
			
			<div id="content_2" class="tab-pane fade in">
				<div class="box">
					<div class="box-content">
						<div class="row">
							<div class="col-sm-9">
								<div class="col-sm-4">
									<div class="form-group">
										<?php echo lang('sample_document', 'sample_document'); ?>​ 
										<span class="red">*</span>
										<div class="controls">
											<a class="btn btn-primary form-control" href="<?= site_url("assets/downloads/F1_1_Claim_for_Registration.pdf") ?>" name="title" class="form-control input-sm" /><?= lang("download"); ?></a>								
										</div>
									</div>
									
									<div class="form-group">
										<?php echo lang('document', 'document'); ?>​ 
										<span class="red">*</span>
										<div class="controls">
											<input accept=".pdf,.doc" type="file"  name="document" data-browse-label="<?= lang('document'); ?>"
												   data-show-upload="false" data-show-preview="false" accept="image/*"
												   class="form-control file" />								
										</div>
									</div>
									
									<?php if($result->attachment_file){ ?>
									<div class="form-group">
										<?php echo lang('document', 'document'); ?>​ 
										<div class="controls">
											<a class="btn btn-danger form-control" target="_blank" href="<?= base_url("assets/uploads/".$result->attachment_file) ?>"><?= lang("document"); ?></a>
										</div>
									</div>
									<?php } ?>
									
								</div>							
							</div>
						</div>				
					</div>
				</div>
			</div>
		</div>

		<?= form_close(); ?>

			<div class="modal fade in" id="take-photo-modal" tabindex="-1" role="dialog" aria-labelledby="payModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
						</button>
						<h4 class="modal-title" id="myModalLabel"><?php echo lang('webcam'); ?></h4>
					</div>
					<div class="modal-body">
						<div class="col-sm-8">
							<div id="webcam"></div>
						</div>
						<div class="col-sm-4">
							<div id="webcam-preview"></div>
						</div>
						
					   <div class="clearfix"></div>
					</div>
					<div class="modal-footer">
					   <input type="button" class="btn btn-default" value="<?= lang("upload") ?>" id="upload" />
					   <input type="button" class="btn btn-danger" value="<?= lang("take_snapshot") ?>" onClick="take_snapshot()" />
					</div>
				</div>
			</div>
		</div>

		<script type="text/javascript" src="<?= $assets ?>styles/helpers/components/js/bootstrap-formhelpers.js"></script>	
		<link href="<?= $assets ?>styles/helpers/components/js/bootstrap-formhelpers.min.css" rel="stylesheet" />
		<style type="text/css">
			.form-control input-sm, .thumbnail {
				border-radius: 2px;
			}
			.btn-danger {
				background-color: #B73333;
			}
			/* File Upload */
			.fake-shadow {
				box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
			}
			.fileUpload {
				position: relative;
				overflow: hidden;
			}
			.fileUpload #logo-id {
				position: absolute;
				top: 0;
				right: 0;
				margin: 0;
				padding: 0;
				font-size: 33px;
				cursor: pointer;
				opacity: 0;
				filter: alpha(opacity=0);
			}
			.img-preview {
				max-width: 100%;
			}
		</style>
		<script type="text/javascript">
			$(document).ready(function() {
				var brand = document.getElementById('logo-id');
				brand.className = 'attachment_upload';
				brand.onchange = function() {
					document.getElementById('fakeUploadLogo').value = this.value.substring(12);
				};
				// Source: http://stackoverflow.com/a/4459419/6396981
				function readURL(input) {
					if (input.files && input.files[0]) {
						var reader = new FileReader(); 
						reader.onload = function(e) {
							$('.img-preview').attr('src', e.target.result);
						};
						reader.readAsDataURL(input.files[0]);
					}
				}
				$("#logo-id").change(function() {
					readURL(this);
				});
				
				$("#upload").click(function(){
					var filename = "<?= site_url()?>/" + $("#webcam-preview img").attr("src");
					$('.img-preview').attr('src', filename);
					return false;
				});
				
				getAllAutoComplete(".commune_o","/getAllTags?v=commune");
				getAllAutoComplete(".district_o","/getAllTags?v=district");
				getAllAutoComplete(".village_o, .village","/getAllTags?v=village");
				
				function getAllAutoComplete(element, base_url){
					$(element).autocomplete({
						source: function (request, response) {
							var q = request.term;
							$.ajax({
								type: 'GET',
								url: '<?= site_url('asylum_seekers/'); ?>'+base_url,
								dataType: "json",
								data: {
									q : q
								},
								success: function (data) {
									response(data);
								}
							});
						},
						minLength: 1,
						autoFocus: false,
						delay: 200,
						response: function (event, ui) {
							if (ui.content[0].id == 0) {
								$(this).removeClass('ui-autocomplete-loading');
								$(this).removeClass('ui-autocomplete-loading');
							}
						},
						select: function (event, ui) {
							event.preventDefault();
							$(this).val(ui.item.label);
						}
					});
				}
				
			});
		</script>
		<script type="text/javascript">
			$(function(){
				
				$("#take-photo").click(function(){
					$('#take-photo-modal').appendTo("body").modal('show');			
					return false;
				});
			})
		</script>

		<!---------///////////////----------->

		<script type="text/javascript" src="<?= $assets ?>webcam/webcam.js"></script>
		<script language="javascript">
			Webcam.set({
				width: 560,
				height: 460,
				image_format: 'jpeg',
				jpeg_quality: 90
			});
			Webcam.attach('#webcam');
		</script>
		<script language="javascript">
			function take_snapshot() {
				Webcam.snap(function(data_uri) {
					document.getElementById('webcam-preview').innerHTML = '<h2>Processing:</h2>';
					var url = "<?= site_url("asylum_seekers/capture") ?>";
					Webcam.upload(data_uri, url , function(code, text) {
						document.getElementById('webcam-preview').innerHTML = '<img width="250" src="'+text+'" />';
					});	
				} );
			}
		</script>
		<style type="text/css">
			#webcam video{
				border:5px solid #999;
				background:#000;
			}
			#webcam-preview img{
				width: 259px;
				min-height:200px;
				border:5px solid #999;
				background:#000;
			}
		</style>

		<!---------///////////////----------->



