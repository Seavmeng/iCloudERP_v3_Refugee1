
	<ul id="myTab" class="nav nav-tabs">
		<li class="">
			<a href="#content_2" class="tab-grey">
				<?= lang("part_a"); ?>
				<i class="fa fa-chevron-right" aria-hidden="true"></i>
			</a>
		</li>
		
		<li class="disabled">
			<a href="#" class="tab-grey">
				<?= lang("part_b"); ?>
				<i class="fa fa-chevron-right" aria-hidden="true"></i>
			</a>
		</li>
		
		<li class="disabled">
			<a href="#" class="tab-grey">
				<?= lang("part_c"); ?>
				<i class="fa fa-chevron-right" aria-hidden="true"></i>
			</a>
		</li> 
		<li class="disabled">
			<a href="#" class="tab-grey">
				<?= lang("part_d"); ?>
				<i class="fa fa-chevron-right" aria-hidden="true"></i>
			</a>
		</li>
		
		<li class="disabled">
			<a href="#" class="tab-grey">
				<?= lang("part_e"); ?>
				<i class="fa fa-chevron-right" aria-hidden="true"></i>
			</a>
		</li>
		
		<li class="disabled">
			<a href="#" class="tab-grey">
				<?= lang("part_f"); ?>
				<i class="fa fa-chevron-right" aria-hidden="true"></i>
			</a>
		</li>
		
		<li class="disabled">
			<a href="#" class="tab-grey">
				<?= lang("part_g"); ?>
				<i class="fa fa-chevron-right" aria-hidden="true"></i>
			</a>
		</li>
		
		<li class="disabled">
			<a href="#" class="tab-grey">
				<?= lang("part_h"); ?>
				<i class="fa fa-chevron-right" aria-hidden="true"></i>
			</a>
		</li>
		
		<li class="disabled">
			<a href="#" class="tab-grey">
				<?= lang("part_i"); ?>
				<i class="fa fa-chevron-right" aria-hidden="true"></i>
			</a>
		</li>
		<li class="disabled">
			<a href="#" class="tab-grey">
				<?= lang("part_j"); ?>
				<i class="fa fa-chevron-right" aria-hidden="true"></i>
			</a>
		</li>
		<li class="disabled">
			<a href="#" class="tab-grey">
				<?= lang("notification"); ?>
				<i class="fa fa-chevron-right" aria-hidden="true"></i>
			</a>
		</li>
		
	</ul>
	
	<?php 
		$pages = $page; 
	?>
	
	<?= form_open_multipart("asylum_seekers/add_rsd_application/".$id); ?>
	
	<?php 
		$countries_ = array(lang("select"));
		foreach($countries as $country){
			$countries_[$country->id] = $country->country;
		} 
		$provinces_ = array(lang("select"));
		foreach($provinces as $province){
			$provinces_[$province->id] = $province->name;
		}  
		$districts_ = array(lang("select"));
		foreach($districts as $district){
			$districts_[$district->id] = $district->name;
		} 
		$communes_ = array(lang("select"));
		foreach($communes as $commune){
			$communes_[$commune->id] = $commune->name;
		} 
	?>
	
	<div class="tab-content">
		
		<div id="content_2" class="tab-pane fade in">
			
			<div class="box">
					
				<div class="box-header">
					<h2 class="blue">
						<i class="fa-fw fa fa-barcode"></i>
						<?= lang("rsd_application") ?> - <?= lang("basic_bio_data") ?>
					</h2>
				</div>
				
			<div class="box-content">
				<div class="row">
				
					<div class="col-sm-6" style="padding-right:0px;">
						
						<div class="col-sm-12">
							<div class="form-group">
								<?php echo lang('counseling_no', 'counseling_no'); ?>​ 
								<div class="controls">
									<input type="text" value="<?= set_value('counseling_no',$counseling->counseling_no) ?>" name="counseling_no" id="counseling_no" class="form-control input-sm" />
									<input type="hidden" name="counseling_id" id="counseling_id" value="<?= $counseling->id ?>"/>									
								</div>
							</div>
						</div>
						
						
						<div class="form-group col-sm-6" >
							<?php echo lang('នាមខ្លួន​ / ​Given Name', 'នាមខ្លួន​ / ​Given Name'); ?>​ 
							<span class="red">*</span>
							<div class="controls">
								<input type="text" value="<?= set_value('firstname_kh', $counseling->firstname_kh) ?>" name="firstname_kh" class="form-control input-sm" />								
							</div>
						</div>
						
						<div class="form-group col-sm-6" >
						<?php echo lang('&nbsp;', '&nbsp;'); ?>​ 
							<div class="controls">
								<input type="text" value="<?= set_value('firstname', $counseling->firstname) ?>" name="firstname" class="form-control input-sm" />								
							</div>
						</div>
						 
						<div class="col-sm-6">
							<div class="form-group">
								<?php echo lang('នាមត្រកូល / ​ Surname', 'នាមត្រកូល / ​ Surname'); ?>​ 
								<span class="red">*</span>
								<div class="controls">
									<input type="text" value="<?= set_value('lastname_kh', $counseling->lastname_kh) ?>" name="lastname_kh" class="form-control input-sm" />								
								</div>
							</div>
						</div>
						
						<div class="form-group col-sm-6" >
						<?php echo lang('&nbsp;', '&nbsp;'); ?>​ 
							<div class="controls">
								<input type="text" value="<?= set_value('lastname', $counseling->lastname) ?>" name="lastname" class="form-control input-sm" />								
							</div>
						</div>
						
						<div class="col-sm-6">
							<div class="form-group">
								<?php echo lang('ឈ្មោះហៅក្រៅ / Nickname', 'ឈ្មោះហៅក្រៅ / Nickname'); ?>​ 
								<div class="controls">
									<input type="text" value="<?= set_value('othername_kh', $counseling->nickname_kh) ?>" name="othername_kh" class="form-control input-sm" />								
								</div>
							</div>
						</div>
						
						<div class="col-sm-6">
							<div class="form-group">
									<?php echo lang('&nbsp;', '&nbsp;'); ?>​ 
								<div class="controls">
									<input type="text" value="<?= set_value('othername', $counseling->nickname) ?>" name="othername" class="form-control input-sm" />								
								</div>
							</div>
						</div>
						
						<div class="col-sm-6">
							<div class="form-group">
								<?php echo lang('ឈ្មោះឪពុក​ / Father Name', 'ឈ្មោះឪពុក​ / Father Name'); ?>​ 
								<span class="red">*</span>
								<div class="controls">
									<input type="text" value="<?= set_value('fathername_kh') ?>" name="fathername_kh" class="form-control input-sm" />								
								</div>
							</div>
						</div>
						
						<div class="col-sm-6">
							<div class="form-group">
									<?php echo lang('&nbsp;', '&nbsp;'); ?>​ 
								<div class="controls">
									<input type="text" value="<?= set_value('fathername') ?>" name="fathername" class="form-control input-sm" />								
								</div>
							</div>
						</div>
						
						<div class="col-sm-6">
							<div class="form-group">
								<?php echo lang('ឈ្មោះម្តាយ​ / Mother Name', 'ឈ្មោះម្តាយ / Mother Name'); ?>​ 
								<span class="red">*</span>
								<div class="controls">
									<input type="text" value="<?= set_value('mothername') ?>" name="mothername_kh" class="form-control input-sm" />								
								</div>
							</div>
						</div>
						
						<div class="col-sm-6">
							<div class="form-group">
									<?php echo lang('&nbsp;', '&nbsp;'); ?>​ 
								<div class="controls">
									<input type="text" value="<?= set_value('mothername') ?>" name="mothername" class="form-control input-sm" />								
								</div>
							</div>
						</div>
						
						<div class="col-sm-6 text-nowrap">
							<div class="form-group">
								<?php echo lang('ឈ្មោះប្តីប្រពន្ធ (បើមាន)  / Spouse’s Name (if applicable)', 'ឈ្មោះប្តីប្រពន្ធ (បើមាន)  / Spouse’s Name (if applicable)'); ?>
								<div class="controls">
									<input type="text" value="<?= set_value('spouse_kh',$counseling->spouse_kh) ?>" name="spouse_kh" class="form-control input-sm" />
								</div>
							</div>
						</div>
						
						<div class="col-sm-6">
							<div class="form-group">
								<?php echo lang('&nbsp;', '&nbsp;'); ?>​ 
								<div class="controls">
									<input type="text" value="<?= set_value('spouse',$counseling->spouse) ?>" name="spouse" class="form-control input-sm" />
								</div>
							</div>
						</div>
						
						
						
						<div class="col-sm-12 ">
							<div class="form-group">
								<?php echo lang('ស្ថានភាពគ្រួសា​/Marital Status', 'ស្ថានភាពគ្រួសា​/Marital Status'); ?>
								<span class="red">*</span>
								<div class="controls">
									<label class="radio-inline">
										<input type="radio" value="single" checked name="marital_status"><?= lang("single") ?>
									</label>
									<label class="radio-inline">
										<input type="radio" value="married" name="marital_status"><?= lang("married") ?>
									</label> 
									<label class="radio-inline">
										<input type="radio" value="engaged" name="marital_status"><?= lang("engaged") ?>
									</label> 
									<label class="radio-inline">
										<input type="radio" value="separated" name="marital_status"><?= lang("separated") ?>
									</label> 
										<label class="radio-inline">
										<input type="radio" value="divorced" name="marital_status"><?= lang("divorced") ?>
									</label>  
								</div>
							</div>
						</div>
						
						<div class="col-sm-6">
							<div class="form-group">
								<?php echo lang('សញ្ជាតិ / Nationality', 'សញ្ជាតិ / Nationality'); ?>
								<span class="red">*</span>
								<div class="controls">
									<input type="text" value="<?= set_value('nationality_kh', $counseling->nationality_kh) ?>" name="nationality_kh" class="form-control input-sm" />
								</div>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<?php echo lang('&nbsp;', '&nbsp;'); ?>​ 
								<div class="controls">
									<input type="text" value="<?= set_value('nationality', $counseling->nationality) ?>" name="nationality" class="form-control input-sm" />
								</div>
							</div>
						</div>
						
						<div class="col-sm-6">
							<div class="form-group">
								<?php echo lang('ថ្ងៃខែឆ្នាំកំណើត ​ / Date of Birth', 'ថ្ងៃខែឆ្នាំកំណើត ​ / Date of Birth'); ?>
								<span class="red">*</span>
								<div class="controls">
									<input type="text" value="<?= set_value('dob', $this->erp->hrsd($counseling->dob) ) ?>" name="dob" class="form-control input-sm date" />
								</div>
							</div>
						</div>
						
					
						<div class="col-sm-6">
							<div class="form-group">
								<?php echo lang('ភេទ / Sex', 'ភេទ / Sex'); ?>
								<div class="controls">
									<label class="radio-inline">
										<input type="radio" value="male" id="male" <?= ($counseling->gender=="male"?"checked":""); ?> name="gender"><?= lang("male") ?>
									</label>
									<label class="radio-inline">
										<input type="radio" value="female" id="female"  <?= ($counseling->gender=="female"?"checked":""); ?> name="gender"><?= lang("female") ?>
									</label> 
								</div>
							</div>
						</div>
						
						<div class="clearfix"></div>
						
						<div class="col-sm-12">
							<div class="panel panel-warning">
                                <div class="panel-heading"><?php echo lang("អាសយដ្ឋាននៅប្រទេសដើម / Address of original country","អាសយដ្ឋាននៅប្រទេសដើម / Address of original country") ?></div>
                                <div class="panel-body" style="padding: 5px;">
									
									<div class="col-lg-6">
										<div class="form-group">
											<?php echo lang('country', 'country'); ?>
											<div class="controls">
												<?php echo form_dropdown('country_o', $countries_, $counseling->country_o, ' class="form-control" '); ?>												
											</div>
										</div>
									</div>
									
									<div class="col-lg-6">
										<div class="form-group">
											<?php echo lang('province', 'province'); ?>
											<div class="controls">
												<?php echo form_dropdown('province_o', $provinces_, $counseling->province_o, ' class="form-control" '); ?>												
											</div>
										</div>
									</div>
									
									<div class="col-lg-6">
										<div class="form-group">
											<?php echo lang('district', 'district'); ?>
											<div class="controls">
												<?php echo form_dropdown('district_o', $districts_, $counseling->district_o, ' class="form-control" '); ?>												
											</div>
										</div>
									</div>
									
									<div class="col-lg-6">
										<div class="form-group">
											<?php echo lang('commune', 'commune'); ?>
											<div class="controls">
												<?php echo form_dropdown('commune_o', $communes_, $counseling->commune_o, ' class="form-control" '); ?>												
											</div>
										</div>
									</div>
									
									<div class="col-sm-12">
										<div class="form-group">
											<?php echo lang('អាសយដ្ឋាន', 'អាសយដ្ឋាន'); ?> (<?php echo lang("KH") ?>)
											<span class="red">*</span>
											<div class="controls">
												<input type="text" value="<?= set_value('address_o_kh',$counseling->original_address_kh) ?>" name="address_o_kh" class="form-control input-sm" />
											</div>
										</div>
									</div>
									
									<div class="col-sm-12">
										<div class="form-group">
											<?php echo lang('អាសយដ្ឋាន', 'អាសយដ្ឋាន'); ?> (<?php echo lang("EN") ?>)
											<div class="controls">
												<input type="text" value="<?= set_value('address_o',$counseling->original_address) ?>" name="address_o" class="form-control input-sm" />
											</div>
										</div>
									</div> 
						
								</div>
							</div>
						</div>
						
						
						<div class="col-sm-12">
							<div class="form-group">
								<button type="submit" class="btn btn-success ">
									<?= lang("submit"); ?>
									
								</button>
							</div>
						</div>
					</div>
					
					<div class="col-sm-6" style="padding-left:0px;">
						
						<div class="col-sm-8">
							<div class="form-group">
								<?php echo lang('ទីកន្លែងចុះឈ្មោះ / Place of Registration', 'ទីកន្លែងចុះឈ្មោះ / Place of Registration'); ?>​ 
								<div class="controls">
									<?php foreach($offices as $office){ ?>
									<label class="radio-inline">
										<input <?= ($application->refugee_office==$office->id?'checked':'')?> type="radio" value="<?= $office->id; ?>" name="refugee_office"> <?= $office->office; ?>
									</label>
									<?php  }  ?>
								</div>
							</div>													
					   
							<div class="col-sm-6" style="padding-left:0px;">
								<div class="form-group text-nowrap ">
									<?php echo lang('លេខផ្តើមករណី ', 'លេខផ្តើមករណី '); ?>​
									<span class="red">*</span>
									<div class="controls">
										<select name="case_prefix" class="form-control" id="case_prefix"> 
											<?php if($case_prefix !="" || $case_prefix != null) { ?>
												<option value=''>...</option>
												<?php foreach($case_prefix as $row){?>
													<option value="<?= strtoupper($row->case_prefix) ?>" <?=($row->case_prefix==$this->input->post("case_prefix"))?"selected":""?>><?= strtoupper($row->case_prefix)?></option>
												<?php }?>
											<?php } else {?> 
												<option value=''>...</option>
											<?php }?>
										</select> 
									</div>
								</div>
							</div>
							<input type="hidden" name="full_case" value="<?= set_value('full_case') ?>" id="full_case" />
						
							<div class="col-sm-6" style="padding-right:0px;">	
								<div class="form-group">
									<?php echo lang('លេខករណី / Case No', 'លេខករណី / Case No'); ?>​
									<span class="red">*</span>
									<div class="controls">
										<input type="text" value="<?= set_value('case_no') ?>" name="case_no" class="form-control" id='case_no'/>								
									</div>
								</div>
							</div> 
							<div class="form-group">
								<?php echo lang('កាលបរិច្ឆេទមកដល់ / Arrived Date', 'កាលបរិច្ឆេទមកដល់ / Arrived Date'); ?>​ 
								<span class="red">*</span>
								<div class="controls">
									<input type="text" value="<?= set_value('arrived_date',date("d/m/Y")) ?>" name="arrived_date" class="form-control input-sm date" />								
								</div>
							</div>
							
							<div class="form-group">
								<?php echo lang('កាលបរិច្ឆេទចុះឈ្មោះ​ / Registered Date', 'កាលបរិច្ឆេទចុះឈ្មោះ​ / Registered Date'); ?>​ 
								<span class="red">*</span>
								<div class="controls">
									<input type="text" value="<?= set_value('reg_date',date("d/m/Y")) ?>" name="reg_date" class="form-control input-sm date" />								
								</div>
							</div>
						</div> 
						<div class="col-md-4 center">
							 <div class="col-md-12" style="padding-left:0px; padding-right:0px;">
								<?= lang("photo", "photo"); ?>
								<?php
									$photo = "no_image.png";
									
									if(!empty($counseling->photo) || !file_exists("assets/uploads/".$counseling->photo)){
										$photo = $counseling->photo;
									}
								?>
								<div class="form-group">
								  <div class="main-img-preview">
									<img class="thumbnail img-preview" id='img_preview' src="<?= site_url("assets/uploads/".$photo); ?>" title="Preview Logo">
								  </div>
								  <div class="input-group">
									<input id="fakeUploadLogo" class="form-control input-sm fake-shadow" placeholder="<?= lang("choose_file") ?>" disabled="disabled">
									<div class="input-group-btn">
									  <div class="fileUpload btn btn-danger fake-shadow">
										<span><i class="fa fa-upload"></i>&nbsp;&nbsp;<?= lang("upload") ?></span>
										<input id="logo-id" accept=".gif,.jpg,.png" data-maxfile="1024" name="photo" type="file" class="attachment_upload">
									  </div>
									</div>
								  </div>
								</div> 
								
							</div> 
						</div>
						
						
						<div class="col-sm-12">
						
							<div class="form-group">
								<?php 
									$applications_ = array(lang("select"));
									foreach($applications as $application){
										$case_no1 = $application->case_prefix.$application->case_no;
										$applications_[$case_no1] = $case_no1;
									} 
								?>
								<?php echo lang('ករណីពាក់ព័ន្ធ  /  Related Case', 'ករណីពាក់ព័ន្ធ / Related Case'); ?>​ 
								
								<div class="controls">
									<?php echo form_dropdown('related_case', $applications_, set_value('related_case'), ' class="form-control" '); ?>									
								</div>
							</div>
							
							<div class="form-group">
								<?php echo lang('តំរូវការចាំបាច់ផ្សេងៗ / Special Need', 'តំរូវការចាំបាច់ផ្សេងៗ / Special Need'); ?>​ 
								<span class="red">*</span>
								<div class="controls">
									<input type="text" value="<?= set_value('special_need') ?>" name="special_need" class="form-control input-sm" />								
								</div>
							</div>
						</div>
						
						<div class="col-sm-6">
							<div class="form-group">
								<?php echo lang('សាសនា / Religion', 'សាសនា / Religion'); ?>
								<span class="red">*</span> 
								<div class="controls">
									<input type="text" value="<?= set_value('religion_kh',$counseling->religion_kh) ?>" name="religion_kh" class="form-control input-sm" />
								</div>
							</div>
						</div>

						<div class="col-sm-6">
							<div class="form-group">
								<?php echo lang('&nbsp;', '&nbsp;'); ?>​ 
								<div class="controls">
									<input type="text" value="<?= set_value('religion',$counseling->religion) ?>" name="religion" class="form-control input-sm" />
								</div>
							</div>
						</div>

						<div class="col-sm-6">
							<div class="form-group">
								<?php echo lang('ជាតិពន្ធុ / Ethnicity', 'ជាតិពន្ធុ / Ethnicity'); ?>
								<span class="red">*</span>
								<div class="controls">
									<input type="text" value="<?= set_value('ethnicity_kh',$counseling->ethnicity_kh) ?>" name="ethnicity_kh" class="form-control input-sm" />
								</div>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<?php echo lang('&nbsp;', '&nbsp;'); ?>​ 
								<div class="controls">
									<input type="text" value="<?= set_value('ethnicity',$counseling->ethnicity) ?>" name="ethnicity" class="form-control input-sm" />
								</div>
							</div>
						</div> 
						
						
						<div class="col-sm-6 text-nowrap">
							<div class="form-group">
								<?php echo lang('ទីកន្លែងកំណើត / Place of Birth', 'ទីកន្លែងកំណើត / Place of Birth'); ?> 
								<span class="red">*</span>
								<div class="controls">
									<input type="text" value="<?= set_value('pob_kh', $counseling->pob_kh) ?>" name="pob_kh" class="form-control input-sm" />
								</div>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group"> 
								<?php echo lang('&nbsp;', '&nbsp;'); ?>​ 
								<div class="controls">
									<input type="text" value="<?= set_value('pob', $counseling->pob) ?>" name="pob" class="form-control input-sm" />
								</div>
							</div>
						</div>
						
						
						
						<div class="col-sm-12">
							<div class="panel panel-warning">
							<div class="panel-heading"><?php echo lang("អាសយដ្ឋានបច្ចុប្បន្ន","អាសយដ្ឋានបច្ចុប្បន្ន") ?> / <?= lang("Current address") ?></div>
							<div class="panel-body" style="padding: 5px;">
								<div class="col-lg-6">
									<div class="form-group">
										<?php echo lang('country', 'country'); ?>
										<div class="controls">
											<?php echo form_dropdown('country', $countries_, $counseling->country, ' class="form-control" '); ?>												
										</div>
									</div>
								</div>
								
								<div class="col-lg-6">
									<div class="form-group">
										<?php echo lang('province', 'province'); ?>
										<div class="controls">
											<?php echo form_dropdown('province', $provinces_, $counseling->province, ' class="form-control" '); ?>												
										</div>
									</div>
								</div>
								
								<div class="col-lg-6">
									<div class="form-group">
										<?php echo lang('district', 'district'); ?>
										<div class="controls">
											<?php echo form_dropdown('district', $districts_, $counseling->district, ' class="form-control" '); ?>												
										</div>
									</div>
								</div>
								
								<div class="col-lg-6">
									<div class="form-group">
										<?php echo lang('commune', 'commune'); ?>
										<div class="controls">
											<?php echo form_dropdown('commune', $communes_, $counseling->commune, ' class="form-control" '); ?>												
										</div>
									</div>
								</div>
								
								<div class="col-sm-12">
									<div class="form-group">
										<?php echo lang('អាសយដ្ឋាន', 'អាសយដ្ឋាន'); ?> (<?php echo lang("KH") ?>)
										<span class="red">*</span>
										<div class="controls">
											<input type="text" value="<?= set_value('address',$counseling->address_kh) ?>" name="address" class="form-control input-sm" />
										</div>
									</div>
								</div>
								
								<div class="col-sm-12">
									<div class="form-group">
										<?php echo lang('អាសយដ្ឋាន', 'អាសយដ្ឋាន'); ?> (<?php echo lang("EN") ?>)
										<div class="controls">
											<input type="text" value="<?= set_value('address_kh',$counseling->address) ?>" name="address_kh" class="form-control input-sm" />
										</div>
									</div>
								</div>
								
							</div>
						</div>
					</div>
					
					
					<div class="col-sm-12 text-nowrap">
						<div class="form-group">
							<?php echo lang('ទូរស័ព្ទទំនាក់ទំនង / Phone number', 'ទូរស័ព្ទទំនាក់ទំនង / Phone number'); ?>​ 
							<div class="controls">
								<input type="text" value="<?= set_value('phone', $counseling->phone) ?>" name="phone" class="form-control input-sm" data-format="+855 (ddd) ddd-dddd" />
							</div>
						</div>
					</div>
						
					</div>
					
				</div>
				
				<?= form_close()?>
				
			</div>

			</div>
			
		</div>
		
		<div id="content_3" class="tab-pane fade in">
			
			<div class="box"> 
				<div class="box-header">
					<h2 class="blue">
						<i class="fa-fw fa fa-barcode"></i>
						 <?= lang("part_b") ?> - <?= lang("education_level", "Education Level") ?>
					</h2>
				</div>
				
				<div class="box-content">
					<div class="row">
						
						<div class="col-sm-12">
							<div class="form-group">
								<?php 
									$str='';
									if(!empty($pages)) { 
										$str = '?page='.$pages; 
									} 
								?>
								<a  data-backdrop="static" data-keyboard="false"  data-toggle="modal" data-target="#myModal" href="<?= base_url().'asylum_seekers/add_education/'.$counseling->id.$str?>"  class="btn btn-warning" id="add_B">
									<?= lang("add"); ?>
									<i class="fa fa-plus"></i>
								</a>
							</div>
						</div>​
						<div class="col-sm-12">
							<div class="form-group"> 
								<table class="table table-condensed table-bordered table-hover table-striped text-center" width="100%">
									<thead>
										<tr>  	
											<td></td>
											<td><?= lang("name_of_institution") ?></td>
											<td><?= lang("place_or_country") ?></td>
											<td><?= lang("from") ?></td>
											<td><?= lang("to") ?></td>
											<td><?= lang("qualigication_obtained") ?></td>
										</tr>
									</thead>
									<tbody id="part_B"> 
									<?php foreach($educations as $edu){ ?>
										<tr>  
											<td>
												<a href="<?= site_url("asylum_seekers/delete_education/".$edu->id); ?>" class='btn btn-danger btn-xs remove'>
													<i class='fa fa-trash'></i>
												</a>
												<a href="<?= site_url("asylum_seekers/edit_education/".$counseling->id.'/'.$edu->id); ?>" class='btn btn-success btn-xs' data-backdrop="static" data-keyboard="false"  data-toggle="modal" data-target="#myModal">
													<i class='fa fa-edit'></i>
												</a>
											</td> 
											<td><?= $edu->name_institution?></td>
											<td><?= $edu->country?></td>
											<td><?= $this->erp->hrsd($edu->from)?></td>
											<td><?= $this->erp->hrsd($edu->to)?></td>
											<td><?= $edu->qualification?></td>
										</tr>
									<?php }?>
									</tbody>
								</table>
								
							</div>
						</div> 
					</div>
				</div> 
			</div>		 
		</div>
		
		<div id="content_4" class="tab-pane fade in">
			
			<div class="box">
					
				<div class="box-header">
					<h2 class="blue">
						<i class="fa-fw fa fa-barcode"></i>
						<?= lang("part_c") ?> – <?= lang("occupation_level") ?>
					</h2>
				</div>
				
				<div class="box-content">
					<div class="row">
						
						<div class="col-sm-12">
							<div class="form-group">
								<a data-backdrop="static" data-keyboard="false"  data-toggle="modal" data-target="#myModal" href="<?= base_url().'asylum_seekers/add_occupation_level/'.$counseling->id?>"  class="btn btn-warning" id="add_C">
									<?= lang("add"); ?>
									<i class="fa fa-plus"></i>
								</a>
							</div>
						</div>
						
						<div class="col-sm-12">
							<div class="form-group"> 
								<table class="table table-condensed table-bordered table-hover table-striped text-center" width="100%">
									<thead>
										<tr>
											<td width='100'></td>
											<td><?= lang("name_of_employer") ?></td>
											<td><?= lang("place_or_country") ?></td>
											<td><?= lang("from") ?></td>
											<td><?= lang("to") ?></td>
											<td><?= lang("job_title") ?></td>
										</tr>
									</thead>
									<tbody id="part_C">
										<?php foreach($occupation as $occ){ ?>
										<tr>  
											<td>
												<a href="<?= site_url("asylum_seekers/delete_occupation_level/".$occ->id); ?>" class='btn btn-danger btn-xs remove'>
													<i class='fa fa-trash'></i>
												</a>
												<a href="<?= site_url("asylum_seekers/edit_occupation_level/".$counseling->id.'/'.$occ->id); ?>" class='btn btn-success btn-xs' data-backdrop="static" data-keyboard="false"  data-toggle="modal" data-target="#myModal">
													<i class='fa fa-edit'></i>
												</a>
											</td>
											<td><?= $occ->name_employer?></td>
											<td><?= $occ->country?></td>
											<td><?= $this->erp->hrsd($occ->from)?></td>
											<td><?= $this->erp->hrsd($occ->to)?></td>
											<td><?= $occ->job_title?></td>​
										</tr>
										<?php }?>
									</tbody>
								</table>
								
							</div>
						</div> 
					</div>
				</div>
					
			</div>			
		
		</div>
		
		<div id="content_5" class="tab-pane fade in"> 
			<div class="box"> 
				<div class="box-header">
					<h2 class="blue">
						<i class="fa-fw fa fa-barcode"></i>
						<?= lang("part_d") ?> – <?= lang("identification_documents") ?>
					</h2>
				</div>
				
				<div class="box-content">
					<div class="row">  
					
					<?= form_open("asylum_seekers/add_registration_documentation/".$id);?>
					
						<div class="col-sm-12">
							<div class="form-group">
								<a data-toggle='modal' data-target='#myModal'  href="<?= base_url().'asylum_seekers/add_identification_document/'.$counseling->id?>" class="btn btn-warning" id="add_D">
									<?= lang("add"); ?>
									<i class="fa fa-plus"></i>
								</a>
							</div>
						</div>
						
						<div class="col-sm-12">
							<div class="form-group">
								
								<table class="text-center table table-condensed table-bordered table-hover table-striped" width="100%">
									<thead>
										<tr>
											<td class="text-center">#</td>
											<td><?= lang("document_type") ?></td>
											<td><?= lang("issued_place") ?></td>
											<td><?= lang("issued_date") ?></td>
											<td><?= lang("expired_date") ?></td>
											<td><?= lang("original_provided") ?></td>
										</tr>
									</thead>
									<tbody id="part_D">
									<?php foreach($identification as $iden){?>
										<tr>
											<td width='100' class='text-justify'>
												<a title="<?= lang('delete')?>" href="<?= site_url("asylum_seekers/delete_identification_document/".$iden->id); ?>" class='btn btn-danger btn-xs remove'>
													<i class='fa fa-trash'></i>
												</a>
												<a title="<?= lang('edit')?>" href="<?= site_url("asylum_seekers/edit_identification_document/".$counseling->id.'/'.$iden->id); ?>" class='btn btn-success btn-xs' data-backdrop="static" data-keyboard="false"  data-toggle="modal" data-target="#myModal">
													<i class='fa fa-edit'></i>
												</a>
												<?php if(!empty($iden->attachment)){?>
												<a target='_blank' title="<?= lang('download')?>" href="<?= site_url("assets/uploads/document/identification/".$iden->attachment); ?>" class='btn btn-primary btn-xs'>
													<i class='fa fa-download'></i> 
												</a>
												<?php }?>
											</td>
											<td><?= $iden->document_type?></td> 
											<td><?= $iden->issued_place?></td> 
											<td><?= $this->erp->hrsd($iden->issued_date)?></td> 
											<td><?= $this->erp->hrsd($iden->expired_date)?></td> 
											<td><?= $iden->original_provided?></td> 
										</tr>
									<?php }?>
									</tbody>
								</table>
								
							</div>
						</div> 
					 <div class="col-sm-12">
							<div class="form-group">  
								<?= form_open("asylum_seekers/add_other_document/".$id); ?>	
								<div class="form-group">
									<?php echo lang('documents_obtained_illegally', 'Documents Obtained Illegally'); ?>
									<p><?= lang("if_any_of_the_documents_listed_above_were_illegally_issued_please_explain_how_they_were_obtained") ?></p>							
									<div class="controls">
										<textarea class="form-control" name="comment_document_obtained_illegally">
											<?= $application->comment_document_obtained_illegally;?>
										</textarea> 	
									</div>
								</div>
								
								<div class="form-group">
									<?php echo lang('missing_documents', 'Missing Documents'); ?>
									<p><?= lang('If_you_are_missing_identity_documents_or_other_documents_that_are_relevant_to_your_claim_please_explain_why_you_do_not_have_these_documents') ?></p>
									<div class="controls">
										<textarea class="form-control" name="document_missing"><?= $application->document_missing;?></textarea> 
									</div>
								</div>
								
								<div class="form-group">
									<p><?= lang('If_you_are_missing_documents_will_you_be_able_to_obtain_these_documents_in_the_future_If_not_please_explain_why') ?>
									</p>
									<div class="controls">
										<textarea name="comment_document_mission" class="form-control input-sm">
											<?= $application->comment_document_mission;?>
										</textarea> ​
									</div>
								</div>
							</div>
						</div> 
						<div class="col-sm-12">
							<div class="form-group">
								<button class="btn btn-success"><?= lang("submit"); ?></button> 
							</div>
						</div>
						
					</div>
					
					<?= form_close();?>
					
				</div>
					
			</div>			
		
		</div>
		
		<div id="content_6" class="tab-pane fade in"> 
			<div class="box">
					
				<div class="box-header">
					<h2 class="blue">
						<i class="fa-fw fa fa-barcode"></i>
						<?= lang("part_e") ?> – <?= lang("applicant_registration_history"); ?>
					</h2>
				</div> 
				<div class="box-content">
					<div class="row"> 
					<?= form_open("asylum_seekers/add_registration_history/".$id);?>
						<div class="col-sm-6">
							<div class="form-group">  
								<div class="form-group">
									<p>1. <?= lang('have_you_ever_been_registered_with_refugee_department') ?></p>							
									<div class="controls">
										<label class="radio-inline" onclick='showFunction(1)'>
											<input type="radio" <?= ($application->registered_refugee_department=='yes'?'checked':'')?> value="yes" name="registered_refugee_department"><?= lang("yes"); ?>
										</label>
										<label class="radio-inline" onclick='hideFunction(1)'>
											<input type="radio"  <?= ($application->registered_refugee_department=='no'?'checked':'')?> value="no" name="registered_refugee_department"><?= lang("no"); ?>
										</label>
									</div>
								</div>
								<div id='answer'>
									<div class="form-group">						
										<div class="controls">
											<p><?= lang('if_yes_where_were_you_registered') ?></p>	
											<input value='<?= $application->registered_refugee_place ?>' type="text" name="registered_refugee_place" class="form-control input-sm" />
										</div>
									</div>
									
									<div class="form-group">						
										<div class="controls">
											<p><?= lang('case_no') ?></p>	
											<input type="text" value='<?= $application->registered_refugee_no ?>'  name="registered_refugee_no" class="form-control input-sm" />
										</div>
									</div>
									
									<div class="form-group">						
										<div class="controls">
											<p><?= lang('date_of_registration') ?></p>	
											<input type="text" value='<?= $this->erp->hrsd($application->registered_refugee_date) ?>'  name="registered_refugee_date" class="form-control input-sm" />
										</div>
									</div>
								</div>
								
								<div class="form-group">
									<p>
									2. <?= lang('have_you_ever_applied_for_refugee_protection_with_UNHC_or_a_government') ?>
									</p>							
									<div class="controls">
										<label class="radio-inline" onclick='showFunction(2)'>
											<input type="radio"  <?= ($application->applied_refugee_protection=='yes'?'checked':'')?> value="yes" name="applied_refugee_protection"><?= lang("yes"); ?>
										</label>
										<label class="radio-inline" onclick='hideFunction(2)'>
											<input type="radio"  <?= ($application->applied_refugee_protection=='no'?'checked':'')?> value="no" name="applied_refugee_protection"><?= lang("no"); ?>
										</label>
									</div>	
								</div>
								<div id='answer2' class="form-group">			
									<div class="controls">
										<p><?= lang('where') ?></p>	
										<input type="text"  value='<?= $application->applied_refugee_place ?>' name="applied_refugee_place" class="form-control input-sm" />
									</div>
									<div class="controls">
										<p><?= lang('when') ?></p>	
										<input type="text"  value='<?= $this->erp->hrsd($application->applied_refugee_date) ?>' name="applied_refugee_date" class="form-control input-sm" />
									</div>	
								</div>  
								<div class="form-group">								
									<div class="controls">
										<p><?= lang('ថ្ងៃទទួលលទ្ធផល') ?></p>	
										<input type="text"  value='<?= set_value('applied_recognized_date',date('d/m/Y'))?>' name="applied_recognized_date" class="date form-control input-sm" />
									</div>	 
								</div>
								<div class="form-group">								
									<div class="controls">
									<p><?= lang('ឋានៈដែលបានទទួល') ?></p> 
										<input type="text"  value='<?= set_value('recognition_refugee')?>' name="recognition_refugee" class="form-control input-sm" />
									</div>	 
								</div>
							</div>
							</div>
							<div class="col-sm-12">
								<div class="control">
									<button type="submit" class="btn btn-success ">
										<?= lang("submit"); ?> 
									</button>
								</div>
							</div>
					<?= form_close();?>
					</div>
				</div> 
			</div>		 
		</div>
		
		<div id="content_7" class="tab-pane fade in">
			
			<div class="box">
					
				<div class="box-header">
					<h2 class="blue">
						<i class="fa-fw fa fa-barcode"></i>
						<?= lang("part_f") ?> - <?= lang("family_members_dependants_accompanying_the_applicants") ?>
					</h2>
				</div>
				
				<div class="box-content">
					<div class="row">
						
						<div class="col-sm-12">
							<div class="form-group">
								<a  data-toggle='modal' data-target='#myModal'  href="<?= base_url().'asylum_seekers/add_family_members_dependants_accompanying/'.$counseling->id?>" class="btn btn-warning" id="add_F">
									<?= lang("add"); ?>
									<i class="fa fa-plus"></i>
								</a>
							</div>
						</div>
						
						<div class="col-sm-12">
							<div class="form-group"> 
								<table class="table text-center table-condensed table-bordered table-hover table-striped" width="100%">
									<thead>
										<tr>
											<td class="text-center">#</td>
											<td><?= lang('full_name') ?></td>
											<td><?= lang('individual_case_no') ?></td>
											<td><?= lang('relationship_to_application') ?></td>
											<td><?= lang('sex') ?></td>
											<td><?= lang('dob') ?> </td>											
										</tr>
									</thead>
									<tbody id="part_F">
									<?php foreach($family_member_accompanying as $member){?>
										<tr>
											<td>
												<a href="<?= site_url("asylum_seekers/delete_family_members_dependants_accompanying/".$member->id); ?>" class='btn btn-danger btn-xs remove'>
													<i class='fa fa-trash'></i>
												</a>
												<a href="<?= site_url("asylum_seekers/edit_family_members_dependants_accompanying/".$counseling->id.'/'.$member->id); ?>" class='btn btn-success btn-xs' data-backdrop="static" data-keyboard="false"  data-toggle="modal" data-target="#myModal">
													<i class='fa fa-edit'></i>
												</a>
											</td> 
											<td><?= $member->lastname_kh.' '.$member->firstname_kh ?></td> 
											<td><?= $member->individual_case_no ?></td> 
											<td><?= lang($member->relationship) ?></td> 
											<td><?= lang($member->gender) ?></td>
											<td><?= $this->erp->hrsd($member->dob) ?></td>
										</tr>	
									<?php }?>										
									</tbody>
								</table>
								
							</div>
						</div>
						 
						
					</div>
				</div>
			</div>			
		
		</div>
		
		<div id="content_8" class="tab-pane fade in">
			
			<div class="box">
					
				<div class="box-header">
					<h2 class="blue">
						<i class="fa-fw fa fa-barcode"></i>
						<?= lang("part_g") ?> - <?= lang("non_accompanying_family_members_dependants_living_inside_home_country") ?>
					</h2>
				</div>
				
				<div class="box-content">
					<div class="row">
						
						<div class="col-sm-12">
							<div class="form-group">
								<a data-backdrop="static" data-keyboard="false"  data-toggle="modal" data-target='#myModal' href="<?= base_url().'asylum_seekers/add_family_members_dependants_none_accompanying/'.$counseling->id?>" class="btn btn-warning" id="add_G">
									<?= lang("add"); ?>
									<i class="fa fa-plus"></i>
								</a>
							</div>
						</div> 
						<div class="col-sm-12">
							<div class="form-group">
								
								<table class="table text-center table-condensed table-bordered table-hover table-striped" width="100%">
									<thead>
										<tr>
											<td class="text-center">#</td>
											<td><?= lang('full_name') ?></td>
											<td><?= lang('relationship_to_application') ?></td>
											<td><?= lang('sex')?></td>
											<td><?= lang('dob')?></td>
											<td><?= lang('address') ?></td>
										</tr>
									</thead>
									<tbody id="part_G"> 
										<?php foreach($family_member_none_accompanying as $member){?>
										<tr>
											<td>
												<a href="<?= site_url("asylum_seekers/delete_family_members_dependants_none_accompanying/".$member->id); ?>" class='btn btn-danger btn-xs remove'>
													<i class='fa fa-trash'></i>
												</a>
												<a href="<?= site_url("asylum_seekers/edit_family_members_dependants_none_accompanying/".$counseling->id.'/'.$member->id); ?>" class='btn btn-success btn-xs' data-backdrop="static" data-keyboard="false"  data-toggle="modal" data-target="#myModal">
													<i class='fa fa-edit'></i>
												</a>
											</td> 
											<td><?= $member->lastname_kh.' '.$member->firstname_kh ?></td>  
											<td><?= lang($member->relationship) ?></td> 
											<td><?= lang($member->gender) ?></td>
											<td><?= $this->erp->hrsd($member->dob) ?></td>
											<td><?= $member->address ?></td>
										</tr>	
									<?php }?>	
										
									</tbody>
								</table>
								
							</div>
						</div>
						 
						
					</div>
				</div>
			</div>			
		
		</div>
		
		<div id="content_9" class="tab-pane fade in">​
			<div class="box">
				<div class="box-header">
					<h2 class="blue">
						<i class="fa-fw fa fa-barcode"></i>
						<?= lang("part_h") ?> - <?= lang("non_accompanying_family_members_dependants_living_outside_home_country") ?>
					</h2>
				</div>
				<div class="box-content">
					<div class="row">  
						<div class="col-sm-12">
							<div class="form-group">
								<a data-backdrop="static" data-keyboard="false"  data-toggle="modal" data-target="#myModal" href="<?= base_url().'asylum_seekers/add_family_members_dependants_none_accompanying_outside_country/'.$counseling->id?>" class="btn btn-warning" id="add_H">
									<?= lang("add"); ?>
									<i class="fa fa-plus"></i>
								</a>
							</div>
						</div>
						
						<div class="col-sm-12">
							<div class="form-group">
								
								<table class="table text-center table-condensed table-bordered table-hover table-striped" width="100%">
									<thead>
										<tr>
											<td class="text-center">#</td>
											<td><?= lang('full_name') ?></td>
											<td><?= lang('relationship_to_application') ?></td>											
											<td><?= lang('dob') ?></td>
											<td><?= lang('address') ?></td>
											<td><?= lang('status') ?></td>
											<td><?= lang('ethnicity') ?></td>
										</tr>
									</thead>
									<tbody id="part_H">
										<?php foreach($family_member_none_accompanying_outside_country as $member){?>
										<tr>
											<td>
												<a href="<?= site_url("asylum_seekers/delete_family_members_dependants_none_accompanying/".$member->id); ?>" class='btn btn-danger btn-xs remove'>
													<i class='fa fa-trash'></i>
												</a>
												<a href="<?= site_url("asylum_seekers/edit_family_members_dependants_none_accompanying_outside_country/".$counseling->id.'/'.$member->id); ?>" class='btn btn-success btn-xs' data-backdrop="static" data-keyboard="false"  data-toggle="modal" data-target="#myModal">
													<i class='fa fa-edit'></i>
												</a>
											</td> 
											<td><?= $member->lastname_kh.' '.$member->firstname_kh ?></td>  
											<td><?= lang($member->relationship) ?></td>  
											<td><?= $this->erp->hrsd($member->dob) ?></td>
											<td><?= $member->address ?></td>
											<td><?= $member->status ?></td>
											<td><?= $member->ethnicity ?></td>
										</tr>
										<?php }?>
									</tbody>
								</table>
								
							</div>
						</div>
						
						
						<?= form_open("asylum_seekers/add_travel_detail/".$id); ?>	
						<div class="col-sm-12">
							<div class="form-group">
								<label>
								<?= lang('date_of_departure_from_home_country')?></label>							
								<div class="controls">
									<input type="text" value='<?= $this->erp->hrsd($application->departured_home_country) ?>' name="departured_home_country" class="form-control input-sm" />
								</div>	
							</div>
							
							<div class="form-group">
								<label>
								<?= lang('means_of_travel_out_of_home_country') ?></label>							
								<div class="controls">
									<input type="text"   value='<?= $application->means_home_country ?>' name="means_home_country" class="form-control input-sm" />
								</div>	
							</div>
							
							     
							<div class="form-group">
								<label>
								<?= lang('exit_point_from_home_country') ?></label>							
								<div class="controls">
									<input type="text"  value='<?= $application->exit_point_home_country ?>'   name="exit_point_home_country" class="form-control input-sm" />
								</div>	
							</div>
							
						</div>
						
						
						<div class="col-sm-12">
							<div class="form-group">
								<a  data-toggle='modal' data-target='#myModal' href="<?= base_url().'asylum_seekers/add_country_transit/'.$counseling->id?>" class="btn btn-danger" id="add_country">
									<?= lang("add"); ?>
									<i class="fa fa-plus"></i>
								</a>
							</div>
						</div> 
						<div class="col-sm-12">
							<div class="form-group">
								<table class="table text-center table-condensed table-bordered table-hover table-striped" width="100%">
									<thead>
										<tr>
											<td class="text-center">#</td>
											<td><?= lang('countries_of_transit') ?></td>
											<td><?= lang('from')?></td>											
											<td><?= lang('to') ?></td>
											<td><?= lang('travel_document_used')?></td>
										</tr>
									</thead>
									<tbody id="part_country">
										<?php foreach($country_transit as $country){?>
										<tr>
											<td width='100'>
												<a href="<?= site_url("asylum_seekers/delete_country_transit/".$country->id); ?>" class='btn btn-danger btn-xs remove'>
													<i class='fa fa-trash'></i>
												</a>
												<a href="<?= site_url("asylum_seekers/edit_country_transit/".$counseling->id.'/'.$country->id); ?>" class='btn btn-success btn-xs' data-backdrop="static" data-keyboard="false"  data-toggle="modal" data-target="#myModal">
													<i class='fa fa-edit'></i>
												</a>
											</td> 
											<td><?= $country->country_transit?></td> 
											<td><?= $this->erp->hrsd($country->from); ?></td> 
											<td><?= $this->erp->hrsd($country->to); ?></td> 
											<td><?= $country->travel_document_used?></td> 
										</tr>
										<?php }?>
									</tbody>
								</table>
							</div>
							
							<div class="form-group">
								<div class="form-group">
									<label><?= lang('entry_point_country') ?></label>							
									<div class="controls">
										<input type="text" value='<?= $application->entry_point_country ?>'   name="entry_point_country" class="form-control input-sm" />
									</div>	
								</div>
							</div>
							
							<div class="form-group">
								<div class="form-group">
									<label><?= lang('when_did_you_arrive_cambodia') ?></label>							
									<div class="controls">
										<input type="text"  value='<?= $this->erp->hrsd($application->arrived_country) ?>'   name="arrived_country" class="form-control input-sm" />
									</div>	
								</div>
							</div>
							
							<div class="form-group">
								<div class="form-group">
									<label> <?= lang('have_you_been_to_Cambodia_before') ?></label>							
									<div class="controls">
										<label class="radio-inline">
											<input type="radio" <?= ($application->accommodation_been=='yes'?'checked':'')?>  value='yes' name="accommodation_been"><?= lang("yes"); ?>
										</label>
										<label class="radio-inline">
											<input type="radio" <?= ($application->accommodation_been=='no'?'checked':'')?> value='no' name="accommodation_been"><?= lang("no"); ?>
										</label>
									</div>	
								</div>
							</div> 
							                                                                   
							<div class="form-group">
								<div class="form-group">
									<label><?= lang('if_yes_please_provide_date_and_duration_of_stay') ?></label>							
									<div class="controls">
										<input type="text" value='<?= $this->erp->hrsd($application->accommodation_date); ?>'  name="accommodation_date" class="form-control date input-sm" />
									</div>	
								</div>
							</div>	
							    
						</div>
						
						<?= form_close();?>
					</div>
				</div>
			</div>
		</div>
		
		<div id="content_10" class="tab-pane fade in">
			
			<div class="box">
				<div class="box-header">
					<h2 class="blue">
						<i class="fa-fw fa fa-barcode"></i>
						<?= lang("part_i") ?> - <?= lang("written_statement") ?>
					</h2>
				</div> 
				<div class="box-content">
					<div class="row">
						<?= form_open("asylum_seekers/add_written_statement/".$id); ?>	
						<div class="col-sm-12">
						<p><?= lang('when_answer_the_queation__') ?>
								<!--When answering the questions below, you should tell us everything about why you believe that you are in need of refugee protection. You should provide as much detail as possible, including any incidents of arrest, detention or harassment by authorities as well as the date the relevant events occurred.  It is important that you provide full and truthful answers to these questions.  If you need more space, please attach a page(s) with the details.--> 
							</p> 
							<div class="form-group">						
								<div class="controls">
									<label>
									<?= lang('why_did_you_leave_your_home_country') ?></label>	
									<textarea name="comment_leave_country"><?= $application->comment_leave_country ?></textarea>
								</div>
							</div>
							
							<div class="form-group">						
								<div class="controls">
									<label><?= lang('what_do_you_believe_may_happen_to_you_or_members_of_your_household_if_you_return_to_your_home_country_please_explain') ?></label>	
									<textarea name="comment_believe_happened"><?= $application->comment_believe_happened?></textarea>
								</div>
							</div> 
						</div>
						
						<?= form_close();?>
					</div>
				</div>
			</div>
		</div> 
		
		<div id="content_11" class="tab-pane fade in">
			<?= form_open("asylum_seekers/add_notification_refugee/".$id); ?>	
				<div class="box">
					<div class="box-header">
						<h2 class="blue">
							<i class="fa-fw fa fa-barcode"></i>
							<?= lang("notification") ?> 
						</h2>
					</div> 
					<div class="box-content">
						<div class="row"> 
							<div class="col-sm-12"> 
								<div class="form-group">						
									<div class="controls">
										<label><?= lang('notified_refugee') ?></label>	
										<textarea name="notification"><?= set_value('notification',$application->notification);?></textarea>
									</div>
								</div> 
							</div>
						</div>
					</div>
				</div>  
				<?= form_close();?>
		</div> 
		
	</div>
	
	<?= form_close(); ?>
	
<div class="clearfix"></div>
<script type="text/javascript">
	$(document).ready(function() {
		var brand = document.getElementById('logo-id');
		brand.className = 'attachment_upload';
		brand.onchange = function() {
			document.getElementById('fakeUploadLogo').value = this.value.substring(12);
		};
		// Source: http://stackoverflow.com/a/4459419/6396981
		function readURL(input) {
			if (input.files && input.files[0]) {
				var reader = new FileReader();
				
				reader.onload = function(e) {
					$('.img-preview').attr('src', e.target.result);
				};
				reader.readAsDataURL(input.files[0]);
			}
		}
		$("#logo-id").change(function() {
			readURL(this);
		});
		
		$("#upload").click(function(){
			var filename = "<?= site_url()?>/" + $("#webcam-preview img").attr("src");
			$('.img-preview').attr('src', filename);
			return false;
		});
		
	});
</script>
<script type="text/javascript">
	 
	
	$(function(){
		$('#case_prefix').on('change', function (e) { 	 
			 var selectValue = $(this).find(":selected").text();
			 var caseNo = $("#case_no").val();
			 $("#full_case").val(selectValue+caseNo);
		});
		
		$('#case_no').on('change', function (e) { 	 
			 var selectValue = $('#case_prefix').find(":selected").text();
			 var caseNo = $("#case_no").val();
			 $("#full_case").val(selectValue+caseNo);
		});
		
		
		$("#counseling_no").autocomplete({
            source: function (request, response) {
                var q = request.term;
				if($.isNumeric(q)){
					$.ajax({
						type: 'get',
						url: '<?= site_url('applications/suggestions'); ?>',
						dataType: "json",
						data: {
							q : q
						},
						success: function (data) {
							response(data);
						}
					});
				}
            },
            minLength: 1,
            autoFocus: false,
            delay: 200,
            response: function (event, ui) {
                if (ui.content[0].id == 0) {
                    $(this).removeClass('ui-autocomplete-loading');
                    $(this).removeClass('ui-autocomplete-loading');
                }
            },
            select: function (event, ui) {
                event.preventDefault();
                if (ui.item.id !== 0) {
					location.href = "<?= site_url("asylum_seekers/add_rsd_application") ?>/" + ui.item.id;
                } 
            }
        });
	});
	

	function activeTab(tab){
		$('.nav-tabs a[href="#content_'+tab+'"]').tab('show');
	};
	
</script>

<script type="text/javascript" src="<?= $assets ?>styles/helpers/components/js/bootstrap-formhelpers.js"></script>	
<link href="<?= $assets ?>styles/helpers/components/js/bootstrap-formhelpers.min.css" rel="stylesheet" />
<style type="text/css">
	.form-control input-sm, .thumbnail {
		border-radius: 2px;
	}
	.btn-danger {
		background-color: #B73333;
	}
	/* File Upload */
	.fake-shadow {
		box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
	}
	.fileUpload {
		position: relative;
		overflow: hidden;
	}
	.fileUpload #logo-id {
		position: absolute;
		top: 0;
		right: 0;
		margin: 0;
		padding: 0;
		font-size: 33px;
		cursor: pointer;
		opacity: 0;
		filter: alpha(opacity=0);
	}
	.img-preview {
		max-width: 100%;
		width:200px !important;
	}
</style>

<script>

	function showFunction(x){
		var n = x;
		if(n==1){ 
			$('#answer').show();
		}	
		if(n==2){ 
			$('#answer2').show();
		}	  
	} 

	function hideFunction(x){
		var n = x;
		if(n==1){ 
			$('#answer').hide();
		}	
		if(n==2){ 
			$('#answer2').hide();
		}	  
	} 

</script>