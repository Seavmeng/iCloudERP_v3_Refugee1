<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">				
			<button type="button" class="close" data-dismiss="modal">
				<span aria-hidden="true"><i class="fa fa-2x">&times;</i></span>
				<span class="sr-only"><?=lang('close');?></span>
			</button>
			<h4 class="modal-title" id="payModalLabel"><?= lang('add_country_transit'); ?>
				
			</h4>
		</div>
		
		<?php $attributes = array('id' => 'form1');?>
		
		<?php  echo form_open_multipart("asylum_seekers/add_country_transit/".$id, $attributes); ?> 
		
		<div class="modal-body"> 
			<div class="col-sm-12" >
				<div class="form-group">
					<?php echo lang('ប្រទេសដែលបានឆ្លងកាត់ ៖', 'ប្រទេសដែលបានឆ្លងកាត់ ៖'); ?>​
					<span class="red">*</span>
					<div class="controls">					
						<input type="text" class="form-control input-sm" name="country_transit"  id="country_transit" />
					</div>
				</div>  
				
				<div class="form-group">
					<?php echo lang('ចាប់ពី៖', 'ចាប់ពី៖'); ?>​
					<span class="red">*</span>
					<div class="controls">
						<input type="text" class="form-control input-sm date"  name="from"  id="from" />
					</div>
				</div> 
				<div class="form-group">
					<?php echo lang('ដល់៖', 'ដល់'); ?>​
					<span class="red">*</span>
					<div class="controls">
						<input type="text" class="form-control input-sm date "  name="to"  id="to" />
					</div>
				</div> 
				<div class="form-group">
					<?php echo lang('ឯកសារធ្វើដំណើរដែលបានប្រើ ៖', 'ឯកសារធ្វើដំណើរដែលបានប្រើ ៖'); ?>​
					<span class="red">*</span>
					<div class="controls">
						<input type="text" class="form-control input-sm "  name="travel_document_used"  id="travel_document_used" />
					</div>
				</div> 
				<div class="form-group">
					<?php echo lang('ភ្ជាប់ឯកសារ ៖', 'ភ្ជាប់ឯកសារ ៖'); ?>​
					<span class="red">*</span>
					<div class="controls">
						<input type="file" class="input-sm "  name="attachment"  id="attachment" />
					</div>
				</div>  
			</div>​ 
			<div class="clearfix"></div> 
		</div>
		
		 <div class="modal-footer">
			<?php echo form_submit('submit', lang('submit'), 'class="btn btn-primary save-data"'); ?>
		 </div>
		
		<?php  echo form_close(); ?>
	</div>
</div>

<script type="text/javascript">
	$(function(){
		$("select").select2();
	}); 
	$(".save-data").on('click',function(){
		var travel_document_used = $('#travel_document_used').val();
		var country_transit = $('#country_transit').val();
		var froms = $('#from').val();
		var to = $('#to').val();
		var qualification = $('#qualification').val();
		if(travel_document_used=='' || country_transit=='' || froms=='' || to == '' || qualification == ''){ 
			bootbox.alert('<?= lang("please_select_all");?>');
			return false;
		}  
	});

	
		
	
</script>
