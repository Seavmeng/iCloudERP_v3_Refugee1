<div class="box a4">
		
		<div class="box-content">

			<div class="row">			
			
				<?php $this->load->view($this->theme."applications/head-cover"); ?>
				
				<div class="col-sm-12 text-center" style="margin-top: -20px;">
					<h2 class="text-cover">ពាក្យសុំផ្តល់ការប្រឹក្សាបឋម</h2>
					<h3>COUNSELING APPLICATION FORM</h3>
				</div>
				
				<div class="col-sm-12 text-justify​ table-fill" style="margin-top:10px;">
					<table width="100%" class="set-border">
						<tr>
							<td style="width:28%;">នាមត្រកូល / Surname <br />
								<?= $result->lastname_kh; ?> <?php if($result->lastname != "") echo " / " . $result->lastname;?>
							</td>
							<td style="width:28%;">នាមខ្លួន / Given Name <br />
								<?= $result->firstname_kh; ?> <?php if($result->firstname != "") echo " / " . $result->firstname;?>
							</td>
							<td>ឈ្មោះហៅក្រៅ / Nickname<br />
								<?= $result->nickname_kh; ?>
							</td>
							<td width="100px;" rowspan="3" class="cover-photo"> 
								<?php
									if(!empty($result->photo) || !file_exists("assets/uploads/".$result->photo)){
										echo "<img src='assets/uploads/".$result->photo."' class='thumb' />";
									}
								?> 
							</td>
						</tr>
						<tr>
							<td>
								ភេទ / Sex: <br />
								<?= ucfirst($result->gender) ?> / <?= lang($result->gender) ?>
							</td>
							<td>ថ្ងៃខែឆ្នាំកំណើត / Date of Birth:<?= $this->erp->toKhmer($this->erp->hrsd($result->dob)); ?></td>
							<td>សញ្ជាតិ / Nationality: <br /><?= $result->nationality_kh; ?></td>
						</tr>
						<tr>
							<td>ជាតិពន្ធុ / Ethnic: <br />
								<?= $result->ethnicity_kh; ?>
								<?= $result->ethnicity; ?>
							</td>
							<td>សាសនា / Religion: <br />
								<?= $result->religion_kh; ?>/
								<?= $result->religion; ?>
							</td>
							<td>ឯកសារធ្វើដំណើរ / Travel documents: <br />
								<?= $result->travel_documents_kh; ?><hr/>
							</td>
						</tr>
					</table>
					
					<table  width="100%" style="border-top: none;" class="set-border">
						<tr>
							<td style="border-top: none;">កន្លែងផ្តល់ / Issued place:
								<?= $result->place_of_issued_kh; ?>
							</td>
							<td style="border-top: none;">ថ្ងៃផ្តល់ / Issued date: 
								<?= $this->erp->toKhmer($this->erp->hrsd($result->issued_date)); ?>
							</td>
							<td style="border-top: none;">ថ្ងៃផុតកំណត់ / Expired date: 
								<?= $this->erp->toKhmer($this->erp->hrsd($result->expired_date)); ?>
							</td>
						</tr>
						<tr>
							<td colspan="3" >ទីកន្លែងកំណើត / Place of Birth: 
								<?= $result->pob_kh; ?> / 
								<?= $result->pob; ?>
							</td>
						</tr>
						<tr>
							<td colspan="3" >
								អាសយដ្ឋាននៅប្រទេសដើម / Address of original country: 
								<?= $result->original_address_kh; ?>
								<?= !empty($village_o)?$village_o."," : ""; ?>
								<?= !empty($commune_o)?$commune_o."," : ""; ?>
								<?= !empty($district_o)?$district_o."," : ""; ?> 
								<?= !empty($province_o->native_name)?$province_o->native_name."," : ""; ?> 
								<?= !empty($country_o->native_name)?$country_o->native_name."," : ""; ?> 
								<?= !empty($state_o->native_name)?$state_o->native_name."," : ""; ?>  
							</td>
						</tr>
					</table>
					
					<table width="100%" style="border-top: none;" class="set-border">
						<tr>
							<td style="border-top: none; height: 70px; width: 30%;">
								មធ្យោបាយធ្វើដំណើរចូលកម្ពុជា  <br/>Means of transportation to Cambodia: <br/>
								<?= ucfirst($result->means_of_transportations) ?>/
								<?= lang($result->means_of_transportations) ?>
							</td>
							<td style="border-top: none;">
								ថ្ងៃមកដល់លើកទីមួយ  <br />
								First arrival date:   <br />
								<?= $this->erp->toKhmer($this->erp->hrsd($result->first_arrived_date)); ?>
							</td>
							<td style="border-top: none;">
								ថ្ងៃចូលចុងក្រោយ<br />
								Last date of entry:  <br />
								<?= $this->erp->toKhmer($this->erp->hrsd($result->last_entry_date)); ?>
							</td>
							<td style="border-top: none;">
								ចូលតាមច្រក<br />
								Point of entry: <br />
								<?= $result->port_of_entry_kh; ?>
							</td>
						</tr>
						<tr>
							<td colspan="4" >
								គោលបំណង / Purpose : <?= $result->purpose_of_entry_kh; ?>
							</td>
						</tr>
						<tr>
							<td colspan="4" style="text-align: center;">
								សមាជិកគ្រួសាររួមដំណើរជាមួយ / Family members travel with
							</td>
						</tr>
					</table> 
					<table width="100%" style="border-top: none;" class="set-border">
						<tr class="text-center"> 
							<td style="border-top: none;">ឈ្មោះ<br/>Name</td>
							<td style="border-top: none;">ភេទ<br/>Sex</td>
							<td style="border-top: none;">សញ្ជាតិ<br/>Nationality</td>
							<td style="border-top: none;">ទំនាក់ទំនង<br/>Relationship</td>
							<td style="border-top: none;">ថ្ងៃខែឆ្នាំកំណើត<br/>Date of Birth</td>
						</tr> 
						<?php foreach($result_members as $key => $member){ ?>
						<?php  $key = $key+1;?>
							<tr> 
								<td class="text-left" style="width: 22%;  white-space:nowrap;">
									<?= $member->lastname ?>&nbsp;<?= $member->firstname ?> / 
									<?= $member->lastname_kh ?>&nbsp;<?= $member->firstname_kh ?>
								</td>
								<td class="text-center" style="width: 5%;">
									<?= lang($member->gender) ?>
								</td>
								<td class="text-center" style="width: 5%;">
									<?= $member->nationality_kh ?>
								</td>
								<td class="text-center" width='5%'>
									<?= lang($member->relationship) ?>
								</td>
								<td class="text-center" style="width: 5%;">
									<?= $this->erp->toKhmer($this->erp->hrsd($member->dob)) ?>
								</td>
							</tr>
						<?php } ?> 
					</table>  
					<table width="100%" style="border-bottom: none;border-top: none;" class="set-border">
						<tr>
							<td style="border-top: none;"​​>
								អាសយដ្ឋានបច្ចុប្បន្នក្នុងព្រះរាជាណាចក្រកម្ពុជា / Current address in Kingdom of Cambodia:<br />
								<?= $result->address_kh; ?>
								<?= $commune->native_name; ?>, 
								<?= $district->native_name; ?>, 
								<?= $province->native_name; ?>, 
							</td>
						</tr> 
					</table>
						<?php  if($key>6){ ?>
						<div class="break_page"></div>
						<?php } ?> 
					<table width="100%"  style="border-top: none;"​​ class="set-border">
						<tr>  
							<td <?php echo ($key<=6)?"style='border-top:none;'":"" ?>  >
								ទូរស័ព្ទទំនាក់ទំនង / Contact number:
								<?= $this->erp->toKhmer($result->phone); ?>
							</td> 
						</tr>
					</table>
				</div> 
				
				<div class="div-right" style="margin-right: 25px; margin-top: 13px;">
						<table class="right-cover text-center">
							<tr>
								<td><?= $this->erp->footer_date_khmer('រាជធានីភ្នំពេញ');?> 
									<h4>ស្នាមមេដៃអ្នកដាក់ពាក្យសុំ </h4>
									<b>Applicant's finger print</b>
								</td>
							</tr>
						</table>
					</div> 
				</div> 
			</div>
		</div>
		<br/>
</div>

<style type="text/css"> 
	.break_page{
		page-break-after: always;
	} 
	@media print{
		.bblack{ background: white !important; }
		.modal-content, .box, 
		.modal-header{ border:none !important; }
		.table { border:1px solid #EEE !important; }
		.box-content{
			padding: 20px !important;
		}
	}
	.table-fill td{
		padding:5px;
	}
	#image-cover{
		width:125px;
		height:125px;
	}
	hr{
		padding:0px;
		margin:0px;
		border:none;
	}
	table.set-border {
		border-collapse: collapse;
		border: 1px solid black;
	}
	table.set-border td {
		border: 1px solid black;
		vertical-align: top;
	}
</style>