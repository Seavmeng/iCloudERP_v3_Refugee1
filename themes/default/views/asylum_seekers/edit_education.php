<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">				
			<button type="button" class="close" data-dismiss="modal">
				<span aria-hidden="true"><i class="fa fa-2x">&times;</i></span>
				<span class="sr-only"><?=lang('close');?></span>
			</button>
			<h4 class="modal-title" id="payModalLabel"><?= lang('edit_education'); ?>
				
			</h4>
		</div>
		
		<?php $attributes = array('id' => 'form1');?>
		
		<?php  echo form_open_multipart("asylum_seekers/edit_education/".$counseling_id.'/'.$education_id, $attributes); ?> 
		
		<div class="modal-body"> 
			<div class="col-sm-12" >  
				<div class="form-group">
					<?php echo lang('ឈ្មោះវិទ្យាស្ថាន៖', 'ឈ្មោះវិទ្យាស្ថាន៖'); ?>​
					<span class="red">*</span>
					<div class="controls">					
						<input type="text" value="<?= $result->name_institution?>" class="form-control input-sm" name="name_institution"  id="name_institution" />
					</div>
				</div> 
				<div class="form-group"> 
					<?php echo lang('ទីកន្លែង / ទីក្រុង៖', 'ទីកន្លែង / ទីក្រុង៖'); ?>​
					<span class="red">*</span>
					<div class="controls">
						<input type="text" value="<?= $result->country?>" class="form-control input-sm"  name="country"   id="country" />
					</div>
				</div>
				
				<div class="form-group">
					<?php echo lang('ចាប់ពី៖', 'ចាប់ពី៖'); ?>​
                    <?php
                        $fdate = explode("-", $result->from);
                        $tdate = explode("-", $result->to);

                    $f = $fdate[2]."/".$fdate[1]."/".$fdate[0];
                    $t = $tdate[2]."/".$tdate[1]."/".$tdate[0];
                    ?>
					<span class="red">*</span>
					<div class="controls">
						<input type="text" value="<?= $f; ?>" class="form-control input-sm date"  name="from"  id="from" />
					</div>
				</div> 
				<div class="form-group">
					<?php echo lang('ដល់៖', 'ដល់'); ?>​
					<span class="red">*</span>
					<div class="controls">
						<input type="text" value="<?= $t; ?>"  class="form-control input-sm date "  name="to"  id="to" />
					</div>
				</div> 
				<div class="form-group">
					<?php echo lang('បរិញ្ញាប័ត្រទទួលបាន', 'បរិញ្ញាប័ត្រទទួលបាន'); ?>​
					<span class="red">*</span>
					<div class="controls">
						<input type="text" value="<?=  $result->qualification ?>" class="form-control input-sm "  name="qualification"  id="qualification" />
					</div>
				</div> 
			</div>​
			
			<div class="clearfix"></div>
			
		</div>
		
		 <div class="modal-footer">
			<?php echo form_submit('submit', lang('submit'), 'class="btn btn-primary save-data"'); ?>
		 </div>
		
		<?php  echo form_close(); ?>
	</div>
</div>

<script type="text/javascript">
	$(function(){
		$("select").select2();
	}); 
	$(".save-data").on('click',function(){
		var name_institution = $('#name_institution').val();
		var country = $('#country').val();
		var froms = $('#from').val();
		var to = $('#to').val();
		var qualification = $('#qualification').val();
		if(name_institution=='' || country=='' || froms=='' || to == '' || qualification == ''){ 
			bootbox.alert('<?= lang("please_select_all");?>');
			return false;
		}  
	});

	
		
	
</script>
