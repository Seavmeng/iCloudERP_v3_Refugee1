<div class="box a4">
		
		<div class="box-content">

			<div class="row">			
			
				<?php $this->load->view($this->theme."applications/head-cover"); ?>
				
				<div class="col-sm-12 text-center" style="margin-top: -20px;">
					<h2 class="text-cover">ពាក្យសុំផ្តល់ការប្រឹក្សាបឋម</h2>
					<h3>COUNSELING APPLICATION FORM</h3>
				</div>
				
				<div class="col-sm-12 text-justify​ table-fill" style="margin-top:10px;">
					<table width="100%" class="set-border">
						<tr>
							<td style="height: 22px; width: 200px;">នាមត្រកូល / Surname <br />
								<?= $result->lastname_kh; ?> <?php if($result->lastname != "") echo " / " . $result->lastname;?>
							</td>
							<td style="width: 180px;">នាមខ្លួន / Given Name <br />
								<?= $result->firstname_kh; ?> <?php if($result->firstname != "") echo " / " . $result->firstname;?>
							</td>
							<td>ឈ្មោះហៅក្រៅ /  Nickname<br />
								<?= $result->nickname_kh; ?>
							</td>
							<td rowspan="3" width="125" height="125">
								<div id="image-cover">
									<?php
										if(!empty($result->photo) || !file_exists("assets/uploads/".$result->photo)){
											echo "<img src='assets/uploads/".$result->photo."' width='125' height='125' class='thumb' />";
										}
									?>
								</div>
							</td>
						</tr>
						<tr>
							<td>
								ភេទ/Sex: <br />
								<?= ucfirst($result->gender) ?> / <?= lang($result->gender) ?>
							</td>
							<td>ថ្ងៃខែឆ្នាំកំណើត/Date of Birth:<?= $this->erp->toKhmer($this->erp->hrsd($result->dob)); ?></td>
							<td>សញ្ជាតិ/Nationality: <br /><?= $result->nationality_kh; ?></td>
						</tr>
						<tr>
							<td>ជាតិពន្ធុ/Ethnic: <br />
								<?= $result->ethnicity_kh; ?>
								<?= $result->ethnicity; ?>
							</td>
							<td>សាសនា/Religion: <br />
								<?= $result->religion_kh; ?>/
								<?= $result->religion; ?>
							</td>
							<td>ឯកសារធ្វើដំណើរ/Travel documents: <br />
								<?= $result->travel_documents_kh; ?><hr/>
							</td>
						</tr>
					</table>
					
					<table  width="100%" style="border-top: none;" class="set-border">
						<tr>
							<td style="border-top: none; height: 50px;">កន្លែងផ្តល់/Issued place: <br />
								<?= $result->place_of_issued_kh; ?><hr/>
							</td>
							<td style="border-top: none;">ថ្ងៃផ្តល់/Issued date: <br />
								<?= $this->erp->toKhmer($this->erp->hrsd($result->issued_date)); ?><hr/>
							</td>
							<td style="border-top: none;">ថ្ងៃផុតកំណត់/Expired date: <br />
								<?= $this->erp->toKhmer($this->erp->hrsd($result->expired_date)); ?><hr/>
							</td>
						</tr>
						<tr>
							<td colspan="3" style="height: 50px;">ទីកន្លែងកំណើត/Place of Birth: <br />
								<?= $result->pob_kh; ?> / 
								<?= $result->pob; ?>
							</td>
						</tr>
						<tr>
							<td colspan="3"  style="height: 50px;">
								អាសយដ្ឋាននៅប្រទេសដើម/Address of original country: <br />
								<?= $result->original_address_kh; ?>
							</td>
						</tr>
					</table>
					
					<table width="100%" style="border-top: none;" class="set-border">
						<tr>
							<td style="border-top: none; height: 70px; width: 30%;">
								មធ្យោបាយធ្វើដំណើរចូលកម្ពុជា / Means of transportation to Cambodia: <br/>
								<?= ucfirst($result->means_of_transportations) ?>/
								<?= lang($result->means_of_transportations) ?>
							</td>
							<td style="border-top: none;">
								ថ្ងៃមកដល់លើកទីមួយ / <br />
								First arrival date:  <br />
								<?= $this->erp->toKhmer($this->erp->hrsd($result->first_arrived_date)); ?>
							</td>
							<td style="border-top: none;">
								ថ្ងៃចូលចុងក្រោយ/ <br />
								Last date of entry:  <br />
								<?= $this->erp->toKhmer($this->erp->hrsd($result->last_entry_date)); ?>
							</td>
							<td style="border-top: none;">
								ចូលតាមច្រក/ <br />
								Point of entry: <br />
								<?= $result->port_of_entry_kh; ?>
							</td>
						</tr>
						<tr>
							<td colspan="4" style="height: 50px;">
								គោលបំណង / Purpose:​<br />
								<?= $result->purpose_of_entry_kh; ?>
							</td>
						</tr>
						<tr>
							<td colspan="4" style="text-align: center;">
								សមាជិកគ្រួសាររួមដំណើរជាមួយ/Family members travel with
							</td>
						</tr>
					</table>
					
					<table width="100%" style="border-top: none;" class="set-border">
						<tr> 
							<td style="border-top: none;">ឈ្មោះ/Name</td>
							<td style="border-top: none;">ភេទ/Sex</td>
							<td style="border-top: none;">សញ្ជាតិ/Nationality</td>
							<td style="border-top: none;">ពាក់ព័ន្ធនិងអ្នកដាក់ពាក្យសុំ/ <br />Relationship with applicant</td>
							<td style="border-top: none;">ថ្ងៃខែឆ្នាំកំណើត/Date of Birth</td>
						</tr> 
						<?php foreach($result_members as $key => $member){ ?>
							<tr> 
								<td style="width: 22%;">
									<?= $member->lastname_kh ?>&nbsp;<?= $member->firstname_kh ?>
								</td>
								<td class="text-center" style="width: 5%;">
									<?= lang($member->gender) ?>
								</td>
								<td class="text-center" style="width: 15s%;">
									<?= $member->nationality_kh ?>
								</td>
								<td style="width: 30%;">
									<?= lang($member->relationship) ?>
								</td>
								<td style="width: 20%;">
									<?= $this->erp->toKhmer($this->erp->hrsd($member->dob)) ?>
								</td>
							</tr>
						<?php } ?>
						
					</table>
					
					<table width="100%" style="border-top: none;" class="set-border">
						<tr>
							<td style="border-top: none;"​​>
								អាសយដ្ឋានបច្ចុប្បន្នក្នុងព្រះរាជាណាចក្រកម្ពុជា / Current address in Kingdom of Cambodia:<br />
								<?= $province->native_name; ?>, 
								<?= $district->native_name; ?>, 
								<?= $commune->native_name; ?>, 
								<?= $result->address_kh; ?>
							</td>
						</tr>
						<tr>
							<td>
								ទូរស័ព្ទទំនាក់ទំនង/Contact number:
								<?= $this->erp->toKhmer($result->phone); ?>
							</td>
						</tr>
					</table>
				</div>
				
				<div class="div-right" style="margin-right: 25px; margin-top: 13px;">
						<table class="right-cover text-center">
							<tr>
								<td><?= $this->erp->footer_date_khmer('រាជធានីភ្នំពេញ');?> 
									<h4>ស្នាមមេដៃអ្នកដាក់ពាក្យសុំ </h4>
									<b>Applicant's finger print</b>
								</td>
							</tr>
						</table>
					</div> 
				</div> 
			</div>
		</div>
		<br/>
</div>

<style type="text/css">
	@media print{
		.bblack{ background: white !important; }
		.modal-content, .box, 
		.modal-header{ border:none !important; }
		.table { border:1px solid #EEE !important; }
	}
	.table-fill td{
		padding:5px;
	}
	#image-cover{
		width:125px;
		height:125px;
	}
	hr{
		padding:0px;
		margin:0px;
		border:none;
	}
	table.set-border {
		border-collapse: collapse;
		border: 1px solid black;
	}
	table.set-border td {
		border: 1px solid black;
		vertical-align: top;
	}
</style>