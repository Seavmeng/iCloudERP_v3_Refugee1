<div class='col-lg-3 director-txt'> 
	<div class='form-group'> 
		<?= lang("នាយកគ្រប់គ្រង៖", "នាយកគ្រប់គ្រង៖");?>
		<select   class="form-control text-cover director">
			<?php foreach($departments as $dep) {?>
			<option value="<?= $dep->department_en ?>"><b><?= $dep->department_kh?></b></option>
			<?php }?>
		</select>
	</div>
</div>
<div class="clearfix"></div>  
<div class="box a4" >
	<div class="box-content">
		<div class="row">
			<br/>
			<br/>
			<br/>
			<br/>
			<br/>
			<br/>
			<br/>
			<br/>
			<br/>
			<div class="col-sm-12 text-center" style="margin-top:0px;">
				<h2 class="text-cover">លិខិតអនុញ្ញាតស្នាក់នៅបណ្តោះអាសន្នសម្រាប់ជនសុំសិទ្ធិជ្រកកោន</h2>
				<h3>PRELIMINARY STAY PERMISSION FOR ASYLUM SEEKER</h3>
			</div>
			<div class="col-sm-12 col-md-12 col-lg-12" >
				<table width="100%"> 
					<tr>
						<td width="20">-</td>
						<td>
							លេខករណី / Case No: 
							<?= $this->erp->toKhmer($result->case_prefix.$result->case_no); ?>
						</td>
						<td rowspan="4" width="100 " class="cover-photo" >
							<img src="<?php echo base_url().'assets/uploads/document/preliminary_stay/'.$preliminary->photo; ?>">
						</td>
					</tr>
					<tr>
						<td>-</td>
						<td>
							 នាមត្រកូល និងនាមខ្លួន / Surname and Given Name: <?= $result->lastname_kh . " " . $result->firstname_kh; ?> 
							<?php if($result->lastname != null) echo " / " . $result->lastname . " " . $result->firstname;; ?>
						</td>
					</tr>
					 
					<tr>
						<td>-</td>
						<td>
							ភេទ / Sex: &nbsp;<?= $this->erp->genderToKhmer($result->gender)." / ".ucfirst($result->gender); ?>
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							- &nbsp;&nbsp;សញ្ជាតិ / Nationality: &nbsp;<?= $result->nationality_kh." / ".$result->nationality; ?>
						</td>
					</tr> 
					<tr>
						<td>-</td>
						<td>
							ថ្ងៃខែឆ្នាំកំណើត / Date of Birth:
							<?= $this->erp->toKhmer($this->erp->hrsd($result->dob)); ?>
						</td>
					</tr>
					<tr>
						<td>-</td>
						<td>
							ទីកន្លែងកំណើត/ Place of Birth:
							<?= $result->pob_kh; ?>
						</td>
					</tr>
					<tr>
						<td>-</td>
						<td colspan="2">
							ទីលំនៅបច្ចុប្បន្ន / Current Address:
							<?= $preliminary->address_kh ?> 
							<?= $this->asylum_seekers->getTagsById($preliminary->village); ?>
							<?= $this->asylum_seekers->getAllAddressKH($preliminary->commune,"commune"); ?>
							<?= $this->asylum_seekers->getAllAddressKH($preliminary->district,"district"); ?>
							<?= $this->asylum_seekers->getAllAddressKH($preliminary->province,"province"); ?>
							<?= $this->asylum_seekers->getAllAddressKH($preliminary->country,"country"); ?>
						</td>
					</tr>
				</table>
				<table width="100%"  style='white-space:nowrap;'>
					<tr> 
						<td width="20">-</td>
						<td style='width: 20px;'>
							មុខរបរបច្ចុប្បន្ន / Current Occupation : 
						</td>
						<td>
							<?= $preliminary->occupation?>
						</td>
						<td style='width: 20px;'>
							-&nbsp;&nbsp;កន្លែងធ្វើការ / Work Place : 
						</td>
						<td>
							<?= $preliminary->work_place?>&nbsp;
						</td>
						<td style='width: 20px;'>
							-&nbsp;&nbsp;និយោជក /  Employer : 
						</td>
						<td>
							<?= $preliminary->manager?>&nbsp;
						</td>
					</tr>
				</table>
				<table width="100%">
					<tr>
						<td width="20">-</td>
						<td>
							ទូរសព្ទ័ទំនាក់ទំនង / Contact Number:
							<?= $this->erp->toKhmer($preliminary->contact_number); ?>
						</td>
					</tr>
					<tr>
						<td>-</td>
						<td>
							សុពលភាពត្រឹមថ្ងៃទី / Valid Until:
							<?= $this->erp->toKhmer($this->erp->hrsd($preliminary->valid_until)); ?>
						</td>
					</tr>
					<tr>
						<td>-</td>
						<td>
							កាលបរិច្ឆេទចេញលិខិត / Date of Issued:
							<?= $this->erp->toKhmer($this->erp->hrsd($preliminary->created_date)); ?>
						</td>
					</tr>
				</table>
				<?php 
				    if(count($members) > 0){ ?>
					
					<table width="100%">
						<tr>
							<td width="20">-</td>
							<td colspan="2">សមាជិកគ្រួសារក្នុងបន្ទុក / Family Member(s):</td>
						</tr>
					</table>
					<div class="wrap-member" >
						<div class="text-center "> 
								<ul class="member-list"> 
									<?php										
										foreach($members as $k => $member){ 
											$x = $k+1;
											$blank_photo = base_url().'assets/uploads/male.png';
											$filename =  base_url().'assets/uploads/members/'.$member->photo;
											$blank = '<div class="photo​ text-center"><b>រូបថត</b><br/>4X6<br/>Photo</div>';
										?>
										<li>
											<div class="image-cover">
												<div class="crop">
													<img src= <?php echo $filename; ?> ​/>
												</div>
												<p><?= $member->lastname_kh . " " .$member->firstname_kh; ?> (<?= lang($member->relationship)?>)</p>
												<p><?= $member->lastname . " " .$member->firstname ; ?></p>
												<p><?= $this->erp->toKhmer($this->erp->hrsd($member->dob)); ?></p>
											</div>
										</li>
									<?php } ?> 
									<?php for($i=$x; $i<8; $i++){ ?>
										<li> 
											<div class="image-cover">
												<div class="crop">
													<img src="<?php echo $blank_photo; ?>" />
												</div>
												<p>&nbsp;</p>
												<p>&nbsp;</p>
												<p>&nbsp;</p> 
											</div>
										</li>
									<?php	} ?> 
								</ul> 
						</div>
					</div>
				<?php
					}
				?>
				<style type="text/css">
					.member-list{
						text-align:center;
					}
					.member-list li{
						display:inline-block;
						padding:15px 17px;
						//float:left;
					}
					.image-cover img{
						width:100px;		
						height:110px;
					}
					.image-cover p{
						padding:5px 0 0 0;
						margin:0px;
					}
					.wrap-member { 
						width: 90%; 
						margin: 0 auto;
						
					}
				</style>
				
				<table width="100%" style="margin-top:20px;" class="space-size">
					<tr>
						<td>
							លិខិតនេះផ្តល់ជូនអ្នកដាក់ពាក្យសុំសិទ្ធិជ្រកកោន និងសមាជិកគ្រួសារក្នុងបន្ទុកសម្រាប់ស្នាក់នៅបណ្តោះអាសន្នក្នុងព្រះរាជាណាចក្រកម្ពុជា ។<br/>
							This letter is issued for bearer and family members to use for temporary stay in Camobodia.<br/>
							<?=$address->location_kh?> <br/>
							<?=$address->location ?> 
						</td>
					</tr>
				</table>
				<div class="div-left"></div>
				<div class="div-right">
					<table class="text-center div-right" style="white-space:nowrap;">
						<tr>
							<td>
								<?= $this->erp->footer_date_khmer('រាជធានីភ្នំពេញ');?>  
								<h3 class="text-cover director_text_sub"></h3>
								<div class="text-cover director_text"  >
								ប្រធាននាយកដ្ឋានជនភៀសខ្លួន</div>
								<div class="director_text_en">Director of Refugee Department</div>
							</td>
						</tr>
					</table>
				</div>
	</div>
</div>
<script type='text/javascript'>
	$(".director").on('click',function(){ 
		var txt = $(".director option:selected").text();
		var txt_en = $(".director option:selected").val();
		if (txt=="រដ្ឋលេខាធិការ") {
			$(".director_text_sub").text('ជ.រដ្ឋមន្រ្តី');
		}else{
			$(".director_text_sub").text('');
		}
		$(".director_text").text(txt); 
		$(".director_text_en").text(txt_en); 
	});
</script>
<style type="text/css">
	@media print{
		.bblack{ background: white !important; }
		.modal-content, .box, 
		.modal-header{ border:none !important; }
		.director-txt {display:none; !important;}
		.box-content{
			padding: 20px !important;
		}
	}
	hr{
		padding:0px;
		margin:0px;
		border-top:none !important;
		border-bottom:dotted 1px !important;
	}
	table td{
		line-height:29px;		
		vertical-align:top;
	}
</style>