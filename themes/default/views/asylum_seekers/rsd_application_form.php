<?php  
	$icon_check='<span style="font-size:17px;"> &#x2611; </span>';
	$icon_uncheck='<span style="font-size:17px;"> &#x2610; </span>'; 
?>
<div class="box a4" >
	<div class="box-content"> 		
		<?php $this->load->view($this->theme."applications/head-cover_1"); ?> 
		<div class="col-sm-12 text-center" style="margin-bottom:10px;">
			<h2 class='title-header-form'>ទម្រង់ចុះឈ្មោះជនសុំសិទ្ធិជ្រកកោន</h2>
			<h2>ASYLUM SEEKER REGISTRATION FORM</h2>
		</div>
		<table width="100%" style="white-space: nowrap;" class="table-top" > 
			<tr> 
				<td colspan="4">
					<?= lang("ទីកន្លែងចុះឈ្មោះ៖ ") ?>​ <?= $office->office_kh; ?><br/>
					<?= lang("Place of Registration :"); ?> <?= $office->office; ?>
				</td>
				<td rowspan="5" class="cover-photo" style="width:110px; border:1px solid #000; text-align:center;">
					<?php 						
						$filename =  base_url().'assets/uploads/'.$result->photo;
						$blank = '<div class="photo​ text-center"><b>រូបថត</b><br/>4X6<br/>Photo</div>';
						if(isset($filename) == true)
						{
							echo "<img src= $filename />";
						}
						else{
							echo $blank;
						}
					?> 
				</td>
			 </tr>
			 
			<tr>
				<td colspan="4">
					<?= lang("លេខករណី :") ?> <?= $this->erp->toKhmer($result->case_prefix.$result->case_no); ?> &nbsp;<br/>
					<?= lang("Case No :") ?> <?= $result->case_prefix.$result->case_no; ?> &nbsp;
				</td>
			</tr>
			
			<tr>
				<td colspan="2">
					<?= lang("ថ្ងៃមកដល់ :") ?><?=  $this->erp->toKhmer($this->erp->hrsd($result->arrived_date)) ?><br/>
					<?= lang("Date of Arrival :") ?>
				</td>
				<td style="border-left:1px solid;" colspan="2">
					<?= lang("ថ្ងៃចុះបញ្ជី :") ?><?= $this->erp->toKhmer($this->erp->hrsd($result->registered_date)) ?><br/>
					<?= lang("Date of Registration :") ?>
				</td>
			</tr>
			
		</table>
		
		<table width="100%" style="white-space: nowrap; border-top:0px;" class="table-top"> 
			<tr>
				<td colspan="2">
					<?= lang("ករណីពាក់ព័ន្ធ :") ?> <?=  $this->erp->toKhmer($result->related_case) ?><br/>
					<?= lang("Related Cases :") ?>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<?= lang("តម្រូវការចាំបាច់ :") ?> <?= $result->special_need ?><br/>
					<?= lang("Physical Need :") ?>
				</td>
			</tr> 
		</table> 
			
	<div class="clearfix"></div> 
	<br/>
	 <!--=========================== Table Part A-->
	 
	 <table width="100%" style="white-space: nowrap;"  border='1' class='table-padding'>
			<tr>
				<td colspan='4' class='text-center title-head'>
					<h2>ផ្នែក​ <span class='title-header-form'>ក </span>- ជីវប្រវត្តិសង្ខេប</h2>
					<h2><?= lang("Part A – Basic Bio Data") ?></h2>
				</td>
			</tr>
			<tr>
				<td colspan='2' class='text-center'>
					នាមត្រកូល / Surname <br/>
					<?= $result->lastname_kh.' / '.$result->lastname; ?>
				</td> 
				<td colspan='2' class='text-center'>
					 នាមខ្លួន / Given Name<br/>
					 <?= $result->firstname_kh.'/ '.$result->firstname?>
				</td>
			</tr>
			<tr>
				<td colspan='4'>
				  <?= lang("ឈ្មោះផ្សេងទៀតដែលធ្លាប់ប្រើ :") ?>
				  <?= $result->othername_kh.' / '.$result->othername ?><br/>
				  <?= lang("Other Names Used :") ?>
				</td>
			</tr> 
		
		<tr>	
			<td colspan='2' width='50%'>
				<?= lang(" ឈ្មោះឪពុក :") ?> <?= $result->fathername_kh ?><br/>
				<?= lang("Father’s Name :") ?> <?= $result->fathername ?>
			</td> 
			<td colspan='2'>
				<?= lang("ឈ្មោះម្តាយ :") ?> <?= $result->mothername_kh ?><br/>
				<?= lang("Mother’s Name :") ?> <?= $result->mothername ?>
			</td> 
		</tr>
		<tr> 
			<td colspan='2'>
				<table width='100%'> 
					<td width='120'>
					 <?= lang("ភេទ :") ?><br/>
					 <?= lang("Sex :") ?>
					</td>
					<td>
						<div class='checkbox-gender'>
							<?php echo ($result->gender == 'male' ? $icon_check : $icon_uncheck);?> ប្រុស  <br/><span style=''> Male </span>
						</div>
						<div class='checkbox-gender'>
							<?php echo ($result->gender == 'female' ? $icon_check : $icon_uncheck);?> ស្រី  <br/><span style=''> Female </span>	
						</div>
					</td>
				</table> 
			</td> 
			<td colspan="2">
				<?= lang("សញ្ជាតិ :") ?><?= $result->nationality_kh?><br/>
				<?= lang("Nationality :") ?> <?= $result->nationality ?>
			</td>   
		</tr>  
		<tr> 
			<td colspan="4" style="border-right:none;">
				<?= lang("ថ្ងៃ ខែ ឆ្នាំកំណើត  (ថ្ងៃ/ខែ ឆ្នាំ) :") ?> <?=  $this->erp->toKhmer($this->erp->hrsd($result->dob)) ?> <br/>
				<?= lang("Date of Birth (dd/mm/yyyy) :") ?>
			</td>
		</tr> 
			<tr>
				<td colspan="4" style="border-right:none;">
					<?= lang("ទីកន្លែងកំណើត :")?> <?= $result->pob_kh ?><br/>
					<?= lang("Place Of Birth :") ?> <?= $result->pob ?>
				</td>
			</tr>
			<tr>
				<td colspan='4'> 
					<table width='100%'>
						<td>
							<div  style='float:left;'>
								<?= lang("ស្ថានភាពគ្រួសារ​​ :") ?><br/>
								<?= lang("Marital Status :") ?>
							</div>
						</td>
						<td>
							<div class='checkbox-family'>
								<?php echo  ($result->marital_status == 'single' ? $icon_check : $icon_uncheck);?> នៅលីវ​  <br/><span style='margin-left:1em;'> Single </span>	 
							</div>
							<div class='checkbox-family'>
								<?php echo  ($result->marital_status == 'married' ? $icon_check : $icon_uncheck);?> រៀបការ  <br/><span style='margin-left:1em;'> Married </span>	 
							</div>
							<div class='checkbox-family'>
								<?php echo  ($result->marital_status == 'engaged' ? $icon_check : $icon_uncheck);?>  ភ្ជាប់​ពាក្យ  <br/><span style='margin-left:1em;'> Engaged </span>	 
							</div>
							<div class='checkbox-family'>
								<?php echo  ($result->marital_status == 'separated' ? $icon_check : $icon_uncheck);?> មេម៉ាយ​ ឬ ពោះម៉ាយ <br/><span style='margin-left:1em;'> Separated </span>	 
							</div>
							<div class='checkbox-family'>
								 <?php echo  ($result->marital_status == 'divorced' ? $icon_check : $icon_uncheck);?> លែងលះ  <br/><span style='margin-left:1em;'> Divorced </span>	 
							</div> 
						</td>
					</table>  
				</td>
			</tr>
			<tr>
				<td colspan='4'>
					<?= lang("ឈ្មោះប្តី-ប្រពន្ធ  (បើមាន) :") ?> <?= $result->spouse_name_kh ?><br/>
					<?= lang("Spouse’s Name (<i>if applicable<i>) :") ?> <?= $result->spouse_name ?>
				</td> 
			</tr>
			<tr>
				<td colspan='2' width='50%'>
					<?= lang("សាសនា​​  :") ?> <?= $result->religion_kh ?><br/>
					<?= lang("Religion :") ?> <?= $result->religion ?>
				</td> 
				<td colspan='2'>
					<?= lang("ជាតិពន្ធ :") ?> <?= $result->ethnicity_kh ?><br/>
					<?= lang("Ethnicity :") ?> <?= $result->ethnicity ?>
				</td>  
			</tr>​
			<tr>
				<td width="100" colspan="4" style="border-top:0 !important;">
					<?= lang(" អាសយដ្ឋាន​ពេញលេញនៃទីកន្លែងស្នាក់នៅចុងក្រោយក្នុងប្រទេសកំណើតៈ") ?><br/>
					Full Address of Last Place of Residence in Home Country:<br/>
					<?= $province_o->native_name; ?>, 
					<?= $district_o->native_name; ?>, 
					<?= $commune_o->native_name; ?>, 
					<?= $result->address_o_kh ?><br/>
					<?= $province_o->name; ?>, 
					<?= $district_o->name; ?>, 
					<?= $commune_o->name; ?>, 
					<?= $result->address_o ?><br/>
				</td>
			</tr>
			<tr>
				<td colspan="4">
					<?= lang(" អាសយដ្ឋានបច្ចុប្បន្ន  និងលេខទូរស័ព្ទទំនាក់ទំនងៈ") ?><br/>
					<?= lang("Present Address & Contact Numbers :") ?><br/>
					<?= $province->native_name; ?>, 
					<?= $district->native_name; ?>, 
					<?= $commune->native_name; ?>, 
					<?= $result->address_kh ?> / 
					<?= $this->erp->toKhmer($result->phone) ?><br/>
					<?= $province->name; ?> , 
					<?= $district->name; ?>, 
					<?= $commune->name; ?> , 
					<?= $result->address ?> / 
					<?= $result->phone ?></b>
					&nbsp;<br/>
				</td>
			</tr>
	 </table>
	 <br/>
	 <!--=========================== Table Part B-->
	 <table class='table-data'>
			<tr>
				<td colspan='5' class='text-center title-head'>
					<h2><?= lang("ផ្នែក​ <span class='title-header-form'>ខ </span> - កម្រិតការសិក្សា") ?></h2>
					<h2><?= lang("Part B – Level of Education") ?></h2>
				</td>
			</tr>
			<tr>
				<td class='text-center' width='250'> 
					<p><?= lang("ឈ្មោះវិទ្យាស្ថាន") ?><br/>
						Name of Institution
					</p> 
				</td>
				<td class='text-center'> 
					<p><?= lang("ទីកន្លែង / ប្រទេស") ?><br/>
						Place/Country
					</p> 
				</td>
				<td class='text-center'> 
					<p><?= lang("ចាប់ពី ( ថ្ងៃ / ខែ / ឆ្នាំ )") ?><br/>
						From (dd/mm/yy)​
					</p> 
				</td>
				<td class='text-center'> 
					<p><?= lang("ដល់ (ថ្ងៃ / ខែ / ឆ្នាំ )") ?><br/>
						To ( dd/mm yy)​
					</p> 
				</td>
				<td class='text-center'> 
					<p><?= lang("សញ្ញាប័ត្រទទួលបាន") ?><br/>
						Qualification Obtained​
					</p> 
				</td>
			</tr>
			<?php $str='
					<tr class="text-center">
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td></td>
					</tr>​​​'; 
					$count=0;
					$min_row=4; 
			if(!empty($education)){
					foreach ($education as $edu){
                        $fdate = explode("-", $edu->from);
                        $tdate = explode("-", $edu->to);

                        $f = $fdate[2]."/".$fdate[1]."/".$fdate[0];
                        $t = $tdate[2]."/".$tdate[1]."/".$tdate[0];
			?>
						<tr class='text-center'>
							<td><?= $edu->name_institution; ?></td>
							<td><?= $edu->country; ?></td>
							<td><?=  $this->erp->toKhmer($f); ?></td>
							<td><?=  $this->erp->toKhmer($t); ?></td>
							<td><?= $edu->qualification; ?></td> 
						</tr>
				<?php 
						$count++;
					}
					if($count<$min_row){
						for($i=$count;$i<$min_row;$i++){/// loop display blank record.
							echo $str;
						} 
					}
			}else {
				?>
				<?php for($i=0;$i<4;$i++){
						echo $str;
						} 
			}?> 
	 </table>
	 <br/>
	 <!--=========================== Table Part C-->
	 <table class='table-data'>
		
			<tr>
				<td colspan='5' class='text-center title-head'>
					<h2><?= lang("ផ្នែក​ <span class='title-header-form'>គ </span> - មុខរបរ ( ថ្មីបំផុតក្នុងប្រទេសកំណើត )") ?></h2>
					<h2><?= lang("Part C – Occupation ( Most recent in home country )") ?></h2>
				</td>
			</tr>
			<tr>
				<td class='text-center'​ width="250"> 
					<p><?= lang("ឈ្មោះក្រុមហ៊ុន / ស្ថាប័ន") ?><br/>
						Name of Company / Institution
					</p> 
				</td>
				<td class='text-center'> 
					<p><?= lang("ទីកន្លែង / ប្រទេស") ?><br/>
						Place / Country 
					</p> 
				</td>
				<td class='text-center'> 
					<p><?= lang("ចាប់ពី ( ថ្ងៃ /ខែ / ឆ្នាំ )") ?><br/>
						From (dd/mm/yy)​
					</p> 
				</td>
				<td class='text-center'> 
					<p><?= lang("ដល់ ( ថ្ងៃ / ខែ / ឆ្នាំ )") ?><br/>
						To ( dd/mm/yy)​
					</p> 
				</td>
				<td class='text-center'> 
					<p><?= lang("មុខងារ") ?><br/>
						Job Title
					</p> 
				</td>
			</tr> 
			<?php $str='
					<tr class="text-center">
						<td></td>
						<td></td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td></td>
					</tr>​​​'; 
					$count=0;
					$min_row=4;
			if(!empty($occupation)){
					foreach ($occupation as $occ){ 
			?>
						<tr class='text-center'>
							<td><?= $occ->name_employer; ?></td>
							<td><?= $occ->country; ?></td>
							<td><?=  $this->erp->toKhmer($this->erp->hrsd($occ->from)); ?></td> 
							<td><?=  $this->erp->toKhmer($this->erp->hrsd($occ->to)); ?></td> 
							<td><?= $occ->job_title; ?></td> 
						</tr>
				<?php 
						$count++;
					}
					if($count<$min_row){
						for($i=$count;$i<$min_row;$i++){/// loop display blank record.
							echo $str;
						} 
					}
			}else {
				?>
				<?php for($i=0;$i<4;$i++){
						echo $str;
						} 
			}?>
	 </table>
	<div class='clearfix'></div>
	  <!--=========================== Table Part D-->
	 <table class='table-data' border='1'>
			<tr>
				<td colspan='5' class='text-center title-head'>
					<h2><?= lang("ផ្នែក​ <span class='title-header-form'>ឃ </span> - ឯកសារបញ្ជាក់អត្តសញ្ញាណ / ឯកសារផ្សេងៗដែលបានផ្តល់") ?></h2>
					<h2><?= lang("Part D – Identification Documents / Other Documents Provided") ?></h2>
				</td>
			</tr>  
			<tr>
				<td class='text-center' width="250"> 
					<p><?= lang("ប្រភេទឯកសារ") ?><br/>
						Document Type 
					</p> 
				</td>
				<td class='text-center'> 
					<p><?= lang("កន្លែងផ្តល់") ?><br/>
						Issued Place 
					</p> 
				</td>
				<td class='text-center'> 
					<p><?= lang("ផ្តល់​ ថ្ងៃ / ខែ / ឆ្នាំ ") ?><br/>
						Issued Date
						(dd/mm/yyyy)
					</p> 
				</td>
				<td class='text-center'> 
					<p><?= lang("ផុតកំណត់​ ថ្ងៃ / ខែ / ឆ្នាំ ") ?><br/>
						Expired Date
						(dd/mm/yyyy)
					</p> 
				</td>
				<td class='text-center'> 
					<p><?= lang("ផ្តល់ច្បាប់ដើមឬ ?") ?><br/>
						Original
						Provided ?
					</p> 
				</td>
			</tr>
			<?php $str=' 
					<tr class="text-center">
						<td></td>
						<td></td>
						<td>&nbsp;</td>
						<td></td>
						<td>
							<span style="font-size:17px;"> &#x2610; </span>Yes <span style="font-size:17px;"> &#x2610; </span>No
						</td>
					</tr>​​​'; 
					$count=0;
					$min_row=4;
			if(!empty($identification_documents)){
				foreach ($identification_documents as $iden_doc){ 
			?>
						<tr class='text-center'>
							<td><?= $iden_doc->document_type; ?></td>
							<td><?= $iden_doc->issued_place; ?></td>
							<td><?=  $this->erp->toKhmer($this->erp->hrsd($iden_doc->issued_date)); ?></td> 
							<td><?=  $this->erp->toKhmer($this->erp->hrsd($iden_doc->expired_date)); ?></td>  
							<td>
								<?php echo ($iden_doc->original_provided == 'yes' ? $icon_check : $icon_uncheck);?> Yes
								
								<?php echo ($iden_doc->original_provided == 'no' ? $icon_check : $icon_uncheck);?> No
							</td> 
						</tr>
				<?php 
						$count++;
					}
					if($count<$min_row){
						for($i=$count;$i<$min_row;$i++){/// loop display blank record.
							echo $str;
						} 
					}
			}else {
				?>
				<?php for($i=0;$i<4;$i++){
						echo $str;
						} 
			}?>  
	 </table>
	 <div class='clearfix'></div>
	 
	 <table class="table-data"> 
		<tr>
			<td>
				<b>ឯកសារដែលទទួលបានដោយខុសច្បាប់ៈ</b><br/>
				Document Obtained​ Illegally : <br/>
				ប្រសិនបើឯកសារណាមួយដែលចុះបញ្ជីខាងលើទទួលបានដោយមិនស្របច្បាប់    សូមពន្យល់ពីវិធីដែលទទួលបានឯកសារទាំងនេះ។<br/>
				If any of the documents listed above were illegally issued, please explain how they were obtained
				<br/>
				<?= $application->comment_document_obtained_illegally?>
			</td>
		</tr>
		<tr>
			<td>
				<b>ឯកសារដែលបាត់បង់ៈ</b><br/>
				Missing Documents : <br/>
				ប្រសិនបើអ្នកបាត់ឯកសារអត្តសញ្ញាណ​  ឬឯកសារផ្សេងៗទៀតដែលទាក់ទងនឹងអំណះអំណាងរបស់អ្នក​  សូមពន្យល់ពីមូលហេតុ។<br/>
				If you are missing identity documents or other documents that are relevant to your claim, please
				explain why you do not have these documents. <br/>
				<?= $application->document_missing?>
				<br/>
				ប្រសិនបើអ្នកបាត់ឯកសារ  តើអ្នកជឿថាអ្នកអាចទទួលបាន​ឯកសារទាំងនេះឡើងវិញទេនៅពេលអនាគត ? បើមិនអាចសូមពន្យល់ពីមូលហេតុ។<br/>
				If you are missing documents, will you be able to obtain these documents in the future? If not, please
				explain why. 
				<b><?php echo $application->comment_document_mission?></b>			
			</td>
		</tr>
	 </table>
	 <div class='clearfix'></div>
	 <!--=========================== Table Part E-->
		<?php     
			foreach($registration_history as $reg) {
				$place = $reg->registered_place;
				$registered_case = $reg->case_no;
				$registered_date =$reg->registered_date;
				$applied_place =$reg->applied_place;
				$applied_date =$reg->applied_date;
				$decision_status =$reg->decision_status;
			} 
		?>
	<br/>
	 <table class='table-data'>
		<tr>
			<td colspan='3' class='text-center title-head'>
				<h2><?= lang("ផ្នែក​  <span class='title-header-form'>ង</span>- ប្រវត្តិនៃការចុះឈ្មោះជនសុំសិទ្ធិជ្រកកោន") ?></h2>
					<h2><?= lang("Part E – Applicant’s Registration History") ?></h2>
			</td>
		</tr>
		<tr> 
			<td colspan='3'>
				<div  style='float:left;'>
						 <p>1.<?= lang("តើអ្នកធ្លាប់បានចុះឈ្មោះជាជនភៀសខ្លួនពីមុនដែរឬទេ ? ") ?><br/>
						Have you ever been registered for Refugee Before ? </p> 
				</div>					
				<div class='checkbox-family'>
					<?php echo ($application->registered_refugee_department=='yes'? $icon_check : $icon_uncheck);?>ធ្លាប់  <br/><span style='margin-left:1em;'> Yes </span>	 
				</div>
				<div class='checkbox-family'>
					<?php echo ($application->registered_refugee_department=='no'? $icon_check : $icon_uncheck);?>មិនធ្លាប់  <br/><span style='margin-left:1em;'> No </span>	 
				</div> 
			</td>
		</tr>
		<tr>
			<td style='vertical-align: top;' rowspan='2'>
				ប្រសិនបើធ្លាប់  តើអ្នកចុះឈ្មោះនៅទីណា ?​<br/> 
				If yes, where were you registered ?<br/>
				-​<?= $application->registered_refugee_place?>    
			</td>
			<td style='vertical-align: top;' rowspan='2'>
				លេខករណីៈ <b><?= $registered_case;?></b><br/>
				Case N<sup>o</sup>:<br/> 
				​<?=$application->registered_refugee_no?>  
			</td>
			<td style='border-bottom:none;'>
				ថ្ងៃខែឆ្នាំនៃការចុះឈ្មោះ<br/>
				Date of registration : (dd/mm/yyyy) :
				 
			</td>
		</tr>
		<tr>
			<td style='border-top:none'><?=  $this->erp->toKhmer($application->registered_refugee_date) ?></td>
		</tr>
		<tr>
			<td colspan='3' style='border-bottom:none;'>
				<div  style='float:left;'>
					<p>2.<?= lang("តើអ្នកធ្លាប់ដាក់ពាក្យសុំការការពារជាជនភៀសខ្លួនជាមួយ UNHCR ឬរដ្ឋាភិបាលណាមួយដែរទេ ? ") ?><br/>
					 Have you ever been applied for refugee protection with UNHCR or any Government ? ​ <br/> 
					</p>  
				</div>					
				<div class='checkbox-family'>
					<?php echo ($application->applied_refugee_protection=='yes'? $icon_check : $icon_uncheck);?>ធ្លាប់  <br/><span style='margin-left:1em;'>Yes</span>	 
				</div>
				<div class='checkbox-family'>
					<?php echo ($application->applied_refugee_protection=='no'? $icon_check : $icon_uncheck);?>មិនធ្លាប់  <br/><span style='margin-left:1em;'>No</span>	 
				</div> 
			</td>
		</tr>
		<tr>
			<td colspan='3' style='border-top:none;'>
				<?= lang("ប្រសិនបើធ្លាប់ សូមផ្តល់ព័ត៌មានលម្អិត ") ?>  <br/>
					 If Yes,​ Provide details
			</td>
		</tr>
		<tr>
			<td>
				នៅទីណា ?<br/>
				Where ?<br/>
				<b><?= $application->applied_refugee_place ?></b>&nbsp;  
			</td>
			<td>
				នៅពេលណា ?<br/>
				When ?<br/>&nbsp;
				<?= ($application->applied_refugee_date>0)?$this->erp->toKhmer($this->erp->hrsd($application->applied_refugee_date)):'' ?>   
			</td>
			<td>
				ឋានៈបានទទួលៈ<br/>
				Status obtained :<br/>
				<b><?= lang($application->recognition_refugee);?></b>&nbsp;
			</td>
		</tr>
	 </table>
	 <div class='clearfix'></div>
	  <!--=========================== Table Part F-->
	 <table class='table-data'> 
			<tr>
				<td colspan='5' class='text-center title-head'>
					<h2><?= lang("ផ្នែក  <span class='title-header-form'>ច</span> - គ្រួសារ និងសមាជិកក្នុងបន្ទុកដែលរួមដំណើរជាមួយអ្នកដាក់ពាក្យសុំ") ?></h2>
					<h2><?= lang("Part F–Family Members and Dependants Accompanying the Applicants") ?></h2>
				</td>
			</tr> 
			<tr>
				<td class='text-center' width='260'> 
					<p><?= lang("ឈ្មោះពេញ") ?><br/>
						Full Name
					</p> 
				</td>
				<td class='text-center' > 
					<p><?= lang("លេខករណី") ?><br/>
						Individual CaseN<sup>o</sup> 
					</p> 
				</td>
				<td class='text-center' width='150'> 
					<p><?= lang(" ត្រូវជាអ្វីជាមួយអ្នកដាក់ពាក្យសុំ ") ?>
						Relationship
						to Applicant​
					</p> 
				</td>
				<td class='text-center' width='50'> 
					<p><?= lang("ភេទ") ?><br/>
						Sex​
					</p> 
				</td>
				<td class='text-center' width='160'> 
					<p><?= lang("ថ្ងៃខែឆ្នាំកំណើត") ?><br/>
						Date of Birth 
						(dd/mm/yyyy)
					</p> 
				</td>
			</tr>
			<?php $str='
					<tr class="text-center">
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td>&nbsp;</td> 
					</tr>​​​'; 
					$count=0;
					$min_row=4;
				
			if(!empty($family_member)){
				foreach ($family_member as $fam_mem){  
			?>
						<tr class='text-center'>
							<td>
								<?= $fam_mem->lastname.' '.$fam_mem->firstname; ?> / 
								<?= $fam_mem->lastname_kh.' '.$fam_mem->firstname_kh; ?>		
							</td>
							<td><?= $fam_mem->individual_case_no; ?></td>
							<td><?= lang($fam_mem->relationship); ?></td>
							<td><?= lang($fam_mem->gender); ?></td>
							<td><?=  $this->erp->toKhmer($this->erp->hrsd($fam_mem->dob)); ?></td>    
						</tr>
				<?php 
						$count++;
					}
					if($count<$min_row){
						for($i=$count;$i<$min_row;$i++){/// loop display blank record.
							echo $str;
						} 
					}
			}else {
				?>
				<?php for($i=0;$i<4;$i++){
						echo $str;
						} 
			}?>  
	 </table>
	 <div class='clearfix'></div>
	 <!-- <div class="break_page"></div> -->
	  <!--=========================== Table Part G--> 
	 <table class='table-data'>​ 
		<thead>
			<tr>
				<td colspan='5' class='text-center title-head'>
					<h2><?= lang("ផ្នែក  <span class='title-header-form'>ឆ </span> - គ្រួសារ និងសមាជិកក្នុងបន្ទុករស់នៅក្នុងប្រទេសកំណើត ដែលមិនបានរួមដំណើរជាមួយអ្នកដាក់ពាក្យសុំ") ?></h2>
					<h2><?= lang("Part G – Non- Accompanying​ Applicant Family Members and Dependants Living Inside Home Country") ?></h2>
				</td>
			</tr>
		</thead>
			<tr>
				<td class='text-center' width='300'> 
					<p><?= lang("ឈ្មោះពេញ") ?><br/>
						Full Name
					</p> 
				</td>​
				<td class='text-center' width='150'> 
					<p><?= lang(" ត្រូវជាអ្វីជាមួយអ្នក ដាក់ពាក្យសុំ ") ?><br/>
						Relationship
						to Applicant​
					</p> 
				</td>
				<td class='text-center' width='50'> 
					<p><?= lang("ភេទ") ?><br/>
						Sex​
					</p> 
				</td>
				<td class='text-center text-top'  > 
					<p><?= lang("ថ្ងៃខែឆ្នាំកំណើត") ?><br/>
						Date of Birth 
						(dd/mm/yyyy)
					</p> 
				</td>
				<td class='text-center ' width='300' > 
					<p><?= lang("អាសយដ្ឋាន")?><br/>
						Address
					</p> 
				</td>
			</tr> 
				<?php $str='
					<tr class="text-center">
						<td></td>
						<td></td>
						<td></td>  
						<td>&nbsp;</td>
						<td></td>  
					</tr>​​​'; 
					$count=0;
					$min_row=4;
			if(!empty($family_member_none_accom)){
				foreach ($family_member_none_accom as $fam_none_acc){  
			?>
					<tr class='text-center'>
						<td>
							<?= $fam_none_acc->lastname.' '. $fam_none_acc->firstname; ?> / 
							<?= $fam_none_acc->lastname_kh.' '. $fam_none_acc->firstname_kh; ?>		
						</td>
						<td><?= $this->erp->toKhmer($fam_none_acc->relationship_khm); ?></td>
						<td><?= $this->erp->genderToKhmer($fam_none_acc->gender); ?></td> 
						<td><?=  $this->erp->toKhmer($this->erp->hrsd($fam_none_acc->dob)); ?></td>  
						<td>
							<?= $fam_none_acc->address_kh ?>,
							<?= $this->asylum_seekers->getTagsById($fam_none_acc->village); ?>,
							<?= $this->asylum_seekers->getAllAddressKH($fam_none_acc->communce,"commune"); ?>
							<?= $this->asylum_seekers->getAllAddressKH($fam_none_acc->district,"district"); ?>
							<?= $this->asylum_seekers->getAllAddressKH($fam_none_acc->province,"province"); ?>
							<?= $this->asylum_seekers->getAllAddressKH($fam_none_acc->country,"country"); ?>
						</td> 							
					</tr>
				<?php 
						$count++;
					}
					if($count<$min_row){
						for($i=$count;$i<$min_row;$i++){/// loop display blank record.
							echo $str;
						} 
					}
			}else {
				?>
				<?php for($i=0;$i<4;$i++){
						echo $str;
						} 
			}?> 
		
	 </table> 
	 <div class='clearfix'></div>
	  <!--===============  End Table Part G--> 
	  
	  <!--=========================== Table Part H--> 
	 <table class='table-data'>​ 
		<thead>
			<tr>
				<td colspan='7' class='text-center title-head' style='white-space:nowrap;'​>
					<h2><?= lang("ផ្នែក  <span class='title-header-form'> ជ </span> - គ្រួសារ និងសមាជិកក្នុងបន្ទុករស់នៅក្រៅប្រទេសកំណើត ដែលមិនបានរួមដំណើរជាមួយអ្នកដាក់ពាក្យសុំ") ?></h2>
					<h2><?= lang("Part H – Non- Accompanying Applicant Family Members and Dependants Living Outside Home Country") ?></h2>
				</td>
			</tr>
		</thead>
			<tr>
				<td class='text-center' width="240"> 
					<p><?= lang("ឈ្មោះពេញ") ?><br/>
						Full Name
					</p> 
				</td>​
				<td class='text-center' width="80" > 
					<p><?= lang(" ត្រូវជាអ្វីជាមួយអ្នកដាក់ពាក្យសុំ ") ?><br/>
						Relationship
						to Applicant​
					</p> 
				</td>
				<td class='text-center'> 
					<p><?= lang("សញ្ជាតិ") ?><br/>
						Citizenship
					</p> 
				</td>
				<td class='text-center' width='50'> 
					<p><?= lang("ភេទ") ?><br/>
						Sex​
					</p> 
				</td>
				<td class='text-center'  width="80"> 
					<p><?= lang("ថ្ងៃខែឆ្នាំកំណើត") ?><br/>
						Date of Birth
						(dd/mm/yyyy)
					</p> 
				</td>
				<td class='text-center'  width="80"> 
					<p><?= lang("ឋានៈនៅទីនោះ")?><br/>
						Status
					</p> 
				</td>
				<td class='text-center'> 
					<p><?= lang("អាសយដ្ឋាន")?><br/>
						Address
					</p> 
				</td>
			</tr> 
				<?php $str='
					<tr class="text-center">
						<td></td>
						<td></td>
						<td></td>
						<td></td>						
						<td>&nbsp;</td>
						<td></td>  
						<td></td>  
					</tr>​​​'; 
					$count=0;
					$min_row=4; 
			if(!empty($family_member_none_accom_outsite_country)){
				foreach ($family_member_none_accom_outsite_country as $row){  
			?>
					<tr class='text-center'>
						<td>
							<?= $row->lastname.' '.$row->firstname; ?> / 
							<?= $row->lastname_kh.' '.$row->firstname_kh; ?>
						</td>
						<td><?= $row->relationship; ?></td> 
						<td><?= $row->ethnicity; ?></td>
						<td><?= $this->erp->genderToKhmer($row->gender); ?></td>
						<td><?= $this->erp->toKhmer($this->erp->hrsd($row->dob)); ?></td>  
						<td><?= $row->status; ?></td> 							
						<td> 
							<?= $row->address_kh ?>,
							<?= $this->asylum_seekers->getTagsById($row->village); ?>,
							<?= $this->asylum_seekers->getAllAddressKH($row->communce,"commune"); ?>
							<?= $this->asylum_seekers->getAllAddressKH($row->district,"district"); ?> 
							<?= $this->asylum_seekers->getAllAddressKH($row->province,"province"); ?>
							<?= $this->asylum_seekers->getAllAddressKH($row->country,"country"); ?> 
						</td> 	 							
					</tr>
				<?php 
						$count++;
					}
					if($count<$min_row){
						for($i=$count;$i<$min_row;$i++){/// loop display blank record.
							echo $str;
						} 
					}
			}else {
				?>
				<?php for($i=0;$i<4;$i++){
						echo $str;
						} 
			}?> 
	 </table> 
	 <div class='clearfix'></div>
	  <!--===============  End Table Part G--> 
	 ​ <!--=========================== Table Part I-->
	 <?php  			 
	  foreach( $family_member_H as $value){
		$departure_date = $value->departured_date;
		$means_of_travel_out = $value->mean_travel_out;
		$exit_point = $value->exit_point;
	   }
	?>
	 <table class='table-data' style="white-space:nowrap;">​
		<thead>
			<tr>
				<td colspan='2' class='text-center title-head'>
					<h2><?= lang("ផ្នែក  <span class='title-header-form'>ឈ </span>​ - ព័ត៌មានលម្អិតអំពីការធ្វើដំណើរ"); ?></h2>
					<h2><?= lang("Part I – Travel​ Details") ?></h2>
				</td>
			</tr>
		</thead>
			<tr>
				<td width='300'>
					1.<?= lang("ថ្ងៃធ្វើដំណើរចាកចេញពីប្រទេសកំណើត ( ថ្ងៃ / ខែ / ឆ្នាំ )​ : ");?><br/>
					&nbsp;&nbsp;&nbsp;&nbsp;<?= lang("Date of Departure from Home Country ( dd / mm / yyyy ) : ");?>
				</td>
				<td>
					<?= ($application->departured_home_country>0)?$this->erp->toKhmer($this->erp->hrsd($application->departured_home_country)):'' ?><br/>
				</td>
			</tr>
			<tr>
				<td width='300' >
					2.<?= lang("មធ្យោបាយធ្វើដំណើរចេញពីប្រទេសកំណើត : ") ?><br/>
					&nbsp;&nbsp;&nbsp;&nbsp;<?= lang("Means of Travel out of Home Country : ") ?>
				</td>
				<td>
					<?= $application->means_home_country?><br/>					
				</td>
			</tr>
			<tr>
				<td width='300' >
					3.<?= lang("​ច្រកចេញពីប្រទេសកំណើត ៈ ") ?><br/>
					&nbsp;&nbsp;&nbsp;&nbsp;<?= lang("Exit Point from Home Country : ") ?>
				</td>
				<td>
					<?= $application->exit_point_home_country ?><br/>					
				</td>
			</tr>
	</table>
	<div class='clearfix'></div>
	<table class='table-data' style="white-space:nowrap;">​
		<tr>
			<td rowspan='2'>
				4.<?= lang("​ប្រទេសឆ្លងកាត់") ?><br/>
				 &nbsp;&nbsp;&nbsp;<?= lang("Countries of Transit : ") ?>
			</td>
			<td colspan='2' class='text-center'>
				<?= lang("រយះពេល  Period") ?>
			</td>
			<td rowspan='2' class='text-center'>
				<?= lang("ឯកសារប្រើសម្រាប់ធ្វើដំណើរ") ?><br/> 
				<?= lang("Travel Document Used") ?>
			</td>
		</tr>
		<tr> 
			<td class='text-center'>
				ចាប់ពី   From <br/>( dd / mm / yyyy )
			</td>
			<td class='text-center'>
				ដល់  To <br/>( dd / mm / yyyy )
			</td>
		</tr> 
		<?php $str="<tr class='text-center'>
						<td></td> 
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td></td> 
					</tr>"; 
					
				$count=0;
				$min_row=4; 
		if(!empty($country_transit)){
			 foreach($country_transit as $country){  
		?>
					<tr class='text-center'>
						<td><?= $country->country_transit?></td> 
						<td><?= ($country->from>0)?$this->erp->hrsd($country->from):''; ?></td> 
						<td><?= ($country->to>0)?$this->erp->hrsd($country->to):''; ?></td> 
						<td><?= $country->travel_document_used?></td>
					</tr>
			<?php 
					$count++;
				}
				if($count<$min_row){
					for($i=$count;$i<$min_row;$i++){/// loop display blank record.
						echo $str;
					} 
				}
		}else {
			?>
			<?php for($i=0;$i<4;$i++){
					echo $str;
					} 
		}?> 
	</table>
	 <div class='clearfix'></div>
	 <table class='table-data' style="white-space:nowrap;">
			<tr>
				<td width='300' style="border-top:none;">
					5.<?= lang("ចូលកម្ពុជាតាមច្រក ៈ ");?><br/>
					&nbsp;&nbsp;&nbsp;&nbsp;<?= lang("Entry Point in Cambodia : ");?>
				</td>
				<td style="border-top:none;">
					<?= $application->entry_point_country ?>
					&nbsp;<br/>
				</td>
			</tr>
			<tr>
				<td width='300'>
					6.<?= lang("ថ្ងៃខែឆ្នាំមកដល់ប្រទេសកម្ពុជា ៈ ");?><br/>
					&nbsp;&nbsp;&nbsp;&nbsp;<?= lang("Date of arrival in Cambodia ( dd / mm / yyyy ) : ");?>
				</td>
				<td>
					<?=  ($application->arrived_country>0)?$this->erp->toKhmer($this->erp->hrsd($application->arrived_country)):'' ?> 
				</td>
			</tr>
			<tr>
				<td width='300'>
					7.<?= lang("កាលពីមុន តើអ្នកធ្លាប់មកកាន់ប្រទេសកម្ពុជាឬទេ ? ");?><br/>
					&nbsp;&nbsp;&nbsp;&nbsp;<?= lang("Have you been Cambodia before ?​");?><br/>
				</td>
				<td>
					<div class='checkbox-family'>
						 <?= ($application->accommodation_been=='yes' ? $icon_check : $icon_uncheck);?>ធ្លាប់  <br/><span style='margin-left:1em;'> Yes </span>	 
					</div>
					<div class='checkbox-family'>
						 <?= ($application->accommodation_been=='no' ? $icon_check : $icon_uncheck);?>មិនធ្លាប់  <br/><span style='margin-left:1em;'> No </span>	 
					</div>
				</td>
			</tr>
			<tr>
				<td width='300'>
					8.<?= lang("ប្រសិនបើជាធ្លាប់​  សូមផ្តល់ថ្ងៃខែឆ្នាំ  និងរយះពេលស្នាក់នៅ");?><br/>
					&nbsp;&nbsp;&nbsp;&nbsp;<?= lang("If <b>Yes,</b>​​ please provide date and duration of stay :​ ");?>
				</td>
				<td>
					<?= ($application->accommodation_date>0)?$this->erp->toKhmer($this->erp->hrsd($application->accommodation_date)):''; ?>
					<br/> <?= $application->accommodation_duration; ?>
					&nbsp;<br/>
				</td>
			</tr>
	 </table>
	 <div class='clearfix'></div>
	 ​<!--=========================== Table Part I-->
	 <table class='table-data' cellspacing='0' width='100%'>​
			<tr>
				<td class='text-center title-head'>
					<h2><?= lang("ផ្នែក  <span class='title-header-form'> ញ </span>​ - ការថ្លែងប្រាប់ជាលាយលក្ខណ៍អក្សរ"); ?></h2>
					<h2><?= lang("Part J – Written Statement") ?></h2>
				</td>
			</tr>  
			<tr>
				<td style='border-bottom:none;'>
					<p>
						ពេលឆ្លើយតប នឹងសំណួរខាងក្រោម អ្នកត្រូវប្រាប់យើងគ្រប់យ៉ាងអំពីមូលហេតុដែលអ្នកយល់ថាអ្នកពិតជាត្រូវការកិច្ចការ ការពារជាជនភៀសខ្លួន។ អ្នកត្រូវផ្តល់ព័ត៌មានលម្អិត រួមទាំងហេតុការណ៍ណាមួយនៃការចាប់ខ្លួន ការឃុំខ្លួន ឬការរំលោភ បំពាន ដោយអាជ្ញាធរ ព្រមទាំងកាលបរិច្ឆេទ ដែលព្រឹត្តិការណ៍ពាក់ព័ន្ធទាំងនោះកើតឡើង។ អ្នកចាំចាច់ត្រូវតែផ្តល់ចម្លើយ ពេញលេញ និងពិតប្រាកដ ចំពោះសំណួរទាំងនេះ។ <br/>
					</p>  
				</td>
			</tr> 
			<tr class='text-justify'>
				<td style='border-top:none;'>
					<p> 
						When answering the questions below, you should tell us everything about why you believe that you are in need of refugee protection. You should provide as much detail as possible, including any incidents of arrest, detention or harassment by authorities as well as the date the relevant events occurred.  It is important that you provide full and truthful answers to these questions.  If you need more space, please attach more page (s) with this chapter.
					</p> 
				</td>
			</tr> 
			<tr>  
				<td style='border-bottom:none;'>
					  1. <?= lang("ហេតុអ្វីបានជាអ្នកចាកចេញពីប្រទេសកំណើតរបស់អ្នក ? ") ?><br/>
					  Why did you leave your home country ? <br/>
					  <?= $application->comment_leave_country ?><br/>
				</td>
			</tr>
			<tr>  
				<td  style='border-bottom:none;'>
					2.<?= lang("តើអ្នកជឿថានឹងមានអ្វីកើតឡើងចំពោះអ្នក ឬសមាជិកក្រុមគ្រួសារអ្នកប្រសិនបើអ្នកវិលត្រឡប់ទៅប្រទេសកំណើតវិញ ?  សូមពន្យល់ ៖ ") ?><br/>
					Do you believe what may happen to you, or members of your household, if you return to your home country ? Please explain. <br/>
					<?= $application->comment_believe_happened; ?>
				</td>
			</tr> 
	 </table>
	 <div class='clearfix'></div>
	 <br/> 
	 <table class='table-data' width='100%'>  
			<tr>
				<td colspan='2' class='text-center title-head' >
					<h2><?= lang("សេចក្តីប្រកាស"); ?></h2>
					<h2><?= lang("Declaration") ?></h2>
				</td>
			</tr> 
			<tr>
				<td colspan="2" style='border-top:none;border-bottom:none;'>
					<p>
						ខ្ញុំសូមប្រកាសថា ព័ត៌មានដែលបានផ្តល់ក្នុងបែបបទនេះគឺ ពិតជាត្រឹមត្រូវ ពេញលេញ និងគ្រប់ជ្រុងជ្រោយ ។<br/>
						I declare the information I have provided on and with this form is complete, correct and current in every details.
					</p>
				</td>
			</tr>
			<tr>
				<td colspan="2" style='border-top:none;border-bottom:none;'>
					<p>
						ខ្ញុំយល់ថា ប្រសិនបើ ខ្ញុំបាន ផ្តល់ព័ត៌មានក្លែងបន្លំ ឬ ព័ត៌មានមិនពិតនោះ ពាក្យសុំឋានៈជនភៀសខ្លួន របស់ខ្ញុំនឹងត្រូវបដិសេធ ឬ ប្រសិនបើ ខ្ញុំត្រូវទទួលស្គាល់ ជាជនភៀស ខ្លួន ហើយនោះ ការទទួលស្គាល់ នឹងត្រូវលុបចោល។ <br/>
						I understand that if I have given false or misleading information, my application for refugee status may be refused, or, if I have been recognized as a refugee, the recognition may be cancelled.
					</p>
				</td>
			</tr>
			<tr>
				<td colspan='2' style='border-top:none;border-bottom:none;'>
					<p> 
						ខ្ញុំសូមជម្រាប នាយកដ្ឋានជនភៀសខ្លួន ថា ខ្ញុំនឹង ជម្រាបអំពីការផ្លាស់ប្តូរណាមួយរបស់ខ្ញុំ ក្នុងខណៈពេលដែលពាក្យសុំ របស់ខ្ញុំកំពុងត្រូវពិចារណា រួមមានទាំងការផ្លាស់ប្តូរទីលំនៅ និង លេខទូរស័ព្ទទំនាក់ទំនង ការមកដល់ ឬការចាកចេញ របស់សមាជិកគ្រួសាររបស់ខ្ញុំ ឬ ការផ្លាស់ប្តូរផ្សេងៗទៀតនៃសមាសភាពក្នុងគ្រួសាររបស់ខ្ញុំ។<br/>
						I undertake to inform Refugee Department of any significant changes to my circumstances while my application is being considered, including any changes to my address and contact numbers, the arrival or departure of members of my household or other changes in the composition of my household.
					</p>
				</td>
			</tr>
			<tr>
				<td style='border-top: none; border-right: none; '>
					<p>ហត្ថលេខាអ្នកដាក់ពាក្យសុំ </p>
					<p>Signature of Applicant : ………………………………… </p>
					<p>កាលបរិច្ឆេទ / Date : ………………………………………… </p>
					<p>ធ្វើនៅ / Done at : …………………………………………</p> 
				</td>
				<td style='border-top: none; border-left: none; text-align:center;'>
					<p>ស្នាមមេដៃអ្នកដាក់ពាក្យសុំ​<br/>Applicant’s thumb print</p>
					<br/>
					<br/>
					<br/>
					<br/>
					<br/>
					<br/>
					<br/>
					<br/>
				</td>
			</tr> 
			<tr>
				<td colspan='2' style='border: none;'>
					<br/>
					<br/>
					<p>ហត្ថលេខា និងឈ្មោះមន្រ្តីចុះឈ្មោះ</p>
					<p>Signature & Name of Registration Officer : ………………………………………</p>
					<p>ហត្ថលេខា និងឈ្មោះអ្នកបកប្រែ</p>
					<p>Signature & Name of Interpreter : ………………………………………</p> 
					<?= $this->erp->footer_date_khmer('រាជធានីភ្នំពេញ');?>
				</td>
			</tr> 	
	 </table>   
</div> 
</div> 
<style type="text/css"> 
	.break_page{
		page-break-after: always;
	}
	h1, h2, h3{
		margin-top:10px !important;
	}  
	.table-padding td{
		padding:5px;
	}
	.title-header-form { 
		font-family: 'Moul', sans-serif !important;
	}
	.checkbox-gender, 
	.checkbox-family {
		float:left;
		margin-left: 20px;
	}
	.title-head{
		background:rgba(117, 119, 125, 0.38);
	}
	table.table-data td{
		border:1px solid black;
	 	padding:5px;
	}
	table.table-data{ 
		width:100%;
		margin: 0 auto; 
	} 
	@media print{ 
		.title-head{ background: rgba(117, 119, 125, 0.38) !important; }
		.bblack{ background: white !important; }
		.modal-content, .box, 
		.modal-header{ border:none !important; }
		.box-content{
			padding: 20px !important;
		}
		
	}
	hr{
		padding:0px;
		margin:0px;
		border-top:none !important;
		border-bottom:dotted 1px !important;
	} 
	.table-top{
		border-bottom:1px solid; 
		border-left:1px solid;
		border-right:1px solid;
	}
	.table-top{
		border-top:1px solid;
	}
	.table-top td{
		padding:5px;
		border-bottom:1px solid;
	}
</style>
