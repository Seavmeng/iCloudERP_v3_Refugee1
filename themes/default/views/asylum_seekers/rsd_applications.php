<?php
	$v = "";

	if($this->input->post("firstname")) {
		$v .= "&firstname=" . $this->input->post("firstname");
		$firstname = $this->input->post("firstname");
	}

	if($this->input->post("lastname")) {
		$v .= "&lastname=" . $this->input->post("lastname");
		$lastname = $this->input->post("lastname");
	}

	if($this->input->post("nationality")) {
		$v .= "&nationality=" . $this->input->post("nationality");
		$nationality = $this->input->post("nationality");
	}

	if($this->input->post("counseling_no")) {
		$v .= "&counseling_no=" . $this->input->post("counseling_no");
		$counseling_no = $this->input->post("counseling_no");
	}

	if($this->input->post("register_from")) {
		$v .= "&register_from=" . $this->input->post("register_from");
		$register_from = $this->input->post("register_from");
	}

	if($this->input->post("register_to")) {
		$v .= "&register_to=" . $this->input->post("register_to");
		$register_to = $this->input->post("register_to");
	}
	if($this->input->get("type")) {
		$v .= "&type=" . $this->input->get("type"); 
	}

?>


<ul id="myTab" class="nav nav-tabs">
	<li class=""><a href="#content_1" class="tab-grey"><?= lang("rsd_application"); ?></a></li>
</ul>
	
	<div class="tab-content">
		
		<div id="content_1" class="tab-pane fade in">
			
			<script>
				$(document).ready(function () {
					var oTable = $('#dataTable').dataTable({
						"aaSorting": [[8, "desc"]],
						"aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?= lang('all') ?>"]],
						"iDisplayLength": <?= $Settings->rows_per_page ?>,
						'bProcessing': true, 'bServerSide': true,
						'sAjaxSource': "<?= site_url('asylum_seekers/getRSDApplications/?v=1'.$v) ?>",
						'fnServerData': function (sSource, aoData, fnCallback) {
							aoData.push({
								"name": "<?= $this->security->get_csrf_token_name() ?>",
								"value": "<?= $this->security->get_csrf_hash() ?>"
							});
							$.ajax({
									'dataType': 'json', 
									'type': 'POST', 
									'url': sSource, 
									'data': aoData, 
									'success': fnCallback
									});
						},
						"aoColumns": [
						{"bSortable": false, "mRender": checkbox}, 			
						{"sClass" : "center"},
						{"sClass" : "center"},
						{"sClass" : "center"},	
						{"sClass" : "center", "mRender": fld},
						{"sClass" : "center"},
						{"sClass" : "center"},
						{"sClass" : "center"},
						{"sClass" : "center", "mRender": fld},
						{"sClass" : "center", "mRender": row_status},
						{"sClass" : "center", "mRender": row_status}, 
						
						{"bSortable": false, "sClass" : "center"}]
						,'fnRowCallback': function (nRow, aData, iDisplayIndex) {
								var oSettings = oTable.fnSettings();
								var action = $('td:eq(15)', nRow);	
								return nRow;
							},
							"fnFooterCallback": function (nRow, aaData, iStart, iEnd, aiDisplay) {
								var total = 0, total_kh = 0;
								for (var i = 0; i < aaData.length; i++) 
								{
								}
								var nCells = nRow.getElementsByTagName('th');					
							}
						}).fnSetFilteringDelay().dtFilter([            				
							{column_number: 0, filter_default_label: "[<?= lang("#") ?>]", filter_type: "text", data: []},
							{column_number: 1, filter_default_label: "[<?= lang("case_no") ?>]", filter_type: "text", data: []},
							{column_number: 2, filter_default_label: "[<?= lang("firstname") ?>]", filter_type: "text", data: []},
							{column_number: 3, filter_default_label: "[<?= lang("lastname") ?>]", filter_type: "text", data: []},
							{column_number: 4, filter_default_label: "[<?= lang("dob") ?>]", filter_type: "text", data: []},								
							{column_number: 5, filter_default_label: "[<?= lang("gender") ?>]", filter_type: "text", data: []},
							{column_number: 6, filter_default_label: "[<?= lang("nationality") ?>]", filter_type: "text", data: []},
							{column_number: 7, filter_default_label: "[<?= lang("refugee_office") ?>]", filter_type: "text", data: []},
							{column_number: 8, filter_default_label: "[<?= lang("requested_date") ?>]", filter_type: "text", data: []}
						], "footer");
				});
			</script>
			
			<div class="box">
				<div class="box-header">
					<h2 class="blue">
						<i class="fa-fw fa fa-barcode"></i>
						<?= lang('rsd_application'); ?>
					</h2>
					
					<div class="box-icon">
						<ul class="btn-tasks">
							<li class="dropdown">
								<a href="#" class="toggle_up tip" title="<?= lang('hide_form') ?>">
									<i class="icon fa fa-toggle-up"></i>
								</a>
							</li>
							<li class="dropdown">
								<a href="#" class="toggle_down tip" title="<?= lang('show_form') ?>">
									<i class="icon fa fa-toggle-down"></i>
								</a>
							</li>
							
							<li class="dropdown">
								<a data-toggle="dropdown" class="dropdown-toggle" href="#">
									<i class="icon fa fa-tasks tip" data-placement="left" title="<?= lang("actions") ?>"></i>
								</a>
								<ul class="dropdown-menu pull-right tasks-menus" role="menu" aria-labelledby="dLabel">
									<li>
										<a href="<?= site_url('asylum_seekers/add_rsd_application'); ?>"><i class="fa fa-plus-circle"></i> 
											<?= lang("add_rsd_application"); ?>
										</a>
									</li>
								</ul>
							</li>
							
						</ul>
					</div>
		
				</div>
			
				<div class="box-content">
					<div class="row">
						<?= form_open_multipart("asylum_seekers/rsd_application"); ?>
						
						<div class="col-sm-12">
							<p class="introtext"><?= lang('list_results'); ?></p>
							<div class="form">				
								
								
								
									<div class="row">
										
										<?= form_open_multipart("Asylum_Seekers/rsd_application") ?>
										<div class="col-sm-3">
											<div class="form-group">
												<?php echo lang('case_no', 'case_no'); ?>
												<input name="counseling_no" value="<?= $counseling_no ?>" placeholder="00016" class="form-control input-sm" type="text">
											</div>
										</div>
										
										<div class="col-sm-3">
											<div class="form-group">
												<?php echo lang('firstname', 'firstname'); ?>
												<input name="firstname" value="<?= $firstname ?>" class="form-control input-sm" id="firstname" type="text">
											</div>
										</div>
										<div class="col-sm-3">
											<div class="form-group">
												<?php echo lang('lastname', 'lastname'); ?>
												<input name="lastname" value="<?= $lastname ?>" class="form-control input-sm" id="lastname" type="text">
											</div>
										</div>
										<div class="col-sm-3">
											<div class="form-group">
												<?php echo lang('nationality', 'nationality'); ?>
												<input name="nationality" value="<?= $nationality ?>" class="form-control input-sm" id="nationality" type="text">
											</div>
										</div>

										<div class="col-sm-3">
											<div class="form-group" >
												<?php echo lang('date_from', 'date_from'); ?>
												<input name="register_from" value="<?= $date_from ?>" class="form-control input-sm date" id="register_from" type="text">
											</div>
										</div>
										
										<div class="col-sm-3">
											<div class="form-group" >
												<?php echo lang('date_to', 'date_to'); ?>
												<input name="register_to" value="<?= $date_to ?>" class="form-control input-sm date" id="register_to" type="text">
											</div>
										</div>
										
										<div class="col-sm-12">
											<div class="form-group">
												<input type="submit" value="<?= lang("search") ?>" name="search" class="btn btn-default btn-success" />												
											</div>
										</div>
										<?= form_close() ?>
									</div>
								
								
								
							</div>				
							
							<div class="table-responsive">
								<table id="dataTable" class="table table-condensed table-bordered table-hover table-striped">
									<thead>
										<tr>
											<th style="width:3%;"><?= lang("#") ?></th>
											<th><?= lang("case_no") ?></th>
											<th><?= lang("firstname") ?></th>
											<th><?= lang("lastname") ?></th>
											<th><?= lang("dob") ?></th>
											<th><?= lang("gender") ?></th>
											<th><?= lang("nationality") ?></th>
											<th><?= lang("refugee_office") ?></th>
											<th><?= lang("requested_date") ?></th>	
											<th><?= lang("notice") ?></th>	
											<th><?= lang("rank") ?></th>	
											<th><?= lang("action") ?></th>
										</tr>
									</thead>
									<tbody>
										
									</tbody>					
									<tfoot>
										<tr>
											<th></th>
											<th></th>
											<th></th>
											<th></th>
											<th></th>											
											<th></th>											
											<th></th>
											<th></th>
											<th></th>
											<th></th>
											<th></th> 
											<th></th>
										</tr>
									</tfoot>
								</table>
							</div>
							
						</div>
						<?= form_close(); ?>
					</div>
				</div>
			</div>
		
		</div>
	</div>

<style type="text/css">
	.table {
		white-space:nowrap;
	}
</style>

<script language="javascript">
    $(document).ready(function () {        
		$(".form").slideUp();
        $('.toggle_down').click(function () {
            $(".form").slideDown();
            return false;
        });
        $('.toggle_up').click(function () {
            $(".form").slideUp();
            return false;
        });

    });
</script>	
	
