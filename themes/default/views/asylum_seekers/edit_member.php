
<div class="modal-dialog modal-lg">
	<div class="modal-content">
		<div class="modal-header">				
			<button type="button" class="close" data-dismiss="modal">
				<span aria-hidden="true"><i class="fa fa-2x">&times;</i></span>
				<span class="sr-only"><?=lang('close');?></span>
			</button>
			<h4 class="modal-title" id="payModalLabel"><?= lang('edi_member'); ?></h4>
		</div>
		<?php $attributes = array('id' => 'form1');?>
		<?php 
			$countries_ = array(lang("select"));
			foreach($countries as $country){
				$countries_[$country->id] = $country->country;
			} 
			?>
			<?php 
			$provinces_ = array(lang("select"));
			foreach($provinces as $province){
				$provinces_[$province->id] = $province->name;
			} 
			?>
			<?php 
			$districts_ = array(lang("select"));
			foreach($districts as $district){
				$districts_[$district->id] = $district->name;
			} 
			?>
			<?php 
			$communes_ = array(lang("select"));
			foreach($communes as $commune){
				$communes_[$commune->id] = $commune->name;
			} 
			?> 
		<?php  echo form_open_multipart("asylum_seekers/edit_member/".$id."/".$counseling_id, $attributes); ?> 
		<div class="modal-body">
			<div class="col-sm-6" >
				<div class="row" >
				 
			<div class="col-sm-6" >
				<div class="form-group">
					<?php echo lang('firstname', 'firstname'); ?>​
					<span class="red">*</span>
					<div class="controls">					
						<input type="text" class="form-control input-sm" value="<?= $member->firstname_kh ?>" name="firstname_kh" />
					</div>
				</div>
			</div>
			<div class="col-sm-6" >
				<div class="form-group">
					<?php echo lang('firstname', 'firstname'); ?>​(EN)
					<span class="red">*</span>
					<div class="controls">					
						<input type="text" class="form-control input-sm" value="<?= $member->firstname ?>" name="firstname" />
					</div>
				</div>
			</div>
			<div class="col-sm-6" >
				<div class="form-group">
					<?php echo lang('lastname', 'lastname'); ?>​
					<span class="red">*</span>
					<div class="controls">					
						<input type="text" class="form-control input-sm" value="<?= $member->lastname_kh ?>" name="lastname_kh" />
					</div>
				</div>
			</div>
			<div class="col-sm-6" >
				<div class="form-group">
					<?php echo lang('lastname', 'lastname'); ?>​(EN)
					<span class="red">*</span>
					<div class="controls">					
						<input type="text" class="form-control input-sm" value="<?= $member->lastname ?>" name="lastname" />
					</div>
				</div>
			</div>	
			<div class="col-sm-6">
				<div class="form-group">
					<?php echo lang('gender', 'gender'); ?>​
					<span class="red">*</span>
					<div class="controls">
						<select class="form-control selectskip" name="gender">
							<option value="male" <?= ($member->gender=="male"?"selected":"") ?>><?= lang("male") ?></option>
							<option value="female" <?= ($member->gender=="female"?"selected":"") ?>><?= lang("female") ?></option>
						</select>				
					</div>
				</div>
			</div>
			<div class="col-sm-6">	
				<div class="form-group">
					<?php echo lang('dob', 'dob'); ?>​
					<span class="red">*</span>
					<div class="controls">
						<input type="text" class="form-control input-sm date" value="<?= $this->erp->hrsd($member->dob) ?>"  name="dob" />
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
			<div class="col-sm-6">	
				<div class="form-group">
					<?php echo lang('nationality', 'nationality'); ?>​
					<span class="red">*</span>
					<div class="controls">
						<input type="text" class="form-control input-sm " value="<?= $member->nationality_kh ?>"  name="nationality_kh" />
					</div>
				</div>
			</div>
                 <div class="col-sm-6">	
				<div class="form-group">
					<?php echo lang('nationality', 'nationality'); ?>​(EN)
					<span class="red">*</span>
					<div class="controls">
						<input type="text" class="form-control input-sm " value="<?= $member->nationality ?>"  name="nationality" />
					</div>
				</div>
			</div>
			<div class="col-sm-6">
				<div class="form-group">
					<?php echo lang('ethnicity', 'ethnicity'); ?>​
					<span class="red">*</span>
					<div class="controls">
						<input type="text" class="form-control input-sm " value="<?= $member->ethnicity_kh?>"  name="ethnicity_kh" />
					</div>
				</div>
			</div>
            <div class="col-sm-6">
				<div class="form-group">
					<?php echo lang('ethnicity', 'ethnicity'); ?>​(EN)
					<span class="red">*</span>
					<div class="controls">
						<input type="text" class="form-control input-sm "  value="<?= $member->ethnicity?>"  name="ethnicity" />
					</div>
				</div>
			</div>
			<div class="col-sm-6">
				<div class="form-group">
					<?php echo lang('occupation', 'occupation'); ?>​
					<span class="red">*</span>
					<div class="controls">
						<input type="text" class="form-control input-sm " value="<?= $member->occupation_kh ?>"  name="occupation_kh" />
					</div>
				</div>
			</div>
                 <div class="col-sm-6">
				<div class="form-group">
					<?php echo lang('occupation', 'occupation'); ?>​(EN)
					<span class="red">*</span>
					<div class="controls">
						<input type="text" class="form-control input-sm " value="<?= $member->occupation ?>"  name="occupation" />
					</div>
				</div>
			</div>
			<div class="col-sm-6">	
				<div class="form-group">
					<?php echo lang('education', 'education'); ?>​
					<span class="red">*</span>
					<div class="controls">
						<input type="text" class="form-control input-sm " value="<?= $member->education_kh ?>"  name="education_kh" />
					</div>
				</div>
			</div>
                 <div class="col-sm-6">	
				<div class="form-group">
					<?php echo lang('education', 'education'); ?>​(EN)
					<span class="red">*</span>
					<div class="controls">
						<input type="text" class="form-control input-sm " value="<?= $member->education ?>"  name="education" />
					</div>
				</div>
			</div>
			<div class="col-sm-12">	
				<div class="form-group">
					<?php echo lang('relationship', 'relationship'); ?>​
					<span class="red">*</span>
					<div class="controls">
						<select class="form-control" name="relationship">
							<?php foreach($relationship as $row){?>
								<option value="<?= $row->id ?>" <?= ($member->relationship==$row->id?'selected':'')?>><?= $row->relationship_kh ?></option>  
							<?php }?> 
						</select>
					</div>
				</div>
			</div> 
			<div class="col-sm-6">	
				<div class="form-group">
					<?php echo lang('photo', 'photo'); ?>​
					<span class="red">*</span>
					<div class="controls">
						<input type="file"  name="userfile" />
					</div>
				</div>
			</div>
								
				</div>
			</div>
			<div class="col-sm-6">
				
			<div class="col-sm-12">		
				<div class="form-group">
					<?php echo lang('pob', 'pob'); ?>​
					<span class="red">*</span>
					<div class="controls">
						<input type="text" class="form-control input-sm " value="<?php echo $member->pob_kh?>"  name="pob_kh" />
					</div>
				</div>
			</div>
            <div class="col-sm-12">		
				<div class="form-group">
					<?php echo lang('pob', 'pob'); ?>​​ (EN)
					<span class="red">*</span>
					<div class="controls">
						<input type="text" class="form-control input-sm " value="<?php echo $member->pob?>" name="pob" />
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
				
				
				<div class="panel panel-warning">
					<div class="panel-heading"><?php echo lang("address","address") ?></div>
					<div class="panel-body" style="padding: 5px;">
						
						<div class="col-lg-12">
							<div class="form-group">
								<?php echo lang('country', 'country'); ?>
								<div class="controls">
									<?php echo form_dropdown('country', $countries_, $member->country, ' class="form-control" '); ?>												
								</div>
							</div>
						</div>
						
						<div class="col-lg-6">
							<div class="form-group">
								<?php echo lang('province', 'province'); ?>
								<div class="controls">
									<?php echo form_dropdown('province', $provinces_, $member->province, ' class="form-control" '); ?>												
								</div>
							</div>
						</div>
						
						<div class="col-lg-6">
							<div class="form-group">
								<?php echo lang('district', 'district'); ?>
								<div class="controls">
									<?php echo form_dropdown('district', $districts_, $member->district, ' class="form-control" '); ?>												
								</div>
							</div>
						</div>
						
						<div class="col-lg-6">
							<div class="form-group">
								<?php echo lang('commune', 'commune'); ?>
								<div class="controls">
									<?php echo form_dropdown('commune', $communes_, $member->commune, ' class="form-control" '); ?>												
								</div>
							</div>
						</div>  
						  
						<div class="col-lg-6">
							<div class="form-group">
								<?php echo lang('village', 'village'); ?>
								<span class="red">*</span>
								<?php  // var_dump($this->applications->getTagsById($member->village)) ?>
								<div class="controls"> 
									<?php  echo form_input('village',$this->asylum_seekers->getTagsById($member->village),'class="form-control village" '); ?>											
								</div>
							</div>
						</div>
					
						<div class="col-lg-12">
							<div class="form-group"> 
								<?php echo lang('other', 'other'); ?>​ (KH)
								<span class="red">*</span>
								<div class="control">
									<input type="text" value="<?php echo $member->address_kh ?>" class="form-control input-sm" name="address_kh"  id="address_kh" />
								</div> 
							</div> 
						</div> 
						
						<div class="col-lg-12">
							<div class="form-group"> 
								<?php echo lang('other', 'other'); ?>​ (EN)
								<span class="red">*</span>
								<div class="control">
									<input type="text" value="<?php echo $member->address ?>" class="form-control input-sm" name="address"  id="address" />
								</div> 
							</div> 
						</div> 
					</div>
				</div> 
				
				
				
				
			</div>
			<div class="clearfix"></div>
		</div>
		 <div class="modal-footer">
			<?php echo form_submit('submit', lang('submit'), 'class="btn btn-primary update"'); ?>
		 </div>
		<?php  echo form_close(); ?>
	</div>
</div><style>
	ul.ui-autocomplete {
			z-index: 1100;
	}
</style>
<script type="text/javascript" src="<?= $assets ?>styles/helpers/components/js/bootstrap-formhelpers.js"></script>	
<link href="<?= $assets ?>styles/helpers/components/js/bootstrap-formhelpers.min.css" rel="stylesheet" />
<script type="text/javascript">
	$(function(){ 
		$("select").select2();
		getAllAutoComplete(".village, .village","/getAllTags?v=village");	
		function getAllAutoComplete(element, base_url){
			$(element).autocomplete({
				source: function (request, response) {
					var q = request.term;
					$.ajax({
						type: 'GET',
						url: '<?= site_url('asylum_seekers/'); ?>'+base_url,
						dataType: "json",
						data: {
							q : q
						},
						success: function (data) {
							console.log(JSON.stringify(data));
							response(data); 
							
						} 
					});
				},
				minLength: 1,
				autoFocus: false,
				delay: 200,
				response: function (event, ui) {
					if (ui.content[0].id == 0) {
						$(this).removeClass('ui-autocomplete-loading');
						$(this).removeClass('ui-autocomplete-loading');
					}
				},
				select: function (event, ui) {
					event.preventDefault();
					$(this).val(ui.item.label);
				}
			});
		}
	});   
</script>
