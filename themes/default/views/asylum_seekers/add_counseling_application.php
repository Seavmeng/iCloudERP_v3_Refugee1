
<ul id="myTab" class="nav nav-tabs">
	<li class="">
		<a href="#content_1" class="tab-grey">
			<?= lang("counseling_application_form"); ?> 
			<i class="fa fa-chevron-right" aria-hidden="true"></i>
		</a>
	</li>
	<li class="">
		<a href="#content_2" class="tab-grey">
			<?= lang("written_claim_for_counseling"); ?> 
			<i class="fa fa-chevron-right" aria-hidden="true"></i>
		</a>
	</li>
</ul>
<?php $attributes = array('novalidate' => 'novalidate');?>
<?= form_open_multipart("asylum_seekers/add_counseling_application",$attributes); ?>

<div class="tab-content">
	<div id="content_1" class="tab-pane fade in">
		<div class="box">
			<div class="box-content">
				<div class="row">	
				<div class="col-sm-5" style="padding-right:0px;">
					<div class="col-sm-6">
						<div class="form-group">
							<?php echo lang('នាមខ្លួន​ ', 'នាមខ្លួន​ / ​Given Name'); ?>
							<span class="red">*</span>
							<div class="controls">
								<input type="text" value="<?= set_value('firstname_kh') ?>" name="firstname_kh" class="form-control input-sm" />								
							</div>
						</div>
					</div>

					<div class="col-sm-6">
						<div class="form-group">
						<?php echo lang('First Name', 'firstname'); ?>
							<span class="red">*</span>
							<div class="controls">
								<input type="text" value="<?= set_value('firstname') ?>" name="firstname" class="form-control input-sm" />								
							</div>
						</div>
					</div>
					
					<div class="col-sm-6">
						<div class="form-group">
							<?php echo lang('នាមត្រកូល ', 'នាមត្រកូល / ​ Surname'); ?>
							<span class="red">*</span>
							<div class="controls">
								<input type="text" value="<?= set_value('lastname_kh') ?>" name="lastname_kh" class="form-control input-sm" />
							</div>
						</div>
					</div>
					
					<div class="col-sm-6">
						<div class="form-group">
						<?php echo lang('Last Name', 'lastname'); ?>
							<span class="red">*</span>
							<div class="controls">
								<input type="text" value="<?= set_value('lastname') ?>" name="lastname" class="form-control input-sm" />
							</div>
						</div>
					</div>
					
					<div class="col-sm-6">
						<div class="form-group">
							<?php echo lang('ឈ្មោះហៅក្រៅ ', 'ឈ្មោះហៅក្រៅ / Nickname'); ?>
							<span class="red">*</span>
							<div class="controls">
								<input type="text" name="nickname_kh" class="form-control input-sm" value="<?= set_value('nickname_kh') ?>"  />
							</div>
						</div>
					</div>
					
					<div class="col-sm-6">
						<div class="form-group">
							<?php echo lang('Nickname ', 'nickname'); ?>
							<span class="red">*</span>
							<div class="controls">
								<input type="text" name="nickname" class="form-control input-sm" value="<?= set_value('nickname') ?>" />
							</div>
						</div>
					</div>
					
					<div class="col-sm-6" >
						<div class="form-group">
							<?php echo lang('ថ្ងៃខែឆ្នាំកំណើត / Date of Birth', 'ថ្ងៃខែឆ្នាំកំណើត / Date of Birth'); ?>
							<span class="red">*</span>
							<div class="controls">
								<input type="text" value="<?= set_value('dob') ?>" name="dob" class="form-control input-sm date" />
							</div>
						</div>
					</div>
					
					<div class="col-sm-6">
						<div class="form-group">
							<?php echo lang('ភេទ /​ Sex', 'ភេទ /​ Sex'); ?>
							<span class="red">*</span>
							<div class="controls">
								<label class="radio-inline">
									<input type="radio" value="male" name="gender"><?= lang('male'); ?>
								</label>
								
								<label class="radio-inline">
									<input type="radio" value="female" checked name="gender"><?= lang('female'); ?>
								</label> 
							</div>
						</div>
					</div> 
					
					<div class="clearfix"></div> 
					<div class="col-sm-6">
						<div class="form-group">
							<?php echo lang('សញ្ជាតិ ', 'សញ្ជាតិ / Nationality'); ?>
							<span class="red">*</span>
							<div class="controls">
								<input type="text" value="" name="nationality_kh" class="form-control input-sm" />
							</div>
						</div>
					</div> 
					<div class="col-sm-6">
						<div class="form-group">
								<?php echo lang(' Nationality','nationality'); ?>
							<span class="red">*</span>
							<div class="controls">
								<input type="text" value="" name="nationality" class="form-control input-sm" />
							</div>
						</div>
					</div>
					
					<div class="col-sm-6">
						<div class="form-group">
							<?php echo lang('សាសនា', 'សាសនា / Religion'); ?>
							<span class="red">*</span>
							<div class="controls">
								<input type="text" value="<?= set_value('religion') ?>" name="religion" class="form-control input-sm" />
							</div>
						</div>
					</div>
					
					<div class="col-sm-6">
						<div class="form-group">
							<?php echo lang('Religion', 'religion'); ?>
							<span class="red">*</span>
							<div class="controls">
								<input type="text" value="<?= set_value('religion_kh') ?>" name="religion_kh" class="form-control input-sm" />
							</div>
						</div>
					</div>
					
					<div class="col-sm-6">
						<div class="form-group">
							<?php echo lang('ជាតិពន្ធុ ', 'ជាតិពន្ធុ / Ethnicity '); ?>
							<span class="red">*</span>
							<div class="controls">
								<input type="text" value="<?= set_value('ethnicity_kh') ?>" name="ethnicity_kh" class="form-control input-sm" />
							</div>
						</div>
					</div>
					
					<div class="col-sm-6">
						<div class="form-group">
							<?php echo lang('Ethnicity', 'ethnicity'); ?>
							<span class="red">*</span>
							<div class="controls">
								<input type="text" value="<?= set_value('ethnicity') ?>" name="ethnicity" class="form-control input-sm" />
							</div>
						</div>
					</div>
					
					<div class="col-sm-6">
						<div class="form-group">
							<?php echo lang("ទីកន្លែងកំណើត ","ទីកន្លែងកំណើត / Place of Birth") ?> 
							<span class="red">*</span>
							<div class="controls">
								<input type="text" value="<?= set_value('pob_kh', $result->pob_kh) ?>" name="pob_kh" class="form-control input-sm" />
							</div>
						</div>
					</div>
					
					<div class="col-sm-6">
						<div class="form-group">
							<?php echo lang('Place of Birth', 'pob;'); ?>
							<span class="red">*</span>
							<div class="controls">
								<input type="text" value="<?= set_value('pob', $result->pob) ?>" name="pob" class="form-control input-sm" />
							</div>
						</div>
					</div>
					
					<?php 
					$countries_ = array(lang("select"));
					foreach($countries as $country){
						$countries_[$country->id] = $country->country;
					} 
					?>
					<?php 
					$states_ = array(lang("select"));
					foreach($states as $state){
						$states_[$state->id] = $state->name;
					} 
					?>
					<?php 
					$provinces_ = array(lang("select"));
					foreach($provinces as $province){
						$provinces_[$province->id] = $province->name;
					} 
					?>
					<?php 
					$districts_ = array(lang("select"));
					foreach($districts as $district){
						$districts_[$district->id] = $district->name;
					} 
					?>
					<?php 
					$communes_ = array(lang("select"));
					foreach($communes as $commune){
						$communes_[$commune->id] = $commune->name;
					} 
					?>
					
					<div class="col-sm-12">
						<div class="panel panel-warning">
							<div class="panel-heading"><?php echo lang("អាសយដ្ឋានបច្ចុប្បន្ន","អាសយដ្ឋានបច្ចុប្បន្ន") ?> / <?= lang("Current address") ?></div>
							<div class="panel-body" style="padding: 5px;">
								
								
								
								<div class="col-lg-12">
									<div class="form-group ">
										<?php echo lang('country', 'country'); ?>
										<div class="controls">
											<?php echo form_dropdown('country', $countries_, 538, ' class="form-control" '); ?>												
										</div>
									</div>
								</div>
								<div class="col-lg-6">
									<div class="form-group">
										<?php echo lang('province', 'province'); ?>
										<div class="controls">
											<?php echo form_dropdown('province', $provinces_, 0, ' class="form-control" '); ?>												
										</div>
									</div>
								</div>
								<div class="col-lg-6">
									<div class="form-group">
										<?php echo lang('district', 'district'); ?>
										<div class="controls">
											<?php echo form_dropdown('district', $districts_, 0, ' class="form-control" '); ?>												
										</div>
									</div>
								</div>
								<div class="col-lg-6">
									<div class="form-group">
										<?php echo lang('commune', 'commune'); ?>
										<div class="controls">
											<?php echo form_dropdown('commune', $communes_, 0, ' class="form-control" '); ?>												
										</div>
									</div>
								</div>
								<div class="col-lg-6">
									<div class="form-group">
										<?php echo lang('village', 'village'); ?>
										<div class="controls">
											<?php 
											echo form_input('village', '','class="form-control village" '); ?>											
										</div>
									</div>
								</div>
								<div class="col-sm-12 text-nowrap">
									<div class="form-group">
										<?php echo lang('other', 'other'); ?> (<?php echo lang("KH") ?>)
										<span class="red">*</span>
										<div class="controls">
											<input type="text" value="<?= set_value('address_kh') ?>" name="address_kh" class="form-control input-sm" />
										</div>
									</div>
								</div>
								<div class="col-sm-12">
									<div class="form-group">
										<?php echo lang('other', 'other'); ?> (<?php echo lang("EN") ?>)
										<div class="controls">
											<input type="text" value="<?= set_value('address') ?>" name="address" class="form-control input-sm" />
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-12">
						<div class="form-group">
							<button class="btn btn-danger right"><?= lang("reset"); ?></button>
							<button class="btn btn-success save-data"><?= lang("submit"); ?></button>
						</div>
					</div>
				</div>
				<div class="col-sm-5" style="padding-left:0px;">
					
						<div class="col-sm-6" style=" white-space: nowrap;">
							<div class="form-group">
								<?php echo lang('ឯកសារធ្វើដំណើរ ', 'ឯកសារធ្វើដំណើរ / Travel documents'); ?>
								<span class="red">*</span>
								<div class="controls">
									<input type="text" value="<?= set_value('travel_documents_kh') ?>" name="travel_documents_kh" class="form-control input-sm" />
								</div>
							</div>
						</div>
						<div class="col-sm-6" >
							<div class="form-group">
								<?php echo lang('Travel documents', 'travel_documents'); ?>
								<div class="controls">
									<input type="text" value="<?= set_value('travel_documents') ?>" name="travel_documents" class="form-control input-sm" />
								</div>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<?php echo lang('កន្លែងផ្តល់ ', 'កន្លែងផ្តល់ / Issued place'); ?>
								<span class="red">*</span>
								<div class="controls">
									<input type="text" value="<?= set_value('place_of_issued_kh', lang("ទីស្នាក់ការកណ្តាល")) ?>" name="place_of_issued_kh" class="form-control input-sm" />
								</div>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<?php echo lang('Issued place', 'place_of_issued'); ?>
									<span class="red">*</span>
								<div class="controls">
									<input type="text" value="<?= set_value('place_of_issued') ?>" name="place_of_issued" class="form-control input-sm" />
								</div>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<?php echo lang('ផ្តល់នៅថ្ងៃទី / Issued date', 'ផ្តល់នៅថ្ងៃទី / Issued date'); ?>
								<span class="red">*</span>
								<div class="controls">
									<input type="text" value="<?= set_value('issued_date', date("d/m/Y")) ?>" name="issued_date" class="form-control input-sm date" />
								</div>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<?php echo lang('ផុតកំណត់នៅថ្ងៃទី / Expired date', 'ផុតកំណត់នៅថ្ងៃទី / Expired date'); ?>
								<span class="red">*</span>
								<div class="controls">
									<input type="text" value="<?= set_value('expired_date', date("d/m/Y")) ?>" name="expired_date" class="form-control input-sm date" />
								</div>
							</div>
						</div>
						<div class="col-sm-12">
							<div class="form-group">
								<?php echo lang('មធ្យោបាយធ្វើដំណើរ / Means of transportation', 'មធ្យោបាយធ្វើដំណើរ / Means of transportation'); ?>
								<span class="red">*</span>
								<div class="controls">
									<label class="radio-inline">
										<input type="radio" checked value="sea" name="means_of_transportations"><?= lang("sea"); ?>
									</label>
									<label class="radio-inline">
										<input type="radio" value="land" name="means_of_transportations"><?= lang("land"); ?>
									</label>
									
									<label class="radio-inline">
										<input type="radio" value="air" name="means_of_transportations"><?= lang("air"); ?>
									</label>
									
									<label class="radio-inline">
										<input type="radio" value="other" name="means_of_transportations"><?= lang("other"); ?>
									</label>
								</div>
							</div>
						</div>
						<div class="clearfix"></div>
						<div class="col-sm-6 text-nowrap">
							<div class="form-group">
								<?php echo lang('ថ្ងៃមកដល់លើកទីមួយ / First arrival date', 'ថ្ងៃមកដល់លើកទីមួយ / First arrival date'); ?>
								<span class="red">*</span>
								<div class="controls">
									<input type="text" value="<?= set_value('first_arrived_date',date("d/m/Y")) ?>" name="first_arrived_date" class="form-control input-sm date" />
								</div>
							</div>
						</div>
						<div class="col-sm-6 text-nowrap">
							<div class="form-group">
								<?php echo lang('ថ្ងៃចូលចុងក្រោយ / Last date of entry', 'ថ្ងៃចូលចុងក្រោយ / Last date of entry'); ?>
								<span class="red">*</span>
								<div class="controls">
									<input type="text" value="<?= set_value('last_entry_date',date("d/m/Y")) ?>" name="last_entry_date" class="form-control input-sm date" />
								</div>
							</div>
						</div>
						<div class='clearfix'></div>
						<div class="col-sm-6">
							<div class="form-group">							
								<?php echo lang('ចូលតាមច្រក ', 'ចូលតាមច្រក / Port of entry'); ?>
								<span class="red">*</span>
								<div class="controls">
									<input type="text"​ value="<?= set_value('port_of_entry_kh') ?>" name="port_of_entry_kh" class="form-control input-sm" />
								</div>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">							
								<?php echo lang('Port of entry', 'port_of_entry'); ?>
								<span class="red">*</span>
								<div class="controls">
									<input type="text"​ value="<?= set_value('port_of_entry') ?>" name="port_of_entry" class="form-control input-sm" />
								</div>
							</div>
						</div>
						<div class="col-sm-6" style="white-space:nowrap;">
							<div class="form-group">
								<?php echo lang('គោលបំណង់នៃការចូល', 'គោលបំណង់នៃការចូល/purpose_of_entry_kh'); ?>
								<span class="red">*</span>
								<div class="controls">
									<input type="text" value="<?= set_value('purpose_of_entry_kh') ?>" name="purpose_of_entry_kh" class="form-control input-sm" />
								</div>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<?php echo lang('Purpose Of Entry', '&nbsp;'); ?>
								<span class="red">*</span>
								<div class="controls">
									<input type="text" value="<?= set_value('purpose_of_entry') ?>" name="purpose_of_entry" class="form-control input-sm" />
								</div>
							</div>
						</div>
						
						<div class="col-sm-6">
							<div class="form-group">
								<?= lang("ទូរស័ព្ទទំនាក់ទំនង / Phone number", "ទូរស័ព្ទទំនាក់ទំនង / Phone number"); ?>
								<span class="red">*</span>
								<div class="controls">
									<input type="text" value="<?= set_value('phone') ?>" name="phone" class="form-control input-sm input-medium bfh-phone" data-format="+855 (ddd) ddd-dddd" />
								</div>
							</div>
						</div>
						<div class="col-sm-12">
						<div class="panel panel-warning">
                                                <div class="panel-heading"><?php echo lang("អាសយដ្ឋាននៅប្រទេសដើម / Address of original country","អាសយដ្ឋាននៅប្រទេសដើម / Address of original country") ?></div>
                                                <div class="panel-body" style="padding: 5px;">
                                                        <div class="col-lg-6">
                                                            <div class="form-group">
                                                                <?php echo lang('country', 'country'); ?>
                                                                <span class="red">*</span>
                                                                <div class="controls">
                                                                    <?php echo form_dropdown('country_o', $countries_, 1, ' class="form-control" '); ?>												
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6">
                                                            <div class="form-group">
                                                                <?php echo lang('state', 'state'); ?>
                                                                <span class="red">*</span>
                                                                <div class="controls">
                                                                    <?php echo form_dropdown('state_o', $states_, 0, ' class="form-control" '); ?>												
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6">
                                                            <div class="form-group">
                                                                <?php echo lang('province', 'province'); ?>
                                                                <span class="red">*</span>
                                                                <div class="controls">
                                                                    <?php echo form_dropdown('province_o', $provinces_, 0, ' class="form-control" '); ?>												
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6">
                                                            <div class="form-group">
                                                                <?php echo lang('district', 'district'); ?>
                                                                <span class="red">*</span>
                                                                <div class="controls">
                                                                    <?php 
                                                                    // echo form_input('district_o', '', 'class="form-control district_o" ');
                                                                    echo form_dropdown('district_o', $districts_, 0, ' class="form-control" '); ?>												
                                                                </div>
                                                            </div>
                                                        </div>		
                                                        <div class="col-lg-6">
                                                            <div class="form-group">
                                                                <?php echo lang('commune', 'commune'); ?>
                                                                <span class="red">*</span>
                                                                <div class="controls">
                                                                    <?php 
                                                                    // echo form_input('commune_o', '','class="form-control commune_o" ');
                                                                    echo form_dropdown('commune_o', $communes_, 0, ' class="form-control" '); ?>												
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6">
                                                            <div class="form-group">
                                                                <?php echo lang('village', 'village'); ?>
                                                                <span class="red">*</span>
                                                                <div class="controls">
                                                                    <?php 
                                                                    echo form_input('village_o', '','class="form-control village_o" ');
                                                                    //echo form_dropdown('commune_o', $communes_, 0, ' class="form-control" '); ?>												
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-12">
                                                            <div class="form-group">
                                                                <?php echo lang('other', 'other'); ?> (<?php echo lang("KH") ?>)
                                                                <div class="controls">
                                                                    <input type="text" value="<?= set_value('address_original_kh') ?>" name="address_original_kh" class="form-control input-sm" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-12">
                                                            <div class="form-group">
                                                                <?php echo lang('other', 'other'); ?> (<?php echo lang("EN") ?>)
                                                                <div class="controls">
                                                                    <input type="text" value="<?= set_value('address_original') ?>" name="address_original" class="form-control input-sm" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                </div>
                                             </div>
                                        </div>
				</div>
				<div class="col-sm-2" style="padding-left:0px;">
					<div class="form-group hidden">
						<?php echo lang('counseling_no', 'counseling_no'); ?>
						<span class="red">*</span>
						<div class="controls">
							<input type="text" readonly="readonly"  class="form-control input-sm" id="counseling_No" value="" / >
							<input type="hidden" name="counseling_no" value="<?= $counseling_no ?>" />
						</div>
					</div>
					
					<div class="col-md-12" style="padding-left:0px; padding-right:0px;">
						<?= lang("photo", "photo"); ?>						
						<div class="form-group">
						  <div class="main-img-preview">
							<img class="thumbnail img-preview" src="<?= site_url("assets/uploads/no_image.png"); ?>" title="Preview Logo">
						  </div>
						  <div class="input-group">
							<input id="fakeUploadLogo" class="form-control input-sm fake-shadow" placeholder="<?= lang("choose_file") ?>" disabled="disabled">
							<div class="input-group-btn">
							  <div class="fileUpload btn btn-danger fake-shadow">
								<span><i class="fa fa-upload"></i>&nbsp;&nbsp;<?= lang("upload") ?></span>
								<input id="logo-id" accept=".gif,.jpg,.png" data-maxfile="1024" name="photo" type="file" class="attachment_upload">
							  </div>
							</div>
						  </div>
						</div>
					</div>
					
					<div class="form-group hidden">
						<div class="controls">
							<a href="#" style="width:100%" class="btn btn-default" id="take-photo"><?= lang("take") ?></a>
						</div>
					</div>
					
				</div>
				
				</div>
			</div>
		</div>
	</div>
	
	<div id="content_2" class="tab-pane fade in">
		<div class="box">
			<div class="box-content">
				<div class="row">							
					<div class="col-sm-9">
						<div class="col-sm-4">
							<div class="form-group">
								<?php echo lang('sample_document', 'Sample Document'); ?>​ 
								<span class="red">*</span>
								<div class="controls">
									<a class="btn btn-primary form-control" href="<?= site_url("assets/downloads/F1_1_Claim_for_Registration.pdf") ?>" name="title" class="form-control input-sm" /><?= lang("download"); ?></a>								
								</div>
							</div>
							
							<div class="form-group">
								<?php echo lang('document', 'document'); ?>​ 
								<span class="red">*</span>
								<div class="controls">
									<input accept=".pdf,.doc" type="file" name="document" data-browse-label="<?= lang('document'); ?>" required="required"
										   data-show-upload="false" data-show-preview="false" accept="image/*"
										   class="form-control file" />									
								</div>
							</div>
						</div> 
					</div>
				</div> 
			</div>
		</div>
	</div>
	
</div>

<?= form_close(); ?>

<div class="modal fade in" id="take-photo-modal" tabindex="-1" role="dialog" aria-labelledby="payModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
				</button>
				<h4 class="modal-title" id="myModalLabel"><?php echo lang('webcam'); ?></h4>
			</div>
			<div class="modal-body">
				<div class="col-sm-8">
					<div id="webcam"></div>
				</div>
				<div class="col-sm-4">
					<div id="webcam-preview"></div>
				</div>
				
			   <div class="clearfix"></div>
			</div>
			<div class="modal-footer">
			   <input type="button" class="btn btn-default" value="<?= lang("upload") ?>" id="upload" />
			   <input type="button" class="btn btn-danger" value="<?= lang("take_snapshot") ?>" onClick="take_snapshot()" />
			</div>
		</div>
	</div>
</div> 
<script type="text/javascript" src="<?= $assets ?>styles/helpers/components/js/bootstrap-formhelpers.js"></script>	
<link href="<?= $assets ?>styles/helpers/components/js/bootstrap-formhelpers.min.css" rel="stylesheet" />
<style type="text/css">
	.form-control input-sm, .thumbnail {
		border-radius: 2px;
	}
	.btn-danger {
		background-color: #B73333;
	}
	/* File Upload */
	.fake-shadow {
		box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
	}
	.fileUpload {
		position: relative;
		overflow: hidden;
	}
	.fileUpload #logo-id {
		position: absolute;
		top: 0;
		right: 0;
		margin: 0;
		padding: 0;
		font-size: 33px;
		cursor: pointer;
		opacity: 0;
		filter: alpha(opacity=0);
	}
	.img-preview {
		max-width: 100%;
	}
</style>
<script type="text/javascript">
	// $(function(){ 
	// $(".save-data").on('click',function() {
			// var insert_counseling_no = $('#counseling_No').val(); 
			// if(insert_counseling_no == ''){
                // bootbox.alert("<?= lang("Please_input_your_required_post") ?>");
				// return false;   
			// }
           // return true;
			
		// });	
	// }); 
</script>

<script type="text/javascript">
	$(document).ready(function() {
		var brand = document.getElementById('logo-id');
		brand.className = 'attachment_upload';
		brand.onchange = function() {
			document.getElementById('fakeUploadLogo').value = this.value.substring(12);
		};		
		function readURL(input) {
			if (input.files && input.files[0]) {
				var reader = new FileReader();
				
				reader.onload = function(e) {
					$('.img-preview').attr('src', e.target.result);
				};
				reader.readAsDataURL(input.files[0]);
			}
		}
		$("#logo-id").change(function() {
			readURL(this);
		});
		
		$("#upload").click(function(){
			var filename = "<?= site_url()?>/" + $("#webcam-preview img").attr("src");
			$('.img-preview').attr('src', filename);
			return false;
		});

		getAllAutoComplete(".commune_o","/getAllTags?v=commune");
		getAllAutoComplete(".district_o","/getAllTags?v=district");
		getAllAutoComplete(".village_o, .village","/getAllTags?v=village");
		
		
		function getAllAutoComplete(element, base_url){
			$(element).autocomplete({
				source: function (request, response) {
					var q = request.term;
					$.ajax({
						type: 'GET',
						url: '<?= site_url('asylum_seekers/'); ?>'+base_url,
						dataType: "json",
						data: {
							q : q
						},
						success: function (data) {
							response(data);
						}
					});
				},
				minLength: 1,
				autoFocus: false,
				delay: 200,
				response: function (event, ui) {
					if (ui.content[0].id == 0) {
						$(this).removeClass('ui-autocomplete-loading');
						$(this).removeClass('ui-autocomplete-loading');
					}
				},
				select: function (event, ui) {
					event.preventDefault();
					$(this).val(ui.item.label);
				}
			});
		}
		
	});
</script>
<script type="text/javascript">
	$(function(){		
		$("#take-photo").click(function(){
			$('#take-photo-modal').appendTo("body").modal('show');			
			return false;
		});
	})
</script>
<script type="text/javascript" src="<?= $assets ?>webcam/webcam.js"></script>
<script language="javascript">
	Webcam.set({
		width: 560,
		height: 460,
		image_format: 'jpeg',
		jpeg_quality: 90
	});
	Webcam.attach('#webcam');	
</script>
<script language="javascript">
	function take_snapshot() {
		Webcam.snap(function(data_uri) {
			document.getElementById('webcam-preview').innerHTML = '<h2>Processing:</h2>';
			var url = "<?= site_url("asylum_seekers/capture") ?>";
			Webcam.upload(data_uri, url , function(code, text) {
				document.getElementById('webcam-preview').innerHTML = '<img width="250" src="'+text+'" />';
			});	
		} );
	}
</script>
<style type="text/css">
	#webcam video{
		border:5px solid #999;
		background:#000;
	}
	#webcam-preview img{
		width: 259px;
		min-height:200px;
		border:5px solid #999;
		background:#000;
	}
</style>

<!---------///////////////----------->



