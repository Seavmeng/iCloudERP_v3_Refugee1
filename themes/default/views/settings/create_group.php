<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('create_group'); ?></h4>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form');
        echo form_open("system_settings/create_group", $attrib); ?>
        <div class="modal-body">
            <p><?= lang('enter_info'); ?></p>

            <div class="form-group">
                <?= lang("group_name", "group_name"); ?></label>
                <?php echo form_input('group_name', '', 'class="form-control" id="group_name" required="required"'); ?>
            </div>
			
			<div class="form-group">
				<?php
					$group_detail = array(""=>lang("select"));
					foreach($groups as $group){
						$group_detail[$group->id] = $group->description;
					}
				?>
                <?= lang("group", "group"); ?></label>
                <?php echo form_dropdown('parent_id',$group_detail, 0, 'class="form-control" id="parent_id"'); ?>
            </div>
			
			<div class="form-group">
				<?php
					$type_detail = array("K1"=>"K1","K2"=>"K2","K3"=>"K3","K4"=>"K4");					
				?>
                <?= lang("type", "type"); ?>
                <?php echo form_dropdown('type',$type_detail, $group->type, 'class="form-control" id="type" required="required"'); ?>
            </div>

            <div class="form-group">
                <?= lang("description", "description"); ?></label>
                <?php echo form_input('description', '', 'class="form-control" id="description"'); ?>
            </div>

        </div>
        <div class="modal-footer">
            <?php echo form_submit('create_group', lang('create_group'), 'class="btn btn-primary"'); ?>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>
<?= $modal_js ?>
