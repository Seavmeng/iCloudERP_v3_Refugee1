<style>
    .table td:first-child {
        font-weight: bold;
    }

    label {
        margin-right:10px;
    }
	.padding0{
		padding:0px;
	}
</style>
<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-folder-open"></i><?= lang('group_permissions_each'); ?></h2>
    </div>
    <div class="box-content">
		<div class="row">
		<div class="col-lg-12">
			<p class="introtext"><?= lang("set_permissions"); ?></p>

		<?php echo form_open("system_settings/permissions/" . $id); ?>		
			<div class="table-responsive">
					<table class="table table-bordered table-hover table-striped" style="white-space:nowrap;">                        
						<thead>
							<tr>
								<th colspan="8" class="text-center">
									<?php echo $group->description . ' ( ' . $group->name . ' ) ' . $this->lang->line("group_permissions_each"); ?>
								</th>
							</tr>
							<tr>
								<th rowspan="2" class="text-center"><?= lang("module_name"); ?></th>
								<th colspan="7" class="text-center"><?= lang("permissions"); ?></th>
							</tr>
							<tr>
								<th class="text-center"><?= lang("view"); ?></th>
								<th class="text-center"><?= lang("add"); ?></th>
								<th class="text-center"><?= lang("edit"); ?></th>
								<th class="text-center"><?= lang("delete"); ?></th>
								<th class="text-center"><?= lang("attachment"); ?></th>
								<th class="text-center"><?= lang("download"); ?></th>
								<th class="text-center"><?= lang("misc"); ?></th>
							</tr>
						</thead> 
						<tbody>	
						<?php if($group->type == "K1"){?>
						<div class="panel">
							<tr>
								<td colspan="0" class="red" ><input type="checkbox" name="administrative_manage_all" class="chckHead">&nbsp;&nbsp;<?= lang("administrative_manage"); ?>	
								</td> 
							</tr>
							<tr>
								<td><?= lang("recognition_refugee"); ?></td>
								<td class="text-center">
									<input type="checkbox" value="1" class="checkbox chcheck" name="recognition_refugees-index" <?php echo $p->{'recognition_refugees-index'} ? "checked" : ''; ?> />
								</td>
								<td class="text-center">
									<input type="checkbox" value="1" class="checkbox chcheck" name="recognition_refugees-add" <?php echo $p->{'recognition_refugees-add'} ? "checked" : ''; ?> />
								</td>
								<td class="text-center">
									<input type="checkbox" value="1" class="checkbox chcheck" name="recognition_refugees-edit" <?php echo $p->{'recognition_refugees-edit'} ? "checked" : ''; ?> />
								</td>
								<td class="text-center">
									<input type="checkbox" value="1" class="checkbox chcheck" name="recognition_refugees-delete" <?php echo $p->{'recognition_refugees-delete'} ? "checked" : ''; ?> />
								</td>
								<td class="text-center">
									<input type="checkbox" value="1" class="checkbox chcheck" name="recognition_refugees-attachment"  <?php echo $p->{'recognition_refugees-attachment'} ? "checked" : ''; ?>/>
								</td>
								<td class="text-center">
									<input type="checkbox" value="1" class="checkbox chcheck" name="recognition_refugees-download"  <?php echo $p->{'recognition_refugees-download'} ? "checked" : ''; ?>/>
								</td>
								<td class="text-center">
									<div class="col-sm-6 text-left">  
										<input type="checkbox" value="1" class="checkbox chcheck" name="recognition_refugees-approved"   <?php echo $p->{'recognition_refugees-approved'} ? "checked" : ''; ?>/>	<label for="products-cost" ><?= lang("approve_recognition_refugee");?></label>
									</div>
									<div class="col-sm-6 text-left">  
										<input type="checkbox" value="1" class="checkbox chcheck" name="recognition_refugees-form"    <?php echo $p->{'recognition_refugees-form'} ? "checked" : ''; ?>/>	<label for="products-cost" ><?= lang("recognition_form");?></label>
									</div>
								</td>
							</tr>
							<tr>
								<td><?= lang("permanent_resident");?></td>
								<td class="text-center">
									<input type="checkbox" value="1" class="checkbox chcheck" name="permanent_resident-index" <?php echo $p->{'permanent_resident-index'}?"checked" : '';?> />
								</td>
								<td class="text-center">
									<input type="checkbox" value="1" class="checkbox chcheck" name="permanent_resident-add" <?php echo $p->{'permanent_resident-add'}?"checked" : '';?> />
								</td>
								<td class="text-center">
									<input type="checkbox" value="1" class="checkbox chcheck" name="permanent_resident-edit" <?php echo $p->{'permanent_resident-edit'}?"checked" : '';?> />
								</td>
								<td class="text-center">
									<input type="checkbox" value="1" class="checkbox chcheck" name="permanent_resident-delete" <?php echo $p->{'permanent_resident-delete'}?"checked" : '';?> />
								</td>
								<td class="text-center">
									<input type="checkbox" value="1" class="checkbox chcheck" name="permanent_resident-attachment" <?php echo $p->{'permanent_resident-attachment'}?"checked" : '';?> />
								</td>
								<td class="text-center">
									<input type="checkbox" value="1" class="checkbox chcheck" name="permanent_resident-download" <?php echo $p->{'permanent_resident-download'}?"checked" : '';?> />
								</td>
								<td class="text-center">
									<div class="col-sm-6 text-left">  
										<input type="checkbox" value="1" class="checkbox chcheck" name="permanent_resident-approved" <?php echo $p->{'permanent_resident-approved'}?"checked" : '';?>  />	<label for="products-cost" ><?= lang("request_card_option");?></label>
									</div>
									<div class="col-sm-6 text-left">  
										<input type="checkbox" value="1" class="checkbox chcheck" name="permanent_resident-form" <?php echo $p->{'permanent_resident-form'}?"checked" : '';?>   />	<label for="products-cost" ><?= lang("permanent_resident");?></label>
									</div>
									<div class="col-sm-6 text-left">  
										<input type="checkbox" value="1" class="checkbox chcheck" name="permanent_resident-print"  <?php echo $p->{'permanent_resident-print'}?"checked" : '';?>  />	<label for="products-cost" ><?= lang("print_permanent_resident_card_refugee");?></label>
									</div>
								</td>
							</tr>
							<tr>
								<td><?= lang("refugee_card");?></td>
								<td class="text-center">
									<input type="checkbox" value="1" class="checkbox chcheck" name="refugee_card-index" <?php echo $p->{'refugee_card-index'}?"checked" : '';?> />
								</td>
								<td class="text-center">
									<input type="checkbox" value="1" class="checkbox chcheck" name="refugee_card-add" <?php echo $p->{'refugee_card-add'}?"checked" : '';?> />
								</td>
								<td class="text-center">
									<input type="checkbox" value="1" class="checkbox chcheck" name="refugee_card-edit" <?php echo $p->{'refugee_card-edit'}?"checked" : '';?> />
								</td>
								<td class="text-center">
									<input type="checkbox" value="1" class="checkbox chcheck" name="refugee_card-delete" <?php echo $p->{'refugee_card-delete'}?"checked" : '';?> />
								</td>
								<td class="text-center">
									<input type="checkbox" value="1" class="checkbox chcheck" name="refugee_card-attachment" <?php echo $p->{'refugee_card-attachment'}?"checked" : '';?>/>
								</td>
								<td class="text-center">
									<input type="checkbox" value="1" class="checkbox chcheck" name="refugee_card-download" <?php echo $p->{'refugee_card-download'}?"checked" : '';?>/>
								</td>
								<td class="text-center">
									<div class="col-sm-6 text-left">  
										<input type="checkbox" value="1" class="checkbox chcheck" name="refugee_card-approved"  <?php echo $p->{'refugee_card-approved'}?"checked" : '';?>/>	<label for="products-cost" ><?= lang("request_card_option");?></label>
									</div>
									<div class="col-sm-6 text-left">  
										<input type="checkbox" value="1" class="checkbox chcheck" name="refugee_card-form"   <?php echo $p->{'refugee_card-form'}?"checked" : '';?>/>	<label for="products-cost" ><?= lang("refugee_card_form");?></label>
									</div>
									<div class="col-sm-6 text-left">  
										<input type="checkbox" value="1" class="checkbox chcheck" name="refugee_card-print"   <?php echo $p->{'refugee_card-print'}?"checked" : '';?>/>	<label for="products-cost" ><?= lang("print_card");?></label>
									</div>
								</td>
							</tr> 
							<tr>
								<td><?= lang("travel_document_refugee"); ?></td>
								<td class="text-center">
								  <input type="checkbox" value="1" class="checkbox chcheck" name="travel_document_refugee-index" <?php echo $p->{'travel_document_refugee-index'} ? "checked" : ''; ?> />
								</td>
								<td class="text-center">
								  <input type="checkbox" value="1" class="checkbox chcheck" name="travel_document_refugee-add" <?php echo $p->{'travel_document_refugee-add'} ? "checked" : ''; ?> />
								</td>
								<td class="text-center">
								  <input type="checkbox" value="1" class="checkbox chcheck" name="travel_document_refugee-edit" <?php echo $p->{'travel_document_refugee-edit'} ? "checked" : ''; ?> />
								</td>
								<td class="text-center">
								  <input type="checkbox" value="1" class="checkbox chcheck" name="travel_document_refugee-delete" <?php echo $p->{'travel_document_refugee-delete'} ? "checked" : ''; ?> />
								</td>
								<td class="text-center">
									<input type="checkbox" value="1" class="checkbox chcheck" name="travel_document_refugee-attachment" <?php echo $p->{'travel_document_refugee-attachment'} ? "checked" : ''; ?>/>
								</td>
								<td class="text-center">
									<input type="checkbox" value="1" class="checkbox chcheck" name="travel_document_refugee-download" <?php echo $p->{'travel_document_refugee-download'} ? "checked" : ''; ?>/>
								</td>
								<td class="text-center">
									<div class="col-sm-6 text-left">  
										<input type="checkbox" value="1" class="checkbox chcheck" name="travel_document_refugee-approved" <?php echo $p->{'travel_document_refugee-approved'} ? "checked" : ''; ?> />	<label for="products-cost" ><?= lang("travel_document_option");?></label>
									</div>
									<div class="col-sm-6 text-left">  
										<input type="checkbox" value="1" class="checkbox chcheck" name="travel_document_refugee-form"  <?php echo $p->{'travel_document_refugee-form'} ? "checked" : ''; ?> />	<label for="products-cost" ><?= lang("travel_document_of_refugee_request_form");?></label>
									</div> 
								</td>
							  </tr>
												
								<tr>
									<td><?= lang("cessation_elimination_refugee")?></td>
									<td class="text-center">
									  <input type="checkbox" value="1" class="checkbox chcheck" name="cessation_refugee_declaration-index" <?php echo $p->{'cessation_refugee_declaration-index'} ? "checked" : ''; ?>>
									</td>
									<td class="text-center">
									  <input type="checkbox" value="1" class="checkbox chcheck" name="cessation_refugee_declaration-add" <?php echo $p->{'cessation_refugee_declaration-add'} ? "checked" : ''; ?>>
									</td>
									<td class="text-center">
									  <input type="checkbox" value="1" class="checkbox chcheck" name="cessation_refugee_declaration-edit" <?php echo $p->{'cessation_refugee_declaration-edit'} ? "checked" : ''; ?>>
									</td>
									<td class="text-center">
									  <input type="checkbox" value="1" class="checkbox chcheck" name="cessation_refugee_declaration-delete" <?php echo $p->{'cessation_refugee_declaration-delete'} ? "checked" : ''; ?>>
									</td>
									<td class="text-center">
										<input type="checkbox" value="1" class="checkbox chcheck" name="cessation_refugee_declaration-attachment"  <?php echo $p->{'cessation_refugee_declaration-attachment'} ? "checked" : ''; ?>/>
									</td>
									<td class="text-center">
										<input type="checkbox" value="1" class="checkbox chcheck" name="cessation_refugee_declaration-download"  <?php echo $p->{'cessation_refugee_declaration-download'} ? "checked" : ''; ?>/>
									</td>
									<td class="text-center">
										<div class="col-sm-6 text-left">  
											<input type="checkbox" value="1" class="checkbox chcheck" name="cessation_refugee_declaration-approved"  <?php echo $p->{'cessation_refugee_declaration-approved'} ? "checked" : ''; ?> />	<label for="products-cost" ><?= lang("approve_cessation_refugee");?></label>
										</div>
										<div class="col-sm-6 text-left">  
											<input type="checkbox" value="1" class="checkbox chcheck" name="cessation_refugee_declaration-form"   <?php echo $p->{'cessation_refugee_declaration-form'} ? "checked" : ''; ?> />	<label for="products-cost" ><?= lang("cessation_refugee_declaration_form");?></label>
										</div>
										<div class="col-sm-6 text-left">  
											<input type="checkbox" value="1" class="checkbox chcheck" name="cessation_refugee_declaration-form_declaration"   <?php echo $p->{'cessation_refugee_declaration-form_declaration'} ? "checked" : ''; ?> />	<label for="products-cost" ><?= lang("cessation_elimination_refugee");?></label>
										</div> 
									</td>
								</tr>
								<tr>
									<td><?= lang("withdrawal_refugee")?></td>
									<td class="text-center">
										<input type="checkbox" value="1" class="checkbox chcheck" name="withdrawal_refugee-index" <?php echo $p->{'withdrawal_refugee-index'} ? "checked" : ''; ?>>
									</td>
									<td class="text-center">
										<input type="checkbox" value="1" class="checkbox chcheck" name="withdrawal_refugee-add" <?php echo $p->{'withdrawal_refugee-add'} ? "checked" : ''; ?>>
									</td>
									<td class="text-center">
										<input type="checkbox" value="1" class="checkbox chcheck" name="withdrawal_refugee-edit" <?php echo $p->{'withdrawal_refugee-edit'} ? "checked" : ''; ?>>
									</td>
									<td class="text-center">
										<input type="checkbox" value="1" class="checkbox chcheck" name="withdrawal_refugee-delete" <?php echo $p->{'withdrawal_refugee-delete'} ? "checked" : ''; ?>>
									</td> 
									<td class="text-center">
										<input type="checkbox" value="1" class="checkbox chcheck" name="withdrawal_refugee-attachment"  <?php echo $p->{'withdrawal_refugee-attachment'} ? "checked" : ''; ?>/>
									</td>
									<td class="text-center">
										<input type="checkbox" value="1" class="checkbox chcheck" name="withdrawal_refugee-download"  <?php echo $p->{'withdrawal_refugee-download'} ? "checked" : ''; ?>/>
									</td>
									<td class="text-center">
										<div class="col-sm-6 text-left">  
											<input type="checkbox" value="1" class="checkbox chcheck" name="withdrawal_refugee-approved"   <?php echo $p->{'withdrawal_refugee-approved'} ? "checked" : ''; ?>/>	<label for="products-cost" ><?= lang("approve_withdrawal_refugee");?></label>
										</div>
										<div class="col-sm-6 text-left">  
											<input type="checkbox" value="1" class="checkbox chcheck" name="withdrawal_refugee-form_declaration"  <?php echo $p->{'withdrawal_refugee-form_declaration'} ? "checked" : ''; ?>  />	<label for="products-cost" ><?= lang("withdrawal_refugee_declaration_form");?></label>
										</div> 
										<div class="col-sm-6 text-left">  
											<input type="checkbox" value="1" class="checkbox chcheck" name="withdrawal_refugee-form"  <?php echo $p->{'withdrawal_refugee-form'} ? "checked" : ''; ?>  />	<label for="products-cost" ><?= lang("withdrawal_refugee");?></label>
										</div> 
									</td>
								</tr>
								<tr>
									<td><?= lang("administration_control_report")?></td>
									<td class="text-center">
										<input type="checkbox" value="1" class="checkbox chcheck" name="administration_control_report-index" <?php echo $p->{'administration_control_report-index'} ? "checked" : ''; ?>>
									</td> 
									<td class="text-center"> 
									</td>
									<td class="text-center"> 
									</td>
									<td class="text-center"> 
									</td>
									<td class="text-center"> 
									</td>
									<td class="text-center"> 
									</td>
									<td class="text-center"> 
									</td> 
								</tr>
						</div>
						<?php }?>
						<?php if($group->type == "K2"){?>
						<div class="rar">
							<tr>
								<td colspan="8" class="red checkallRar" ><input type="checkbox" name="Receiving_applications_registrations_all" class="chckHead"><?= lang("Receiving_applications_registrations"); ?></td>								
							</tr>
								<tr>
									<td><?= lang("counseling"); ?></td>
									<td class="text-center">
										<input type="checkbox" value="1" class="checkbox chcheckRar" name="counseling-index" <?php echo $p->{'counseling-index'} ? "checked" : ''; ?>>
									</td>
									<td class="text-center">
										<input type="checkbox" value="1" class="checkbox chcheckRar" name="counseling-add" <?php echo $p->{'counseling-add'} ? "checked" : ''; ?>>
									</td>
									<td class="text-center">
										<input type="checkbox" value="1" class="checkbox chcheckRar" name="counseling-edit" <?php echo $p->{'counseling-edit'} ? "checked" : ''; ?>>
									</td>
									<td class="text-center">
									  <input type="checkbox" value="1" class="checkbox chcheckRar" name="counseling-delete" <?php echo $p->{'counseling-delete'} ? "checked" : ''; ?>>
									</td>
									<td class="text-center">
										<input type="checkbox" value="1" class="checkbox chcheckRar" name="counseling-attachment"  <?php echo $p->{'counseling-attachment'} ? "checked" : ''; ?>/>
									</td>
									<td class="text-center">
										<input type="checkbox" value="1" class="checkbox chcheckRar" name="counseling-download"  <?php echo $p->{'counseling-download'} ? "checked" : ''; ?>/>
									</td>
									<td class="text-center">
										<div class="col-sm-6 text-left">  
											<input type="checkbox" value="1" class="checkbox chcheckRar" name="counseling-application_form"   <?php echo $p->{'counseling-application_form'} ? "checked" : ''; ?>/>	<label for="products-cost" ><?= lang("counseling_application_form");?></label>
										</div>
										<div class="col-sm-6 text-left">  
											<input type="checkbox" value="1" class="checkbox chcheckRar" name="counseling-approve"  <?php echo $p->{'counseling-approve'} ? "checked" : ''; ?>  />	<label for="products-cost" ><?= lang("approve_counseling");?></label>
										</div> 										
									</td>
								</tr>								
								<tr>
									<td><?= lang("rsd_application"); ?></td>
									<td class="text-center">
										  <input type="checkbox" value="1" class="checkbox chcheckRar" name="rsd_applications-index" <?php echo $p->{'rsd_applications-index'} ? "checked" : ''; ?>>
									</td>					
									<td class="text-center">
									  <input type="checkbox" value="1" class="checkbox chcheckRar" name="rsd_applications-add" <?php echo $p->{'rsd_applications-add'} ? "checked" : ''; ?>>
									</td>						
									<td class="text-center">
									  <input type="checkbox" value="1" class="checkbox chcheckRar" name="rsd_applications-edit" <?php echo $p->{'rsd_applications-edit'} ? "checked" : ''; ?>>
									</td>						
									<td class="text-center">
									  <input type="checkbox" value="1" class="checkbox chcheckRar" name="rsd_applications-delete" <?php echo $p->{'rsd_applications-delete'} ? "checked" : ''; ?>>
									</td>
									<td class="text-center">
										<input type="checkbox" value="1" class="checkbox chcheckRar" name="rsd_applications-attachment"  <?php echo $p->{'rsd_applications-attachment'} ? "checked" : ''; ?>/>
									</td>
									<td class="text-center">
										<input type="checkbox" value="1" class="checkbox chcheckRar" name="rsd_applications-download"  <?php echo $p->{'rsd_applications-download'} ? "checked" : ''; ?>/>
									</td>
									<td class="text-center">
										<div class="col-sm-6 text-left">  
											<input type="checkbox" value="1" class="checkbox chcheckRar" name="rsd_applications-application_form"   <?php echo $p->{'rsd_applications-application_form'} ? "checked" : ''; ?>/>  <label for="products-cost" ><?= lang("rsd_application_form");?></label>
										</div>														
									</td>
								</tr>											
								<tr>
									<td><?= lang("withdrawal_asylum"); ?></td>
									<td class="text-center">
										<input type="checkbox" value="1" class="checkbox chcheckRar" name="withdrawal_of_asylum-index" <?php echo $p->{'withdrawal_of_asylum-index'} ? "checked" : ''; ?>>
									</td>
									<td class="text-center">
										<input type="checkbox" value="1" class="checkbox chcheckRar" name="withdrawal_of_asylum-add" <?php echo $p->{'withdrawal_of_asylum-add'} ? "checked" : ''; ?>>
									</td>
									<td class="text-center">
										<input type="checkbox" value="1" class="checkbox chcheckRar" name="withdrawal_of_asylum-edit" <?php echo $p->{'withdrawal_of_asylum-edit'} ? "checked" : ''; ?>>
									</td>
									<td class="text-center">
										<input type="checkbox" value="1" class="checkbox chcheckRar" name="withdrawal_of_asylum-delete" <?php echo $p->{'withdrawal_of_asylum-delete'} ? "checked" : ''; ?>>
									</td>
									<td class="text-center">
										<input type="checkbox" value="1" class="checkbox chcheckRar" name="withdrawal_of_asylum-attachment"  <?php echo $p->{'withdrawal_of_asylum-attachment'} ? "checked" : ''; ?>/>
									</td>
									<td class="text-center">
										<input type="checkbox" value="1" class="checkbox chcheckRar" name="withdrawal_of_asylum-download"  <?php echo $p->{'withdrawal_of_asylum-download'} ? "checked" : ''; ?>/>
									</td>
									<td class="text-center">
										<div class="col-sm-6 text-left">  
											<input type="checkbox" value="1" class="checkbox chcheckRar" name="withdrawal_of_asylum-application_form"   <?php echo $p->{'withdrawal_of_asylum-application_form'} ? "checked" : ''; ?>/>  <label for="products-cost" ><?= lang("withdrawal_of_asylum_application_form");?></label>
										</div>														
									</td>
								</tr>
								<tr>
									<td><?= lang("preliminary_stay"); ?></td>
									<td class="text-center">
										<input type="checkbox" value="1" class="checkbox chcheckRar" name="preliminary_stay-index" <?php echo $p->{'preliminary_stay-index'} ? "checked" : ''; ?>>
									</td>
									<td class="text-center">
										<input type="checkbox" value="1" class="checkbox chcheckRar" name="preliminary_stay-add" <?php echo $p->{'preliminary_stay-add'} ? "checked" : ''; ?>>
									</td>
									<td class="text-center">
										<input type="checkbox" value="1" class="checkbox chcheckRar" name="preliminary_stay-edit" <?php echo $p->{'preliminary_stay-edit'} ? "checked" : ''; ?>>
									</td>
									<td class="text-center">
										<input type="checkbox" value="1" class="checkbox chcheckRar" name="preliminary_stay-delete" <?php echo $p->{'preliminary_stay-delete'} ? "checked" : ''; ?>>
									</td>
									<td class="text-center">
										<input type="checkbox" value="1" class="checkbox chcheckRar" name="preliminary_stay-attachment"  <?php echo $p->{'preliminary_stay-attachment'} ? "checked" : ''; ?>/>
									</td>
									<td class="text-center">
										<input type="checkbox" value="1" class="checkbox chcheckRar" name="preliminary_stay-download"  <?php echo $p->{'preliminary_stay-download'} ? "checked" : ''; ?>/>
									</td>
									<td class="text-center">
										<div class="col-sm-6 text-left">  
											<input type="checkbox" value="1" class="checkbox chcheckRar" name="preliminary_stay-application_form"   <?php echo $p->{'preliminary_stay-application_form'} ? "checked" : ''; ?>/>  <label for="products-cost" ><?= lang("preliminary_stay_form");?></label>
										</div>														
									</td>
								</tr>
								<tr>
									<td><?= lang("asylum_seekers_report"); ?></td>
									<td class="text-center">
										<input type="checkbox" value="1" class="checkbox chcheckRar" name="asylum_seekers_report-index" <?php echo $p->{'asylum_seekers_report-index'} ? "checked" : ''; ?>>
									</td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td> 
								</tr>
							</div>
						<?php }?>
						<?php if($group->type == "K3"){?>
							<div class="rip">
								<tr>
									<td colspan=8" class="red" ><input type="checkbox" name="research_identify_position_all" class="chckHead"> <?= lang("research_and_determination_office"); ?></td>
								</tr>
								<tr>
									<td><?= lang("appointment_refugees"); ?></td>
									<td class="text-center">
									  <input type="checkbox" value="1" class="checkbox chcheckRIP" name="appointment_refugees-index" <?php echo $p->{'appointment_refugees-index'} ? "checked" : ''; ?>>
									</td>
									<td class="text-center">
									  <input type="checkbox" value="1" class="checkbox chcheckRIP" name="appointment_refugees-add" <?php echo $p->{'appointment_refugees-add'} ? "checked" : ''; ?>>
									</td>
									<td class="text-center">
									  <input type="checkbox" value="1" class="checkbox chcheckRIP" name="appointment_refugees-edit" <?php echo $p->{'appointment_refugees-edit'} ? "checked" : ''; ?>>
									</td>
									<td class="text-center">
										<input type="checkbox" value="1" class="checkbox chcheckRIP" name="appointment_refugees-delete" <?php echo $p->{'appointment_refugees-delete'} ? "checked" : ''; ?>>
									</td>
									<td class="text-center">
										<input type="checkbox" value="1" class="checkbox chcheckRIP" name="appointment_refugees-attachment"  <?php echo $p->{'appointment_refugees-attachment'} ? "checked" : ''; ?>/>
									</td>
									<td class="text-center">
										<input type="checkbox" value="1" class="checkbox chcheckRIP" name="appointment_refugees-download"  <?php echo $p->{'appointment_refugees-download'} ? "checked" : ''; ?>/>
									</td>
									<td class="text-center">
										<div class="col-sm-6 text-left">  
											<input type="checkbox" value="1" class="checkbox chcheckRIP" name="appointment_refugees-application_form"   <?php echo $p->{'appointment_refugees-application_form'} ? "checked" : ''; ?>/>  <label for="products-cost" ><?= lang("appointment_refugee_form");?></label>
										</div>														
									</td>
								</tr>

								<tr>
									<td><?= lang("interview_refugees"); ?></td>
									<td class="text-center">
									  <input type="checkbox" value="1" class="checkbox chcheckRIP" name="interview_refugees-index" <?php echo $p->{'interview_refugees-index'} ? "checked" : ''; ?>>
									</td>
									<td class="text-center">
									  <input type="checkbox" value="1" class="checkbox chcheckRIP" name="interview_refugees-add" <?php echo $p->{'interview_refugees-add'} ? "checked" : ''; ?>>
									</td>
									<td class="text-center">
									  <input type="checkbox" value="1" class="checkbox chcheckRIP" name="interview_refugees-edit" <?php echo $p->{'interview_refugees-edit'} ? "checked" : ''; ?>>
									</td>
									<td class="text-center">
										<input type="checkbox" value="1" class="checkbox chcheckRIP" name="interview_refugees-delete" <?php echo $p->{'interview_refugees-delete'} ? "checked" : ''; ?>>
									</td>
									<td class="text-center">
										<input type="checkbox" value="1" class="checkbox chcheckRIP" name="interview_refugees-attachment"  <?php echo $p->{'interview_refugees-attachment'} ? "checked" : ''; ?>/>
									</td>
									<td class="text-center">
										<input type="checkbox" value="1" class="checkbox chcheckRIP" name="interview_refugees-download"  <?php echo $p->{'interview_refugees-download'} ? "checked" : ''; ?>/>
									</td>
									<td class="text-center">
										<div class="col-sm-6 text-left">  
											<input type="checkbox" value="1" class="checkbox chcheckRIP" name="interview_refugees-application_form"   <?php echo $p->{'interview_refugees-application_form'} ? "checked" : ''; ?>/>  <label for="products-cost" ><?= lang("interview_form");?></label>
										</div>														
									</td>
								</tr>

								<tr>
									<td><?= lang("evaluation_refugees"); ?></td>
									<td class="text-center">
									  <input type="checkbox" value="1" class="checkbox chcheckRIP" name="evaluation_refugees-index" <?php echo $p->{'evaluation_refugees-index'} ? "checked" : ''; ?>>
									</td>
									<td class="text-center">
										<input type="checkbox" value="1" class="checkbox chcheckRIP" name="evaluation_refugees-add" <?php echo $p->{'evaluation_refugees-add'} ? "checked" : ''; ?>>
									</td>
									<td class="text-center">
										<input type="checkbox" value="1" class="checkbox chcheckRIP" name="evaluation_refugees-edit" <?php echo $p->{'evaluation_refugees-edit'} ? "checked" : ''; ?>>
									</td>
									<td class="text-center">
									  <input type="checkbox" value="1" class="checkbox chcheckRIP" name="evaluation_refugees-delete" <?php echo $p->{'evaluation_refugees-delete'} ? "checked" : ''; ?>>
									</td>
									<td class="text-center">
										<input type="checkbox" value="1" class="checkbox chcheckRIP" name="evaluation_refugees-attachment"  <?php echo $p->{'evaluation_refugees-attachment'} ? "checked" : ''; ?>/>
									</td>
									<td class="text-center">
										<input type="checkbox" value="1" class="checkbox chcheckRIP" name="evaluation_refugees-download"  <?php echo $p->{'evaluation_refugees-download'} ? "checked" : ''; ?>/>
									</td>
									<td class="text-center">
										<div class="col-sm-6 text-left">  
											<input type="checkbox" value="1" class="checkbox chcheckRIP" name="evaluation_refugees-procedure_decision"   <?php echo $p->{'evaluation_refugees-procedure_decision'} ? "checked" : ''; ?>/>  <label for="products-cost" ><?= lang("procedure_decision");?></label>
										</div>
										<div class="col-sm-6 text-left">  
											<input type="checkbox" value="1" class="checkbox chcheckRIP" name="evaluation_refugees-extension_notification"   <?php echo $p->{'evaluation_refugees-extension_notification'} ? "checked" : ''; ?>/>  <label for="products-cost" ><?= lang("extension_notification");?></label>
										</div>	
										<div class="col-sm-6 text-left">  
											<input type="checkbox" value="1" class="checkbox chcheckRIP" name="evaluation_refugees-review_evaluation"   <?php echo $p->{'evaluation_refugees-review_evaluation'} ? "checked" : ''; ?>/>  <label for="products-cost" ><?= lang("review_evaluation_refugee");?></label>
										</div>
									</td>
								</tr>
								<tr>
									<td><?= lang("negative_refugee"); ?></td>
									<td class="text-center">
										<input type="checkbox" value="1" class="checkbox chcheckRIP" name="negative_refugee-index" <?php echo $p->{'negative_refugee-index'} ? "checked" : ''; ?>>
									</td>
									<td class="text-center">
									  <input type="checkbox" value="1" class="checkbox chcheckRIP" name="negative_refugee-add" <?php echo $p->{'negative_refugee-add'} ? "checked" : ''; ?>>
									</td>
									<td class="text-center">
									  <input type="checkbox" value="1" class="checkbox chcheckRIP" name="negative_refugee-edit" <?php echo $p->{'negative_refugee-edit'} ? "checked" : ''; ?>>
									</td>
									<td class="text-center">
									  <input type="checkbox" value="1" class="checkbox chcheckRIP" name="negative_refugee-delete" <?php echo $p->{'negative_refugee-delete'} ? "checked" : ''; ?>>
									</td>
									<td class="text-center">
										<input type="checkbox" value="1" class="checkbox chcheckRIP" name="negative_refugee-attachment"  <?php echo $p->{'negative_refugee-attachment'} ? "checked" : ''; ?>/>
									</td>
									<td class="text-center">
										<input type="checkbox" value="1" class="checkbox chcheckRIP" name="negative_refugee-download"  <?php echo $p->{'negative_refugee-download'} ? "checked" : ''; ?>/>
									</td>
									<td class="text-center">
										<div class="col-sm-6 text-left">  
											<input type="checkbox" value="1" class="checkbox chcheckRIP" name="negative_refugee-application_form"   <?php echo $p->{'negative_refugee-application_form'} ? "checked" : ''; ?>/>  <label for="products-cost" ><?= lang("negative_refugee");?></label>
										</div>	
									</td>
								</tr>
									
								<tr>
									<td><?= lang("negative_refugee_appeal"); ?></td>
									<td class="text-center">
										<input type="checkbox" value="1" class="checkbox chcheckRIP" name="negative_refugee_appeal-index" <?php echo $p->{'negative_refugee_appeal-index'} ? "checked" : ''; ?>>
									</td>
									<td class="text-center">
									  <input type="checkbox" value="1" class="checkbox chcheckRIP" name="negative_refugee_appeal-add" <?php echo $p->{'negative_refugee_appeal-add'} ? "checked" : ''; ?>>
									</td>
									<td class="text-center">
									  <input type="checkbox" value="1" class="checkbox chcheckRIP" name="negative_refugee_appeal-edit" <?php echo $p->{'negative_refugee_appeal-edit'} ? "checked" : ''; ?>>
									</td>
									<td class="text-center">
									  <input type="checkbox" value="1" class="checkbox chcheckRIP" name="negative_refugee_appeal-delete" <?php echo $p->{'negative_refugee_appeal-delete'} ? "checked" : ''; ?>>
									</td>
									<td class="text-center">
										<input type="checkbox" value="1" class="checkbox chcheckRIP" name="negative_refugee_appeal-attachment"  <?php echo $p->{'negative_refugee_appeal-attachment'} ? "checked" : ''; ?>/>
									</td>
									<td class="text-center">
										<input type="checkbox" value="1" class="checkbox chcheckRIP" name="negative_refugee_appeal-download"  <?php echo $p->{'negative_refugee_appeal-download'} ? "checked" : ''; ?>/>
									</td>
									<td class="text-center">
										<div class="col-sm-6 text-left">  
											<input type="checkbox" value="1" class="checkbox chcheckRIP" name="negative_refugee_appeal-application_form"   <?php echo $p->{'negative_refugee_appeal-application_form'} ? "checked" : ''; ?>/>  <label for="products-cost" ><?= lang("negative_refugee_appeal");?></label>
										</div>	
									</td>
								</tr>
									
								<tr>
									<td><?= lang("research_sections"); ?></td>
									<td class="text-center">
										<input type="checkbox" value="1" class="checkbox chcheckRIP" name="research_sections-index" <?php echo $p->{'research_sections-index'} ? "checked" : ''; ?>>
									</td> 
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
								</tr>
								
								
								<tr>
									<td><?= lang("interview_appointment_appeal"); ?></td>
									<td class="text-center">
										<input type="checkbox" value="1" class="checkbox chcheckRIP" name="interview_appointment_appeal-index" <?php echo $p->{'interview_appointment_appeal-index'} ? "checked" : ''; ?>>
									</td>
									<td class="text-center">
									  <input type="checkbox" value="1" class="checkbox chcheckRIP" name="interview_appointment_appeal-add" <?php echo $p->{'interview_appointment_appeal-add'} ? "checked" : ''; ?>>
									</td>
									<td class="text-center">
									  <input type="checkbox" value="1" class="checkbox chcheckRIP" name="interview_appointment_appeal-edit" <?php echo $p->{'interview_appointment_appeal-edit'} ? "checked" : ''; ?>>
									</td>
									<td class="text-center">
									  <input type="checkbox" value="1" class="checkbox chcheckRIP" name="interview_appointment_appeal-delete" <?php echo $p->{'interview_appointment_appeal-delete'} ? "checked" : ''; ?>>
									</td>
									<td class="text-center">
										<input type="checkbox" value="1" class="checkbox chcheckRIP" name="interview_appointment_appeal-attachment"  <?php echo $p->{'interview_appointment_appeal-attachment'} ? "checked" : ''; ?>/>
									</td>
									<td class="text-center">
										<input type="checkbox" value="1" class="checkbox chcheckRIP" name="interview_appointment_appeal-download"  <?php echo $p->{'interview_appointment_appeal-download'} ? "checked" : ''; ?>/>
									</td>
									<td class="text-center">
										<div class="col-sm-6 text-left">  
											<input type="checkbox" value="1" class="checkbox chcheckRIP" name="interview_appointment_appeal-form"   <?php echo $p->{'interview_appointment_appeal-form'} ? "checked" : ''; ?>/>  <label for="products-cost" ><?= lang("appointment_refugee_form");?></label>
										</div>	
									</td>
								</tr>
								<tr>
									<td><?= lang("interview_appeal"); ?></td>
									<td class="text-center">
										<input type="checkbox" value="1" class="checkbox chcheckRIP" name="interview_appeal-index" <?php echo $p->{'interview_appeal-index'} ? "checked" : ''; ?>>
									</td>
									<td class="text-center">
									  <input type="checkbox" value="1" class="checkbox chcheckRIP" name="interview_appeal-add" <?php echo $p->{'interview_appeal-add'} ? "checked" : ''; ?>>
									</td>
									<td class="text-center">
									  <input type="checkbox" value="1" class="checkbox chcheckRIP" name="interview_appeal-edit" <?php echo $p->{'interview_appeal-edit'} ? "checked" : ''; ?>>
									</td>
									<td class="text-center">
									  <input type="checkbox" value="1" class="checkbox chcheckRIP" name="interview_appeal-delete" <?php echo $p->{'interview_appeal-delete'} ? "checked" : ''; ?>>
									</td>
									<td class="text-center">
										<input type="checkbox" value="1" class="checkbox chcheckRIP" name="interview_appeal-attachment"  <?php echo $p->{'interview_appeal-attachment'} ? "checked" : ''; ?>/>
									</td>
									<td class="text-center">
										<input type="checkbox" value="1" class="checkbox chcheckRIP" name="interview_appeal-download"  <?php echo $p->{'interview_appeal-download'} ? "checked" : ''; ?>/>
									</td>
									<td class="text-center">
										<div class="col-sm-6 text-left">  
											<input type="checkbox" value="1" class="checkbox chcheckRIP" name="interview_appeal-form"   <?php echo $p->{'interview_appeal-form'} ? "checked" : ''; ?>/>  <label for="products-cost" ><?= lang("interview_form");?></label>
										</div>	
									</td>
								</tr>
								<tr>
									<td><?= lang("interview_evaluate_appeal"); ?></td>
									<td class="text-center">
										<input type="checkbox" value="1" class="checkbox chcheckRIP" name="interview_evaluate_appeal-index" <?php echo $p->{'interview_evaluate_appeal-index'} ? "checked" : ''; ?>>
									</td>
									<td class="text-center">
									  <input type="checkbox" value="1" class="checkbox chcheckRIP" name="interview_evaluate_appeal-add" <?php echo $p->{'interview_evaluate_appeal-add'} ? "checked" : ''; ?>>
									</td>
									<td class="text-center">
									  <input type="checkbox" value="1" class="checkbox chcheckRIP" name="interview_evaluate_appeal-edit" <?php echo $p->{'interview_evaluate_appeal-edit'} ? "checked" : ''; ?>>
									</td>
									<td class="text-center">
									  <input type="checkbox" value="1" class="checkbox chcheckRIP" name="interview_evaluate_appeal-delete" <?php echo $p->{'interview_evaluate_appeal-delete'} ? "checked" : ''; ?>>
									</td>
									<td class="text-center">
										<input type="checkbox" value="1" class="checkbox chcheckRIP" name="interview_evaluate_appeal-attachment"  <?php echo $p->{'interview_evaluate_appeal-attachment'} ? "checked" : ''; ?>/>
									</td>
									<td class="text-center">
										<input type="checkbox" value="1" class="checkbox chcheckRIP" name="interview_evaluate_appeal-download"  <?php echo $p->{'interview_evaluate_appeal-download'} ? "checked" : ''; ?>/>
									</td>
									<td class="text-center">
										<div class="col-sm-6 text-left">  
											<input type="checkbox" value="1" class="checkbox chcheckRIP" name="interview_evaluate_appeal-procedure_decision"   <?php echo $p->{'interview_evaluate_appeal-procedure_decision'} ? "checked" : ''; ?>/>  <label for="products-cost" ><?= lang("procedure_decision");?></label>
										</div>
										<div class="col-sm-6 text-left">  
											<input type="checkbox" value="1" class="checkbox chcheckRIP" name="interview_evaluate_appeal-extension_notification"   <?php echo $p->{'interview_evaluate_appeal-extension_notification'} ? "checked" : ''; ?>/>  <label for="products-cost" ><?= lang("extension_notification");?></label>
										</div>	
										<div class="col-sm-6 text-left">  
											<input type="checkbox" value="1" class="checkbox chcheckRIP" name="interview_evaluate_appeal-review_evaluation"   <?php echo $p->{'interview_evaluate_appeal-review_evaluation'} ? "checked" : ''; ?>/>  <label for="products-cost" ><?= lang("review_evaluation_refugee");?></label>
										</div>
									</td>
								</tr>
								<tr>
									<td><?= lang("negative_evaluate_refugee_appeal"); ?></td>
									<td class="text-center">
										<input type="checkbox" value="1" class="checkbox chcheckRIP" name="negative_evaluate_refugee_appeal-index" <?php echo $p->{'negative_evaluate_refugee_appeal-index'} ? "checked" : ''; ?>>
									</td>
									<td class="text-center">
									  <input type="checkbox" value="1" class="checkbox chcheckRIP" name="negative_evaluate_refugee_appeal-add" <?php echo $p->{'negative_evaluate_refugee_appeal-add'} ? "checked" : ''; ?>>
									</td>
									<td class="text-center">
									  <input type="checkbox" value="1" class="checkbox chcheckRIP" name="negative_evaluate_refugee_appeal-edit" <?php echo $p->{'negative_evaluate_refugee_appeal-edit'} ? "checked" : ''; ?>>
									</td>
									<td class="text-center">
									  <input type="checkbox" value="1" class="checkbox chcheckRIP" name="negative_evaluate_refugee_appeal-delete" <?php echo $p->{'negative_evaluate_refugee_appeal-delete'} ? "checked" : ''; ?>>
									</td>
									<td class="text-center">
										<input type="checkbox" value="1" class="checkbox chcheckRIP" name="negative_evaluate_refugee_appeal-attachment"  <?php echo $p->{'negative_evaluate_refugee_appeal-attachment'} ? "checked" : ''; ?>/>
									</td>
									<td class="text-center">
										<input type="checkbox" value="1" class="checkbox chcheckRIP" name="negative_evaluate_refugee_appeal-download"  <?php echo $p->{'negative_evaluate_refugee_appeal-download'} ? "checked" : ''; ?>/>
									</td>
									<td class="text-center">
										<div class="col-sm-6 text-left">  
											<input type="checkbox" value="1" class="checkbox chcheckRIP" name="negative_evaluate_refugee_appeal-form"   <?php echo $p->{'negative_evaluate_refugee_appeal-form'} ? "checked" : ''; ?>/>  <label for="products-cost" ><?= lang("negative_refugee");?></label>
										</div>	
									</td>
								</tr>
                                <!-- Add Permission K2 into K3 -->
                                <tr>
                                    <td><?= lang("rsd_application"); ?> (ក២) </td>
                                    <td class="text-center">
                                        <input type="checkbox" value="1" class="checkbox chcheckRar" name="rsd_applications-index" <?php echo $p->{'rsd_applications-index'} ? "checked" : ''; ?>>
                                    </td>
                                    <td class="text-center">
                                        <input type="checkbox" value="1" class="checkbox chcheckRar" name="rsd_applications-add" <?php echo $p->{'rsd_applications-add'} ? "checked" : ''; ?>>
                                    </td>
                                    <td class="text-center">
                                        <input type="checkbox" value="1" class="checkbox chcheckRar" name="rsd_applications-edit" <?php echo $p->{'rsd_applications-edit'} ? "checked" : ''; ?>>
                                    </td>
                                    <td class="text-center">
                                        <input type="checkbox" value="1" class="checkbox chcheckRar" name="rsd_applications-delete" <?php echo $p->{'rsd_applications-delete'} ? "checked" : ''; ?>>
                                    </td>
                                    <td class="text-center">
                                        <input type="checkbox" value="1" class="checkbox chcheckRar" name="rsd_applications-attachment"  <?php echo $p->{'rsd_applications-attachment'} ? "checked" : ''; ?>/>
                                    </td>
                                    <td class="text-center">
                                        <input type="checkbox" value="1" class="checkbox chcheckRar" name="rsd_applications-download"  <?php echo $p->{'rsd_applications-download'} ? "checked" : ''; ?>/>
                                    </td>
                                    <td class="text-center">
                                        <div class="col-sm-6 text-left">
                                            <input type="checkbox" value="1" class="checkbox chcheckRar" name="rsd_applications-application_form"   <?php echo $p->{'rsd_applications-application_form'} ? "checked" : ''; ?>/>  <label for="products-cost" ><?= lang("rsd_application_form");?></label>
                                        </div>
                                    </td>
                                </tr>
                                <!-- END -->
						<?php } ?>  
						<?php if($group->type == "K4"){?>
							<div class="ms">
								<tr>
									<td colspan="8" class="red" ><input type="checkbox" name="management_social_all" class="chckHead"> <?= lang("management_social"); ?></td>
								</tr>
								<tr>
									<td><?= lang("family_guarantee_request"); ?></td>
									<td class="text-center">
										<input type="checkbox" value="1" class="checkbox chcheckSM" name="family_guarantee_request-index" <?php echo $p->{'family_guarantee_request-index'} ? "checked" : ''; ?>>
									</td>
									<td class="text-center">
									  <input type="checkbox" value="1" class="checkbox chcheckSM" name="family_guarantee_request-add" <?php echo $p->{'family_guarantee_request-add'} ? "checked" : ''; ?>>
									</td>
									<td class="text-center">
									  <input type="checkbox" value="1" class="checkbox chcheckSM" name="family_guarantee_request-edit" <?php echo $p->{'family_guarantee_request-edit'} ? "checked" : ''; ?>>
									</td>
									<td class="text-center">
									  <input type="checkbox" value="1" class="checkbox chcheckSM" name="family_guarantee_request-delete" <?php echo $p->{'family_guarantee_request-delete'} ? "checked" : ''; ?>>
									</td>
									<td class="text-center">
										<input type="checkbox" value="1" class="checkbox chcheckSM" name="family_guarantee_request-attachment"  <?php echo $p->{'family_guarantee_request-attachment'} ? "checked" : ''; ?>/>
									</td>
									<td class="text-center">
										<input type="checkbox" value="1" class="checkbox chcheckSM" name="family_guarantee_request-download"  <?php echo $p->{'family_guarantee_request-download'} ? "checked" : ''; ?>/>
									</td>
									<td class="text-center">
										<div class="col-sm-6 text-left">  
											<input type="checkbox" value="1" class="checkbox chcheckSM" name="family_guarantee_request-application_form"   <?php echo $p->{'family_guarantee_request-application_form'} ? "checked" : ''; ?>/>  <label for="products-cost" ><?= lang("family_guarantee_request_option");?></label>
										</div>											
										<div class="col-sm-6 text-left">  
											<input type="checkbox" value="1" class="checkbox chcheckSM" name="family_guarantee_request-request_option"   <?php echo $p->{'family_guarantee_request-request_option'} ? "checked" : ''; ?>/>  <label for="products-cost" ><?= lang("family_guarantee_request_form");?></label>
										</div>
									</td>
								</tr>
								<tr>
									<td><?= lang("home_visits"); ?></td>
									<td class="text-center">
									  <input type="checkbox" value="1" class="checkbox chcheckSM" name="home_visits-index" <?php echo $p->{'home_visits-index'} ? "checked" : ''; ?>>
									</td>
									<td class="text-center">
									  <input type="checkbox" value="1" class="checkbox chcheckSM" name="home_visits-add" <?php echo $p->{'home_visits-add'} ? "checked" : ''; ?>>
									</td>
									<td class="text-center">
									  <input type="checkbox" value="1" class="checkbox chcheckSM" name="home_visits-edit" <?php echo $p->{'home_visits-edit'} ? "checked" : ''; ?>>
									</td>
									<td class="text-center">
									  <input type="checkbox" value="1" class="checkbox chcheckSM" name="home_visits-delete" <?php echo $p->{'home_visits-delete'} ? "checked" : ''; ?>>
									</td> 
									<td class="text-center">
										<input type="checkbox" value="1" class="checkbox chcheckSM" name="home_visits-attachment"  <?php echo $p->{'home_visits-attachment'} ? "checked" : ''; ?>/>
									</td>
									<td class="text-center">
										<input type="checkbox" value="1" class="checkbox chcheckSM" name="home_visits-download"  <?php echo $p->{'home_visits-download'} ? "checked" : ''; ?>/>
									</td>
									<td class="text-center">
										<div class="col-sm-6 text-left">  
											<input type="checkbox" value="1" class="checkbox chcheckSM" name="home_visits-application_form_simple"   <?php echo $p->{'home_visits-application_form_simple'} ? "checked" : ''; ?>/>  <label for="products-cost" ><?= lang("home_visit_form_sample");?></label>
										</div>											
										<div class="col-sm-6 text-left">  
											<input type="checkbox" value="1" class="checkbox chcheckSM" name="home_visits-application_form"   <?php echo $p->{'home_visits-application_form'} ? "checked" : ''; ?>/>  <label for="products-cost" ><?= lang("home_visit_form");?></label>
										</div>
									</td>
							  </tr>	
							  <tr>
									<td><?= lang("social_affairs"); ?></td>
									<td class="text-center">
									  <input type="checkbox" value="1" class="checkbox chcheckSM" name="social_affairs-index" <?php echo $p->{'social_affairs-index'} ? "checked" : ''; ?>>
									</td>
									 <td></td>
									 <td></td>
									 <td></td>
									 <td></td>
									 <td></td>
									 <td></td>
							  </tr>	
						<?php } ?>
							</div>							 
							</tbody>
						</table>
					</div>
					<div class="form-actions">
						<button type="submit" class="btn btn-primary"><?=lang('update')?></button>
					</div>
				<?php echo form_close();  ?>
            </div>
        </div>
    </div>
</div>
<!-- Function to check for Administration Management -->
<script type="text/javascript"> 
$(function() {  
	$("input[name='administrative_manage_all']").on("ifChecked",function(){  
		$('.chcheck').iCheck('check');		
	});   
	$("input[name='administrative_manage_all']").on("ifUnchecked",function(){
		$('.chcheck').iCheck('uncheck');
	});
})

// Function to chech for Receiving Application and Registration
$(function() {  
	$("input[name='Receiving_applications_registrations_all']").on("ifChecked",function(){  
		$('.chcheckRar').iCheck('check');		
	});   
	$("input[name='Receiving_applications_registrations_all']").on("ifUnchecked",function(){
		$('.chcheckRar').iCheck('uncheck');
	});
})

// Function to check all for Research and Identify position
$(function() {  
	$("input[name='research_identify_position_all']").on("ifChecked",function(){  
		$('.chcheckRIP').iCheck('check');		
	});   
	$("input[name='research_identify_position_all']").on("ifUnchecked",function(){
		$('.chcheckRIP').iCheck('uncheck');
	});
})

// Function to check all for Management and social affaire
$(function() {  
	$("input[name='management_social_all']").on("ifChecked",function(){  
		$('.chcheckSM').iCheck('check');		
	});   
	$("input[name='management_social_all']").on("ifUnchecked",function(){
		$('.chcheckSM').iCheck('uncheck');
	});
})
</script>
