<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('edit_category'); ?></h4>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form');
        echo form_open_multipart("application_setup/edit_category/".$id,$attrib); ?>
		
        <div class="modal-body">
            <p><?= lang('enter_info'); ?></p>  
			<div class="form-group">
                <?php echo lang('name', 'name'); ?>
				<span class='red'>*</span>
                <div class="controls">
                    <input type="text" value="<?= set_value("name",$result->name);?>" class="form-control" name="name" />
                </div>
            </div>  
			<div class="form-group">
                <?php echo lang('code', 'code'); ?>
				<span class='red'>*</span>
                <div class="controls">
                    <input type="text" value="<?= set_value("code",$result->code);?>" class="form-control" name="code" />
                </div>
            </div>  
			<div class="form-group">
                <?php echo lang('description', 'description'); ?>
				<span class='red'>*</span>
                <div class="controls">
                    <input type="text"  value="<?= set_value("description",$result->description);?>"  class="form-control" name="description" />
                </div>
            </div> 
			<div class="form-group">
                <?php echo lang('status', 'status'); ?>
				<span class='red'>*</span>
				<div class="controls"> 
					<select name="status" id="status" class="form-control">
						<option <?php echo ($result->status=='active')?"selected":""?> value="active" ><?= lang('active');?></option>
						<option <?php echo ($result->status=='inactive')?"selected":""?> value="inactive" ><?= lang('inactive');?></option>
					</select>
					
				</div>
            </div> 
        </div> 
        <div class="modal-footer">
            <?php echo form_submit('add', lang('save'), 'class="btn btn-primary"'); ?>
        </div> 
    </div>
    <?php echo form_close(); ?>
</div>
<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
<?= $modal_js ?>
