<script>
	$(document).ready(function () {
		var oTable = $('#dataTable').dataTable({
			"aaSorting": [[1, "asc"]],
			"aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?= lang('all') ?>"]],
			"iDisplayLength": <?= $Settings->rows_per_page ?>,
			'bProcessing': true, 'bServerSide': true,
			'sAjaxSource': "<?= site_url('application_setup/getRecognitionTemplate/?v=1'.$v) ?>",
			'fnServerData': function (sSource, aoData, fnCallback) {
				aoData.push({
					"name": "<?= $this->security->get_csrf_token_name() ?>",
					"value": "<?= $this->security->get_csrf_hash() ?>"
				});
				$.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
				},
				"aoColumns": [	{"bSortable": false, "mRender": checkbox}, 		
				{"sClass" : "center"},
				{"sClass" : "justify"},
				{"sClass" : "center"},
				{"sClass" : "center"}]
				,'fnRowCallback': function (nRow, aData, iDisplayIndex) {
						var oSettings = oTable.fnSettings();
						var action = $('td:eq(15)', nRow);	
						return nRow;
					},
					"fnFooterCallback": function (nRow, aaData, iStart, iEnd, aiDisplay) {		
					}
				}).fnSetFilteringDelay().dtFilter([            				
					{column_number: 1, filter_default_label: "[<?= lang("id") ?>]", filter_type: "text", data: []},
					{column_number: 2, filter_default_label: "[<?= lang("description") ?>]", filter_type: "text", data: []},
					{column_number: 3, filter_default_label: "[<?= lang("type") ?>]", filter_type: "text", data: []},			
				 			
				], "footer");
	});
</script> 
<ul id="myTab" class="nav nav-tabs">
	<li class="">
		<a href="#content_1" class="tab-grey"><?= lang("recognition_templete"); ?></a>
	</li>
</ul>

<div id="content_1" class="tab-pane fade in">
		<div class="box">
			<div class="box-header">
				<h2 class="blue">
					<i class="fa-fw fa fa-barcode"></i>
					<?= lang("recognition_templete") ?>
				</h2>
				<div class="box-icon">
					<ul class="btn-tasks"> 
						<li class="dropdown">
							<a data-toggle="dropdown" class="dropdown-toggle" href="#">
								<i class="icon fa fa-tasks tip" data-placement="left" title="<?= lang("actions") ?>"></i>
							</a>
							<ul class="dropdown-menu pull-right tasks-menus" role="menu" aria-labelledby="dLabel">
								<li>
									<a href="<?= site_url('application_setup/add_recognition_template'); ?>"  ><i class="fa fa-plus-circle"></i> 
										<?= lang("add_recognition_template"); ?>
									</a>
								</li> 
							</ul>
						</li> 	
					</ul>
				</div>
			</div> 
			<div class="box-content">
				<div class="row">
					<div class="col-md-12">
						<div class="table-responsive">
							<table id="dataTable" class="table table-condensed table-bordered table-hover table-striped">
								<thead>
									<tr>
										<th style="min-width:30px; width: 30px; text-align: center;">
											<input class="checkbox checkth" type="checkbox" name="check"/>
										</th>
										<th  style="min-width:40px; width: 40px; text-align: center;"><?= lang("id") ?></th>
										<th ><?= lang("description") ?></th>
										<th><?= lang("type") ?></th> 
										<th width='100'><?= lang("action") ?></th>
									</tr>
								</thead>
								<tbody>
								</tbody>					
								<tfoot>
									<tr> 
										<th></th>
										<th></th>
										<th></th>
										<th></th>
										<th></th>											
										 
									</tr>
								</tfoot>
							</table>
						</div> 
					</div> 
				</div> 
			</div> 	
		</div>
</div>
	
	
	
									
								