<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('edit_department_refugee'); ?></h4>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form');
        echo form_open_multipart("application_setup/edit_department_refugee/".$id,$attrib); ?>
		
        <div class="modal-body"> 
            <p><?= lang('enter_info'); ?></p>  
			<div class="form-group">
                <?php echo lang('department_kh', 'department_kh'); ?>
				<span class='red'>*</span>
                <div class="controls">
                    <input type="text" value='<?= $result->department_kh?>' class="form-control" name="department_kh" />
                </div>
            </div> 
			<div class="form-group">
                <?php echo lang('department_en', 'department_en'); ?>
				<span class='red'>*</span>
                <div class="controls">
                    <input type="text" value='<?= $result->department_en?>' class="form-control" name="department_en" />
                </div>
            </div> 
			<div class="form-group">
                <?php echo lang('status', 'status'); ?>
				<span class='red'>*</span>
				<div class="controls"> 
					 <?php
						$status = array('inactive'=>'Inactive', 'active' => 'Active');
						echo form_dropdown('status', $status, $result->status, 'id="status" class="form-control" ');
					?>
				</div>
            </div> 
        </div> 
        <div class="modal-footer">
            <?php echo form_submit('save', lang('save'), 'class="btn btn-primary"'); ?>
        </div> 
    </div>
    <?php echo form_close(); ?>
</div>
<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
<?= $modal_js ?>
