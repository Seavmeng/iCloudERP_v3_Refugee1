<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('edit_state'); ?></h4>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form');
        echo form_open_multipart("application_setup/edit_state/".$id,$attrib); ?>
		
        <div class="modal-body">
            <p><?= lang('enter_info'); ?></p>  
			
			<div class="form-group">
                <?php echo lang('code','code'); ?>
				<span class='red'></span>
                <div class="controls">
                    <input type="text"value="<?= set_value('code',$result->code);?>" class="form-control" name="code"  id="code" />
                </div>
            </div> 
			<div class="form-group">
                <?php echo lang('name','name'); ?>
				<span class='red'>*</span>
                <div class="controls">
                    <input type="text"value="<?= set_value('name',$result->name);?> "class="form-control" name="name" id="name" />
                </div>
            </div> 
			<div class="form-group">
                <?php echo lang('native_name', 'native_name'); ?>
				<span class='red'>*</span>
                <div class="controls">
                    <input type="text" value="<?= set_value('native_name',$result->native_name);?>"class="form-control" name="native_name" id="native_name" />
                </div>
            </div> 	
            <div class="form-group">
                <?php echo lang('countries', 'countries'); ?>
				<span class='red'>*</span>
                <div class="controls">
                   <select class="form-control" id="countries" name="countries">
					<?php foreach($country as $row){?> 
							 <option value="<?php echo $row->id?>" <?php echo ($row->id == $result->country_id)?"selected":""?> ><?php echo $row->country?></option>
					<?php } ?>  
			   </select>
                </div>
            </div> 	
        </div> 
        <div class="modal-footer">
            <?php echo form_submit('add', lang('submit'), 'class="btn btn-success save-data"'); ?>
        </div> 
    </div>
    <?php echo form_close(); ?>
</div>
<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
<?= $modal_js ?>
<script type="text/javascript">
	$(".save-data").on('click',function(event){	 
		var code = $('#code').val();
		var countries = $('#countries').val();
		var name = $('#name').val();
		var native_name= $('#native_name').val();
		if(countries == '' ||name == '' || native_name == '' ){
			bootbox.alert('<?= lang("please_select_all");?>');
			return false;
		}  
	});   
</script>
