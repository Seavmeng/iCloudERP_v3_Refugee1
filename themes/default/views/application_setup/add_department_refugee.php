<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('add_department_refugee'); ?></h4>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form');
        echo form_open_multipart("application_setup/add_department_refugee/",$attrib); ?>
		
        <div class="modal-body">
            <p><?= lang('enter_info'); ?></p>  
			<div class="form-group">
                <?php echo lang('department_kh', 'department_kh'); ?>
				<span class='red'>*</span>
                <div class="controls">
                    <input type="text" class="form-control" name="department_kh" />
                </div>
            </div> 
			<div class="form-group">
                <?php echo lang('department_en', 'department_en'); ?>
				<span class='red'>*</span>
                <div class="controls">
                    <input type="text" class="form-control" name="department_en" />
                </div>
            </div> 
			<div class="form-group">
                <?php echo lang('status', 'status'); ?>
				<span class='red'>*</span>
				<div class="controls">
					<?php
					$status = array('inactive'=>'Inactive', 'active' => 'Active');
					echo form_dropdown('status', $status, 0, 'id="status" class="form-control" ');
					?>
				</div>
            </div> 
        </div> 
        <div class="modal-footer">
            <?php echo form_submit('add', lang('add'), 'class="btn btn-primary save-data"'); ?>
        </div> 
    </div>
    <?php echo form_close(); ?>
</div>
<script type="text/javascript">
    $(".save-data").on('click',function(event){  
        var department_kh = $('input[name="department_kh"]').val();
        var department_en = $('input[name="department_en"]').val();
        if( department_kh == '' || department_en == ''){ 
            bootbox.alert('<?= lang("please_select_all");?>');
            return false;
        } 
    });
</script>
<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
<?= $modal_js ?>
