<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('add_question'); ?></h4>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form');
        echo form_open_multipart("application_setup/add_question/",$attrib); ?>
		
        <div class="modal-body">
            <p><?= lang('enter_info'); ?></p>
			
			<div class="form-group">
                <?php echo lang('level', 'level'); ?>
                <span class="red">*</span>
                <div class="controls">
                    <input type="text" class="form-control" name="level" />
                </div>
            </div>
			
			<div class="form-group">
                <?php echo lang('question', 'question'); ?>
                <span class="red">*</span>
                <div class="controls">
                    <input type="text" class="form-control" name="question" />
                </div>
            </div>
			
            <div class="form-group">
                <?php echo lang('question_kh', 'question_kh'); ?>
                <span class="red">*</span>
                <div class="controls">
					<input type="text" class="form-control" name="question_kh" />
                </div>
            </div> 
			
			<div class="form-group">
                <?php echo lang('status', 'status'); ?>
				<div class="controls">
					<?php
					$status = array('inactive'=>'Inactive', 'active' => 'Active');
					echo form_dropdown('status', $status, 0, 'id="status" class="form-control" ');
					?>
				</div>
            </div>
			
        </div>
		
        <div class="modal-footer">
            <?php echo form_submit('add_question', lang('add_question'), 'class="btn btn-primary save-data"'); ?>
        </div>
		
    </div>
    <?php echo form_close(); ?>
</div>
<script type="text/javascript">
    $(".save-data").on('click',function(event){  
        var level = $('input[name="level"]').val();
        var question = $('input[name="question"]').val();
        var question_kh = $('input[name="question_kh"]').val();
        if( level == '' || question == '' || question_kh == '' ){ 
            bootbox.alert('<?= lang("please_select_all");?>');
            return false;
        } 
    });
</script>
<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
<?= $modal_js ?>
