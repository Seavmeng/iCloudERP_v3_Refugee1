
<script>
	$(document).ready(function () {
		var oTable = $('#dataTable').dataTable({
			"aaSorting": [[1, "asc"]],
			"aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?= lang('all') ?>"]],
			"iDisplayLength": <?= $Settings->rows_per_page ?>,
			'bProcessing': true, 'bServerSide': true,
			'sAjaxSource': "<?= site_url('application_setup/getProvinces/?v=1'.$v) ?>",
			'fnServerData': function (sSource, aoData, fnCallback) {
				aoData.push({
					"name": "<?= $this->security->get_csrf_token_name() ?>",
					"value": "<?= $this->security->get_csrf_hash() ?>"
				});
				$.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
				},
				"aoColumns": [	{"bSortable": false, "mRender": checkbox}, 		
				{"sClass" : "center"},
				{"sClass" : "center"}, 
				{"sClass" : "center"}, 
			     {"sClass" : "center"}, 
			     {"sClass" : "center"}, 
				{"sClass" : "center"},null]
				,'fnRowCallback': function (nRow, aData, iDisplayIndex) {
						var oSettings = oTable.fnSettings();
						var action = $('td:eq(15)', nRow);	
						return nRow;
					},
					"fnFooterCallback": function (nRow, aaData, iStart, iEnd, aiDisplay) {		
					}
				}).fnSetFilteringDelay().dtFilter([            				
					{column_number: 1, filter_default_label: "[<?= lang("id") ?>]", filter_type: "text", data: []},
					{column_number: 2, filter_default_label: "[<?= lang("countries") ?>]", filter_type: "text", data: []},
					{column_number: 3, filter_default_label: "[<?= lang("states") ?>]", filter_type: "text", data: []},
					{column_number: 4, filter_default_label: "[<?= lang("code") ?>]", filter_type: "text", data: []},
					{column_number: 5, filter_default_label: "[<?= lang("name") ?>]", filter_type: "text", data: []},	 		
					{column_number: 6, filter_default_label: "[<?= lang("native_name") ?>]", filter_type: "text", data: []},	 		 		
					{column_number: 7, filter_default_label: "[<?= lang("action") ?>]", filter_type: "text", data: []},	 		 		
				], "footer");
	});
</script>
	<div class="box">
		<div class="box-header">
			<h2 class="blue"><i class="fa-fw fa fa-barcode"></i><?= lang('Provinces'); ?></h2>
			<div class="box-icon">
				<ul class="btn-tasks">
					<li class="dropdown">
						<a href="#" class="toggle_up tip" title="<?= lang('hide_form') ?>">
							<i class="icon fa fa-toggle-up"></i>
						</a>
					</li>
					<li class="dropdown">
						<a href="#" class="toggle_down tip" title="<?= lang('show_form') ?>">
							<i class="icon fa fa-toggle-down"></i>
						</a>
					</li>
					
					<li class="dropdown">
						<a data-toggle="dropdown" class="dropdown-toggle" href="#">
							<i class="icon fa fa-tasks tip" data-placement="left" title="<?= lang("actions") ?>"></i>
						</a>
						<ul class="dropdown-menu pull-right tasks-menus" role="menu" aria-labelledby="dLabel">
							<li>
								<a href="<?= site_url('application_setup/add_province'); ?>" data-toggle="modal" data-target="#myModal" ><i class="fa fa-plus-circle"></i> 
									<?= lang("add_province"); ?>
								</a>
							</li>
							<!--<li>
								<a href="#" id="excel" data-action="export_excel"><i class="fa fa-file-excel-o"></i> 
									<?//= lang('export_to_excel') ?>
								</a>
							</li>-->
						</ul>
					</li>
							
				</ul>
			</div>
		</div>
		<div class="box-content">
			<div class="row">
			<?= form_open_multipart("application_setup/provinces"); ?>
				<div class="col-sm-12">
					<p class="introtext"><?= lang('case_prefix'); ?></p>
					<div class="form">				
						<div class="row">
							<div class="col-sm-3">		
								<div class="form-group">
									<?php echo lang('office_name', 'office_name'); ?>
									<?php
										$type =  array("...");
										foreach($question_types as $value){
											$type[$value->id] = $value->question_type;
										}
										echo form_dropdown('type',$type, 0, 'class="form-control" id="type"');
									?>
								</div>
							</div>
							<div class="col-sm-12">
								<div class="form-group">
									<input type="submit" value="<?= lang("search") ?>" class="btn btn-default btn-info" />
								</div>
							</div>
						</div>
					</div>				
					<div class="table-responsive">
						<table id="dataTable" class="text-center table table-condensed table-bordered table-hover table-striped">
							<thead>
								<tr>
									<th style="min-width:30px; width: 30px; text-align: center;">
										<input class="checkbox checkth" type="checkbox" name="check"/>
									</th>
									<th  style="min-width:40px; width: 40px; text-align: center;"><?= lang("id") ?></th>
									<th width="150"><?= lang("countries") ?></th>
									<th width="150"><?= lang("states") ?></th>
									<th width="150"><?= lang("code") ?></th>
									<th width="150"><?= lang("name") ?></th>
									<th width="100"><?= lang("native_name") ?></th>
									<th width="100"><?= lang("action") ?></th>
								</tr>
							</thead>
							<tbody>
							</tbody>					
							<tfoot>
								<tr> 
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>
				<?= form_close(); ?>
			</div>
		</div>
	</div>
<script language="javascript">
    $(document).ready(function () {        
		
		$(".form").slideUp();
		
        $('.toggle_down').click(function () {
            $(".form").slideDown();
            return false;
        });
		
        $('.toggle_up').click(function () {
            $(".form").slideUp();
            return false;
        });
		
    });
</script>	
	
