<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('add_country'); ?></h4>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form');
        echo form_open_multipart("application_setup/add_country/",$attrib); ?>
		
        <div class="modal-body">
            <p><?= lang('enter_info'); ?></p>  
			<div class="form-group">
                <?php echo lang('country_code', 'country_code'); ?>
				<span class='red'>*</span>
                <div class="controls">
                    <input type="text" class="form-control" name="country_code"  id="country_code" />
                </div>
            </div> 
			<div class="form-group">
                <?php echo lang('iso_code', 'iso_code'); ?>
				<span class='red'>*</span>
                <div class="controls">
                    <input type="text" class="form-control" name="iso_code" id="iso_code" />
                </div>
            </div> 
			<div class="form-group">
                <?php echo lang('country', 'country'); ?>
				<span class='red'>*</span>
                <div class="controls">
                    <input type="text" class="form-control" name="country" id="country" />
                </div>
            </div> 	
            <div class="form-group">
                <?php echo lang('native_name', 'native_name'); ?>
				<span class='red'>*</span>
                <div class="controls">
                    <input type="text" class="form-control" name="native_name" id="native_name" />
                </div>
            </div> 	
            <div class="form-group">
                <?php echo lang('region', 'region'); ?>
				<span class='red'>*</span>
                <div class="controls">
                    <input type="text" class="form-control" name="region" id="region"/>
                </div>
            </div> 
        </div> 
        <div class="modal-footer">
            <?php echo form_submit('add', lang('add_country'), 'class="btn btn-success save-data"'); ?>
        </div> 
    </div>
    <?php echo form_close(); ?>
</div>
<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
<?= $modal_js ?>
<script type="text/javascript">
	$(".save-data").on('click',function(event){	 
		var country_code = $('#country_code').val();
		var iso_code = $('#iso_code').val();
		var country = $('#country').val();
		var native_name= $('#native_name').val();
		var region = $('#region').val(); 
		if(country_code == '' ||iso_code == '' ||country == '' || native_name == '' || region == ''  ){ 
			bootbox.alert('<?= lang("please_select_all");?>');
			return false;
		}  
	});    
</script>
