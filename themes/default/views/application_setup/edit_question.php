<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('edit_question'); ?></h4>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form');
        echo form_open_multipart("application_setup/edit_question/".$id,$attrib); ?>
        <div class="modal-body">
		
            <p><?= lang('enter_info'); ?></p>

			<div class="form-group">
                <?php echo lang('level', 'level'); ?>
                <div class="controls">
                    <input type="text" class="form-control" name="level" value="<?= $row->level ?>" />
                </div>
            </div>
			
			<div class="form-group">
                <?php echo lang('question', 'question'); ?>
                <div class="controls">
                    <input type="text" class="form-control" name="question" value="<?= $row->question ?>" />
                </div>
            </div>
			
            <div class="form-group">
                <?php echo lang('question_kh', 'question_kh'); ?>
                <div class="controls">
					<input type="text" class="form-control" name="question_kh" value="<?= $row->question_kh ?>" />
                </div>
            </div> 
			
			<div class="form-group">
                <?php echo lang('status', 'status'); ?>
				<div class="controls">
					<?php
						$status = array('inactive'=>'Inactive', 'active' => 'Active');
						echo form_dropdown('status', $status, $row->status, 'id="status" class="form-control" ');
					?>
				</div>
            </div>
			
        </div>
        <div class="modal-footer">
            <?php echo form_submit('edit_question', lang('edit_question'), 'class="btn btn-primary"'); ?>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>
<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
<?= $modal_js ?>
