	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
				</button>
				<h4 class="modal-title" id="myModalLabel"><?php echo lang('add_tag'); ?></h4>
			</div>
			<?php $attrib = array('data-toggle' => 'validator', 'role' => 'form');
			echo form_open_multipart("application_setup/add_tag/",$attrib); ?>
			
			<div class="modal-body">
				<p><?= lang('enter_info'); ?></p>  
					<div class="form-group">
						<?php echo lang('name', 'name'); ?>
						<span class='red'>*</span>
						<div class="controls">
							<input type="text" class="form-control" name="name" id="name" />
						</div>
					</div> 
					<div class="form-group">
						<?php echo lang('native_name', 'native_name'); ?>
						<span class='red'>*</span>
						<div class="controls">
							<input type="text" class="form-control" name="native_name" id="native_name" />
						</div>
					</div> 	
			</div> 
			<div class="modal-footer">
				<?php echo form_submit('add', lang('submit'), 'class="btn btn-success save-data"'); ?>
			</div> 
		</div>
		<?php echo form_close(); ?>
	</div>
	<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
	<?= $modal_js ?>
	<script type="text/javascript">
		$(".save-data").on('click',function(event){	 
			var name = $('#name').val();
			var native_name= $('#native_name').val();
			if(name == '' ||native_name == '' ){ 
				bootbox.alert('<?= lang("please_select_all");?>');
				return false;
			}  
		}); 
	</script>