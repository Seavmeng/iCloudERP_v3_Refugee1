<div class="modal-dialog" id='myModalOffice'>
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('add_office'); ?></h4>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form');
        echo form_open_multipart("application_setup/add_office/",$attrib); ?>
		
        <div class="modal-body">
            <p><?= lang('enter_info'); ?></p>
			
			<div class="form-group">
                <?php echo lang('office_en', 'Office En'); ?>
				<span class="red">*</span>
                <div class="controls">
                    <input type="text" class="form-control" name="office_en" />
                </div>
            </div>
			
			<div class="form-group">
                <?php echo lang('office_kh', 'Office Kh'); ?>
				<span class="red">*</span>
                <div class="controls">
                    <input type="text" class="form-control" name="office_kh" />
                </div>
            </div>
			
			<div class="form-group">
                <?php echo lang('location', 'location'); ?>
				<span class="red">*</span>
                <div class="controls">
                    <input type="text" class="form-control" name="location" />
                </div>
            </div>
			
            <div class="form-group">
                <?php echo lang('location_kh', 'location_kh'); ?>
				<span class="red">*</span>
                <div class="controls">
					<input type="text" class="form-control" name="location_kh" />
                </div>
            </div> 
			
			<div class="form-group">
                <?php echo lang('status', 'status'); ?>
				<div class="controls">
					<?php
					$status = array('inactive'=>'Inactive', 'active' => 'Active');
					echo form_dropdown('status', $status, 0, 'id="status" class="form-control" ');
					?>
				</div>
            </div>
			
        </div>
		
        <div class="modal-footer">
            <?php echo form_submit('add_office', lang('add_office'), 'class="btn btn-primary save-data"'); ?>
        </div>
		
    </div>
    <?php echo form_close(); ?>
</div>
<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
<?= $modal_js ?>
<script type="text/javascript">
    $(".save-data").on('click',function(event){  
        var office_en = $('input[name="office_en"]').val();
        var office_kh = $('input[name="office_kh"]').val();
        var location = $('input[name="location"]').val();
        var location_kh = $('input[name="location_kh"]').val();
        if(office_en == '' || office_kh == '' || location == '' || location_kh == ''){ 
            bootbox.alert('<?= lang("please_select_all");?>');
            return false;
        } 
    });
</script>
