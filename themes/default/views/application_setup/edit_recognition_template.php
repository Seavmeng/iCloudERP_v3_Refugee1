 
<ul id="myTab" class="nav nav-tabs">
	<li class="">
		<a href="#content_1" class="tab-grey"><?= lang("edit_recognition_template"); ?></a>
	</li>
</ul> 
<?= form_open_multipart("application_setup/edit_recognition_template/".$id); ?>
	 	<div class="box">
			
			<div class="box-header">
				<h2 class="blue">
					<i class="fa-fw fa fa-barcode"></i>
					<?= lang("edit_recognition_template") ?>
				</h2> 
			</div>  
			<div class="box-content">
				<div class='row' >
					<div class="col-sm-6">  
						<div class="form-group">
							<?= lang("​ប្រភេទ៖","​ប្រភេទ៖");?>
							<span class="red">*</span> 
							<select name='case_type' id="case_type" class='form-control'>
								<option value='0'><?= lang("select") ?></option>
									<?php foreach($result as $row){	?>
											<option value="<?= $row->case_prefix?>" <?= ($row->case_prefix==$recognition_template->type)?"selected":''?>><?= $row->case_prefix?></option>
									<?php }?>
							</select> 
						</div>   
					</div> 
					<div class="clearfix"></div>
					<div class="col-sm-6">  
						<div class='form-group'> 
							<?= lang('សម្គាល់៖','សម្គាល់៖');?> 
							<div class="controls">
								<input type="text" value="<?= $recognition_template->name?>" class='form-control' name='note'></input>	
							</div>
						</div> 
					</div> 
				</div>
				<div class="row">
					<div class="col-md-6 col-sm-6">  
						<div class="form-group">
							<?= lang("​ប្រការ២៖","ប្រការ២");?>
							<div class="controls">
								<input type="text" value="<?= $recognition_template->brokar2?>" class="form-control" name="brokar2"></input>
							</div>
						</div>   
					</div> 
					<div class="clearfix"></div> 
					<div class="col-md-6 col-sm-6">  
						<div class='form-group'> 
							<?= lang('ប្រការ៣៖','ប្រការ៣៖');?> 
							<div class="controls">
								<input type="text" value="<?= $recognition_template->brokar3?>" class='form-control' name='brokar3'></input>	
							</div>
						</div> 
					</div> 
				</div>
				<div class="row">
					<div class="col-sm-12">  
						<div class="form-group"> 
								<?= lang('បរិយាយ៖','បរិយាយ៖');?>
								<textarea class="form-group" name="recognition">
									 	<?= $recognition_template->description;?>	
								</textarea> 
						</div>  
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<div class="form-group">
							<div class="controls">
								<button type='submit' class="btn btn-success save" /><?= lang("submit") ?></button>								
							</div>
						</div>
					</div> 
				</div>
			</div>
	</div>
<script type='text/javascript'>
	$(".save").click(function(){
		 var type = $("select[name='case_type']").val();
		 if(type==0){
			bootbox.alert('<?= lang("please_select_all");?>');
			return false; 
		 } 
	});

</script>
	
	
	
									
								