<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('edit_case_prefix'); ?></h4>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form');
        echo form_open_multipart("application_setup/edit_case_prefix/".$id,$attrib); ?>
		
        <div class="modal-body">
            <p><?= lang('enter_info'); ?></p>  
			<div class="form-group">
                <?php echo lang('case_prefix', 'case_prefix'); ?>
				<span class='red'>*</span>
                <div class="controls">
                    <input type="text" value="<?= set_value('case_prefix',$result->case_prefix) ?>" class="form-control" name="case_prefix" />
                </div>
            </div> 
			<div class="form-group">
                <?php echo lang('status', 'status'); ?>
				<span class='red'>*</span>
				<div class="controls">
					<select class='form-control' name='status'>
						<option value='active' <?= ($result->status=='active'?"selected":"")?>><?= lang('active')?></option>
						<option value='inactive' <?= ($result->status=='inactive'?"selected":"")?>><?= lang('inactive')?></option>
					</select> 
				</div>
            </div> 
        </div> 
        <div class="modal-footer">
            <?php echo form_submit('save', lang('save'), 'class="btn btn-primary"'); ?>
        </div> 
    </div>
    <?php echo form_close(); ?>
</div>
<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
<?= $modal_js ?>
