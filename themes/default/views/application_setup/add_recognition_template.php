 
<ul id="myTab" class="nav nav-tabs">
	<li class="">
		<a href="#content_1" class="tab-grey"><?= lang("add_recognition_template"); ?></a>
	</li>
</ul> <?= form_open_multipart("application_setup/add_recognition_template"); ?>
			
	<div class="box">
			
			<div class="box-header">
				<h2 class="blue">
					<i class="fa-fw fa fa-barcode"></i>
					<?= lang("add_recognition_template") ?>
				</h2> 
			</div> 
			<div class="box-content">
				<div class='row' >
					<div class="col-sm-6">  
						<div class="form-group">
							<?= lang('type');?> 
							<span class="red">*</span> 
							<select name='case_type' id="case_type" class='form-control'>
								<option value='0'><?= lang("select") ?></option>
									<?php foreach($result as $row){	?>
											<option value="<?= $row->case_prefix?>"   <?=  $row->case_prefix==$this->input->post("case_type")?"selected":""  ?>><?= $row->case_prefix?></option>
									<?php }?>
							</select> 
						</div>   
					</div> 
					<div class="clearfix"></div>
					<div class="col-md-6 col-sm-6">  
						<div class='form-group'> 
							<?= lang('notice');?> 
							<div class="controls">
								<input type="text"​ value='<?php echo set_value("note");?>' class='form-control' name='note'></input>	
							</div>
						</div> 
					</div> 
				</div>
				<div class="row">
					<div class="col-md-6 col-sm-6">  
						<div class="form-group">
							<?= lang('article2');?>
							<div class="controls">
								<input type="text" value='<?= set_value("brokar2")?>' class="form-control" name="brokar2"></input>
							</div>
						</div>   
					</div> 
					<div class="clearfix"></div> 
					<div class="col-md-6 col-sm-6">  
						<div class='form-group'> 
							<?= lang('article3');?> 
							<div class="controls">
								<input type="text" value='<?= set_value("brokar3")?>' class='form-control' name='brokar3'></input>	
							</div>
						</div> 
					</div> 
				</div>
				<div class="row">
					<div class="col-md-12 col-sm-12">  
						<div class="form-group"> 
								<?= lang('description_art');?>
								<textarea class="form-group" name="recognition">
									<ul class="content-list">
										<li>បានឃើញរដ្ឋធម្មនុញ្ញ នៃព្រះរាជាណាចក្រកម្ពុជា</li>
										<li>បានឃើញព្រះរាជក្រឹត្យលេខ នស/រកត ០៩១៣/៩០៣ ចុះថ្ងៃទី ២៤ ខែ កញ្ញា ឆ្នាំ ២០១៣  ស្ដីពីការតែងតាំងរាជរដ្ឋាភិបាលនៃ ព្រះរាជាណាចក្រកម្ពុជា</li>
										<li>បានឃើញព្រះរាជក្រមលេខ ០២.ន.ស.៩៤ ចុះថ្ងៃទី ២០ ខែ កក្កដា ឆ្នាំ ១៩៩៤ ដែលប្រកាសអោយប្រើច្បាប់ស្ដីពីការរៀបចំ  និងការ ប្រព្រឹត្ដទៅនៃគណៈរដ្ឋមន្ដ្រី  </li>
										<li>បានឃើញព្រះរាជក្រមលេខ នស/រកម ០១៩៦/០៨ ចុះថ្ងៃទី ២៤ ខែ មករា ឆ្នាំ ១៩៩៦   ដែលប្រកាសអោយប្រើច្បាប់ស្ដីពី ការបង្កើត ក្រសួងមហាផ្ទៃ  </li>
										<li>បានឃើញព្រះរាជក្រមលេខ ០៥  នស ៩៤  ចុះថ្ងៃទី ២២ ខែ កញ្ញា ឆ្នាំ ១៩៩៤  ដែលប្រកាសអោយប្រើច្បាប់ស្ដីពីអន្ដោប្រវេសន៍</li>
										<li>បានឃើញអនុសញ្ញាអន្ដរជាតិ ឆ្នាំ ១៩៥១ និង ពិធីសារ ឆ្នាំ ១៩៦៧ ទាក់ទិនទៅនឹងឋានៈនៃជនភៀសខ្លួន  ដែលប្រទេសកម្ពុជាបានក្លាយ ទៅជាភាគីហត្ថលេខី នៅថ្ងៃទី ១៥ ខែ តុលា ឆ្នាំ ១៩៩២  ស្ដីពីឋានៈនៃជនភៀសខ្លួន </li>
										<li>បានឃើញអនុក្រឹត្យលេខ ២២៤ អនក្រ.បក  ចុះថ្ងៃទី ១៧ ខែ ធ្នូ  ឆ្នាំ ២០០៩ ស្ដីពីនីតិវិធីក្នុងការពិនិត្យទទួលស្គាល់ ផ្ដល់ឋានៈជនភៀស ខ្លួន ឬសិទ្ធិជ្រកកោនដល់ជនបរទេស នៅក្នុងព្រះរាជាណាចក្រកម្ពុជា  </li>
										<li>យោងតាមការចំាបាច់របស់ក្រសួងមហាផ្ទៃ</li>
									</ul>		
								</textarea> 
						</div>  
					</div>
					<div class="col-sm-12">
						<div class="form-group">
							<div class="controls">
								<button type='submit' class="btn btn-success save" /><?= lang("submit") ?></button>								
							</div>
						</div>
					</div> 
				</div>
			</div>
	</div>
<script type='text/javascript'>
	$(".save").click(function(){
		 var type = $("select[name='case_type']").val();
		 if(type==0){
			bootbox.alert('<?= lang("please_select_all");?>');
			return false; 
		 } 
	});

</script>
	
	
	
									
								