<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('add_family_relationship'); ?></h4>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form');
        echo form_open_multipart("application_setup/add_family_relationship/",$attrib); ?>
		
        <div class="modal-body">
            <p><?= lang('enter_info'); ?></p>  
			<div class="form-group">
                <?php echo lang('relationship', 'relationship'); ?>
				<span class='red'>*</span>
                <div class="controls">
                    <input type="text" class="form-control" name="relationship" />
                </div>
            </div> 
			<div class="form-group">
                <?php echo lang('relationship_kh', 'relationship_kh'); ?>
				<span class='red'>*</span>
                <div class="controls">
                    <input type="text" class="form-control" name="relationship_kh" />
                </div>
            </div> 
			<div class="form-group">
                <?php echo lang('status', 'status'); ?>
				<span class='red'>*</span>
				<div class="controls">
					<?php
					$status = array('inactive'=>'Inactive', 'active' => 'Active');
					echo form_dropdown('status', $status, 0, 'id="status" class="form-control" ');
					?>
				</div>
            </div> 
        </div> 
        <div class="modal-footer">
            <?php echo form_submit('add', lang('add'), 'class="btn btn-primary save-data"'); ?>
        </div> 
    </div>
    <?php echo form_close(); ?>
</div>
<script type="text/javascript">
    $(".save-data").on('click',function(event){  
        var relationship = $('input[name="relationship"]').val();
        var relationship_kh = $('input[name="relationship_kh"]').val();
        if( relationship == '' || relationship_kh == ''){ 
            bootbox.alert('<?= lang("please_select_all");?>');
            return false;
        } 
    });
</script>
<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
<?= $modal_js ?>
