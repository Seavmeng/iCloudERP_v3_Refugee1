
<?php   
	$icon_check='<span style="font-size:17px;">&#x2611;</span>';
	$icon_uncheck='<span style="font-size:17px;">&#x2610;</span>'; 
?>
<div class="box" >
	<div class="box-content"> 		
			<?php $this->load->view($this->theme."applications/head-cover_1"); ?> 
			
			<div class='clearfix'></div> 
			
			<div class="col-sm-12 text-center">
				<h2 class='text-cover' style="margin-top:0px;">ការអនុញ្ញាតស្នាក់នៅបណ្ដោះអាសន្ឋ</h2>
				<h3>PRELIMINARY APPLICATION REPORT</h3>
			</div>   
		<div class='wrap-content'> 			
			<div class='clearfix'></div>
			<br/> 
			<table width='100%' class="relationship_table">
			<thead>
				<tr>
					<td><b>ល.រ</b></td>
					<td style="width:80px;" class='text-center'><b>រូបភាព</b></td>
					<td  class='text-center'><b>លេខករណី</b></td>				 
					<td  class='text-center'><b>ឈ្មោះ</b></td>
					<td  class='text-center'><b>ឈ្មោះជាអក្សរទ្បាតាំង</b></td>	
					<td  class='text-center'><b>ភេទ</b></td>	
					<td  class='text-center'><b>ថ្ងៃខែឆ្នាំកំណើត</b></td>				 
					<td  class='text-center'><b>សញ្ជាតិ</b></td>				 
					<td  class='text-center'><b>អាសយដ្ឋាន</b></td>				 
					<td  class='text-center'><b>កាលបរិច្ឆេទចេញ</b></td>				 
					<td  class='text-center'><b>មាន​សុពលភាព​ដល់</b></td>
				</tr>
			</thead>
			<tbody>	
						
				<?php
					$results= $this->applications->getAsylumSeekerPreliminaryReports(); 
					if(!$results){
						echo "<tr><td colspan='9'>".lang("nothing_found")."</td></tr>";
					}
				?>
				<?php 
					$total_case = 0;
					$case_no = array();	$total_gender = array();
					foreach($results as $i => $row){ 
						$total_gender[$row->gender] += 1;
						$case_no[$row->case_no] = $row->case_no; 
						$total_case += 1;
						$country = $this->applications->getCountryById($row->country);
						$province = $this->applications->getProvinceById($row->province);
						$commune = $this->applications->getCommunceById($row->commune);						
						$district = $this->applications->getDistrictById($row->district);						
					?>
					<tr>
						<td class="center"><?= $this->erp->toKhmer(($i+1)) ?></td>	
						<td class="center">
							<div class="cover-photo ">
							<?php 						 
								$photo = $row->photo; 
							?> 							
								<?php if(!$relationship){ ?>
									<img src="<?= base_url("assets/uploads/".$photo) ?>"  />
								<?php } else { ?>
									<img src="<?= base_url("assets/uploads/members/".$photo) ?>" />
								<?php } ?> 
							</div>
						</td>
						<td class="center"><?= $row->case_prefix ?><?= $row->case_no ?></td>
						<td class="left"><?= $row->firstname_kh." ".$row->lastname_kh ?></td>
						<td class="left"><?= $row->firstname." ".$row->lastname ?></td>
						<td class="center"><?= lang($row->gender) ?></td>
						<td class="center"><?= $this->erp->toKhmer($this->erp->hrsd($row->dob)) ?></td>
						<td class="center"><?= $row->nationality_kh ?></td>						
						<td class="center">
							<?php								
								echo $commune->native_name." ".$district->native_name." ".$province->native_name;
							?>
						</td>
						<td class="center"><?= $this->erp->toKhmer($this->erp->hrsd($row->created_date)) ?></td>
						<td class="center"><?= $this->erp->toKhmer($this->erp->hrsd($row->valid_until)) ?></td>
					</tr>
				<?php } ?>
				
				<tr>				
					<td class="right" colspan="11">						
						<?= lang("total") ?><?= $this->erp->toKhmer(count($case_no)) ?> <?= lang("ករណី") ?>​ , 
						<?= lang("people") ?><?= $this->erp->toKhmer(($total_gender['female'] + $total_gender['male'])) ?><?= lang("នាក់") ?>,
						<?= lang("female") ?><?= $this->erp->toKhmer((int)$total_gender['female']) ?><?= lang("នាក់") ?> ,
						<?= lang("male") ?><?= $this->erp->toKhmer((int)$total_gender['male']) ?><?= lang("នាក់") ?> 
					</td>
				</tr>				
				</tbody>
			</table>
			<style type="text/css">
				.relationship_table td{
					white-space: nowrap;
					border: 1px solid #000;
				}
				.relationship_table:last-child{
					border:none !important;
				}
				.break_page{
					page-break-after: always;
				}
			</style> 			
		</div>
		
	</div>
	
</div> 
<style type="text/css"> 
	@media print{
		.bblack{ background: white !important; }   
		.modal-content, .box, 
		.modal-header{ border:none!important; }
		.table { border:none !important; }
	}
	.wrap-content {
		min-height:800px;
		font-size:14px;
	}
	.wrap-content table td {
		padding:6px;
		vertical-align: top;
	}
	.attachment {
		width:30%; 
		font-size:9px; 
		padding:0px;
		margin-left: 30px;
	}
	.list{
		white-space:nowrap;
	}
</style>