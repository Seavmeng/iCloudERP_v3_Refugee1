
<?php   
	$icon_check='<span style="font-size:17px;">&#x2611;</span>';
	$icon_uncheck='<span style="font-size:17px;">&#x2610;</span>'; 
?>
<div class="box" >
	<div class="box-content"> 		
			<?php $this->load->view($this->theme."applications/head-cover_1"); ?> 
			
			<div class='clearfix'></div> 
			
			<div class="col-sm-12 text-center">
				<h2 class='text-cover' style="margin-top:0px;">របាយការណ៍កំណត់ត្រាការចុះសួរសុខទុក្ខ</h2>
				<h3>REPORT OF HOME VISIT</h3>
			</div>   
		<div class='wrap-content'> 	 
			<table width='100%' class="relationship_table">
			<thead>	
				<tr>
					<td><b>ល.រ</b></td>
					<td><b>លេខករណី</b></td>
					<td  class='text-center'><b>ឈ្មោះ</b></td>
					<!--<td  class='text-center'><b>ឈ្មោះជាអក្សរទ្បាតាំង</b></td>-->
					<td  class='text-center'><b>ភេទ</b></td>
					<td  class='text-center'><b>ថ្ងៃខែឆ្នាំកំណើត</b></td>
					<td  class='text-center'><b>សញ្ជាតិ</b></td>				 
					<td  class='text-center'><b>ទំនាក់ទំនង</b></td>
					<td  class='text-center'><b>ថ្ងៃចុះសួរសុខទុក្ខ</b></td>
					<td  class='text-center'><b>ទាក់ទងបាន</b></td>
					<td  class='text-center'><b>ជំនួយឧបត្ថម្ភ</b></td>
					<td  class='text-center'><b>មុខរបរ</b></td>
					<td  class='text-center'><b>ធ្លាប់មានពិរុទ្ធ</b></td>
					<td  class='text-center'><b>ផ្ទះលេខ</b></td>
					<td  class='text-center'><b>ឃុំ / សង្កាត់</b></td>
					<td  class='text-center'><b>ស្រុក / ខ័ណ្</b></td>
					<td  class='text-center'><b>ខេត្ត / ក្រុង</b></td>
					<!--<td  class='text-center'><b>ប្រទេស</b></td>-->
				</tr>
			</thead>
			<tbody>	
				 <?php
				 $results= $this->applications->getRefugeeHomeVisitReports();
				 
					if(!$results){
						echo "<tr><td colspan='9'>".lang("nothing_found")."</td></tr>";
					}
					
				?>
				<?php 
					$c_no = array(); $total_gender = array();
					foreach($results as $i => $row){ 					
						$total_gender[$row->gender] += 1;
						$c_no[$row->counseling_no] = $row->counseling_no;
						$country = $this->applications->getCountryById($row->country);
						$province = $this->applications->getProvinceById($row->province);
						$commune = $this->applications->getCommunceById($row->commune);						
						$district = $this->applications->getDistrictById($row->district);
						$relationship = $this->applications->getRelationshipById($row->relationship);
                        $visit = $this->applications->getHomeVisitById($row->id);
						$relationship1_detail = lang("ម្ចាស់ករណី");
						$case_no[$row->case_no] = $row->case_no;
                        $status = array("អត់","មាន");
                        $contact = array("មិនបាន","បាន");
                        $total_day_visit[$row->id] = $row->id;
						if($relationship){
							$relationship1_detail = $relationship->relationship_kh;
						}
					?>  
					<tr> 
						<td class="center">
							<?= $this->erp->toKhmer(($i+1)) ?>
						</td>
						<td class="center"><?= $row->case_prefix ?><?= $row->case_no ?></td>
						<td class="left"><?= $row->firstname_kh." ".$row->lastname_kh."<br>".$row->firstname." ".$row->lastname?></td>
						<!--<td class="left"><?= $row->firstname." ".$row->lastname ?></td>-->
						<td class="center"><?= lang($row->gender) ?></td>
						<td class="center"><?= $this->erp->toKhmer($this->erp->hrsd($row->dob)) ?></td>
						<td class="center"><?= $row->nationality_kh ?></td>
						<td class="center"><?= $relationship1_detail ?></td>
						<td class="center"><?= $this->erp->toKhmer($this->erp->hrsd($row->day_visit)) ?></td>
						<td class="center">
                            <?php
                                if($row->relationship==0){
                                    echo $contact[$visit->is_contact];
                                }
                            ?>
                        </td>
						<td class="center">
                            <?php
                                if($row->relationship==0){
                                    echo $status[$visit->sponsorship_support];
                                }
                            ?>
                        </td>
						<td class="center"><?= $row->occupation_kh; ?></td>
						<td class="center">
                            <?php
                                if($row->relationship==0){
                                    echo $status[$visit->local_authorities_opinion];
                                }
                            ?>
                        </td>
						<td class="center" style="width: 150px; white-space: normal;"><?= $row->address_kh; ?></td>
						<td class="center"><?= $commune->native_name; ?></td>
						<td class="center"><?= $district->native_name; ?></td>
						<td class="center"><?= $province->native_name; ?></td>
						<!--<td class="center"><?= $country->native_name;?></td>-->
					</tr>
				<?php 
					$i++;
				} ?>​
				
				<tr>					
					<td class="right" colspan="17">
						<?= lang("total") ?><?= $this->erp->toKhmer(count($case_no)) ?> <?= lang("ករណី") ?>​
						<!--<?= lang("people") ?><?= $this->erp->toKhmer(($total_gender['female'] + $total_gender['male'])) ?><?= lang("នាក់") ?>,
						<?= lang("female") ?><?= $this->erp->toKhmer((int)$total_gender['female']) ?><?= lang("នាក់") ?> ,
						<?= lang("male") ?><?= $this->erp->toKhmer((int)$total_gender['male']) ?><?= lang("នាក់") ?> -->
					</td>​	
				</tr>
                <tr>
                     <td class="right" colspan="17">
                         <?= lang("ចំនួនការចុះសួរសុខទុក្ខ ") ?><?= $this->erp->toKhmer((int)(count($total_day_visit))) ?> <?= lang("ដង") ?>​
                     </td>
                </tr>
				</tbody>
			</table>
			<style type="text/css">
				.relationship_table td{
					white-space: nowrap;
					border: 1px solid #000;
				}
				.relationship_table:last-child{
					border:none !important;
				}
				.break_page{
					page-break-after: always;
				}
			</style> 			
		</div>
		
	</div>
	
</div> 
<style type="text/css"> 
	@media print{
		.bblack{ background: white !important; }   
		.modal-content, .box, 
		.modal-header{ border:none!important; }
		.table { border:none !important; }
	}
	.wrap-content {
		min-height:800px;
		font-size:14px;
	}
	.wrap-content table td {
		padding:6px;
		vertical-align: top;
	}
	.attachment {
		width:30%; 
		font-size:9px; 
		padding:0px;
		margin-left: 30px;
	}
	.list{
		white-space:nowrap;
	}
</style>