<div class="row no-print">
	<div class="box-content">
		<div class="col-md-3" >		
			<?php echo lang('លំដាប់ដំណាក់កាលនីតិវិធី', ' លំដាប់ដំណាក់កាលនីតិវិធី '); ?> :
			
			<div class="form-group" style="white-space:nowrap;">  	
					<?php 
						$section_details = array(
												"0"	=> lang("មិនទាន់ជ្រើសរើស"),
												"all"	=> lang(" បង្ហាញទាំងអស់"),
												"register"	=> lang(" ទើបចុះឈ្មោះ"),
												"interview_appointment"	=> lang("បានណាត់សម្ភាសន៍"),	
												"interviewed"	=> lang("សម្ភាសន៍រួច"),	
												"interview_evaluate"	=> lang("វិនិច័្ឆយលើការសម្ភាសន៍"),	
												"first_reject" =>lang("បដិសេធជាលើកទីមួយ"),  // stand by
												"first_recognise" =>lang("ទទួលស្គាល់លើកទីមួយ"), //  stand by
												"interview_request_on_appeal"	=> lang("ដាក់ពាក្យស្នើសុំលើបណ្តឹងឧទ្ធរណ៍"),
												"check_out_request_on_appeal" =>lang("ពិនិត្យលេីការស្នេីសុំបណ្តឹងឧទ្ធរណ៏"), //  stand by
												"interview_appointment_on_appeal" => lang("ណាត់សម្ភាសន៍លើបណ្តឹងឧទ្ធរណ៍"),   //stand by
												"interview_on_appeal"	=> lang("សម្ភាសន៍លើបណ្តឹងឧទ្ធរណ៍"),	 
                                                "interview_evaluate_on_appeal"	=> lang("វិនិច័្ឆយលើបណ្តឹងឧទ្ធរណ៍"),
												"reject_on_appeal" =>lang("បដិសេធនៅលើបណ្តឹងឧទ្ធរណ៍"), //  stand by
												"accept_on_appeal" =>lang("ទទួលស្គាល់លើបណ្តឹងឧទ្ធរណ៏"), //  stand by
											);
											
						echo form_dropdown('sector', $section_details, ($_GET['sector']?$_GET['sector']:'0'), ' class="form-control" id="sector"'); 
					?> 
			</div> 
		</div>
	</div>
</div>

	<?php
		
		if($_GET['sector']=='register'){ 
			$data['results'] = $this->applications->getAsylumRegisterReports();
			$this->load->view($this->theme."application_reports/register_report", $data);
		}else if($_GET['sector']=='interview_appointment'){ 
			$data['results'] = $this->applications->getInteviewAppointmentReports();
			$this->load->view($this->theme."application_reports/interview_appointment_report", $data);
		}else if($_GET['sector']=='interviewed'){ 
			$data['results'] = $this->applications->getInteviewedReports();
			$this->load->view($this->theme."application_reports/interviewed_report", $data);
		}else if($_GET['sector']=='interview_on_appeal'){ 
			$data['results'] = $this->applications->getInteviewOnAppealReports();
			$this->load->view($this->theme."application_reports/interview_on_appeal_report", $data);
		}else if($_GET['sector']=='interview_evaluate'){ 
			$data['results'] = $this->applications->getInteviewedEvaluateReports();
			$this->load->view($this->theme."application_reports/interview_evaluate_report", $data);
		}else if($_GET['sector']=='interview_evaluate_on_appeal'){ 
			$data['results'] = $this->applications->getInteviewedEvaluateOnAppealReports();
			$this->load->view($this->theme."application_reports/interview_evaluate_on_appeal_report", $data);
		}else if($_GET['sector']=='interview_negative_appeal'){ 
			$data['results'] = $this->applications->getInterviewNegativeAppealReports();
			$this->load->view($this->theme."application_reports/interview_negative_appeal_report", $data);
		}else if($_GET['sector']=='all'){ 
			$data['results'] = $this->applications->getAllRegisterReports();
			$this->load->view($this->theme."application_reports/all_register_report", $data);
		}else if($_GET['sector']=='interview_request_on_appeal'){ 
			$data['results'] = $this->applications->getInterviewRequestOnAppealReports();
			$this->load->view($this->theme."application_reports/interview_request_on_appeal_report", $data);
		}

		else if($_GET['sector']=='accept_on_appeal'){
			$data['results'] = $this->applications->getAcceptOnAppeal();
			$this->load->view($this->theme."application_reports/accept_on_appeal", $data);
		}
		else if($_GET['sector']=='reject_on_appeal'){
			$data['results'] = $this->applications->getRejectOnAppeal();
			$this->load->view($this->theme."application_reports/reject_on_appeal", $data);
		}
        else if($_GET['sector']=='check_out_request_on_appeal'){
			$data['results'] = $this->applications->getCheckOutRequestOnAppeal();
			$this->load->view($this->theme."application_reports/check_out_request_on_appeal", $data);
		}
		else if($_GET['sector']=='first_reject'){
			$data['results'] = $this->applications->getFirstReject();
			$this->load->view($this->theme."application_reports/first_reject", $data);
		}
		else if($_GET['sector']=='first_recognise'){
			$data['results'] = $this->applications->getFirstRecognise();
			$this->load->view($this->theme."application_reports/first_recognise", $data);
		}
		else if($_GET['sector'] == 'interview_appointment_on_appeal'){
			$data['results'] = $this->applications->getInterviewAppointmentOnAppeal();
			$this->load->view($this->theme."application_reports/interview_appointment_on_appeal_report", $data);
		}

		else{
			$data['results'] = "";
			$this->load->view($this->theme."application_reports/register_report", $data);
		} 
	?> 			
<style>
	@media print{ 
		.bblack{ background: white !important; }   
		.modal-content, .box, 
		.modal-header{ border:none!important; }
		.table { border:none !important; }
	} 
</style>
<script type="text/javascript">
	$(function(){
		$("#sector").on("change",function(){
			var val = $(this).val();
			location.href="<?= site_url("application_reports/research_sections?sector=") ?>"+val;
		})
	});
</script>