<div class="box">
	<div class="box-content">
		<div class="row">			
			<?= form_open("application_reports/social_affairs?sector=".$_GET['sector']); ?>			
				<div class="col-md-3">
					<div class="form-group"> 		
						<?php echo lang('ស្វែងរកតាមអក្សរ', 'ស្វែងរកតាមអក្សរ'); ?> :
						
						<input type="text" value="<?= isset($_POST['q'])?$_POST['q']:''; ?>" class="form-control" name="q" />
					</div> 
				</div>
				<div class="col-md-3">
					<div class="form-group"> 		
						<?php echo lang('លេខករណី', 'លេខករណី	'); ?> :
						
						<input type="text" value="<?= isset($_POST['case_no'])?$_POST['case_no']:''; ?>" class="form-control" name="case_no" />
					</div> 
				</div>
				<div class="col-md-3">
					<div class="form-group"> 		
						<?php echo lang('ថ្ងៃចុះសួរសុខទុក្ខចាប់ពី', 'ថ្ងៃចុះសួរសុខទុក្ខចាប់ពី'); ?> :
						
						<input type="text" autocomplete="off" value="<?= isset($_POST['from'])?$_POST['from']:''; ?>" class="form-control date from" name="from" />
					</div> 
				</div>
				<div class="col-md-3">
					<div class="form-group"> 		
						<?php echo lang('រហូតដល់ថ្ងៃទី', 'រហូតដល់ថ្ងៃទី'); ?> :
						
						<input type="text" autocomplete="off" value="<?= isset($_POST['to'])?$_POST['to']:''; ?>"	 class="form-control date to" name="to" />
					</div> 
				</div>
				<div class="col-md-12">
					<div class="form-group"> 
						<div class="controls">
							<button type="submit" class="search btn btn-primary" />
								<?= lang("ស្វែងរក"); ?> <i class='fa fa-search' aria-hidden='true'></i>
							</button>
							<button type="submit"  class="btn btn-success" name="export">
								<?= lang("អ៊ិចសេល"); ?> <i class="fa fa-file-excel-o" aria-hidden="true"></i>
							</button>
                            <button type="submit" name="print_research"  class="btn btn-info"><?php echo lang("បោះពុម្ព");?> <i class="fa fa-print" target="_blank"></i></a>
                            </button>
						</div> 
					</div> 
				</div>			
			<?= form_close(); ?>
		</div>

		<table class="table table-condensed table-bordered table2excel"> 
			<tbody>
				<tr class="bold text-center bblue" style="color:#FFF;">
					<td width="3%"><?= lang("ល.រ"); ?></td>  				
					<td><?= lang("លេខករណី") ?></td>
					<td><?= lang("ឈ្មោះ"); ?></td>
					<td><?= lang("ឈ្មោះជាអក្សរទ្បាតាំង"); ?></td>
					<td><?= lang("ភេទ"); ?></td>
					<td><?= lang("ថ្ងៃខែឆ្នាំកំណើត"); ?></td>
					<td><?= lang("សញ្ជាតិ"); ?></td>
					<td><?= lang("ទំនាក់ទំនង"); ?></td>
					<td><?= lang("day_visit"); ?></td>
					<td><?= lang("ទាក់ទងបាន"); ?></td>
					<td><?= lang("ជំនួយឧបត្ថម្ភ"); ?></td>
					<td><?= lang("មុខរបរ"); ?></td>
					<!-- <td><?= lang("មានបណ្ណ័សម្គាល់?"); ?></td> -->
					<td><?= lang("ធ្លាប់មានពិរុទ្ធ"); ?></td>
					<td><?= lang("ផ្ទះលេខ"); ?></td>
					<td><?= lang("ឃុំ / សង្កាត់"); ?></td>
					<td><?= lang("ស្រុក / ខ័ណ្ឌ "); ?></td>
					<td><?= lang("ខេត្ត / ក្រុង"); ?></td>
					<!--<td><?= lang("ប្រទេស"); ?></td>-->
				</tr>			
				<?php
					if(!$results){
						echo "<tr><td colspan='19'>".lang("nothing_found")."</td></tr>";
					}
					
				?>
				<?php
					$request_cards = $this->applications->getAllRequestCards();
					$request_card_applications = array();
					foreach($request_cards as $card){
						$request_card_applications[$card->applications_id] = 1;						
					}
				
					$case_no = array();	$total_gender = array();
				    $total_day_visit = array();;
					foreach($results as $i => $row){
						$total_gender[$row->gender] += 1;
						$case_no[$row->case_no] = $row->case_no;						
						$relationship = $this->applications->getRelationshipById($row->relationship);
						$relationship1_detail = lang("ម្ចាស់ករណី");
						if($relationship){
							$relationship1_detail = $relationship->relationship_kh;
						}						
						$country = $this->applications->getCountryById($row->country);
						$province = $this->applications->getProvinceById($row->province);
						$commune = $this->applications->getCommunceById($row->commune);						
						$district = $this->applications->getDistrictById($row->district);
						$visit = $this->applications->getHomeVisitById($row->id);
						$status = array("អត់","មាន");
						$contact = array("មិនបាន","បាន");
						$total_day_visit[$row->id] = $row->id;

					?>
						<tr>
							<td class="center"><?= $this->erp->toKhmer(($i+1)) ?></td>						
							<td class="center"><?= $row->case_prefix ?><?= $row->case_no ?></td>
							<td class="left"><?= $row->firstname_kh." ".$row->lastname_kh ?></td>
							<td class="left"><?= $row->firstname." ".$row->lastname ?></td>
							<td class="center"><?= lang($row->gender) ?></td>
							<td class="left"><?= $this->erp->toKhmer($this->erp->hrsd($row->dob)) ?></td>
							<td class="center"><?= $row->nationality_kh ?></td>
							<td class="center"><?= $relationship1_detail ?></td>
							<td class="center"><?= $this->erp->toKhmer($this->erp->hrsd($row->day_visit)) ?></td>
							<td class="center">
								<?php 
									if($row->relationship==0){										
										echo $contact[$visit->is_contact];
									}
								?>
							</td>
							<td class="center">
								<?php 
									if($row->relationship==0){										
										echo $status[$visit->sponsorship_support];
									}
								?>
							</td>
							<td class="center">
								<?= $row->occupation_kh; ?>
							</td>
						
							<td class="center">
								<?php 
									if($row->relationship==0){										
										echo $status[$visit->local_authorities_opinion];
									}
								?>
							</td>
							<td class="center"><?= $row->address_kh ?></td>
							<td class="center"><?= $commune->native_name; ?></td>
							<td class="center"><?= $district->native_name; ?></td>
							<td class="center"><?= $province->native_name; ?></td>
							<!--<td class="center"><?= $country->native_name;?></td>-->
						</tr>
				<?php } ?>
			</tbody>
			<tfoot>
				<tr>
					<td class="right" colspan="17">						
						<?= lang("total") ?><?= $this->erp->toKhmer(count($case_no)) ?> <?= lang("ករណី") ?>​
						<!--<?= lang("people") ?><?= $this->erp->toKhmer(($total_gender['female'] + $total_gender['male'])) ?><?= lang("នាក់") ?>,
						<?= lang("female") ?><?= $this->erp->toKhmer((int)$total_gender['female']) ?><?= lang("នាក់") ?> ,
						<?= lang("male") ?><?= $this->erp->toKhmer((int)$total_gender['male']) ?><?= lang("នាក់") ?>-->
					</td>
				</tr>
				<tr>
					<td class="right" colspan="17">						
						<?= lang("ចំនួនការចុះសួរសុខទុក្ខ ") ?><?= $this->erp->toKhmer((int)(count($total_day_visit))) ?> <?= lang("ដង") ?>​
					</td>
				</tr>				
			</tfoot>
		</table>
	</div>
</div>

<style type="text/css">	
	.table {
		width:100%;			
		overflow-y:scroll;
		white-space:nowrap;			
	}
</style>


