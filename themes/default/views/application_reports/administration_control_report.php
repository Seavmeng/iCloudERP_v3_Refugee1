<div class="row no-print">
	<div class="box-content">
		<div class="col-md-3" >		
			<?php echo lang('តាមផ្នែករដ្ឋបាល ', 'តាមផ្នែករដ្ឋបាល '); ?> :
			
			<div class="form-group" style="white-space:nowrap;">  	
					<?php 
						$section_details = array(
												"0"	=> lang("មិនទាន់ជ្រើសរើស"),
												"refugee_recognition"	=> lang("ការទទួលស្គាល់ជនភៀសខ្លួន"),
												"refugee_permanent_card"	=> lang("ពាក្យសុំប័ណ្ណស្នាក់នៅ "),
												"refugee_card"	=> lang("ពាក្យសុំប័ណ្ណសំគាល់ជនភៀសខ្លួន "),
												"refugee_travel_document"	=> lang("ឯកសារធ្វើដំណើរជនភៀសខ្លួន"),												
												"refugee_cessation"	=> lang("បែបបទសុំបញ្ឈប់ឋានៈជនភៀសខ្លួន"),												
												"refugee_withdrawal"	=> lang("បែបបទលុបឈ្មោះចេញពីបញ្ជីជនភៀស"),												
												
											); 
						echo form_dropdown('sector', $section_details, ($_GET['sector']?$_GET['sector']:'0'), ' class="form-control" id="sector"'); 
					?> 
			</div> 
		</div>
	</div>
</div>

	<?php
		if($_GET['sector']=='refugee_recognition'){
			$data['results'] = $this->applications->getRefugeeRecognitionReports();
			$this->load->view($this->theme."application_reports/refugee_recognition_report", $data);
		}else if($_GET['sector']=='refugee_permanent_card'){ 
			$data['results'] = $this->applications->getRefugeePermanentReports();
			$this->load->view($this->theme."application_reports/refugee_permanent_card_report", $data);
		}else if($_GET['sector']=='refugee_card'){
			$data['results'] = $this->applications->getRefugeeCardReports();			
			$this->load->view($this->theme."application_reports/refugee_card_report", $data);
		}else if($_GET['sector']=='refugee_travel_document'){
			$data['results'] = $this->applications->getRefugeeTravelDocumentReports();			
			$this->load->view($this->theme."application_reports/refugee_travel_document_report", $data);
		}else if($_GET['sector']=='refugee_cessation'){
			$data['results'] = $this->applications->getRefugeeCessationReports();			
			$this->load->view($this->theme."application_reports/refugee_cessation_report", $data);
		}else if($_GET['sector']=='refugee_withdrawal'){
			$data['results'] = $this->applications->getRefugeeWithdrawalReports();			
			$this->load->view($this->theme."application_reports/refugee_withdrawal_report", $data);
		}else{
			$data['results'] = "";
			$this->load->view($this->theme."application_reports/refugee_recognition_report", $data);
		} 
	?>
<style>
	@media print{ 
		.bblack{ background: white !important; }   
		.modal-content, .box, 
		.modal-header{ border:none!important; }
		.table { border:none !important; }
	} 
</style>
<script type="text/javascript">
	$(function(){
		$("#sector").on("change",function(){
			var val = $(this).val();
			location.href="<?= site_url("application_reports/administration_control_report?sector=") ?>"+val;
		})
	});
</script>