<div class="row no-print">
	<div class="box-content">
		<div class="col-md-3" >		
			<?php echo lang('តាមផ្នែកទទួលពាក្យនិងចុះឈ្មោះ ', 'តាមផ្នែកទទួលពាក្យនិងចុះឈ្មោះ '); ?> :
			
			<div class="form-group" style="white-space:nowrap;">  	
					<?php 
						$section_details = array(
												"0"	=> lang("មិនទាន់ជ្រើសរើស"),
												"counseling"	=> lang("ពាក្យសុំផ្តល់ការប្រឹក្សាបឋម"),
												"asylum_seekers"	=> lang("ទម្រង់ចុះឈ្មោះជនសុំសិទ្ធិជ្រកកោន "),
												"preliminary_stay"	=> lang("ការអនុញ្ញាតស្នាក់នៅបណ្តោះអាសន្ន "),
												"withdrawal_asylum"	=> lang("ទម្រង់ដកពាក្យសុំសិទ្ធិជ្រកកោន"),												
											);
											
						echo form_dropdown('sector', $section_details, ($_GET['sector']?$_GET['sector']:'0'), ' class="form-control" id="sector"'); 
					?> 
			</div> 
		</div>
	</div>
</div>
	<?php
		if($_GET['sector']=='asylum_seekers'){
			$data['results'] = $this->applications->getAsylumSeekerRegisterReports();
			$this->load->view($this->theme."application_reports/asylum_seekers_register_report", $data);
		}
		else if($_GET['sector']=='preliminary_stay'){ 
			$data['results'] = $this->applications->getAsylumSeekerPreliminaryReports();
			$this->load->view($this->theme."application_reports/asylum_seekers_preliminary_report", $data);
		}
		else if($_GET['sector']=='withdrawal_asylum'){
			$data['results'] = $this->applications->getAsylumSeekerWithdrawalReports();			
			$this->load->view($this->theme."application_reports/asylum_seekers_withdrawal_report", $data);
		}else if($_GET['sector']=='counseling'){
			$data['results'] = $this->applications->getAsylumSeekerReports();			
			$this->load->view($this->theme."application_reports/asylum_seekers_counseling_report", $data);
		}else{
			$data['results'] = "";			
			$this->load->view($this->theme."application_reports/asylum_seekers_counseling_report", $data);
		}
	?>
<style>
	@media print{ 
		.bblack{ background: white !important; }   
		.modal-content, .box, 
		.modal-header{ border:none!important; }
		.table { border:none !important; }
	} 
</style>
<script type="text/javascript">
	$(function(){
		$("#sector").on("change",function(){
			var val = $(this).val();
			location.href="<?= site_url("application_reports/asylum_seekers_report?sector=") ?>"+val;
		})
	});
</script>