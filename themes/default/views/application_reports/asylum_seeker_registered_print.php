<div class="box">
	<div class="box-content">
		<div class="row">			
			<?= form_open("application_reports/asylum_seekers_report?sector=".$_GET['sector']); ?>
				
				<div class="col-md-3">
					<div class="form-group"> 		
						<?php echo lang('ស្វែងរកតាមអក្សរ', 'ស្វែងរកតាមអក្សរ'); ?> :
						<span class="red">*</span>
						<input type="text" value="<?= isset($_POST['q'])?$_POST['q']:''; ?>" class="form-control" name="q" />
					</div> 
				</div>
				<div class="col-md-3">
					<div class="form-group"> 		
						<?php echo lang('លេខបឋម', 'លេខបឋម	'); ?> :
						<span class="red">*</span>
						<input type="text" value="<?= isset($_POST['counseling_no'])?$_POST['counseling_no']:''; ?>" class="form-control" name="counseling_no" />
					</div> 
				</div>
				
				<div class="col-md-3">
					<div class="form-group"> 		
						<?php echo lang('ថ្ងៃចុះឈ្មោះ', 'ថ្ងៃចុះឈ្មោះ'); ?> :
						<span class="red">*</span>
						<input type="text" value="<?= isset($_POST['from'])?$_POST['from']:''; ?>" class="form-control date from" name="from" />
					</div> 
				</div>
				<div class="col-md-3">
					<div class="form-group"> 		
						<?php echo lang('រហូតដល់ថ្ងៃទី', 'រហូតដល់ថ្ងៃទី'); ?> :
						<span class="red">*</span>
						<input type="text" value="<?= isset($_POST['to'])?$_POST['to']:''; ?>" class="form-control date to" name="to" />
					</div> 
				</div>
				<div class="col-md-12">
					<div class="form-group"> 
						<div class="controls">
							<button type="submit" class="search btn btn-primary" />
								<?= lang("ស្វែងរក"); ?> <i class='fa fa-search' aria-hidden='true'></i>
							</button>
							<button type="submit" class="btn btn-success" name="export">
								<?= lang("អ៊ិចសេល"); ?> <i class="fa fa-file-excel-o" aria-hidden="true"></i>
							</button>
							<button type="submit" class="btn btn-success" name="report">
								<?= lang("របាយការណ៏"); ?> <i class="fa fa-file-excel-o" aria-hidden="true"></i>
							</button>
						</div> 
					</div> 
				</div>			
			<?= form_close(); ?>
		</div>

		<table class="table table-condensed table-bordered table2excel"> 
			<tr class="bold text-center bblue" style="color:#FFF;">
				<td width="3%"><?= lang("ល.រ"); ?></td>  
				<td><?= lang("លេខបឋម"); ?></td>
				<td><?= lang("លេខករណី") ?></td>
				<td><?= lang("ឈ្មោះ"); ?></td>
				<td><?= lang("ឈ្មោះជាអក្សរទ្បាតាំង"); ?></td>
				<td><?= lang("ភេទ"); ?></td>
				<td><?= lang("ថ្ងៃខែឆ្នាំកំណើត"); ?></td>
				<td><?= lang("សញ្ជាតិ"); ?></td>
				<td><?= lang("អាសយដ្ឋាន"); ?></td>						
				<td><?= lang("ការបរិច្ឆេទចុះឈ្មោះ"); ?></td> 
			</tr>
			<tbody>
				<?php
					if(!$results){
						echo "<tr><td colspan='9'>".lang("nothing_found")."</td></tr>";
					}
				?>
				<?php 
					$case_no = array();	$total_gender = array();
					foreach($results as $i => $row){
						$total_gender[$row->gender] += 1;
						$case_no[$row->case_no] = $row->case_no;
					?>
					<tr>
						<td class="center"><?= $this->erp->toKhmer(($i+1)) ?></td>
						<td class="center"><?= $row->counseling_no ?></td>
						<td class="center"><?= $row->case_prefix ?><?= $row->case_no ?></td>
						<td class="left"><?= $row->firstname_kh." ".$row->lastname_kh ?></td>
						<td class="left"><?= $row->firstname." ".$row->lastname ?></td>
						<td class="center"><?= lang($row->gender) ?></td>
						<td class="center"><?= $this->erp->toKhmer($this->erp->hrsd($row->dob)) ?></td>
						<td class="center"><?= $row->nationality_kh ?></td>
						<td class="center"></td>
						<td class="center"><?= $this->erp->toKhmer($this->erp->hrsd($row->created_date)) ?></td>
					</tr>
				<?php } ?>
			</tbody>
			<tfoot>
				<tr>
					<td></td>					
					<td class="right" colspan="9">
						<?= lang("total") ?><?= $this->erp->toKhmer(count($case_no)) ?> <?= lang("ករណី") ?>​ , 
						<?= lang("female") ?><?= $this->erp->toKhmer($total_gender['female']) ?><?= lang("នាក់") ?> ,
						<?= lang("male") ?><?= $this->erp->toKhmer($total_gender['male']) ?><?= lang("នាក់") ?> ,
						<?= lang("total") ?><?= $this->erp->toKhmer(($total_gender['female'] + $total_gender['male'])) ?><?= lang("នាក់") ?>
					</td>
				</tr>				
			</tfoot>
		</table>
	</div>
</div>
