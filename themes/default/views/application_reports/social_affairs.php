<div class="row no-print">
	<div class="box-content">
		<div class="col-md-3" >		
			<?php echo lang(' ផ្នែកគ្រប់គ្រងនិងកិច្ចកាសង្គម ', ' ផ្នែកគ្រប់គ្រងនិងកិច្ចកាសង្គម '); ?> :
			
			<div class="form-group" style="white-space:nowrap;">  	
					<?php 
						$section_details = array(
												"0"	=> lang("មិនទាន់ជ្រើសរើស"),
												"family_guarantee"	=> lang(" ពាក្យសុំធានាគ្រួសារ"),
												"home_visit"	=> lang("កំណត់ត្រាការចុះសួរសុខទុក្ខ "),											
											);
											
						echo form_dropdown('sector', $section_details, ($_GET['sector']?$_GET['sector']:'counseling'), ' class="form-control" id="sector"'); 
					?> 
			</div> 
		</div>
	</div>
</div>

	<?php
		
		if($_GET['sector']=='family_guarantee'){
			$data['results'] = $this->applications->getRefugeeGuaranteeReports();
			$this->load->view($this->theme."application_reports/refugee_family_guarantee_report", $data);
		}
		else if($_GET['sector']=='home_visit'){
			$data['results'] = $this->applications->getRefugeeHomeVisitReports();
			$this->load->view($this->theme."application_reports/refugee_home_visit_report", $data);
		}else {
			$data['results'] = "";
			$this->load->view($this->theme."application_reports/refugee_home_visit_report", $data);
		}

	?>
			
<style>
	@media print{ 
		.bblack{ background: white !important; }   
		.modal-content, .box, 
		.modal-header{ border:none!important; }
		.table { border:none !important; }
	} 
</style>
<script type="text/javascript">
	$(function(){
		$("#sector").on("change",function(){
			var val = $(this).val();
			location.href="<?= site_url("application_reports/social_affairs?sector=") ?>"+val;
		})
	});
</script>