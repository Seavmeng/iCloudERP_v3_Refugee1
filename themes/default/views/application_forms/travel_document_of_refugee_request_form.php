
<?php  	
	$icon_check='<span style="font-size:17px;">&#x2611;</span>';
	$icon_uncheck='<span style="font-size:17px;">&#x2610;</span>'; 
?>

<div class="box a4" >
	<div class="box-content "> 		 
			<?php $this->load->view($this->theme."application_forms/form_header"); ?> 
			<div class='clearfix'></div>
			<div class="col-sm-12 text-center" >
				<h2 class='text-cover' style="margin-top:0px;">ពាក្យស្នើសុំឯកសារធ្វើដំណើរជនភៀសខ្លួន</h2>
				<h3>REQUEST FOR REFUGEE TRAVEL DOCUMENT</h3>
			</div>    
			  
		<div class='wrap-content'>  
		
			<table width='100%'>
				<tr>
					<td width='20'> -</td>
					<td>
						 លេខករណី / Case Number : 
						<?php echo $result->case_prefix.$result->case_no; ?>
					</td>
					<td  rowspan='4' class='text-right cover-photo'> 
						<?php
							$photo = "no_image.png";
							if(!empty($travel_doc->photo) || !file_exists("assets/uploads/".$travel_doc->photo)){
								$photo = $travel_doc->photo;
							}
						?>
						<img src="<?= base_url("assets/uploads/".$photo); ?>" class="thumb" />
						 
					</td>
				</tr>
				<tr>
					<td width='20'>-</td>
					<td>
					ជាម្ចាស់ករណី / Principle Applicant: <?php echo ($travel_doc->member_id == 0 ? $icon_check : $icon_uncheck);?>
					&nbsp;&nbsp;
					ឬ ជាសមាជិកក្នុងបន្ទុក / Dependent Member: <?php echo ($travel_doc->member_id >= 1 ? $icon_check : $icon_uncheck);?>
					</td>
				</tr>  
				<tr>
					<td width='20'>-</td>
					<td>នាមត្រកូល និងនាមខ្លួន / Surname and Given Name :</td> 
				</tr>    
				<tr>
					<td width='20'></td> 
					<td colspan='2'>
						<?php 
							if($member){ 
								echo $member->lastname_kh.' '.$member->firstname_kh." / ".strtoupper($member->lastname." ".$member->firstname);
							}else {
								echo $result->lastname_kh.' '.$result->firstname_kh." / ".strtoupper($result->lastname." ".$result->firstname);
							}
						?>
					</td>
				</tr>
			</table>
				
				<table width='100%'>
				    <tr>
						<td width='20'>-</td>
						<td>
							<?php 								
								if($member){ 
									$gender = $member->gender;
								}else {
									$gender = $result->gender;
								}
							?>
							
							​ភេទ/ Sex : 
							<span>ប្រុស / Male:&nbsp; <?php echo ($gender == 'male' ? $icon_check : $icon_uncheck); ?></span>
							&nbsp;&nbsp;
							<span>ស្រី / Female:&nbsp; <?php echo ($gender == 'female' ? $icon_check : $icon_uncheck); ?></span>
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							- &nbsp;&nbsp; ថ្ងៃខែឆ្នាំកំណើត  /  Date of  Birth :&nbsp;
							<?php 
								if($member){ 
									 echo $this->erp->hrsd($member->dob);
								}else {
									echo $this->erp->hrsd($result->dob);
									echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
									echo '(ម្ចាស់ករណី)';
								}
							?>
						</td>
					</tr> 
				    <tr>
						<td>-</td>
						<td>
							ទីកន្លែងកំណើត/ Place of Birth : 
							<?php 
								if($member){ 
									echo $member->pob;
								}else {
									echo $result->pob;
								}
							?>
						</td>
                   </tr>
				<tr>
					<td>-</td>
					<td>​សញ្ជាតិ / Nationality : 
						<?php 
							if($member){ 
								echo $member->nationality_kh." / ".$member->nationality;
							}else {
								echo $result->nationality_kh." / ".$result->nationality;
							}
						?>
					</td>
				</tr> 
				
			</table>
				
			<table  width='100%'>
				<tr>
					<td width='20'>-</td>
					<td>
						កំពស់ / Height :   <?= $travel_doc->height ?>
					</td>
					<td>
						-&nbsp;&nbsp; សំបុរ / Complexion : <?= $travel_doc->complexion?>
					</td>
					<td>
						-&nbsp;&nbsp; ផ្ទៃមុខ / Shape of Face :<?= $travel_doc->shape_of_face?>
					</td>
				</tr> 
				<tr>
					<td width='20'>-</td>
					<td>
						សក់ / Hair : <?= $travel_doc->hair?> 
					</td>
					<td>
						-&nbsp;&nbsp; ភ្នែកពណ៍ / Eye Color : <?= $travel_doc->color_of_eyes?>
					</td>
					<td></td>
				</tr>
				<tr>
					<td width='20'>-</td>
					<td colspan="2">ស្លាកស្នាមពិសេស/ Distinguishing Marks: <?= $travel_doc->distinguishing_marks?></td>
				</tr>
				<tr>
			</table>
				
			<table width='100%'>
			   <tr>
				  <td width='20'>-</td>
				  <td>
					 ថ្ងៃខែឆ្នាំ​បានទទួលឋានៈ  / Date of Status Recognition :
					 <?= (!empty($decision)? $this->erp->hrsd($decision->recognized_date):''); ?>
				  </td>
			   </tr>
			   <tr>
				  <td width='20'>-</td>
				  <td>
					 មុខរបរបច្ចុប្បន្ន  / Current Occupation: 
					 <?= $travel_doc->occupation_kh  ?>
				  </td>
			   </tr>
			   <tr>
				  <td width='20' style="vertical-align: top;">‌‌-</td>
				  <td>
					 អាសយដ្ឋាននៅកម្ពុជា  /  Address in Cambodia :   
						<?= $travel_doc->address_kh ?>
						<?= $this->applications->getTagsById($travel_doc->village); ?>,
						<?= $this->applications->getAllAddressKH($travel_doc->commune,"commune"); ?>  
						<?= $this->applications->getAllAddressKH($travel_doc->district,"district"); ?> 
						<?= $this->applications->getAllAddressKH($travel_doc->province,"province"); ?>
						<?= $this->applications->getAllAddressKH($travel_doc->country,"country"); ?> 
				  </td>
			   </tr> 
			   <tr>
				  <td width='20'>-</td>
				  <td> 
					 លេខទូរស័ព្ទទំនាក់ទំនងសាម៉ីខ្លួន   /  Contact Number: 
					<?= $travel_doc->phone ?>
				  </td>
			   </tr>
			   
			   <tr>
				  <td width='20'>- </td>
				  <td>
					 អាសយដ្ឋានក្នុងករណីមានអាសន្ន  / Emergencies Address​: 
					<?= $travel_doc->emergencies_address ?>
				  </td>
			   </tr>
			   <tr>
				  <td width='20'>-</td>
				  <td>
					 ក្នុងករណីមានអាសន្ន សូមទាក់ទង / In Case of Emergencies, Contact​: 
					 <?= $travel_doc->emergencies_contact ?>
				  </td>
			   </tr>
			   <tr>
				  <td colspan="2" class='space-size'>
					 ខ្ញុំបាទ / នាងខ្ញុំសូមគោរពស្នើសុំនាយកដ្ឋានជនភៀសខ្លួន នៃអគ្គនាយកដ្ឋានអន្តោប្រវេសន៍ចេញ​ឯកសារធ្វើ​​ដំណើរ​ជន​ភៀស​ខ្លួន​ប្រើ​ប្រាស់​ជា​ផ្លូវ​ការ សម្រាប់ធ្វើដំណើរទៅប្រទេស <?= $travel_doc->to_country_kh ?> ដើម្បី <?= $travel_doc->travel_for_kh ?>។ រាល់ព័ត៌មាន​ខាង​លើពិត​ជាច្បាស់​លាស់ និងត្រឹមត្រូវ បើខុស​ពីការពិត​ខ្ញុំបាទ / នាងខ្ញុំសូមទទួលខុស​ត្រូវចំពោះមុខច្បាប់។
					 <br/>
					 I hereby would like to request the Refugee Department, General Department of Immigration to issue Refugee Travel Document to travel to <?= $travel_doc->to_country ?> for <?= $travel_doc->travel_for ?>The information provided above is true and accurate. If it is misrepresented or false, I will hold responsibility to the law.
				  </td>
			   </tr>
			</table> 
			 
			<div class="clearfix"></div>	 
				<div style="margin-top:-20px;">
					<?php $this->load->view($this->theme."application_forms/form_footer"); ?>
				</div>
			<div class="clearfix"></div>
			<br/>
			
			<div class="attachment">			
				<center>
					 <p>ភ្ជាប់មកជាមួយនូវឯកសារ ច្បាប់ដើម (បើមាន) </p>
					 <p>Attach with original files:(If applicable)</p>
				</center>
				<ul class="list">
					<li>ប្រកាសជនភៀសខ្លួន  /  Refugee Certificate (Prakas) </li>
					<li>ប័ណ្ណសំគាល់ជនភៀសខ្លួន ឬប័ណ្ណស្នាក់នៅ  /  Refugee card or Resident card</li>
				</ul>  
			</div> 
		</div>
	</div>
</div>
 
<style type="text/css">
	.attachment{ 
		font-size:9px;
		width:40%;
		margin-left: 30px;
	} 
	.attachment p{
		margin:0px;
		padding:0px;
		text-decoration:underline;
		text-align:center;	
	}
	@media print{
		.bblack{ background: white !important; }   
		.modal-content, .box,
		.modal-header{ border:none!important; }
		.table { border:none !important; }
		.box-content{
			padding: 20px 20px 0 20px !important;
		}
	}
	.wrap-content {
		min-height:800px;
		font-size:14px;
	}
	.wrap-content table td {
		padding:4px;
		font-size:14px;
	} 
</style>