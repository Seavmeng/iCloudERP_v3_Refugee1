<div class="head-cover">
	<div class="div-left col-sm-6">
		<table class="left-cover">
			<tr>
				<td class="center">​
					<h2>ក្រសួងមហាផ្ទៃ</h2>
					<span class='text-english'>Ministry of Interior</span> 
				</td>
			</tr> 
			<tr>
				<td class="center">
					<h2>អគ្គនាយកដ្ឋានអន្តោប្រវេសន៍</h2>
					<span class="text-english">General Department of Immigration</span>
				</td>
			</tr>
			<tr>
				<td class="center">
					<h2>នាយកដ្ឋានជនភៀសខ្លួន</h2>
					<span class="text-english">Department of Refugee</span>  <br/>
					<span class='symbol_line'>r1s<span>
				</td>
			</tr>
		</table> 
	</div>
	<div class="div-right text-right">
		<table class="right-cover text-center">
			<tr>
				<td>
					<h2>ព្រះរាជាណាចក្រកម្ពុជា </h2>
					<span class="text-english">Kingdom of Cambodia</span>
				</td>
			</tr>
			<tr>
				<td>
					<h2>ជាតិ សាសនា  ព្រះមហាក្សត្រ</h2>
					<span class="text-english">Nation   Religion   King</span><br/>
					<span class='symbol_line'>3<span>
				</td>
			</tr>
			<tr>
				<td>
					<h2>&nbsp; </h2>
					<span>&nbsp;</span>
				</td>
			</tr>
		</table>
	</div>
	<div class="clearfix"></style>
</div>
<div class="clearfix"></div>
<style type="text/css">
	@font-face {
		font-family: "TACTENG Font";
		src: url(themes/default/assets/fonts/TACTENG.TTF) format("truetype");
	}
	span.symbol_line { 
		font-size:20px;
		font-family: "TACTENG Font", Verdana, Tahoma;
	}
	.right-cover{
		float:right;
	}
	.left-cover h2,
	.right-cover h2{
		white-space: nowrap;
		margin-top: 5px !important; 
		margin-bottom: 5px !important;
		font-size:14px !important;
		font-family: 'Moul', sans-serif !important;
	}
</style>