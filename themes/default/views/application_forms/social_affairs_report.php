<div class="row no-print">
	<?php 
		 $array_ = array(
				lang("select"),
				lang("ការប្រកាសទទួលស្គាល់ជនភៀសខ្លួន"),
				lang("ការប្រកាសបញ្ឈប់ឋានៈជនភៀសខ្លួន"),
				lang("ការប្រកាសលុបឈ្មោះជនភៀសខ្លួន"),
				lang("ពាក្យសុំប័ណ្ណសម្គាល់ជនភៀសខ្លួន"),
				lang("ពាក្យសុំប័ណ្ណស្នាក់នៅជនភៀសខ្លួន"), 
				lang("ពាក្យសុំឯកសារជនភៀសខ្លួន"), 
				);
	?>
	<div class="col-md-12">
		<div class="col-md-4" >		
			<?php echo lang('តាមផ្នែក', 'តាមផ្នែក'); ?> :
			<span class="red">*</span>
			<div class="form-group" style="white-space:nowrap;">  	
				<select class="form-control" > 
						<?php foreach($array_ as $key => $value) { ?>
								<option value="<?= $key ?>"><?=  $value ?></option>
						
						<?php }?> 
				</select> 
			</div> 
		</div>
		<div class="col-md-2" >
			<div class="form-group"> 		
				<?php echo lang('ចាប់ពីថ្ងៃទី', 'ចាប់ពីថ្ងៃទី'); ?> :
				<span class="red">*</span>
				<input type="text" value="<?= $this->input->get("from"); ?>" class="form-control date from" name="from" />
			</div> 
		</div>
		<div class="col-md-2" >
			<div class="form-group"> 		
				<?php echo lang('រហូតដល់ថ្ងៃទី', 'រហូតដល់ថ្ងៃទី'); ?> :
				<span class="red">*</span>
				<input type="text" value="<?= $this->input->get("to"); ?>" class="form-control date to" name="to" />
			</div> 
		</div>
		<div class="col-md-2">
			<div class="form-group"> 		
				<?php echo lang('ស្ថានភាព', 'ស្ថានភាព'); ?> :
				<span class="red">*</span>
				<div class="controls">					
					<select class="status form-control" name="status">
						<option value="approved"><?= lang("approved") ?></option>
						<option value="rejected"><?= lang("rejected") ?></option>
						<option value="pending"><?= lang("pending") ?></option>
					</select>
				</div>
			</div> 
		</div>

		<div class="col-md-3">
			<div class="form-group">
				<?php echo lang('&nbsp;', '&nbsp;'); ?>
				<div class="controls">
					<button type="submit" class="search btn btn-primary" />
						<?= lang("ស្វែងរក"); ?> <i class='fa fa-search' aria-hidden='true'></i>
					</button>
				</div> 
			</div> 
		</div> 	
	</div> 
</div>

<div class="box">
	<div class="box-content">
		<div class="row">
			
			<div class="col-sm-12">
				<button class="btn btn-success pull-right export-btn" data-export="export" ><?= lang("អ៊ិចសេល"); ?> <i class="fa fa-file-excel-o" aria-hidden="true"></i> </button>
				<table class="table table-condensed table-bordered table2excel">
					<tr>
						<td colspan="9">
							<h2><?= lang("របាយការណ៍ជំនាញ") ?></h2>
						</td>
					</tr>
					<tr class="bold text-center">
						<td width="3%"><?= lang("ល.រ"); ?></td>
						<td width="100px"><?= lang("រូបភាព"); ?></td>
						<td><?= lang("លេខករណី"); ?></td>
						<td><?= lang("នាមត្រកូល និងនាមខ្លួន"); ?></td>
						<td><?= lang("ភេទ"); ?></td>
						<td width="200px"><?= lang("ថ្ងៃខែឆ្នាំកំណើត"); ?></td>
						<td width="150px"><?= lang("សញ្ជាតិ"); ?></td>
						<td width="200px"><?= lang("លេខទូរស័ព្ទ"); ?></td>
						<td><?= lang("status"); ?></td>
					</tr>
				<?php 
					$applications = $this->db->query("select * from erp_fa_rsd_applications")->result();
					$male = 0;
					$female = 0;
					$total = 0;
					foreach($applications as $i => $application){ ?>
					<?php	 
							$total = $i+1;  
							if($application->gender == "male") {
								$male++;
							}
					?>
						<tr>
							<td class="center"><?= $this->erp->toKhmer($i+1); ?></td>
							<td class="center cover-photo"><img src="<?= base_url()?>assets/uploads/<?= $application->photo;?>" alt="image"/></td>
							<td class="center"><?= $this->erp->toKhmer($application->case_prefix." ".$application->case_no); ?></td>
							<td><?= $application->lastname_kh." ".$application->firstname_kh; ?></td>
							<td class="center"><?= lang($application->gender) ?></td>
							<td class="center"><?= $this->erp->toKhmer($this->erp->hrsd($application->dob)) ?></td>
							<td class="center"><?= $application->nationality_kh; ?></td>
							<td class="center"><?= $this->erp->toKhmer($application->phone) ?></td>
							<td>
								
							</td>
						</tr>
				<?php }	
				 	$female = $total - $male; 
				?>   
					<tr>
						<td><?= lang("សរុបៈ")?> 
						</td>
						<td>
						<?= $this->erp->toKhmer($total) ?> <?= lang("នាក់")?> 
						</td>
					</tr>
					<tr>
						<td><?= lang("ប្រុសៈ")?>  
						</td>
						<td>
							<?= $this->erp->toKhmer($male) ?><?= lang("នាក់")?>
						</td>
					</tr>
					<tr> 
						<td>
							<?= lang("ស្រីៈ")?> 
						
						</td>
						<td> 
							<?= $this->erp->toKhmer($female) ?><?= lang("នាក់")?> 
						</td>
					</tr>  
				</table>  
			</div>
		</div>				
	</div>
</div> 
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js" type="text/javascript" charset="utf-8"></script>
<script src="<?= base_url()?>themes/default/assets/js/jquery.table2excel.js" type="text/javascript" charset="utf-8"></script>
<script>
	 $(function() {
		 $(".export-btn").on("click",function(){
			 $(".table2excel").table2excel({
				exclude: ".noExl",
				name: "Excel Document Name",
				filename: "myFileName" + new Date().toISOString().replace(/[\-\:\.]/g, ""),
				fileext: ".docx",
				exclude_img: true,
				exclude_links: true,
				exclude_inputs: true
			});
		 });
		});
</script> 
<style>
	@media print{
		.bblack{ background: white !important; }   
		.modal-content, .box, 
		.modal-header{ border:none!important; }
		.table { border:none !important; }
	}
</style>