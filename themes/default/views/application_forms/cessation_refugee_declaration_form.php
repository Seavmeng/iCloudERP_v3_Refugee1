<?php 
	echo $this->output->enable_profiler(TRUE);
?>
<div class='col-lg-12' > 
	<div class='col-lg-3 director-txt'> 
		<div class='form-group'> 
			<?= lang("នាយកគ្រប់គ្រង៖", "នាយកគ្រប់គ្រង៖");?>
			<select   class="form-control text-cover director">
				<?php foreach($departments as $dep) {?>
				<option value="<?= $dep->department_en ?>"><b><?= $dep->department_kh?></b></option>
				<?php }?>
			</select>
		</div> 
	</div>
	<div class='col-lg-3 frame'>
		<div class='form-group'> 
			<?= lang("កំណត់ស៊ុម៖", "កំណត់ស៊ុម៖");?><br/>
			​<input checked name="border-frame" class="form-control" type="checkbox"/>&nbsp;&nbsp;<?php echo lang("បង្ហាញ");?>
		</div>
	</div>
</div>
<div class="clearfix"></div>
		<div class="content-cover a4">
				<img src="<?= base_url("assets/bg33.jpg") ?>" class="hidden-bg" >
					<span class='case_no_footer' ><?= $this->erp->toKhmer($result->case_prefix." ".$result->case_no); ?>  </span>
				</img>
				
				<div class="cover-in">
					<div class="content-header" style="height: 170px;">
					</div>
					<div class="cover-left"></div>
					<div class="text-center cover-center" >
						<h3><?= lang("ប្រកាស") ?></h3>
						<h3><?= lang("ស្តីពី") ?></h3>
						<h3><?= lang("ការបញ្ឈប់ឋានៈជនភៀសខ្លួន") ?></h3>
						<h3><span class='cover-line'>3</span></h3>
						<h3><?= lang("ឧបនាយករដ្ឋមន្រ្តី រដ្ឋមន្រ្តីក្រសួងមហាផ្ទៃ") ?></h3>
					</div>
					<div class="cover-right">
						<div class="cover-photo"> 
						
							<?php if($member_declare){ ?>
								<img src="<?= base_url("assets/uploads/members/".$member_declare->photo) ?>" height="110" width="100" />
							<?php } else { ?>
								<img src="<?= base_url("assets/uploads/".$result->photo) ?>" height="110" width="100" />
							<?php } ?> 
						
						
						
						</div>
					</div>
					<div style="clear:both;"></div>
					<div style="clear:both;"></div>
					<style type="text/css">
						.cover-photo{
							margin-top:30px;
							margin-left:20px;
							width:120px;
							height:120px;
						}
						.cover-center{
							float:left; 
							width:48%;
						}
						.cover-left{
							float:left; 
							width:25%; 
							height:10px;
						}
						.cover-right{
							float:right; 
							width:25%; 
						}
					</style> 
					<div class="content-body recognition-list text-left"  style=" text-align: justify;text-justify: inter-word;"> 
						 <?php echo $recognition->description ?>
					</div>
					<div class="text-center">
						<h3><u><?= lang("សម្រេច") ?></u></h3>
					</div> 
						<table width="100%" class="text-left" style="margin-left:-10px;">
							<tr>
								<td width="60" class="text-cover">
									ប្រការ<span class='khmer-number'>១ </span>៖&nbsp;&nbsp;
								</td>
								<td colspan="3">
									បញ្ឈប់ឈ្មោះ 
									<?php if($member_declare){ ?>
										<span class='bold'> <?= $member_declare->lastname_kh ." ".$member_declare->firstname_kh." / ".$member_declare->lastname ." ".$member_declare->firstname ?></span> 
									<?php } else { ?>
										<span class='bold'> <?= $result->lastname_kh ." ".$result->firstname_kh." / ".$result->lastname ." ".$result->firstname ?></span> 
									<?php } ?>
								</td>
								<td>
									ភេទ 
									<?php if($member_declare){ ?>
										<span class='bold'> <?= lang($member_declare->gender) ?></span>
									<?php } else { ?>
										<span class='bold'> <?= lang($result->gender) ?></span>
									<?php } ?>
								</td>
							</tr>
							<tr>
								<td></td>
								<td>
									ថ្ងៃខែឆ្នាំកំណើត
									<?php if($member_declare){ ?>
										<span class='bold'> &nbsp;<?= $this->erp->khmer_date_format($member_declare->dob); ?> </span>
									<?php } else { ?>
										<span class='bold'> &nbsp;<?= $this->erp->khmer_date_format($result->dob); ?> </span>
									<?php } ?>
								</td>
								<td>
									សញ្ជាតិ 
									<?php if($member_declare){ ?>
										<span class='bold'> <?= $member_declare->nationality_kh; ?>  </span>
									<?php } else { ?>
										<span class='bold'> <?= $result->nationality_kh; ?>  </span>
									<?php } ?>
								</td> 							
								<td>
									ជាតិពន្ធុ 
									<?php if($member_declare){ ?>
										<span class='bold'> <?= $member_declare->ethnicity_kh ; ?>  </span>
									<?php } else { ?>
										<span class='bold'> <?= $result->ethnicity_kh; ?>  </span>
									<?php } ?>
									
								</td>
								<td>
									ករណីលេខ 
									<span class='bold'><?= $this->erp->toKhmer($result->case_prefix." ".$result->case_no); ?>  </span>
								</td>
							</tr> 
							<tr>
								<td></td>
								<td>
									ថ្ងៃខែឆ្នាំចុះឈ្មោះ  
									<span class='bold'> <?=  $this->erp->khmer_date_format($result->registered_date); ?>  </span>
								</td>
								<td></td>
								<td></td>
								<td>
									<b><?= (!empty($members)?lang("(ម្ចាស់ករណី)"):"")?></b>
								</td>
							</tr> 
							<tr>
								<td width="60"></td>
								<td colspan='4'>
									ទីកន្លែងកំណើត​ ​​ 
									<?php if($member_declare){ ?>
										<span class='bold'><?= $member_declare->pob_kh; ?></span> 
									<?php } else { ?>
										<span class='bold'><?= $result->pob_kh; ?></span> 
									<?php } ?>
								</td>
							</tr> 
						<?php 
							if(!empty($members)){					
								//$male = $this->applications->getGroupByGenderMembersByApplicationId($result->id, 'male');
								//$female = $this->applications->getGroupByGenderMembersByApplicationId($result->id, 'female');
								
								$male = 0;
								$female = 0;
								foreach($members as $m) {
									$member = $this->applications->getFamilyMembersDependantAccompanyingById($m->member_id);
									if($member->gender == "male") {
										$male++; 
									} 
								}
								$total_gender = (count($members)<10 ? 0:'').count($members); 
								$male = (count($male)<10 ?0:'').$male;
								$female = (count($total_gender - $male)<10 ? 0:'').($total_gender - $male);
								
						?>
							<tr>
								<td width="60"></td>
								<td colspan='4'>
								និងសមាជិកគ្រួសារក្នុងបន្ទុកចំនួន <b> <?= $this->erp->toKhmer($total_gender); ?> </b>នាក់ 
								ប្រុស <?= $this->erp->toKhmer($male) ?> នាក់ ស្រី <?= $this->erp->toKhmer($female) ?> នាក់
								អាយុក្រោម ១៨ ឆ្នាំ  ដូចមានភ្ជាប់មកជាមួយក្នុងតារាង
								</td>  
							</tr> 
						<?php }?>
							<tr>
								<td width="60"></td>
								<td colspan='4'><?php echo (!empty($members))?"ឧបសម្ព័ន្ធ":""?><span class='bold'>ពីឋានៈជាជនភៀសខ្លួន</span> ។</td>
							</tr> 
							<tr>
								<td width="60" class="text-cover" style="position: absolute;">
									ប្រការ<span class='khmer-number'>២ </span>៖
								</td>
								<td colspan="4" style="white-space: normal;">
									<?php echo !empty($recognition->brokar2)?$recognition->brokar2:"
										ជនភៀសខ្លួនដូចមានក្នុងប្រការ១ ខាងលើមានកាតព្វកិច្ចគោរព និងអនុវត្តច្បាប់របស់ព្រះរាជាណាចក្រកម្ពុជាឱ្យបានមឺុងម៉ាត់ ។
										"?>
								</td>
							<tr> 
							<tr>
								<td width="60" class="text-cover" style="position: absolute;">
									ប្រការ<span class='khmer-number'>៣ </span>៖
								</td>
								<td colspan="4" style="white-space: normal;">
									<?php echo !empty($recognition->brokar3)?$recognition->brokar3:"
									ស្នងការរាជធានី-ខេត្តនិងស្ថាប័នដែលមានការពាក់ព័ន្ធជាមួយសាមុីខ្លួន ត្រូវអនុវត្តប្រកាសនេះចាប់ពីថ្ងៃចុះហត្ថលេខាតទៅ។
									"?>
								</td>
							<tr>
						</table> 
					<div class="content-footer​" style="float:right; margin-right:30px;">
						<table width="100%">
							<tr>
								<td class='text-center' style="margin-top: 3px;">
								<?= $this->erp->footer_date_khmer('រាជធានីភ្នំពេញ');?> 
								</td>
							</tr>
							<tr>
								<td>
									<center> 
										<h3 class="director_text_sub"></h3>
										<h3 class="director_text">ឧបនាយករដ្ឋមន្រ្តី រដ្ឋមន្រ្តីក្រសួងមហាផ្ទៃ</h3>
									</center>
								</td>
							</tr>
						</table>
					</div>
				</div>
		</div>
	<?php if(!empty($members)){ ?>
	
	<div class="content-cover-empty a4">
		<img src="<?= base_url("assets/bg34_empty.jpg") ?>" class="hidden-bg-empty" >
			<span class='case_no_footer' ><?= $this->erp->toKhmer($result->case_prefix." ".$result->case_no); ?>  </span>
		</img>
		<div class="cover-in">	
			<div class="content-header" style="height: 80px;"></div>
			<div style="padding:0 10px 0 0px;">
				<div class="text-center">
					<p class="text-cover"><?= lang("តារាងឧបសម្ព័ន្ធ") ?></p>
					<br/>
					<p>
					តារាងឧបសម្ព័ន្ធនៃប្រកាសស្តីពី ការបញ្ឈប់ឋានៈជនភៀសខ្លួន ឈ្មោះ  
					<span class='bold'> <?= $result->lastname_kh ." ".$result->firstname_kh." / ".ucfirst($result->lastname) ." ".ucfirst($result->firstname) ?></span> 
					ក្នុងករណីលេខ 
					<span class='bold'> <?= $this->erp->toKhmer($result->case_prefix." ".$result->case_no); ?>  </span> ​។
					</p>
				</div>
				<div class="clearfix"></div>
				<table class="table-members">
					<thead>
						<tr>
							<th class="text-center">ល.រ</th>
							<th class="text-center">នាមត្រកូល</th>
							<th class="text-center">ភេទ</th>
							<th class="text-center">ថ្ងៃខែឆ្នាំកំណើត</th>
							<th class="text-center">សញ្ញាតិ</th>
							<th class="text-center">ទំនាក់ទំនង</th>
							<th class="text-center" width="100px">រូបថត</th>
						</tr>
					</thead>
					<tbody>
						<?php 
						$relationships_ = array();
						foreach($relationships as $relation){
							$relationships_[$relation->id] = $relation->relationship_kh;
						}
						
						foreach($members as $key => $m){ 
							
							$member = $this->applications->getFamilyMembersDependantAccompanyingById($m->member_id);																								
							$photo = "no_image.png";
							if(!empty($member->photo) || !file_exists("assets/uploads/".$member->photo)){
								$photo = $member->photo;
							}								
						?>
							<tr>
								<td class="text-center"><?= $this->erp->toKhmer($key+1)?></td>
								<td class="text-center"><?= $member->lastname_kh." ".$member->firstname_kh  ?><br/><?= $member->lastname." ".$member->firstname  ?></td>
								<td class="text-center"><?= lang($member->gender); ?></td>
								<td class="text-center"><?= $this->erp->khmer_date_format($member->dob); ?></td>
								<td class="text-center"><?= $member->nationality_kh ?></td>
								<td class="text-center"><?= $relationships_[$member->relationship]; ?></td>
								<td class="text-center">
									<img src='<?= base_url("assets/uploads/members/".$photo) ?>' class='thumb' />
								</td>
							<tr>
						<?php } ?>
							<thead>
								<tr>
									<th colspan="7">
										សរុប <?= $this->erp->toKhmer($total_gender) ?> នាក់ ប្រុស <?= $this->erp->toKhmer($male) ?> នាក់ ស្រី <?= $this->erp->toKhmer($female) ?> នាក់
									</th>
								</tr>
							</thead>
					<tbody>
				</table>
			</div>
		</div>	
	</div>
	
	<?php } ?>
			

<script type='text/javascript'>
	$(".director").on('click',function(){ 
		var txt = $(".director option:selected").text();
		var txt_en = $(".director option:selected").val();
		if (txt=="រដ្ឋលេខាធិការ") {
			$(".director_text_sub").text('ជ.រដ្ឋមន្រ្តី');
		}else{
			$(".director_text_sub").text('');
		}
		$(".director_text").text(txt); 
		$(".director_text_en").text(txt_en); 
	});
	$("input[name='border-frame']").on('ifChecked',function(){
			$(".content-cover").css({
				"background":'url("assets/bg33.jpg")',
				"background-size": '800px auto',
				"background-repeat": 'no-repeat',
				"background-repeat": 'block',
			}); 
			$(".content-cover-empty").css({
				"background":'url("assets/bg34_empty.jpg")',
				"background-size": '800px auto',
				"background-repeat": 'no-repeat',
				"background-repeat": 'block',
			});  
			    
			$(".hidden-bg").attr("src","<?= base_url("assets/bg33.jpg") ?>");
			$(".hidden-bg-empty").attr("src","<?= base_url("assets/bg34_empty.jpg") ?>");
	});
	$("input[name='border-frame']").on('ifUnchecked',function(){
			$(".content-cover,.content-cover-empty").css({"background":"white"});
			$(".hidden-bg").removeAttr("src");
			$(".hidden-bg-empty").removeAttr("src");
	}); 
</script> 
<style type="text/css" media="all">
	@media print{
		.bblack{ background: white !important; }
		.modal-content, .box, 
		.modal-header{ border:none !important; }
		.table { border:1px solid #000 !important; }
		.frame,.hidden-bg,.hidden-bg-empty{
			display:block !important;
		}
		.frame,.director-txt{display:none !important;}
	}
	.table-members{
		width:100%;
		margin-top:10px;
	}
	.table-members th{
		padding:10px;
		border:2px solid #000;
		border-spacing:0px;
	}
	.table-members td{
		padding:0px;
		border:2px solid #000;
		border-spacing:0px;
	} 
	
	.hidden-bg,.hidden-bg-empty{
		z-index:-9999;
		position:absolute;
		display:none;
	}
	.content-cover span, p {
		padding: 0;
		margin: 0;
		font-size:12px;	
	}
	.content-cover center{
		font-size:12px;
		font-family: 'Moul', sans-serif !important;
	}
	.content-cover table td{
		font-size:12px;	
		line-height:26px;
		white-space: nowrap;
	}
	.content-cover{
		width:800px;
		min-height:1129px;
		background:url("assets/bg33.jpg");
		background-size: 800px auto;
		background-repeat: no-repeat;
		color: blue;
		position:relative;
	}
	.content-cover-empty{
		width:800px;
		min-height:1129px;
		background:url("assets/bg34_empty.jpg");
		background-size: 800px auto;
		background-repeat: no-repeat;
		color: blue;
		position:relative;
	}
	.cover-in{
		margin:0 70px;
		width:670px;
		position:absolute;
		white-space: wrap;
		padding:20px;
	}	
	ul{
		list-style: none;
		text-align:justify;
		padding: 0;
		margin: 0;
	}
	.recognition-list > ul li:before {
	  content: '-';
	  position: absolute;
	  margin-left: -10px;
	}  
	h3 {
		line-height: 5px;
	}
	.content-cover h3{ 
		font-family: 'Moul', sans-serif !important;
	}
	hr{
		padding:0px;
		margin:0px;
		border-top:none !important;
		border-bottom:dotted 1px !important;
	}
	@font-face {
		font-family: "TACTENG Font";
		src: url(themes/default/assets/fonts/TACTENG.TTF) format("truetype");
	} 
	span.cover-line { 
		font-size:30px;
		padding:0px;
		font-family: "TACTENG Font";
	}  
	.content-body, table, .content-body li{ 
		line-height:19px;
		font-size: 12px !important;
	}
	@font-face {
			font-family: "Khmer Moul";
			src: url(themes/default/assets/fonts/KhmerMoul.TTF) format("truetype");
	}  
	span.khmer-number {
		font-family:'Moul'; 
	}
	table {
		text-align: justify;
		text-justify: inter-word;		
	} 
</style>