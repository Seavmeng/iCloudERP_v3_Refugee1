
<div class="modal-dialog" style="width: 430px;">
	<div class="modal-content">
		<div class="modal-body" style="height: 230px;"> 
			<table width="50%" class="information_table">
				<?php
					$photo = "no_image.png";
					if(!empty($request_card->photo) || !file_exists("assets/uploads/".$request_card->photo)){
						$photo = $request_card->photo;
					}
				?>
				<tr>
					<td rowspan="7">
						<div class="pic_profile">
							<img src="<?= site_url("assets/uploads/".$photo); ?>" style="width:70px;height:70px;"/>
						</div>
					</td>
				</tr>
				<tr>
					<td>
						ឈ្មោះ / Name ៖ 
						<span>
							<?= $application->firstname_kh ?> &nbsp;
							<?= $application->lastname_kh ?>	
						</span>
					</td>
				</tr>
				<tr>
					<td>
						ភេទ / Sex ៖
						<span>
						<?= lang($application->gender) ?>
						</span>
					</td>
				</tr>
				<tr>
					<td>
						ថ្ងៃខែឆ្នាំកំណើត​ / Date of Birth ៖
						<span>
						<?= $this->erp->toKhmer($this->erp->hrsd($application->dob)) ?>
						</span>
					</td>
				</tr>
				<tr>
					<td>
						សញ្ជាតិ / Nationality ៖
						<span>
						<?= $application->nationality_kh ?>
						</span>
					</td>
				</tr>
				<tr>
					<td>
						ប្រកាសទទួលស្គាល់ឋានៈរបស់ក្រសូងមហាផ្ទៃលេខ ៖ 
						<span>
							<?= $this->erp->toKhmer($recognization->recognized_no) ?> ប្រ.ក
						<span>						
					</td>
				</tr>
				<tr>
					<td>
						ថ្ងៃចេញប្រកាស ៖ 
						<span>
							<?= $this->erp->toKhmer($this->erp->hrsd($recognization->recognized_date)); ?>
						</span>
					</td>
				</tr>
				<tr>
					<td>
						ករណីលេខ
					</td>
					<td>
						អាសយដ្ឋាន ៖ 
					</td>
                           
				</tr>
				
				<tr>
					<td>
						ID ៖ 
						<span>
							<?= $this->erp->toKhmer($application->case_prefix.$application->case_no) ?>
						</span>
					</td>
					<td>
						<?= ($request_card->repeat==1?"(ទុតិយតា)":"") ?>	
					</td>
				</tr>
				
			</table>
			<div class="card_left">
				<br/>
				<br/>
				<table class="expiry_table">
					<tr>
						<td>ថ្ងៃផុតកំណត់</td>
						<td>
							<span>
							<?= $this->erp->toKhmer($this->erp->hrsd($request_card->expiry_date)); ?>		
							</span>
						</td>
					</tr>
					<tr>
						<td>Expiry Date</td>
						<td>
							<span>
								<?= $this->erp->hrsd($request_card->expiry_date); ?>
							</span>							
						</td>
					</tr>
				</table>
			</div>
			<div class="card_right text-center">
				<p>ភ្នំពេញ, ថ្ងៃទី <?= $this->erp->toKhmer(date("d")) ?> ខែ <?= $this->erp->KhmerMonth(date("m")) ?> ឆ្នាំ <?= $this->erp->toKhmer(date("Y")) ?></p>
					<span class="text-cover">អគ្គនាយកអន្តោប្រវេសន៍</span>
			</div>	
			<div class="clearfix"></div> 
	     </div>
	</div>
</div>
<style type="text/css">
     	@media print{
		.bblack{ background: white !important; }   
		.modal-content, .box, 
		.modal-header{ border:none!important; }
		.table { border:none !important; }
	}
	.information_table td{
		line-height: 15px;
		vertical-align: top;
		color: #2e7bd1;
		font-size: 12px;
           white-space: nowrap;
	}
	.card_left, .card_right{
		width:50%;
	}
	.card_left{
		float: left;
		font-size: 12px;
	}
	.card_right{
		float: right;
		font-size: 12px;
	}
	.pic_profile{
	  padding-left:2px;
	}
	.pic_profile img{
		border: 2px solid #EEE;
		width: 70px;
		height: 70px;
	}
	.expiry_table td{
		padding: 2px;
		border: 2px solid #EEE;
		color: #2e7bd1;
           white-space: nowrap;
	}
	.information_table span, .expiry_table span{
		color:#000000;
		font-weight: bold;
           white-space: nowrap;
	}
     .text-cover{
        font-family: 'Moul', sans-serif !important;
    }
    .expiry_table{
          border-collapse: collapse;
    }
</style>