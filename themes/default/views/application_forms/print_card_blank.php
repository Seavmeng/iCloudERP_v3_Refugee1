 
<div class="card" width="410px;"> 
	<img class="card_cover" src="<?= base_url()?>assets/card_back.jpg" />
	<table  class="information_table">
		<?php
			$photo = "no_image.png";
			if(!empty($request_card->photo)){
				$photo = $request_card->photo;
			}else {
				$photo = $application->photo;
			}		 	
		?> 
		<tr>
			<td rowspan="7"> 
				<div class="pic_profile">
					<img src="<?= site_url("assets/uploads/".$photo); ?>" />
				</div>
			</td>
		</tr>
        <?php if((strlen($application->firstname ) < 10) && (strlen($application->lastname ) < 10)){ ?>
            <tr>
                <td>
                    <p>
                    <span class="label-hidden">ឈ្មោះ / Name:&nbsp;&nbsp;</span>
                    <span>
                        <?= $application->lastname_kh ?> &nbsp;
                        <?= $application->firstname_kh ?>
                        <?php if($application->firstname){?>
                        <?= " / ".$application->lastname ?>
                        <?= $application->firstname ?>
                        <?php }?>
                    </span>
                    </p>
                </td>
            </tr>
        <?php } else { ?>
            <tr>
                <td>
                    <p style="margin-left: 70px; margin-top: -4px; line-height: 11px;">
                        <span>
                        <?= $application->lastname_kh ?> &nbsp;
                            <?= $application->firstname_kh ?>
                            <?php if($application->firstname){?>
                                <?= " <br> ".$application->lastname ?>
                                <?= $application->firstname ?>
                            <?php }?>
                    </span>
                    </p>
                </td>
            </tr>
        <?php  }?>
		<tr>
			<td>
				<span class="label-hidden">ភេទ / Sex:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>
				<span>
				<?= lang($application->gender) ?> 
				<?= " / ".ucfirst($application->gender) ?>  
				</span>
			</td>
		</tr>
		<tr>
			<td>
				<span class="label-hidden">ថ្ងៃខែឆ្នាំកំណើត​ / Date of Birth:&nbsp;&nbsp;&nbsp;&nbsp; </span>
				<span>
				<?= $this->erp->hrsd($application->dob) ?>
				</span>
			</td>
		</tr>
		<tr>
			<td>
				<span class="label-hidden">សញ្ជាតិ / Nationality:&nbsp;&nbsp;&nbsp;&nbsp; </span>
				<span>
				<?= $application->nationality_kh ?> 
				<?php if($application->nationality){?>
					<?= " / ".$application->nationality ?> 
				<?php }?>
				</span>
			</td>
		</tr>
		<tr>
			<td>
				<span class="label-hidden">ប្រកាសទទួលស្គាល់ឋានៈរបស់ក្រសួងមហាផ្ទៃលេខ :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
				<span>
					<?= $this->erp->toKhmer($application->recognized_no) ?> ប្រ.ក
				<span>						
			</td>
		</tr>
		<tr>
			<td>
				<span class="label-hidden">ថ្ងៃចេញប្រកាស :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
				<span>
					<?= $this->erp->hrsd($application->recognized_date); ?>
				</span>
			</td>
		</tr>
		<tr>
			<td> 
				<br/>
				<span style="margin-top: -6px; display: block;">
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?= $application->case_prefix." ".$application->case_no; ?>
				</span> 
			</td>
			<td>
				<span class="label-hidden">អាសយដ្ឋាន:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
				   <span> 
					<?= $request_card->address_kh ?> 
					<?= lang("ស.").$this->applications->getAllAddressKHCard($request_card->commune,"commune"); ?> 
					<?= lang("ខ.").$this->applications->getAllAddressKHCard($request_card->district,"district"); ?> 
					<?= lang("ក.").$this->applications->getAllAddressKHCard($request_card->province,"province"); ?> 
				   </span>
		   </td> 
		</tr>
		<tr>
			<td>
				<span  style="position:absolute;color:red;font-weight:bold;"><?= ($request_card->repeat==1?"(ទុតិយតា)":"") ?></span>
			</td>
		</tr> 
	</table>
	<div class="card_left">
		<br/>
		<br/>
		<table class="expiry_table">
			<tr>
				<td><span class="label-hidden">ថ្ងៃផុតកំណត់:</span></td>
				<td>
					<span>
					<?= $this->erp->toKhmer($this->erp->hrsd($request_card->expiry_date)); ?>		
					</span>
				</td>
			</tr>
			<tr> 
				<td><span class="label-hidden">Expiry Date:</span></td>
				<td>
					<span>
						<?= $this->erp->hrsd($request_card->expiry_date);?>
					</span>							
				</td>
			</tr>
		</table>
	</div> 
	 
		<?php  
			$date = explode('-', $request_card->date_sign);
			$ddate = isset($date[2])?$date[2]:"00";
			$dmonth = isset($date[1])?$date[1]:"00";
			$dyear = isset($date[0])?$date[0]:"00";
		?>
	<div class="card_right text-center">
		<div class="stamp_signature_box">
			<img src="<?= site_url('assets/'.$Settings->stamp_and_signature); ?>" class="stamp_signature" />
			<p class="stamp_signature_title">
				<?= !empty($setting->prefix_name_officer)?$setting->prefix_name_officer:lang("នាយឧត្តមសេនីយ៍");?>   
				&nbsp;&nbsp;
				<span class="text-cover" style="color:#f90808;font-size:13px;"><?= !empty($setting->name_officer)?$setting->name_officer:lang("សុខ ផល");?></span>
			</p>
		</div>	
		<p class="text-date"><span class="label-hidden">ភ្នំពេញ, ថ្ងៃទី </span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span><?= $this->erp->toKhmer($ddate) ?> ខែ <?= $this->erp->KhmerMonth($dmonth) ?> ឆ្នាំ <?= $this->erp->toKhmer($dyear) ?></span></p>				
		<span class="text-cover label-hidden">អគ្គនាយកអន្តោប្រវេសន៍</span>		
	</div>	 
</div>
<div style="clear:both;"></div>  
<br/>
<div class="card_front" width="410px" > 
	<img class="card_cover" src="<?= base_url()?>assets/card_front.jpg" style="margin-top:10px;"/>
	<table class="cover_front">
		<tr>
			<td style="text-align:center;font-weight:bold;">
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
			<span><?php echo sprintf("%04s", $request_card->barcode_no); ?></span>
			</td>
		</tr>
		<tr>
			<td>
				<?php $br = $this->erp->save_barcode(sprintf("%04s", $request_card->barcode_no), 'code128',20, false); ?> 
				<img src="<?= base_url() ?>assets/uploads/barcode<?= $this->session->userdata('user_id') ?>.png" alt="001"/>     
			</td>
		</tr>
	</table> 
</div> 
<style type="text/css">

	@media print{
		body{padding:0px !important;margin:0px !important;}   
		.card_front,.card{padding:0px !important; height: 210px !important; width: 410px !important;border:none !important;}			 
		.card_cover{display:none !important;}
	}
	.option { 
		position:relative;
		left:20px; 
	}
	.label-hidden {
		visibility:hidden;
	} 
	.card_cover { 
		position: absolute;
		position: absolute;
		width: 425px;
		z-index: -1 !important;
		height: 268px;
	}
	.cover_front{ 
	    float: right;    
		margin-top: 170px !important;
		margin-right: -7px;
	} 
	.information_table {
		position: relative;
		left: 25px;
		top: 12px; 
	} 
	.pic_profile img{    
		width: 85px;
		height: 100px;
		top: 9px;
		position: relative; 
		right: 8px;
	}
	.text-date{
		position: relative;
		left: 15px !important;
		top: 0px;
		font-size: 11px !important; 
		color: black;
		font-weight:bold;
	}
	body{
		padding:0px !important;
		margin:0px !important;
		font-family: 'Siemreap', sans-serif; 
	}
	
	@font-face {
		font-family: "Siemreap";
		font-style: normal;
		font-weight: 400;
		src: local('Siemreap Regular'), local('Siemreap-Regular'), url(<?=$assets?>/fonts/Siemreap.woff2) format('woff2');
		unicode-range: U+1780-17FF, U+200B-200C, U+25CC;
	}
	@font-face {
		font-family: 'Moul';
		font-style: normal;
		font-weight: 400;
		src: local('Moul'), url(<?=$assets?>/fonts/Moul.woff2) format('woff2');
		unicode-range: U+1780-17FF, U+200B-200C, U+25CC;
	}
	.stamp_signature_box{
		position:absolute; 
	    margin-left:10px;
        margin-top: -11px;
		
	}
	.stamp_signature{
		width:150px !important;
		z-index: 0 !important;
		position: relative;
        margin-left: 20px !important;
	}
	.stamp_signature_title{
		margin-top: -10px;
		padding:0px;
		color:#f90808;
		font-weight:bold;
		font-size:11px;
	}
	.card,.card_front{ 
		height: 250px;
		width: 410px;
	}  
	.barcode,.card_no { 
		width: 100px;
		position: relative;
		top: 200px ;
		text-align: center;
		right: 20px;
		font-weight:bold;
		float: right;
	}
	.information_table td  {
		line-height: 16px;
		vertical-align: top;
		color: #2e7bd1;
		font-size: 11px; 
		white-space: nowrap;
		font-weight:bold;
    }
	.card_left{
		float: left;
		
	}
	.card_right{
		color: #2e7bd1;
		float: right; 
		line-height:7px;
		font-weight:bold; 
	}  
	.text-cover{
		font-family: 'Moul', sans-serif !important;
	}
	.expiry_table{
		border-collapse: collapse; 
		position: relative;
		left: 28px; 
		top:7px;
	}
	.expiry_table td{ 
		color: #2e7bd1; 
		font-weight:bold;
		white-space: nowrap;
		padding:3px;
	}
	.information_table span, .expiry_table span{
		color: #000000;
		font-weight: bold;
		white-space: normal;
		word-wrap: break-word;
		font-size: 11px;
	} 
</style>