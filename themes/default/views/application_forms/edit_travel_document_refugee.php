<div class="box">
	<div class="box-header">
		<h2 class="blue">
			<i class="fa-fw fa fa-barcode"></i>
			<?= lang("edit_travel_document_refugee") ?>
		</h2>
	</div>
	<div class="box-content">
		<div class="row">  
			<?php  echo form_open_multipart("application_forms/edit_travel_document_refugee/".$id.'/'.$travel_id); ?>
			<?php 
			$countries_ = array(lang("select"));
			foreach($countries as $country){
				$countries_[$country->id] = $country->country;
			} 
			?>
			<?php 
			$provinces_ = array(lang("select"));
			foreach($provinces as $province){
				$provinces_[$province->id] = $province->name;
			} 
			?>
			<?php 
			$districts_ = array(lang("select"));
			foreach($districts as $district){
				$districts_[$district->id] = $district->name;
			} 
			?>
			<?php 
			$communes_ = array(lang("select"));
			foreach($communes as $commune){
				$communes_[$commune->id] = $commune->name;
			} 
		?>
				<div class="col-sm-5"> 
					<div class="row">
						<div class="col-sm-6">						
							<div class="form-group">
								<?php echo lang('case_no', 'case_no'); ?>​ 
								<span class="red">*</span>
								<div class="control">
									<input type="text" disabled value="<?= $application->case_prefix.$application->case_no?>"  name='case_no' class="form-control input-sm case_no" id="case_no" >
									<input type="hidden" name="application_id" value="<?= $result->application_id;?>">
								</div>
							</div>
						</div> 
						<div class="col-lg-6">
							<?php echo lang('ឈ្មោះអ្នកស្នើសុំ', 'ឈ្មោះអ្នកស្នើសុំ'); ?>​ 
							<?php
								$members_ = array(lang("select"));
								foreach($members as $member){
									$members_[$member->id] = $member->firstname_kh . " " . $member->lastname_kh;
								}						
							?>
							<span class="red">*</span>
							<div class="control" id="members">
								<?php echo form_dropdown('member', $members_, $result->member_id, ' class="form-control" disabled '); ?>
							</div>
					    </div>
					
					</div>
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<?php echo lang('occupation', 'occupation'); ?>​(KH)
								<span class="red">*</span>
								<div class="control">
									<input type="text" value='<?= set_value('occupation_kh',$result->occupation_kh);?>' class="form-control input-sm"  name="occupation_kh" />
								</div>
							</div>						
						</div> 
						<div class="col-sm-6">
							<div class="form-group">
								<?php echo lang('occupation', 'occupation'); ?>​(EN)
								<span class="red">*</span>
								<div class="control">
									<input type="text" value='<?= set_value('occupation',$result->occupation);?>' class="form-control input-sm"  name="occupation" />
								</div>
							</div>						
						</div> 
					</div> 
					<div class="clearfix"></div>
					<div class="panel panel-warning">
						<div class="panel-heading"><?php echo lang("address","address") ?></div>
						<div class="panel-body" style="padding: 5px;">	
							<div class="col-lg-12">
								<div class="form-group">
									<?php echo lang('country', 'country'); ?>
									<div class="controls">
										<?php echo form_dropdown('country', $countries_,$result->country, ' class="form-control" '); ?>												
									</div>
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<?php echo lang('province', 'province'); ?>
									<div class="controls">
										<?php echo form_dropdown('province', $provinces_, $result->province, ' class="form-control" '); ?>												
									</div>
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<?php echo lang('district', 'district'); ?>
									<div class="controls">
										<?php echo form_dropdown('district', $districts_, $result->district, ' class="form-control" '); ?>												
									</div>
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<?php echo lang('commune', 'commune'); ?>
									<div class="controls">
										<?php echo form_dropdown('commune', $communes_, $result->commune, ' class="form-control" '); ?>												
									</div>
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<?php echo lang('village', 'village'); ?>
									<span class="red">*</span>
									<div class="controls">
										<?php 
										echo form_input('village', set_value('village',$this->applications->getTagsById($result->village)),'class="form-control village" '); ?>											
									</div>
								</div>
							</div>
							<div class="col-lg-12">
								<div class="form-group"> 
									<?php echo lang('other', 'other'); ?>​ 
									<span>(KH)</span>
									<span class="red">*</span>
									<div class="control">
										<input type="text" value="<?= $result->address_kh ?>" class="form-control input-sm" name="address_kh"  id="address_kh" />
									</div> 
								</div> 
							</div> 
							<div class="col-lg-12">
								<div class="form-group"> 
									<?php echo lang('other', 'other'); ?>​ 
									<span>(EN)</span>
									<span class="red">*</span>
									<div class="control">
										<input type="text" value="<?= $result->address ?>" class="form-control input-sm" name="address"  id="address" />
									</div> 
								</div> 
							</div> 	
						</div>
					</div> 
					<div class="col-sm-12 form-group hidden">
						<?php echo lang('attachment','attachment'); ?>​ 
						<span class="red">
							<i class="fa fa-file-word-o" aria-hidden="true"></i>&nbsp;
							<i class="fa fa-file-pdf-o" aria-hidden="true"></i>
						</span>	
						<input accept=".pdf,.doc" type="file" name="attachment" data-browse-label="<?= lang('document'); ?>" ​ data-show-upload="false" data-show-preview="false" accept="image/*" class="file" />	
					</div> 
					<div class="col-sm-12">
						<div class="form-group">
							<div class="control">
								<button type="submit"  class="btn btn-default btn-success"><?= lang("submit") ?></button>
							</div>
						</div>
					</div>  
				</div>  
				<div class="col-sm-5">	
					<div class="row">
						<div class="col-sm-6" >
							<div class="form-group">
								<?php echo lang('លេខទំនាក់ទំនងពេលមានអាសន្ន', 'លេខទំនាក់ទំនងពេលមានអាសន្ន'); ?>​
								<span class="red">*</span>
								<div class="control">
									<input type="text" value='<?= set_value('emergencies_contact', $result->emergencies_contact);?>' class="form-control input-sm bfh-phone" data-format="+855 (ddd) ddd-dddd" name="emergencies_contact" />
								</div>
							</div>
						</div>
						<div class="col-sm-6" >
							<div class="form-group">
								<?php echo lang('លេខទំនាក់ទំនង សាមុីខ្លួន', 'លេខទំនាក់ទំនង សាមុីខ្លួន '); ?>​
								<span class="red">*</span>
								<div class="control">
									<input type="text" value='<?= set_value('phone',$result->phone);?>' class="form-control input-sm bfh-phone" data-format="+855 (ddd) ddd-dddd" name="phone" />
								</div>
							</div>
						</div> 
					</div>
					<div class="form-group">
						<?php echo lang('អាសយដ្ឋានពេលមានអាសន្ន', 'អាសយដ្ឋានពេលមានអាសន្នន'); ?>​
						<span class="red">*</span>
						<div class="control">
							<input type="text" value='<?= set_value('emergencies_address', $result->emergencies_address);?>' class="form-control input-sm"  name="emergencies_address" />
						</div>
					</div>
					<div class="panel panel-warning">
						<div class="panel-heading"><?php echo lang("ភិនភាគ","ភិនភាគ") ?></div>
						<div class="panel-body" style="padding: 5px;">
						<div class="col-sm-6">
							<div class="form-group">
								<?php echo lang('កម្ពស់ ', 'កម្ពស់ '); ?>​
								<span class="red">*</span>
								<div class="control">
									<input type="text"​​ value='<?= set_value('height',$result->height);?>' class="form-control input-sm" name="height" />
								</div>
							</div>
						</div>
						<div class="col-sm-6">									
							<div class="form-group">
								<?php echo lang('សំបុរ ', 'សំបុរ '); ?>​ 
								<span class="red">*</span>
								<div class="control">
									<input type="text"​​ value='<?= set_value('complexion',$result->complexion);?>' class="form-control input-sm" name="complexion" />
								</div>
							</div>
						</div>​
						<div class="col-sm-6">
							<div class="form-group">
								<?php echo lang('ផ្ទៃមុខ ', 'ផ្ទៃមុខ '); ?>​
								<span class="red">*</span>
								<div class="control">
									<input type="text" value='<?= set_value('shape_of_face',$result->shape_of_face);?>' class="form-control input-sm" name="shape_of_face" />
								</div>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<?php echo lang('ភ្នែកពណ៌ ', 'ភ្នែកពណ៌ '); ?>​
								<span class="red">*</span>
								<div class="control">
									<input type="text" value='<?= set_value('color_of_eyes',$result->color_of_eyes);?>' class="form-control input-sm" name="color_of_eyes" />
								</div>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<?php echo lang('ស្លាកស្នាមពិសេស ', 'ស្លាកស្នាមពិសេស '); ?>​
								<span class="red">*</span>
								<div class="control">
									<input type="text" value='<?= set_value('distinguishing_marks',$result->distinguishing_marks);?>' class="form-control input-sm" name="distinguishing_marks" />
								</div>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<?php echo lang('សក់ ', 'សក់ '); ?>​
								<span class="red">*</span>
								<div class="control">
									<input type="text" value='<?= set_value('hair',$result->hair);?>' class="form-control input-sm" name="hair" />
								</div>
							</div>
						</div>
					</div>
				</div>	
					<div class="col-sm-6">
						<div class="form-group">
							<?php echo lang('ធ្វើដំណើរទៅប្រទេស ', 'ធ្វើដំណើរទៅប្រទេស '); ?>​(KH)
							<span class="red">*</span>
							<div class="control">
								<input type="text" value='<?= set_value('to_country_kh',$result->to_country_kh);?>' class="form-control input-sm" name="to_country_kh" />
							</div>
						</div>
					</div>
					<div class="col-sm-6" >									
						<div class="form-group">
							<?php echo lang('ធ្វើដំណើរទៅប្រទេស', 'ធ្វើដំណើរទៅប្រទេស'); ?>​(EN)
                            <span class="red">*</span>							
							<div class="control">
								<input type="text" value='<?= set_value('to_country',$result->to_country);?>' class="form-control input-sm " name="to_country" />
							</div>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="form-group">
							<?php echo lang('ដើម្បី ', 'ដើម្បី '); ?>​(KH)
							<span class="red">*</span>
							<div class="control">
								<input type="text" value='<?= set_value('travel_for_kh',$result->travel_for_kh);?>' class="form-control input-sm" name="travel_for_kh" />
							</div>
						</div>
					</div>
					<div class="col-sm-6">									
						<div class="form-group">
							<?php echo lang('ដើម្បី', 'ដើម្បី'); ?>(EN)
							<span class="red">*</span>​  
							<div class="control">
								<input type="text" value='<?= set_value('travel_for',$result->travel_for);?>' class="form-control input-sm" name="travel_for" />
							</div>
						</div>
					</div>
			</div> 
			<div class='col-sm-2'>
				<?= lang("photo", "photo"); ?>
				<?php
					$photo = "no_image.png";
					if(!empty($result->photo) || !file_exists("assets/uploads/".$result->photo)){
						$photo = $result->photo;
					}
				?>
				<div class="form-group">
					  <div class="main-img-preview">
						<img class="thumbnail img-preview" id='img_preview' src="<?= set_value('preview',site_url("assets/uploads/".$photo)); ?>" title="Preview Logo" name='preview'>
					  </div>
					  <div class="input-group">
						<input id="fakeUploadLogo" class="form-control input-sm fake-shadow" placeholder="<?= lang("choose_file") ?>" disabled="disabled">
						<div class="input-group-btn">
						  <div class="fileUpload btn btn-danger fake-shadow">
							<span><i class="fa fa-upload"></i>&nbsp;&nbsp;<?= lang("upload") ?></span>
							<input id="logo-id" accept=".gif,.jpg,.png" data-maxfile="1024" name="photo" type="file" class="attachment_upload">
						  </div>
						</div>
					  </div>
				</div>
			</div>
			<?php echo form_close(); ?> 
	</div>
</div>
<style>
	/* File Upload */
	.fake-shadow {
		box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
	}
	.fileUpload {
		position: relative;
		overflow: hidden;
	}
	.fileUpload #logo-id {
		position: absolute;
		top: 0;
		right: 0;
		margin: 0;
		padding: 0;
		font-size: 33px;
		cursor: pointer;
		opacity: 0;
		filter: alpha(opacity=0);
	}
	.img-preview {
		max-width: 100%;
		width:200px !important;
	}
</style>
<script type="text/javascript">
	$(document).ready(function() {
		var brand = document.getElementById('logo-id');
		brand.className = 'attachment_upload';
		brand.onchange = function() {
			document.getElementById('fakeUploadLogo').value = this.value.substring(12);
		};
		// Source: http://stackoverflow.com/a/4459419/6396981
		function readURL(input) {
			if (input.files && input.files[0]) {
				var reader = new FileReader();
				
				reader.onload = function(e) {
					$('.img-preview').attr('src', e.target.result);
				};
				reader.readAsDataURL(input.files[0]);
			}
		}
		$("#logo-id").change(function() {
			readURL(this);
		});
		
		$("#upload").click(function(){
			var filename = "<?= site_url()?>/" + $("#webcam-preview img").attr("src");
			$('.img-preview').attr('src', filename);
			return false;
		});
		
	});
</script>
<script type="text/javascript" src="<?= $assets ?>styles/helpers/components/js/bootstrap-formhelpers.js"></script>	
<link href="<?= $assets ?>styles/helpers/components/js/bootstrap-formhelpers.min.css" rel="stylesheet" />
<script type="text/javascript">
	$(function(){ 
		$(".case_no").autocomplete({
			source: function (request, response) {
				var q = request.term;
				if($.isNumeric(q)){
					$.ajax({
						type: 'get',
						url: '<?= site_url('applications/suggestions_caseno'); ?>',
						dataType: "json",
						data: {
							q : q
						},
						success: function (data) {
							response(data);
						}
					});
				}
			},
			minLength: 1,
			autoFocus: false,
			delay: 200,
			response: function (event, ui) {
				if (ui.content[0].id == 0) {
					$(this).removeClass('ui-autocomplete-loading');
					$(this).removeClass('ui-autocomplete-loading');
				}
			},
			select: function (event, ui) {
				event.preventDefault();
				if (ui.item.id !== 0) { 
					var date = new Date(ui.item.row.dob);
					var dob = (date.getDate() + 1) + '/' + pad(date.getMonth()) + '/' +  date.getFullYear();
					
					$("input[name='application_id'").val(ui.item.row.application_id);
					$("input[name='user_name'").val(ui.item.row.lastname+"_"+ui.item.row.firstname);
					$("input[name='username'").val(ui.item.row.lastname_kh+" "+ui.item.row.firstname_kh);
					$("input[name='ethnicity'").val(ui.item.row.ethnicity_kh); 
					$("#"+ui.item.row.gender+"").iCheck("check"); 
					$("#"+ui.item.row.marital_status+"").iCheck("check");
					$("input[name='dob']").val(dob); 
				} 
			}
		});
		
		getAllAutoComplete(".village, .village","/getAllTags?v=village");
		
	function getAllAutoComplete(element, base_url){
		$(element).autocomplete({
			source: function (request, response) {
				var q = request.term;
				$.ajax({
					type: 'GET',
					url: '<?= site_url('asylum_seekers/'); ?>'+base_url,
					dataType: "json",
					data: {
						q : q
					},
					success: function (data) {
						response(data);
					}
				});
			},
			minLength: 1,
			autoFocus: false,
			delay: 200,
			response: function (event, ui) {
				if (ui.content[0].id == 0) {
					$(this).removeClass('ui-autocomplete-loading');
					$(this).removeClass('ui-autocomplete-loading');
				}
			},
			select: function (event, ui) {
				event.preventDefault();
				$(this).val(ui.item.label);
			}
		});
	}
		
	}); 
	function pad(str) {
	  str = str.toString();
	  return str.length < 2 ? pad("0" + str, 2) : str;
	}
</script>


