<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">				
			<button type="button" class="close" data-dismiss="modal">
				<span aria-hidden="true"><i class="fa fa-2x">&times;</i></span>
				<span class="sr-only"><?=lang('close');?></span>
			</button>
			<h4 class="modal-title" id="payModalLabel"><?= lang('approve_recognition_refugee'); ?></h4>
		</div>
		
		<?php $attributes = array('id' => 'form1'); ?>
		
		<?php  echo form_open_multipart("application_forms/approve_recognition_refugee/".$id, $attributes); ?> 
		
		<div class="modal-body">
							
			<div class="col-sm-12">				
				<div class="form-group">
					<?php echo lang('លេខប្រកាស​', 'លេខប្រកាស​'); ?>​ 
					<span class="red">*</span>
					<div class="control">
						<input type="text" value="<?= $recognition->recognized_no ?>" id='recognized_no' name="recognized_no"  class="form-control input-sm" />
					</div>
				</div> 
				<div class="form-group">
					<?php echo lang('ថ្ងៃចេញប្រកាស', 'ថ្ងៃចេញប្រកាស'); ?>​ 
					<span class="red">*</span>
					<div class="control">
						<input type="text" value="<?= ($recognition->recognized_date)?$this->erp->hrsd($recognition->recognized_date):date("d/m/Y"); ?>" id='recognized_date' name='recognized_date'  class="form-control input-sm date"​ />						
					</div>
				</div>
				<div class="form-group">
					<?php echo lang('ថ្ងៃប្រគល់ប្រកាស', 'ថ្ងៃប្រគល់ប្រកាស'); ?>​ 
					<span class="red">*</span>
					<div class="control">
						<input type="text" value="<?= ($recognition->provided_date)?$this->erp->hrsd($recognition->provided_date):date("d/m/Y"); ?>" id='provided_date' name='provided_date'  class="form-control input-sm date"​ />						
					</div>
				</div>
				​
			</div>​
			<div class="clearfix"></div>			
		</div>
		 
		<div class="modal-footer">
		   <?php echo form_submit('submit', lang('submit'), 'class="btn btn-primary save"'); ?>
		</div>
		
		<?php  echo form_close(); ?>
		
	</div>
</div>

<?= $modal_js ?>
<style type="text/css"> 
	ul.ui-autocomplete {
		z-index: 1100 !important;
	}
</style>
<script type="text/javascript">
	$(function(){
		
		$(".case_no").autocomplete({
			source: function (request, response) {
				var q = request.term;
					$.ajax({
						type: 'get',
						url: '<?= site_url('applications/suggestions_caseno'); ?>',
						dataType: "json",
						data: {
							q : q
						},
						success: function (data) {
							response(data);
						}
					});
			},
			minLength: 1,
			autoFocus: false,
			delay: 200,
			response: function (event, ui) {
				if (ui.content[0].id == 0) {
					$(this).removeClass('ui-autocomplete-loading');
					$(this).removeClass('ui-autocomplete-loading');
				}
			},
			select: function (event, ui) {
				event.preventDefault();
				if (ui.item.id !== 0) { 					
					var id = ui.item.id;					
					
					/*
					*
					* SELECT MULTIPLE MEMBERS
					*/
					
					$.ajax({
						type: 'GET',
						url: '<?= site_url('applications/getFamilyRecognitionRefugees'); ?>',
						dataType: "json",
						data: { id : id },
						success: function (response) {											
							$(".div-member").html(response);						
							$('input[type="checkbox"],[type="radio"]').not('.skip').iCheck({
								checkboxClass: 'icheckbox_square-blue',
								radioClass: 'iradio_square-blue',
								increaseArea: '20%' // optional
							});
						}  
					}); 
					
					/*
					*
					* SELECT MEMBER 
					*/
					
					$.ajax({
						type: 'GET',
						url: '<?= site_url('application_forms/getMemberRefugees'); ?>',
						dataType: "json",
						data: { id : id },
						success: function (response) {											
							$("#members").html(response);
							$("select").select2();
						}  
					});
					
					$("input[name='application_id'").val(ui.item.row.id);
					$(this).val(ui.item.label);
				} 
			}
		});  
		
	});
</script>
