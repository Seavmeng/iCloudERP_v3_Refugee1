
<?php  
	$icon_check='<span style="font-size:17px;">&#x2611;</span>';
	$icon_uncheck='<span style="font-size:17px;">&#x2610;</span>';
?>
<div class="box a4" >
	<div class="box-content"> 		
		<?php $this->load->view($this->theme."applications/head-cover_1"); ?>  
		<div class='clearfix'></div>  
		<div class="col-sm-12 text-center" >
			<h2 class='text-cover'>ពាក្យស្នើសុំបញ្ឈប់ និងការលុបចោលឋានៈជនភៀសខ្លួន</h2> 
			<span>CESSATION AND ELIMINATION OF REFUGEE STATUS APPLICATION FORM</span>
		</div>  
		<div class='clearfix'></div>  
		<br/>
		<div class='wrap-content'>
			<table width='100%' style='white-space:nowrap;'>
				<tr>
					<td width='300'>នាមត្រកូល / Surname </td> 
					<td>នាមខ្លួន / Given Name </td>
					<td rowspan="6" width="100">
						<?php
							if(!empty($result->photo) || !file_exists("assets/uploads/".$result->photo)){
								echo "<img src='assets/uploads/".$result->photo."' width='100' height='125' class='thumb' />";
							}
						?>
					</td>
				</tr>
				<tr>
					<td>
						<?= $result->firstname_kh." / ".strtoupper($result->firstname); ?>
					</td> 
					<td>
						<?= $result->lastname_kh." / ".strtoupper($result->lastname); ?>
					</td>
				</tr>
				<tr>
					<td>ថ្ងៃ ខែ ឆ្នាំកំណើត  / Date of Birth : <?= $this->erp->toKhmer($this->erp->hrsd($result->dob)) ?></td>
					<td>
						ភេទ / Sex:
						ប្រុស / Male &nbsp; <?php echo  ($result->gender == 'male' ? $icon_check : $icon_uncheck);?>
						&nbsp; 
						ស្រី / Female &nbsp; <?php echo  ($result->gender == 'female' ? $icon_check : $icon_uncheck);?>
					</td>
				</tr>
				<tr>
					<td colspan='2'> ទីកន្លែងកំណើត/Place of Birth : <?= $result->pob_kh ?></td>
				</tr>​
			</table> 
			<table width='100%' style='margin-top:-20px;'> 
					<tr>
						<td>លេខករណី</td>
						<td>សញ្ជាតិ</td>​​
						<td>ថ្ងៃខែឆ្នាំ​ទទួលឋាន :</td>
					</tr>
					<tr>
						<td>Case Number​: <?= $result->case_prefix.$result->case_no ?></td>
						<td>Nationality: <?= $result->nationality_kh ?></td>
						<td>Date of Status Recognition: <?= $this->erp->toKhmer($this->erp->hrsd($result->registered_date)) ?></td>
					</tr>​
			</table>
			<?php if(!empty($family_member)){ ?>
				<table width='80%' style='margin:0 auto;'>
					<tr>
						<td colspan="7" class='text-center'>
							សមាជិកគ្រួសាររួមដំណើរជាមួយ / Family members travel with
						</td> 
					</tr> 
					<tr>
						<td width='10'></td>  
						<td>ឈ្មោះ</td>
						<td></td>
						<td>ភេទ</td>  
						<td></td>  
						<td>ថ្ងៃខែឆ្នាំកំណើត</td>  
						<td></td>  
					</tr> 
					<?php foreach ($family_member as $i => $fam_mem){ ?>
						<tr>
							<td><?= ($i+1); ?></td>  
							<td>Name</td>
							<td><?= $fam_mem->lastname_kh.' '.$fam_mem->firstname_kh; ?></td>  
							<td>Sex</td>  
							<td><?= $this->erp->genderToKhmer($fam_mem->gender); ?><b/></td>  
							<td>Date of Birth</td>  
							<td><?= $this->erp->toKhmer($this->erp->hrsd($fam_mem->dob)); ?><b/></td>  
						</tr>   
						<?php  } ?>
				</table> 
			<?php } ?>
			
			<table width='100%'>
				<tr>
					<td width="100">
						អាសយដ្ឋាននៅកម្ពុជា / ទូរស័ព្ទទំនាក់ទំនង  Address in Cambodia / Contact Number:
					</td> 
				</tr>
				<tr> 
					<td>
						<?= $this->applications->getAllAddressKH($application->country,"country"); ?> 
						<?= $this->applications->getAllAddressKH($application->province,"province"); ?>
						<?= $this->applications->getAllAddressKH($application->district,"district"); ?>
						<?= $this->applications->getAllAddressKH($application->communce,"communce"); ?>
						<?= $this->applications->getTagsById($application->village); ?> ,
						<?= $application->address_kh ?>
						/
						<?= $application->contact_number ?>
					</td>
				</tr>
				<tr>
					<td class='space-size'> 
						ខ្ញុំបាទ / នាងខ្ញុំស្នើសុំនាយកដ្ឋានជនភៀសខ្លួន នៃអគ្គនាយកដ្ឋានអន្តោប្រវេសន៍បញ្ឈប់និងលប់ចោលឋានៈជាជនភៀសខ្លួននៅកម្ពុជាដោយ ហេតុផលដូចខាងក្រោម / I hereby would like to request the Refugee Department, General Department of Immigration to cancel my refugee status and close the case in Cambodia for the following reason(s):
					</td>
				</tr>   
				<tr>
					<td> 
						&nbsp;<hr/>
					</td>
				</tr> 
				<tr>
					<td> 
						&nbsp;<hr/>
					</td>
				</tr> 
				<tr>
					<td> 
						&nbsp;<hr/>
					</td>
				</tr> 
				<tr>
					<td> 
						&nbsp;<hr/>
					</td>
				</tr> 
			</table> 
			
			<div class="clearfix"></div> 
			<?php $this->load->view($this->theme."application_forms/form_footer"); ?>  
			<div class="clearfix"></div>
			
			<br/>
			
		</div>	
	</div>
</div>
  
<style type="text/css">
	@media print{
		.bblack{ background: white !important; }   
		.modal-content, .box, 
		.modal-header{ border:none!important; }
		.table { border:none !important;}
	}
	.wrap-content {
		min-height:800px;
		border:1px solid #000;
		font-size:14px;
	}
	.wrap-content table td {
		padding:4px;
	}
	hr{
		padding:0px;
		margin:0px;
		border-top:none !important;
		border-bottom:dotted 1px !important;
	}   
</style>