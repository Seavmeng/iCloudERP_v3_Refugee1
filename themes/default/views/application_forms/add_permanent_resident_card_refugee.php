
<div class="modal-dialog modal-lg">
	<div class="modal-content">
		<div class="modal-header">				
			<button type="button" class="close" data-dismiss="modal">
				<span aria-hidden="true"><i class="fa fa-2x">&times;</i></span>
				<span class="sr-only"><?=lang('close');?></span>
			</button>
			<h4 class="modal-title" id="payModalLabel"><?= lang("add_permanent_resident_card_refugee") ?></h4>
		</div>
		<?php  echo form_open_multipart("application_forms/add_permanent_resident_card_refugee/".$result->id); ?>
		<?php 
			$countries_ = array(lang("select"));
			foreach($countries as $country){
				$countries_[$country->id] = $country->country;
			} 
			?>
			<?php 
			$provinces_ = array(lang("select"));
			foreach($provinces as $province){
				$provinces_[$province->id] = $province->name;
			} 
			?>
			<?php 
			$districts_ = array(lang("select"));
			foreach($districts as $district){
				$districts_[$district->id] = $district->name;
			} 
			?>
			<?php 
			$communes_ = array(lang("select"));
			foreach($communes as $commune){
				$communes_[$commune->id] = $commune->name;
			} 
			
		?>

		<div class="modal-body">
		
			<div class="col-sm-9">
			
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<?php echo lang('case_no', 'case_no'); ?>​ 
							<span class="red">*</span>
							<div class="control">
								<input type="text"  class="form-control input-sm case_no"  id="case_no" />
								<input type="hidden" name="application_id" id="application_id">
							</div>
						</div>  
					</div>  
					<div class="col-lg-6">
						<?php echo lang('ឈ្មោះអ្នកស្នើសុំ', 'ឈ្មោះអ្នកស្នើសុំ'); ?>​ 
						<span class="red">*</span>
						<div class="control" id="members">
							<select class="form-control" id="member_id"></select>
						</div>
				    </div>				
				</div> 
				<div class="row">
				    <div class="col-md-6">
						<div class="form-group">
							<?php echo lang('occupation', 'occupation'); ?>​ (KH)
							<span class="red">*</span>
							<div class="control">
								<input type="text" value="" class="form-control input-sm " name="occupation_kh"  id="occupation_kh" />
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<?php echo lang('occupation', 'occupation'); ?>​ (EN)
							<span class="red">*</span>
							<div class="control">
								<input type="text" value="" class="form-control input-sm " name="occupation"  id="occupation" />
							</div>
						</div>
					</div>
				</div>
				<div class="panel panel-warning">
					<div class="panel-heading"><?php echo lang("address","address") ?></div>
					<div class="panel-body" style="padding: 5px;">
						
						<div class="col-lg-12">
							<div class="form-group">
								<?php echo lang('country', 'country'); ?>
								<div class="controls">
									<?php echo form_dropdown('country', $countries_, 1, ' class="form-control" '); ?>												
								</div>
							</div>
						</div>
						
						<div class="col-lg-6">
							<div class="form-group">
								<?php echo lang('province', 'province'); ?>
								<div class="controls">
									<?php echo form_dropdown('province', $provinces_, 0, ' class="form-control" '); ?>												
								</div>
							</div>
						</div>
						
						<div class="col-lg-6">
							<div class="form-group">
								<?php echo lang('district', 'district'); ?>
								<div class="controls">
									<?php echo form_dropdown('district', $districts_, 0, ' class="form-control" '); ?>												
								</div>
							</div>
						</div>
						
						<div class="col-lg-6">
							<div class="form-group">
								<?php echo lang('commune', 'commune'); ?>
								<div class="controls">
									<?php echo form_dropdown('commune', $communes_, 0, ' class="form-control" '); ?>												
								</div>
							</div>
						</div>
						
						<div class="col-lg-6">
							<div class="form-group">
								<?php echo lang('village', 'village'); ?>
								<span class="red">*</span>
								<div class="controls">
									<?php 
									 echo form_input('village', set_value('village'),'class="form-control village" ');
									?>											
								</div>
							</div>
						</div>
						
						<div class="col-lg-12">
							<div class="form-group"> 
								<?php echo lang('other', 'other'); ?>​ 
								<span>(KH)</span>
								<span class="red">*</span>
								<div class="control">
									<input type="text" value="" class="form-control input-sm" name="address_kh"  id="address_kh" />
								</div> 
							</div> 
						</div> 
						
						<div class="col-lg-12">
							<div class="form-group"> 
								<?php echo lang('other', 'other'); ?>​ 
								<span>(EN)</span>
								<span class="red">*</span>
								<div class="control">
									<input type="text" value="<?= set_value('address_kh');?>" class="form-control input-sm" name="address"  id="address" />
								</div> 
							</div> 
						</div> 
						
					</div>
				</div> 
				
				<div class="row">
					
					<div class="col-lg-6">
						<div class="form-group"> 
							<?php echo lang('contact_number', 'contact_number'); ?>​ 
							<span class="red">*</span>
							<div class="control">
								<input type="text" class="form-control input-sm bfh-phone" data-format="+855 (ddd) ddd-dddd" name="contact_number" id="contact_number" />
							</div> 
						</div>
					</div>
                   <div class="col-lg-6">
						 <div class="form-group"> 
							<div class="control">
								<?php echo lang('ថ្ងៃចេញប្រកាស ', 'ថ្ងៃចេញប្រកាស '); ?>
								<div class="controls">
									<input type="text" value="<?= set_value('recognized_date') ?>" name="recognized_date" class="form-control input-sm date" />
								</div>
							</div> 
						</div> 
					</div> 
					<div class="col-lg-6">
						<div class="form-group">
							<?php echo lang('reasons', 'reasons'); ?>​ 
							<span class="red">*</span>
							<div class="control">
								<?php 
								$reasons_ = array();
								foreach($reasons as $reason){
									$reasons_[$reason->id] = $reason->name_kh;
								}
								echo form_dropdown('reasons', $reasons_, 0, ' class="form-control" '); ?>						 
							</div>
						</div>
					</div> 
					
					<div class="col-lg-6">
						<div class="form-group"> 
							<?php echo lang('ទុតិយតា', 'ទុតិយតា'); ?>​ 
							<span class="red">*</span>
							<div class="control">
								<label class="checkbox-inline">
								  <input type="checkbox" name="repeat" value="1">
								</label>
							</div> 
						</div>
					</div>
                   
				</div>	
			 
			 </div>
			 
			 
			 
			<div class="col-md-3">
				<?= lang("photo", "photo"); ?>
				<?php
					$photo = "no_image.png";
					if(!empty($request_card->photo) || !file_exists("assets/uploads/".$request_card->photo)){
						$photo = $request_card->photo;
					}
				?>
				<div class="form-group">
					  <div class="main-img-preview">
						<img class="thumbnail img-preview" id='img_preview' src="<?= site_url("assets/uploads/".$photo); ?>" title="Preview Logo" alt='image profile'>
					  </div>
					  <div class="input-group">
						<input id="fakeUploadLogo" class="form-control input-sm fake-shadow" placeholder="<?= lang("choose_file") ?>" disabled="disabled">
						<div class="input-group-btn">
						  <div class="fileUpload btn btn-danger fake-shadow">
							<span><i class="fa fa-upload"></i>&nbsp;&nbsp;<?= lang("upload") ?></span>
							<input id="logo-id" accept=".gif,.jpg,.png" data-maxfile="1024" name="photo" type="file" class="attachment_upload" >
						  </div>
						</div>
					  </div>
				</div>
				<input type="hidden" name="photoHidden" />
			</div>	
				
			<div class="clearfix"></div> 
		</div>
		
		<div class="modal-footer">
			<?php echo form_submit('form1', lang('submit'), 'class="btn btn-primary save-data"'); ?>
		</div>
		<?php  echo form_close(); ?>
	</div>
</div>
<style type="text/css"> 
	ul.ui-autocomplete {
		z-index: 1100 !important;
	}
	/* File Upload */
	.fake-shadow {
		box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
	}
	.fileUpload {
		position: relative;
		overflow: hidden;
	}
	.fileUpload #logo-id {
		position: absolute;
		top: 0;
		right: 0;
		margin: 0;
		padding: 0;
		font-size: 33px;
		cursor: pointer;
		opacity: 0;
		filter: alpha(opacity=0);
	}
	.img-preview {
		max-width: 100%;
		width:200px !important;
	}
</style>
<script type="text/javascript">
	$(document).ready(function() {
		var brand = document.getElementById('logo-id');
		brand.className = 'attachment_upload';
		brand.onchange = function() {
			document.getElementById('fakeUploadLogo').value = this.value.substring(12);
		};
		// Source: http://stackoverflow.com/a/4459419/6396981
		function readURL(input) {
			if (input.files && input.files[0]) {
				var reader = new FileReader();
				
				reader.onload = function(e) {
					$('.img-preview').attr('src', e.target.result);
				};
				reader.readAsDataURL(input.files[0]);
			}
		}
		$("#logo-id").change(function() {
			readURL(this);
		});
		
		$("#upload").click(function(){
			var filename = "<?= site_url()?>/" + $("#webcam-preview img").attr("src");
			$('.img-preview').attr('src', filename);
			return false;
		});
		
	});
</script>
<script type="text/javascript" src="<?= $assets ?>styles/helpers/components/js/bootstrap-formhelpers.js"></script>	
<link href="<?= $assets ?>styles/helpers/components/js/bootstrap-formhelpers.min.css" rel="stylesheet" />
<?= $modal_js ?>
<script type="text/javascript">
	/* $(".save-data").on('click',function(event){	 
		var application_id = $('#application_id').val();
		var case_no = $('#case_no').val();
		var occupation= $('#occupation').val();
		var address = $('#address').val();
		var contact_number = $('#contact_number').val(); 
		var file = $("#logo-id").val();
		if(application_id == '' || occupation == '' || address == '' || contact_number == "+855 " ){ 
			return false;
		} 
	}); */
	$(function(){ 
		$(".case_no").autocomplete({
			source: function (request, response) {
				var q = request.term;
				//if($.isNumeric(q)){
					$.ajax({
						type: 'get',
						url: '<?= site_url('applications/suggestions_caseno_recognition'); ?>',
						dataType: "json",
						data: {
							q : q
						},
						success: function (data) {
							response(data);
						}
					});
				//}
			},
			minLength: 1,
			autoFocus: false,
			delay: 200,
			response: function (event, ui) {
				if (ui.content[0].id == 0) {
					$(this).removeClass('ui-autocomplete-loading');
					$(this).removeClass('ui-autocomplete-loading');
				}
			},
			select: function (event, ui) {
				event.preventDefault();
				if (ui.item.id !== 0) {  
					var id = ui.item.id;
					$.ajax({
						type: 'GET',
						url: '<?= site_url('application_forms/getMemberRefugees'); ?>',
						dataType: "json",
						data: { id : id },
						success: function (response) {								
							$("#members").html(response);				
							$("select").select2();
							$("input[name='application_id'").val(ui.item.row.application_id);					
							$("#member_id").trigger("change");	
						}  
					});
					$(this).val(ui.item.label);
						 
				} 
			}
		});  
	});

	$(document).on('change','#member_id',function(){
		var application_id = $('#application_id').val();
		var member_id = $('select#member_id').val();
		$.ajax({
			url: '<?= site_url('application_forms/getLastDataByApplication'); ?>',
			type: 'GET',
			dataType: "JSON",
			contentType:'applications/json; charset=utf-8',
			data: { application_id : application_id,
					member_id : member_id
			 },
			success: function (d) {										
				$('input[name="occupation_kh"]').val(d.result.occupation_kh);
				$('input[name="occupation"]').val(d.result.occupation);
				$("select[name='country'] option[value="+d.result.country+"]").attr('selected','selected').change();
				$("select[name='province'] option[value="+d.result.province+"]").attr('selected','selected').change();
				$("select[name='district'] option[value="+d.result.district+"]").attr('selected','selected').change();
				$("select[name='commune'] option[value="+d.result.commune+"]").attr('selected','selected').change();
				$("input[name='village']").val(d.vill);
				$('input[name="address_kh"]').val(d.result.address_kh);
				$('input[name="address"]').val(d.result.address);
				$('input[name="contact_number"]').val(d.result.phone);
				$("#img_preview").attr('src','assets/uploads/'+d.result.photo).change();
				$("input[name='photoHidden']").val(d.result.photo);
			}  
		});
	});

	function pad(str) {
	  str = str.toString();
	  return str.length < 2 ? pad("0" + str, 2) : str;
	}
	
	getAllAutoComplete(".village, .village","/getAllTags?v=village");
		
	function getAllAutoComplete(element, base_url){
		$(element).autocomplete({
			source: function (request, response) {
				var q = request.term;
				$.ajax({
					type: 'GET',
					url: '<?= site_url('asylum_seekers/'); ?>'+base_url,
					dataType: "json",
					data: {
						q : q
					},
					success: function (data) {
						response(data);
					}
				});
			},
			minLength: 1,
			autoFocus: false,
			delay: 200,
			response: function (event, ui) {
				if (ui.content[0].id == 0) {
					$(this).removeClass('ui-autocomplete-loading');
					$(this).removeClass('ui-autocomplete-loading');
				}
			},
			select: function (event, ui) {
				event.preventDefault();
				$(this).val(ui.item.label);
			}
		});
	}
		
</script>