
<?php  
	$icon_check='<span style="font-size:17px;">&#x2611;</span>';
	$icon_uncheck='<span style="font-size:17px;">&#x2610;</span>';
?>
<div class="box a4" >
	<div class="box-content"> 		
		<?php $this->load->view($this->theme."applications/head-cover_1"); ?>  
		<div class='clearfix'></div>  
		<div class="col-sm-12 text-center" >
			<h2 class='text-cover'>បែបបទសុំបញ្ឈប់ឋានៈជនភៀសខ្លួន</h2> 
			<h3>CESSATION OF REFUGEE STATUS FORM</h3>
		</div>  
		
		<div class='wrap-content'>
			<table width="100%" style='white-space:nowrap;'>
			   <tr>
					<td width="20">-</td>
					<td>លេខករណី /  Case Number​: <?= $this->erp->toKhmer($result->case_prefix.$result->case_no) ;?></td>
					<td rowspan='4' class='text-right cover-photo'>
						
						<?php if($member_declare){ ?>
							<img src="<?= base_url("assets/uploads/members/".$member_declare->photo) ?>"  />
						<?php } else { ?>
							<img src="<?= base_url("assets/uploads/".$result->photo) ?>" />
							<span style="margin-right: 20px;">(ម្ចាស់ករណី)</span>
						<?php } ?>
						
					</td>
               </tr>
			   <tr>
				  <td width="20">-</td>
                  <td>
					ជាម្ចាស់ករណី / Principle Applicant: <?php echo ($application->member_id == 0 ? $icon_check : $icon_uncheck);?>
					&nbsp;&nbsp;&nbsp;&nbsp;
					ឬ ជាសមាជិកក្នុងបន្ទុក / Dependent Member: <?php echo ($application->member_id >= 1 ? $icon_check : $icon_uncheck);?>
					</td> 
			   </tr>
			   <tr>
				  <td width="20">-</td>
				  <td>នាមត្រកូល និងនាមខ្លួន / Surname and Given Name:</td>​​
			   </tr>
			   <tr>
				  <td width="20"></td>
				  <td> 
					 <?php if($member_declare){ ?>
						<?= $member_declare->lastname_kh." ".$member_declare->firstname_kh; ?> 
						<?php if($member_declare->lastname != "") echo " / " . strtoupper($member_declare->lastname." ".$member_declare->firstname); ?>
					 <?php } else { ?>
						<?= $result->lastname_kh." ".$result->firstname_kh; ?> 
						<?php if($result->lastname != "") echo " / " . strtoupper($result->lastname." ".$result->firstname); ?>
					<?php } ?>	
				  </td>
			   </tr>
			   <tr>
				   <td width="20">-</td>
                   <td>
					 <?php 
						if($member_declare){ 
							$gender = $member_declare->gender;
						} else {
							$gender = $result->gender;
						} 
						?>	
					​ភេទ/ Sex : 
					<span>ប្រុស / Male: <?php echo ($gender == 'male' ? $icon_check : $icon_uncheck); ?></span>
					<span>ស្រី / Female: <?php echo ($gender == 'female' ? $icon_check : $icon_uncheck); ?></span>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  ថ្ងៃខែឆ្នាំកំណើត / Date of Birth : 
					 <?php if($member_declare){ ?>
						<?= $this->erp->toKhmer($this->erp->hrsd($member_declare->dob))?>
					 <?php } else { ?>
						<?= $this->erp->toKhmer($this->erp->hrsd($result->dob))?>
					 <?php } ?>						
				  </td>
			   </tr>
			   <tr>
				  <td width="20">-</td>
				  <td>ទីកន្លែងកំណើត / Place of Birth :
					 <?php 
						if($member_declare){ 
							$pob_kh = $member_declare->pob_kh;
						} else {
							$pob_kh = $result->pob_kh;
						} 
						?>
					 <?= $pob_kh?> 
				  </td>
			   </tr> 
			   <tr>
				  <td width="20">-</td>
				  <td>
					 សញ្ជាតិ / Nationality:
					<?php if($member_declare){ ?>
					<?= $member_declare->nationality_kh." / ".$member_declare->nationality ; ?>
					<?php } else { ?>
					<?= $result->nationality_kh." / ".$result->nationality ; ?>
					<?php } ?>
				  </td>
			   </tr>
			   <tr>
				  <td width="20">-</td>
				  <td> ថ្ងៃខែឆ្នាំ​ទទួលឋានៈ / Date of Status Recognition: 
					 <?= ($decision->recognized_date>0)?$this->erp->toKhmer($this->erp->hrsd($decision->recognized_date)):""; ?>
				  </td>
			   </tr>
			</table>
			
			<?php if($members){ ?>
			<table width="100%">
				<tr>
					<td class='text-center'>សមាជិកគ្រួសាររួមដំណើរជាមួយ / Family members travel with.</td>
				</tr>
			</table>			
			<table width="70%" style='margin:0 auto;'>				
				<tr>
					<td width='30px'>ល.រ </td>
					<td>ឈ្មោះ / Name </td> 
					<td width="100px">ភេទ  / Gender</td>
					<td>ថ្ងៃខែឆ្នាំកំណើត / Date of Birth</td>
				</tr>
				<?php   				
					foreach($members as $key => $m){
					$member = $this->applications->getFamilyMembersDependantAccompanyingById($m->member_id);					
				?>
					<tr>
						<td class="text-center"><?= $this->erp->toKhmer($key+1) ?>. </td>
						<td><?= $member->lastname_kh ." ".$member->firstname_kh ?></td>
						<td class="text-left"><?= $this->erp->genderToKhmer($member->gender) ?></td>
						<td class="text-left"><?= $this->erp->toKhmer($this->erp->hrsd($member->dob)); ?></td>
					</tr>
				<?php } ?>
			</table>
			<?php } ?>
			
			<table width='100%'>
				<tr>
					<td width='20' style="vertical-align: top;">-</td>
					<td>
					អាសយដ្ឋាននៅកម្ពុជា  / Address in Cambodia:    
						<?= $application->address_kh ?>
						<?= $this->applications->getTagsById($application->village); ?>,
						<?= $this->applications->getAllAddressKH($application->commune,"commune"); ?>  
						<?= $this->applications->getAllAddressKH($application->district,"district"); ?> 
						<?= $this->applications->getAllAddressKH($application->province,"province"); ?>
						<?= $this->applications->getAllAddressKH($application->country,"country"); ?> 
					</td>
				</tr>  
				<tr>
					<td width='20'>-</td>
					<td>
						លេខទូរស័ព្ទទំនាក់ទំនង  / Contact Number: <?php echo $this->erp->toKhmer($application->contact_number)?>
					</td>
				</tr>
				<tr>
					<td width='20'>-</td>
					<td>
						ហេតុផល / Reason(s):
					</td>
				</tr>
				<?php if($application->reasons){ ?>
				<tr>
					<td></td>
					<td colspan="2"> 
						<?= $application->reasons; ?>
					</td>
				</tr>
				<?php }else{ ?>
				<tr>
					<td colspan="2"> 
						&nbsp;<hr/>
					</td>
				</tr>
				<tr>
					<td colspan="2"> 
						&nbsp;<hr/>
					</td>
				</tr>
				<tr>
					<td colspan="2"> 
						&nbsp;<hr/>
					</td>
				</tr>
				<?php } ?>
			</table> 
			
			<div class="clearfix"></div> 
			<?php $this->load->view($this->theme."application_forms/form_footer1"); ?>  
			<div class="clearfix"></div>
			
			<br/>
			
		</div>	
	</div>
</div>
  
<style type="text/css">
	@media print{
		.bblack{ background: white !important; }   
		.modal-content, .box, 
		.modal-header{ border:none!important; }
		.table { border:none !important;}
		.box-content{
			padding: 20px !important;
		}
	}
	.wrap-content {
		min-height:800px;
		font-size:14px;
	}
	.wrap-content table td {
		padding:4px;
	}
	hr{
		padding:0px;
		margin:0px;
		border-top:none !important;
		border-bottom:dotted 1px !important;
	}   
</style>