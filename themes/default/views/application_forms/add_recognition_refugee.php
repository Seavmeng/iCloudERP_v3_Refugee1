<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">				
			<button type="button" class="close" data-dismiss="modal">
				<span aria-hidden="true"><i class="fa fa-2x">&times;</i></span>
				<span class="sr-only"><?=lang('close');?></span>
			</button>
			<h4 class="modal-title" id="payModalLabel"><?= lang('ការទទួលស្គាល់ជនភៀសខ្លួន'); ?></h4>
		</div>
		
		<?php $attributes = array('id' => 'form1'); ?>
		
		<?php  echo form_open_multipart("application_forms/add_recognition_refugee/", $attributes); ?> 
		
		<div class="modal-body">
							
			<div class="col-sm-12">
				<div class="form-group">
					<?php echo lang('case_no', 'case_no'); ?>​ 
					<span class="red">*</span>
					<div class="control">
						<input type="text"  class="form-control  input-sm case_no" id="case_no" />
						<input type="hidden" name="application_id"  />
					</div>
				</div> 
				
				<div class="form-group">
					<?php echo lang('លេខករណីជា ៖', 'លេខករណីជា ៖'); ?> 
					<div class="controls">
						<?php $applicants = array("0"=>lang("ជាម្ចាស់ករណី"),"1"=>lang("ជាសមាជិកក្នុងបន្ទុក")); ?>
						<?php echo form_dropdown('applicant', $applicants, 0, ' class="form-control applicant" '); ?>												
					</div>
				</div>
				
				<script type="text/javascript">
					$(function(){
						$(".select_member").hide();
						
						$(".applicant").on("change",function(){
							var applicant = $(this).val();
							if(applicant == 1){
								$(".applicant_members").hide();
								$(".select_member").show();
							}else{
								$(".applicant_members").show();
								$(".select_member").hide();
							}
						});
					})
				</script>
				
				<div class="clearfix"></div>
				
				<div class="form-group applicant_members">
					<?php echo lang('សមាជិកគ្រួសារ', 'សមាជិកគ្រួសារ'); ?>​ 
					<span class="red">*</span>
					<div class="controls"> 
						<table width="100%" class='table table-condensed table-bordered'>
							<thead>
							<tr>
								<td width='30'>#</td>
								<td>ឈ្មោះ</td>
								<td>ភេទ </td>
								<td>ទំនាក់ទំនង</td>
								<td>ថ្ងៃខែឆ្នាំកំណើត </td>
							</tr>
							</thead>
							<tbody class='div-member'>
								
							</tbody>
						</table>
					</div> 
				</div>
				
				<div class="form-group select_member">
					<?php echo lang('ឈ្មោះអ្នកស្នើសុំ', 'ឈ្មោះអ្នកស្នើសុំ'); ?>​
					<span class="red">*</span>
					<div class="controls" id="members"> 
						<select class="form-control"></select>
					</div>
				</div>
				​
			</div>​
			<div class="clearfix"></div>
			
		</div>
		 
		<div class="modal-footer">
		   <?php echo form_submit('submit', lang('submit'), 'class="btn btn-primary save save-data"'); ?>
		</div>
		
		<?php  echo form_close(); ?>
		
	</div>
</div>

<?= $modal_js ?>
<style type="text/css"> 
	ul.ui-autocomplete {
		z-index: 1100 !important;
	}
</style>
<script type="text/javascript">
	$(".save-data").on('click',function(event){  
        var application_id = $('input[name="application_id"]').val();
        if( application_id == ''){ 
            bootbox.alert('<?= lang("please_select_all");?>');
            return false;
        } 
    });
	$(function(){
		
		$(".case_no").autocomplete({
			source: function (request, response) {
				var q = request.term;
					$.ajax({
						type: 'get',
						url: '<?= site_url('applications/suggestions_caseno_recognition'); ?>',
						dataType: "json",
						data: {
							q : q
						},
						success: function (data) {
							response(data);
						}
					});
			},
			minLength: 1,
			autoFocus: false,
			delay: 200,
			response: function (event, ui) {
				if (ui.content[0].id == 0) {
					$(this).removeClass('ui-autocomplete-loading');
					$(this).removeClass('ui-autocomplete-loading');
				}
			},
			select: function (event, ui) {
				event.preventDefault();
				if (ui.item.id !== 0) { 					
					var id = ui.item.id;					
					
					/*
					*
					* SELECT MULTIPLE MEMBERS
					*/
					
					$.ajax({
						type: 'GET',
						url: '<?= site_url('applications/getFamilyRecognitionRefugees'); ?>',
						dataType: "json",
						data: { id : id },
						success: function (response) {											
							$(".div-member").html(response);						
							$('input[type="checkbox"],[type="radio"]').not('.skip').iCheck({
								checkboxClass: 'icheckbox_square-blue',
								radioClass: 'iradio_square-blue',
								increaseArea: '20%' // optional
							});
						}  
					}); 
					
					/*
					*
					* SELECT MEMBER 
					*/
					
					$.ajax({
						type: 'GET',
						url: '<?= site_url('application_forms/getMemberRefugees'); ?>',
						dataType: "json",
						data: { id : id },
						success: function (response) {											
							$("#members").html(response);
							$("select").select2();
						}  
					});
					
					$("input[name='application_id'").val(ui.item.row.id);
					$(this).val(ui.item.label);
				} 
			}
		});  
		
	});
</script>
