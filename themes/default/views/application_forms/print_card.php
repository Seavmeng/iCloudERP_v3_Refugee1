

<div class="card" width="410px;"> 
	<table  class="information_table">
		<?php
			$photo = "no_image.png";
			if(!empty($request_card->photo) || !file_exists("assets/uploads/".$request_card->photo)){
				$photo = $request_card->photo;
			}
		?> 
		<tr>
			<td rowspan="7">
				<div class="pic_profile">
					<img src="<?= site_url("assets/uploads/".$photo); ?>" style="width: 85px;height: 100px;"/>
				</div>
			</td>
		</tr>
		<tr>
			<td>
				<p>ឈ្មោះ / Name:
				<span>
					<?= $application->firstname_kh ?> &nbsp;
					<?= $application->lastname_kh ?>	
				</span>
				</p>
			</td>
		</tr>
		<tr>
			<td>
				ភេទ / Sex:
				<span>
				<?= lang($application->gender) ?>
				</span>
			</td>
		</tr>
		<tr>
			<td>
				ថ្ងៃខែឆ្នាំកំណើត​ / Date of Birth:
				<span>
				<?= $this->erp->toKhmer($this->erp->hrsd($application->dob)) ?>
				</span>
			</td>
		</tr>
		<tr>
			<td>
				សញ្ជាតិ / Nationality:
				<span>
				<?= $application->nationality_kh ?>
				</span>
			</td>
		</tr>
		<tr>
			<td>
				ប្រកាសទទួលស្គាល់ឋានៈរបស់ក្រសួងមហាផ្ទៃលេខ :
				<span>
					<?= $this->erp->toKhmer($recognization->recognized_no) ?> ប្រ.ក
				<span>						
			</td>
		</tr>
		<tr>
			<td>
				ថ្ងៃចេញប្រកាស :
				<span>
					<?= $this->erp->toKhmer($this->erp->hrsd($recognization->recognized_date)); ?>
				</span>
			</td>
		</tr>
		<tr>
			<td>
				<p style="margin-top:-5px;">
				ករណីលេខ <br>
				ID:
				<span>
					<?= $this->erp->toKhmer($application->case_no) ?>
				</span>
				</p>
			</td>
			<td>
				អាសយដ្ឋាន:
				   <span>
					<?= $this->applications->getAllAddressKH($request_card->country,"country"); ?>
					<?= $this->applications->getAllAddressKH($request_card->province,"province"); ?>
					<?= $this->applications->getAllAddressKH($request_card->district,"district"); ?>
					<?= $this->applications->getAllAddressKH($request_card->communce,"commune"); ?>
					<?= $this->applications->getTagsById($request_card->village); ?>
					<?= $request_card->address_kh ?> 
				   </span>
		   </td> 
		</tr>
		<tr>
			<td>
				<span  style="position:absolute;color:red;font-weight:bold;"><?= ($request_card->repeat==1?"(ទុតិយតា)":"") ?></span>
			</td>
		</tr>
		
	</table>
	
	<div class="card_left">
		<br/><br/><br/>
		<table class="expiry_table">
			<tr>
				<td>ថ្ងៃផុតកំណត់</td>
				<td>
					<span>
					<?= $this->erp->toKhmer($this->erp->hrsd($request_card->expiry_date)); ?>		
					</span>
				</td>
			</tr>
			<tr>
				<td>Expiry Date</td>
				<td>
					<span>
						<?= $this->erp->hrsd($request_card->expiry_date); ?>
					</span>							
				</td>
			</tr>
		</table>
	</div>
	
	<div class="card_right text-center">
		<div class="stamp_signature_box">
			<img src="<?php echo base_url('assets/stamp_and_signature3.png'); ?>" class="stamp_signature" />
			<p class="stamp_signature_title">
				នាយឧត្តមសេនីយ៍
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<span class="text-cover" style="color:#f90808;">សុខ ផល</span>
			</p>
		</div>	
		<p>ភ្នំពេញ, ថ្ងៃទី <?= $this->erp->toKhmer(date("d")) ?> ខែ <?= $this->erp->KhmerMonth(date("m")) ?> ឆ្នាំ <?= $this->erp->toKhmer(date("Y")) ?></p>				
		<span class="text-cover ">អគ្គនាយកអន្តោប្រវេសន៍</span>		
	</div>	 
</div>
<div class="card_front"> 
	<div class="card_no"> 
		001
	</div>
	<br/>
	<div class="barcode">    
		<div class="col-xs-12 text-right"> 
			<?php $br = $this->erp->save_barcode('001', 'code39',20, false); ?> 
			<img src="<?= base_url() ?>assets/uploads/barcode<?= $this->session->userdata('user_id') ?>.png" alt="001"/>     
		</div> 
	</div> 
</div>  
<style type="text/css">
	@font-face {
		font-family: "Siemreap";
		font-style: normal;
		font-weight: 400;
		src: local('Siemreap Regular'), local('Siemreap-Regular'), url(<?=$assets?>/fonts/Siemreap.woff2) format('woff2');
		unicode-range: U+1780-17FF, U+200B-200C, U+25CC;
	}
	@font-face {
		font-family: 'Moul';
		font-style: normal;
		font-weight: 400;
		src: local('Moul'), url(<?=$assets?>/fonts/Moul.woff2) format('woff2');
		unicode-range: U+1780-17FF, U+200B-200C, U+25CC;
	}
	@media print{
		.information_table td{ font-size:10px !important; color: #2e7bd1 !important;}
	    .information_table span,.expiry_table span{ font-size:10px !important; }
		.expiry_table td{font-size:8px !important;}
		.card_right{ font-size:10px !important; margin-top: -10px!important; margin-right: 10px!important; }
		.card_front,.card{padding:0px !important; height: 210px !important; width: 410px !important;border:none !important;}			
	}
	.stamp_signature_box{
		position:absolute; 
		margin-left:-20px; 
	}
	.stamp_signature{
		width:120px !important;
		z-index: 0 !important;
	}
	.stamp_signature_title{
		margin:0px;
		padding:0px;
		color:#f90808;
		font-weight:bold;
	}
	.card,.card_front{ 
		height: 250px;
		width: 410px;
		font-size: 12px;
		border: 1px solid;
		border-radius: 10px;
		padding:10px;
	} 
	.barcode,.card_no { 
		width: 100px;
		position: relative;
		top: 200px ;
		text-align: center;
		right: 20px;
		font-weight:bold;
		float: right;
	}
	.information_table td  {
		line-height: 15px;
		vertical-align: top;
		color: #2e7bd1;
		font-size: 12px;
		font-family: 'Times New Roman', 'Siemreap', sans-serif;
		white-space: nowrap;
		font-weight:bold;
    }
	.card_left{
		float: left;
		font-size: 12px;
		font-family: 'Times New Roman', 'Siemreap', sans-serif;
	}
	.card_right{
		color: #2e7bd1;
		float: right;
		font-size: 12px;
		font-family: 'Times New Roman', 'Siemreap', sans-serif;
		line-height:7px;
		font-weight:bold;
		
	}
	.pic_profile{
	    padding-left:2px;
		margin:5px;
		margin-left:-3px;
		margin-left:-3px;
	}
	.pic_profile img{
		border: 2px solid #EEE;
		width: 85px;
		height: 100px;
	}
	.text-cover{
		font-family: 'Moul', sans-serif !important;
	}
	.expiry_table{
		border-collapse: collapse; 
	}
	.expiry_table td{
		padding: 2px;
		border: 1.5px solid #2e7bd1;
		color: #2e7bd1; 
		font-weight:bold;
		white-space: nowrap;
		font-size: 12px;
	}
	.information_table span, .expiry_table span{
		color: #000000;
		font-weight: bold;
		white-space: normal;
		word-wrap: break-word;
		font-size: 12px;
	} 
</style>