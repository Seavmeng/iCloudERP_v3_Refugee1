<?php
	$v='';
	
	if($this->input->post('case_no')){
		$v .='&case_no='. $this->input->post('case_no');
	}
	
	if($this->input->post('date_from')){
		$v .='&date_from='. $this->input->post('date_from');
	}
	if($this->input->post('date_to')){
		$v .='&date_to='. $this->input->post('date_to');
	}
?>

<ul id="myTab" class="nav nav-tabs">
	<li class="">
		<a href="#content_2" class="tab-grey"><?= lang("permanent_resident_card_refugee"); ?></a>
	</li>
</ul>

<div class="tab-content">

	<div id="content_2" class="tab-pane fade in">

		<script>
				$(document).ready(function () {
					var oTable = $('#dataTable13').dataTable({
						"aaSorting": [[8,'desc']],
						"aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?= lang('all') ?>"]],
						"iDisplayLength": <?= $Settings->rows_per_page ?>,
						'bProcessing': true, 'bServerSide': true,
						'sAjaxSource': "<?= site_url('application_forms/getPermanentResidentCardRefugees/?v=1'.$v) ?>",
						'fnServerData': function (sSource, aoData, fnCallback) {
							aoData.push({
								"name": "<?= $this->security->get_csrf_token_name() ?>",
								"value": "<?= $this->security->get_csrf_hash() ?>"
							});
							$.ajax({
									'dataType': 'json',
									'type': 'POST',
									'url': sSource,
									'data': aoData,
									'success': fnCallback
									});
						},
						"aoColumns": [
						{"bSortable": false, "mRender": checkbox},
						{"sClass" : "center"},
						{"sClass" : "center"},
						{"sClass" : "center"},
						{"sClass" : "center", "mRender" : fld},
						{"sClass" : "center"},
						{"sClass" : "center"},
						{"sClass" : "center"},
						{"sClass" : "center", "mRender" : fld},
						{"mRender" : row_status},
						{"mRender" : fld ,"sClass" : "center"}, 
						{"bSortable": false, "sClass" : "center"}]
						,'fnRowCallback': function (nRow, aData, iDisplayIndex) {
								var oSettings = oTable.fnSettings();
								var action = $('td:eq(11)', nRow); 
								if (aData[9] == 'approved'){
									action.find('.approve-permanent_resident').remove();
									action.find('.edit-permanent_resident').remove();
									//action.find('.delete-permanent_resident').remove();
									action.find('.reject-permanent_resident').remove();

								}
								return nRow;
							},
							"fnFooterCallback": function (nRow, aaData, iStart, iEnd, aiDisplay) {
								var total = 0, total_kh = 0;
								for (var i = 0; i < aaData.length; i++)
								{
								}
								var nCells = nRow.getElementsByTagName('th');
							}
						}).fnSetFilteringDelay().dtFilter([
							{column_number: 0, filter_default_label: "[<?= lang("#") ?>]", filter_type: "text", data: []},
							{column_number: 1, filter_default_label: "[<?= lang("case_no") ?>]", filter_type: "text", data: []},
							{column_number: 2, filter_default_label: "[<?= lang("firstname") ?>]", filter_type: "text", data: []},
							{column_number: 3, filter_default_label: "[<?= lang("lastname") ?>]", filter_type: "text", data: []},
							{column_number: 4, filter_default_label: "[<?= lang("dob") ?>]", filter_type: "text", data: []},
							{column_number: 5, filter_default_label: "[<?= lang("gender") ?>]", filter_type: "text", data: []},
							{column_number: 6, filter_default_label: "[<?= lang("nationality") ?>]", filter_type: "text", data: []},
							{column_number: 7, filter_default_label: "[<?= lang("relationship") ?>]", filter_type: "text", data: []},
							{column_number: 8, filter_default_label: "[<?= lang("requested_date") ?>]", filter_type: "text", data: []},
							{column_number: 9, filter_default_label: "[<?= lang("status") ?>]", filter_type: "text", data: []},
							{column_number: 10, filter_default_label: "[<?= lang("expiry_date") ?>]", filter_type: "text", data: []},
						], "footer");
				});
			</script>

			<div class="box">
				<div class="box-header">
					<h2 class="blue">
						<i class="fa-fw fa fa-barcode"></i>
						<?= lang('permanent_resident_card_refugee'); ?>
					</h2>

					<div class="box-icon">
						<ul class="btn-tasks">
							<li class="dropdown">
								<a href="#" class="toggle_up tip" title="<?= lang('hide_form') ?>">
									<i class="icon fa fa-toggle-up"></i>
								</a>
							</li>
							<li class="dropdown">
								<a href="#" class="toggle_down tip" title="<?= lang('show_form') ?>">
									<i class="icon fa fa-toggle-down"></i>
								</a>
							</li>

							<li class="dropdown">
								<a data-toggle="dropdown" class="dropdown-toggle" href="#">
									<i class="icon fa fa-tasks tip" data-placement="left" title="<?= lang("actions") ?>"></i>
								</a>
								<ul class="dropdown-menu pull-right tasks-menus" role="menu" aria-labelledby="dLabel">
									<li>
										<a data-backdrop="static" data-keyboard="false"  data-toggle="modal" data-target="#myModal"  href="<?= site_url('application_forms/add_permanent_resident_card_refugee'); ?>"><i class="fa fa-plus-circle"></i>
											<?= lang("add_permanent_resident_card_refugee"); ?>
										</a>
									</li>
								</ul>
							</li>
					</div>

				</div>

				<div class="box-content">
					<div class="row">
						<?= form_open_multipart("application_forms/permanent_resident_card_refugee"); ?>

						<div class="col-sm-12">
							<p class="introtext"><?= lang('list_results'); ?></p>
							<div class="form">

								<div class="row">
                                    <div class="col-sm-2">
										<div class="form-group">
											<?php echo lang('case_no', 'case_no'); ?>
											<input name="case_no" class="form-control input-sm" type="text">
										</div>
									</div>
									<div class="col-sm-2">
										<div class="form-group">
											<?php echo lang('កាលបរិច្ឆេទសុំចាប់ពី', 'កាលបរិច្ឆេទសុំចាប់ពី'); ?>
											<input name="date_from" value="" class="form-control input-sm date" type="text">
										</div>
									</div>
									<div class="col-sm-2">
										<div class="form-group">
											<?php echo lang('រហូតដល់ថ្ងៃទី', 'រហូតដល់ថ្ងៃទី'); ?>
											<input name="date_to" value="" class="form-control input-sm date" type="text">
										</div>
									</div> 

									<div class="col-sm-12">
										<div class="form-group">
											<input type="submit" value="<?= lang("search") ?>" name="search" class="btn btn-success" />
										</div>
									</div>

								</div>

							</div>

							<div class="table-responsive">
								<table id="dataTable13" class="table table-condensed table-bordered table-hover table-striped">
									<thead>
										<tr>
											<th style="width:3%;"><?= lang("#") ?></th>
											<th><?= lang("case_no") ?></th>
											<th><?= lang("firstname") ?></th>
											<th><?= lang("lastname") ?></th>
											<th><?= lang("dob") ?></th>
											<th><?= lang("gender") ?></th>
											<th><?= lang("nationality") ?></th>
											<th><?= lang("relationship") ?></th>
											<th><?= lang("requested_date") ?></th>
											<th><?= lang("status") ?></th>
											<th><?= lang("expiry_date") ?></th> 
											<th><?= lang("action") ?></th>
										</tr>
									</thead>
									<tbody>

									</tbody>
									<tfoot>
										<tr>
											<th></th>
											<th></th>
											<th></th> 
											<th></th>
											<th></th>
											<th></th>
											<th></th>
											<th></th>
											<th></th>
											<th></th>
											<th></th>
											<th></th>
										</tr>
									</tfoot>
								</table>
							</div>

						</div>
						<?= form_close(); ?>
					</div>
				</div>
			</div>

	</div>


</div>

<style type="text/css">
	.table{
		white-space: nowrap;
		width:100%;
	}
</style>

<script language="javascript">
    $(document).ready(function () {

		$(".form").slideUp();
        $('.toggle_down').click(function () {
            $(".form").slideDown();
            return false;
        });

        $('.toggle_up').click(function () {
            $(".form").slideUp();
            return false;
        });
    });
</script>
