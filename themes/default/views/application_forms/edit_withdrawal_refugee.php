
<div class="modal-dialog modal-lg">
	<div class="modal-content">
		<div class="modal-header">				
			<button type="button" class="close" data-dismiss="modal">
				<span aria-hidden="true"><i class="fa fa-2x">&times;</i></span>
				<span class="sr-only"><?=lang('close');?></span>
			</button>
			<h4 class="modal-title" id="payModalLabel"><?= lang("edit_withdrawal_refugee") ?></h4>
		</div>
		<?php  echo form_open_multipart("application_forms/edit_withdrawal_refugee/".$id); ?>
		<?php 
			$countries_ = array(lang("select"));
			foreach($countries as $country){
				$countries_[$country->id] = $country->country;
			} 
			?>
			<?php 
			$provinces_ = array(lang("select"));
			foreach($provinces as $province){
				$provinces_[$province->id] = $province->name;
			} 
			?>
			<?php 
			$districts_ = array(lang("select"));
			foreach($districts as $district){
				$districts_[$district->id] = $district->name;
			} 
			?>
			<?php 
			$communes_ = array(lang("select"));
			foreach($communes as $commune){
				$communes_[$commune->id] = $commune->name;
			} 
		?>
		<div class="modal-body">  
			
			<div class="col-sm-9">
			
					<div class="form-group">
						<?php echo lang('case_no', 'case_no'); ?>​ 
						<span class="red">*</span>
						<div class="control">
							<input type="text" value="<?= $result->case_prefix.$result->case_no?>" disabled class="form-control input-sm case_no">
							<input type="hidden" name="application_id" value="<?= $result->id;?>">
						</div>
					</div> 
				
					<div class="panel panel-warning">
						<div class="panel-heading"><?php echo lang("address","address") ?></div>
						<div class="panel-body" style="padding: 5px;">
							<div class="col-lg-12">
								<div class="form-group">
									<?php echo lang('country', 'country'); ?>
									<div class="controls">
										<?php echo form_dropdown('country', $countries_, $withdrawal->country, ' class="form-control" '); ?>												
									</div>
								</div>
							</div>
							
							<div class="col-lg-6">
								<div class="form-group">
									<?php echo lang('province', 'province'); ?>
									<div class="controls">
										<?php echo form_dropdown('province', $provinces_, $withdrawal->province, ' class="form-control" '); ?>												
									</div>
								</div>
							</div>
							
							<div class="col-lg-6">
								<div class="form-group">
									<?php echo lang('district', 'district'); ?>
									<div class="controls">
										<?php echo form_dropdown('district', $districts_,  $withdrawal->district, ' class="form-control" '); ?>												
									</div>
								</div>
							</div>
							
							<div class="col-lg-6">
								<div class="form-group">
									<?php echo lang('commune', 'commune'); ?>
									<div class="controls">
										<?php echo form_dropdown('commune', $communes_,  $withdrawal->commune, ' class="form-control" '); ?>												
									</div>
								</div>
							</div>
							
							<div class="col-lg-6">
								<div class="form-group">
									<?php echo lang('village', 'village'); ?>
									<span class="red">*</span>
									<div class="controls">
										<?php 
										echo form_input('village', set_value('village',$this->applications->getTagsById($withdrawal->village)),'class="form-control village" '); ?>											
									</div>
								</div>
							</div>
							
							<div class="col-lg-12">
								<div class="form-group"> 
									<?php echo lang('other', 'other'); ?>​ 
									<span>(KH)</span>
									<span class="red">*</span>
									<div class="control">
										<input type="text" value="<?= $withdrawal->address_kh ?>" class="form-control input-sm" name="address_kh"  id="address_kh" />
									</div> 
								</div> 
							</div> 
							
							<div class="col-lg-12">
								<div class="form-group"> 
									<?php echo lang('other', 'other'); ?>​ 
									<span>(EN)</span>
									<span class="red">*</span>
									<div class="control">
										<input type="text" value="<?= $withdrawal->address ?>" class="form-control input-sm" name="address"  id="address" />
									</div> 
								</div> 
							</div> 
						</div>
					</div> 
				<div class="form-group">
					<?php echo lang('contact_number', 'contact_number'); ?>​ 
					<span class="red">*</span>
					<div class="control">
						<input type="text" value="<?= $withdrawal->contact_number?>" class="form-control input-sm bfh-phone" data-format="+855 (ddd) ddd-dddd" name="contact_number" id="contact_number" />
					</div> 
				</div>
				<div class="form-group">
					<?php echo lang('reasons', 'reasons'); ?>​ 
					<span class="red">*</span>
					<div class="control">
						 <textarea rows='5' class="form-control input-sm text-area" name="reasons" id="reasons">
						 <?= $withdrawal->reasons?>
						 </textarea>
					</div>
				</div> 	 			
			</div> 	
			
			<div class="col-md-3">
				<?= lang("photo", "photo"); ?>
				<?php
					$photo = "no_image.png";
					if(!empty($withdrawal->photo) || !file_exists("assets/uploads/".$withdrawal->photo)){
						$photo = $withdrawal->photo;
					}
				?>
				<div class="form-group">
					  <div class="main-img-preview">
						<img class="thumbnail img-preview" id='img_preview' src="<?= site_url("assets/uploads/".$photo); ?>" title="Preview Logo">
					  </div>
					  <div class="input-group">
						<input id="fakeUploadLogo" class="form-control input-sm fake-shadow" placeholder="<?= lang("choose_file") ?>" disabled="disabled">
						<div class="input-group-btn">
						  <div class="fileUpload btn btn-danger fake-shadow">
							<span><i class="fa fa-upload"></i>&nbsp;&nbsp;<?= lang("upload") ?></span>
							<input id="logo-id" accept=".gif,.jpg,.png" data-maxfile="1024" name="photo" type="file" class="attachment_upload">
						  </div>
						</div>
					  </div>
				</div>   
			</div>  
			<div class="clearfix"></div>  
		</div>
		
		<div class="modal-footer">
			<?php echo form_submit('form1', lang('submit'), 'class="btn btn-primary save-data"'); ?>
		</div>
		<?php  echo form_close(); ?>
	</div>
</div>

 <style type="text/css"> 
 
	ul.ui-autocomplete {
		z-index: 1100;
	}

	/* File Upload */
	
	.fake-shadow {
		box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
	}
	.fileUpload {
		position: relative;
		overflow: hidden;
	}
	.fileUpload #logo-id {
		position: absolute;
		top: 0;
		right: 0;
		margin: 0;
		padding: 0;
		font-size: 33px;
		cursor: pointer;
		opacity: 0;
		filter: alpha(opacity=0);
	}
	.img-preview {
		max-width: 100%;
		width:200px !important;
	}
</style>
<script type="text/javascript">
	$(document).ready(function() {
		var brand = document.getElementById('logo-id');
		brand.className = 'attachment_upload';
		brand.onchange = function() {
			document.getElementById('fakeUploadLogo').value = this.value.substring(12);
		};
		// Source: http://stackoverflow.com/a/4459419/6396981
		function readURL(input) {
			if (input.files && input.files[0]) {
				var reader = new FileReader();
				
				reader.onload = function(e) {
					$('.img-preview').attr('src', e.target.result);
				};
				reader.readAsDataURL(input.files[0]);
			}
		}
		$("#logo-id").change(function() {
			readURL(this);
		});
		
		$("#upload").click(function(){
			var filename = "<?= site_url()?>/" + $("#webcam-preview img").attr("src");
			$('.img-preview').attr('src', filename);
			return false;
		});
		
	});
</script>
<script type="text/javascript" src="<?= $assets ?>styles/helpers/components/js/bootstrap-formhelpers.js"></script>	
<link href="<?= $assets ?>styles/helpers/components/js/bootstrap-formhelpers.min.css" rel="stylesheet" />
<?= $modal_js ?>
<script type="text/javascript">
	$(function(){ 
		$(".case_no").autocomplete({
			source: function (request, response) {
				var q = request.term;
				if($.isNumeric(q)){
					$.ajax({
						type: 'get',
						url: '<?= site_url('applications/suggestions_caseno'); ?>',
						dataType: "json",
						data: {
							q : q
						},
						success: function (data) {
							response(data);
						}
					});
				}
			},
			minLength: 1,
			autoFocus: false,
			delay: 200,
			response: function (event, ui) {
				if (ui.content[0].id == 0) {
					$(this).removeClass('ui-autocomplete-loading');
					$(this).removeClass('ui-autocomplete-loading');
				}
			},
			select: function (event, ui) {
				event.preventDefault();
				if (ui.item.id !== 0) {  
					$("input[name='application_id'").val(ui.item.row.application_id);
					 
				} 
			}
		});
		
		getAllAutoComplete(".village, .village","/getAllTags?v=village");
		
	function getAllAutoComplete(element, base_url){
		$(element).autocomplete({
			source: function (request, response) {
				var q = request.term;
				$.ajax({
					type: 'GET',
					url: '<?= site_url('asylum_seekers/'); ?>'+base_url,
					dataType: "json",
					data: {
						q : q
					},
					success: function (data) {
						response(data);
					}
				});
			},
			minLength: 1,
			autoFocus: false,
			delay: 200,
			response: function (event, ui) {
				if (ui.content[0].id == 0) {
					$(this).removeClass('ui-autocomplete-loading');
					$(this).removeClass('ui-autocomplete-loading');
				}
			},
			select: function (event, ui) {
				event.preventDefault();
				$(this).val(ui.item.label);
			}
		});
	}
		
	}); 
	function pad(str) {
	  str = str.toString();
	  return str.length < 2 ? pad("0" + str, 2) : str;
	}
	
	
</script>