
<?php  	
	$icon_check='<span style="font-size:17px;">&#x2611;</span>';
	$icon_uncheck='<span style="font-size:17px;">&#x2610;</span>'; 
?>
<div class="box a4" >
	<div class="box-content"> 					
		<?php $this->load->view($this->theme."application_forms/form_header"); ?> 
		
		<div class="col-sm-12 text-center">
			<h2 class='text-cover' style="margin-top:0px;">ពាក្យស្នើសុំប័ណ្ណសំគាល់ជនភៀសខ្លួន  <?= ($request_card->repeat==1?"(ទុតិយតា)":"") ?></h2>
			<h3>REQUEST FOR REFUGEE IDENTITY CARD</h3>
		</div>   
			
		<div class="wrap-content">
				<table width='100%'>
					<tr>
						<td width='20'>-</td>
						<td>
						     លេខករណី / Case Number:
								<?php echo $result->case_prefix.$result->case_no; ?>
						</td>
						<td rowspan='4' class='text-right cover-photo'>
							<?php
								$photo = "no_image.png";
								if(!empty($request_card->photo) || !file_exists("assets/uploads/".$request_card->photo)){
									$photo = $request_card->photo;
								}
							?>
							<img src="<?= base_url("assets/uploads/".$photo); ?>" class="thumb" />
						</td>
					</tr>
					<tr>
						<td width='20'>-</td>
						<td>		
						ជាម្ចាស់ករណី / Principle Applicant: <?php echo ($request_card->member_id == 0 ? $icon_check : $icon_uncheck);?>
						&nbsp;&nbsp;
						ឬ ជាសមាជិកក្នុងបន្ទុក / Dependent Member: <?php echo ($request_card->member_id >= 1 ? $icon_check : $icon_uncheck);?>
						</td> 
					</tr>    
					<tr>
						<td width='20'>-</td>
						<td>នាមត្រកូល និងនាមខ្លួន / Surname and Given Name:</td> 
					</tr>    
					<tr>
						<td width='20'></td> 
						<td colspan='2'>
							<?php 
								if($member){ 
									echo $member->lastname_kh.' '.$member->firstname_kh." / ".strtoupper($member->lastname." ".$member->firstname);
								}else {
									echo $result->lastname_kh.' '.$result->firstname_kh." / ".strtoupper($result->lastname." ".$result->firstname);
								}
							?>
						</td>
					</tr>
				</table>
					
				<table width='100%'>
                    <tr>
						<td width='20'>-</td>
						<td>
							<?php 								
								if($member){ 
									$gender = $member->gender;
								}else {
									$gender = $result->gender;
								}
							?>							
							​ភេទ/ Sex : 
							<span>ប្រុស / Male: <?php echo ($gender == 'male' ? $icon_check : $icon_uncheck); ?></span>
							<span>ស្រី / Female: <?php echo ($gender == 'female' ? $icon_check : $icon_uncheck); ?></span>
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							  - &nbsp;&nbsp; ថ្ងៃខែឆ្នាំកំណើត  /  Date of  Birth :
							<?php 
								if($member){ 
									 echo date('d/m/Y',strtotime($member->dob));
								}else {
									echo  date('d/m/Y',strtotime($result->dob));
								}
								if(!$member){
								echo '<span​ style="float: right;margin-right: 35px;">(ម្ចាស់ករណី)</span>';
								}
							?>
						</td>	
					
					</tr> 
					<tr>
						<td>-</td>
						<td>
							ទីកន្លែងកំណើត / Place of Birth: 
							<?php 
								if($member){ 
									echo $member->pob;
								}else {
									echo $result->pob;
								}
							?>
						</td>
					</tr> 
					<tr>
						<td>-</td>
						<td>​សញ្ជាតិ / Nationality : 
						<?php 
							if($member){ 
								echo $member->nationality_kh." / ".$member->nationality;
							}else {
								echo $result->nationality_kh." / ".$result->nationality;
							}
						?>
						</td>
					</tr> 
					<tr>
						<td width='20'>-</td>
						<td>
							ថ្ងៃខែឆ្នាំ​បានទទួលឋានៈ / Date of Status Recognition: 
							<?= date("d/m/Y",strtotime($request_card->recognized_date)) ?>
						</td>
					</tr> 
					<tr>
						<td width='20'>-</td>
						<td>
							​មុខរបរបច្ចុប្បន្ន / Current Occupation : 							
							<?= $request_card->occupation; ?>							
						</td>
					</tr>
					<tr>
						<td width='20'>-</td>
						  <td>
							អាសយដ្ឋាននៅកម្ពុជា  / Address in Cambodia: 
						  </td>
					</tr> 
					<tr>
						<td width='20'></td>
						<td>   
							<?= $request_card->address_kh ?>
							<?= $this->applications->getTagsById($request_card->village); ?>,
							<?= $this->applications->getAllAddressKH($request_card->commune,"commune"); ?>  
							<?= $this->applications->getAllAddressKH($request_card->district,"district"); ?> 
							<?= $this->applications->getAllAddressKH($request_card->province,"province"); ?>
							<?= $this->applications->getAllAddressKH($request_card->country,"country"); ?> 
						</td> 
					</tr>
					<tr>
						<td width='20'>-</td>
						<td>
							លេខទូរស័ព្ទទំនាក់ទំនង  / Contact Number: 
							<?php echo $request_card->contact_number; ?>
						</td>
					</tr>
					<?php if($request_card->repeat==1){ ?>
					<tr>
						<td width='20'>-</td>
						<td>
							
							មូលហេតុ / Reason: &nbsp;&nbsp;	
							 <?= $reason->name_kh . " / " . $reason->name; ?>
						</td>
					</tr>
					<?php } ?>
					
					<tr>
						<td colspan="2" class="space-size">
							ខ្ញុំបាទ / នាងខ្ញុំសូមគោរពស្នើសុំនាយកដ្ឋានជនភៀសខ្លួន នៃអគ្គនាយកដ្ឋានអន្តោប្រវេសន៍ ចេញប័ណ្ណសំគាល់ជនភៀសខ្លួនសម្រាប់ប្រើប្រាស់ ជាផ្លូវការ។ រាល់ព័ត៌មានខាងលើពិតជាច្បាស់លាស់ និងត្រឹមត្រូវ បើខុសពីការពិត ខ្ញុំបាទ / នាងខ្ញុំសូមទទួលខុសត្រូវចំពោះមុខច្បាប់ ។
							<br/>I hereby would like to request the Refugee Department, General Department of Immigration to issue Refugee Identity Card for official use. The information provided above is true and accurate. If it is misrepresented or false, I will hold responsibility to the law.
						</td>
					</tr>
					
			</table>
			
			<div class="clearfix"></div>
			
			<?php $this->load->view($this->theme."application_forms/form_footer"); ?> 
			
			<div class="clearfix"></div> 
			
			<br/>
			
		</div> ​	
	</div>
</div>

<style type="text/css">
	@media print{
		.bblack{ background: white !important; }   
		.modal-content, .box, 
		.modal-header{ border:none!important; }
		.table { border:none !important; }
		/*.box, .box-content{
			margin-top:0px !important;
			padding-top:0px !important;
		}*/
		.box-content{
			padding: 20px!important;
		}
	}
	
	.wrap-content {
		min-height:800px;		
		font-size:14px;
	}
	.wrap-content table td {
		padding:2px;
		line-height:26px;
	}
</style>