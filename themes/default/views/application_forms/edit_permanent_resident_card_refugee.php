
<div class="modal-dialog modal-lg">
	<div class="modal-content">
		<div class="modal-header">				
			<button type="button" class="close" data-dismiss="modal">
				<span aria-hidden="true"><i class="fa fa-2x">&times;</i></span>
				<span class="sr-only"><?=lang('close');?></span>
			</button>
			<h4 class="modal-title" id="payModalLabel"><?= lang("edit_permanent_resident_card_refugee") ?></h4>
		</div>
		<?php  echo form_open_multipart("application_forms/edit_permanent_resident_card_refugee/". $id); ?>
		<?php 
			$countries_ = array(lang("select"));
			foreach($countries as $country){
				$countries_[$country->id] = $country->country;
			} 
			?>
			<?php 
			$provinces_ = array(lang("select"));
			foreach($provinces as $province){
				$provinces_[$province->id] = $province->name;
			} 
			?>
			<?php 
			$districts_ = array(lang("select"));
			foreach($districts as $district){
				$districts_[$district->id] = $district->name;
			} 
			?>
			<?php 
			$communes_ = array(lang("select"));
			foreach($communes as $commune){
				$communes_[$commune->id] = $commune->name;
			} 
		?>
		<div class="modal-body">  
		
			<div class="col-sm-9">
			
				<div class="row">
					<div class="col-lg-6">
						<div class="form-group">
							<?php echo lang('case_no', 'case_no'); ?>​ 
							<span class="red">*</span>
							<div class="control">
								<input type="text" class="form-control input-sm case_no"  disabled value="<?= $result->case_prefix.$result->case_no?>" >
								<input type="hidden" name="application_id" value="<?= $result->id;?>">
							</div>
						</div> 
					</div>  
					<div class="col-lg-6">
						<?php echo lang('ឈ្មោះអ្នកស្នើសុំ', 'ឈ្មោះអ្នកស្នើសុំ'); ?>​ 
						<?php
							$members_ = array(lang("select"));
							foreach($members as $member){
								$members_[$member->id] = $member->firstname_kh . " " . $member->lastname_kh;
							}						
						?>
						<span class="red">*</span>
						<div class="control" id="members">
							<?php echo form_dropdown('member', $members_, $request_card->member_id, ' class="form-control" disabled '); ?>
						</div>
				    </div>
				</div>
				<div class="row">
				    <div class="col-lg-6">
						<div class="form-group">
							<?php echo lang('occupation', 'occupation'); ?>​ (KH)
							<span class="red">*</span>
							<div class="control">
								<input type="text" value="<?= set_value('occupation_kh',$request_card->occupation_kh);?>" class="form-control input-sm " name="occupation_kh"  id="occupation_kh" />
							</div>
						</div> 
					</div>
					<div class="col-lg-6">
						<div class="form-group">
							<?php echo lang('occupation', 'occupation'); ?>​ (EN)
							<span class="red">*</span>
							<div class="control">
								<input type="text" value="<?= set_value('occupation',$request_card->occupation);?>" class="form-control input-sm " name="occupation"  id="occupation" />
							</div>
						</div> 
					</div>
				</div>
				<div class="panel panel-warning">
					<div class="panel-heading"><?php echo lang("address","address") ?></div>
					<div class="panel-body" style="padding: 5px;">
						
						<div class="col-lg-12">
							<div class="form-group">
								<?php echo lang('country', 'country'); ?>
								<div class="controls">
									<?php echo form_dropdown('country', $countries_, $request_card->country, ' class="form-control" '); ?>												
								</div>
							</div>
						</div>
						
						<div class="col-lg-6">
							<div class="form-group">
								<?php echo lang('province', 'province'); ?>
								<div class="controls">
									<?php echo form_dropdown('province', $provinces_, $request_card->province, ' class="form-control" '); ?>												
								</div>
							</div>
						</div>
						
						<div class="col-lg-6">
							<div class="form-group">
								<?php echo lang('district', 'district'); ?>
								<div class="controls">
									<?php echo form_dropdown('district', $districts_, $request_card->district, ' class="form-control" '); ?>												
								</div>
							</div>
						</div>
						
						<div class="col-lg-6">
							<div class="form-group">
								<?php echo lang('commune', 'commune'); ?>
								<div class="controls">
									<?php echo form_dropdown('commune', $communes_, $request_card->commune, ' class="form-control" '); ?>												
								</div>
							</div>
						</div>
						
						<div class="col-lg-6">
							<div class="form-group">
								<?php echo lang('village', 'village'); ?>
								<span class="red">*</span>
								<div class="controls">
									<?php 
									echo form_input('village', set_value('village',$this->applications->getTagsById($request_card->village)),'class="form-control village" '); ?>											
								</div>
							</div>
						</div>
						
						<div class="col-lg-12">
							<div class="form-group"> 
								<?php echo lang('other', 'other'); ?>​ 
								<span>(KH)</span>
								<span class="red">*</span>
								<div class="control">
									<input type="text" value="<?= $request_card->address_kh ?>" class="form-control input-sm" name="address_kh"  id="address_kh" />
								</div> 
							</div> 
						</div> 
						
						<div class="col-lg-12">
							<div class="form-group"> 
								<?php echo lang('other', 'other'); ?>​ 
								<span class="red">*</span>
								<div class="control">
									<input type="text" value="<?= $request_card->address ?>" class="form-control input-sm" name="address"  id="address" />
								</div> 
							</div> 
						</div> 
						
					</div>
				</div> 
				
				<div class="row">
					
					<div class="col-md-6">
						<div class="form-group"> 
							<?php echo lang('contact_number', 'contact_number'); ?>​ 
							<span class="red">*</span>
							<div class="control">
								<input type="text" value="<?= set_value('contact_number',$request_card->contact_number)?>" class="form-control input-sm bfh-phone" data-format="+855 (ddd) ddd-dddd" name="contact_number"  id="contact_number" />
							</div> 
						</div>	
					</div>
					<div class="col-md-6">
						<div class="form-group"> 
							<div class="control">
								<?php echo lang('ថ្ងៃចេញប្រកាស ', 'ថ្ងៃចេញប្រកាស '); ?>
								<span class="red">*</span>
								<div class="controls">
									<input type="text" value="<?= set_value('recognized_date', $this->erp->hrsd($request_card->recognized_date)) ?>" name="recognized_date" class="form-control input-sm date" />
								</div>
							</div> 
						</div>
					</div>
				    <div class="col-md-6">
						<div class="form-group">
							<?php echo lang('reasons', 'reasons'); ?>​ 
							<span class="red">*</span>
							<div class="control">
								<?php 
								$reasons_ = array();
								foreach($reasons as $reason){
									$reasons_[$reason->id] = $reason->name_kh;
								}
								echo form_dropdown('reasons', $reasons_, $request_card->reasons, ' class="form-control" '); ?>						 
							</div>
						</div>
					</div>
				    <div class="col-md-6">
						<div class="form-group"> 
							<?php echo lang('ទុតិយតា', 'ទុតិយតា'); ?>​ 					
							<div class="control">
								<label class="checkbox-inline">
								  <input type="checkbox" name="repeat" <?= (($request_card->repeat)?"checked":""); ?> value="1">
								</label>
							</div> 
						</div>
					</div>
				     
				</div>
				
				<div class="form-group hidden">
					<?php echo lang('attachment','attachment'); ?>​ 
					<span class="red">
						<i class="fa fa-file-word-o" aria-hidden="true"></i>&nbsp;
						<i class="fa fa-file-pdf-o" aria-hidden="true"></i>
					</span>	
					<input accept=".pdf,.doc" type="file" name="attachment" data-browse-label="<?= lang('document'); ?>" ​ data-show-upload="false" data-show-preview="false" accept="image/*" class="file" />	
				</div>  
			</div>
			
			<div class="col-md-3">
				<?= lang("photo", "photo"); ?>
				<?php
					$photo = "no_image.png";
					if(!empty($request_card->photo) || !file_exists("assets/uploads/".$request_card->photo)){
						$photo = $request_card->photo;
					}
				?>
				<div class="form-group">
				  <div class="main-img-preview">
					<img class="thumbnail img-preview" id='img_preview' src="<?= site_url("assets/uploads/".$photo); ?>" title="Preview Logo" alt='image profile'>
				  </div>
				  <div class="input-group">
					<input id="fakeUploadLogo" class="form-control input-sm fake-shadow" placeholder="<?= lang("choose_file") ?>" disabled="disabled">
					<div class="input-group-btn">
					  <div class="fileUpload btn btn-danger fake-shadow">
						<span><i class="fa fa-upload"></i>&nbsp;&nbsp;<?= lang("upload") ?></span>
						<input id="logo-id" accept=".gif,.jpg,.png" data-maxfile="1024" name="photo" type="file" class="attachment_upload" >
					  </div>
					</div>
				  </div>
				</div>
			</div>		
				
			<div class="clearfix"></div> 
		</div>
		
		<div class="modal-footer">
			<?php echo form_submit('form1', lang('submit'), 'class="btn btn-primary save-data"'); ?>
		</div>
		<?php  echo form_close(); ?>
	</div>
</div>
 <style type="text/css"> 
	ul.ui-autocomplete {
		z-index: 1100 !important;
	}
	/* File Upload */
	.fake-shadow {
		box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
	}
	.fileUpload {
		position: relative;
		overflow: hidden;
	}
	.fileUpload #logo-id {
		position: absolute;
		top: 0;
		right: 0;
		margin: 0;
		padding: 0;
		font-size: 33px;
		cursor: pointer;
		opacity: 0;
		filter: alpha(opacity=0);
	}
	.img-preview {
		max-width: 100%;
		width:200px !important;
	}
</style>
<script type="text/javascript">
	$(document).ready(function() {
		var brand = document.getElementById('logo-id');
		brand.className = 'attachment_upload';
		brand.onchange = function() {
			document.getElementById('fakeUploadLogo').value = this.value.substring(12);
		};
		// Source: http://stackoverflow.com/a/4459419/6396981
		function readURL(input) {
			if (input.files && input.files[0]) {
				var reader = new FileReader();
				
				reader.onload = function(e) {
					$('.img-preview').attr('src', e.target.result);
				};
				reader.readAsDataURL(input.files[0]);
			}
		}
		$("#logo-id").change(function() {
			readURL(this);
		});
		
		$("#upload").click(function(){
			var filename = "<?= site_url()?>/" + $("#webcam-preview img").attr("src");
			$('.img-preview').attr('src', filename);
			return false;
		});
		
	});
</script>
<script type="text/javascript" src="<?= $assets ?>styles/helpers/components/js/bootstrap-formhelpers.js"></script>	
<link href="<?= $assets ?>styles/helpers/components/js/bootstrap-formhelpers.min.css" rel="stylesheet" />
<?= $modal_js ?>
<script type="text/javascript">
	$(".save-data").on('click',function(event) {	 

			var application_id = $('#application_id').val();
			var occupation= $('#occupation').val();
			var address = $('#address').val();
			var contact_number = $('#contact_number').val();  
			if(application_id == '' || occupation == '' || address == '' || contact_number == "+855 " ){ 
				bootbox.alert('<?= lang("please_select_all");?>');
				return false;
			}  
			if(contact_number.length < 18 ) {
				bootbox.alert("<?= lang("wrong_contact_number_format") ?>");
				return false;
			} 
			
		});

	$(function(){ 
		$(".case_no").autocomplete({
			source: function (request, response) {
				var q = request.term;
				if($.isNumeric(q)){
					$.ajax({
						type: 'get',
						url: '<?= site_url('applications/suggestions_caseno'); ?>',
						dataType: "json",
						data: {
							q : q
						},
						success: function (data) {
							response(data);
						}
					});
				}
			},
			minLength: 1,
			autoFocus: false,
			delay: 200,
			response: function (event, ui) {
				if (ui.content[0].id == 0) {
					$(this).removeClass('ui-autocomplete-loading');
					$(this).removeClass('ui-autocomplete-loading');
				}
			},
			select: function (event, ui) {
				event.preventDefault();
				if (ui.item.id !== 0) {  
					$("input[name='application_id'").val(ui.item.row.application_id);
					 
				} 
			}
		});
		
	}); 
	function pad(str) {
	  str = str.toString();
	  return str.length < 2 ? pad("0" + str, 2) : str;
	}
	
	getAllAutoComplete(".village, .village","/getAllTags?v=village");
		
	function getAllAutoComplete(element, base_url){
		$(element).autocomplete({
			source: function (request, response) {
				var q = request.term;
				$.ajax({
					type: 'GET',
					url: '<?= site_url('asylum_seekers/'); ?>'+base_url,
					dataType: "json",
					data: {
						q : q
					},
					success: function (data) {
						response(data);
					}
				});
			},
			minLength: 1,
			autoFocus: false,
			delay: 200,
			response: function (event, ui) {
				if (ui.content[0].id == 0) {
					$(this).removeClass('ui-autocomplete-loading');
					$(this).removeClass('ui-autocomplete-loading');
				}
			},
			select: function (event, ui) {
				event.preventDefault();
				$(this).val(ui.item.label);
			}
		});
	}
	
	
</script>