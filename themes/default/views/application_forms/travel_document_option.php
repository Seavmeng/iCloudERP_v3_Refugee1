<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">				
			<button type="button" class="close" data-dismiss="modal">
				<span aria-hidden="true"><i class="fa fa-2x">&times;</i></span>
				<span class="sr-only"><?=lang('close');?></span>
			</button>
			<h4 class="modal-title" id="payModalLabel"><?= lang('travel_document_option'); ?></h4>
		</div>
		<?php $attributes = array('id' => 'form1'); ?>
		<?php  echo form_open_multipart("application_forms/travel_document_option/". $id, $attributes); ?> 
		<div class="modal-body">
			<?php
				$date = date("d/m/Y");
				$status = 0;
				
				if($travel->status == "approved"){
					$date = $this->erp->hrsd($travel->approved_date);
					$status = $travel->approved_by;
				}
				if($travel->status == "rejected"){
					$date = $this->erp->hrsd($travel->rejected_date);
					$status = $travel->rejected_by;
				}
			?>
			<div class="col-sm-12">
				<div class="form-group">
					<?php echo lang('date','date'); ?>​ 
					<span class="red">*</span>
					<div class="control">
						<input type="text" value="<?= ($date) ?>" id='date' name='date'  class="form-control input-sm date"​ />						
					</div>
				</div>
				<div class="form-group">
					<?php echo lang('status', 'status'); ?> 
					<div class="controls">
						<?php $applicants = array("approved"=>lang("approved"),"rejected"=>lang("rejected")); ?>
						<?php echo form_dropdown('status', $applicants, $status, ' class="form-control" '); ?>												
					</div>
				</div>
				<div class="clearfix"></div>
			</div>​
		</div>
		
		<div class="modal-footer">
		   <?php echo form_submit('submit', lang('submit'), 'class="btn btn-primary save"'); ?>
		</div>
		<?php  echo form_close(); ?>
	</div>
</div>
<?= $modal_js ?>


