<?php if ($Owner || $Admin) {  
?>

<script src="<?= $assets; ?>js/hc/highcharts.js"></script>
<script type="text/javascript">
    $(function () {
        $('#m2bschart').highcharts({
            chart: {type: 'column'},
            title: {text: ''},
            credits: {enabled: false},
            xAxis: {type: 'category', labels: {rotation: -60, style: {fontSize: '13px'}}},
            yAxis: {min: 0, title: {text: ''}},
            legend: {enabled: false},
            series: [{
                name: '<?=lang('របាយការណ៏');?>',
                data: [<?php 
     			if(!empty($counselings)){
					foreach ($counselings as $r) {
						if($r->number > 0) {
							echo "['(".lang($r->status).")', ".$r->number."],";
						}
					} 
                }
				if(!empty($rsds)){
					foreach ($rsds as $rd) {
						if($rd->number > 0) {
							echo "['(".lang($rd->status).")', ".$rd->number."],";
						}
					}
                }
                ?>],
                dataLabels: {
                    enabled: true,
                    rotation: -90,
                    color: '#000',
                    align: 'right',
                    y: -25,
                    style: {fontSize: '12px'}
                }
            }]
        });
		
		
        $('#chart').highcharts({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false
            },
            title: {text: ''},
            credits: {enabled: false},
            tooltip: {
                formatter: function () {
                    return '<div class="tooltip-inner hc-tip" style="margin-bottom:0;">' + this.key + '<br><strong>' + currencyFormat(this.y) + '</strong> (' + formatNumber(this.percentage) + '%)';
                },
                followPointer: true,
                useHTML: true,
                borderWidth: 0,
                shadow: false,
                valueDecimals: site.settings.decimals,
                style: {fontSize: '14px', padding: '0', color: '#000000'}
            },
            plotOptions: {
                pie: {
                    dataLabels: {
                        enabled: true,
                        formatter: function () {
                            return '<h3 style="margin:-15px 0 0 0;"><b>' + this.point.name + '</b>:<br><b> ' + currencyFormat(this.y) + '</b></h3>';
                        },
                        useHTML: true
                    }
                }
            },
            series: [{
                type: 'pie',
                name: '<?php echo lang("5"); ?>',
                data: [ 
                    ['<?php echo lang("សម្ភាសន៏ជនភៀសខ្លួន"); ?>', <?= !empty($interview_refugees->number)?$interview_refugees->number:0 ?>],
                    ['<?php echo lang("អវិជ្ជមាននៃការកំណត់ឋានៈ"); ?>',<?= !empty($interview_negatives->number)?$interview_negatives->number:0 ?> ],
                    ['<?php echo lang("ការទទួលស្គាល់ជនភៀសខ្លួន"); ?>', <?= !empty($interview_recognitions->number)?$interview_recognitions->number:0 ?>] 
                ]  
            }]
        }); 
    });
</script>
	<div class="row" style="margin-bottom: 15px;">	
		<div class="col-lg-12">
			<div class="box">
				<div class="box-header">
					<h2 class="blue"><i class="fa fa-th"></i><span class="break"></span><?= lang("") ?></h2>
				</div>
				
				<div class="box-content">
					<div class="col-lg-3 col-md-4 col-xs-6">
						<a class="bblue white quick-button small" href="asylum_seekers/counseling">
							<i class="fa fa-barcode"></i>
							<p><?= lang(" ទំរង់ពាក្យសុំផ្តល់ការប្រឹក្សាបឋម") ?></p>
							<h1><?=  $count_all_counseling->number ;?></h1> 
						</a>
					</div>
					<div class="col-lg-3 col-md-4 col-xs-6">
						<a class="bdarkGreen white quick-button small" href="asylum_seekers/rsd_application">
							<i class="fa fa-heart"></i>
							<p><?= lang(" ទម្រង់ចុះឈ្មោះជនសុំសិទ្ធិជ្រកកោន") ?></p>
							<h1><?=  $count_all_rsd->number ;?></h1> 
						</a>
					</div>
					<div class="col-lg-3 col-md-4 col-xs-6">
						<a class="blightOrange white quick-button small" href="application_forms/recognition_refugee">
							<i class="fa fa-star-o"></i>
							<p><?= lang(" ការទទួលស្គាល់ជនភៀសខ្លួន") ?></p>
							<h1><?=  $count_all_refugee_recognized->number ;?></h1> 
						</a>
					</div>
					<div class="col-lg-3 col-md-4 col-xs-6">
						<a class="bred white quick-button small" href="application_forms/refugee_card">
							<i class="fa fa-group"></i>
							<p><?= lang(" ពាក្យសុំប័ណ្ណសំគាល់ជនភៀសខ្លួន") ?></p>
							<h1><?=  $count_all_refugee_card->number ;?></h1>  
						</a>
					</div>
					<div class="clearfix"></div>
					
				</div>
				
			</div>
		</div>
	
	</div>
	
	<?php if ($Owner || $Admin) { ?>
	
	<div class="row" style="margin-bottom: 15px;">
	
		<div class="col-sm-6">
			<div class="box">
				<div class="box-header">
					<h2 class="blue">
						<i class="fa-fw fa fa-bar-chart-o"></i>
						<?= lang('ផ្នែកទទួលពាក្យ និងចុះឈ្មោះ') ?>
					</h2>
					<div class="box-icon">
						<ul class="btn-tasks">
							<li class="dropdown">
								<a href="javascript:void(0)" title="<?= lang('print') ?>" onclick="window.print()">
									<i class="icon fa fa-print"></i>
								</a>
							</li>                    
						</ul>
					</div>
				</div>
				<div class="box-content">
					<div class="row">						
						<div id="m2bschart" style="width:100%; height:450px;"></div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="col-sm-6">
			<div class="box">
				<div class="box-header">
					<h2 class="blue">
						<i class="fa-fw fa fa-bar-chart-o"></i>
						<?= lang(' ផ្នែកស្រាវជ្រាវ និងកំណត់ឋានះ ') ?>
					</h2>
					<div class="box-icon">
						<ul class="btn-tasks">
							<li class="dropdown">
								<a href="javascript:void(0)" title="<?= lang('print') ?>" onclick="window.print()">
									<i class="icon fa fa-print"></i>
								</a>
							</li>                    
						</ul>
					</div>
				</div>
				<div class="box-content">
					<div id="chart" style="width:100%; height:450px;"></div>
				</div>
			</div>
		</div>
		<div class="col-sm-4 hidden">
			<div class="box">
				<div class="box-header">
					<h2 class="blue">
						<i class="fa-fw fa fa-bar-chart-o"></i>
						<?= lang('  ផ្នែកគ្រប់គ្រង និងកិច្ចកាសង្គម  ') ?>
					</h2>
					<div class="box-icon">
						<ul class="btn-tasks">
							<li class="dropdown">
								<a href="javascript:void(0)" title="<?= lang('print') ?>" onclick="window.print()">
									<i class="icon fa fa-print"></i>
								</a>
							</li>                    
						</ul>
					</div>
				</div>
				<div class="box-content">
				</div>
			</div>
		</div>
	</div>
	
	<?php } ?>


<?php } ?>

